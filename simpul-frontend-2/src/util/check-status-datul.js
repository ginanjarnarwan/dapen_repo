export default function (i) {

	if (i === 1) {
		return 'Selesai Datul'
	} else if (i === 2) {
		return 'Sudah Datul - Menunggu Konfirmasi Dapentel'
	} else if (i === 3) {
		return 'Sudah Datul - Menunggu Konfirmasi P2TEL'
	} else if (i === 4) {
		return 'Belum Datul'
	} else if (i === 5) {
		return 'Tidak Wajib Datul'
	}

}