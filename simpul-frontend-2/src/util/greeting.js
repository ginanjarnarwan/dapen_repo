import * as moment from 'moment'

export default () => {
  let time = 'Selamat Pagi'
  const currentHour = parseFloat(moment().format('HH'))
  if (currentHour >= 12 && currentHour <= 14) {
    time = 'Selamat Siang'
  }
  if (currentHour >= 14 && currentHour <= 18) {
    time = 'Selamat Sore'
  }
  if (currentHour >= 18) {
    time = 'Selamat Malam'
  }
  return `${time}`
}
