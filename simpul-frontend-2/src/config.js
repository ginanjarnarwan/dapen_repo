import axios from 'axios'

// const DOMAIN = 'http://178.128.80.242:3000'
// const DOMAIN = 'http://128.199.244.131:3500'
const DOMAIN = 'https://apisimpul.finnet.co.id/'


export const HTTP = axios.create({
	// baseURL: 'https://apisimpul.finnet.co.id/'
	//baseURL: 'http://159.65.129.17/api/simpul-backend/',
	baseURL: DOMAIN,
	// baseURL: 'http://128.199.244.131:3000',
	//baseURL: 'http://localhost:3003'
	// headers: { 'Access-Control-Allow-Methods': '*' },
	
})

export const HTTPFORM = axios.create({
         // baseURL: 'https://apisimpul.finnet.co.id/',
         //baseURL: 'http://159.65.129.17/api/simpul-backend/',
         baseURL: DOMAIN,
         // baseURL: 'http://128.199.244.131:3000',
         //baseURL: 'http://localhost:3003',
         headers: {
           "content-type": "multipart/form-data"
         }
       });

export const HTTPEXCEL = axios.create({
         // baseURL: 'https://apisimpul.finnet.co.id/',
         //baseURL: 'http://159.65.129.17/api/simpul-backend/',
         baseURL: DOMAIN,
         // baseURL: 'http://128.199.244.131:3000',
         //baseURL: 'http://localhost:3003',
         responseType: "blob"
       });