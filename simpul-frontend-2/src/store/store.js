import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'

Vue.use(Vuex)

const vuexLocal = new VuexPersistence({
  storage: window.localStorage
})

export const store = new Vuex.Store({
  state: {
    login: {
      token: ''
    },
    header_name: {
      nik_user: '',
      nama_lengkap: ''
    },
   
  },
  mutations: {
    setlogin: (state, login) => {
      Object.assign(state.login, login)
    },
    setlogout: (state) => {
      Object.assign(state.login, null)
    },
    setheadername: (state, data) => {
      Object.assign(state.header_name, data)
    }
    
  },
  plugins: [vuexLocal.plugin]
})
