import Vue from 'vue'
import Router from 'vue-router'

function loadPages (component) {
  // '@' is aliased to src/components
  return () => import(`@/pages/${component}.vue`)
}

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: "/",
      name: "Login",
      component: loadPages("login")
    },
    {
      path: "/dashboard",
      name: "dashboard",
      component: loadPages("dashboard")
    },
    {
      path: "/anggota",
      name: "anggota",
      component: loadPages("anggota")
    },
    {
      path: "/datul-bank",
      name: "Datul Bank",
      component: loadPages("datul-bank/cari")
    },
    {
      path: "/data-ulang",
      name: "Data Ulang",
      component: loadPages("data-ulang/anggota/dashboard")
    },

    {
      path: "/data-ulang/daftar-wajib-datul",
      name: "Daftar Wajib Data Ulang",
      meta: {
        status: null
      },
      component: loadPages("data-ulang/anggota/data-daftar-wajib-datul")
    },

    {
      path: "/data-ulang/tangguhanl",
      name: "Tangguhan",
      meta: {
        status: 6
      },
      component: loadPages("data-ulang/anggota/data-tangguhan")
    },

    {
      path: "/data-ulang/pmp-baru",
      name: "PMP - Baru",
      meta: {
        status: null
      },
      component: loadPages("data-ulang/anggota/data-pmp-baru")
    },

    {
      path: "/data-ulang/tidak-wajib-datul",
      name: "Tidak Wajib Data Ulang",
      meta: {
        status: 5
      },
      component: loadPages("data-ulang/anggota/data-tidak-wajib-datul")
    },

    {
      path: "/data-ulang/belum-datul",
      name: "Belum Data Ulang",
      meta: {
        status: 4
      },
      component: loadPages("data-ulang/anggota/data-belum-datul")
    },

    {
      path: "/data-ulang/menunggu-konfirmasi-p2tel",
      name: "Sudah Datul - Menunggu Konfirmasi P2Tel",
      meta: {
        status: 3
      },
      component: loadPages("data-ulang/anggota/data-menunggu-konfirmasi-p2tel")
    },

    {
      path: "/data-ulang/program-dapen-menyapa",
      name: "Program Dapen Menyapa",
      meta: {
        status: 2
      },
      component: loadPages("data-ulang/anggota/data-menunggu-konfirmasi-dapen")
    },

    {
      path: "/data-ulang/selesai-datul",
      name: "Selesai Datul",
      meta: {
        status: 1
      },
      component: loadPages("data-ulang/anggota/data-selesai-datul")
    },

    {
      path: "/datul",
      name: "Selesai Datul",
      component: loadPages("datul-anggota/summary-datul-anggota")
    },
    {
      path: "/datul/daftar-wajib-datul",
      parent: "datul",
      name: "Daftar Wajib Datul",
      component: loadPages("datul-anggota/daftar-wajib-datul")
    },
    {
      path: "/datul/tidak-wajib-datul",
      name: "Tidak Wajib Datul",
      component: loadPages("datul-anggota/tidak-wajib-datul")
    },
    {
      path: "/tangguhan",
      name: "Tangguhan",
      component: loadPages("datul-anggota/tangguhan")
    },
    {
      path: "/pmp-baru",
      name: "Tangguhan",
      component: loadPages("datul-anggota/pmp-baru")
    },
    {
      path: "/berita/berita-baru",
      name: "BeritaBaru",
      component: loadPages("berita/berita-baru")
    },
    {
      path: "/berita/daftar-berita",
      name: "DaftarBerita",
      component: loadPages("berita/daftar-berita")
    },
    {
      path: "/lapor",
      name: "Lapor",
      component: loadPages("lapor")
    },

    {
      path: "/pelayanan",
      name: "Pelayanan",
      component: loadPages("pelayanan/")
    },

    {
      path: "/pelayanan/non-datul-dan-kunjungan/dashboard",
      name: "Non Datul dan Kunjungan",
      component: loadPages("pelayanan/non-datul-dan-kunjungan/dashboard")
    },

    {
      path: "/pelayanan/non-datul-dan-kunjungan/daftar-absen",
      name: "Daftar Absen Non Datul dan Kunjungan",
      component: loadPages("pelayanan/non-datul-dan-kunjungan/daftar-absen")
    },

    {
      path: "/pelayanan/non-datul-dan-kunjungan/daftar-belum",
      name: "Daftar Belum",
      component: loadPages("pelayanan/non-datul-dan-kunjungan/daftar-belum")
    },

    {
      path: "/pelayanan/non-datul-dan-kunjungan/daftar-selesai",
      name: "Daftar Selesai",
      component: loadPages("pelayanan/non-datul-dan-kunjungan/daftar-selesai")
    },

    {
      path: "/pelayanan/non-datul-dan-kunjungan/daftar-kunjungan",
      name: "Daftar Kunjungan",
      component: loadPages("pelayanan/non-datul-dan-kunjungan/daftar-kunjungan")
    },

    {
      path:
        "/pelayanan/non-datul-dan-kunjungan/daftar-belum-absen-dan-kunjungan",
      name: "Daftar Belum Absen dan Kunjungan",
      component: loadPages(
        "pelayanan/non-datul-dan-kunjungan/daftar-belum-absen-dan-kunjungan"
      )
    },

    {
      path: "/pelayanan/non-datul-dan-kunjungan/daftar-menunggu-kunjungan",
      name: "Daftar Menunggu Kunjungan",
      component: loadPages(
        "pelayanan/non-datul-dan-kunjungan/daftar-menunggu-kunjungan"
      )
    },

    {
      path: "/pelayanan/non-datul-dan-kunjungan/daftar-selesai-kunjungan",
      name: "Daftar Selesai Kunjungan",
      component: loadPages(
        "pelayanan/non-datul-dan-kunjungan/daftar-selesai-kunjungan"
      )
    },

    {
      path: "/pelayanan/non-datul-dan-kunjungan/form-absen/:id",
      name: "Form Absen",
      component: loadPages("pelayanan/non-datul-dan-kunjungan/form-absen")
    },

    {
      path: "/pelayanan/non-datul-dan-kunjungan/absen-sukses/:id",
      name: "Absen Sukses",
      component: loadPages("pelayanan/non-datul-dan-kunjungan/absen-sukses")
    },

    {
      path: "/pelayanan/non-datul-dan-kunjungan/form-kunjungan/:id",
      name: "Form Kunjungan",
      component: loadPages("pelayanan/non-datul-dan-kunjungan/form-kunjungan")
    },

    {
      path: "/pelayanan/non-datul-dan-kunjungan/kunjungan-sukses/:id",
      name: "Kunjungan Sukses",
      component: loadPages("pelayanan/non-datul-dan-kunjungan/kunjungan-sukses")
    },

    {
      path: "/pelayanan/non-datul-dan-kunjungan/rekap",
      name: "Rekap",
      component: loadPages("pelayanan/non-datul-dan-kunjungan/rekap")
    },

    {
      path: "/pelayanan/non-datul-dan-kunjungan/daftar-kunjungan/:item",
      name: "Daftar Kunjungan",
      component: loadPages("pelayanan/non-datul-dan-kunjungan/daftar-kunjungan")
    },

    {
      path: "/pelayanan/non-datul-dan-kunjungan/daftar-menunggu-kunjungan/:item",
      name: "Daftar Menunggu Kunjungan",
      component: loadPages("pelayanan/non-datul-dan-kunjungan/daftar-menunggu-kunjungan")
    },

    {
      path: "/pelayanan/non-datul-dan-kunjungan/daftar-selesai-kunjungan/:item",
      name: "Daftar Selesai Kunjungan",
      component: loadPages("pelayanan/non-datul-dan-kunjungan/daftar-selesai-kunjungan")
    },

    // {
    //   path: '/laporan/laporan-baru',
    //   name: 'LaporanBaru',
    //   component: loadPages('laporan/laporan-baru')
    // },
    // {
    //   path: '/laporan/daftar-laporan',
    //   name: 'DaftarLaporan',
    //   component: loadPages('laporan/daftar-laporan')
    // },
    {
      path: "/mobile-config/welcome-screen",
      name: "WelcomeScreen",
      component: loadPages("mobile-config/welcome-screen")
    },
    {
      path: "/mobile-config/login-screen",
      name: "LoginScreen",
      component: loadPages("mobile-config/login-screen")
    },
    {
      path: "/mobile-config/user-list",
      name: "UserList",
      component: loadPages("mobile-config/user-list")
    },
    {
      path: "/mobile-config/berita",
      name: "Berita",
      component: loadPages("mobile-config/berita")
    },
    {
      path: "/mobile-config/dashboard-splash",
      name: "DashboardSplash",
      component: loadPages("mobile-config/dashboard-splash")
    },

    {
      path: "/p2tel/user-list",
      name: "P2tel User List",
      component: loadPages("p2tel/user-list")
    },
    {
      path: "/admin/update-data-pmp",
      name: "Update Data PMP",
      component: loadPages("admin/update-data-pmp")
    },
    {
      path: "/profile/:id",
      name: "Profile",
      component: loadPages("pmp/profile")
    },
    {
      path: "/data-profile/:id",
      name: "Data Profile",
      component: loadPages("anggota/data-profile")
    },
    {
      path: "/data-profile-update/:id",
      name: "Data Profile",
      component: loadPages("anggota/data-profile-update")
    },
    {
      path: "/datul/:id",
      name: "datul",
      component: loadPages("pmp/datul")
    }
  ],
  mode: "hash"
});
