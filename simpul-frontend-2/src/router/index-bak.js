import Vue from 'vue'
import Router from 'vue-router'

function loadPages (component) {
  // '@' is aliased to src/components
  return () => import(`@/pages/${component}.vue`)
}

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Login',
      component: loadPages('Login')
    },
    {
      path: '/dashboard',
      name: 'dashboard',
      component: loadPages('Dashboard')
    },
    {
      path: '/datul-anggota',
      name: 'Selesai Datul',
      component: loadPages('datul-anggota/summary-datul-anggota')
    },
    {
      path: '/datul-anggota/selesai-datul',
      parent: 'datul-anggota',
      name: 'Selesai Datul',
      component: loadPages('datul-anggota/selesai-datul')
    },
    {
      path: '/datul-anggota/belum-datul',
      name: 'Belum Datul',
      component: loadPages('datul-anggota/belum-datul')
    },
    {
      path: '/datul-anggota/total-anggota',
      name: 'Total Anggota',
      component: loadPages('datul-anggota/total-anggota')
    },
    {
      path: '/datul-anggota/konfirmasi-p2tel',
      name: 'Konfirmasi P2TEL',
      component: loadPages('datul-anggota/konfirmasi-p2tel')
    },
    {
      path: '/datul-anggota/konfirmasi-dapentel',
      name: 'Konfirmasi Dapentel',
      component: loadPages('datul-anggota/konfirmasi-dapentel')
    },
    {
      path: '/datul-anggota/tidak-wajib-datul',
      name: 'Tidak Wajib Datul',
      component: loadPages('datul-anggota/tidak-wajib-datul')
    },
    {
      path: '/berita/berita-baru',
      name: 'BeritaBaru',
      component: loadPages('berita/berita-baru')
    },
    {
      path: '/berita/daftar-berita',
      name: 'DaftarBerita',
      component: loadPages('berita/daftar-berita')
    },
    {
      path: '/laporan/laporan-baru',
      name: 'LaporanBaru',
      component: loadPages('laporan/laporan-baru')
    },
    {
      path: '/laporan/daftar-laporan',
      name: 'DaftarLaporan',
      component: loadPages('laporan/daftar-laporan')
    },
    {
      path: '/mobile-config/welcome-screen',
      name: 'WelcomeScreen',
      component: loadPages('mobile-config/welcome-screen')
    },
    {
      path: '/mobile-config/login-screen',
      name: 'LoginScreen',
      component: loadPages('mobile-config/login-screen')
    },
    {
      path: '/mobile-config/dashboard-splash',
      name: 'DashboardSplash',
      component: loadPages('mobile-config/dashboard-splash')
    },
    {
      path: '/profile/:id',
      name: 'Profile',
      component: loadPages('pmp/profile')
    }
  ],
  mode: 'hash'
})
