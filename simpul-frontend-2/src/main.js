// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import {store} from './store/store'
import router from './router'
import BootstrapVue from '@/components/bootstrap-vue/src'
import AsyncComputed from 'vue-async-computed'
import VeeValidate from 'vee-validate'
import VueFormWizard from 'vue-form-wizard'
import 'vue-form-wizard/dist/vue-form-wizard.min.css'
import Loading from 'vue-loading-overlay';
import 'vue-loading-overlay/dist/vue-loading.min.css';

Vue.use(AsyncComputed)
Vue.use(BootstrapVue)
Vue.use(VeeValidate)
Vue.use(VueFormWizard)
Vue.use(Loading)

Vue.config.productionTip = false

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App }
})
