var number_format;
var db_format;
var rp_format;

$(document).ready(function() {

	number_format = function(number, decimals, dec_point, thousands_sep)
	{
		number = (number + '').replace(/[^0-9+\-Ee.]/g, '');
		number = (number + '').replace(/[.]/g, '');
		var n = !isFinite(+number) ? 0 : +number,
			prec = !isFinite(+decimals) ? 0 : Math.abs(decimals),
			sep = (typeof thousands_sep === 'undefined') ? '.' : thousands_sep,
			dec = (typeof dec_point === 'undefined') ? ',' : dec_point,
			s = '',
			toFixedFix = function (n, prec) {
				var k = Math.pow(10, prec);
				return '' + Math.round(n * k) / k;
			};
		// Fix for IE parseFloat(0.55).toFixed(0) = 0;
		s = (prec ? toFixedFix(n, prec) : '' + Math.round(n)).split('.');
		if (s[0].length > 3) {
			s[0] = s[0].replace(/\B(?=(?:\d{3})+(?!\d))/g, sep);
		}
		if ((s[1] || '').length < prec) {
			s[1] = s[1] || '';
			s[1] += new Array(prec - s[1].length + 1).join('0');
		}
		return s.join(dec);
	}

	db_format = function(number)
	{
		if (number=='') return 0;
		else return parseInt((number + '').replace(/[.,\,Rp]/g, ''));
	}

	rp_format = function(number)
	{
		return 'Rp ' + number_format(number) + ',-';
	}

	currency_init = function(){
		$('.uang').keyup(function() {
			$(this).val(number_format($(this).val()));
		});
		$('.uang').change(function() {
			$(this).val(number_format($(this).val()));
		});

		$('.uang').each(function() {
			$(this).val(number_format($(this).val()));
		});
		$('.rupiah').each(function() {
			$(this).val(rp_format($(this).val()));
		});
	}
	currency_init();
});
