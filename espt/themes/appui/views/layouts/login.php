<!DOCTYPE html>
<!--[if IE 9]>         <html class="no-js lt-ie10" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">

        <title>Login - E-SPT Pensiun</title>

        <link rel="shortcut icon" href="<?php echo Yii::app()->theme->baseUrl;?>/img/placeholders/layout/logo.png">
        <link rel="apple-touch-icon" href="<?php echo Yii::app()->theme->baseUrl;?>/img/icon57.png" sizes="57x57">
        <link rel="apple-touch-icon" href="<?php echo Yii::app()->theme->baseUrl;?>/img/icon72.png" sizes="72x72">
        <link rel="apple-touch-icon" href="<?php echo Yii::app()->theme->baseUrl;?>/img/icon76.png" sizes="76x76">
        <link rel="apple-touch-icon" href="<?php echo Yii::app()->theme->baseUrl;?>/img/icon114.png" sizes="114x114">
        <link rel="apple-touch-icon" href="<?php echo Yii::app()->theme->baseUrl;?>/img/icon120.png" sizes="120x120">
        <link rel="apple-touch-icon" href="<?php echo Yii::app()->theme->baseUrl;?>/img/icon144.png" sizes="144x144">
        <link rel="apple-touch-icon" href="<?php echo Yii::app()->theme->baseUrl;?>/img/icon152.png" sizes="152x152">
        <link rel="apple-touch-icon" href="<?php echo Yii::app()->theme->baseUrl;?>/img/icon180.png" sizes="180x180">
        <!-- END Icons -->

        <?php 
        Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/css/bootstrap.min.css');
        Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/css/plugins.css');
        Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/css/main.css');
        Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/css/themes.css');
        ?>

    </head>
    <body>
        <!-- Full Background -->
        <!-- For best results use an image with a resolution of 1280x1280 pixels (prefer a blurred image for smaller file size) -->
        <img src="<?php echo Yii::app()->theme->baseUrl;?>/img/placeholders/layout/header.jpg" alt="Full Background" class="full-bg animation-pulseSlow">
        <!-- END Full Background -->
        <?php echo $content; ?>
        
         <?php
        Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/vendor/modernizr-3.3.1.min.js', CClientScript::POS_BEGIN);
        Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/vendor/jquery-2.2.4.min.js', CClientScript::POS_END);
        Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/vendor/bootstrap.min.js', CClientScript::POS_END);
        Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/plugins.js', CClientScript::POS_END);
        Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/app.js', CClientScript::POS_END);
        Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/pages/readyLogin.js', CClientScript::POS_END);
        ?>
        <script>$(function(){ ReadyLogin.init(); });</script>
    </body>
</html>