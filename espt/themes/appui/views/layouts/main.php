<!DOCTYPE html>
<!--[if IE 9]>         <html class="no-js lt-ie10" lang="en"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js" lang="en"> <!--<![endif]-->
<head>
<meta charset="utf-8">

<title>E-SPT</title>
<!-- Icons -->
<!-- The following icons can be replaced with your own, they are used by desktop and mobile browsers -->
<link rel="shortcut icon" href="<?php echo Yii::app()->theme->baseUrl;?>/placeholders/layout/logo.png">
<link rel="apple-touch-icon" href="<?php echo Yii::app()->theme->baseUrl;?>/img/icon57.png" sizes="57x57">
<link rel="apple-touch-icon" href="<?php echo Yii::app()->theme->baseUrl;?>/img/icon72.png" sizes="72x72">
<link rel="apple-touch-icon" href="<?php echo Yii::app()->theme->baseUrl;?>/img/icon76.png" sizes="76x76">
<link rel="apple-touch-icon" href="<?php echo Yii::app()->theme->baseUrl;?>/img/icon114.png" sizes="114x114">
<link rel="apple-touch-icon" href="<?php echo Yii::app()->theme->baseUrl;?>/img/icon120.png" sizes="120x120">
<link rel="apple-touch-icon" href="<?php echo Yii::app()->theme->baseUrl;?>/img/icon144.png" sizes="144x144">
<link rel="apple-touch-icon" href="<?php echo Yii::app()->theme->baseUrl;?>/img/icon152.png" sizes="152x152">
<link rel="apple-touch-icon" href="<?php echo Yii::app()->theme->baseUrl;?>/img/icon180.png" sizes="180x180">
<!-- END Icons -->

<!-- Stylesheets -->
<!-- Bootstrap is included in its original form, unaltered -->
<?php 
Yii::app()->clientScript->registerCoreScript('jquery');
Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/css/sweetalert.css');
Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/css/bootstrap.min.css');
Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/css/plugins.css');
Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/css/main.css');
Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/css/themes.css');
?>
<?php Yii::app()->getClientScript()->registerCoreScript('jquery.ui'); ?>

</head>
<?php MyApp::setLog('ok'); ?>
<body>
<!-- Page Wrapper -->
<!-- In the PHP version you can set the following options from inc/config file -->
<!--
	Available classes:

	'page-loading'      enables page preloader
-->
<div id="page-wrapper" class="page-loading">
	<!-- Preloader -->
	<!-- Preloader functionality (initialized in js/app.js) - pageLoading() -->
	<!-- Used only if page preloader enabled from inc/config (PHP version) or the class 'page-loading' is added in #page-wrapper element (HTML version) -->
	<div class="preloader">
		<div class="inner">
			<!-- Animation spinner for all modern browsers -->
			<div class="preloader-spinner themed-background hidden-lt-ie10"></div>

			<!-- Text for IE9 -->
			<h3 class="text-primary visible-lt-ie10"><strong>Loading..</strong></h3>
		</div>
	</div>
	<div id="page-container" class="header-fixed-top sidebar-visible-lg-full">
		<!-- Alternative Sidebar -->
		<div id="sidebar-alt" tabindex="-1" aria-hidden="true">
			<!-- Toggle Alternative Sidebar Button (visible only in static layout) -->
			<a href="javascript:void(0)" id="sidebar-alt-close" onclick="App.sidebar('toggle-sidebar-alt');"><i class="fa fa-times"></i></a>

		</div>
		<!-- END Alternative Sidebar -->

		<!-- Main Sidebar -->
		<div id="sidebar">
			<!-- Sidebar Brand -->
			<div id="sidebar-brand" class="themed-background">
				<a href="<?php echo Yii::app()->createUrl('site/index');?>" class="sidebar-title">
					<img src="<?php echo Yii::app()->theme->baseUrl;?>/img/placeholders/layout/logo2.png" style="height:20px;"> <span class="sidebar-nav-mini-hide">E - <strong>SPT</strong></span>
				</a>
			</div>
			<!-- END Sidebar Brand -->

			<!-- Wrapper for scrolling functionality -->
			<div id="sidebar-scroll">
				<!-- Sidebar Content -->
				<div class="sidebar-content">
					<!-- Sidebar Navigation -->
					<ul class="sidebar-nav">
						<li>
							<a href="<?php echo Yii::app()->createUrl('/site/index');?>"><i class="fa fa-home sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Home</span></a>
						</li>

						<?php if (Yii::app()->user->roles == "Admin"|| Yii::app()->user->roles == "Admin_witel" || Yii::app()->user->roles == "Admin_dapen") {	?>
						<li>
							<a href="<?php echo Yii::app()->createUrl('/admin/dashboardAdmin');?>"><i class="fa fa-cog sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Dashboard</span></a>
						</li>
						<?php } ?>

						<?php
						if (Yii::app()->user->roles == "Admin" || Yii::app()->user->roles == "Admin_witel" || Yii::app()->user->roles == "Admin_dapen" || Yii::app()->user->roles == "Finance") {
						?>
						<?php
						if (Yii::app()->user->roles == "Admin" || Yii::app()->user->roles == "Admin_witel" || Yii::app()->user->roles == "Admin_dapen") {
						?>
						<li>
							<a href="<?php echo Yii::app()->createUrl('/admin/searchbynik');?>"><i class="fa fa-cog sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Search By NIK</span></a>
						</li>
						<?php } ?>
						<?php
						if (Yii::app()->user->roles == "Admin" || Yii::app()->user->roles == "Admin_dapen" || Yii::app()->user->roles == "Finance") {
						?>
						<li>
							<a href="#" class="sidebar-nav-menu"><span class="sidebar-nav-ripple" style="height: 220px; width: 220px; top: -97px; left: 42px;"></span><i class="fa fa-chevron-left sidebar-nav-indicator sidebar-nav-mini-hide"></i><i class="fa fa-upload sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Upload Bukti Potong</span></a>
							<ul>
								<li>
									<?php
									if (Yii::app()->user->roles == "Admin" || Yii::app()->user->roles == "Finance") {
									?>
									<a href="<?php echo Yii::app()->createUrl('/admin/uploadtidakfinal');?>">Tidak Final</a>
									<?php } ?>
									<a href="<?php echo Yii::app()->createUrl('/admin/uploadfinal');?>">Final</a>
								</li>
							</ul>
						</li>
						<?php } ?>
						<?php
						if (Yii::app()->user->roles == "Admin") {
						?>
						<li>
							<a href="<?php echo Yii::app()->createUrl('/admin/upload1770s');?>"><i class="fa fa-upload sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Upload 1770-S</span></a>
						</li>
						<?php
						}
						?>
						<?php
						}
						?>
						<?php 
						if (Yii::app()->user->roles == "Admin" || Yii::app()->user->roles == "Admin_dapen") {
						?>
						<li>
							<a href="<?php echo Yii::app()->createUrl('/admin/upload1721');?>"><i class="fa fa-upload sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Upload 1721-A1</span></a>
						</li>
						<?php
						}
						?>
						<?php
						if (Yii::app()->user->roles == "Admin") {
						?>
						<li>
							<a href="<?php echo Yii::app()->createUrl('/user/index');?>"><i class="fa fa-cog sidebar-nav-icon"></i><span class="sidebar-nav-mini-hide">Manage Password</span></a>
						</li>
						<?php
						}
						?>
					</ul>
				</div>
				<!-- END Sidebar Content -->
			</div>
			<!-- END Wrapper for scrolling functionality -->

			<!-- Sidebar Extra Info -->
			<div id="sidebar-extra-info" class="sidebar-content sidebar-nav-mini-hide">
				<div class="text-center">
					<small>&copy; Telkom Indonesia 2017</small>
				</div>
			</div>
			<!-- END Sidebar Extra Info -->
		</div>
		<!-- END Main Sidebar -->

		<!-- Main Container -->
		<div id="main-container">
			<!-- Header -->
			<!-- In the PHP version you can set the following options from inc/config file -->
			<!--
				Available header.navbar classes:

				'navbar-default'            for the default light header
				'navbar-inverse'            for an alternative dark header

				'navbar-fixed-top'          for a top fixed header (fixed main sidebar with scroll will be auto initialized, functionality can be found in js/app.js - handleSidebar())
					'header-fixed-top'      has to be added on #page-container only if the class 'navbar-fixed-top' was added

				'navbar-fixed-bottom'       for a bottom fixed header (fixed main sidebar with scroll will be auto initialized, functionality can be found in js/app.js - handleSidebar()))
					'header-fixed-bottom'   has to be added on #page-container only if the class 'navbar-fixed-bottom' was added
			-->
			<header class="navbar navbar-inverse navbar-fixed-top" style="height:0px;">
				<!-- Left Header Navigation -->
				<ul class="nav navbar-nav-custom">
					<!-- Main Sidebar Toggle Button -->
					<li>
						<a href="javascript:void(0)" onclick="App.sidebar('toggle-sidebar');this.blur();">
							<i class="fa fa-ellipsis-v fa-fw animation-fadeInRight" id="sidebar-toggle-mini"></i>
							<i class="fa fa-bars fa-fw animation-fadeInRight" id="sidebar-toggle-full"></i>
						</a>
					</li>
				</ul>
				<!-- END Left Header Navigation -->

				<!-- Right Header Navigation -->
				<ul class="nav navbar-nav-custom pull-right">
					<li class="dropdown">
						<a href="javascript:void(0)" class="dropdown-toggle" data-toggle="dropdown">
							<img style="background:#f3f3f3 url(https://myworkbook.telkom.co.id/mwb/hcm_api/foto.php?nik=<?php echo Yii::app()->user->id;?>)no-repeat center / cover">
						</a>
						<ul class="dropdown-menu dropdown-menu-right">
							<li class="dropdown-header">
								<strong><?php echo Yii::app()->user->id;?></strong>
							</li>
							<li>
								<a href="<?php echo Yii::app()->createUrl('site/logout');?>">
									<i class="fa fa-power-off fa-fw pull-right"></i>
									Log out
								</a>
							</li>
						</ul>
					</li>
					<!-- END User Dropdown -->
				</ul>
				<!-- END Right Header Navigation -->
			</header>
			<!-- END Header -->

			<!-- Page content -->
			<div id="page-content">
				<?php echo $content;?>
			</div>
			<!-- END Page Content -->
		</div>
		<!-- END Main Container -->
	</div>
	<!-- END Page Container -->
</div>
<!-- END Page Wrapper -->

<!-- jQuery, Bootstrap, jQuery plugins and Custom JS code -->
<?php
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/vendor/modernizr-3.3.1.min.js', CClientScript::POS_BEGIN);
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/sweetalert.min.js', CClientScript::POS_BEGIN);
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/vendor/bootstrap.min.js', CClientScript::POS_BEGIN);
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/plugins.js', CClientScript::POS_BEGIN);
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/app.js', CClientScript::POS_BEGIN);
Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/js/pages/readyDashboard.js', CClientScript::POS_BEGIN);
Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/javascriptHelper.js', CClientScript::POS_BEGIN);
?>

<script>//$(function(){ ReadyDashboard.init(); });</script>
</body>
</html>