<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
	<meta name="description" content="Admin, Dashboard, Bootstrap" />
	<link rel="shortcut icon" sizes="196x196" href="<?php echo Yii::app()->theme->baseUrl.'/dist/image/logo.png'?>">
	<title>Cost Allocation</title>
	
  <?php  
	Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/libs/bower/font-awesome/css/font-awesome.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/libs/bower/material-design-iconic-font/dist/css/material-design-iconic-font.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/libs/bower/animate.css/animate.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/libs/bower/fullcalendar/dist/fullcalendar.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/libs/bower/perfect-scrollbar/css/perfect-scrollbar.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/dist/css/bootstrap.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/dist/css/core.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/dist/css/app.css');
  Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/dist/css/sweetalert.css');
  ?>
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,500,600,700,800,900,300">
  <script>
		Breakpoints();
	</script>
</head>
	
<body class="menubar-left menubar-unfold menubar-light theme-primary">
<!--============= start main area -->

<!-- APP NAVBAR ==========-->
<nav id="app-navbar" class="navbar navbar-inverse navbar-fixed-top primary">
<?php

?>
  <!-- navbar header -->
  <div class="navbar-header">
    <button type="button" id="menubar-toggle-btn" class="navbar-toggle visible-xs-inline-block navbar-toggle-left hamburger hamburger--collapse js-hamburger">
      <span class="sr-only">Toggle navigation</span>
      <span class="hamburger-box"><span class="hamburger-inner"></span></span>
    </button>

    <button type="button" class="navbar-toggle navbar-toggle-right collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
      <span class="sr-only">Toggle navigation</span>
      <span class="zmdi zmdi-hc-lg zmdi-more"></span>
    </button>

    <button type="button" class="navbar-toggle navbar-toggle-right collapsed" data-toggle="collapse" data-target="#navbar-search" aria-expanded="false">
      <span class="sr-only">Toggle navigation</span>
      <span class="zmdi zmdi-hc-lg zmdi-search"></span>
    </button>

    <a href="<?php Yii::app()->createUrl('payslip/index');?>" class="navbar-brand">
      <span class="brand-icon"><i class="fa fa-gg"></i></span>
      <span class="brand-name">Cost Allocation</span>
    </a>
  </div><!-- .navbar-header -->
  
  <div class="navbar-container container-fluid">
    <div class="collapse navbar-collapse" id="app-navbar-collapse">
      <ul class="nav navbar-toolbar navbar-toolbar-left navbar-left">
        <li class="hidden-float hidden-menubar-top">
          <a href="javascript:void(0)" role="button" id="menubar-fold-btn" class="hamburger hamburger--arrowalt is-active js-hamburger">
            <span class="hamburger-box"><span class="hamburger-inner"></span></span>
          </a>
        </li>
        <li>
          <h5 class="page-title hidden-menubar-top hidden-float">Dashboard</h5>
        </li>
      </ul>

      <ul class="nav navbar-toolbar navbar-toolbar-right navbar-right">
        <li class="nav-item dropdown hidden-float">
          <a href="javascript:void(0)" data-toggle="collapse" data-target="#navbar-search" aria-expanded="false">
            <i class="zmdi zmdi-hc-lg zmdi-search"></i>
          </a>
        </li>
      </ul>
    </div>
  </div><!-- navbar-container -->
</nav>
<!--========== END app navbar -->

<!-- APP ASIDE ==========-->
<aside id="menubar" class="menubar light">
  <div class="app-user">
    <div class="media">
      <div class="media-left">
        <div class="avatar avatar-md avatar-circle">
          <a href="javascript:void(0)">
          <img class="img-responsive" style="background:#f3f3f3 url(https://myworkbook.telkom.co.id/mwb/hcm_api/foto.php?nik=<?php echo Yii::app()->user->id;?>)no-repeat center / cover">
          <!-- <img class="img-responsive" src="../assets/images/221.jpg" alt="avatar"/></a> -->
        </div><!-- .avatar -->
      </div>
      <div class="media-body">
        <div class="foldable">
          <!-- <h5><a href="javascript:void(0)" class="username">Nanan Hernawan</a></h5> -->
          <ul>
            <li class="dropdown">
              <a href="javascript:void(0)" class="dropdown-toggle usertitle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <small><?php echo Yii::app()->user->id;?></small>
                <span class="caret"></span>
              </a>
              <ul class="dropdown-menu animated flipInY">
                <!-- <li>
                  <a class="text-color" href="<?php echo Yii::app()->createUrl('payslip/index')?>">
                    <span class="m-r-xs"><i class="fa fa-home"></i></span>
                    <span>Home</span>
                  </a>
                </li>
                <li>
                  <a class="text-color" href="profile.html">
                    <span class="m-r-xs"><i class="fa fa-user"></i></span>
                    <span>Profile</span>
                  </a>
                </li>
                <li>
                  <a class="text-color" href="settings.html">
                    <span class="m-r-xs"><i class="fa fa-gear"></i></span>
                    <span>Settings</span>
                  </a>
                </li> -->
                <li role="separator" class="divider"></li>
                <li>
                  <a class="text-color" href="<?php echo Yii::app()->createUrl('site/logout');?>">
                    <span class="m-r-xs"><i class="fa fa-power-off"></i></span>
                    <span>Logout</span>
                  </a>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div><!-- .media-body -->
    </div><!-- .media -->
  </div><!-- .app-user -->

  <div class="menubar-scroll">
    <div class="menubar-scroll-inner">
      <ul class="app-menu">
        <li>
          <a href="<?php echo Yii::app()->createUrl('payslip/index');?>">
            <i class="fa fa-home fa-2x"></i>
            <span class="menu-text">Home</span>
          </a>
        </li>
        <?php
        if (Yii::app()->user->roles=='Admin') {
        ?>
        <!-- <li>
          <a href="<?php echo Yii::app()->createUrl('payslip/uploadhrcurrent');?>">
            <i class="fa fa-user fa-2x"></i>
            <span class="menu-text">Upload HR Current</span>
          </a>
        </li>
        <li>
          <a href="<?php echo Yii::app()->createUrl('payslip/upload');?>">
            <i class="fa fa-upload fa-2x"></i>
            <span class="menu-text">Upload Payslip</span>
          </a>
        </li> -->
        <li>
          <a href="<?php echo Yii::app()->createUrl('payslip/uploadatk');?>">
            <i class="fa fa-upload fa-2x"></i>
            <span class="menu-text">Upload Segmen TREG</span>
          </a>
        </li>
        <li>
          <a href="<?php echo Yii::app()->createUrl('tRealsap/index');?>">
            <i class="fa fa-cog fa-2x"></i>
            <span class="menu-text">Manage Real SAP</span>
          </a>
        </li>
        <?php } ?>
        <!-- <li>
          <a href="<?php echo Yii::app()->createUrl('payslip/report');?>">
            <i class="fa fa-file fa-2x"></i>
            <span class="menu-text">Report</span>
          </a>
        </li> -->
        <?php
        if (Yii::app()->user->roles == 'Admin') {
        ?>
        <li>
          <a href="<?php echo Yii::app()->createUrl('Authassignment/index');?>">
            <i class="fa fa-user fa-2x"></i>
            <span class="menu-text">Manage User</span>
          </a>
        </li>
        <?php
        }
        ?>
        <li>
          <a href="<?php echo Yii::app()->createUrl('payslip/informasi');?>">
            <i class="fa fa-info-circle fa-2x"></i>
            <span class="menu-text">Informasi</span>
          </a>
        </li>
      </ul><!-- .app-menu -->
    </div><!-- .menubar-scroll-inner -->
  </div><!-- .menubar-scroll -->
</aside>
<!--========== END app aside -->

<!-- navbar search -->
<div id="navbar-search" class="navbar-search collapse">
  <div class="navbar-search-inner">
    <form action="#">
      <span class="search-icon"><i class="fa fa-search"></i></span>
      <input class="search-field" type="search" placeholder="search..."/>
    </form>
    <button type="button" class="search-close" data-toggle="collapse" data-target="#navbar-search" aria-expanded="false">
      <i class="fa fa-close"></i>
    </button>
  </div>
  <div class="navbar-search-backdrop" data-toggle="collapse" data-target="#navbar-search" aria-expanded="false"></div>
</div><!-- .navbar-search -->

<!-- APP MAIN ==========-->
<main id="app-main" class="app-main">
<div class="wrap">
	<section class="app-content">
		<?php echo $content; ?>
	</section><!-- #dash-content -->
</div><!-- .wrap -->
  <!-- APP FOOTER -->
  <div class="wrap p-t-0">
    <footer class="app-footer">
      <div class="clearfix">
        <!-- <ul class="footer-menu pull-right">
          <li><a href="javascript:void(0)">Careers</a></li>
          <li><a href="javascript:void(0)">Privacy Policy</a></li>
          <li><a href="javascript:void(0)">Feedback <i class="fa fa-angle-up m-l-md"></i></a></li>
        </ul> -->
        <div class="copyright pull-left">Copyright Telkom Indonesia 2017 &copy;</div>
      </div>
    </footer>
  </div>
  <!-- /#app-footer -->
</main>
<!--========== END app main -->

	<!-- APP CUSTOMIZER -->

	
	<!-- SIDE PANEL -->

	<!-- build:js ../assets/js/core.min.js -->
  <?php
  Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/libs/bower/breakpoints.js/dist/breakpoints.min.js', CClientScript::POS_BEGIN);
  Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/libs/bower/jquery/dist/jquery.js', CClientScript::POS_BEGIN);
  Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/libs/bower/jquery-ui/jquery-ui.min.js', CClientScript::POS_END);
  Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/libs/bower/jQuery-Storage-API/jquery.storageapi.min.js', CClientScript::POS_END);
  Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/libs/bower/bootstrap-sass/assets/javascripts/bootstrap.js', CClientScript::POS_END);
  Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/libs/bower/jquery-slimscroll/jquery.slimscroll.js', CClientScript::POS_END);
  Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/libs/bower/perfect-scrollbar/js/perfect-scrollbar.jquery.js', CClientScript::POS_END);
  Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/libs/bower/PACE/pace.min.js', CClientScript::POS_END);
  
  Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/dist/js/library.js', CClientScript::POS_END);
  Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/dist/js/plugins.js', CClientScript::POS_END);
  Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/dist/js/app.js', CClientScript::POS_END);
  
  Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/libs/bower/moment/moment.js', CClientScript::POS_END);
  Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/libs/bower/fullcalendar/dist/fullcalendar.min.js', CClientScript::POS_END);
  Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/dist/js/fullcalendar.js', CClientScript::POS_END);
  Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/dist/js/sweetalert.min.js', CClientScript::POS_BEGIN);
  Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/dist/js/tableHeadFixer.js', CClientScript::POS_BEGIN);
  Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/jquery.number.min.js', CClientScript::POS_BEGIN);
  Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/javascriptHelper.js', CClientScript::POS_BEGIN);
  ?>
<script type="text/javascript">
	$(document).ready(function($){
        $('.uang').number( true , 0 , ',' , '.' );
    });
  $('.del-btn').click(function(e){
    link = $(this).attr('href');
    e.preventDefault();
      swal({
        title: "Apakah anda yakin akan menghapus?",
        // text: "You will not be able to recover this imaginary file!",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#10c469",
        confirmButtonText: "Yes",
        closeOnConfirm: false,
        html: false
      }, function(){
        swal({
          title : "<i class='fa fa-spinner fa-pulse fa-3x fa-fw'></i>",
          html : "<i class='fa fa-spinner fa-pulse fa-3x fa-fw'></i>",
          showCancelButton: false,
          showConfirmButton: false
        });
        window.location.replace(link);
      });
  });
</script>
</body>
</html>