<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
	<meta name="description" content="Admin, Dashboard, Bootstrap" />
	<link rel="shortcut icon" sizes="196x196" href="<?php echo Yii::app()->theme->baseUrl.'/dist/image/logo.png'?>">
	<title>Cost Allocation</title>
	
  <?php  
	Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/libs/bower/font-awesome/css/font-awesome.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/libs/bower/material-design-iconic-font/dist/css/material-design-iconic-font.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/libs/bower/animate.css/animate.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/libs/bower/fullcalendar/dist/fullcalendar.min.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/libs/bower/perfect-scrollbar/css/perfect-scrollbar.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/dist/css/bootstrap.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/dist/css/core.css');
	Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/dist/css/app.css');
  Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/dist/css/sweetalert.css');
  ?>
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Raleway:400,500,600,700,800,900,300">
  <script>
		Breakpoints();
	</script>
</head>
	
<body class="und">
<?php echo $content; ?>

  <?php
  Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/libs/bower/breakpoints.js/dist/breakpoints.min.js', CClientScript::POS_BEGIN);
  Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/libs/bower/jquery/dist/jquery.js', CClientScript::POS_BEGIN);
  Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/libs/bower/jquery-ui/jquery-ui.min.js', CClientScript::POS_END);
  Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/libs/bower/jQuery-Storage-API/jquery.storageapi.min.js', CClientScript::POS_END);
  Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/libs/bower/bootstrap-sass/assets/javascripts/bootstrap.js', CClientScript::POS_END);
  Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/libs/bower/jquery-slimscroll/jquery.slimscroll.js', CClientScript::POS_END);
  Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/libs/bower/perfect-scrollbar/js/perfect-scrollbar.jquery.js', CClientScript::POS_END);
  Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/libs/bower/PACE/pace.min.js', CClientScript::POS_END);
  
  Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/dist/js/library.js', CClientScript::POS_END);
  Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/dist/js/plugins.js', CClientScript::POS_END);
  Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/dist/js/app.js', CClientScript::POS_END);
  
  Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/libs/bower/moment/moment.js', CClientScript::POS_END);
  Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/libs/bower/fullcalendar/dist/fullcalendar.min.js', CClientScript::POS_END);
  Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/dist/js/fullcalendar.js', CClientScript::POS_END);
  Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/dist/js/sweetalert.min.js', CClientScript::POS_BEGIN);
  Yii::app()->clientScript->registerScriptFile(Yii::app()->theme->baseUrl . '/dist/js/tableHeadFixer.js', CClientScript::POS_BEGIN);
  Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/jquery.number.min.js', CClientScript::POS_BEGIN);
  Yii::app()->clientScript->registerScriptFile(Yii::app()->baseUrl . '/js/javascriptHelper.js', CClientScript::POS_BEGIN);
  ?>
<script type="text/javascript">
	$(document).ready(function($){
        $('.uang').number( true , 0 , ',' , '.' );
    })
</script>
</body>
</html>