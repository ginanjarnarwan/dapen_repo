<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $model <?php echo $this->getModelClass(); ?> */

<?php
$label = $this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs=array(
	'$label'=>array('index'),
	'Manage',
);\n";
?>


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$('#<?php echo $this->class2id($this->modelClass); ?>-grid').yiiGridView('update', {
data: $(this).serialize()
});
return false;
});
");
?>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Manage <?php echo $this->pluralize($this->class2name($this->modelClass)); ?></h3>
				<div class="pull-right">
					<?php echo '<?php echo CHtml::link("Create", Yii::app()->controller->createUrl("create"), array("class" => "btn btn-sm btn-primary")); ?>'." \n"; ?>
				</div>
			</div>
			<div class="box-body">
				<div class="alert alert-danger text-center">
					<?php echo "<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?> \n"; ?>
				</div>
				<div class="search-form" style="display:none">
					<div class="box box-solid box-primary">
						<div class="box-body">
							<?php echo "<?php \$this->renderPartial('_search',array(
								'model'=>\$model,
							)); ?>\n"; ?>
						</div>
					</div>
				</div><!-- search-form -->
				<?php echo "<?php"; ?> $this->widget('zii.widgets.grid.CGridView', array(
				'id'=>'<?php echo $this->class2id($this->modelClass); ?>-grid',
				'dataProvider'=>$model->search(),
				'filter'=>$model,
				'enableSorting' => false,
				'itemsCssClass' => 'table table-bordered table-striped',
				'pagerCssClass'=>'paging',
				'pager'=>array(
					'class'=>'CLinkPager',
					'header'=>'',
					'selectedPageCssClass'=>'active',
					'htmlOptions'=>array(
						'class'=>'pagination pagination-sm'
					)
				),
				'columns'=>array(
				<?php
				$count = 0;
				foreach ($this->tableSchema->columns as $column) {
					if (++$count == 7)
					echo "\t\t/*\n";
					echo "\t\t'" . $column->name . "',\n";
				}
				if ($count >= 7)
				echo "\t\t*/\n";
				?>
				array(
				'class'=>'CButtonColumn',
				'buttons' => array(
				'view' => array(
				'label' => '<i class="fa fa-external-link"></i>',
				'imageUrl' => FALSE,
				'options' => array('class' => 'btn btn-primary btn-condensed btn-sm')
				),
				'update' => array(
				'label' => '<i class="fa fa-pencil"></i>',
				'imageUrl' => FALSE,
				'options' => array('class' => 'btn btn-warning btn-condensed btn-sm')
				),
				'delete' => array(
				'label' => '<i class="fa fa-trash"></i>',
				'imageUrl' => FALSE,
				'options' => array('class' => 'btn btn-danger btn-condensed btn-sm')
				),
				),
				'htmlOptions' => array('style' => 'width:130px;text-align:center;')
				),
				),
				)); ?>
			</div>
		</div>
	</div>
</div>
