<?php
/**
 * The following variables are available in this template:
 * - $this: the CrudCode object
 */
?>
<?php echo "<?php\n"; ?>
/* @var $this <?php echo $this->getControllerClass(); ?> */
/* @var $model <?php echo $this->getModelClass(); ?> */
/* @var $form CActiveForm */
?>

<div class="box-body">

    <?php echo "<?php \$form=\$this->beginWidget('CActiveForm', array(
	'id'=>'" . $this->class2id($this->modelClass) . "-form',
	'enableAjaxValidation'=>false,
        'htmlOptions'=>array('class'=>'form-horizontal')
)); ?>\n"; ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo "<?php echo \$form->errorSummary(\$model); ?>\n"; ?>

    <?php
    foreach ($this->tableSchema->columns as $column) {
        if ($column->autoIncrement)
            continue;
        ?>
        <div class="form-group">
            <?php echo "<?php echo \$form->labelEx(\$model,'{$column->name}', array('class'=>'col-sm-2 control-label')); ?>\n"; ?>
            <div class="col-md-10">
                <?php echo "<?php echo \$form->textField(\$model, '{$column->name}', array('class' => 'form-control')); ?>\n"; ?>
                <?php echo "<?php echo \$form->error(\$model,'{$column->name}', array('class'=>'alert alert-danger')); ?>\n"; ?>
            </div>
        </div>

        <?php
    }
    ?>
</div>

<div class="box-footer">
    <?php echo "<?php echo CHtml::link('Cancel', Yii::app()->controller->createUrl('index'), array('class' => 'btn btn-sm btn-default')); ?> \n"; ?>
    <?php echo "<?php echo CHtml::submitButton(\$model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-sm btn-info pull-right')); ?> \n"; ?>
</div>

<?php echo "<?php \$this->endWidget(); ?>\n"; ?>
