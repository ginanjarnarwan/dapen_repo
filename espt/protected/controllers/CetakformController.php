<?php

class CetakformController extends Controller
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','sptlama','sptsprint','popsptsprint','Spttidakfinal','Sptfinal','Popspttidakfinal','Popsptfinal','Spt1770s','Spt1770slampiran1','spt1770slampiran2','Popspt1770s','downloadssp','downlog'),
				'users'=>array('@'),
				// 'roles'=>array('Admin')
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionSptlama($tahun,$nik,$bulan,$bulanakhir,$kode)
	{
		// var_dump($tahun,$nik,$bulan,$bulanakhir,$kode);exit();
		$query = "select * from rpt_spt1721_".$tahun." where n_nik=:nik and n_bulanawal=:bulan and c_kode_kpp=:kode and n_bulanakhir=:bulanakhir";
		$cmd = Yii::app()->db->createCommand($query);
		$cmd->bindParam(':nik',$nik);
		$cmd->bindParam(':bulan',$bulan);
		$cmd->bindParam(':kode',$kode);
		$cmd->bindParam(':bulanakhir',$bulanakhir);
		$data1 = $cmd->queryAll();

		$query2 = "select b.N_NIK_TTD, b.V_NAMA_TTD, b.V_JABATAN_TTD, b.V_KOTA_TTD, b.V_PATH_TTD 
					from rpt_spt1721_".$tahun." a,t_par_fin_".$tahun." b where n_nik=:nik
					and n_bulanawal=:bulan and c_kode_kpp=:kode ";
		$cmd2 = Yii::app()->db->createCommand($query2);
		$cmd2->bindParam(':nik',$nik);
		$cmd2->bindParam(':bulan',$bulan);
		$cmd2->bindParam(':kode',$kode);
		// $cmd2->bindParam(':bulanakhir',$bulanakhir);
		$data2 = $cmd2->queryAll();
		$res1 = array();
		$res2 = array();
		foreach ($data1[0] as $key => $value) {
			$res1[] = $value;
		}
		foreach ($data2[0] as $key => $value) {
			$res2[] = $value;
		}
		$data1 = array(0=>$res1);
		$data2 = array(0=>$res2);
		$this->renderPartial('sptlama',array(
			'data1'=>$data1,
			'data2'=>$data2
		));
	}

	public function actionPopsptsprint($tahun,$nik,$bulan,$bulanakhir,$kode,$namapemotong)
	{		
		MyApp::setLog('view');
		$this->renderPartial('popsptsprint',array(
			'tahun'=>$tahun,
			'nik'=>$nik,
			'bulan'=>$bulan,
			'bulanakhir'=>$bulanakhir,
			'kode'=>$kode,
			'namapemotong'=>$namapemotong
		));
	}

	public function actionSptsprint($tahun,$nik,$bulan,$bulanakhir,$kode,$namapemotong)
	{
		// $this->layout='print';
		// var_dump($tahun,$nik,$bulan,$bulanakhir,$kode);exit();
		
		MyApp::setLog('print');
		$query = "select * from rpt_spt1721_".$tahun." where N_NIK=:nik and N_BULANAWAL=:bulan and C_KODE_KPP=:kode and N_BULANAKHIR=:bulanakhir and V_NAMA_PEMOTONG = :namapemotong";
		$cmd = Yii::app()->db->createCommand($query);
		$cmd->bindParam(':nik',$nik);
		$cmd->bindParam(':bulan',$bulan);
		$cmd->bindParam(':kode',$kode);
		$cmd->bindParam(':bulanakhir',$bulanakhir);
		$cmd->bindParam(':namapemotong',$namapemotong);
		$data1 = $cmd->queryAll();

		$query2 = "select b.N_NIK_TTD, b.V_NAMA_TTD, b.V_JABATAN_TTD, b.V_KOTA_TTD, b.V_PATH_TTD, b.V_NAMA_PEMOTONG, b.NPWP_PEMOTONG
					from rpt_spt1721_".$tahun." a,t_par_fin_".$tahun." b where n_nik=:nik
					and n_bulanawal=:bulan and c_kode_kpp=:kode and b.V_NAMA_PEMOTONG = :namapem and a.V_NAMA_PEMOTONG = :namapem and a.C_PAYAREA = b.C_PAYAREA";
		$cmd2 = Yii::app()->db->createCommand($query2);
		$cmd2->bindParam(':nik',$nik);
		$cmd2->bindParam(':bulan',$bulan);
		$cmd2->bindParam(':kode',$kode);
		$cmd2->bindParam(':namapem',$namapemotong);
		// $cmd2->bindParam(':bulanakhir',$bulanakhir);
		$data2 = $cmd2->queryAll();
		$res1 = array();
		$res2 = array();
		foreach ($data1[0] as $key => $value) {
			$res1[] = $value;
		}
		foreach ($data2[0] as $key => $value) {
			$res2[] = $value;
		}
		$data1 = array(0=>$res1);
		$data2 = array(0=>$res2);
		$this->renderPartial('sptsprint',array(
			'data1'=>$data1,
			'data2'=>$data2
		));
	}

	public function actionPopspttidakfinal($tahun,$nobukti,$npwp_pemotong)
	{
		MyApp::setLog('view');
		$this->renderPartial('popspttidakfinal',array(
			'tahun'=>$tahun,
			'nobukti'=>$nobukti,
			'npwp_pemotong'=>$npwp_pemotong,
		));
	}

	public function actionPopsptfinal($tahun,$nobukti,$npwp_pemotong)
	{		
		MyApp::setLog('view');
		$this->renderPartial('popsptfinal',array(
			'tahun'=>$tahun,
			'nobukti'=>$nobukti,
			'npwp_pemotong'=>$npwp_pemotong,
		));
	}

	public function actionPopspt1770s($tahun,$nik)
	{
		MyApp::setLog('view');
		$this->renderPartial('popspt1770s',array(
			'tahun'=>$tahun,
			'nik'=>$nik,
		));
	}

	public function actionSpttidakfinal($tahun,$nobukti,$npwp_pemotong)
	{
		MyApp::setLog('view');
		$sql = "SELECT * FROM bukti_potong_nonfinal_".$tahun." where nomor_bukti_potong=:no AND npwp_pemotong='".$npwp_pemotong."'";
		$tdkfinal = Yii::app()->db->createCommand($sql)
						->bindParam(':no',$nobukti)
						->queryAll();
		$sql2 = "SELECT * FROM t_par_fin_".$tahun." where V_NAMA_PEMOTONG = :pem and C_PAYAREA = 'TF' limit 1";
		$paraf = Yii::app()->db->createCommand($sql2)
						->bindParam(':pem',$tdkfinal[0]['nama_pemotong'])
						->queryRow();
		$dist = Yii::app()->db->createCommand("SELECT DISTINCT(kategori) from kode_objek_pajak_nonfinal")
						->queryAll();
		foreach ($dist as $key => $value) {
			$sql3 = "SELECT * FROM kode_objek_pajak_nonfinal where kategori = :kat";
			$kd = Yii::app()->db->createCommand($sql3)
					->bindParam(':kat',$value['kategori'])
					->queryAll();
			$dist[$key]['value'] = $kd;
		}

		$kodeobjek = $dist;
		$this->renderPartial('spttidakfinal',array(
			'tdkfinal'=>$tdkfinal,
			'paraf'=>$paraf,
			'kodeobjek'=>$kodeobjek
		));
	}

	public function actionSptfinal($tahun,$nobukti,$npwp_pemotong)
	{
		MyApp::setLog('view');
		$sql = "SELECT * FROM bukti_potong_final_".$tahun." where nomor_bukti_potong=:no AND npwp_pemotong='".$npwp_pemotong."'";
		$final = Yii::app()->db->createCommand($sql)
						->bindParam(':no',$nobukti)
						->queryAll();
		$sql2 = "SELECT * FROM t_par_fin_".$tahun." WHERE V_NAMA_PEMOTONG = :pem and C_PAYAREA = 'FN' limit 1";
		$paraf = Yii::app()->db->createCommand($sql2)
						->bindParam(':pem',$final[0]['nama_pemotong'])
						->queryRow();
		$dist = Yii::app()->db->createCommand("SELECT * from kode_objek_pajak_final")
						->queryAll();

		$kodeobjek = $dist;
		$this->renderPartial('sptfinal',array(
			'final'=>$final,
			'paraf'=>$paraf,
			'kodeobjek'=>$kodeobjek
		));
	}

	public function actionSpt1770s($tahun,$nik)
	{
		MyApp::setLog('view');
		$sql = "SELECT * FROM rpt_spt1770s_".$tahun." where n_nik = :nik";
		$data = Yii::app()->db->createCommand($sql)
						->bindParam(':nik',$nik)
						->queryRow();

		$sql2 = "SELECT * FROM t_par_fin_".$tahun." where V_NAMA_PEMOTONG = 'PT. Telekomunikasi Indonesia,Tbk' limit 1";
		$paraf = Yii::app()->db->createCommand($sql2)
						->queryRow();

		$tabel_cek = array();
        $tahun_cek = array();
        $tahun_current = date("Y");
        $tahun_min = date("Y") - 5;
        for ($i=$tahun_current; $i >= $tahun_min; $i--) { 
            $namatbl = 'rpt_spt1721_'.$i;
            $cektab = Yii::app()->db->createCommand("SHOW TABLES LIKE :par")
                                        ->bindParam(':par',$namatbl)
                                        ->queryAll();
            if (count($cektab) > 0 ){
                $tabel_cek[] = $namatbl;
                $tahun_cek[] = $i;
            }
        }
        $thn_max = max($tahun_cek);
		$sql3 = "select * from rpt_spt1721_".$thn_max." where n_nik=:nik and V_NAMA_PEMOTONG = 'PT. Telekomunikasi Indonesia,Tbk' ";
		$data1721 = Yii::app()->db->createCommand($sql3)
					->bindParam(':nik',$nik)
					->queryRow();

		$sql2 = "SELECT * FROM bukti_potong_final_".$tahun." where nik=:nik";
		$final = Yii::app()->db->createCommand($sql2)
						->bindParam(':nik',$nik)
						->queryAll();
		$brutolamp2 = 0;
		$pphlamp2 = 0;
		foreach ($final as $key => $value) {
			$brutolamp2 = $brutolamp2+$value['jumlah_bruto'];
			$pphlamp2 = $pphlamp2+$value['jumlah_pph'];
		}

		$sql3 = "SELECT * FROM bukti_potong_nonfinal_".$tahun." where nik=:nik";
		$tdkfinal = Yii::app()->db->createCommand($sql3)
						->bindParam(':nik',$nik)
						->queryAll();

		$this->renderPartial('spt1770s',array(
			'data'=>$data,
			'paraf'=>$paraf,
			'data1721'=>$data1721,
			'final'=>$final,
			'tdkfinal'=>$tdkfinal,
			'brutolamp2'=>$brutolamp2,
			'pphlamp2'=>$pphlamp2,
			'tahun'=>$tahun,
		));
	}

	public function actionSpt1770slampiran1($tahun,$nik)
	{
		MyApp::setLog('view');
		$sql = "SELECT * FROM rpt_spt1770s_".$tahun." where n_nik=:nik and n_tahun_pajak = :thn";
		$final = Yii::app()->db->createCommand($sql)
						->bindParam(':nik',$nik)
						->bindParam(':thn',$thn)
						->queryAll();
		$sql2 = "SELECT * FROM t_par_fin_".$tahun." limit 1";
		$paraf = Yii::app()->db->createCommand($sql2)
						->queryRow();

		$this->renderPartial('spt1770slampiran1',array(
			'final'=>$final,
			'paraf'=>$paraf,
			'kodeobjek'=>array(),
		));
	}

	public function actionSpt1770slampiran2($tahun,$nik)
	{
		MyApp::setLog('view');
		$sql = "SELECT * FROM rpt_spt1770s_".$tahun." where n_nik=:nik and n_tahun_pajak = :thn";
		$final = Yii::app()->db->createCommand($sql)
						->bindParam(':nik',$nik)
						->bindParam(':thn',$thn)
						->queryAll();
		$sql2 = "SELECT * FROM t_par_fin_".$tahun." limit 1";
		$paraf = Yii::app()->db->createCommand($sql2)
						->queryRow();

		$this->renderPartial('spt1770slampiran2',array(
			'final'=>$final,
			'paraf'=>$paraf,
			'kodeobjek'=>array(),
		));
	}

	public function actionDownloadssp($nik,$npwp,$tahun,$id){
		MyApp::setLog('download');
		$sql = "SELECT * FROM ssp_".$tahun." where n_nik=:nik and n_tahun_pajak = :thn and n_npwp = :npwp and id = :id";

		$ssp = Yii::app()->db->createCommand($sql)
						->bindParam(':nik',$nik)
						->bindParam(':thn',$tahun)
						->bindParam(':npwp',$npwp)
						->bindParam(':id',$id)
						->queryRow();
		$filePath = Yii::getPathOfAlias('webroot') . "/document/ssp/".$ssp['filename'];
		$filename = 'ssp_'.$nik.'_'.$tahun.'.'.pathinfo($filePath, PATHINFO_EXTENSION);
		if (file_exists($filePath))
			return Yii::app()->getRequest()->sendFile($filename,file_get_contents($filePath));
		else
            throw new CHttpException(404, 'Document does not exist.');
	}

	public function actionDownlog(){
		header( 'Content-type: text/html; charset=utf-8' );
	    echo '<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>';
	    echo str_pad('',4096); //fill browser buffer

		$model = new DownloadLog;
		$model->telkomnik 	= $_POST['telkomnik'];
		$model->sarana 		= $_POST['sarana'];
		$model->jenis_form 	= $_POST['jenis_form'];
		$model->nomor_bukti = $_POST['nomor_bukti'];
		$model->keterangan 	= '';
		$model->update_by 	= Yii::app()->user->username;
		$model->keterangan 	= '';
		$model->datetime 	= date('Y-m-d H:i:s');
		if($model->save()){
			$return_data = array(
			    'status' => true
			);
		}else{
			$return_data = array(
			    'status' => false
			);
		}
		ob_flush(); flush();
        usleep(1);
        echo '<script>window.close();</script>';
	}
}