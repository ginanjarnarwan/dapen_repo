<?php

class UserController extends Controller
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','create','update','delete','Popshowman','showman'),
				'roles'=>array('Admin'),
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionPopshowman()
	{
		$temp = Yii::app()->request->getParam('temp');
		$this->renderPartial('popshowman',array(
			'temp'=>$temp,
		));
	}

	public function actionShowman()
	{
		$this->layout = 'iframe';
		$tabel_cek = array();
        $tahun_cek = array();
        $tahun_current = date("Y");
        $tahun_min = date("Y") - 5;
        for ($i=$tahun_current; $i >= $tahun_min; $i--) { 
            $namatbl = 'rpt_spt1721_'.$i;
            $cektab = Yii::app()->db->createCommand("SHOW TABLES LIKE :par")
                                        ->bindParam(':par',$namatbl)
                                        ->queryAll();
            if (count($cektab) > 0 ){
                $tabel_cek[] = $namatbl;
                $tahun_cek[] = $i;
            }
        }

        $sql = "SELECT * FROM rpt_spt1721_".max($tahun_cek)." WHERE N_NIK IS NOT NULL";
		$temp = Yii::app()->request->getParam('temp');
		$par = array();
		$nik_search = null;
		$nama_search = null;
		if (isset($_REQUEST['Karyawan'])) {
			$nik_search = $_REQUEST['Karyawan']['nik'];
			$nama_search = $_REQUEST['Karyawan']['nama'];
			if(!empty($_REQUEST['Karyawan']['nik'])){
				$sql .= " AND N_NIK = :nik";
				$par[':nik'] = $_REQUEST['Karyawan']['nik'];
			}
			if(!empty($_REQUEST['Karyawan']['nama'])){
				$sql .= " AND V_NAMA_KARYAWAN LIKE :nama";
				$par[':nama'] = "%".$_REQUEST['Karyawan']['nama']."%";
			}
		}
		$query = Yii::app()->db->createCommand($sql);
        $query->params = $par;
        $count_sql = Yii::app()->db->createCommand("SELECT COUNT(tr.N_NIK) FROM (" . $sql . ") tr");
        $count_sql->params = $par;
        $count = $count_sql->queryScalar();
        // $count = 1;
        $data =  new CSqlDataProvider($query, array(
            'keyField' => 'N_NIK',
            'totalItemCount' => $count,
            'pagination' => array(
                'pageSize' => 12,
            ),
        ));

		$this->render('showman',array('data'=>$data,'temp'=>$temp,'nik_search'=>$nik_search,'nama_search'=>$nama_search));
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new User;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			$model->password = md5($_POST['User']['password']);
			if($model->save())
				$this->redirect(array('index'));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);
		$tabel_cek = array();
        $tahun_cek = array();
        $tahun_current = date("Y");
        $tahun_min = date("Y") - 5;
        for ($i=$tahun_current; $i >= $tahun_min; $i--) { 
            $namatbl = 'rpt_spt1721_'.$i;
            $cektab = Yii::app()->db->createCommand("SHOW TABLES LIKE :par")
                                        ->bindParam(':par',$namatbl)
                                        ->queryAll();
            if (count($cektab) > 0 ){
                $tabel_cek[] = $namatbl;
                $tahun_cek[] = $i;
            }
        }
        $sqln = "SELECT * FROM rpt_spt1721_".max($tahun_cek)." WHERE N_NIK = :nik";
        $cmdn = Yii::app()->db->createCommand($sqln);
        $cmdn->bindParam(':nik',$model->nik);
        $namkar = $cmdn->queryRow();
        $namkar = $namkar['V_NAMA_KARYAWAN'];
        // var_dump($namkar);exit();

		if(isset($_POST['User']))
		{
			$model->attributes=$_POST['User'];
			$model->password = md5($_POST['User']['password']);
			if($model->save())
				$this->redirect(array('index'));
		}

		$this->render('update',array(
			'model'=>$model,
			'namkar'=>$namkar,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		$model=new User('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['User']))
			$model->attributes=$_GET['User'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return User the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=User::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param User $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='user-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
