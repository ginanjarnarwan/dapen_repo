<?php

class AdminController extends Controller
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','dashboardAdmin','searchbynik','resultfindbynik','Uploadtidakfinal','Uploadfinal','upload1770s','upload1721'),
				'roles'=>array('Admin')
			),
            array('allow',  // allow all users to perform 'index' and 'view' actions
                'actions'=>array('searchbynik','resultfindbynik','dashboardAdmin'),
                'roles'=>array('Admin_witel')
            ),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('upload1721','Uploadfinal','searchbynik','resultfindbynik','dashboardAdmin'),
				'roles'=>array('Admin_dapen')
			),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('Uploadfinal','Uploadtidakfinal'),
				'roles'=>array('Finance')
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	public function actionIndex()
	{
		$this->render('index');
	}

    public function actionDashboardAdmin()
    {

		$total_user = Userapp::model()->findAllByAttributes(array());

		$query_user_admin = "select * from userapp as a,authassignment as b where a.telkomnik = b.userid";
		$total_user_admin = Yii::app()->db->createCommand($query_user_admin)->queryAll();

		$query_user_register = "select * from userapp where status='true'";
		$total_user_register = Yii::app()->db->createCommand($query_user_register)->queryAll();

		$query_user_admin_register = "select * from userapp as a,authassignment as b where a.telkomnik = b.userid and a.status='true'";
		$total_user_admin_register = Yii::app()->db->createCommand($query_user_admin_register)->queryAll();
		
		$query_download = "select * from download_log  group by update_by,nomor_bukti,jenis_form;";
		$query_gagal_download = "select * from download_log where telkomnik is null group by update_by,nomor_bukti,jenis_form;";
		$query_download_admin = "select * from download_log  where sarana != 'Pribadi' group by update_by,nomor_bukti,jenis_form;";
		$query_download_user = "select * from download_log where sarana = 'Pribadi' group by update_by,nomor_bukti,jenis_form;";
		
		$total_download = Yii::app()->db->createCommand($query_download)->queryAll();
		$total_gagal_download = Yii::app()->db->createCommand($query_gagal_download)->queryAll();
		$total_download_admin = Yii::app()->db->createCommand($query_download_admin)->queryAll();
		$total_download_user = Yii::app()->db->createCommand($query_download_user)->queryAll();
        

        $this->render('dashboardAdmin',array(
			'total_download' => $total_download,
			'total_gagal_download' => $total_gagal_download,
			'total_download_admin' => $total_download_admin,
			'total_download_user' => $total_download_user,    
			
			'total_user' => $total_user,
			'total_user_admin' => $total_user_admin,

			'total_user_register' => $total_user_register,
			'total_user_admin_register' => $total_user_admin_register,
		
		), false, true);
    }

    public function actionSearchbynik()
    {
        $this->render('searchbynik');
    }

	public function actionResultfindbynik()
	{
		$nik = Yii::app()->request->getParam('nik');
		MyApp::setLog('search');

        $tabel_cek = array();
        $tahun_cek = array();
        $tahun_current = date("Y");
        $tahun_min = date("Y") - 5;
        for ($i=$tahun_current; $i >= $tahun_min; $i--) { 
            $namatbl = 'rpt_spt1721_'.$i;
            $cektab = Yii::app()->db->createCommand("SHOW TABLES LIKE :par")
                                        ->bindParam(':par',$namatbl)
                                        ->queryAll();
            if (count($cektab) > 0 ){
                $tabel_cek[] = $namatbl;
                $tahun_cek[] = $i;
            }

            $namatbl2 = 'bukti_potong_nonfinal_'.$i;
            $cektab2 = Yii::app()->db->createCommand("SHOW TABLES LIKE :par")
                                        ->bindParam(':par',$namatbl2)
                                        ->queryAll();
            if (count($cektab2) > 0 ){
                $tabel_cek2[] = $namatbl2;
                $tahun_cek2[] = $i;
            }

            $namatbl3 = 'bukti_potong_final_'.$i;
            $cektab3 = Yii::app()->db->createCommand("SHOW TABLES LIKE :par")
                                        ->bindParam(':par',$namatbl3)
                                        ->queryAll();
            if (count($cektab3) > 0 ){
                $tabel_cek3[] = $namatbl3;
                $tahun_cek3[] = $i;
            }

            $namatbl4 = 'rpt_spt1770s_'.$i;
            $cektab4 = Yii::app()->db->createCommand("SHOW TABLES LIKE :par")
                                        ->bindParam(':par',$namatbl4)
                                        ->queryAll();
            if (count($cektab4) > 0 ){
                $tabel_cek4[] = $namatbl4;
                $tahun_cek4[] = $i;
            }

            $namatbl5 = 'ssp_'.$i;
            $cektab5 = Yii::app()->db->createCommand("SHOW TABLES LIKE :par")
                                        ->bindParam(':par',$namatbl5)
                                        ->queryAll();
            if (count($cektab5) > 0 ){
                $tabel_cek5[] = $namatbl5;
                $tahun_cek5[] = $i;
            }
        }

        //start 1.query spt1721-a1
        $data=null;
        if(count($tabel_cek)>0){
            $query = 'select a.* from (';
            $a = 1;
            $jml = count($tabel_cek);
            foreach ($tabel_cek as $key => $value) {
                $query .= " SELECT * FROM ".$value." where N_NIK=:nik ";
                if ($jml-$a == 0) {
                    $query .= " ) a ORDER BY a.N_TAHUN ";
                }else{
                    $query .= " UNION ";
                }
                $a++;
            }
            $sql = Yii::app()->db->createCommand($query);
            $sql->bindParam(':nik',$nik);
            $data = $sql->queryAll();
        }
        //end

        //start 2. query bukti potong non final
        $datanonfinal=null;
        if(count($tabel_cek2)){
            $query2 = 'select a.* from (';
            $a = 1;
            $jml2 = count($tabel_cek2);
            foreach ($tabel_cek2 as $key => $value) {
                $query2 .= " SELECT * FROM ".$value." where nik=:nik GROUP BY nomor_bukti_potong";
                if ($jml2-$a == 0) {
                    $query2 .= " ) a ORDER BY a.tahun_pajak ";
                }else{
                    $query2 .= " UNION ";
                }
                $a++;
            }
            $sql2 = Yii::app()->db->createCommand($query2);
            $sql2->bindParam(':nik',$nik);
            $datanonfinal = $sql2->queryAll();
        }
        //end

        //start 3. query bukti potong final
        $datafinal = null;
        if(count($tabel_cek3)){
            $query3 = 'select a.* from (';
            $a = 1;
            $jml3 = count($tabel_cek3);
            foreach ($tabel_cek3 as $key => $value) {
                $query3 .= " SELECT * FROM ".$value." where nik=:nik GROUP BY nomor_bukti_potong";
                if ($jml3-$a == 0) {
                    $query3 .= " ) a ORDER BY a.tahun_pajak ";
                }else{
                    $query3 .= " UNION ";
                }
                $a++;
            }
            $sql3 = Yii::app()->db->createCommand($query3);
            $sql3->bindParam(':nik',$nik);
            $datafinal = $sql3->queryAll();
        }
        //end

        //start 4. query 1770s
        $data1770s = null;
        if(count($tabel_cek4)){
            $query4 = 'select a.* from (';
            $a = 1;
            $jml4 = count($tabel_cek4);
            foreach ($tabel_cek4 as $key => $value) {
                $query4 .= " SELECT * FROM ".$value." where n_nik=:nik ";
                if ($jml4-$a == 0) {
                    $query4 .= " ) a ORDER BY a.n_tahun_pajak ";
                }else{
                    $query4 .= " UNION ";
                }
                $a++;
            }
            $sql4 = Yii::app()->db->createCommand($query4);
            $sql4->bindParam(':nik',$nik);
            $data1770s = $sql4->queryAll();
        }
        //end

         //start 5. query ssp
        $datassp = null;
        if(count($tabel_cek5)){
            $query5 = 'select a.* from (';
            $a = 1;
            $jml5 = count($tabel_cek5);
            foreach ($tabel_cek5 as $key => $value) {
                $query5 .= " SELECT * FROM ".$value." where n_nik=:nik ";
                if ($jml5-$a == 0) {
                    $query5 .= " ) a ORDER BY a.n_tahun_pajak ";
                }else{
                    $query5 .= " UNION ";
                }
                $a++;
            }
            $sql5 = Yii::app()->db->createCommand($query5);
            $sql5->bindParam(':nik',$nik);
            $datassp = $sql5->queryAll();
        }
        //end

        $thnbs = max($tahun_cek);
        $query="SELECT * FROM rpt_spt1721_".$thnbs." where n_nik=:nik";
        $res2 = Yii::app()->db->createCommand($query)
                ->bindParam(':nik',$nik)
                ->queryAll();
        $query2 ="SELECT EFIN FROM efin_npwp where NIK=:nik";
        $res3 = Yii::app()->db->createCommand($query2)
                ->bindParam(':nik',$nik)
                ->queryAll();
        $efin = Yii::app()->db->createCommand($query2)
                    ->bindParam(':nik',$nik)
                    ->queryScalar();
        if(!empty($res2)){
            foreach ($res2[0] as $key => $value) {
                $data2[] = $value;
            }
        }
        if (!empty($res3)) {
            foreach ($res3[0] as $key => $value) {
                $data3[] = $value;
            }
        }
        $res2 = isset($data2)?array(0=>$data2) : null;
        $res3 = isset($data3)?array(0=>$data3) : null;

        $bukti_potong=  " 1 -".$res2[0][6]." . ".$res2[0][3]." - ".$res2[0][0];
        $query3 ="SELECT * FROM t_par_fin_".$thnbs."";
        $res4 = Yii::app()->db->createCommand($query3)
                ->queryAll();
        foreach ($res4[0] as $key => $value) {
            $data4[] = $value;
        }
        $res4 = array(0=>$data4);

        $stat_k = "";
        $stat_tk = "";
        $stat = $res2[0][24];
        if (strstr($stat, "TK"))
        {
            $stat_tk = str_replace("TK/", "TK/", $stat);
        }
        else
        {
            $stat_k = str_replace("K/", "K/", $stat);
        }

        // $queryresume = "SELECT 
        //                 t.n_nik,
        //                 t.v_nama,
        //                 (select s.V_NPWP_PRIBADI from rpt_spt1721_".$thnbs." s where s.N_NIK = t.n_nik limit 1) AS NPWP_PEGAWAI,
        //                 CASE 
        //                     WHEN t.n_jumlah_pph_terutang-t.n_pph_dipotong > 0 THEN 'Kurang Bayar'
        //                     WHEN t.n_jumlah_pph_terutang-t.n_pph_dipotong < 0 THEN 'Lebih Bayar'
        //                     WHEN t.n_jumlah_pph_terutang-t.n_pph_dipotong = 0 THEN 'Nihil'
        //                 END AS status_spt,
        //                 t.n_jumlah_pph_terutang-t.n_pph_dipotong as selisih,
        //                 (select s.V_NPWP_KANTOR from rpt_spt1721_".$thnbs." s where s.N_NIK = t.n_nik and s.V_NAMA_PEMOTONG = 'Dapen') AS NPWP_DAPEN,
        //                 (select s.V_NPWP_KANTOR from rpt_spt1721_".$thnbs." s where s.N_NIK = t.n_nik and s.V_NAMA_PEMOTONG = 'PT. Telekomunikasi Indonesia,Tbk') AS NPWP_Telkom
        //                 FROM rpt_spt1770s_".$thnbs." t WHERE t.n_nik = :nik";

        $queryresume = "select s.* from rpt_spt1721_".$thnbs." s where s.N_NIK = :nik limit 1";
        
        $resume = Yii::app()->db->createCommand($queryresume)
                    ->bindParam(':nik',$nik)
                    ->queryRow();

        $this->renderPartial('resultfindbynik',array(
        	'nik'=>$nik,
            'data'=>$data,
            'stat_k'=>$stat_k,
            'stat_tk'=>$stat_tk,
            'res2'=>$res2,
            'res3'=>$res3,
            'res4'=>$res4,
            'bukti_potong'=>$bukti_potong,
            'data1770s'=>$data1770s,
            'datafinal'=>$datafinal,
            'datanonfinal'=>$datanonfinal,
            'datassp'=>$datassp,
            'efin'=>$efin,
            'resume'=>$resume
        ), false, true);
	}

	public function actionUploadfinal(){
		$model = new Uploadfinal;
		$model->unsetAttributes();
		$tahuncurrent = date("Y");
		$min = $tahuncurrent - 2;
		$max = $tahuncurrent + 6;
		$arraytahun = array();
		for ($i=$min; $i < $max; $i++) { 
			$arraytahun[$i] = $i;
		}
		if (isset($_POST['Uploadfinal'])) {
			$role = Yii::app()->user->roles;
			if ($role != 'Admin' && $role != 'Finance') {
				$assrole = Yii::app()->db->createCommand("SELECT pemotong from assignment_pemotong WHERE itemname = :itm limit 1")
						->bindParam(':itm',$role)
						->queryScalar();
			}else{
				$assrole = null;
			}
			$tahun = $_POST['Uploadfinal']['tahun'];
			$model->tahun = $_POST['Uploadfinal']['tahun'];
			$model->fileupload = $_POST['Uploadfinal']['fileupload'];
			// if($model->validate()){
				$upload = CUploadedFile::getInstance($model, 'fileupload');
				if ($upload){
					$upload_dir = Yii::getPathOfAlias('upload_dir');
	                if (!is_dir($upload_dir)) {
	                    mkdir($upload_dir);
	                    chmod($upload_dir, 0777);
	                }

	                $_peserta = $upload_dir . '/fileupload';
	                $nama_file = str_replace(".", "", microtime(true)) . "-" . $upload->name;
	                if(!is_dir($_peserta)){
	                    mkdir($_peserta);
	                    chmod($_peserta, 0777);
	                    $upload->saveAs($_peserta . "/" . $nama_file);
	                }else $upload->saveAs($_peserta . "/" . $nama_file);
					$dirfile = $_peserta . "/" . $nama_file;

					$transaction = Yii::app()->db->beginTransaction();
					try{
						Yii::import('application.vendor.PHPExcel.PHPExcel.IOFactory', true);
		                $inputFileName = $upload_dir . "/fileupload/" . $nama_file;
		                $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
		                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
		                $objPHPExcel = $objReader->load($inputFileName);
		                $return = $objPHPExcel->getActiveSheet()->toArray(null, true,true,true);
		                $kolom = $return[1]['A'].$return[1]['B'].$return[1]['C'].$return[1]['D'].$return[1]['E'].$return[1]['F'].$return[1]['G'].$return[1]['H'].$return[1]['I'].$return[1]['J'].$return[1]['K'].$return[1]['L'].$return[1]['M'].$return[1]['N'].$return[1]['O'];
		                $kolom = strtolower(preg_replace('/\s+/', '', $kolom));
		                $par_kol = "masapajaktahunpajakpembetulannomorbuktipotongnpwpniknamaalamatkodepajakjumlahbrutotarifjumlahpphnpwppemotongnamapemotongtanggalbuktipotong";
		                if($kolom==$par_kol){
		                	$namatbl1 = 'bukti_potong_final_'.$tahun;
							$cektab1 = Yii::app()->db->createCommand("SHOW TABLES LIKE :par")
														->bindParam(':par',$namatbl1)
														->queryAll();
							if (count($cektab1) < 1) {
								$querybckp1 = "CREATE TABLE ".$namatbl1." (
												`id`  int(11) NOT NULL AUTO_INCREMENT ,
												`masa_pajak`  int(11) NULL DEFAULT NULL ,
												`tahun_pajak`  int(11) NULL DEFAULT NULL ,
												`pembetulan`  int(11) NULL DEFAULT NULL ,
												`nomor_bukti_potong`  varchar(30) NULL DEFAULT NULL ,
												`npwp`  varchar(30) NULL DEFAULT NULL ,
												`nik`  varchar(8) NULL DEFAULT NULL ,
												`nama`  varchar(80) NULL DEFAULT NULL ,
												`alamat`  varchar(80) NULL DEFAULT NULL ,
												`kode_pajak`  varchar(20) NULL DEFAULT NULL ,
												`jumlah_bruto`  int(11) NULL DEFAULT NULL ,
												`tarif`  int(11) NULL DEFAULT NULL ,
												`jumlah_pph`  int(11) NULL DEFAULT NULL ,
												`npwp_pemotong`  varchar(30) NULL DEFAULT NULL ,
												`nama_pemotong`  varchar(80) NULL DEFAULT NULL ,
												`tanggal_bukti_potong`  date NULL DEFAULT NULL ,
												PRIMARY KEY (`id`)
												)";

								$sqlbckp1 = Yii::app()->db->createCommand($querybckp1);
								$sqlbckp1->query();
							}
							$pesan = array();
							$jmlinsert = 0;
							$jmlupdate = 0;
			                foreach ($return as $key => $value){
			                    if ($key > 1) {
			                        if(!empty($value['D'])){
			                        	if ($value['B'] != $tahun) {
			                        		$pesan[] = 'NIK = ' . $value['F'] . " , No. Bukti Potong = "  . $value['D'] . ", <b style='color:red;'>Data tahun tidak sesuai</b>.";
			                        	}else{
				                            $cek = Yii::app()->db->createCommand("SELECT COUNT(*) FROM ".$namatbl1." WHERE nomor_bukti_potong = :nmr AND kode_pajak = :kd")->bindParam(':nmr',$value['D'])->bindParam(':kd',$value['I'])->queryScalar();
				                            if ($cek < 1) {
				                            	$sqlinsert = "INSERT INTO ".$namatbl1." (masa_pajak,tahun_pajak,pembetulan,nomor_bukti_potong,npwp,nik,nama,alamat,kode_pajak,jumlah_bruto,tarif,jumlah_pph,npwp_pemotong,nama_pemotong,tanggal_bukti_potong) VALUES (:masa_pajak,:tahun_pajak,:pembetulan,:nomor_bukti_potong,:npwp,:nik,:nama,:alamat,:kode_pajak,:jumlah_bruto,:tarif,:jumlah_pph,:npwp_pemotong,:nama_pemotong,:tanggal_bukti_potong)";
				                            	$cmdinsert = Yii::app()->db->createCommand($sqlinsert);
				                            	$cmdinsert->params = array(
				                            		':masa_pajak'=>$value['A'],
				                            		':tahun_pajak'=>$value['B'],
				                            		':pembetulan'=>$value['C'],
				                            		':nomor_bukti_potong'=>$value['D'],
				                            		':npwp'=>$value['E'],
				                            		':nik'=>$value['F'],
				                            		':nama'=>$value['G'],
				                            		':alamat'=>$value['H'],
				                            		':kode_pajak'=>$value['I'],
				                            		':jumlah_bruto'=>$value['J'],
				                            		':tarif'=>$value['K'],
				                            		':jumlah_pph'=>$value['L'],
				                            		':npwp_pemotong'=>$value['M'],
				                            		':nama_pemotong'=>is_null($assrole) ? $value['N'] : $assrole,
				                            		':tanggal_bukti_potong'=>date("Y-m-d",strtotime(str_replace("/", "-", $value['O']))),
				                            	);
				                            	$cmdinsert->query();
				                            	$jmlinsert++;
				                            }else{
				                            	$sqlupdate = "UPDATE ".$namatbl1." set masa_pajak= :masa_pajak,tahun_pajak=:tahun_pajak,pembetulan=:pembetulan,nomor_bukti_potong=:nomor_bukti_potong,npwp=:npwp,nik=:nik,nama=:nama,alamat=:alamat,kode_pajak=:kode_pajak,jumlah_bruto=:jumlah_bruto,tarif=:tarif,jumlah_pph=:jumlah_pph,npwp_pemotong=:npwp_pemotong,nama_pemotong=:nama_pemotong,tanggal_bukti_potong=:tanggal_bukti_potong where nomor_bukti_potong = :nomor_bukti_potong AND kode_pajak = :kode_pajak";
				                            	$cmdupdate = Yii::app()->db->createCommand($sqlupdate);
				                            	$cmdupdate->params = array(
				                            		':masa_pajak'=>$value['A'],
				                            		':tahun_pajak'=>$value['B'],
				                            		':pembetulan'=>$value['C'],
				                            		':nomor_bukti_potong'=>$value['D'],
				                            		':npwp'=>$value['E'],
				                            		':nik'=>$value['F'],
				                            		':nama'=>$value['G'],
				                            		':alamat'=>$value['H'],
				                            		':kode_pajak'=>$value['I'],
				                            		':jumlah_bruto'=>$value['J'],
				                            		':tarif'=>$value['K'],
				                            		':jumlah_pph'=>$value['L'],
				                            		':npwp_pemotong'=>$value['M'],
				                            		':nama_pemotong'=>is_null($assrole) ? $value['N'] : $assrole,
				                            		':tanggal_bukti_potong'=>date("Y-m-d",strtotime(str_replace("/", "-", $value['O']))),
				                            	);
				                            	$cmdupdate->query();
				                            	$jmlupdate++;
				                            }
			                        	}
			                        }
			                    }
			                }
			                if(count($pesan) > 0) {
				                $datapesan = implode("<br>", $pesan);
				            	Yii::app()->user->setFlash('tidaksave', $datapesan);
								
								MyApp::setLog('gagal');
				            }								
								MyApp::setLog('sukses');
				            	Yii::app()->user->setFlash('berhasil', "data berhasil disimpan, ".$jmlinsert." data diinsert, ".$jmlupdate." data diupdate.");
				            
				            // echo "<pre>";
				            // print_r($datapesan);exit();
				            // echo "<pre>";
		                }else{					
							MyApp::setLog('gagal');		
		                	Yii::app()->user->setFlash('tidaksave', "template format excel tidak sesuai");
		                }
					    unlink($dirfile);
						$transaction->commit();
						$this->redirect(array('uploadfinal'));
					}catch(Exception $e){
					    $transaction->rollBack();
						
						MyApp::setLog('gagal');
					    throw new CHttpException(null,"ERROR: ".$e->getMessage());
					}
				}
			// }
		}
		$this->render('uploadfinal',array(
			'model'=>$model,
			'arraytahun'=>$arraytahun,
		));
	}

	public function actionUploadtidakfinal(){
		$model = new Uploadtidakfinal;
		$model->unsetAttributes();
		$tahuncurrent = date("Y");
		$min = $tahuncurrent - 2;
		$max = $tahuncurrent + 6;
		$arraytahun = array();
		for ($i=$min; $i < $max; $i++) { 
			$arraytahun[$i] = $i;
		}
		if (isset($_POST['Uploadtidakfinal'])) {
			$tahun = $_POST['Uploadtidakfinal']['tahun'];
			$model->tahun = $_POST['Uploadtidakfinal']['tahun'];
			$model->fileupload = $_POST['Uploadtidakfinal']['fileupload'];
			// if($model->validate()){
				$upload = CUploadedFile::getInstance($model, 'fileupload');
				if ($upload){
					$upload_dir = Yii::getPathOfAlias('upload_dir');
	                if (!is_dir($upload_dir)) {
	                    mkdir($upload_dir);
	                    chmod($upload_dir, 0777);
	                }

	                $_peserta = $upload_dir . '/fileupload';
	                $nama_file = str_replace(".", "", microtime(true)) . "-" . $upload->name;
	                if(!is_dir($_peserta)){
	                    mkdir($_peserta);
	                    chmod($_peserta, 0777);
	                    $upload->saveAs($_peserta . "/" . $nama_file);
	                }else $upload->saveAs($_peserta . "/" . $nama_file);
					$dirfile = $_peserta . "/" . $nama_file;

					$transaction = Yii::app()->db->beginTransaction();
					try{
						Yii::import('application.vendor.PHPExcel.PHPExcel.IOFactory', true);
		                $inputFileName = $upload_dir . "/fileupload/" . $nama_file;
		                $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
		                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
		                $objPHPExcel = $objReader->load($inputFileName);
		                $return = $objPHPExcel->getActiveSheet()->toArray(null, true,true,true);
		                $kolom = $return[1]['A'].$return[1]['B'].$return[1]['C'].$return[1]['D'].$return[1]['E'].$return[1]['F'].$return[1]['G'].$return[1]['H'].$return[1]['I'].$return[1]['J'].$return[1]['K'].$return[1]['L'].$return[1]['M'].$return[1]['N'].$return[1]['O'].$return[1]['P'].$return[1]['Q'].$return[1]['R'].$return[1]['S'];
		                $kolom = strtolower(preg_replace('/\s+/', '', $kolom));
		                $par_kol = "masapajaktahunpajakpembetulannomorbuktipotongnpwpniknamaalamatwpluarnegerikodenegarakodepajakjumlahbrutojumlahdpptanpanpwptarifjumlahpphnpwppemotongnamapemotongtanggalbuktipotong";
		                if($kolom==$par_kol){
		                	$namatbl1 = 'bukti_potong_nonfinal_'.$tahun;
							$cektab1 = Yii::app()->db->createCommand("SHOW TABLES LIKE :par")
														->bindParam(':par',$namatbl1)
														->queryAll();
							if (count($cektab1) < 1) {
								$querybckp1 = "CREATE TABLE ".$namatbl1." (
												`id`  int(11) NOT NULL AUTO_INCREMENT ,
												`masa_pajak`  int(11) NULL DEFAULT NULL ,
												`tahun_pajak`  int(11) NULL DEFAULT NULL ,
												`pembetulan`  int(11) NULL DEFAULT NULL ,
												`nomor_bukti_potong`  varchar(30) NOT NULL ,
												`npwp`  varchar(30) NULL DEFAULT NULL ,
												`nik`  varchar(8) NULL DEFAULT NULL ,
												`nama`  varchar(80) NULL DEFAULT NULL ,
												`alamat`  varchar(80) NULL DEFAULT NULL ,
												`wp_luar_negeri`  varchar(5) NULL DEFAULT NULL ,
												`kode_negara`  varchar(20) NULL DEFAULT NULL ,
												`kode_pajak`  varchar(30) NULL DEFAULT NULL ,
												`jumlah_bruto`  int(11) NULL DEFAULT NULL ,
												`jumlah_dpp`  int(11) NULL DEFAULT NULL ,
												`tanpa_npwp`  varchar(5) NULL DEFAULT NULL ,
												`tarif`  int(11) NULL DEFAULT NULL ,
												`jumlah_pph`  int(11) NULL DEFAULT NULL ,
												`npwp_pemotong`  varchar(30) NULL DEFAULT NULL ,
												`nama_pemotong`  varchar(80) NULL DEFAULT NULL ,
												`tanggal_bukti_potong`  date NULL DEFAULT NULL ,
												PRIMARY KEY (`id`)
												)";

								$sqlbckp1 = Yii::app()->db->createCommand($querybckp1);
								$sqlbckp1->query();
							}
							$pesan = array();
							$jmlinsert = 0;
							$jmlupdate = 0;
			                foreach ($return as $key => $value){
			                    if ($key > 1) {
			                        if(!empty($value['D'])){
			                        	if ($value['B'] != $tahun) {
			                        		$pesan[] = 'NIK = ' . $value['F'] . " , No. Bukti Potong = "  . $value['D'] . ", <b style='color:red;'>Data tahun tidak sesuai</b>.";
			                        	}else{
				                            $cek = Yii::app()->db->createCommand("SELECT COUNT(*) FROM ".$namatbl1." WHERE nomor_bukti_potong = :nmr AND kode_pajak = :kd")->bindParam(':nmr',$value['D'])->bindParam(':kd',$value['K'])->queryScalar();
				                            if ($cek < 1) {
				                            	$sqlinsert = "INSERT INTO ".$namatbl1." (masa_pajak,tahun_pajak,pembetulan,nomor_bukti_potong,npwp,nik,nama,alamat,wp_luar_negeri,kode_negara,kode_pajak,jumlah_bruto,jumlah_dpp,tanpa_npwp,tarif,jumlah_pph,npwp_pemotong,nama_pemotong,tanggal_bukti_potong) VALUES (:masa_pajak,:tahun_pajak,:pembetulan,:nomor_bukti_potong,:npwp,:nik,:nama,:alamat,:wp_luar_negeri,:kode_negara,:kode_pajak,:jumlah_bruto,:jumlah_dpp,:tanpa_npwp,:tarif,:jumlah_pph,:npwp_pemotong,:nama_pemotong,:tanggal_bukti_potong)";
				                            	$cmdinsert = Yii::app()->db->createCommand($sqlinsert);
				                            	$cmdinsert->params = array(
				                            		':masa_pajak'=>$value['A'],
				                            		':tahun_pajak'=>$value['B'],
				                            		':pembetulan'=>$value['C'],
				                            		':nomor_bukti_potong'=>$value['D'],
				                            		':npwp'=>$value['E'],
				                            		':nik'=>$value['F'],
				                            		':nama'=>$value['G'],
				                            		':alamat'=>$value['H'],
				                            		':wp_luar_negeri'=>$value['I'],
				                            		':kode_negara'=>$value['J'],
				                            		':kode_pajak'=>$value['K'],
				                            		':jumlah_bruto'=>$value['L'],
				                            		':jumlah_dpp'=>$value['M'],
				                            		':tanpa_npwp'=>$value['N'],
				                            		':tarif'=>$value['O'],
				                            		':jumlah_pph'=>$value['P'],
				                            		':npwp_pemotong'=>$value['Q'],
				                            		':nama_pemotong'=>$value['R'],
				                            		':tanggal_bukti_potong'=>date("Y-m-d",strtotime(str_replace("/", "-", $value['S']))),
				                            	);
				                            	$cmdinsert->query();
				                            	$jmlinsert++;
				                            }else{
				                            	$sqlupdate = "UPDATE ".$namatbl1." set masa_pajak= :masa_pajak,tahun_pajak=:tahun_pajak,pembetulan=:pembetulan,nomor_bukti_potong=:nomor_bukti_potong,npwp=:npwp,nik=:nik,nama=:nama,alamat=:alamat,wp_luar_negeri=:wp_luar_negeri,kode_negara=:kode_negara,kode_pajak=:kode_pajak,jumlah_bruto=:jumlah_bruto,jumlah_dpp=:jumlah_dpp,tanpa_npwp=:tanpa_npwp,tarif=:tarif,jumlah_pph=:jumlah_pph,npwp_pemotong=:npwp_pemotong,nama_pemotong=:nama_pemotong,tanggal_bukti_potong=:tanggal_bukti_potong where nomor_bukti_potong = :nomor_bukti_potong AND kode_pajak = :kode_pajak";
				                            	$cmdupdate = Yii::app()->db->createCommand($sqlupdate);
				                            	$cmdupdate->params = array(
				                            		':masa_pajak'=>$value['A'],
				                            		':tahun_pajak'=>$value['B'],
				                            		':pembetulan'=>$value['C'],
				                            		':nomor_bukti_potong'=>$value['D'],
				                            		':npwp'=>$value['E'],
				                            		':nik'=>$value['F'],
				                            		':nama'=>$value['G'],
				                            		':alamat'=>$value['H'],
				                            		':wp_luar_negeri'=>$value['I'],
				                            		':kode_negara'=>$value['J'],
				                            		':kode_pajak'=>$value['K'],
				                            		':jumlah_bruto'=>$value['L'],
				                            		':jumlah_dpp'=>$value['M'],
				                            		':tanpa_npwp'=>$value['N'],
				                            		':tarif'=>$value['O'],
				                            		':jumlah_pph'=>$value['P'],
				                            		':npwp_pemotong'=>$value['Q'],
				                            		':nama_pemotong'=>$value['R'],
				                            		':tanggal_bukti_potong'=>date("Y-m-d",strtotime(str_replace("/", "-", $value['S']))),
				                            	);
				                            	$cmdupdate->query();
				                            	$jmlupdate++;
				                            	// $pesan[] = 'NIK = ' . $value['F'] . " , No. Bukti Potong = "  . $value['D'] . ", <b style='color:red;'>Data tersebut telah ada dalam database</b>.";
				                            }
			                        	}
			                        }
			                    }
			                }
			                if(count($pesan) > 0) {
				                $datapesan = implode("<br>", $pesan);
								
								MyApp::setLog('gagal');
				            	Yii::app()->user->setFlash('tidaksave', $datapesan);
				            }
								MyApp::setLog('sukses');
				            	Yii::app()->user->setFlash('berhasil', "data berhasil disimpan, ".$jmlinsert." data diinsert, ".$jmlupdate." data diupdate.");
				            
				            // echo "<pre>";
				            // print_r($datapesan);exit();
				            // echo "<pre>";
		                }else{
							MyApp::setLog('gagal');
		                	Yii::app()->user->setFlash('tidaksave', "template format excel tidak sesuai");
		                }
						$transaction->commit();
					    unlink($dirfile);
						$this->redirect(array('uploadtidakfinal'));
					}catch(Exception $e){
					    $transaction->rollBack();
						MyApp::setLog('gagal');
					    throw new CHttpException(null,"ERROR: ".$e->getMessage());
					}
				}
			// }
		}
		$this->render('uploadtidakfinal',array(
			'model'=>$model,
			'arraytahun'=>$arraytahun,
		));
	}

	public function actionUpload1770s(){
		$model = new Upload1770s;
		$model->unsetAttributes();
		$tahuncurrent = date("Y");
		$min = $tahuncurrent - 2;
		$max = $tahuncurrent + 6;
		$arraytahun = array();
		for ($i=$min; $i < $max; $i++) { 
			$arraytahun[$i] = $i;
		}
		if (isset($_POST['Upload1770s'])) {
			$tahun = $_POST['Upload1770s']['tahun'];
			$model->tahun = $_POST['Upload1770s']['tahun'];
			$model->fileupload = $_POST['Upload1770s']['fileupload'];
			// if($model->validate()){
				$upload = CUploadedFile::getInstance($model, 'fileupload');
				if ($upload){
					$upload_dir = Yii::getPathOfAlias('upload_dir');
	                if (!is_dir($upload_dir)) {
	                    mkdir($upload_dir);
	                    chmod($upload_dir, 0777);
	                }

	                $_peserta = $upload_dir . '/fileupload';
	                $nama_file = str_replace(".", "", microtime(true)) . "-" . $upload->name;
	                if(!is_dir($_peserta)){
	                    mkdir($_peserta);
	                    chmod($_peserta, 0777);
	                    $upload->saveAs($_peserta . "/" . $nama_file);
	                }else $upload->saveAs($_peserta . "/" . $nama_file);
					$dirfile = $_peserta . "/" . $nama_file;

					$transaction = Yii::app()->db->beginTransaction();
					try{
						Yii::import('application.vendor.PHPExcel.PHPExcel.IOFactory', true);
		                $inputFileName = $upload_dir . "/fileupload/" . $nama_file;
		                $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
		                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
		                $objPHPExcel = $objReader->load($inputFileName);
		                $return = $objPHPExcel->getActiveSheet()->toArray(null, true,true,true);
		                $kolom = $return[1]['A'].$return[1]['B'].$return[1]['C'].$return[1]['D'].$return[1]['E'].$return[1]['F'].$return[1]['G'].$return[1]['H'].$return[1]['I'].$return[1]['J'].$return[1]['K'].$return[1]['L'].$return[1]['M'].$return[1]['N'].$return[1]['O'].$return[1]['P'].$return[1]['Q'].$return[1]['R'].$return[1]['S'];
		                $kolom = strtolower(preg_replace('/\s+/', '', $kolom));
		                $par_kol = "tahunpembetulankeniknamapenghasilannetopenghasilannetolainpenghasilannetolnjumlahnetozakatsumbanganjumlahnetodikurangizakatptkppkppphterutangpengambilankreditjumlahpphterutangpphdipotong";
		                if($kolom==$par_kol){
		                	$namatbl1 = 'rpt_spt1770s_'.$tahun;
							$cektab1 = Yii::app()->db->createCommand("SHOW TABLES LIKE :par")
														->bindParam(':par',$namatbl1)
														->queryAll();
							if (count($cektab1) < 1) {
								$querybckp1 = "CREATE TABLE `rpt_spt1770s_".$tahun."` (
											  `id` int(11) NOT NULL AUTO_INCREMENT,
											  `n_tahun_pajak` int(4) DEFAULT NULL,
											  `n_pembetulan_ke` int(2) DEFAULT NULL,
											  `n_nik` int(8) DEFAULT NULL,
											  `v_nama` varchar(255) DEFAULT NULL,
											  `n_penghasilan_neto_pekerjaan` int(11) DEFAULT NULL,
											  `n_penghasilan_neto_lain` int(11) DEFAULT NULL,
											  `n_penghasilan_neto_luar_negeri` int(11) DEFAULT NULL,
											  `n_jumlah_neto` int(11) DEFAULT NULL,
											  `n_zakat_sumbangan` int(11) DEFAULT NULL,
											  `n_jumlah_neto_dikurangi_zakat` int(11) DEFAULT NULL,
											  `n_ptkp` int(11) DEFAULT NULL,
											  `n_pkp` int(11) DEFAULT NULL,
											  `n_pph_terutang` int(11) DEFAULT NULL,
											  `n_pengembalian_kredit` int(11) DEFAULT NULL,
											  `n_jumlah_pph_terutang` int(11) DEFAULT NULL,
											  `n_pph_dipotong` int(11) DEFAULT NULL,
											  PRIMARY KEY (`id`)
											) ENGINE=MyISAM AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;";

								$sqlbckp1 = Yii::app()->db->createCommand($querybckp1);
								$sqlbckp1->query();
							}
							$pesan = array();
							$jmlinsert = 0;
							$jmlupdate = 0;
			                foreach ($return as $key => $value){
			                    if ($key > 1) {
			                        if(!empty($value['C'])){
			                        	if ($value['A'] != $tahun) {
			                        		$pesan[] = 'NIK = ' . $value['C'] . " , Tahun = "  . $value['A'] . ", <b style='color:red;'>Data tahun tidak sesuai</b>.";
			                        	}else{
				                            $cek = Yii::app()->db->createCommand("SELECT COUNT(*) FROM ".$namatbl1." WHERE n_nik = :nik AND n_tahun_pajak = :thn")->bindParam(':nik',$value['C'])->bindParam(':thn',$value['A'])->queryScalar();
				                            if ($cek < 1) {
				                            	$sqlinsert = "INSERT INTO ".$namatbl1." (
				                            		n_tahun_pajak,
													n_pembetulan_ke,
													n_nik,
													v_nama,
													n_penghasilan_neto_pekerjaan,
													n_penghasilan_neto_lain,
													n_penghasilan_neto_luar_negeri,
													n_jumlah_neto,
													n_zakat_sumbangan,
													n_jumlah_neto_dikurangi_zakat,
													n_ptkp,
													n_pkp,
													n_pph_terutang,
													n_pengembalian_kredit,
													n_jumlah_pph_terutang,
													n_pph_dipotong
				                            	) VALUES (
				                            		:n_tahun_pajak,
													:n_pembetulan_ke,
													:n_nik,
													:v_nama,
													:n_penghasilan_neto_pekerjaan,
													:n_penghasilan_neto_lain,
													:n_penghasilan_neto_luar_negeri,
													:n_jumlah_neto,
													:n_zakat_sumbangan,
													:n_jumlah_neto_dikurangi_zakat,
													:n_ptkp,
													:n_pkp,
													:n_pph_terutang,
													:n_pengembalian_kredit,
													:n_jumlah_pph_terutang,
													:n_pph_dipotong
				                            	)";
				                            	$cmdinsert = Yii::app()->db->createCommand($sqlinsert);
				                            	$cmdinsert->params = array(
				                            		':n_tahun_pajak'=>$value['A'],
													':n_pembetulan_ke'=>$value['B'],
													':n_nik'=>$value['C'],
													':v_nama'=>$value['D'],
													':n_penghasilan_neto_pekerjaan'=>$value['E'],
													':n_penghasilan_neto_lain'=>$value['F'],
													':n_penghasilan_neto_luar_negeri'=>$value['G'],
													':n_jumlah_neto'=>$value['H'],
													':n_zakat_sumbangan'=>$value['I'],
													':n_jumlah_neto_dikurangi_zakat'=>$value['J'],
													':n_ptkp'=>$value['K'],
													':n_pkp'=>$value['L'],
													':n_pph_terutang'=>$value['M'],
													':n_pengembalian_kredit'=>$value['N'],
													':n_jumlah_pph_terutang'=>$value['O'],
													':n_pph_dipotong'=>$value['P'],
				                            	);
				                            	$cmdinsert->query();
				                            	$jmlinsert++;
				                            }else{
				                            	$sqlupdate = "UPDATE ".$namatbl1." set 
				                            				n_tahun_pajak = :n_tahun_pajak,
															n_pembetulan_ke = :n_pembetulan_ke,
															n_nik = :n_nik,
															v_nama = :v_nama,
															n_penghasilan_neto_pekerjaan = :n_penghasilan_neto_pekerjaan,
															n_penghasilan_neto_lain = :n_penghasilan_neto_lain,
															n_penghasilan_neto_luar_negeri = :n_penghasilan_neto_luar_negeri,
															n_jumlah_neto = :n_jumlah_neto,
															n_zakat_sumbangan = :n_zakat_sumbangan,
															n_jumlah_neto_dikurangi_zakat = :n_jumlah_neto_dikurangi_zakat,
															n_ptkp = :n_ptkp,
															n_pkp = :n_pkp,
															n_pph_terutang = :n_pph_terutang,
															n_pengembalian_kredit = :n_pengembalian_kredit,
															n_jumlah_pph_terutang = :n_jumlah_pph_terutang,
															n_pph_dipotong = :n_pph_dipotong
				                            	 			where n_nik = :n_nik AND n_tahun_pajak = :n_tahun_pajak";
				                            	$cmdupdate = Yii::app()->db->createCommand($sqlupdate);
				                            	$cmdupdate->params = array(
				                            		':n_tahun_pajak'=>$value['A'],
													':n_pembetulan_ke'=>$value['B'],
													':n_nik'=>$value['C'],
													':v_nama'=>$value['D'],
													':n_penghasilan_neto_pekerjaan'=>$value['E'],
													':n_penghasilan_neto_lain'=>$value['F'],
													':n_penghasilan_neto_luar_negeri'=>$value['G'],
													':n_jumlah_neto'=>$value['H'],
													':n_zakat_sumbangan'=>$value['I'],
													':n_jumlah_neto_dikurangi_zakat'=>$value['J'],
													':n_ptkp'=>$value['K'],
													':n_pkp'=>$value['L'],
													':n_pph_terutang'=>$value['M'],
													':n_pengembalian_kredit'=>$value['N'],
													':n_jumlah_pph_terutang'=>$value['O'],
													':n_pph_dipotong'=>$value['P'],
				                            	);
				                            	$cmdupdate->query();
				                            	$jmlupdate++;
				                            	// $pesan[] = 'NIK = ' . $value['F'] . " , No. Bukti Potong = "  . $value['D'] . ", <b style='color:red;'>Data tersebut telah ada dalam database</b>.";
				                            }
			                        	}
			                        }
			                    }
			                }
			                if(count($pesan) > 0) {
				                $datapesan = implode("<br>", $pesan);
								MyApp::setLog('gagal');
				            	Yii::app()->user->setFlash('tidaksave', $datapesan);
				            }
								MyApp::setLog('sukses');
				            	Yii::app()->user->setFlash('berhasil', "data berhasil disimpan, ".$jmlinsert." data diinsert, ".$jmlupdate." data diupdate.");
				            
				            // echo "<pre>";
				            // print_r($datapesan);exit();
				            // echo "<pre>";
		                }else{
							MyApp::setLog('gagal');
		                	Yii::app()->user->setFlash('tidaksave', "template format excel tidak sesuai");
		                }
						$transaction->commit();
					    unlink($dirfile);
						$this->redirect(array('upload1770s'));
					}catch(Exception $e){
					    $transaction->rollBack();
						MyApp::setLog('gagal');
					    throw new CHttpException(null,"ERROR: ".$e->getMessage());
					}
				}
			// }
		}
		$this->render('upload1770s',array(
			'model'=>$model,
			'arraytahun'=>$arraytahun,
		));
	}

	public function actionUpload1721(){
		$model = new Upload1721;
		$model->unsetAttributes();
		$tahuncurrent = date("Y");
		$min = $tahuncurrent - 2;
		$max = $tahuncurrent + 6;
		$arraytahun = array();
		$role = Yii::app()->user->roles;
		$assrole = Yii::app()->db->createCommand("SELECT pemotong from assignment_pemotong WHERE itemname = :itm limit 1")
					->bindParam(':itm',$role)
					->queryScalar();
		$arrayassrole = array();
		$arrayassrole['PT. Telekomunikasi Indonesia,Tbk'] = 'PT. Telekomunikasi Indonesia,Tbk';
		$allpemotong = Yii::app()->db->createCommand("SELECT pemotong from assignment_pemotong")
						->queryAll();
		foreach ($allpemotong as $key => $value) {
			$arrayassrole[$value['pemotong']] = $value['pemotong'];
		}
		for ($i=$min; $i < $max; $i++) { 
			$arraytahun[$i] = $i;
		}
		if (isset($_POST['Upload1721'])) {
			if ($role != 'Admin') {
				$namapem = $assrole;
			}else{
				$namapem = !empty($_POST['Upload1721']['assrole']) ? $_POST['Upload1721']['assrole'] : 'PT. Telekomunikasi Indonesia,Tbk';
			}
			$tahun = $_POST['Upload1721']['tahun'];
			$model->tahun = $_POST['Upload1721']['tahun'];
			$model->fileupload = $_POST['Upload1721']['fileupload'];
			// if($model->validate()){
				$upload = CUploadedFile::getInstance($model, 'fileupload');
				if ($upload){
					$upload_dir = Yii::getPathOfAlias('upload_dir');
	                if (!is_dir($upload_dir)) {
	                    mkdir($upload_dir);
	                    chmod($upload_dir, 0777);
	                }

	                $_peserta = $upload_dir . '/fileupload';
	                $nama_file = str_replace(".", "", microtime(true)) . "-" . $upload->name;
	                if(!is_dir($_peserta)){
	                    mkdir($_peserta);
	                    chmod($_peserta, 0777);
	                    $upload->saveAs($_peserta . "/" . $nama_file);
	                }else $upload->saveAs($_peserta . "/" . $nama_file);
					$dirfile = $_peserta . "/" . $nama_file;

					$transaction = Yii::app()->db->beginTransaction();
					try{
						Yii::import('application.vendor.PHPExcel.PHPExcel.IOFactory', true);
		                $inputFileName = $upload_dir . "/fileupload/" . $nama_file;
		                $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
		                $objReader = PHPExcel_IOFactory::createReader($inputFileType);
		                $objPHPExcel = $objReader->load($inputFileName);
		                $return = $objPHPExcel->getActiveSheet()->toArray(null, true,true,true);
		                $kolom = $return[3]['A'].$return[3]['B'].$return[3]['C'].$return[3]['D'].$return[3]['E'].$return[3]['F'].$return[3]['G'].$return[3]['H'].$return[3]['I'].$return[3]['J'].$return[3]['K'].$return[3]['L'].$return[3]['M'].$return[3]['N'].$return[3]['O'].$return[3]['P'].$return[3]['Q'].$return[3]['R'].$return[3]['S'].$return[3]['T'].$return[3]['U'].$return[3]['V'].$return[3]['W'].$return[3]['X'].$return[3]['Y'].$return[3]['Z'].$return[3]['AA'].$return[3]['AB'].$return[3]['AC'].$return[3]['AD'].$return[3]['AE'].$return[3]['AF'].$return[3]['AG'].$return[3]['AH'].$return[3]['AI'].$return[3]['AJ'].$return[3]['AK'].$return[3]['AL'].$return[3]['AM'].$return[3]['AN'].$return[3]['AO'].$return[3]['AP'].$return[3]['AQ'].$return[3]['AR'];
		                $kolom = strtolower(preg_replace('/\s+/', '', $kolom));
		                $par_kol = "masapajaktahunpajakpembetulannomorbuktipotongmasaperolehanawalmasaperolehanakhirnpwpniknamaalamatjeniskelaminstatusptkpjumlahtanggungannamajabatanwpluarnegerikodenegarakodepajakjumlah1jumlah2jumlah3jumlah4jumlah5jumlah6jumlah7jumlah8jumlah9jumlah10jumlah11jumlah12jumlah13jumlah14jumlah15jumlah16jumlah17jumlah18jumlah19jumlah20statuspindahnpwppemotongnamapemotongtanggalbuktipotongpayrollareakodekppnamakpp";
		                if($kolom==$par_kol){
		                	$namatbl1 = 'rpt_spt1721_'.$tahun;
							$cektab1 = Yii::app()->db->createCommand("SHOW TABLES LIKE :par")
														->bindParam(':par',$namatbl1)
														->queryAll();
							if (count($cektab1) < 1) {
								$querybckp1 = "CREATE TABLE `rpt_spt1721_".$tahun."` (
												`N_URUT` varchar(30) DEFAULT NULL,
												`N_NIK` decimal(6,0) DEFAULT NULL,
												`V_NAMA_KARYAWAN` varchar(50) DEFAULT NULL,
												`N_TAHUN` decimal(4,0) DEFAULT NULL,
												`N_BULANAWAL` decimal(2,0) DEFAULT NULL,
												`V_BULANAWAL` varchar(10) DEFAULT NULL,
												`N_BULANAKHIR` decimal(2,0) DEFAULT NULL,
												`V_BULANAKHIR` varchar(10) DEFAULT NULL,
												`C_PAYAREA` char(2) DEFAULT NULL,
												`C_KODE_PSA` char(4) DEFAULT NULL,
												`C_NAMA_PSA` varchar(60) DEFAULT NULL,
												`C_KODE_KPP` char(4) DEFAULT NULL,
												`V_NAMA_KPP` varchar(40) DEFAULT NULL,
												`V_NPWP_KANTOR` varchar(15) DEFAULT NULL,
												`V_NPWP_PRIBADI` varchar(15) DEFAULT NULL,
												`V_JABATAN` varchar(100) DEFAULT NULL,
												`V_ALAMAT` varchar(100) DEFAULT NULL,
												`V_ALAMAT_KANTOR` varchar(100) DEFAULT NULL,
												`C_BISNIS_AREA` char(4) DEFAULT NULL,
												`V_BISNIS_AREA` varchar(100) DEFAULT NULL,
												`C_KODE_FIN` varchar(6) DEFAULT NULL,
												`V_NAMA_FIN` varchar(100) DEFAULT NULL,
												`C_STATUS_NIKAH` char(1) DEFAULT NULL,
												`C_GENDER` char(1) DEFAULT NULL,
												`V_ADT` varchar(4) DEFAULT NULL,
												`N_01` decimal(12,0) DEFAULT NULL,
												`N_02` decimal(12,0) DEFAULT NULL,
												`N_03` decimal(12,0) DEFAULT NULL,
												`N_04` decimal(12,0) DEFAULT NULL,
												`N_05` decimal(12,0) DEFAULT NULL,
												`N_06` decimal(12,0) DEFAULT NULL,
												`N_07` decimal(12,0) DEFAULT NULL,
												`N_08` decimal(12,0) DEFAULT NULL,
												`N_09` decimal(12,0) DEFAULT NULL,
												`N_10` decimal(12,0) DEFAULT NULL,
												`N_11` decimal(12,0) DEFAULT NULL,
												`N_12` decimal(12,0) DEFAULT NULL,
												`N_13` decimal(12,0) DEFAULT NULL,
												`N_14` decimal(12,0) DEFAULT NULL,
												`N_15` decimal(12,0) DEFAULT NULL,
												`N_16` decimal(12,0) DEFAULT NULL,
												`N_17` decimal(12,0) DEFAULT NULL,
												`N_18` decimal(12,0) DEFAULT NULL,
												`N_19` decimal(12,0) DEFAULT NULL,
												`N_20` decimal(12,0) DEFAULT NULL,
												`N_21` decimal(12,0) DEFAULT NULL,
												`N_22` decimal(12,0) DEFAULT NULL,
												`N_23` decimal(12,0) DEFAULT NULL,
												`N_24` decimal(12,0) DEFAULT NULL,
												`N_NIK_TTD` decimal(6,0) DEFAULT NULL,
												`V_NAMA_TTD` varchar(50) DEFAULT NULL,
												`V_JABATAN_TTD` varchar(100) DEFAULT NULL,
												`V_KOTA_TTD` varchar(50) DEFAULT NULL,
												`D_TGL_UBAH` datetime DEFAULT NULL,
												`IDOPERATOR` decimal(8,0) DEFAULT NULL,
												`TEMPLATE_TTD` text,
												`V_NAMA_PEMOTONG` varchar(255) DEFAULT NULL,
                                                `KODE_PAJAK` varchar(20) DEFAULT NULL
												) ENGINE=InnoDB DEFAULT CHARSET=utf8;";

								$sqlbckp1 = Yii::app()->db->createCommand($querybckp1);
								$sqlbckp1->query();
							}
							$pesan = array();
							$jmlinsert = 0;
							$jmlupdate = 0;
			                foreach ($return as $key => $value){
			                    if ($key > 3) {
			                        if(!empty($value['H'])){
			                        	if ($value['B'] != $tahun) {
			                        		$pesan[] = 'NIK = ' . $value['H'] . " , Tahun = "  . $value['B'] . ", <b style='color:red;'>Data tahun tidak sesuai</b>.";
			                        	}else{
				                            $cek = Yii::app()->db->createCommand("SELECT COUNT(*) FROM ".$namatbl1." WHERE N_NIK = :nik AND N_TAHUN = :thn AND V_NAMA_PEMOTONG = :namapem AND N_BULANAWAL = :N_BULANAWAL AND N_BULANAKHIR = :N_BULANAKHIR AND N_URUT = :N_URUT")->bindParam(':nik',$value['H'])->bindParam(':thn',$value['B'])->bindParam(':namapem',$namapem)->bindParam(':N_BULANAWAL',$value['E'])->bindParam(':N_BULANAKHIR',$value['F'])->bindParam(':N_URUT',$value['D'])->queryScalar();
				                            if ($cek < 1) {
				                            	$sqlinsert = "INSERT INTO ".$namatbl1." (
				                            		N_URUT,
				                            		N_NIK,
													V_NAMA_KARYAWAN,
													N_TAHUN,
													N_BULANAWAL,
													N_BULANAKHIR,
													C_PAYAREA,
													C_KODE_PSA,
													C_NAMA_PSA,
													C_KODE_KPP,
													V_NAMA_KPP,
													V_NPWP_KANTOR,
													V_NPWP_PRIBADI,
													V_JABATAN,
													V_ALAMAT,
													V_ALAMAT_KANTOR,
													C_BISNIS_AREA,
													V_BISNIS_AREA,
													C_KODE_FIN,
													V_NAMA_FIN,
													C_STATUS_NIKAH,
													C_GENDER,
													V_ADT,
													N_01,
													N_02,
													N_03,
													N_04,
													N_05,
													N_06,
                                                    N_07,
													N_08,
													N_09,
                                                    N_10,
													N_11,
													N_12,
													N_13,
													N_14,
													N_15,
													N_16,
													N_17,
													N_18,
													N_19,
													N_20,
													N_21,
													N_24,
													V_NAMA_PEMOTONG,
													V_BULANAWAL,
													V_BULANAKHIR,
                                                    KODE_PAJAK
				                            	) VALUES (
				                            		:N_URUT,
				                            		:N_NIK,
													:V_NAMA_KARYAWAN,
													:N_TAHUN,
													:N_BULANAWAL,
													:N_BULANAKHIR,
													:C_PAYAREA,
													:C_KODE_PSA,
													:C_NAMA_PSA,
													:C_KODE_KPP,
													:V_NAMA_KPP,
													:V_NPWP_KANTOR,
													:V_NPWP_PRIBADI,
													:V_JABATAN,
													:V_ALAMAT,
													:V_ALAMAT_KANTOR,
													:C_BISNIS_AREA,
													:V_BISNIS_AREA,
													:C_KODE_FIN,
													:V_NAMA_FIN,
													:C_STATUS_NIKAH,
													:C_GENDER,
													:V_ADT,
													:N_01,
													:N_02,
													:N_03,
													:N_04,
													:N_05,
													:N_06,
                                                    :N_07,
													:N_08,
													:N_09,
                                                    :N_10,
													:N_11,
													:N_12,
													:N_13,
													:N_14,
													:N_15,
													:N_16,
													:N_17,
													:N_18,
													:N_19,
													:N_20,
													:N_21,
													:N_24,
													:V_NAMA_PEMOTONG,
													:V_BULANAWAL,
													:V_BULANAKHIR,
                                                    :KODE_PAJAK
				                            	)";
				                            	$cmdinsert = Yii::app()->db->createCommand($sqlinsert);
				                            	$cmdinsert->params = array(
				                            		':N_URUT'=>$value['D'],
				                            		':N_NIK'=>$value['H'],
													':V_NAMA_KARYAWAN'=>$value['I'],
													':N_TAHUN'=>$value['B'],
													':N_BULANAWAL'=>$value['E'],
													':N_BULANAKHIR'=>$value['F'],
													':C_PAYAREA'=>$value['AP'],
													':C_KODE_PSA'=>null,
													':C_NAMA_PSA'=>null,
													':C_KODE_KPP'=>$value['AQ'],
													':V_NAMA_KPP'=>$value['AR'],
													':V_NPWP_KANTOR'=>$value['AM'],
													':V_NPWP_PRIBADI'=>$value['G'],
													':V_JABATAN'=>$value['N'],
													':V_ALAMAT'=>$value['J'],
													':V_ALAMAT_KANTOR'=>null,
													':C_BISNIS_AREA'=>null,
													':V_BISNIS_AREA'=>null,
													':C_KODE_FIN'=>null,
													':V_NAMA_FIN'=>null,
                                                    ':C_STATUS_NIKAH'=>$value['L']=='TK'?0:1,
													':C_GENDER'=> $value['K'] == 'F' ? 2 : 1,
													':V_ADT'=>$value['L'].'/'.($value['M']==0 ? '-' : $value['M']),
													':N_01'=>str_replace(',', '',str_replace('.', '', $value['R'])),
													':N_02'=>str_replace(',', '',str_replace('.', '', $value['S'])),
													':N_03'=>str_replace(',', '',str_replace('.', '', $value['T'])),
													':N_04'=>str_replace(',', '',str_replace('.', '', $value['U'])),
													':N_05'=>str_replace(',', '',str_replace('.', '', $value['V'])),
													':N_06'=>str_replace(',', '',str_replace('.', '', $value['W'])),
                                                    ':N_07'=>str_replace(',', '',str_replace('.', '', $value['X'])),
													':N_08'=>str_replace(',', '',str_replace('.', '', $value['Y'])),
													':N_09'=>str_replace(',', '',str_replace('.', '', $value['Z'])),
                                                    ':N_10'=>str_replace(',', '',str_replace('.', '', $value['AA'])),
													':N_11'=>str_replace(',', '',str_replace('.', '', $value['AB'])),
													':N_12'=>str_replace(',', '',str_replace('.', '', $value['AC'])),
													':N_13'=>str_replace(',', '',str_replace('.', '', $value['AD'])),
													':N_14'=>str_replace(',', '',str_replace('.', '', $value['AE'])),
													':N_15'=>str_replace(',', '',str_replace('.', '', $value['AF'])),
													':N_16'=>str_replace(',', '',str_replace('.', '', $value['AG'])),
													':N_17'=>str_replace(',', '',str_replace('.', '', $value['AH'])),
													':N_18'=>str_replace(',', '',str_replace('.', '', $value['AI'])),
													':N_19'=>str_replace(',', '',str_replace('.', '', $value['AJ'])),
													':N_20'=>str_replace(',', '',str_replace('.', '', $value['AK'])),
													':N_21'=>NULL,
													':N_24'=>NULL,
													':V_NAMA_PEMOTONG'=>$namapem,
													':V_BULANAWAL'=>MyApp::convertBulan($value['E']),
													':V_BULANAKHIR'=>MyApp::convertBulan($value['F']),
                                                    ':KODE_PAJAK'=>$value['Q']
				                            	);
				                            	$cmdinsert->query();
				                            	$jmlinsert++;
				                            }else{
				                            	$sqlupdate = "UPDATE ".$namatbl1." set 
				                            				N_URUT = :N_URUT,
				                            				N_NIK = :N_NIK,
															V_NAMA_KARYAWAN = :V_NAMA_KARYAWAN,
															N_TAHUN = :N_TAHUN,
															N_BULANAWAL = :N_BULANAWAL,
															N_BULANAKHIR = :N_BULANAKHIR,
															C_PAYAREA = :C_PAYAREA,
															C_KODE_PSA = :C_KODE_PSA,
															C_NAMA_PSA = :C_NAMA_PSA,
															C_KODE_KPP = :C_KODE_KPP,
															V_NAMA_KPP = :V_NAMA_KPP,
															V_NPWP_KANTOR = :V_NPWP_KANTOR,
															V_NPWP_PRIBADI = :V_NPWP_PRIBADI,
															V_JABATAN = :V_JABATAN,
															V_ALAMAT = :V_ALAMAT,
															V_ALAMAT_KANTOR = :V_ALAMAT_KANTOR,
															C_BISNIS_AREA = :C_BISNIS_AREA,
															V_BISNIS_AREA = :V_BISNIS_AREA,
															C_KODE_FIN = :C_KODE_FIN,
															V_NAMA_FIN = :V_NAMA_FIN,
															C_STATUS_NIKAH = :C_STATUS_NIKAH,
															C_GENDER = :C_GENDER,
															V_ADT = :V_ADT,
															N_01 = :N_01,
															N_02 = :N_02,
															N_03 = :N_03,
															N_04 = :N_04,
															N_05 = :N_05,
															N_06 = :N_06,
                                                            N_07 = :N_07,
															N_08 = :N_08,
															N_09 = :N_09,
                                                            N_10 = :N_10,
															N_11 = :N_11,
															N_12 = :N_12,
															N_13 = :N_13,
															N_14 = :N_14,
															N_15 = :N_15,
															N_16 = :N_16,
															N_17 = :N_17,
															N_18 = :N_18,
															N_19 = :N_19,
															N_20 = :N_20,
															N_21 = :N_21,
															N_24 = :N_24,
															V_NAMA_PEMOTONG = :V_NAMA_PEMOTONG,
															V_BULANAWAL = :V_BULANAWAL,
															V_BULANAKHIR = :V_BULANAKHIR,
                                                            KODE_PAJAK = :KODE_PAJAK
				                            	 			WHERE 
				                            	 			N_NIK = :N_NIK AND N_TAHUN = :N_TAHUN AND V_NAMA_PEMOTONG = :V_NAMA_PEMOTONG AND N_BULANAWAL = :N_BULANAWAL AND N_BULANAKHIR = :N_BULANAKHIR AND N_URUT = :N_URUT";
				                            	$cmdupdate = Yii::app()->db->createCommand($sqlupdate);
				                            	$cmdupdate->params = array(
				                            		':N_URUT'=>$value['D'],
                                                    ':N_NIK'=>$value['H'],
                                                    ':V_NAMA_KARYAWAN'=>$value['I'],
                                                    ':N_TAHUN'=>$value['B'],
                                                    ':N_BULANAWAL'=>$value['E'],
                                                    ':N_BULANAKHIR'=>$value['F'],
                                                    ':C_PAYAREA'=>$value['AP'],
                                                    ':C_KODE_PSA'=>null,
                                                    ':C_NAMA_PSA'=>null,
                                                    ':C_KODE_KPP'=>$value['AQ'],
                                                    ':V_NAMA_KPP'=>$value['AR'],
                                                    ':V_NPWP_KANTOR'=>$value['AM'],
                                                    ':V_NPWP_PRIBADI'=>$value['G'],
                                                    ':V_JABATAN'=>$value['N'],
                                                    ':V_ALAMAT'=>$value['J'],
                                                    ':V_ALAMAT_KANTOR'=>null,
                                                    ':C_BISNIS_AREA'=>null,
                                                    ':V_BISNIS_AREA'=>null,
                                                    ':C_KODE_FIN'=>null,
                                                    ':V_NAMA_FIN'=>null,
                                                    ':C_STATUS_NIKAH'=>$value['L']=='TK'?0:1,
                                                    ':C_GENDER'=> $value['K'] == 'F' ? 2 : 1,
                                                    ':V_ADT'=>$value['L'].'/'.($value['M']==0 ? '-' : $value['M']),
                                                    ':N_01'=>str_replace(',', '',str_replace('.', '', $value['R'])),
                                                    ':N_02'=>str_replace(',', '',str_replace('.', '', $value['S'])),
                                                    ':N_03'=>str_replace(',', '',str_replace('.', '', $value['T'])),
                                                    ':N_04'=>str_replace(',', '',str_replace('.', '', $value['U'])),
                                                    ':N_05'=>str_replace(',', '',str_replace('.', '', $value['V'])),
                                                    ':N_06'=>str_replace(',', '',str_replace('.', '', $value['W'])),
                                                    ':N_07'=>str_replace(',', '',str_replace('.', '', $value['X'])),
                                                    ':N_08'=>str_replace(',', '',str_replace('.', '', $value['Y'])),
                                                    ':N_09'=>str_replace(',', '',str_replace('.', '', $value['Z'])),
                                                    ':N_10'=>str_replace(',', '',str_replace('.', '', $value['AA'])),
                                                    ':N_11'=>str_replace(',', '',str_replace('.', '', $value['AB'])),
                                                    ':N_12'=>str_replace(',', '',str_replace('.', '', $value['AC'])),
                                                    ':N_13'=>str_replace(',', '',str_replace('.', '', $value['AD'])),
                                                    ':N_14'=>str_replace(',', '',str_replace('.', '', $value['AE'])),
                                                    ':N_15'=>str_replace(',', '',str_replace('.', '', $value['AF'])),
                                                    ':N_16'=>str_replace(',', '',str_replace('.', '', $value['AG'])),
                                                    ':N_17'=>str_replace(',', '',str_replace('.', '', $value['AH'])),
                                                    ':N_18'=>str_replace(',', '',str_replace('.', '', $value['AI'])),
                                                    ':N_19'=>str_replace(',', '',str_replace('.', '', $value['AJ'])),
                                                    ':N_20'=>str_replace(',', '',str_replace('.', '', $value['AK'])),
                                                    ':N_21'=>NULL,
                                                    ':N_24'=>NULL,
                                                    ':V_NAMA_PEMOTONG'=>$namapem,
                                                    ':V_BULANAWAL'=>MyApp::convertBulan($value['E']),
                                                    ':V_BULANAKHIR'=>MyApp::convertBulan($value['F']),
                                                    ':KODE_PAJAK'=>$value['Q']
				                            	);
				                            	$cmdupdate->query();
				                            	$jmlupdate++;
				                            	// $pesan[] = 'NIK = ' . $value['F'] . " , No. Bukti Potong = "  . $value['D'] . ", <b style='color:red;'>Data tersebut telah ada dalam database</b>.";
				                            }
			                        	}
			                        }
			                    }
			                }
			                if(count($pesan) > 0) {
				                $datapesan = implode("<br>", $pesan);
								MyApp::setLog('gagal');
				            	Yii::app()->user->setFlash('tidaksave', $datapesan);
				            }
								MyApp::setLog('sukses');
				            	Yii::app()->user->setFlash('berhasil', "data berhasil disimpan, ".$jmlinsert." data diinsert, ".$jmlupdate." data diupdate.");
				            
				            // echo "<pre>";
				            // print_r($datapesan);exit();
				            // echo "<pre>";
		                }else{
							MyApp::setLog('gagal');
		                	Yii::app()->user->setFlash('tidaksave', "template format excel tidak sesuai");
		                }
						$transaction->commit();
					    unlink($dirfile);
						$this->redirect(array('upload1721'));
					}catch(Exception $e){
					    $transaction->rollBack();
						MyApp::setLog('gagal');
					    throw new CHttpException(null,"ERROR: ".$e->getMessage());
					}
				}
			// }
		}
		$this->render('upload1721',array(
			'model'=>$model,
			'arraytahun'=>$arraytahun,
			'assrole'=>$assrole,
			'arrayassrole'=>$arrayassrole
		));
	}

}