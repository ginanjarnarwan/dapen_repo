<?php

class PayslipController extends Controller
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','create','update','delete','upload','proses','report','reportdivisi','reportglobal','reportglobalnow','uploadhrcurrent','Uploadatk','Inputall','deletecost','Reportdivisinow','Reportnow','Informasi','Popshowman','Showman'),
				// 'users'=>array('@'),
				'roles'=>array('Admin')
			),
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','report','reportdivisi','reportglobal','reportglobalnow','Informasi','Popshowman','Showman'),
				// 'users'=>array('@'),
				'roles'=>array('View')
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new CDINAS;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['CDINAS']))
		{
			$model->attributes=$_POST['CDINAS'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->DINASID));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['CDINAS']))
		{
			$model->attributes=$_POST['CDINAS'];
			if($model->save())
				$this->redirect(array('view','id'=>$model->DINASID));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		$blncrnt= date("m");
		$thncrnt= date("Y");
		$cekakhir = Yii::app()->db->createCommand("SELECT * FROM t_realsap t ORDER BY t.TAHUN DESC,t.BULAN DESC LIMIT 1")
					->queryRow();
		$cekblnini = Yii::app()->db->createCommand("SELECT * FROM t_realsap t WHERE t.TAHUN=:tahun AND t.BULAN=:bulan LIMIT 1")
					->bindParam(':bulan',$blncrnt)
					->bindParam(':tahun',$thncrnt)
					->queryRow();

		if($cekblnini){
			$blnshow = $cekblnini["BULAN"];
			$thnshow = $cekblnini["TAHUN"];
		}else{
			$blnshow = $cekakhir["BULAN"];
			$thnshow = $cekakhir["TAHUN"];
		}

		$model=new TRealsap('search');
		$model->TAHUN = date("Y");

		$filter = false;
		if(isset($_REQUEST['TRealsapex'])){
			$model->unsetAttributes();  // clear any default values
			$model->attributes=$_REQUEST['TRealsapex'];
			$filter = true;
		}

		$model2=new TRealsap('search');
		$model2->TAHUN = $thnshow;
		$model2->BULAN = $blnshow;
		if(isset($_GET['TRealsap'])){
			$model2->unsetAttributes();  // clear any default values
			$model->attributes=$_GET['TRealsap'];
		}

		$this->render('index',array(
			'model'=>$model,
			'model2'=>$model2,
			'thnshow'=>$thnshow,
			'blnshow'=>$blnshow,
			'filter'=>$filter,
		));
	}

	public function actionProses($tahun,$bulan)
	{
		$relsap = TRealsap::model()->findByAttributes(array('TAHUN'=>$tahun,'BULAN'=>$bulan));
		$pyslip = TPayslip::model()->findAllByAttributes(array('TAHUN'=>$tahun,'BULAN'=>$bulan));
		// $cek1 = MyApp::statusProses($tahun,$bulan);
		$ceq = "SELECT 
				cr.NIK as NIK1,atk.NIK as NIK2,atk.SEGMENTASI_KODE 
				FROM 
				t_payslip py
				LEFT JOIN m_hr_current cr ON cr.BULAN = py.BULAN AND cr.TAHUN = py.TAHUN AND cr.NIK = py.NIK 
				LEFT JOIN m_mapping_atk atk ON atk.NIK = py.NIK AND atk.TAHUN = py.TAHUN AND atk.BULAN = py.BULAN 
				where cr.TAHUN = :tahun AND cr.BULAN = :bulan AND (atk.NIK = '' || atk.NIK is null) AND cr.KODE_DIVISI IN ('DIV-TR1','DIV-TR2','DIV-TR3','DIV-TR4','DIV-TR5','DIV-TR6','DIV-TR7');";
		$sqlceq = Yii::app()->db->createCommand($ceq);
		$sqlceq->bindParam(':tahun',$tahun);
		$sqlceq->bindParam(':bulan',$bulan);
		$hasceq = $sqlceq->queryAll();

		$ceq2 = "SELECT * FROM m_hr_current cur 
					WHERE (cur.KODE_DIVISI = '' OR cur.KODE_DIVISI IS NULL) AND cur.BULAN = :bulan AND cur.TAHUN = :tahun";
		$sqlceq2 = Yii::app()->db->createCommand($ceq2);
		$sqlceq2->BindParam(':bulan',$bulan);
		$sqlceq2->BindParam(':tahun',$tahun);
		$hasceq2 = $sqlceq2->queryAll();

		$message = "Tidak dapat diproses, terdapat <a class='btn btn-outline btn-danger' href='#' id='export_blmmap_".$bulan."_".$tahun."'><b>".count($hasceq)."</b></a> belum dimappingkan dan <a class='btn btn-outline btn-danger' href='#' id='export_blmdv_".$bulan."_".$tahun."'><b>".count($hasceq2)."</b></a> kode divisi kosong, segera lengkapi terlebih dahulu";
		// var_dump($message);exit();
		if (count($pyslip) < 1) {
			Yii::app()->user->setFlash('danger', "Belum ada data Payslip bulan tersebut");
			$this->redirect(array('index'));
		}elseif (empty($relsap)) {
			Yii::app()->user->setFlash('danger', "Belum ada data real SAP bulan tersebut");
			$this->redirect(array('index'));
		}elseif(count($hasceq) > 0 || count($hasceq2) > 0){
			Yii::app()->user->setFlash('danger', $message);
			$this->redirect(array('index'));
		}else{
			$hrcur = MHrCurrent::model()->findAllByAttributes(array('TAHUN'=>$tahun,'BULAN'=>$bulan));
			$mapingatk = MMappingAtk::model()->findAllByAttributes(array('TAHUN'=>$tahun,'BULAN'=>$bulan));
			if (count($hrcur)<1 || count($mapingatk)<1) {
				Yii::app()->user->setFlash('danger', "data hr current bulan tersebut belum tersedia, silakan upload terlebih dahulu");
				$this->redirect(array('index'));	
			}else{
			$transaction = Yii::app()->db->beginTransaction();
				try{
					$del1 = "DELETE FROM t_pros_cfu_fu  WHERE BULAN = :bulan AND TAHUN = :tahun";
					$sqldel1 = Yii::app()->db->createCommand($del1);
					$sqldel1->bindParam(':bulan',$bulan);
					$sqldel1->bindParam(':tahun',$tahun);
					$sqldel1->query();

					$created_by = Yii::app()->user->id;
					$created_time = date("Y-m-d h:i:sa");

					$query1 = "INSERT INTO t_pros_cfu_fu(TAHUN,BULAN,SEGMENTASI_KODE,TREG_NTREG,SEGMENTASI,JUMLAH,TOTAL,PROSENTASE,CREATED_BY,CREATED_TIME)
								SELECT
									a.TAHUN,
									a.BULAN,
									-- sum(coalesce(a.BASE_SALARY,0)) as BASE_SALARY,
									-- sum(coalesce(a.BANTUAN_KEMAHALAN,0)) as BANTUAN_KEMAHALAN,
									-- sum(coalesce(a.INSENTIF_PREMIUM,0)) as INSENTIF_PREMIUM,
									-- sum(coalesce(a.INSENTIF_JABATAN,0)) as INSENTIF_JABATAN,
									-- sum(coalesce(a.ADJUSTMENT_INSENTIF_JBT,0)) as ADJUSTMENT_INSENTIF_JBT,
									b.SEGMENTASI_KODE,
									c.TREG_NTREG,
									c.SEGMENTASI,
									(sum(coalesce(a.TGC,0))) as jumlah,
									(select (sum(coalesce(a.TGC,0))) as total from t_payslip a WHERE a.BULAN = :bulan AND a.TAHUN = :tahun) as total,
									((sum(coalesce(a.TGC,0)))/(select (sum(coalesce(a.TGC,0))) as total from t_payslip a WHERE a.BULAN = :bulan AND a.TAHUN = :tahun)) as prosentase,
									(select :created_by) as CREATED_BY,
									(select :created_time) as CREATED_TIME
								FROM
									`t_payslip` a
								JOIN m_mapping_atk b ON a.NIK = b.NIK AND b.BULAN = a.BULAN AND b.TAHUN = a.TAHUN
								JOIN m_segmentasi c ON b.SEGMENTASI_KODE = c.KODE
								WHERE a.BULAN = :bulan AND a.TAHUN = :tahun
								GROUP BY c.TREG_NTREG,c.SEGMENTASI;";
					$sql1 = Yii::app()->db->createCommand($query1);
					$sql1->bindParam(':created_by',$created_by);
					$sql1->bindParam(':created_time',$created_time);
					$sql1->bindParam(':bulan',$bulan);
					$sql1->bindParam(':tahun',$tahun);
					$sql1->query();

					$del2 = "DELETE FROM t_pros_divisi WHERE TAHUN = :tahun AND BULAN = :bulan";
					$sqldel2 = Yii::app()->db->createCommand($del2);
					$sqldel2->bindParam(':bulan',$bulan);
					$sqldel2->bindParam(':tahun',$tahun);
					$sqldel2->query();					
					
					$query2 = "INSERT INTO t_pros_divisi(TAHUN,BULAN,C_KODE_DIVISI,V_SHORT_DIVISI,JUMLAH,TOTAL,PROSENTASE,CREATED_BY,CREATED_TIME)
								SELECT
									a.TAHUN,
									a.BULAN,
									f.KODE_DIVISI,
									f.DIVISI,
									-- sum(coalesce(a.BASE_SALARY,0)) as BASE_SALARY,
									-- sum(coalesce(a.BANTUAN_KEMAHALAN,0)) as BANTUAN_KEMAHALAN,
									-- sum(coalesce(a.INSENTIF_PREMIUM,0)) as INSENTIF_PREMIUM,
									-- sum(coalesce(a.INSENTIF_JABATAN,0)) as INSENTIF_JABATAN,
									-- sum(coalesce(a.ADJUSTMENT_INSENTIF_JBT,0)) as ADJUSTMENT_INSENTIF_JBT,
									(sum(coalesce(a.TGC,0))) as jumlah,
									(select (sum(coalesce(a.TGC,0))) as total from t_payslip a WHERE a.BULAN = :bulan AND a.TAHUN = :tahun) as total,
									((sum(coalesce(a.TGC,0)))/(select (sum(coalesce(a.TGC,0))) as total from t_payslip a WHERE a.BULAN = :bulan AND a.TAHUN = :tahun)) as prosentase,
									(select :created_by) as CREATED_BY,
									(select :created_time) as CREATED_TIME
								FROM
									`t_payslip` a
								JOIN m_hr_current f ON a.NIK = f.NIK AND a.BULAN = f.BULAN AND a.TAHUN = f.TAHUN
								JOIN m_mapping_atk b ON a.NIK = b.NIK AND a.BULAN = b.BULAN AND a.TAHUN = b.TAHUN
								JOIN m_segmentasi c ON b.SEGMENTASI_KODE = c.KODE
								WHERE a.BULAN = :bulan AND a.TAHUN = :tahun
								GROUP BY f.KODE_DIVISI
								ORDER BY f.KODE_DIVISI;";
					$sql2 = Yii::app()->db->createCommand($query2);
					$sql2->bindParam(':created_by',$created_by);
					$sql2->bindParam(':created_time',$created_time);
					$sql2->bindParam(':bulan',$bulan);
					$sql2->bindParam(':tahun',$tahun);
					$sql2->query();					

					$del3 = "DELETE FROM t_pros_witel WHERE TAHUN = :tahun AND BULAN = :bulan";
					$sqldel3 = Yii::app()->db->createCommand($del3);
					$sqldel3->bindParam(':bulan',$bulan);
					$sqldel3->bindParam(':tahun',$tahun);
					$sqldel3->query();			

					$query3 = "INSERT INTO t_pros_witel (TAHUN,BULAN,KODE_DIVISI,DIVISI,C_HOST,JUMLAH,TOTAL,PROSENTASE,CREATED_BY,CREATED_TIME)
								SELECT
									a.TAHUN,
									a.BULAN,
									f.KODE_DIVISI,
									f.DIVISI,
									f.C_HOST,
									-- sum(coalesce(a.BASE_SALARY,0)) as BASE_SALARY,
									-- sum(coalesce(a.BANTUAN_KEMAHALAN,0)) as BANTUAN_KEMAHALAN,
									-- sum(coalesce(a.INSENTIF_PREMIUM,0)) as INSENTIF_PREMIUM,
									-- sum(coalesce(a.INSENTIF_JABATAN,0)) as INSENTIF_JABATAN,
									-- sum(coalesce(a.ADJUSTMENT_INSENTIF_JBT,0)) as ADJUSTMENT_INSENTIF_JBT,
									(sum(coalesce(a.TGC,0))) as jumlah,
									(select 
										(sum(coalesce(ax.TGC,0))) as total
										from t_payslip ax
										JOIN m_hr_current x ON x.NIK = ax.NIK AND x.BULAN = ax.BULAN AND x.TAHUN =ax.TAHUN
										WHERE x.KODE_DIVISI = f.KODE_DIVISI AND ax.BULAN = :bulan AND ax.TAHUN = :tahun
										GROUP BY x.KODE_DIVISI) as TOTAL,
									((sum(coalesce(a.TGC,0)))/(select 
										(sum(coalesce(ax.TGC,0))) as total
										from t_payslip ax
										JOIN m_hr_current x ON x.NIK = ax.NIK AND ax.BULAN = x.BULAN AND ax.TAHUN = x.TAHUN
										WHERE x.KODE_DIVISI = f.KODE_DIVISI AND ax.BULAN = :bulan AND ax.TAHUN = :tahun
										GROUP BY x.KODE_DIVISI)) as prosentase,
									(select :created_by) as CREATED_BY,
									(select :created_time) as CREATED_TIME
								FROM
									`t_payslip` a
								JOIN m_hr_current f ON a.NIK = f.NIK AND a.BULAN = f.BULAN AND a.TAHUN = f.TAHUN
								JOIN m_mapping_atk b ON a.NIK = b.NIK AND a.BULAN = b.BULAN AND a.TAHUN = b.TAHUN
								JOIN m_segmentasi c ON b.SEGMENTASI_KODE = c.KODE
								WHERE c.TREG_NTREG = 'TREG' AND a.BULAN = :bulan AND a.TAHUN = :tahun
								GROUP BY f.KODE_DIVISI,f.C_HOST
								;";
					$sql3 = Yii::app()->db->createCommand($query3);
					$sql3->bindParam(':created_by',$created_by);
					$sql3->bindParam(':created_time',$created_time);
					$sql3->bindParam(':bulan',$bulan);
					$sql3->bindParam(':tahun',$tahun);
					$sql3->query();

					$query4 = "INSERT INTO t_pros_witel_form (BULAN,TAHUN,KODE_DIVISI,C_HOST,K_SEG,TOTAL,CREATED_BY,CREATED_TIME) 
								SELECT 
								py.BULAN,
								py.TAHUN,
								cr.KODE_DIVISI, 
								cr.C_HOST,
								seg.K_SEG,
								(SUM(py.TGC)) TOTAL,
								(select :created_by) as CREATED_BY,
								(select :created_time) as CREATED_TIME
								FROM t_payslip py 
								JOIN m_hr_current cr ON cr.NIK = py.NIK AND cr.BULAN = py.BULAN AND cr.TAHUN = py.TAHUN
								JOIN m_mapping_atk mst ON mst.NIK = cr.NIK AND cr.BULAN = mst.BULAN AND cr.TAHUN = mst.TAHUN 
								JOIN m_segmentasi seg ON seg.KODE = mst.SEGMENTASI_KODE
								WHERE cr.KODE_DIVISI IN('DIV-TR1','DIV-TR2','DIV-TR3','DIV-TR4','DIV-TR5','DIV-TR6','DIV-TR7')
								AND py.BULAN=:bulan AND py.TAHUN=:tahun
								GROUP BY cr.C_HOST,seg.K_SEG,py.TAHUN,py.BULAN";
					$sql4 = Yii::app()->db->createCommand($query4);
					$sql4->bindParam(':created_by',$created_by);
					$sql4->bindParam(':created_time',$created_time);
					$sql4->bindParam(':bulan',$bulan);
					$sql4->bindParam(':tahun',$tahun);
					$sql4->query();
					
					$namatbl1 = 'm_hr_current_'.$bulan.'_'.$tahun;
					$cektab1 = Yii::app()->db->createCommand("SHOW TABLES LIKE :par")
												->bindParam(':par',$namatbl1)
												->queryAll();
					if (count($cektab1) < 1) {
						$indbulan='index_bulan_'.$namatbl1;
						$indtahun='index_tahun_'.$namatbl1;
						$indkddiv='index_kddiv_'.$namatbl1;
						$indhost='index_host_'.$namatbl1;
						$indnik='index_nik_'.$namatbl1;
						$indobjpos='index_objpos_'.$namatbl1;
						$querybckp1 = "CREATE TABLE ".$namatbl1." (
										`ID`  int NOT NULL AUTO_INCREMENT ,
										`NIK`  varchar(9) NOT NULL ,
										`NAMA`  varchar(70) NULL ,
										`PERSA`  varchar(10) NULL ,
										`SUBAREA`  varchar(10) NULL ,
										`BAND`  varchar(3) NULL ,
										`NAMA_POSISI`  varchar(255) NULL ,
										`NAMA_UNIT`  varchar(255) NULL ,
										`C_HOST`  varchar(10) NULL ,
										`KODE_LOKER`  varchar(255) NULL ,
										`KODE_DIVISI`  varchar(255) NULL ,
										`DIVISI`  varchar(255) NULL ,
										`OBJID_POSISI`  varchar(255) NULL ,
										`BULAN`  int(2) NULL ,
										`TAHUN`  int(4) NULL ,
										`CREATED_BY`  varchar(255) NULL ,
										`CREATED_TIME`  datetime NULL ,
										PRIMARY KEY (`ID`),
										INDEX ".$indbulan." (`BULAN`) USING BTREE ,
										INDEX ".$indtahun." (`TAHUN`) USING BTREE ,
										INDEX ".$indkddiv." (`KODE_DIVISI`) USING BTREE, 
										INDEX ".$indhost." (`C_HOST`) USING BTREE, 
										INDEX ".$indnik." (`NIK`) USING BTREE, 
										INDEX ".$indobjpos." (`OBJID_POSISI`) USING BTREE 
										)";

						$sqlbckp1 = Yii::app()->db->createCommand($querybckp1);
						$sqlbckp1->query();
					}else{
						$trunctab1 = Yii::app()->db->createCommand("TRUNCATE TABLE  ".$namatbl1.";")->query();
					}

					$querymove1 = "INSERT INTO ".$namatbl1." (NIK,NAMA,PERSA,SUBAREA,BAND,NAMA_POSISI,NAMA_UNIT,C_HOST,KODE_LOKER,KODE_DIVISI,DIVISI,OBJID_POSISI,BULAN,TAHUN,CREATED_BY,CREATED_TIME) 
						SELECT NIK,NAMA,PERSA,SUBAREA,BAND,NAMA_POSISI,NAMA_UNIT,C_HOST,KODE_LOKER,KODE_DIVISI,DIVISI,OBJID_POSISI,BULAN,TAHUN,CREATED_BY,CREATED_TIME
						FROM m_hr_current py WHERE py.BULAN=:bulan AND py.TAHUN=:tahun";
					$sqlmove1 = Yii::app()->db->createCommand($querymove1);
					$sqlmove1->bindParam(':bulan',$bulan);
					$sqlmove1->bindParam(':tahun',$tahun);
					$sqlmove1->query();

					$querydelmove1 = "DELETE FROM m_hr_current WHERE BULAN=:bulan AND TAHUN=:tahun";
					$sqldelmove1 = Yii::app()->db->createCommand($querydelmove1);
					$sqldelmove1->bindParam(':bulan',$bulan);
					$sqldelmove1->bindParam(':tahun',$tahun);
					$sqldelmove1->query();

					$namatbl2 = 't_payslip_'.$bulan.'_'.$tahun;
					$cektab2 = Yii::app()->db->createCommand("SHOW TABLES LIKE :par")
												->bindParam(':par',$namatbl2)
												->queryAll();
					if (count($cektab2) < 1) {
						$indbulan='index_bulan_'.$namatbl2;
						$indtahun='index_tahun_'.$namatbl2;
						$indnik='index_nik_'.$namatbl2;
						$querybckp2 = "CREATE TABLE `".$namatbl2."` (
										`ID`  int NOT NULL AUTO_INCREMENT ,
										`NIK`  varchar(255) NULL ,
										`NAMA`  varchar(255) NULL ,
										`BULAN`  int(2) NULL ,
										`TAHUN`  int(4) NULL ,
										`TGC`  decimal(60,30) NULL ,
										`CREATED_BY`  varchar(255) NULL ,
										`CREATED_TIME`  datetime NULL ,
										PRIMARY KEY (`ID`),
										INDEX ".$indbulan." (`BULAN`) USING BTREE ,
										INDEX ".$indtahun." (`TAHUN`) USING BTREE ,
										INDEX ".$indnik." (`NIK`) USING BTREE 
										)";
						$sqlbckp2 = Yii::app()->db->createCommand($querybckp2);
						$sqlbckp2->query();
					}else{
						$trunctab2 = Yii::app()->db->createCommand("TRUNCATE TABLE  ".$namatbl2.";")->query();
					}							
					$querymove2 = "INSERT INTO ".$namatbl2." (NIK,NAMA,BULAN,TAHUN,TGC,CREATED_BY,CREATED_TIME) 
						SELECT NIK,NAMA,BULAN,TAHUN,TGC,CREATED_BY,CREATED_TIME
						FROM t_payslip py WHERE py.BULAN=:bulan AND py.TAHUN=:tahun";
					$sqlmove2 = Yii::app()->db->createCommand($querymove2);
					$sqlmove2->bindParam(':bulan',$bulan);
					$sqlmove2->bindParam(':tahun',$tahun);
					$sqlmove2->query();

					$querydelmove2 = "DELETE FROM t_payslip WHERE BULAN=:bulan AND TAHUN=:tahun";
					$sqldelmove2 = Yii::app()->db->createCommand($querydelmove2);
					$sqldelmove2->bindParam(':bulan',$bulan);
					$sqldelmove2->bindParam(':tahun',$tahun);
					$sqldelmove2->query();


					$namatbl3 = 'm_mapping_atk_'.$bulan.'_'.$tahun;
					$cektab3 = Yii::app()->db->createCommand("SHOW TABLES LIKE :par")
												->bindParam(':par',$namatbl3)
												->queryAll();
					if (count($cektab3) < 1) {
						$indbulan='index_bulan_'.$namatbl3;
						$indtahun='index_tahun_'.$namatbl3;
						$indnik='index_nik_'.$namatbl3;
						$indseg='index_seg_'.$namatbl3;
						$querybckp3 = "CREATE TABLE `".$namatbl3."` (
										`ID`  int NOT NULL AUTO_INCREMENT ,
										`NIK`  int(11) NULL ,
										`SEGMENTASI_KODE`  varchar(255) NULL ,
										`BULAN`  int(2) NULL ,
										`TAHUN`  int(4) NULL ,
										`CREATED_BY`  varchar(255) NULL ,
										`CREATED_TIME`  datetime NULL ,
										PRIMARY KEY (`ID`),
										INDEX ".$indbulan." (`BULAN`) USING BTREE ,
										INDEX ".$indtahun." (`TAHUN`) USING BTREE ,
										INDEX ".$indnik." (`NIK`) USING BTREE,
										INDEX ".$indseg." (`SEGMENTASI_KODE`) USING BTREE
										)";
						$sqlbckp3 = Yii::app()->db->createCommand($querybckp3);
						$sqlbckp3->query();
					}else{
						$trunctab3 = Yii::app()->db->createCommand("TRUNCATE TABLE  ".$namatbl3.";")->query();
					}							
					$querymove3 = "INSERT INTO ".$namatbl3." (NIK,SEGMENTASI_KODE,BULAN,TAHUN,CREATED_BY,CREATED_TIME) 
						SELECT NIK,SEGMENTASI_KODE,BULAN,TAHUN,CREATED_BY,CREATED_TIME
						FROM m_mapping_atk py WHERE py.BULAN=:bulan AND py.TAHUN=:tahun";
					$sqlmove3 = Yii::app()->db->createCommand($querymove3);
					$sqlmove3->bindParam(':bulan',$bulan);
					$sqlmove3->bindParam(':tahun',$tahun);
					$sqlmove3->query();

					$querydelmove3 = "DELETE FROM m_mapping_atk WHERE BULAN=:bulan AND TAHUN=:tahun";
					$sqldelmove3 = Yii::app()->db->createCommand($querydelmove3);
					$sqldelmove3->bindParam(':bulan',$bulan);
					$sqldelmove3->bindParam(':tahun',$tahun);
					$sqldelmove3->query();

					$transaction->commit();
					Yii::app()->user->setFlash('success', "Data telah diproses report telah siap");
					$this->redirect(array('index'));
				}
				catch(Exception $e){
				    $transaction->rollBack();
				    throw new CHttpException(null,"ERROR: ".$e->getMessage());
				}
			}
		}
	}

	public function actionUpload($tahun=null,$bulan=null)
	{
		$model = new TPayslip;		
		$model->unsetAttributes();
		if (isset($_POST['TPayslip'])) {
			$model->attributes = $_POST['TPayslip'];
			$model->scenario ='upload_xls';
			$upload = CUploadedFile::getInstance($model, 'fileupload');
			if ($upload){
                $upload_dir = Yii::getPathOfAlias('upload_dir');
                if (!is_dir($upload_dir)) {
                    mkdir($upload_dir);
                    chmod($upload_dir, 0777);
                }

                $_peserta = $upload_dir . '/fileupload';
                $nama_file = str_replace(".", "", microtime(true)) . "-" . $upload->name;
                if(!is_dir($_peserta)){
                    mkdir($_peserta);
                    chmod($_peserta, 0777);
                    $upload->saveAs($_peserta . "/" . $nama_file);
                }else $upload->saveAs($_peserta . "/" . $nama_file);
				$bulan = $model->BULAN;
				$tahun = $model->TAHUN;
				$dirfile = $_peserta . "/" . $nama_file;

				$transaction = Yii::app()->db->beginTransaction();
				try{
	                $querydel = "DELETE FROM t_payslip WHERE BULAN = :bulan AND TAHUN = :tahun";
	                $sqldel = Yii::app()->db->createCommand($querydel);
	                $sqldel->bindParam(':bulan',$bulan);
					$sqldel->bindParam(':tahun',$tahun);
					$par1 = $sqldel->query();

					$del1 = "DELETE FROM t_pros_cfu_fu  WHERE BULAN = :bulan AND TAHUN = :tahun";
					$sqldel1 = Yii::app()->db->createCommand($del1);
					$sqldel1->bindParam(':bulan',$bulan);
					$sqldel1->bindParam(':tahun',$tahun);
					$sqldel1->query();

					$del2 = "DELETE FROM t_pros_divisi WHERE TAHUN = :tahun AND BULAN = :bulan";
					$sqldel2 = Yii::app()->db->createCommand($del2);
					$sqldel2->bindParam(':bulan',$bulan);
					$sqldel2->bindParam(':tahun',$tahun);
					$sqldel2->query();

					$del3 = "DELETE FROM t_pros_witel WHERE TAHUN = :tahun AND BULAN = :bulan";
					$sqldel3 = Yii::app()->db->createCommand($del3);
					$sqldel3->bindParam(':bulan',$bulan);
					$sqldel3->bindParam(':tahun',$tahun);
					$sqldel3->query();

					$del6 = "DELETE FROM t_pros_witel_form WHERE BULAN = :bulan AND TAHUN = :tahun";
					$sqldel6 = Yii::app()->db->createCommand($del6);
					$sqldel6->bindParam(':bulan',$bulan);
					$sqldel6->bindParam(':tahun',$tahun);
					$sqldel6->query();

	                $var = '"';
	                $query =  "LOAD DATA LOCAL INFILE 
								:file INTO TABLE t_payslip 
								FIELDS TERMINATED BY ';'
								ENCLOSED BY '".$var."'
								LINES TERMINATED BY '\r\n'
								IGNORE 1 rows
								(NIK,NAMA,@tgc)
								SET BULAN = :bulan, TAHUN =:tahun, 
								TGC = REPLACE(@tgc, '.', ''),
								CREATED_BY = :created_by,
								CREATED_TIME = :created_time;";
					$sql = Yii::app()->db->createCommand($query);
					$sql->bindParam(':file',$dirfile);
					$sql->bindParam(':bulan',$bulan);
					$sql->bindParam(':tahun',$tahun);
					$created_by = Yii::app()->user->id;
					$created_time = date("Y-m-d h:i:sa");
					$sql->bindParam(':created_by',$created_by);
					$sql->bindParam(':created_time',$created_time);
					$par2 = $sql->query();
				        
				    $transaction->commit();
				    unlink($dirfile);
					$this->redirect(array('index'));
				}
				catch(Exception $e){
				    $transaction->rollBack();
				    throw new CHttpException(null,"ERROR: ".$e->getMessage());
				}
                // Yii::import('application.vendor.PHPExcel.PHPExcel.IOFactory', true);
                // $inputFileName = $upload_dir . "/fileupload/" . $nama_file;
                // $inputFileType = PHPExcel_IOFactory::identify($inputFileName);
                // $objReader = PHPExcel_IOFactory::createReader($inputFileType);
                // $objPHPExcel = $objReader->load($inputFileName);
                // $return = $objPHPExcel->getActiveSheet()->toArray(null, true,true,true);

                // var_dump($return);exit;

       //          foreach ($return as $key => $value){
       //              if ($key > 2) {
       //                  $criteria = new CDbCriteria;
       //          		$criteria->addCondition("TAHUN = :tahun AND BULAN = :bulan");
       //          		$criteria->params = array(
       //                      ':tahun'=>$model->TAHUN,
       //                      ':bulan'=>$model->BULAN,
       //                  );
       //                  $criteria->compare('NIK', $value['A']);
       //                  $is_ada = TPayslip::model()->find($criteria);
       //                  if ($is_ada){
       //                      $_pesan[] = $value['A'];
       //                  }else{
       //                      $_model = new TPayslip;
       //                      $_model->NIK = $is_ada->NIK;
       //                      $_model->NAMA = $value['B'];
							// $_model->ESGRP_TXT = $value['C'];
							// $_model->PERSA = $value['D'];
							// $_model->SUBAREA = $value['E'];
							// $_model->BAND = $value['F'];
							// $_model->ROLE_CATEGORY = $value['G'];
							// $_model->NAMA_POSISI = $value['H'];
							// $_model->NAMA_UNIT = $value['I'];
							// $_model->BASE_SALARY = $value['J'];
							// $_model->BANTUAN_KEMAHALAN = $value['K'];
							// $_model->TUNJANGAN_PAJAK = $value['L'];
							// $_model->INSENTIF_PREMIUM = $value['M'];
							// $_model->TUNJANGAN_ASKEDIR = $value['N'];
							// $_model->BBP = $value['O'];
							// $_model->ADJUSTMENT_BBP = $value['P'];
							// $_model->UANG_LEMBUR = $value['Q'];
							// $_model->INSENTIF_JABATAN = $value['R'];
							// $_model->ADJUSTMENT_INSENTIF_JBT = $value['S'];
							// $_model->BPJS_KES = $value['T'];
							// $_model->TUNJANGAN_DPLK = $value['U'];
							// $_model->JKK_JAMSOSTEK = $value['V'];
							// $_model->JHT_JAMSOSTEK = $value['W'];
							// $_model->JKM_JAMSOSTEK = $value['X'];
							// $_model->BPJS_JP = $value['Y'];
							// $_model->RAPEL_BPJS_JP = $value['Z'];
							// $_model->POTONGAN_ASKEDIR = $value['AA'];
							// $_model->SIMPANAN_KOPERASI = $value['AB'];
							// $_model->SUMBANGAN_KEMATIAN = $value['AC'];
							// $_model->POTONGAN_TASPEN = $value['AD'];
							// $_model->RAPEL_TASPEN = $value['AE'];
							// $_model->POTONGAN_IDP = $value['AF'];
							// $_model->TABUNGAN_WAJIB_PERUMAHAN = $value['AG'];
							// $_model->ANGSURAN_TETAP_KOPEGTEL = $value['AH'];
							// $_model->ANGSURAN_LAIN_KOPEGTEL = $value['AI'];
							// $_model->POTONGAN_INFAQ = $value['AJ'];
							// $_model->POTONGAN_DANA_PERSEMBAHAN = $value['AK'];
							// $_model->POTONGAN_DANA_PUNIA = $value['AL'];
							// $_model->POTONGAN_ZAKAT = $value['AM'];
							// $_model->POT_TMB_DANA_PERSEMBAHAN = $value['AN'];
							// $_model->POT_TMB_DANA_PUNIA = $value['AO'];
							// $_model->IURAN_ORG_KARYAWAN = $value['AP'];
							// $_model->POT_BAKTI_U_NEGRI = $value['AQ'];
							// $_model->POT_DPLK_EE = $value['AR'];
							// $_model->POT_DPLK_TITIPAN = $value['AS'];
							// $_model->POT_LAIN_LAIN = $value['AT'];
							// $_model->POTAONGAN_CIA_TCASH = $value['AU'];
							// $_model->POT_BPJS_KES = $value['AV'];
							// $_model->POT_JHT_JAMSOSTEK = $value['AW'];
							// $_model->POT_JHT_JAMSOSTEK_EE = $value['AX'];
							// $_model->POT_JKK_JKM_JAMSOSTEK = $value['AY'];
							// $_model->POT_BPJS_JP = $value['AZ'];
							// $_model->RPL_POT_BPJS_JP = $value['BA'];
							// $_model->POT_BPJS_JP_EE = $value['BB'];
							// $_model->ANGSURAN_KOPTEL = $value['BC'];
							// $_model->ANGSURAN_LAIN_KOPTEL = $value['BD'];
							// $_model->BULAN = $is_ada->BULAN;
							// $_model->TAHUN = $is_ada->TAHUN;
       //                      if (!$_model->save()){
       //                          $__pesan = array();
       //                          foreach($_model->getErrors() as $key=>$val){
       //                              $__pesan[] = $key . " : " . implode(", ", $val);
       //                          }
       //                          $pesan[] = 'NIK = ' . $value['A'] . " > "  . implode(",", $__pesan);
       //                      }
       //                  }
       //              }
       //          }
				// unlink($inputFileName);
            }
		}
		$this->render('upload',array(
			'model'=>$model,
			'tahun'=>$tahun,
			'bulan'=>$bulan
		));
	}

	public function actionUploadatk($tahun=null,$bulan=null){
		$model = new MMappingAtkMaster;
		$model->unsetAttributes();
		if (isset($_POST['MMappingAtkMaster'])) {
			$model->attributes = $_POST['MMappingAtkMaster'];
			$model->scenario ='upload_xls';
			$upload = CUploadedFile::getInstance($model, 'fileupload');
			if ($upload){
                $upload_dir = Yii::getPathOfAlias('upload_dir');
                if (!is_dir($upload_dir)) {
                    mkdir($upload_dir);
                    chmod($upload_dir, 0777);
                }

                $_peserta = $upload_dir . '/fileupload';
                $nama_file = str_replace(".", "", microtime(true)) . "-" . $upload->name;
                if(!is_dir($_peserta)){
                    mkdir($_peserta);
                    chmod($_peserta, 0777);
                    $upload->saveAs($_peserta . "/" . $nama_file);
                }else $upload->saveAs($_peserta . "/" . $nama_file);
				$dirfile = $_peserta . "/" . $nama_file;

				$transaction = Yii::app()->db->beginTransaction();
				try{
	                $var = '"';
	                $query =  "LOAD DATA LOCAL INFILE 
								:file INTO TABLE m_mapping_atk_master 
								FIELDS TERMINATED BY ';'
								ENCLOSED BY '".$var."'
								LINES TERMINATED BY '\r\n'
								IGNORE 1 rows
								(@a1,@a2,@a3,@a4,@a5,@a6,@a7,@a8)
								SET OBJID_POSISI = @a3, NAMA_POSISI = @a6,Final_Segment = @a8, KODE_DIVISI=@a4, NAMA_DIVISI = @a5, NAMA_UNIT = @a7;";
					$sql = Yii::app()->db->createCommand($query);
					$sql->bindParam(':file',$dirfile);
					$par2 = $sql->query();

					$query2 = "INSERT INTO m_mapping_atk (NIK,SEGMENTASI_KODE,BULAN,TAHUN,CREATED_BY,CREATED_TIME)
								SELECT cr.NIK,seg.KODE,cr.BULAN, cr.TAHUN,(SELECT :created_by) as CREATED_BY,(SELECT :created_time) as CREATED_TIME
								FROM m_hr_current cr
								LEFT JOIN m_mapping_atk atk ON cr.NIK = atk.NIK AND cr.BULAN = atk.BULAN AND cr.TAHUN = atk.TAHUN 
								LEFT JOIN m_mapping_atk_master ms ON cr.OBJID_POSISI = ms.OBJID_POSISI
								LEFT JOIN m_segmentasi seg ON ms.Final_Segment = seg.FINAL_SEGMENT
								WHERE cr.KODE_DIVISI IN ('DIV-TR1','DIV-TR2','DIV-TR3','DIV-TR4','DIV-TR5','DIV-TR6','DIV-TR7') 
								AND atk.SEGMENTASI_KODE IS NULL 
								AND ms.Final_Segment IS NOT NULL";
					$sql2 = Yii::app()->db->createCommand($query2);
					$created_by = Yii::app()->user->id;
					$created_time = date("Y-m-d h:i:sa");
					$sql2->bindParam(':created_by',$created_by);
					$sql2->bindParam(':created_time',$created_time);
					$sql2->query();
				        
				    $transaction->commit();
				    unlink($dirfile);
					$this->redirect(array('index'));
				}
				catch(Exception $e){
				    $transaction->rollBack();
				    throw new CHttpException(null,"ERROR: ".$e->getMessage());
				}
			}
		}
		$this->render('uploadatk',array(
			'model'=>$model,
			'tahun'=>$tahun,
			'bulan'=>$bulan
		));
	}

	public function actionUploadhrcurrent($tahun=null,$bulan=null){
		$model = new MHrCurrent;		
		$model->unsetAttributes();
		if (isset($_POST['MHrCurrent'])) {
			$model->attributes = $_POST['MHrCurrent'];
			$model->scenario ='upload_xls';
			$upload = CUploadedFile::getInstance($model, 'fileupload');
			if ($upload){
                $upload_dir = Yii::getPathOfAlias('upload_dir');
                if (!is_dir($upload_dir)) {
                    mkdir($upload_dir);
                    chmod($upload_dir, 0777);
                }

                $_peserta = $upload_dir . '/fileupload';
                $nama_file = str_replace(".", "", microtime(true)) . "-" . $upload->name;
                if(!is_dir($_peserta)){
                    mkdir($_peserta);
                    chmod($_peserta, 0777);
                    $upload->saveAs($_peserta . "/" . $nama_file);
                }else $upload->saveAs($_peserta . "/" . $nama_file);
				$bulan = $model->BULAN;
				$tahun = $model->TAHUN;
				$dirfile = $_peserta . "/" . $nama_file;

				$transaction = Yii::app()->db->beginTransaction();
				try{
	                $querydel = "DELETE FROM m_hr_current WHERE BULAN = :bulan AND TAHUN = :tahun";
	                $sqldel = Yii::app()->db->createCommand($querydel);
	                $sqldel->bindParam(':bulan',$bulan);
					$sqldel->bindParam(':tahun',$tahun);
					$par1 = $sqldel->query();

					$del1 = "DELETE FROM t_pros_cfu_fu  WHERE BULAN = :bulan AND TAHUN = :tahun";
					$sqldel1 = Yii::app()->db->createCommand($del1);
					$sqldel1->bindParam(':bulan',$bulan);
					$sqldel1->bindParam(':tahun',$tahun);
					$sqldel1->query();

					$del2 = "DELETE FROM t_pros_divisi WHERE TAHUN = :tahun AND BULAN = :bulan";
					$sqldel2 = Yii::app()->db->createCommand($del2);
					$sqldel2->bindParam(':bulan',$bulan);
					$sqldel2->bindParam(':tahun',$tahun);
					$sqldel2->query();

					$del3 = "DELETE FROM t_pros_witel WHERE TAHUN = :tahun AND BULAN = :bulan";
					$sqldel3 = Yii::app()->db->createCommand($del3);
					$sqldel3->bindParam(':bulan',$bulan);
					$sqldel3->bindParam(':tahun',$tahun);
					$sqldel3->query();

					$del4 = "DELETE atk FROM m_mapping_atk atk
								JOIN m_hr_current hr ON atk.NIK = hr.NIK AND atk.BULAN = hr.BULAN AND atk.TAHUN = hr.TAHUN
					 			JOIN m_mapping_cfu_fu seg ON seg.KODE_DIVISI = hr.KODE_DIVISI WHERE (hr.KODE_DIVISI <> '' || hr.KODE_DIVISI IS NOT NULL) AND hr.KODE_DIVISI NOT IN ('DIV-TR1','DIV-TR2','DIV-TR3','DIV-TR4','DIV-TR5','DIV-TR6','DIV-TR7') AND hr.BULAN = :bulan AND hr.TAHUN = :tahun";
					$sqldel4 = Yii::app()->db->createCommand($del4);
					$sqldel4->bindParam(':bulan',$bulan);
					$sqldel4->bindParam(':tahun',$tahun);
					$sqldel4->query();

					$del5 = "DELETE atk FROM m_mapping_atk atk
								JOIN m_hr_current hr ON atk.NIK = hr.NIK AND atk.BULAN = hr.BULAN AND atk.TAHUN = hr.TAHUN
					 			WHERE hr.KODE_DIVISI IN ('DIV-TR1','DIV-TR2','DIV-TR3','DIV-TR4','DIV-TR5','DIV-TR6','DIV-TR7') 
					 			AND hr.BULAN = :bulan AND hr.TAHUN = :tahun";
					$sqldel5 = Yii::app()->db->createCommand($del5);
					$sqldel5->bindParam(':bulan',$bulan);
					$sqldel5->bindParam(':tahun',$tahun);
					$sqldel5->query();

					$delatk = "DELETE FROM m_mapping_atk WHERE BULAN=:bulan AND TAHUN=:tahun";
					$sqldelatk = Yii::app()->db->createCommand($delatk);
					$sqldelatk->bindParam(':bulan',$bulan);
					$sqldelatk->bindParam(':tahun',$tahun);
					$sqldelatk->query();

					$del6 = "DELETE FROM t_pros_witel_form WHERE BULAN = :bulan AND TAHUN = :tahun";
					$sqldel6 = Yii::app()->db->createCommand($del6);
					$sqldel6->bindParam(':bulan',$bulan);
					$sqldel6->bindParam(':tahun',$tahun);
					$sqldel6->query();

	                $var = '"';
	                $query =  "LOAD DATA LOCAL INFILE 
								:file INTO TABLE m_hr_current 
								FIELDS TERMINATED BY ';'
								ENCLOSED BY '".$var."'
								LINES TERMINATED BY '\r\n'
								IGNORE 2 rows
								(@ax1,@ax2,@a1,	@a2,	@a3,	@a4,	@a5,	@a6,	@a7,	@a8,	@a9,	@a10,	@a11,	@a12,	@a13,	@a14,	@a15,	@a16,	@a17,	@a18,	@a19,	@a20,	@a21,	@a22,	@a23,	@a24,	@a25,	@a26,	@a27,	@a28,	@a29,	@a30,	@a31,	@a32,	@a33,	@a34,	@a35,	@a36,	@a37,	@a38,	@a39,	@a40,	@a41,	@a42,	@a43,	@a44,	@a45,	@a46,	@a47,	@a48,	@a49,	@a50,	@a51,	@a52,	@a53,	@a54,	@a55,	@a56,	@a57,	@a58,	@a59,	@a60,	@a61,	@a62,	@a63,	@a64,	@a65,	@a66,	@a67,	@a68,	@a69,	@a70,	@a71,	@a72,	@a73,	@a74,	@a75,	@a76,	@a77,	@a78,	@a79,	@a80,	@a81,	@a82,	@a83,	@a84,	@a85,	@a86,	@a87,	@a88,	@a89,	@a90,	@a91,	@a92,	@a93,	@a94,	@a95,	@a96,	@a97,	@a98,	@a99,	@a100,	@a101,	@a102,	@a103,	@a104,	@a105,	@a106,	@a107,	@a108,	@a109,	@a110,	@a111)
								SET BULAN = :bulan, TAHUN = :tahun,NIK = @a1,NAMA = @a2,PERSA = @a47,SUBAREA = @a49,BAND = @a84,NAMA_POSISI = @a27,NAMA_UNIT = @a23,C_HOST = @a92,KODE_LOKER = @a22,KODE_DIVISI = @a19,DIVISI = @a20,OBJID_POSISI = @a25,
									CREATED_BY = :created_by, CREATED_TIME = :created_time
								;";
					$sql = Yii::app()->db->createCommand($query);
					$created_by = Yii::app()->user->id;
					$created_time = date("Y-m-d h:i:sa");
					$sql->bindParam(':created_by',$created_by);
					$sql->bindParam(':created_time',$created_time);
					$sql->bindParam(':file',$dirfile);
					$sql->bindParam(':bulan',$bulan);
					$sql->bindParam(':tahun',$tahun);
					$par2 = $sql->query();

					$querydelno = "DELETE FROM `m_hr_current` WHERE (NIK = '' OR NIK IS NULL); ";
					$sqldelno = Yii::app()->db->createCommand($querydelno);
					$sqldelno->query();

					$querymapn = "INSERT INTO m_mapping_atk (NIK,SEGMENTASI_KODE,BULAN,TAHUN,CREATED_BY,CREATED_TIME)
									SELECT hr.NIK,seg.KODE_SEGMENTASI,hr.BULAN,hr.TAHUN,(select :created_by) as CREATED_BY,(select :created_time) as CREATED_TIME FROM m_hr_current hr 
									JOIN m_mapping_cfu_fu seg ON seg.KODE_DIVISI = hr.KODE_DIVISI WHERE (hr.KODE_DIVISI <> '' || hr.KODE_DIVISI IS NOT NULL) AND hr.KODE_DIVISI NOT IN ('DIV-TR1','DIV-TR2','DIV-TR3','DIV-TR4','DIV-TR5','DIV-TR6','DIV-TR7') AND hr.BULAN = :bulan AND hr.TAHUN = :tahun;"; 
					$sqlmapn = Yii::app()->db->createCommand($querymapn);
					$sqlmapn->bindParam(':created_by',$created_by);
					$sqlmapn->bindParam(':created_time',$created_time);
					$sqlmapn->bindParam(':bulan',$bulan);
					$sqlmapn->bindParam(':tahun',$tahun);
					$sqlmapn->query();

					$querymapt = "INSERT INTO m_mapping_atk (NIK,SEGMENTASI_KODE,BULAN,TAHUN,CREATED_BY,CREATED_TIME)
									SELECT cr.NIK, seg.KODE, cr.BULAN, cr.TAHUN,(select :created_by) as CREATED_BY,(select :created_time) as CREATED_TIME  FROM m_hr_current cr
									JOIN m_mapping_atk_master mas ON mas.OBJID_POSISI = cr.OBJID_POSISI
									JOIN m_segmentasi seg ON seg.FINAL_SEGMENT = mas.Final_Segment
									WHERE cr.BULAN = :bulan AND cr.TAHUN = :tahun 
									AND cr.KODE_DIVISI IN ('DIV-TR1','DIV-TR2','DIV-TR3','DIV-TR4','DIV-TR5','DIV-TR6','DIV-TR7')";
					$sqlmapt = Yii::app()->db->createCommand($querymapt);
					$sqlmapt->bindParam(':created_by',$created_by);
					$sqlmapt->bindParam(':created_time',$created_time);
					$sqlmapt->bindParam(':bulan',$bulan);
					$sqlmapt->bindParam(':tahun',$tahun);
					$sqlmapt->query();
				        
				    $transaction->commit();
				    unlink($dirfile);
					$this->redirect(array('index'));
				}
				catch(Exception $e){
				    $transaction->rollBack();
				    throw new CHttpException(null,"ERROR: ".$e->getMessage());
				}
			}
		}
		$this->render('uploadhrcurrent',array(
			'model'=>$model,
			'tahun'=>$tahun,
			'bulan'=>$bulan
		));
	}

	public function actionInputall()
	{	
		$realsap = new TRealsap;
		$hrcur = new MHrCurrent;
		$payslip = new TPayslip;
		if(isset($_POST['TRealsap']))
		{
			$ccfu = TProsCfuFu::model()->findAllByAttributes(array('TAHUN'=>$_POST['TRealsap']['TAHUN'],'BULAN'=>$_POST['TRealsap']['BULAN']));
			$cdiv = TProsDivisi::model()->findAllByAttributes(array('TAHUN'=>$_POST['TRealsap']['TAHUN'],'BULAN'=>$_POST['TRealsap']['BULAN']));
			$cwitel = TProsWitel::model()->findAllByAttributes(array('TAHUN'=>$_POST['TRealsap']['TAHUN'],'BULAN'=>$_POST['TRealsap']['BULAN']));
			$cwitel2 = TProsWitelForm::model()->findAllByAttributes(array('TAHUN'=>$_POST['TRealsap']['TAHUN'],'BULAN'=>$_POST['TRealsap']['BULAN']));

			$chrex = MHrCurrent::model()->findAllByAttributes(array('TAHUN'=>$_POST['TRealsap']['TAHUN'],'BULAN'=>$_POST['TRealsap']['BULAN']));
			$cpyex = TPayslip::model()->findAllByAttributes(array('TAHUN'=>$_POST['TRealsap']['TAHUN'],'BULAN'=>$_POST['TRealsap']['BULAN']));
			$crealsapex = TRealsap::model()->findAllByAttributes(array('TAHUN'=>$_POST['TRealsap']['TAHUN'],'BULAN'=>$_POST['TRealsap']['BULAN']));

			if (count($ccfu)>0 || count($cdiv)>0 || count($cwitel)>0 || count($cwitel2)>0 || count($chrex)>0 || count($cpyex)>0 || count($crealsapex)>0) {
				Yii::app()->user->setFlash('danger', "Data di bulan ".$_POST['TRealsap']['BULAN']." ".$_POST['TRealsap']['TAHUN']." sudah tersedia");
				$this->redirect(array('inputall'));
			}

			$realsap->attributes=$_POST['TRealsap'];
			$hrcur->scenario ='upload_xls';
			$payslip->scenario ='upload_xls';
			$realsap->REALISASI = str_replace('.', '', $_POST['TRealsap']['REALISASI']);
			$hrcur->BULAN = $_POST['TRealsap']['BULAN'];
			$hrcur->TAHUN = $_POST['TRealsap']['TAHUN'];
			$payslip->BULAN = $_POST['TRealsap']['BULAN'];
			$payslip->TAHUN = $_POST['TRealsap']['TAHUN'];
			$upload1 = CUploadedFile::getInstance($hrcur, 'fileupload');
			$upload2 = CUploadedFile::getInstance($payslip, 'fileupload');
			if ($upload1 && $upload2){
                $upload_dir1 = Yii::getPathOfAlias('upload_dir');
                if (!is_dir($upload_dir1)) {
                    mkdir($upload_dir1);
                    chmod($upload_dir1, 0777);
                }

                $_peserta1 = $upload_dir1 . '/fileupload';
                $nama_file1 = str_replace(".", "", microtime(true)) . "-" . $upload1->name;
                $nama_file2 = str_replace(".", "", microtime(true)) . "-" . $upload2->name;
                
                if(!is_dir($_peserta1)){
                    mkdir($_peserta1);
                    chmod($_peserta1, 0777);
                    $upload1->saveAs($_peserta1 . "/" . $nama_file1);
                    $upload2->saveAs($_peserta1 . "/" . $nama_file2);
                }else {
                	$upload1->saveAs($_peserta1 . "/" . $nama_file1);
                	$upload2->saveAs($_peserta1 . "/" . $nama_file2);
                }
				
				$bulan = $_POST['TRealsap']['BULAN'];
				$tahun = $_POST['TRealsap']['TAHUN'];
				$dirfile1 = $_peserta1 . "/" . $nama_file1;
				$dirfile2 = $_peserta1 . "/" . $nama_file2;
				$transaction = Yii::app()->db->beginTransaction();
				try{
					//start save real sap
					$realsap->save();
					$created_by = Yii::app()->user->id;
					$created_time = date("Y-m-d h:i:sa");
					//end save real sap

					//start save payslip
					$querydel = "DELETE FROM t_payslip WHERE BULAN = :bulan AND TAHUN = :tahun";
	                $sqldel = Yii::app()->db->createCommand($querydel);
	                $sqldel->bindParam(':bulan',$bulan);
					$sqldel->bindParam(':tahun',$tahun);
					$par1 = $sqldel->query();

					$querydel = "DELETE FROM m_hr_current WHERE BULAN = :bulan AND TAHUN = :tahun";
	                $sqldel = Yii::app()->db->createCommand($querydel);
	                $sqldel->bindParam(':bulan',$bulan);
					$sqldel->bindParam(':tahun',$tahun);
					$par1 = $sqldel->query();

					$del1 = "DELETE FROM t_pros_cfu_fu  WHERE BULAN = :bulan AND TAHUN = :tahun";
					$sqldel1 = Yii::app()->db->createCommand($del1);
					$sqldel1->bindParam(':bulan',$bulan);
					$sqldel1->bindParam(':tahun',$tahun);
					$sqldel1->query();

					$del2 = "DELETE FROM t_pros_divisi WHERE TAHUN = :tahun AND BULAN = :bulan";
					$sqldel2 = Yii::app()->db->createCommand($del2);
					$sqldel2->bindParam(':bulan',$bulan);
					$sqldel2->bindParam(':tahun',$tahun);
					$sqldel2->query();

					$del3 = "DELETE FROM t_pros_witel WHERE TAHUN = :tahun AND BULAN = :bulan";
					$sqldel3 = Yii::app()->db->createCommand($del3);
					$sqldel3->bindParam(':bulan',$bulan);
					$sqldel3->bindParam(':tahun',$tahun);
					$sqldel3->query();

					$del4 = "DELETE atk FROM m_mapping_atk atk
								JOIN m_hr_current hr ON atk.NIK = hr.NIK AND atk.BULAN = hr.BULAN AND atk.TAHUN = hr.TAHUN
					 			JOIN m_mapping_cfu_fu seg ON seg.KODE_DIVISI = hr.KODE_DIVISI WHERE (hr.KODE_DIVISI <> '' || hr.KODE_DIVISI IS NOT NULL) AND hr.KODE_DIVISI NOT IN ('DIV-TR1','DIV-TR2','DIV-TR3','DIV-TR4','DIV-TR5','DIV-TR6','DIV-TR7') AND hr.BULAN = :bulan AND hr.TAHUN = :tahun";
					$sqldel4 = Yii::app()->db->createCommand($del4);
					$sqldel4->bindParam(':bulan',$bulan);
					$sqldel4->bindParam(':tahun',$tahun);
					$sqldel4->query();

					$del5 = "DELETE atk FROM m_mapping_atk atk
								JOIN m_hr_current hr ON atk.NIK = hr.NIK AND atk.BULAN = hr.BULAN AND atk.TAHUN = hr.TAHUN
					 			WHERE hr.KODE_DIVISI IN ('DIV-TR1','DIV-TR2','DIV-TR3','DIV-TR4','DIV-TR5','DIV-TR6','DIV-TR7') 
					 			AND hr.BULAN = :bulan AND hr.TAHUN = :tahun";
					$sqldel5 = Yii::app()->db->createCommand($del5);
					$sqldel5->bindParam(':bulan',$bulan);
					$sqldel5->bindParam(':tahun',$tahun);
					$sqldel5->query();

					$del6 = "DELETE FROM t_pros_witel_form WHERE BULAN = :bulan AND TAHUN = :tahun";
					$sqldel6 = Yii::app()->db->createCommand($del6);
					$sqldel6->bindParam(':bulan',$bulan);
					$sqldel6->bindParam(':tahun',$tahun);
					$sqldel6->query();

	                $var = '"';
	                $query =  "LOAD DATA LOCAL INFILE 
								:file INTO TABLE t_payslip 
								FIELDS TERMINATED BY ';'
								ENCLOSED BY '".$var."'
								LINES TERMINATED BY '\r\n'
								IGNORE 1 rows
								(NIK,NAMA,@tgc)
								SET BULAN = :bulan, TAHUN =:tahun, 
								TGC = REPLACE(@tgc, '.', ''),
								CREATED_BY = :created_by,
								CREATED_TIME = :created_time;";
					$sql = Yii::app()->db->createCommand($query);
					$sql->bindParam(':file',$dirfile2);
					$sql->bindParam(':bulan',$bulan);
					$sql->bindParam(':tahun',$tahun);
					$sql->bindParam(':created_by',$created_by);
					$sql->bindParam(':created_time',$created_time);
					// var_dump($sql);exit();
					$par2 = $sql->query();
					//end save payslip

					//start save hr current


	                $var = '"';
	                $query =  "LOAD DATA LOCAL INFILE 
								:file INTO TABLE m_hr_current 
								FIELDS TERMINATED BY ';'
								ENCLOSED BY '".$var."'
								LINES TERMINATED BY '\r\n'
								IGNORE 2 rows
								(@ax1,@ax2,@a1,	@a2,	@a3,	@a4,	@a5,	@a6,	@a7,	@a8,	@a9,	@a10,	@a11,	@a12,	@a13,	@a14,	@a15,	@a16,	@a17,	@a18,	@a19,	@a20,	@a21,	@a22,	@a23,	@a24,	@a25,	@a26,	@a27,	@a28,	@a29,	@a30,	@a31,	@a32,	@a33,	@a34,	@a35,	@a36,	@a37,	@a38,	@a39,	@a40,	@a41,	@a42,	@a43,	@a44,	@a45,	@a46,	@a47,	@a48,	@a49,	@a50,	@a51,	@a52,	@a53,	@a54,	@a55,	@a56,	@a57,	@a58,	@a59,	@a60,	@a61,	@a62,	@a63,	@a64,	@a65,	@a66,	@a67,	@a68,	@a69,	@a70,	@a71,	@a72,	@a73,	@a74,	@a75,	@a76,	@a77,	@a78,	@a79,	@a80,	@a81,	@a82,	@a83,	@a84,	@a85,	@a86,	@a87,	@a88,	@a89,	@a90,	@a91,	@a92,	@a93,	@a94,	@a95,	@a96,	@a97,	@a98,	@a99,	@a100,	@a101,	@a102,	@a103,	@a104,	@a105,	@a106,	@a107,	@a108,	@a109,	@a110,	@a111)
								SET BULAN = :bulan, TAHUN = :tahun,NIK = @a1,NAMA = @a2,PERSA = @a47,SUBAREA = @a49,BAND = @a84,NAMA_POSISI = @a27,NAMA_UNIT = @a23,C_HOST = @a92,KODE_LOKER = @a22,KODE_DIVISI = @a19,DIVISI = @a20,OBJID_POSISI = @a25,
									CREATED_BY = :created_by, CREATED_TIME = :created_time
								;";
					$sql = Yii::app()->db->createCommand($query);
					$sql->bindParam(':created_by',$created_by);
					$sql->bindParam(':created_time',$created_time);
					$sql->bindParam(':file',$dirfile1);
					$sql->bindParam(':bulan',$bulan);
					$sql->bindParam(':tahun',$tahun);
					$par2 = $sql->query();

					$querydelno = "DELETE FROM `m_hr_current` WHERE (NIK = '' OR NIK IS NULL); ";
					$sqldelno = Yii::app()->db->createCommand($querydelno);
					$sqldelno->query();

					$querymapn = "INSERT INTO m_mapping_atk (NIK,SEGMENTASI_KODE,BULAN,TAHUN,CREATED_BY,CREATED_TIME)
									SELECT hr.NIK,seg.KODE_SEGMENTASI,hr.BULAN,hr.TAHUN,(select :created_by) as CREATED_BY,(select :created_time) as CREATED_TIME FROM m_hr_current hr 
									JOIN m_mapping_cfu_fu seg ON seg.KODE_DIVISI = hr.KODE_DIVISI WHERE (hr.KODE_DIVISI <> '' || hr.KODE_DIVISI IS NOT NULL) AND hr.KODE_DIVISI NOT IN ('DIV-TR1','DIV-TR2','DIV-TR3','DIV-TR4','DIV-TR5','DIV-TR6','DIV-TR7') AND hr.BULAN = :bulan AND hr.TAHUN = :tahun;"; 
					$sqlmapn = Yii::app()->db->createCommand($querymapn);
					$sqlmapn->bindParam(':created_by',$created_by);
					$sqlmapn->bindParam(':created_time',$created_time);
					$sqlmapn->bindParam(':bulan',$bulan);
					$sqlmapn->bindParam(':tahun',$tahun);
					$sqlmapn->query();

					$querymapt = "INSERT INTO m_mapping_atk (NIK,SEGMENTASI_KODE,BULAN,TAHUN,CREATED_BY,CREATED_TIME)
									SELECT cr.NIK, seg.KODE, cr.BULAN, cr.TAHUN,(select :created_by) as CREATED_BY,(select :created_time) as CREATED_TIME  FROM m_hr_current cr
									JOIN m_mapping_atk_master mas ON mas.OBJID_POSISI = cr.OBJID_POSISI
									JOIN m_segmentasi seg ON seg.FINAL_SEGMENT = mas.Final_Segment
									WHERE cr.BULAN = :bulan AND cr.TAHUN = :tahun 
									AND cr.KODE_DIVISI IN ('DIV-TR1','DIV-TR2','DIV-TR3','DIV-TR4','DIV-TR5','DIV-TR6','DIV-TR7')";
					$sqlmapt = Yii::app()->db->createCommand($querymapt);
					$sqlmapt->bindParam(':created_by',$created_by);
					$sqlmapt->bindParam(':created_time',$created_time);
					$sqlmapt->bindParam(':bulan',$bulan);
					$sqlmapt->bindParam(':tahun',$tahun);
					$sqlmapt->query();
				    //end save hr current

				    $transaction->commit();
				    unlink($dirfile1);
				    unlink($dirfile2);
					$this->redirect(array('index'));
				}
				catch(Exception $e){
				    $transaction->rollBack();
				    throw new CHttpException(null,"ERROR: ".$e->getMessage());
				}
			}
			
		}
		$this->render('inputall',array(
			'realsap'=>$realsap,
			'hrcur'=>$hrcur,
			'payslip'=>$payslip
		));
	}

	public function actionReport_old($tahun,$bulan)
	{	
		$query1 = "SELECT 
					tab2.KODE_DIVISI,
					tab2.DIVISI,
					tab2.PERSONEL_COST_DIVISI,
					SUM(tab2.BOH) as BOH,
					SUM(tab2.CON) as CON,
					SUM(tab2.ENT) as ENT,
					SUM(tab2.WIB) as WIB,
					SUM(tab2.NOH) as NOH,
					SUM(tab2.OOH) as OOH,
					SUM(tab2.SOH) as SOH,
					SUM(tab2.PERSONEL_COST_WITEL) as JML_PERSONEL_COST_WITEL, 
					SUM(tab2.TOTAL_ORG) as JML_ORG,
					SUM(tab2.CONSUMER_CFUFU) as CONSUMER_CFUFU,
					SUM(tab2.ENTERPRISE_CFUFU) as ENTERPRISE_CFUFU,
					SUM(tab2.WIB_CFUFU) as WIB_CFUFU,
					SUM(tab2.DIGITAL_CFUFU) as DIGITAL_CFUFU,
					SUM(tab2.MOBILE_CFUFU) as MOBILE_CFUFU,
					SUM(tab2.TOTAL_CFUFU) as JML_CFUFU
					FROM
					(SELECT *,
					(tab1.BOH+tab1.CON+tab1.ENT+tab1.NOH+tab1.OOH+tab1.SOH+tab1.WIB) as TOTAL_ORG,
					(round(tab1.CONSUMER_CFUFU + tab1.ENTERPRISE_CFUFU + tab1.DIGITAL_CFUFU + tab1.MOBILE_CFUFU + tab1.WIB_CFUFU)) as TOTAL_CFUFU, 
					(round(tab1.CONSUMER_CFUFU + tab1.ENTERPRISE_CFUFU + tab1.DIGITAL_CFUFU + tab1.MOBILE_CFUFU + tab1.WIB_CFUFU) - tab1.PERSONEL_COST_WITEL) as SELISIH from 
					(SELECT
					mwt.KODE_DIVISI,
					wt.DIVISI,
					mwt.C_HOST, 
					mwt.WITEL,
					wt.PROSENTASE,
					round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) as PERSONEL_COST_DIVISI,
					round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10) as PERSONEL_COST_WITEL,
					coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'BOH') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) as BOH,
					coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'CON') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) as CON,
					coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'ENT') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) as ENT,
					coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'WIB') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) as WIB,
					coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'NOH') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) as NOH,
					coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'OOH') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) as OOH,
					coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'SOH') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) as SOH,

					(coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'BOH') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) * (100/3/100))+
					(coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'CON') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) * 0.667300380228137) +
					(coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'ENT') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) * 0.667300380228137) +
					(coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'WIB') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) * 0.667300380228137) +
					(coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'NOH') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) * 0.59) +
					(coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'OOH') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) * 0.667300380228137) +
					(coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'SOH') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) * 0.25) as CONSUMER_CFUFU,

					(coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'BOH') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) * (100/3/100))+
					(coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'CON') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) * 0.287072243346008) +
					(coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'ENT') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) * 0.287072243346008) +
					(coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'WIB') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) * 0.287072243346008) +
					(coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'NOH') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) * 0.20) +
					(coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'OOH') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) * 0.287072243346008) +
					(coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'SOH') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) * 0.25) as ENTERPRISE_CFUFU,

					(coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'BOH') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) * (100/3/100))+
					(coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'CON') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) * 0.0456273764258555) +
					(coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'ENT') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) * 0.0456273764258555) +
					(coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'WIB') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) * 0.0456273764258555) +
					(coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'NOH') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) * 0.21) +
					(coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'OOH') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) * 0.0456273764258555) +
					(coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'SOH') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) * 0.25) as WIB_CFUFU,

					0 as DIGITAL_CFUFU,

					(0.25 * coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'SOH') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0)) as MOBILE_CFUFU

					FROM t_pros_witel wt
					JOIN m_witel mwt ON mwt.KODE_DIVISI = wt.KODE_DIVISI AND mwt.C_HOST = wt.C_HOST
					JOIN t_pros_divisi dis ON dis.C_KODE_DIVISI = wt.KODE_DIVISI
					WHERE wt.BULAN=:bulan AND wt.TAHUN=:tahun
					) tab1) tab2 GROUP BY tab2.KODE_DIVISI;"; 
				
		$sql1 = Yii::app()->db->createCommand($query1); 
		$sql1->bindParam(':tahun',$tahun);
		$sql1->bindParam(':bulan',$bulan);
		$datatreg = $sql1->queryAll();
		$tregarray = array();
		for ($i=0; $i < count($datatreg); $i++) { 
			$tregarray[$i] = $datatreg[$i];
			$querywitel = "SELECT *,
							round(tab1.BOH + tab1.CON + tab1.ENT + tab1.WIB + tab1.NOH + tab1.OOH + tab1.SOH) as TOTAL_ORG,
							round(tab1.CONSUMER_CFUFU + tab1.ENTERPRISE_CFUFU + tab1.DIGITAL_CFUFU + tab1.MOBILE_CFUFU + tab1.WIB_CFUFU) as TOTAL_CFUFU, 
							(round(tab1.CONSUMER_CFUFU + tab1.ENTERPRISE_CFUFU + tab1.DIGITAL_CFUFU + tab1.MOBILE_CFUFU + tab1.WIB_CFUFU) - tab1.PERSONEL_COST_WITEL) as SELISIH from 
							(SELECT
							mwt.KODE_DIVISI,
							mwt.C_HOST, 
							mwt.WITEL,
							wt.PROSENTASE,
							round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,0) as PERSONEL_COST_DIVISI,
							round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,0) * wt.PROSENTASE,0) as PERSONEL_COST_WITEL,
							coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'BOH') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) as BOH,
							coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'CON') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) as CON,
							coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'ENT') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) as ENT,
							coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'WIB') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) as WIB,
							coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'NOH') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) as NOH,
							coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'OOH') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) as OOH,
							coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'SOH') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) as SOH,

							(coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'BOH') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) * (100/3/100))+
							(coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'CON') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) * 0.667300380228137) +
							(coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'ENT') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) * 0.667300380228137) +
							(coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'WIB') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) * 0.667300380228137) +
							(coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'NOH') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) * 0.59) +
							(coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'OOH') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) * 0.667300380228137) +
							(coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'SOH') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) * 0.25) as CONSUMER_CFUFU,

							(coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'BOH') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) * (100/3/100))+
							(coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'CON') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) * 0.287072243346008) +
							(coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'ENT') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) * 0.287072243346008) +
							(coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'WIB') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) * 0.287072243346008) +
							(coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'NOH') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) * 0.20) +
							(coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'OOH') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) * 0.287072243346008) +
							(coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'SOH') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) * 0.25) as ENTERPRISE_CFUFU,

							(coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'BOH') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) * (100/3/100))+
							(coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'CON') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) * 0.0456273764258555) +
							(coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'ENT') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) * 0.0456273764258555) +
							(coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'WIB') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) * 0.0456273764258555) +
							(coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'NOH') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) * 0.21) +
							(coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'OOH') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) * 0.0456273764258555) +
							(coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'SOH') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0) * 0.25) as WIB_CFUFU,

							0 as DIGITAL_CFUFU,

							(0.25 * coalesce(round((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = 'TREG' AND seg.SEGMENT = 'SOH') * round(round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) * wt.PROSENTASE,10),10),0)) as MOBILE_CFUFU

							FROM t_pros_witel wt
							JOIN m_witel mwt ON mwt.KODE_DIVISI = wt.KODE_DIVISI AND mwt.C_HOST = wt.C_HOST
							JOIN t_pros_divisi dis ON dis.C_KODE_DIVISI = wt.KODE_DIVISI
							WHERE wt.BULAN=:bulan AND wt.TAHUN=:tahun) tab1 WHERE tab1.KODE_DIVISI = :kodedivisi;";
			$sqlwitel = Yii::app()->db->createCommand($querywitel);
			$sqlwitel->bindParam(':tahun',$tahun);
			$sqlwitel->bindParam(':bulan',$bulan);
			$sqlwitel->bindParam(':kodedivisi',$tregarray[$i]['KODE_DIVISI']);
			$witel = $sqlwitel->queryAll();
			$tregarray[$i]['WITEL'] = $witel;
		}

		// var_dump($tregarray[1]);exit();
	
		$this->render('report',array(
			'tahun'=>$tahun,
			'bulan'=>$bulan,
			'tregarray'=>$tregarray
		));
	}

	public function actionReport($tahun,$bulan)
	{	
		$query1 = "SELECT
					si.C_KODE_DIVISI,
					si.V_SHORT_DIVISI,
					((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * si.PROSENTASE) as PERSONEL_COST_DIVISI,
					(select count(t.ID) from t_payslip_".$bulan."_".$tahun." t JOIN m_hr_current_".$bulan."_".$tahun." m 
								ON t.NIK = m.NIK WHERE m.KODE_DIVISI = si.C_KODE_DIVISI) as JML_ORG  
					FROM t_pros_divisi si
					WHERE si.TAHUN=:tahun AND si.BULAN=:bulan AND 
					si.C_KODE_DIVISI IN ('DIV-TR1','DIV-TR2','DIV-TR3','DIV-TR4','DIV-TR5','DIV-TR6','DIV-TR7')"; 
				
		$sql1 = Yii::app()->db->createCommand($query1); 
		$sql1->bindParam(':tahun',$tahun);
		$sql1->bindParam(':bulan',$bulan);
		$datatreg = $sql1->queryAll();
		$tregarray = array();
		for ($i=0; $i < count($datatreg); $i++) { 
			$tregarray[$i] = $datatreg[$i];
			$querywitel = "SELECT
							sub4.KODE_DIVISI,
							sub4.C_HOST,
							sub4.WITEL,
							sub4.PROSENTASE,
							(select count(t.ID) from t_payslip_".$bulan."_".$tahun." t JOIN m_hr_current_".$bulan."_".$tahun." m 
								ON t.NIK = m.NIK WHERE m.C_HOST = sub4.C_HOST AND m.KODE_DIVISI = sub4.KODE_DIVISI) as JML_ORG,
							-- sub4.PERSONEL_COST_DIVISI,
							(sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) as PERSONEL_COST_WITEL,
							(sub4.CONSUMER*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) as CONSUMER,
							(sub4.ENTERPRISE*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) as ENTERPRISE,
							(sub4.WIB*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) as WIB,
							(sub4.DIGITAL*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) as DIGITAL,
							(sub4.MOBILE*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) as MOBILE,

							(
								(sub4.CONSUMER*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) +
								(sub4.ENTERPRISE*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) +
								(sub4.WIB*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) +
								(sub4.DIGITAL*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) +
								(sub4.MOBILE*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE)
							) as TOTAL

							FROM (SELECT
							sub3.*,
							el.PROSENTASE,
							((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * si.PROSENTASE) as PERSONEL_COST_DIVISI 
							FROM (SELECT 
							sub2.KODE_DIVISI,
							sub2.C_HOST,
							sub2.WITEL,
							(
								COALESCE(sub2.SOH*COALESCE((select sns.PR_CONSUMER from m_prosentasi_segment sns where sns.K_SEG='SOH'),0),0) +
								COALESCE(sub2.NOH*COALESCE((select sns.PR_CONSUMER from m_prosentasi_segment sns where sns.K_SEG='NOH'),0),0) +
								COALESCE(sub2.BOH*COALESCE((select sns.PR_CONSUMER from m_prosentasi_segment sns where sns.K_SEG='BOH'),0),0) +
								COALESCE(sub2.OOH*COALESCE((select sns.PR_CONSUMER from m_prosentasi_segment sns where sns.K_SEG='OOH'),0),0) +
								COALESCE(sub2.CON*COALESCE((select sns.PR_CONSUMER from m_prosentasi_segment sns where sns.K_SEG='CON' AND sns.TREG_NTREG='TREG'),0),0) +
								COALESCE(sub2.ENT*COALESCE((select sns.PR_CONSUMER from m_prosentasi_segment sns where sns.K_SEG='ENT' AND sns.TREG_NTREG='TREG'),0),0) +
								COALESCE(sub2.WIB*COALESCE((select sns.PR_CONSUMER from m_prosentasi_segment sns where sns.K_SEG='WIB' AND sns.TREG_NTREG='TREG'),0),0) 
							) as CONSUMER,
							(
								COALESCE(sub2.SOH*COALESCE((select sns.PR_ENTERPRISE from m_prosentasi_segment sns where sns.K_SEG='SOH'),0),0) +
								COALESCE(sub2.NOH*COALESCE((select sns.PR_ENTERPRISE from m_prosentasi_segment sns where sns.K_SEG='NOH'),0),0) +
								COALESCE(sub2.BOH*COALESCE((select sns.PR_ENTERPRISE from m_prosentasi_segment sns where sns.K_SEG='BOH'),0),0) +
								COALESCE(sub2.OOH*COALESCE((select sns.PR_ENTERPRISE from m_prosentasi_segment sns where sns.K_SEG='OOH'),0),0) +
								COALESCE(sub2.CON*COALESCE((select sns.PR_ENTERPRISE from m_prosentasi_segment sns where sns.K_SEG='CON' AND sns.TREG_NTREG='TREG'),0),0) +
								COALESCE(sub2.ENT*COALESCE((select sns.PR_ENTERPRISE from m_prosentasi_segment sns where sns.K_SEG='ENT' AND sns.TREG_NTREG='TREG'),0),0) +
								COALESCE(sub2.WIB*COALESCE((select sns.PR_ENTERPRISE from m_prosentasi_segment sns where sns.K_SEG='WIB' AND sns.TREG_NTREG='TREG'),0),0) 
							) as ENTERPRISE,	
							(
								COALESCE(sub2.SOH*COALESCE((select sns.PR_WIB from m_prosentasi_segment sns where sns.K_SEG='SOH'),0),0) +
								COALESCE(sub2.NOH*COALESCE((select sns.PR_WIB from m_prosentasi_segment sns where sns.K_SEG='NOH'),0),0) +
								COALESCE(sub2.BOH*COALESCE((select sns.PR_WIB from m_prosentasi_segment sns where sns.K_SEG='BOH'),0),0) +
								COALESCE(sub2.OOH*COALESCE((select sns.PR_WIB from m_prosentasi_segment sns where sns.K_SEG='OOH'),0),0) +
								COALESCE(sub2.CON*COALESCE((select sns.PR_WIB from m_prosentasi_segment sns where sns.K_SEG='CON' AND sns.TREG_NTREG='TREG'),0),0) +
								COALESCE(sub2.ENT*COALESCE((select sns.PR_WIB from m_prosentasi_segment sns where sns.K_SEG='ENT' AND sns.TREG_NTREG='TREG'),0),0) +
								COALESCE(sub2.WIB*COALESCE((select sns.PR_WIB from m_prosentasi_segment sns where sns.K_SEG='WIB' AND sns.TREG_NTREG='TREG'),0),0) 
							) as WIB,
							(
								COALESCE(sub2.SOH*COALESCE((select sns.PR_DIGITAL from m_prosentasi_segment sns where sns.K_SEG='SOH'),0),0) +
								COALESCE(sub2.NOH*COALESCE((select sns.PR_DIGITAL from m_prosentasi_segment sns where sns.K_SEG='NOH'),0),0) +
								COALESCE(sub2.BOH*COALESCE((select sns.PR_DIGITAL from m_prosentasi_segment sns where sns.K_SEG='BOH'),0),0) +
								COALESCE(sub2.OOH*COALESCE((select sns.PR_DIGITAL from m_prosentasi_segment sns where sns.K_SEG='OOH'),0),0) +
								COALESCE(sub2.CON*COALESCE((select sns.PR_DIGITAL from m_prosentasi_segment sns where sns.K_SEG='CON' AND sns.TREG_NTREG='TREG'),0),0) +
								COALESCE(sub2.ENT*COALESCE((select sns.PR_DIGITAL from m_prosentasi_segment sns where sns.K_SEG='ENT' AND sns.TREG_NTREG='TREG'),0),0) +
								COALESCE(sub2.WIB*COALESCE((select sns.PR_DIGITAL from m_prosentasi_segment sns where sns.K_SEG='WIB' AND sns.TREG_NTREG='TREG'),0),0) 
							) as DIGITAL,
							(
								COALESCE(sub2.SOH*COALESCE((select sns.PR_MOBILE from m_prosentasi_segment sns where sns.K_SEG='SOH'),0),0) +
								COALESCE(sub2.NOH*COALESCE((select sns.PR_MOBILE from m_prosentasi_segment sns where sns.K_SEG='NOH'),0),0) +
								COALESCE(sub2.BOH*COALESCE((select sns.PR_MOBILE from m_prosentasi_segment sns where sns.K_SEG='BOH'),0),0) +
								COALESCE(sub2.OOH*COALESCE((select sns.PR_MOBILE from m_prosentasi_segment sns where sns.K_SEG='OOH'),0),0) +
								COALESCE(sub2.CON*COALESCE((select sns.PR_MOBILE from m_prosentasi_segment sns where sns.K_SEG='CON' AND sns.TREG_NTREG='TREG'),0),0) +
								COALESCE(sub2.ENT*COALESCE((select sns.PR_MOBILE from m_prosentasi_segment sns where sns.K_SEG='ENT' AND sns.TREG_NTREG='TREG'),0),0) +
								COALESCE(sub2.WIB*COALESCE((select sns.PR_MOBILE from m_prosentasi_segment sns where sns.K_SEG='WIB' AND sns.TREG_NTREG='TREG'),0),0) 
							) as MOBILE
							FROM (SELECT
							rek.KODE_DIVISI, 
							rek.C_HOST,
							rek.WITEL,
							(rek.SOH/rek.GRAND_TOTAL) as SOH,
							(rek.NOH/rek.GRAND_TOTAL) as NOH,
							(rek.BOH/rek.GRAND_TOTAL) as BOH,
							(rek.OOH/rek.GRAND_TOTAL) as OOH,
							(rek.CON/rek.GRAND_TOTAL) as CON,
							(rek.ENT/rek.GRAND_TOTAL) as ENT,
							(rek.WIB/rek.GRAND_TOTAL) as WIB
							FROM (SELECT wtl.C_HOST,wtl.KODE_DIVISI,wtl.WITEL,
							(SELECT 
							wit.TOTAL
							FROM t_pros_witel_form wit WHERE wit.K_SEG = 'BOH' AND wit.C_HOST=wtl.C_HOST AND wit.BULAN=:bulan AND wit.TAHUN=:tahun) BOH,
							(SELECT 
							wit.TOTAL
							FROM t_pros_witel_form wit WHERE wit.K_SEG = 'SOH' AND wit.C_HOST=wtl.C_HOST AND wit.BULAN=:bulan AND wit.TAHUN=:tahun) SOH,
							(SELECT 
							wit.TOTAL
							FROM t_pros_witel_form wit WHERE wit.K_SEG = 'OOH' AND wit.C_HOST=wtl.C_HOST AND wit.BULAN=:bulan AND wit.TAHUN=:tahun) OOH,
							(SELECT 
							wit.TOTAL
							FROM t_pros_witel_form wit WHERE wit.K_SEG = 'NOH' AND wit.C_HOST=wtl.C_HOST AND wit.BULAN=:bulan AND wit.TAHUN=:tahun) NOH,
							(SELECT 
							wit.TOTAL
							FROM t_pros_witel_form wit WHERE wit.K_SEG = 'CON' AND wit.C_HOST=wtl.C_HOST AND wit.BULAN=:bulan AND wit.TAHUN=:tahun) CON,
							(SELECT 
							wit.TOTAL
							FROM t_pros_witel_form wit WHERE wit.K_SEG = 'ENT' AND wit.C_HOST=wtl.C_HOST AND wit.BULAN=:bulan AND wit.TAHUN=:tahun) ENT,
							(SELECT 
							wit.TOTAL
							FROM t_pros_witel_form wit WHERE wit.K_SEG = 'WIB' AND wit.C_HOST=wtl.C_HOST AND wit.BULAN=:bulan AND wit.TAHUN=:tahun) WIB,
							(SELECT 
							SUM(wit.TOTAL) as TOTAL
							FROM t_pros_witel_form wit WHERE wit.C_HOST=wtl.C_HOST AND wit.BULAN=:bulan AND wit.TAHUN=:tahun) GRAND_TOTAL
							FROM m_witel wtl) rek ) sub2) sub3
							JOIN t_pros_witel el ON el.C_HOST =  sub3.C_HOST AND el.BULAN=:bulan AND el.TAHUN=:tahun
							JOIN t_pros_divisi si ON si.C_KODE_DIVISI = sub3.KODE_DIVISI AND si.TAHUN=:tahun AND si.BULAN=:bulan) sub4 WHERE sub4.KODE_DIVISI=:kodedivisi";
			$sqlwitel = Yii::app()->db->createCommand($querywitel);
			$sqlwitel->bindParam(':tahun',$tahun);
			$sqlwitel->bindParam(':bulan',$bulan);
			$sqlwitel->bindParam(':kodedivisi',$tregarray[$i]['C_KODE_DIVISI']);
			$witel = $sqlwitel->queryAll();
			$tregarray[$i]['WITEL'] = $witel;
		}
		$querytot = "SELECT 
					SUM(sub5.CONSUMER) as TOTAL_CONSUMER,
					SUM(sub5.ENTERPRISE) as TOTAL_ENTERPRISE,
					SUM(sub5.WIB) as TOTAL_WIB,
					SUM(sub5.DIGITAL) as TOTAL_DIGITAL,
					SUM(sub5.MOBILE) as TOTAL_MOBILE,
					SUM(sub5.TOTAL) as GRAND_TOTAL,
					SUM(sub5.PERSONEL_COST_WITEL) as TOTAL_PERSONEL,
					SUM(sub5.JML_ORG) as TOTAL_ORG
					FROM (SELECT
					sub4.KODE_DIVISI,
					sub4.C_HOST,
					sub4.WITEL,
					sub4.PROSENTASE,
					(select count(t.ID) from t_payslip_".$bulan."_".$tahun." t JOIN m_hr_current_".$bulan."_".$tahun." m 
								ON t.NIK = m.NIK WHERE m.C_HOST = sub4.C_HOST AND m.KODE_DIVISI = sub4.KODE_DIVISI) as JML_ORG,
					-- sub4.PERSONEL_COST_DIVISI,
					(sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) as PERSONEL_COST_WITEL,
					(sub4.CONSUMER*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) as CONSUMER,
					(sub4.ENTERPRISE*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) as ENTERPRISE,
					(sub4.WIB*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) as WIB,
					(sub4.DIGITAL*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) as DIGITAL,
					(sub4.MOBILE*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) as MOBILE,

					(
						(sub4.CONSUMER*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) +
						(sub4.ENTERPRISE*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) +
						(sub4.WIB*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) +
						(sub4.DIGITAL*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) +
						(sub4.MOBILE*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE)
					) as TOTAL

					FROM (SELECT
					sub3.*,
					el.PROSENTASE,
					((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * si.PROSENTASE) as PERSONEL_COST_DIVISI 
					FROM (SELECT 
					sub2.KODE_DIVISI,
					sub2.C_HOST,
					sub2.WITEL,
					(
						COALESCE(sub2.SOH*COALESCE((select sns.PR_CONSUMER from m_prosentasi_segment sns where sns.K_SEG='SOH'),0),0) +
						COALESCE(sub2.NOH*COALESCE((select sns.PR_CONSUMER from m_prosentasi_segment sns where sns.K_SEG='NOH'),0),0) +
						COALESCE(sub2.BOH*COALESCE((select sns.PR_CONSUMER from m_prosentasi_segment sns where sns.K_SEG='BOH'),0),0) +
						COALESCE(sub2.OOH*COALESCE((select sns.PR_CONSUMER from m_prosentasi_segment sns where sns.K_SEG='OOH'),0),0) +
						COALESCE(sub2.CON*COALESCE((select sns.PR_CONSUMER from m_prosentasi_segment sns where sns.K_SEG='CON' AND sns.TREG_NTREG='TREG'),0),0) +
						COALESCE(sub2.ENT*COALESCE((select sns.PR_CONSUMER from m_prosentasi_segment sns where sns.K_SEG='ENT' AND sns.TREG_NTREG='TREG'),0),0) +
						COALESCE(sub2.WIB*COALESCE((select sns.PR_CONSUMER from m_prosentasi_segment sns where sns.K_SEG='WIB' AND sns.TREG_NTREG='TREG'),0),0) 
					) as CONSUMER,
					(
						COALESCE(sub2.SOH*COALESCE((select sns.PR_ENTERPRISE from m_prosentasi_segment sns where sns.K_SEG='SOH'),0),0) +
						COALESCE(sub2.NOH*COALESCE((select sns.PR_ENTERPRISE from m_prosentasi_segment sns where sns.K_SEG='NOH'),0),0) +
						COALESCE(sub2.BOH*COALESCE((select sns.PR_ENTERPRISE from m_prosentasi_segment sns where sns.K_SEG='BOH'),0),0) +
						COALESCE(sub2.OOH*COALESCE((select sns.PR_ENTERPRISE from m_prosentasi_segment sns where sns.K_SEG='OOH'),0),0) +
						COALESCE(sub2.CON*COALESCE((select sns.PR_ENTERPRISE from m_prosentasi_segment sns where sns.K_SEG='CON' AND sns.TREG_NTREG='TREG'),0),0) +
						COALESCE(sub2.ENT*COALESCE((select sns.PR_ENTERPRISE from m_prosentasi_segment sns where sns.K_SEG='ENT' AND sns.TREG_NTREG='TREG'),0),0) +
						COALESCE(sub2.WIB*COALESCE((select sns.PR_ENTERPRISE from m_prosentasi_segment sns where sns.K_SEG='WIB' AND sns.TREG_NTREG='TREG'),0),0) 
					) as ENTERPRISE,	
					(
						COALESCE(sub2.SOH*COALESCE((select sns.PR_WIB from m_prosentasi_segment sns where sns.K_SEG='SOH'),0),0) +
						COALESCE(sub2.NOH*COALESCE((select sns.PR_WIB from m_prosentasi_segment sns where sns.K_SEG='NOH'),0),0) +
						COALESCE(sub2.BOH*COALESCE((select sns.PR_WIB from m_prosentasi_segment sns where sns.K_SEG='BOH'),0),0) +
						COALESCE(sub2.OOH*COALESCE((select sns.PR_WIB from m_prosentasi_segment sns where sns.K_SEG='OOH'),0),0) +
						COALESCE(sub2.CON*COALESCE((select sns.PR_WIB from m_prosentasi_segment sns where sns.K_SEG='CON' AND sns.TREG_NTREG='TREG'),0),0) +
						COALESCE(sub2.ENT*COALESCE((select sns.PR_WIB from m_prosentasi_segment sns where sns.K_SEG='ENT' AND sns.TREG_NTREG='TREG'),0),0) +
						COALESCE(sub2.WIB*COALESCE((select sns.PR_WIB from m_prosentasi_segment sns where sns.K_SEG='WIB' AND sns.TREG_NTREG='TREG'),0),0) 
					) as WIB,
					(
						COALESCE(sub2.SOH*COALESCE((select sns.PR_DIGITAL from m_prosentasi_segment sns where sns.K_SEG='SOH'),0),0) +
						COALESCE(sub2.NOH*COALESCE((select sns.PR_DIGITAL from m_prosentasi_segment sns where sns.K_SEG='NOH'),0),0) +
						COALESCE(sub2.BOH*COALESCE((select sns.PR_DIGITAL from m_prosentasi_segment sns where sns.K_SEG='BOH'),0),0) +
						COALESCE(sub2.OOH*COALESCE((select sns.PR_DIGITAL from m_prosentasi_segment sns where sns.K_SEG='OOH'),0),0) +
						COALESCE(sub2.CON*COALESCE((select sns.PR_DIGITAL from m_prosentasi_segment sns where sns.K_SEG='CON' AND sns.TREG_NTREG='TREG'),0),0) +
						COALESCE(sub2.ENT*COALESCE((select sns.PR_DIGITAL from m_prosentasi_segment sns where sns.K_SEG='ENT' AND sns.TREG_NTREG='TREG'),0),0) +
						COALESCE(sub2.WIB*COALESCE((select sns.PR_DIGITAL from m_prosentasi_segment sns where sns.K_SEG='WIB' AND sns.TREG_NTREG='TREG'),0),0) 
					) as DIGITAL,
					(
						COALESCE(sub2.SOH*COALESCE((select sns.PR_MOBILE from m_prosentasi_segment sns where sns.K_SEG='SOH'),0),0) +
						COALESCE(sub2.NOH*COALESCE((select sns.PR_MOBILE from m_prosentasi_segment sns where sns.K_SEG='NOH'),0),0) +
						COALESCE(sub2.BOH*COALESCE((select sns.PR_MOBILE from m_prosentasi_segment sns where sns.K_SEG='BOH'),0),0) +
						COALESCE(sub2.OOH*COALESCE((select sns.PR_MOBILE from m_prosentasi_segment sns where sns.K_SEG='OOH'),0),0) +
						COALESCE(sub2.CON*COALESCE((select sns.PR_MOBILE from m_prosentasi_segment sns where sns.K_SEG='CON' AND sns.TREG_NTREG='TREG'),0),0) +
						COALESCE(sub2.ENT*COALESCE((select sns.PR_MOBILE from m_prosentasi_segment sns where sns.K_SEG='ENT' AND sns.TREG_NTREG='TREG'),0),0) +
						COALESCE(sub2.WIB*COALESCE((select sns.PR_MOBILE from m_prosentasi_segment sns where sns.K_SEG='WIB' AND sns.TREG_NTREG='TREG'),0),0) 
					) as MOBILE
					FROM (SELECT
					rek.KODE_DIVISI, 
					rek.C_HOST,
					rek.WITEL,
					(rek.SOH/rek.GRAND_TOTAL) as SOH,
					(rek.NOH/rek.GRAND_TOTAL) as NOH,
					(rek.BOH/rek.GRAND_TOTAL) as BOH,
					(rek.OOH/rek.GRAND_TOTAL) as OOH,
					(rek.CON/rek.GRAND_TOTAL) as CON,
					(rek.ENT/rek.GRAND_TOTAL) as ENT,
					(rek.WIB/rek.GRAND_TOTAL) as WIB
					FROM (SELECT wtl.C_HOST,wtl.KODE_DIVISI,wtl.WITEL,
					(SELECT 
					wit.TOTAL
					FROM t_pros_witel_form wit WHERE wit.K_SEG = 'BOH' AND wit.C_HOST=wtl.C_HOST AND wit.BULAN=:bulan AND wit.TAHUN=:tahun) BOH,
					(SELECT 
					wit.TOTAL
					FROM t_pros_witel_form wit WHERE wit.K_SEG = 'SOH' AND wit.C_HOST=wtl.C_HOST AND wit.BULAN=:bulan AND wit.TAHUN=:tahun) SOH,
					(SELECT 
					wit.TOTAL
					FROM t_pros_witel_form wit WHERE wit.K_SEG = 'OOH' AND wit.C_HOST=wtl.C_HOST AND wit.BULAN=:bulan AND wit.TAHUN=:tahun) OOH,
					(SELECT 
					wit.TOTAL
					FROM t_pros_witel_form wit WHERE wit.K_SEG = 'NOH' AND wit.C_HOST=wtl.C_HOST AND wit.BULAN=:bulan AND wit.TAHUN=:tahun) NOH,
					(SELECT 
					wit.TOTAL
					FROM t_pros_witel_form wit WHERE wit.K_SEG = 'CON' AND wit.C_HOST=wtl.C_HOST AND wit.BULAN=:bulan AND wit.TAHUN=:tahun) CON,
					(SELECT 
					wit.TOTAL
					FROM t_pros_witel_form wit WHERE wit.K_SEG = 'ENT' AND wit.C_HOST=wtl.C_HOST AND wit.BULAN=:bulan AND wit.TAHUN=:tahun) ENT,
					(SELECT 
					wit.TOTAL
					FROM t_pros_witel_form wit WHERE wit.K_SEG = 'WIB' AND wit.C_HOST=wtl.C_HOST AND wit.BULAN=:bulan AND wit.TAHUN=:tahun) WIB,
					(SELECT 
					SUM(wit.TOTAL) as TOTAL
					FROM t_pros_witel_form wit WHERE wit.C_HOST=wtl.C_HOST AND wit.BULAN=:bulan AND wit.TAHUN=:tahun) GRAND_TOTAL
					FROM m_witel wtl) rek ) sub2) sub3
					JOIN t_pros_witel el ON el.C_HOST =  sub3.C_HOST AND el.BULAN=:bulan AND el.TAHUN=:tahun
					JOIN t_pros_divisi si ON si.C_KODE_DIVISI = sub3.KODE_DIVISI AND si.TAHUN=:tahun AND si.BULAN=:bulan) sub4) sub5";
		$sqltot = Yii::app()->db->createCommand($querytot);
		$sqltot->bindParam(':tahun',$tahun);
		$sqltot->bindParam(':bulan',$bulan);
		$tot = $sqltot->queryRow();
	
		$this->render('report',array(
			'tahun'=>$tahun,
			'bulan'=>$bulan,
			'tregarray'=>$tregarray,
			'tot'=>$tot,
		));
	}

	public function actionReportnow($tahun,$bulan)
	{
		$sqldis = "SELECT DISTINCT(t.BULAN) FROM t_pros_cfu_fu t WHERE t.TAHUN = :tahun AND t.BULAN <= :bulan ORDER BY t.BULAN";
		$cmddis = Yii::app()->db->createCommand($sqldis);
		$cmddis->bindParam(':tahun',$tahun);
		$cmddis->bindParam(':bulan',$bulan);
		$blndis = $cmddis->queryAll();

		$query1 = "SELECT 
					sub.C_KODE_DIVISI,
					sub.V_SHORT_DIVISI,
					sum(sub.PERSONEL_COST_DIVISI) PERSONEL_COST_DIVISI
					FROM (SELECT
					si.C_KODE_DIVISI,
					si.V_SHORT_DIVISI,
					((select rel.REALISASI from t_realsap rel where rel.TAHUN=si.TAHUN and rel.BULAN=si.BULAN) * si.PROSENTASE) as PERSONEL_COST_DIVISI  
					FROM t_pros_divisi si
					WHERE si.TAHUN=:tahun AND si.BULAN<=:bulan AND 
					si.C_KODE_DIVISI IN ('DIV-TR1','DIV-TR2','DIV-TR3','DIV-TR4','DIV-TR5','DIV-TR6','DIV-TR7')) sub
					GROUP BY sub.C_KODE_DIVISI";
		$sql1 = Yii::app()->db->createCommand($query1); 
		$sql1->bindParam(':tahun',$tahun);
		$sql1->bindParam(':bulan',$bulan);
		$datatreg = $sql1->queryAll();
		$tregarray = array();
		$jml = count($blndis);
		for ($i=0; $i < count($datatreg); $i++) { 
			$tregarray[$i] = $datatreg[$i];
			$querywitel = "SELECT
					tb1.KODE_DIVISI,
					tb1.C_HOST,
					tb1.WITEL,
					SUM(tb1.PERSONEL_COST_WITEL) PERSONEL_COST_WITEL,
					SUM(tb1.CONSUMER) CONSUMER,
					SUM(tb1.ENTERPRISE) ENTERPRISE,
					SUM(tb1.WIB) WIB,
					SUM(tb1.DIGITAL) DIGITAL,
					SUM(tb1.MOBILE) MOBILE,
					SUM(tb1.TOTAL) TOTAL
					FROM (";

			$a=1;
			foreach ($blndis as $key => $value) {
				$querywitel .= "(SELECT
								sub4.KODE_DIVISI,
								sub4.C_HOST,
								sub4.WITEL,
								sub4.PROSENTASE,
								-- sub4.PERSONEL_COST_DIVISI,
								(sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) as PERSONEL_COST_WITEL,
								(sub4.CONSUMER*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) as CONSUMER,
								(sub4.ENTERPRISE*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) as ENTERPRISE,
								(sub4.WIB*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) as WIB,
								(sub4.DIGITAL*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) as DIGITAL,
								(sub4.MOBILE*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) as MOBILE,

								(
									(sub4.CONSUMER*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) +
									(sub4.ENTERPRISE*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) +
									(sub4.WIB*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) +
									(sub4.DIGITAL*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) +
									(sub4.MOBILE*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE)
								) as TOTAL

								FROM (SELECT
								sub3.*,
								el.PROSENTASE,
								((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan".$value["BULAN"].") * si.PROSENTASE) as PERSONEL_COST_DIVISI 
								FROM (SELECT 
								sub2.KODE_DIVISI,
								sub2.C_HOST,
								sub2.WITEL,
								(
									COALESCE(sub2.SOH*COALESCE((select sns.PR_CONSUMER from m_prosentasi_segment sns where sns.K_SEG='SOH'),0),0) +
									COALESCE(sub2.NOH*COALESCE((select sns.PR_CONSUMER from m_prosentasi_segment sns where sns.K_SEG='NOH'),0),0) +
									COALESCE(sub2.BOH*COALESCE((select sns.PR_CONSUMER from m_prosentasi_segment sns where sns.K_SEG='BOH'),0),0) +
									COALESCE(sub2.OOH*COALESCE((select sns.PR_CONSUMER from m_prosentasi_segment sns where sns.K_SEG='OOH'),0),0) +
									COALESCE(sub2.CON*COALESCE((select sns.PR_CONSUMER from m_prosentasi_segment sns where sns.K_SEG='CON' AND sns.TREG_NTREG='TREG'),0),0) +
									COALESCE(sub2.ENT*COALESCE((select sns.PR_CONSUMER from m_prosentasi_segment sns where sns.K_SEG='ENT' AND sns.TREG_NTREG='TREG'),0),0) +
									COALESCE(sub2.WIB*COALESCE((select sns.PR_CONSUMER from m_prosentasi_segment sns where sns.K_SEG='WIB' AND sns.TREG_NTREG='TREG'),0),0) 
								) as CONSUMER,
								(
									COALESCE(sub2.SOH*COALESCE((select sns.PR_ENTERPRISE from m_prosentasi_segment sns where sns.K_SEG='SOH'),0),0) +
									COALESCE(sub2.NOH*COALESCE((select sns.PR_ENTERPRISE from m_prosentasi_segment sns where sns.K_SEG='NOH'),0),0) +
									COALESCE(sub2.BOH*COALESCE((select sns.PR_ENTERPRISE from m_prosentasi_segment sns where sns.K_SEG='BOH'),0),0) +
									COALESCE(sub2.OOH*COALESCE((select sns.PR_ENTERPRISE from m_prosentasi_segment sns where sns.K_SEG='OOH'),0),0) +
									COALESCE(sub2.CON*COALESCE((select sns.PR_ENTERPRISE from m_prosentasi_segment sns where sns.K_SEG='CON' AND sns.TREG_NTREG='TREG'),0),0) +
									COALESCE(sub2.ENT*COALESCE((select sns.PR_ENTERPRISE from m_prosentasi_segment sns where sns.K_SEG='ENT' AND sns.TREG_NTREG='TREG'),0),0) +
									COALESCE(sub2.WIB*COALESCE((select sns.PR_ENTERPRISE from m_prosentasi_segment sns where sns.K_SEG='WIB' AND sns.TREG_NTREG='TREG'),0),0) 
								) as ENTERPRISE,	
								(
									COALESCE(sub2.SOH*COALESCE((select sns.PR_WIB from m_prosentasi_segment sns where sns.K_SEG='SOH'),0),0) +
									COALESCE(sub2.NOH*COALESCE((select sns.PR_WIB from m_prosentasi_segment sns where sns.K_SEG='NOH'),0),0) +
									COALESCE(sub2.BOH*COALESCE((select sns.PR_WIB from m_prosentasi_segment sns where sns.K_SEG='BOH'),0),0) +
									COALESCE(sub2.OOH*COALESCE((select sns.PR_WIB from m_prosentasi_segment sns where sns.K_SEG='OOH'),0),0) +
									COALESCE(sub2.CON*COALESCE((select sns.PR_WIB from m_prosentasi_segment sns where sns.K_SEG='CON' AND sns.TREG_NTREG='TREG'),0),0) +
									COALESCE(sub2.ENT*COALESCE((select sns.PR_WIB from m_prosentasi_segment sns where sns.K_SEG='ENT' AND sns.TREG_NTREG='TREG'),0),0) +
									COALESCE(sub2.WIB*COALESCE((select sns.PR_WIB from m_prosentasi_segment sns where sns.K_SEG='WIB' AND sns.TREG_NTREG='TREG'),0),0) 
								) as WIB,
								(
									COALESCE(sub2.SOH*COALESCE((select sns.PR_DIGITAL from m_prosentasi_segment sns where sns.K_SEG='SOH'),0),0) +
									COALESCE(sub2.NOH*COALESCE((select sns.PR_DIGITAL from m_prosentasi_segment sns where sns.K_SEG='NOH'),0),0) +
									COALESCE(sub2.BOH*COALESCE((select sns.PR_DIGITAL from m_prosentasi_segment sns where sns.K_SEG='BOH'),0),0) +
									COALESCE(sub2.OOH*COALESCE((select sns.PR_DIGITAL from m_prosentasi_segment sns where sns.K_SEG='OOH'),0),0) +
									COALESCE(sub2.CON*COALESCE((select sns.PR_DIGITAL from m_prosentasi_segment sns where sns.K_SEG='CON' AND sns.TREG_NTREG='TREG'),0),0) +
									COALESCE(sub2.ENT*COALESCE((select sns.PR_DIGITAL from m_prosentasi_segment sns where sns.K_SEG='ENT' AND sns.TREG_NTREG='TREG'),0),0) +
									COALESCE(sub2.WIB*COALESCE((select sns.PR_DIGITAL from m_prosentasi_segment sns where sns.K_SEG='WIB' AND sns.TREG_NTREG='TREG'),0),0) 
								) as DIGITAL,
								(
									COALESCE(sub2.SOH*COALESCE((select sns.PR_MOBILE from m_prosentasi_segment sns where sns.K_SEG='SOH'),0),0) +
									COALESCE(sub2.NOH*COALESCE((select sns.PR_MOBILE from m_prosentasi_segment sns where sns.K_SEG='NOH'),0),0) +
									COALESCE(sub2.BOH*COALESCE((select sns.PR_MOBILE from m_prosentasi_segment sns where sns.K_SEG='BOH'),0),0) +
									COALESCE(sub2.OOH*COALESCE((select sns.PR_MOBILE from m_prosentasi_segment sns where sns.K_SEG='OOH'),0),0) +
									COALESCE(sub2.CON*COALESCE((select sns.PR_MOBILE from m_prosentasi_segment sns where sns.K_SEG='CON' AND sns.TREG_NTREG='TREG'),0),0) +
									COALESCE(sub2.ENT*COALESCE((select sns.PR_MOBILE from m_prosentasi_segment sns where sns.K_SEG='ENT' AND sns.TREG_NTREG='TREG'),0),0) +
									COALESCE(sub2.WIB*COALESCE((select sns.PR_MOBILE from m_prosentasi_segment sns where sns.K_SEG='WIB' AND sns.TREG_NTREG='TREG'),0),0) 
								) as MOBILE
								FROM (SELECT
								rek.KODE_DIVISI, 
								rek.C_HOST,
								rek.WITEL,
								(rek.SOH/rek.GRAND_TOTAL) as SOH,
								(rek.NOH/rek.GRAND_TOTAL) as NOH,
								(rek.BOH/rek.GRAND_TOTAL) as BOH,
								(rek.OOH/rek.GRAND_TOTAL) as OOH,
								(rek.CON/rek.GRAND_TOTAL) as CON,
								(rek.ENT/rek.GRAND_TOTAL) as ENT,
								(rek.WIB/rek.GRAND_TOTAL) as WIB
								FROM (SELECT wtl.C_HOST,wtl.KODE_DIVISI,wtl.WITEL,
								(SELECT 
								wit.TOTAL
								FROM t_pros_witel_form wit WHERE wit.K_SEG = 'BOH' AND wit.C_HOST=wtl.C_HOST AND wit.BULAN=:bulan".$value["BULAN"]." AND wit.TAHUN=:tahun) BOH,
								(SELECT 
								wit.TOTAL
								FROM t_pros_witel_form wit WHERE wit.K_SEG = 'SOH' AND wit.C_HOST=wtl.C_HOST AND wit.BULAN=:bulan".$value["BULAN"]." AND wit.TAHUN=:tahun) SOH,
								(SELECT 
								wit.TOTAL
								FROM t_pros_witel_form wit WHERE wit.K_SEG = 'OOH' AND wit.C_HOST=wtl.C_HOST AND wit.BULAN=:bulan".$value["BULAN"]." AND wit.TAHUN=:tahun) OOH,
								(SELECT 
								wit.TOTAL
								FROM t_pros_witel_form wit WHERE wit.K_SEG = 'NOH' AND wit.C_HOST=wtl.C_HOST AND wit.BULAN=:bulan".$value["BULAN"]." AND wit.TAHUN=:tahun) NOH,
								(SELECT 
								wit.TOTAL
								FROM t_pros_witel_form wit WHERE wit.K_SEG = 'CON' AND wit.C_HOST=wtl.C_HOST AND wit.BULAN=:bulan".$value["BULAN"]." AND wit.TAHUN=:tahun) CON,
								(SELECT 
								wit.TOTAL
								FROM t_pros_witel_form wit WHERE wit.K_SEG = 'ENT' AND wit.C_HOST=wtl.C_HOST AND wit.BULAN=:bulan".$value["BULAN"]." AND wit.TAHUN=:tahun) ENT,
								(SELECT 
								wit.TOTAL
								FROM t_pros_witel_form wit WHERE wit.K_SEG = 'WIB' AND wit.C_HOST=wtl.C_HOST AND wit.BULAN=:bulan".$value["BULAN"]." AND wit.TAHUN=:tahun) WIB,
								(SELECT 
								SUM(wit.TOTAL) as TOTAL
								FROM t_pros_witel_form wit WHERE wit.C_HOST=wtl.C_HOST AND wit.BULAN=:bulan".$value["BULAN"]." AND wit.TAHUN=:tahun) GRAND_TOTAL
								FROM m_witel wtl) rek ) sub2) sub3
								JOIN t_pros_witel el ON el.C_HOST =  sub3.C_HOST AND el.BULAN=:bulan".$value["BULAN"]." AND el.TAHUN=:tahun
								JOIN t_pros_divisi si ON si.C_KODE_DIVISI = sub3.KODE_DIVISI AND si.TAHUN=:tahun AND si.BULAN=:bulan".$value["BULAN"].") sub4 WHERE sub4.KODE_DIVISI=:kodedivisi)";
				if ($jml-$a != 0) {
					$querywitel .= " UNION ";
				}else{
					$querywitel .= ") tb1 GROUP BY tb1.C_HOST";
				}

				$a++;
			}
			$sqlwitel = Yii::app()->db->createCommand($querywitel);
			$sqlwitel->bindParam(':tahun',$tahun);
			foreach ($blndis as $key => $value) {
				$par = ':bulan'.$value["BULAN"];
				$sqlwitel->bindParam($par,$value["BULAN"]);
			}
			$sqlwitel->bindParam(':bulan',$bulan);
			$sqlwitel->bindParam(':kodedivisi',$tregarray[$i]['C_KODE_DIVISI']);
			$witel = $sqlwitel->queryAll();
			$tregarray[$i]['WITEL'] = $witel;
		}

		// var_dump($tregarray[1]['WITEL']);exit();
		$querytot = "SELECT 
					SUM(tb1.TOTAL_CONSUMER) TOTAL_CONSUMER,
					SUM(tb1.TOTAL_ENTERPRISE) as TOTAL_ENTERPRISE,
					SUM(tb1.TOTAL_WIB) as TOTAL_WIB,
					SUM(tb1.TOTAL_DIGITAL) as TOTAL_DIGITAL,
					SUM(tb1.TOTAL_MOBILE) as TOTAL_MOBILE,
					SUM(tb1.GRAND_TOTAL) as GRAND_TOTAL,
					SUM(tb1.TOTAL_PERSONEL) as TOTAL_PERSONEL
					FROM ( ";
		$a=1;
		foreach ($blndis as $key => $value) {				
			$querytot .= "(SELECT 
						SUM(sub5.CONSUMER) as TOTAL_CONSUMER,
						SUM(sub5.ENTERPRISE) as TOTAL_ENTERPRISE,
						SUM(sub5.WIB) as TOTAL_WIB,
						SUM(sub5.DIGITAL) as TOTAL_DIGITAL,
						SUM(sub5.MOBILE) as TOTAL_MOBILE,
						SUM(sub5.TOTAL) as GRAND_TOTAL,
						SUM(sub5.PERSONEL_COST_WITEL) as TOTAL_PERSONEL
						FROM (SELECT
						sub4.KODE_DIVISI,
						sub4.C_HOST,
						sub4.WITEL,
						sub4.PROSENTASE,
						-- sub4.PERSONEL_COST_DIVISI,
						(sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) as PERSONEL_COST_WITEL,
						(sub4.CONSUMER*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) as CONSUMER,
						(sub4.ENTERPRISE*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) as ENTERPRISE,
						(sub4.WIB*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) as WIB,
						(sub4.DIGITAL*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) as DIGITAL,
						(sub4.MOBILE*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) as MOBILE,

						(
							(sub4.CONSUMER*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) +
							(sub4.ENTERPRISE*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) +
							(sub4.WIB*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) +
							(sub4.DIGITAL*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) +
							(sub4.MOBILE*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE)
						) as TOTAL

						FROM (SELECT
						sub3.*,
						el.PROSENTASE,
						((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan".$value["BULAN"].") * si.PROSENTASE) as PERSONEL_COST_DIVISI 
						FROM (SELECT 
						sub2.KODE_DIVISI,
						sub2.C_HOST,
						sub2.WITEL,
						(
							COALESCE(sub2.SOH*COALESCE((select sns.PR_CONSUMER from m_prosentasi_segment sns where sns.K_SEG='SOH'),0),0) +
							COALESCE(sub2.NOH*COALESCE((select sns.PR_CONSUMER from m_prosentasi_segment sns where sns.K_SEG='NOH'),0),0) +
							COALESCE(sub2.BOH*COALESCE((select sns.PR_CONSUMER from m_prosentasi_segment sns where sns.K_SEG='BOH'),0),0) +
							COALESCE(sub2.OOH*COALESCE((select sns.PR_CONSUMER from m_prosentasi_segment sns where sns.K_SEG='OOH'),0),0) +
							COALESCE(sub2.CON*COALESCE((select sns.PR_CONSUMER from m_prosentasi_segment sns where sns.K_SEG='CON' AND sns.TREG_NTREG='TREG'),0),0) +
							COALESCE(sub2.ENT*COALESCE((select sns.PR_CONSUMER from m_prosentasi_segment sns where sns.K_SEG='ENT' AND sns.TREG_NTREG='TREG'),0),0) +
							COALESCE(sub2.WIB*COALESCE((select sns.PR_CONSUMER from m_prosentasi_segment sns where sns.K_SEG='WIB' AND sns.TREG_NTREG='TREG'),0),0) 
						) as CONSUMER,
						(
							COALESCE(sub2.SOH*COALESCE((select sns.PR_ENTERPRISE from m_prosentasi_segment sns where sns.K_SEG='SOH'),0),0) +
							COALESCE(sub2.NOH*COALESCE((select sns.PR_ENTERPRISE from m_prosentasi_segment sns where sns.K_SEG='NOH'),0),0) +
							COALESCE(sub2.BOH*COALESCE((select sns.PR_ENTERPRISE from m_prosentasi_segment sns where sns.K_SEG='BOH'),0),0) +
							COALESCE(sub2.OOH*COALESCE((select sns.PR_ENTERPRISE from m_prosentasi_segment sns where sns.K_SEG='OOH'),0),0) +
							COALESCE(sub2.CON*COALESCE((select sns.PR_ENTERPRISE from m_prosentasi_segment sns where sns.K_SEG='CON' AND sns.TREG_NTREG='TREG'),0),0) +
							COALESCE(sub2.ENT*COALESCE((select sns.PR_ENTERPRISE from m_prosentasi_segment sns where sns.K_SEG='ENT' AND sns.TREG_NTREG='TREG'),0),0) +
							COALESCE(sub2.WIB*COALESCE((select sns.PR_ENTERPRISE from m_prosentasi_segment sns where sns.K_SEG='WIB' AND sns.TREG_NTREG='TREG'),0),0) 
						) as ENTERPRISE,	
						(
							COALESCE(sub2.SOH*COALESCE((select sns.PR_WIB from m_prosentasi_segment sns where sns.K_SEG='SOH'),0),0) +
							COALESCE(sub2.NOH*COALESCE((select sns.PR_WIB from m_prosentasi_segment sns where sns.K_SEG='NOH'),0),0) +
							COALESCE(sub2.BOH*COALESCE((select sns.PR_WIB from m_prosentasi_segment sns where sns.K_SEG='BOH'),0),0) +
							COALESCE(sub2.OOH*COALESCE((select sns.PR_WIB from m_prosentasi_segment sns where sns.K_SEG='OOH'),0),0) +
							COALESCE(sub2.CON*COALESCE((select sns.PR_WIB from m_prosentasi_segment sns where sns.K_SEG='CON' AND sns.TREG_NTREG='TREG'),0),0) +
							COALESCE(sub2.ENT*COALESCE((select sns.PR_WIB from m_prosentasi_segment sns where sns.K_SEG='ENT' AND sns.TREG_NTREG='TREG'),0),0) +
							COALESCE(sub2.WIB*COALESCE((select sns.PR_WIB from m_prosentasi_segment sns where sns.K_SEG='WIB' AND sns.TREG_NTREG='TREG'),0),0) 
						) as WIB,
						(
							COALESCE(sub2.SOH*COALESCE((select sns.PR_DIGITAL from m_prosentasi_segment sns where sns.K_SEG='SOH'),0),0) +
							COALESCE(sub2.NOH*COALESCE((select sns.PR_DIGITAL from m_prosentasi_segment sns where sns.K_SEG='NOH'),0),0) +
							COALESCE(sub2.BOH*COALESCE((select sns.PR_DIGITAL from m_prosentasi_segment sns where sns.K_SEG='BOH'),0),0) +
							COALESCE(sub2.OOH*COALESCE((select sns.PR_DIGITAL from m_prosentasi_segment sns where sns.K_SEG='OOH'),0),0) +
							COALESCE(sub2.CON*COALESCE((select sns.PR_DIGITAL from m_prosentasi_segment sns where sns.K_SEG='CON' AND sns.TREG_NTREG='TREG'),0),0) +
							COALESCE(sub2.ENT*COALESCE((select sns.PR_DIGITAL from m_prosentasi_segment sns where sns.K_SEG='ENT' AND sns.TREG_NTREG='TREG'),0),0) +
							COALESCE(sub2.WIB*COALESCE((select sns.PR_DIGITAL from m_prosentasi_segment sns where sns.K_SEG='WIB' AND sns.TREG_NTREG='TREG'),0),0) 
						) as DIGITAL,
						(
							COALESCE(sub2.SOH*COALESCE((select sns.PR_MOBILE from m_prosentasi_segment sns where sns.K_SEG='SOH'),0),0) +
							COALESCE(sub2.NOH*COALESCE((select sns.PR_MOBILE from m_prosentasi_segment sns where sns.K_SEG='NOH'),0),0) +
							COALESCE(sub2.BOH*COALESCE((select sns.PR_MOBILE from m_prosentasi_segment sns where sns.K_SEG='BOH'),0),0) +
							COALESCE(sub2.OOH*COALESCE((select sns.PR_MOBILE from m_prosentasi_segment sns where sns.K_SEG='OOH'),0),0) +
							COALESCE(sub2.CON*COALESCE((select sns.PR_MOBILE from m_prosentasi_segment sns where sns.K_SEG='CON' AND sns.TREG_NTREG='TREG'),0),0) +
							COALESCE(sub2.ENT*COALESCE((select sns.PR_MOBILE from m_prosentasi_segment sns where sns.K_SEG='ENT' AND sns.TREG_NTREG='TREG'),0),0) +
							COALESCE(sub2.WIB*COALESCE((select sns.PR_MOBILE from m_prosentasi_segment sns where sns.K_SEG='WIB' AND sns.TREG_NTREG='TREG'),0),0) 
						) as MOBILE
						FROM (SELECT
						rek.KODE_DIVISI, 
						rek.C_HOST,
						rek.WITEL,
						(rek.SOH/rek.GRAND_TOTAL) as SOH,
						(rek.NOH/rek.GRAND_TOTAL) as NOH,
						(rek.BOH/rek.GRAND_TOTAL) as BOH,
						(rek.OOH/rek.GRAND_TOTAL) as OOH,
						(rek.CON/rek.GRAND_TOTAL) as CON,
						(rek.ENT/rek.GRAND_TOTAL) as ENT,
						(rek.WIB/rek.GRAND_TOTAL) as WIB
						FROM (SELECT wtl.C_HOST,wtl.KODE_DIVISI,wtl.WITEL,
						(SELECT 
						wit.TOTAL
						FROM t_pros_witel_form wit WHERE wit.K_SEG = 'BOH' AND wit.C_HOST=wtl.C_HOST AND wit.BULAN=:bulan".$value["BULAN"]." AND wit.TAHUN=:tahun) BOH,
						(SELECT 
						wit.TOTAL
						FROM t_pros_witel_form wit WHERE wit.K_SEG = 'SOH' AND wit.C_HOST=wtl.C_HOST AND wit.BULAN=:bulan".$value["BULAN"]." AND wit.TAHUN=:tahun) SOH,
						(SELECT 
						wit.TOTAL
						FROM t_pros_witel_form wit WHERE wit.K_SEG = 'OOH' AND wit.C_HOST=wtl.C_HOST AND wit.BULAN=:bulan".$value["BULAN"]." AND wit.TAHUN=:tahun) OOH,
						(SELECT 
						wit.TOTAL
						FROM t_pros_witel_form wit WHERE wit.K_SEG = 'NOH' AND wit.C_HOST=wtl.C_HOST AND wit.BULAN=:bulan".$value["BULAN"]." AND wit.TAHUN=:tahun) NOH,
						(SELECT 
						wit.TOTAL
						FROM t_pros_witel_form wit WHERE wit.K_SEG = 'CON' AND wit.C_HOST=wtl.C_HOST AND wit.BULAN=:bulan".$value["BULAN"]." AND wit.TAHUN=:tahun) CON,
						(SELECT 
						wit.TOTAL
						FROM t_pros_witel_form wit WHERE wit.K_SEG = 'ENT' AND wit.C_HOST=wtl.C_HOST AND wit.BULAN=:bulan".$value["BULAN"]." AND wit.TAHUN=:tahun) ENT,
						(SELECT 
						wit.TOTAL
						FROM t_pros_witel_form wit WHERE wit.K_SEG = 'WIB' AND wit.C_HOST=wtl.C_HOST AND wit.BULAN=:bulan".$value["BULAN"]." AND wit.TAHUN=:tahun) WIB,
						(SELECT 
						SUM(wit.TOTAL) as TOTAL
						FROM t_pros_witel_form wit WHERE wit.C_HOST=wtl.C_HOST AND wit.BULAN=:bulan".$value["BULAN"]." AND wit.TAHUN=:tahun) GRAND_TOTAL
						FROM m_witel wtl) rek ) sub2) sub3
						JOIN t_pros_witel el ON el.C_HOST =  sub3.C_HOST AND el.BULAN=:bulan".$value["BULAN"]." AND el.TAHUN=:tahun
						JOIN t_pros_divisi si ON si.C_KODE_DIVISI = sub3.KODE_DIVISI AND si.TAHUN=:tahun AND si.BULAN=:bulan".$value["BULAN"].") sub4) sub5)";
			if ($jml-$a != 0) {
				$querytot .= " UNION ";
			}else{
				$querytot .= ") tb1";
			}

			$a++;
		}

		$sqltot = Yii::app()->db->createCommand($querytot);
		$sqltot->bindParam(':tahun',$tahun);
		foreach ($blndis as $key => $value) {
			$par = ':bulan'.$value["BULAN"];
			$sqltot->bindParam($par,$value["BULAN"]);
		}
		$tot = $sqltot->queryRow();

		$this->render('reportnow',array(
			'tahun'=>$tahun,
			'bulan'=>$bulan,
			'tregarray'=>$tregarray,
			'tot'=>$tot,
		));
	}

	public function actionReportdivisinow($tahun,$bulan)
	{
		$sqldis = "SELECT DISTINCT(t.BULAN) FROM t_pros_cfu_fu t WHERE t.TAHUN = :tahun AND t.BULAN <= :bulan ORDER BY t.BULAN";
		$cmddis = Yii::app()->db->createCommand($sqldis);
		$cmddis->bindParam(':tahun',$tahun);
		$cmddis->bindParam(':bulan',$bulan);
		$blndis = $cmddis->queryAll();

		$sqlrealsapnow = "SELECT SUM(t.REALISASI) FROM t_realsap t WHERE t.TAHUN = :tahun AND t.BULAN <= :bulan";
		$cmdrealsapnow = Yii::app()->db->createCommand($sqlrealsapnow);
		$cmdrealsapnow->bindParam(':tahun',$tahun);
		$cmdrealsapnow->bindParam(':bulan',$bulan);
		$realsapnow = $cmdrealsapnow->queryScalar();

		$query = "SELECT 
					tb1.C_KODE_DIVISI,
					tb1.divisi,
					tb1.CFU_FU,
					SUM(tb1.PERSONEL_COST) PERSONEL_COST,
					SUM(tb1.CONSUMER) CONSUMER,
					SUM(tb1.ENTERPRISE) ENTERPRISE,
					SUM(tb1.WIB) WIB,
					SUM(tb1.DIGITAL) DIGITAL,
					SUM(tb1.MOBILE) MOBILE,
					SUM(tb1.TOTAL) TOTAL
					FROM ( ";
		$query2 = "SELECT
					tb1.KODE_DIVISI,
					tb1.V_SHORT_DIVISI,
					SUM(tb1.PERSONEL_COST_DIVISI) PERSONEL_COST_DIVISI,
					SUM(tb1.CONSUMER) CONSUMER,
					SUM(tb1.ENTERPRISE) ENTERPRISE,
					SUM(tb1.WIB) WIB,
					SUM(tb1.DIGITAL) DIGITAL,
					SUM(tb1.MOBILE) MOBILE,
					SUM(tb1.TOTAL) TOTAL
					FROM ( ";

		$jml = count($blndis);
		$a=1;
		foreach ($blndis as $key => $value) {
			$query .= "(SELECT 
				dis.C_KODE_DIVISI, 
				map.divisi,
				map.CFU_FU,
				dis.PROSENTASE,
				(select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan".$value["BULAN"].") as REALISASI_SAP,
				(select SUM(coalesce(rel.REALISASI,0)) from t_realsap rel where rel.TAHUN=:tahun AND rel.BULAN <= :bulan".$value["BULAN"].") as REALISASI_SAP_SD_NOW, 
				round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan".$value["BULAN"].") * dis.PROSENTASE,10) as PERSONEL_COST, 
				round((select SUM(coalesce(rel.REALISASI,0)) from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN<=:bulan".$value["BULAN"].") * dis.PROSENTASE,10) as PERSONEL_COST_SD_NOW, 
				coalesce(round(preseg.PR_CONSUMER * ((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan".$value["BULAN"].") * dis.PROSENTASE ),10),0) as CONSUMER,
				coalesce(round(preseg.PR_ENTERPRISE * ((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan".$value["BULAN"].") * dis.PROSENTASE ),10),0) as ENTERPRISE,
				coalesce(round(preseg.PR_WIB * ((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan".$value["BULAN"].") * dis.PROSENTASE ),10),0) as WIB,
				coalesce(round(preseg.PR_DIGITAL * ((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan".$value["BULAN"].") * dis.PROSENTASE ),10),0) as DIGITAL,
				coalesce(round(preseg.PR_MOBILE * ((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan".$value["BULAN"].") * dis.PROSENTASE ),10),0) as MOBILE,

				coalesce(round(preseg.PR_CONSUMER * ((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan".$value["BULAN"].") * dis.PROSENTASE ),10),0) +
				coalesce(round(preseg.PR_ENTERPRISE * ((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan".$value["BULAN"].") * dis.PROSENTASE ),10),0) +
				coalesce(round(preseg.PR_WIB * ((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan".$value["BULAN"].") * dis.PROSENTASE ),10),0) +
				coalesce(round(preseg.PR_DIGITAL * ((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan".$value["BULAN"].") * dis.PROSENTASE ),10),0) +
				coalesce(round(preseg.PR_MOBILE * ((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan".$value["BULAN"].") * dis.PROSENTASE ),10),0) as TOTAL
				from t_pros_divisi dis
				JOIN m_mapping_cfu_fu map ON dis.C_KODE_DIVISI = map.KODE_DIVISI
				JOIN m_segmentasi men ON men.KODE = map.KODE_SEGMENTASI
				JOIN m_prosentasi_segment preseg ON preseg.TREG_NTREG = 'NON TREG' AND preseg.K_SEG = men.K_SEG
				WHERE map.JENIS <> 'NO' AND dis.BULAN=:bulan".$value["BULAN"]." AND dis.TAHUN=:tahun AND dis.C_KODE_DIVISI NOT IN('DIV-TR1','DIV-TR2','DIV-TR3','DIV-TR4','DIV-TR5','DIV-TR6','DIV-TR7')
				ORDER BY dis.C_KODE_DIVISI)";
			
			$query2 .= "(SELECT 
					sub5.KODE_DIVISI,
					pd.PROSENTASE,
					pd.V_SHORT_DIVISI,
					sum(sub5.PROSENTASE),
					sum(sub5.PERSONEL_COST_WITEL) as PERSONEL_COST_DIVISI,
					sum(sub5.CONSUMER) as CONSUMER,
					sum(sub5.ENTERPRISE) as ENTERPRISE,
					SUM(sub5.WIB) as WIB,
					SUM(sub5.DIGITAL) as DIGITAL,
					SUM(sub5.MOBILE) as MOBILE,
					SUM(sub5.TOTAL) as TOTAL
					FROM (SELECT
					sub4.KODE_DIVISI,
					sub4.C_HOST,
					sub4.WITEL,
					sub4.PROSENTASE,
					-- sub4.PERSONEL_COST_DIVISI,
					(sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) as PERSONEL_COST_WITEL,
					(sub4.CONSUMER*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) as CONSUMER,
					(sub4.ENTERPRISE*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) as ENTERPRISE,
					(sub4.WIB*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) as WIB,
					(sub4.DIGITAL*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) as DIGITAL,
					(sub4.MOBILE*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) as MOBILE,

					(
						(sub4.CONSUMER*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) +
						(sub4.ENTERPRISE*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) +
						(sub4.WIB*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) +
						(sub4.DIGITAL*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) +
						(sub4.MOBILE*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE)
					) as TOTAL

					FROM (SELECT
					sub3.*,
					el.PROSENTASE,
					((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan".$value["BULAN"].") * si.PROSENTASE) as PERSONEL_COST_DIVISI 
					FROM (SELECT 
					sub2.KODE_DIVISI,
					sub2.C_HOST,
					sub2.WITEL,
					(
						COALESCE(sub2.SOH*COALESCE((select sns.PR_CONSUMER from m_prosentasi_segment sns where sns.K_SEG='SOH'),0),0) +
						COALESCE(sub2.NOH*COALESCE((select sns.PR_CONSUMER from m_prosentasi_segment sns where sns.K_SEG='NOH'),0),0) +
						COALESCE(sub2.BOH*COALESCE((select sns.PR_CONSUMER from m_prosentasi_segment sns where sns.K_SEG='BOH'),0),0) +
						COALESCE(sub2.OOH*COALESCE((select sns.PR_CONSUMER from m_prosentasi_segment sns where sns.K_SEG='OOH'),0),0) +
						COALESCE(sub2.CON*COALESCE((select sns.PR_CONSUMER from m_prosentasi_segment sns where sns.K_SEG='CON' AND sns.TREG_NTREG='TREG'),0),0) +
						COALESCE(sub2.ENT*COALESCE((select sns.PR_CONSUMER from m_prosentasi_segment sns where sns.K_SEG='ENT' AND sns.TREG_NTREG='TREG'),0),0) +
						COALESCE(sub2.WIB*COALESCE((select sns.PR_CONSUMER from m_prosentasi_segment sns where sns.K_SEG='WIB' AND sns.TREG_NTREG='TREG'),0),0) 
					) as CONSUMER,
					(
						COALESCE(sub2.SOH*COALESCE((select sns.PR_ENTERPRISE from m_prosentasi_segment sns where sns.K_SEG='SOH'),0),0) +
						COALESCE(sub2.NOH*COALESCE((select sns.PR_ENTERPRISE from m_prosentasi_segment sns where sns.K_SEG='NOH'),0),0) +
						COALESCE(sub2.BOH*COALESCE((select sns.PR_ENTERPRISE from m_prosentasi_segment sns where sns.K_SEG='BOH'),0),0) +
						COALESCE(sub2.OOH*COALESCE((select sns.PR_ENTERPRISE from m_prosentasi_segment sns where sns.K_SEG='OOH'),0),0) +
						COALESCE(sub2.CON*COALESCE((select sns.PR_ENTERPRISE from m_prosentasi_segment sns where sns.K_SEG='CON' AND sns.TREG_NTREG='TREG'),0),0) +
						COALESCE(sub2.ENT*COALESCE((select sns.PR_ENTERPRISE from m_prosentasi_segment sns where sns.K_SEG='ENT' AND sns.TREG_NTREG='TREG'),0),0) +
						COALESCE(sub2.WIB*COALESCE((select sns.PR_ENTERPRISE from m_prosentasi_segment sns where sns.K_SEG='WIB' AND sns.TREG_NTREG='TREG'),0),0) 
					) as ENTERPRISE,	
					(
						COALESCE(sub2.SOH*COALESCE((select sns.PR_WIB from m_prosentasi_segment sns where sns.K_SEG='SOH'),0),0) +
						COALESCE(sub2.NOH*COALESCE((select sns.PR_WIB from m_prosentasi_segment sns where sns.K_SEG='NOH'),0),0) +
						COALESCE(sub2.BOH*COALESCE((select sns.PR_WIB from m_prosentasi_segment sns where sns.K_SEG='BOH'),0),0) +
						COALESCE(sub2.OOH*COALESCE((select sns.PR_WIB from m_prosentasi_segment sns where sns.K_SEG='OOH'),0),0) +
						COALESCE(sub2.CON*COALESCE((select sns.PR_WIB from m_prosentasi_segment sns where sns.K_SEG='CON' AND sns.TREG_NTREG='TREG'),0),0) +
						COALESCE(sub2.ENT*COALESCE((select sns.PR_WIB from m_prosentasi_segment sns where sns.K_SEG='ENT' AND sns.TREG_NTREG='TREG'),0),0) +
						COALESCE(sub2.WIB*COALESCE((select sns.PR_WIB from m_prosentasi_segment sns where sns.K_SEG='WIB' AND sns.TREG_NTREG='TREG'),0),0) 
					) as WIB,
					(
						COALESCE(sub2.SOH*COALESCE((select sns.PR_DIGITAL from m_prosentasi_segment sns where sns.K_SEG='SOH'),0),0) +
						COALESCE(sub2.NOH*COALESCE((select sns.PR_DIGITAL from m_prosentasi_segment sns where sns.K_SEG='NOH'),0),0) +
						COALESCE(sub2.BOH*COALESCE((select sns.PR_DIGITAL from m_prosentasi_segment sns where sns.K_SEG='BOH'),0),0) +
						COALESCE(sub2.OOH*COALESCE((select sns.PR_DIGITAL from m_prosentasi_segment sns where sns.K_SEG='OOH'),0),0) +
						COALESCE(sub2.CON*COALESCE((select sns.PR_DIGITAL from m_prosentasi_segment sns where sns.K_SEG='CON' AND sns.TREG_NTREG='TREG'),0),0) +
						COALESCE(sub2.ENT*COALESCE((select sns.PR_DIGITAL from m_prosentasi_segment sns where sns.K_SEG='ENT' AND sns.TREG_NTREG='TREG'),0),0) +
						COALESCE(sub2.WIB*COALESCE((select sns.PR_DIGITAL from m_prosentasi_segment sns where sns.K_SEG='WIB' AND sns.TREG_NTREG='TREG'),0),0) 
					) as DIGITAL,
					(
						COALESCE(sub2.SOH*COALESCE((select sns.PR_MOBILE from m_prosentasi_segment sns where sns.K_SEG='SOH'),0),0) +
						COALESCE(sub2.NOH*COALESCE((select sns.PR_MOBILE from m_prosentasi_segment sns where sns.K_SEG='NOH'),0),0) +
						COALESCE(sub2.BOH*COALESCE((select sns.PR_MOBILE from m_prosentasi_segment sns where sns.K_SEG='BOH'),0),0) +
						COALESCE(sub2.OOH*COALESCE((select sns.PR_MOBILE from m_prosentasi_segment sns where sns.K_SEG='OOH'),0),0) +
						COALESCE(sub2.CON*COALESCE((select sns.PR_MOBILE from m_prosentasi_segment sns where sns.K_SEG='CON' AND sns.TREG_NTREG='TREG'),0),0) +
						COALESCE(sub2.ENT*COALESCE((select sns.PR_MOBILE from m_prosentasi_segment sns where sns.K_SEG='ENT' AND sns.TREG_NTREG='TREG'),0),0) +
						COALESCE(sub2.WIB*COALESCE((select sns.PR_MOBILE from m_prosentasi_segment sns where sns.K_SEG='WIB' AND sns.TREG_NTREG='TREG'),0),0) 
					) as MOBILE
					FROM (SELECT
					rek.KODE_DIVISI, 
					rek.C_HOST,
					rek.WITEL,
					(rek.SOH/rek.GRAND_TOTAL) as SOH,
					(rek.NOH/rek.GRAND_TOTAL) as NOH,
					(rek.BOH/rek.GRAND_TOTAL) as BOH,
					(rek.OOH/rek.GRAND_TOTAL) as OOH,
					(rek.CON/rek.GRAND_TOTAL) as CON,
					(rek.ENT/rek.GRAND_TOTAL) as ENT,
					(rek.WIB/rek.GRAND_TOTAL) as WIB
					FROM (SELECT wtl.C_HOST,wtl.KODE_DIVISI,wtl.WITEL,
					(SELECT 
					wit.TOTAL
					FROM t_pros_witel_form wit WHERE wit.K_SEG = 'BOH' AND wit.C_HOST=wtl.C_HOST AND wit.BULAN=:bulan".$value["BULAN"]." AND wit.TAHUN=:tahun) BOH,
					(SELECT 
					wit.TOTAL
					FROM t_pros_witel_form wit WHERE wit.K_SEG = 'SOH' AND wit.C_HOST=wtl.C_HOST AND wit.BULAN=:bulan".$value["BULAN"]." AND wit.TAHUN=:tahun) SOH,
					(SELECT 
					wit.TOTAL
					FROM t_pros_witel_form wit WHERE wit.K_SEG = 'OOH' AND wit.C_HOST=wtl.C_HOST AND wit.BULAN=:bulan".$value["BULAN"]." AND wit.TAHUN=:tahun) OOH,
					(SELECT 
					wit.TOTAL
					FROM t_pros_witel_form wit WHERE wit.K_SEG = 'NOH' AND wit.C_HOST=wtl.C_HOST AND wit.BULAN=:bulan".$value["BULAN"]." AND wit.TAHUN=:tahun) NOH,
					(SELECT 
					wit.TOTAL
					FROM t_pros_witel_form wit WHERE wit.K_SEG = 'CON' AND wit.C_HOST=wtl.C_HOST AND wit.BULAN=:bulan".$value["BULAN"]." AND wit.TAHUN=:tahun) CON,
					(SELECT 
					wit.TOTAL
					FROM t_pros_witel_form wit WHERE wit.K_SEG = 'ENT' AND wit.C_HOST=wtl.C_HOST AND wit.BULAN=:bulan".$value["BULAN"]." AND wit.TAHUN=:tahun) ENT,
					(SELECT 
					wit.TOTAL
					FROM t_pros_witel_form wit WHERE wit.K_SEG = 'WIB' AND wit.C_HOST=wtl.C_HOST AND wit.BULAN=:bulan".$value["BULAN"]." AND wit.TAHUN=:tahun) WIB,
					(SELECT 
					SUM(wit.TOTAL) as TOTAL
					FROM t_pros_witel_form wit WHERE wit.C_HOST=wtl.C_HOST AND wit.BULAN=:bulan".$value["BULAN"]." AND wit.TAHUN=:tahun) GRAND_TOTAL
					FROM m_witel wtl) rek ) sub2) sub3
					JOIN t_pros_witel el ON el.C_HOST =  sub3.C_HOST AND el.BULAN=:bulan".$value["BULAN"]." AND el.TAHUN=:tahun
					JOIN t_pros_divisi si ON si.C_KODE_DIVISI = sub3.KODE_DIVISI AND si.TAHUN=:tahun AND si.BULAN=:bulan".$value["BULAN"].") sub4 ) sub5
					JOIN t_pros_divisi pd ON pd.C_KODE_DIVISI = sub5.KODE_DIVISI AND pd.TAHUN=:tahun AND pd.BULAN=:bulan".$value["BULAN"]."
					GROUP BY sub5.KODE_DIVISI)";
			if ($jml-$a != 0) {
				$query .= " UNION ";
				$query2 .= " UNION ";
			}else{
				$query .= ") tb1 GROUP BY tb1.C_KODE_DIVISI";
				$query2 .= ") tb1 GROUP BY tb1.KODE_DIVISI";
			}

			$a++;
		}

		$cmd1 = Yii::app()->db->createCommand($query);
		$cmd2 = Yii::app()->db->createCommand($query2);
		$cmd1->bindParam(':tahun',$tahun);
		$cmd2->bindParam(':tahun',$tahun);

		$querytot1 = "SELECT SUM(ini.PERSONEL_COST) PERSONEL_COST,SUM(ini.CONSUMER) CON,SUM(ini.ENTERPRISE) ENT,SUM(ini.WIB) WIB,SUM(ini.DIGITAL) DIG,SUM(ini.MOBILE) MOB,SUM(ini.TOTAL)  TOTAL FROM (".$query.") ini";
		$tot1 = Yii::app()->db->createCommand($querytot1);
		$tot1->bindParam(':tahun',$tahun);

		$querytot2 = "SELECT SUM(ini.PERSONEL_COST_DIVISI) PERSONEL_COST_DIVISI,SUM(ini.CONSUMER) CON,SUM(ini.ENTERPRISE) ENT,SUM(ini.WIB) WIB,SUM(ini.DIGITAL) DIG,SUM(ini.MOBILE) MOB,(SUM(ini.CONSUMER)+SUM(ini.ENTERPRISE)+SUM(ini.WIB)+SUM(ini.DIGITAL)+SUM(ini.MOBILE)) TOTAL FROM (".$query2.") ini";
		$tot2 = Yii::app()->db->createCommand($querytot2);
		$tot2->bindParam(':tahun',$tahun);

		$a = 1;
		foreach ($blndis as $key => $value) {
			$par = ':bulan'.$value["BULAN"];
			$tot1->bindParam($par,$value["BULAN"]);
			$tot2->bindParam($par,$value["BULAN"]);
			$cmd1->bindParam($par,$value["BULAN"]);
			$cmd2->bindParam($par,$value["BULAN"]);
			$a++;
		}
		$tot1 = $tot1->queryRow();
		$tot2 = $tot2->queryRow();
		$data = $cmd1->queryAll();
		$data2 = $cmd2->queryAll();


		$this->render('reportdivisinow',array(
			'tahun'=>$tahun,
			'bulan'=>$bulan,
			'data'=>$data,
			'data2'=>$data2,
			'tot1'=>$tot1,
			'tot2'=>$tot2
		));

	}

	public function actionReportdivisi($tahun,$bulan)
	{
		$query = "SELECT 
				dis.C_KODE_DIVISI,
				(select count(t.ID) from t_payslip_".$bulan."_".$tahun." t JOIN m_hr_current_".$bulan."_".$tahun." m 
								ON t.NIK = m.NIK WHERE m.KODE_DIVISI = dis.C_KODE_DIVISI) as JML_ORG,   
				map.divisi,
				map.CFU_FU,
				dis.PROSENTASE,
				(select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) as REALISASI_SAP,
				(select SUM(coalesce(rel.REALISASI,0)) from t_realsap rel where rel.TAHUN=:tahun AND rel.BULAN <= :bulan) as REALISASI_SAP_SD_NOW, 
				round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) as PERSONEL_COST, 
				round((select SUM(coalesce(rel.REALISASI,0)) from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN<=:bulan) * dis.PROSENTASE,10) as PERSONEL_COST_SD_NOW, 
				coalesce(round(preseg.PR_CONSUMER * ((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE ),10),0) as CONSUMER,
				coalesce(round(preseg.PR_ENTERPRISE * ((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE ),10),0) as ENTERPRISE,
				coalesce(round(preseg.PR_WIB * ((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE ),10),0) as WIB,
				coalesce(round(preseg.PR_DIGITAL * ((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE ),10),0) as DIGITAL,
				coalesce(round(preseg.PR_MOBILE * ((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE ),10),0) as MOBILE,

				coalesce(round(preseg.PR_CONSUMER * ((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE ),10),0) +
				coalesce(round(preseg.PR_ENTERPRISE * ((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE ),10),0) +
				coalesce(round(preseg.PR_WIB * ((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE ),10),0) +
				coalesce(round(preseg.PR_DIGITAL * ((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE ),10),0) +
				coalesce(round(preseg.PR_MOBILE * ((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE ),10),0) as TOTAL
				from t_pros_divisi dis
				JOIN m_mapping_cfu_fu map ON dis.C_KODE_DIVISI = map.KODE_DIVISI
				JOIN m_segmentasi men ON men.KODE = map.KODE_SEGMENTASI
				JOIN m_prosentasi_segment preseg ON preseg.TREG_NTREG = 'NON TREG' AND preseg.K_SEG = men.K_SEG
				WHERE map.JENIS <> 'NO' AND dis.BULAN=:bulan AND dis.TAHUN=:tahun AND dis.C_KODE_DIVISI NOT IN('DIV-TR1','DIV-TR2','DIV-TR3','DIV-TR4','DIV-TR5','DIV-TR6','DIV-TR7')
				ORDER BY dis.C_KODE_DIVISI";
		$sql =  Yii::app()->db->createCommand($query);
		$sql->bindParam(':tahun',$tahun);
		$sql->bindParam(':bulan',$bulan);
		$data = $sql->queryAll();

		$query2_ex = "SELECT 
					dis.C_KODE_DIVISI, 
					map.divisi,
					map.CFU_FU,
					dis.PROSENTASE,
					(select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) as REALISASI_SAP,
					(select SUM(coalesce(rel.REALISASI,0)) from t_realsap rel where rel.TAHUN=:tahun AND rel.BULAN <= rel.BULAN=:bulan) as REALISASI_SAP_SD_NOW, 
					round((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * dis.PROSENTASE,10) as PERSONEL_COST, 
					round((select SUM(coalesce(rel.REALISASI,0)) from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN<=6) * dis.PROSENTASE,10) as PERSONEL_COST_SD_NOW, 

					coalesce(round(
					(SELECT 
					SUM(cf.PROSENTASE / (select SUM(f.PROSENTASE) FROM `t_pros_cfu_fu` f WHERE f.TREG_NTREG = 'TREG') * (SELECT t_pr.PROSENTASE FROM t_pros_divisi t_pr WHERE t_pr.C_KODE_DIVISI = dis.C_KODE_DIVISI AND t_pr.TAHUN=:tahun AND t_pr.BULAN =:bulan) * pr.PR_CONSUMER) as CONSUMER
					FROM `t_pros_cfu_fu` cf 
					JOIN m_segmentasi men ON men.KODE = cf.SEGMENTASI_KODE
					JOIN m_prosentasi_segment pr ON pr.TREG_NTREG = 'TREG' AND pr.K_SEG = men.K_SEG 
					WHERE cf.TREG_NTREG = 'TREG')   
					* ((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan)),10),0) as CONSUMER,

					coalesce(round(
					(SELECT 
					SUM(cf.PROSENTASE / (select SUM(f.PROSENTASE) FROM `t_pros_cfu_fu` f WHERE f.TREG_NTREG = 'TREG') * (SELECT t_pr.PROSENTASE FROM t_pros_divisi t_pr WHERE t_pr.C_KODE_DIVISI = dis.C_KODE_DIVISI AND t_pr.TAHUN=:tahun AND t_pr.BULAN =:bulan) * pr.PR_ENTERPRISE) as ENTERPRISE
					FROM `t_pros_cfu_fu` cf 
					JOIN m_segmentasi men ON men.KODE = cf.SEGMENTASI_KODE
					JOIN m_prosentasi_segment pr ON pr.TREG_NTREG = 'TREG' AND pr.K_SEG = men.K_SEG 
					WHERE cf.TREG_NTREG = 'TREG') 
					* ((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan)),10),0) as ENTERPRISE,

					coalesce(round(
					(SELECT 
					SUM(cf.PROSENTASE / (select SUM(f.PROSENTASE) FROM `t_pros_cfu_fu` f WHERE f.TREG_NTREG = 'TREG') * (SELECT t_pr.PROSENTASE FROM t_pros_divisi t_pr WHERE t_pr.C_KODE_DIVISI = dis.C_KODE_DIVISI AND t_pr.TAHUN=:tahun AND t_pr.BULAN =:bulan) * pr.PR_WIB) as WIB
					FROM `t_pros_cfu_fu` cf 
					JOIN m_segmentasi men ON men.KODE = cf.SEGMENTASI_KODE
					JOIN m_prosentasi_segment pr ON pr.TREG_NTREG = 'TREG' AND pr.K_SEG = men.K_SEG 
					WHERE cf.TREG_NTREG = 'TREG') 
					* ((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan)),10),0) as WIB,

					coalesce(round(
					(SELECT 
					SUM(cf.PROSENTASE / (select SUM(f.PROSENTASE) FROM `t_pros_cfu_fu` f WHERE f.TREG_NTREG = 'TREG') * (SELECT t_pr.PROSENTASE FROM t_pros_divisi t_pr WHERE t_pr.C_KODE_DIVISI = dis.C_KODE_DIVISI AND t_pr.TAHUN=:tahun AND t_pr.BULAN =:bulan) * pr.PR_DIGITAL) as DIGITAL
					FROM `t_pros_cfu_fu` cf 
					JOIN m_segmentasi men ON men.KODE = cf.SEGMENTASI_KODE
					JOIN m_prosentasi_segment pr ON pr.TREG_NTREG = 'TREG' AND pr.K_SEG = men.K_SEG 
					WHERE cf.TREG_NTREG = 'TREG')
					* ((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan)),10),0) as DIGITAL,

					coalesce(round(
					(SELECT 
					SUM(cf.PROSENTASE / (select SUM(f.PROSENTASE) FROM `t_pros_cfu_fu` f WHERE f.TREG_NTREG = 'TREG') * (SELECT t_pr.PROSENTASE FROM t_pros_divisi t_pr WHERE t_pr.C_KODE_DIVISI = dis.C_KODE_DIVISI AND t_pr.TAHUN=:tahun AND t_pr.BULAN =:bulan) * pr.PR_MOBILE) as MOBILE
					FROM `t_pros_cfu_fu` cf 
					JOIN m_segmentasi men ON men.KODE = cf.SEGMENTASI_KODE
					JOIN m_prosentasi_segment pr ON pr.TREG_NTREG = 'TREG' AND pr.K_SEG = men.K_SEG 
					WHERE cf.TREG_NTREG = 'TREG') 
					* ((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan)),10),0) as MOBILE,
					
					(coalesce(round(
					(SELECT 
					SUM(cf.PROSENTASE / (select SUM(f.PROSENTASE) FROM `t_pros_cfu_fu` f WHERE f.TREG_NTREG = 'TREG') * (SELECT t_pr.PROSENTASE FROM t_pros_divisi t_pr WHERE t_pr.C_KODE_DIVISI = dis.C_KODE_DIVISI AND t_pr.TAHUN=:tahun AND t_pr.BULAN =:bulan) * pr.PR_CONSUMER) as CONSUMER
					FROM `t_pros_cfu_fu` cf 
					JOIN m_segmentasi men ON men.KODE = cf.SEGMENTASI_KODE
					JOIN m_prosentasi_segment pr ON pr.TREG_NTREG = 'TREG' AND pr.K_SEG = men.K_SEG 
					WHERE cf.TREG_NTREG = 'TREG')   
					* ((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan)),10),0) +

					coalesce(round(
					(SELECT 
					SUM(cf.PROSENTASE / (select SUM(f.PROSENTASE) FROM `t_pros_cfu_fu` f WHERE f.TREG_NTREG = 'TREG') * (SELECT t_pr.PROSENTASE FROM t_pros_divisi t_pr WHERE t_pr.C_KODE_DIVISI = dis.C_KODE_DIVISI AND t_pr.TAHUN=:tahun AND t_pr.BULAN =:bulan) * pr.PR_ENTERPRISE) as ENTERPRISE
					FROM `t_pros_cfu_fu` cf 
					JOIN m_segmentasi men ON men.KODE = cf.SEGMENTASI_KODE
					JOIN m_prosentasi_segment pr ON pr.TREG_NTREG = 'TREG' AND pr.K_SEG = men.K_SEG 
					WHERE cf.TREG_NTREG = 'TREG') 
					* ((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan)),10),0) +

					coalesce(round(
					(SELECT 
					SUM(cf.PROSENTASE / (select SUM(f.PROSENTASE) FROM `t_pros_cfu_fu` f WHERE f.TREG_NTREG = 'TREG') * (SELECT t_pr.PROSENTASE FROM t_pros_divisi t_pr WHERE t_pr.C_KODE_DIVISI = dis.C_KODE_DIVISI AND t_pr.TAHUN=:tahun AND t_pr.BULAN =:bulan) * pr.PR_WIB) as WIB
					FROM `t_pros_cfu_fu` cf 
					JOIN m_segmentasi men ON men.KODE = cf.SEGMENTASI_KODE
					JOIN m_prosentasi_segment pr ON pr.TREG_NTREG = 'TREG' AND pr.K_SEG = men.K_SEG 
					WHERE cf.TREG_NTREG = 'TREG') 
					* ((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan)),10),0) +

					coalesce(round(
					(SELECT 
					SUM(cf.PROSENTASE / (select SUM(f.PROSENTASE) FROM `t_pros_cfu_fu` f WHERE f.TREG_NTREG = 'TREG') * (SELECT t_pr.PROSENTASE FROM t_pros_divisi t_pr WHERE t_pr.C_KODE_DIVISI = dis.C_KODE_DIVISI AND t_pr.TAHUN=:tahun AND t_pr.BULAN =:bulan) * pr.PR_DIGITAL) as DIGITAL
					FROM `t_pros_cfu_fu` cf 
					JOIN m_segmentasi men ON men.KODE = cf.SEGMENTASI_KODE
					JOIN m_prosentasi_segment pr ON pr.TREG_NTREG = 'TREG' AND pr.K_SEG = men.K_SEG 
					WHERE cf.TREG_NTREG = 'TREG')
					* ((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan)),10),0) +

					coalesce(round(
					(SELECT 
					SUM(cf.PROSENTASE / (select SUM(f.PROSENTASE) FROM `t_pros_cfu_fu` f WHERE f.TREG_NTREG = 'TREG') * (SELECT t_pr.PROSENTASE FROM t_pros_divisi t_pr WHERE t_pr.C_KODE_DIVISI = dis.C_KODE_DIVISI AND t_pr.TAHUN=:tahun AND t_pr.BULAN =:bulan) * pr.PR_MOBILE) as MOBILE
					FROM `t_pros_cfu_fu` cf 
					JOIN m_segmentasi men ON men.KODE = cf.SEGMENTASI_KODE
					JOIN m_prosentasi_segment pr ON pr.TREG_NTREG = 'TREG' AND pr.K_SEG = men.K_SEG 
					WHERE cf.TREG_NTREG = 'TREG') 
					* ((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan)),10),0)) as JML
					

					-- coalesce(round(preseg.PR_CONSUMER * ((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan)),10),0) +
					-- coalesce(round(preseg.PR_ENTERPRISE * ((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan)),10),0) +
					-- coalesce(round(preseg.PR_WIB * ((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan)),10),0) +
					-- coalesce(round(preseg.PR_DIGITAL * ((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan)),10),0) +
					-- coalesce(round(preseg.PR_MOBILE * ((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan)),10),0) as TOTAL
					from t_pros_divisi dis
					JOIN m_mapping_cfu_fu map ON dis.C_KODE_DIVISI = map.KODE_DIVISI
					JOIN m_segmentasi men ON men.KODE = map.KODE_SEGMENTASI
					JOIN m_prosentasi_segment preseg ON preseg.TREG_NTREG = 'NON TREG' AND preseg.K_SEG = men.K_SEG
					WHERE dis.BULAN=:bulan AND dis.TAHUN=:tahun AND dis.C_KODE_DIVISI IN('DIV-TR1','DIV-TR2','DIV-TR3','DIV-TR4','DIV-TR5','DIV-TR6','DIV-TR7') ";
		$query2 ="SELECT 
					sub5.KODE_DIVISI,
					(select count(t.ID) from t_payslip_".$bulan."_".$tahun." t JOIN m_hr_current_".$bulan."_".$tahun." m 
								ON t.NIK = m.NIK WHERE m.KODE_DIVISI = sub5.KODE_DIVISI) as JML_ORG,
					pd.PROSENTASE,
					pd.V_SHORT_DIVISI,
					sum(sub5.PROSENTASE),
					sum(sub5.PERSONEL_COST_WITEL) as PERSONEL_COST_DIVISI,
					sum(sub5.CONSUMER) as CONSUMER,
					sum(sub5.ENTERPRISE) as ENTERPRISE,
					SUM(sub5.WIB) as WIB,
					SUM(sub5.DIGITAL) as DIGITAL,
					SUM(sub5.MOBILE) as MOBILE,
					SUM(sub5.TOTAL) as TOTAL
					FROM (SELECT
					sub4.KODE_DIVISI,
					sub4.C_HOST,
					sub4.WITEL,
					sub4.PROSENTASE,
					-- sub4.PERSONEL_COST_DIVISI,
					(sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) as PERSONEL_COST_WITEL,
					(sub4.CONSUMER*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) as CONSUMER,
					(sub4.ENTERPRISE*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) as ENTERPRISE,
					(sub4.WIB*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) as WIB,
					(sub4.DIGITAL*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) as DIGITAL,
					(sub4.MOBILE*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) as MOBILE,

					(
						(sub4.CONSUMER*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) +
						(sub4.ENTERPRISE*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) +
						(sub4.WIB*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) +
						(sub4.DIGITAL*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE) +
						(sub4.MOBILE*sub4.PERSONEL_COST_DIVISI*sub4.PROSENTASE)
					) as TOTAL

					FROM (SELECT
					sub3.*,
					el.PROSENTASE,
					((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) * si.PROSENTASE) as PERSONEL_COST_DIVISI 
					FROM (SELECT 
					sub2.KODE_DIVISI,
					sub2.C_HOST,
					sub2.WITEL,
					(
						COALESCE(sub2.SOH*COALESCE((select sns.PR_CONSUMER from m_prosentasi_segment sns where sns.K_SEG='SOH'),0),0) +
						COALESCE(sub2.NOH*COALESCE((select sns.PR_CONSUMER from m_prosentasi_segment sns where sns.K_SEG='NOH'),0),0) +
						COALESCE(sub2.BOH*COALESCE((select sns.PR_CONSUMER from m_prosentasi_segment sns where sns.K_SEG='BOH'),0),0) +
						COALESCE(sub2.OOH*COALESCE((select sns.PR_CONSUMER from m_prosentasi_segment sns where sns.K_SEG='OOH'),0),0) +
						COALESCE(sub2.CON*COALESCE((select sns.PR_CONSUMER from m_prosentasi_segment sns where sns.K_SEG='CON' AND sns.TREG_NTREG='TREG'),0),0) +
						COALESCE(sub2.ENT*COALESCE((select sns.PR_CONSUMER from m_prosentasi_segment sns where sns.K_SEG='ENT' AND sns.TREG_NTREG='TREG'),0),0) +
						COALESCE(sub2.WIB*COALESCE((select sns.PR_CONSUMER from m_prosentasi_segment sns where sns.K_SEG='WIB' AND sns.TREG_NTREG='TREG'),0),0) 
					) as CONSUMER,
					(
						COALESCE(sub2.SOH*COALESCE((select sns.PR_ENTERPRISE from m_prosentasi_segment sns where sns.K_SEG='SOH'),0),0) +
						COALESCE(sub2.NOH*COALESCE((select sns.PR_ENTERPRISE from m_prosentasi_segment sns where sns.K_SEG='NOH'),0),0) +
						COALESCE(sub2.BOH*COALESCE((select sns.PR_ENTERPRISE from m_prosentasi_segment sns where sns.K_SEG='BOH'),0),0) +
						COALESCE(sub2.OOH*COALESCE((select sns.PR_ENTERPRISE from m_prosentasi_segment sns where sns.K_SEG='OOH'),0),0) +
						COALESCE(sub2.CON*COALESCE((select sns.PR_ENTERPRISE from m_prosentasi_segment sns where sns.K_SEG='CON' AND sns.TREG_NTREG='TREG'),0),0) +
						COALESCE(sub2.ENT*COALESCE((select sns.PR_ENTERPRISE from m_prosentasi_segment sns where sns.K_SEG='ENT' AND sns.TREG_NTREG='TREG'),0),0) +
						COALESCE(sub2.WIB*COALESCE((select sns.PR_ENTERPRISE from m_prosentasi_segment sns where sns.K_SEG='WIB' AND sns.TREG_NTREG='TREG'),0),0) 
					) as ENTERPRISE,	
					(
						COALESCE(sub2.SOH*COALESCE((select sns.PR_WIB from m_prosentasi_segment sns where sns.K_SEG='SOH'),0),0) +
						COALESCE(sub2.NOH*COALESCE((select sns.PR_WIB from m_prosentasi_segment sns where sns.K_SEG='NOH'),0),0) +
						COALESCE(sub2.BOH*COALESCE((select sns.PR_WIB from m_prosentasi_segment sns where sns.K_SEG='BOH'),0),0) +
						COALESCE(sub2.OOH*COALESCE((select sns.PR_WIB from m_prosentasi_segment sns where sns.K_SEG='OOH'),0),0) +
						COALESCE(sub2.CON*COALESCE((select sns.PR_WIB from m_prosentasi_segment sns where sns.K_SEG='CON' AND sns.TREG_NTREG='TREG'),0),0) +
						COALESCE(sub2.ENT*COALESCE((select sns.PR_WIB from m_prosentasi_segment sns where sns.K_SEG='ENT' AND sns.TREG_NTREG='TREG'),0),0) +
						COALESCE(sub2.WIB*COALESCE((select sns.PR_WIB from m_prosentasi_segment sns where sns.K_SEG='WIB' AND sns.TREG_NTREG='TREG'),0),0) 
					) as WIB,
					(
						COALESCE(sub2.SOH*COALESCE((select sns.PR_DIGITAL from m_prosentasi_segment sns where sns.K_SEG='SOH'),0),0) +
						COALESCE(sub2.NOH*COALESCE((select sns.PR_DIGITAL from m_prosentasi_segment sns where sns.K_SEG='NOH'),0),0) +
						COALESCE(sub2.BOH*COALESCE((select sns.PR_DIGITAL from m_prosentasi_segment sns where sns.K_SEG='BOH'),0),0) +
						COALESCE(sub2.OOH*COALESCE((select sns.PR_DIGITAL from m_prosentasi_segment sns where sns.K_SEG='OOH'),0),0) +
						COALESCE(sub2.CON*COALESCE((select sns.PR_DIGITAL from m_prosentasi_segment sns where sns.K_SEG='CON' AND sns.TREG_NTREG='TREG'),0),0) +
						COALESCE(sub2.ENT*COALESCE((select sns.PR_DIGITAL from m_prosentasi_segment sns where sns.K_SEG='ENT' AND sns.TREG_NTREG='TREG'),0),0) +
						COALESCE(sub2.WIB*COALESCE((select sns.PR_DIGITAL from m_prosentasi_segment sns where sns.K_SEG='WIB' AND sns.TREG_NTREG='TREG'),0),0) 
					) as DIGITAL,
					(
						COALESCE(sub2.SOH*COALESCE((select sns.PR_MOBILE from m_prosentasi_segment sns where sns.K_SEG='SOH'),0),0) +
						COALESCE(sub2.NOH*COALESCE((select sns.PR_MOBILE from m_prosentasi_segment sns where sns.K_SEG='NOH'),0),0) +
						COALESCE(sub2.BOH*COALESCE((select sns.PR_MOBILE from m_prosentasi_segment sns where sns.K_SEG='BOH'),0),0) +
						COALESCE(sub2.OOH*COALESCE((select sns.PR_MOBILE from m_prosentasi_segment sns where sns.K_SEG='OOH'),0),0) +
						COALESCE(sub2.CON*COALESCE((select sns.PR_MOBILE from m_prosentasi_segment sns where sns.K_SEG='CON' AND sns.TREG_NTREG='TREG'),0),0) +
						COALESCE(sub2.ENT*COALESCE((select sns.PR_MOBILE from m_prosentasi_segment sns where sns.K_SEG='ENT' AND sns.TREG_NTREG='TREG'),0),0) +
						COALESCE(sub2.WIB*COALESCE((select sns.PR_MOBILE from m_prosentasi_segment sns where sns.K_SEG='WIB' AND sns.TREG_NTREG='TREG'),0),0) 
					) as MOBILE
					FROM (SELECT
					rek.KODE_DIVISI, 
					rek.C_HOST,
					rek.WITEL,
					(rek.SOH/rek.GRAND_TOTAL) as SOH,
					(rek.NOH/rek.GRAND_TOTAL) as NOH,
					(rek.BOH/rek.GRAND_TOTAL) as BOH,
					(rek.OOH/rek.GRAND_TOTAL) as OOH,
					(rek.CON/rek.GRAND_TOTAL) as CON,
					(rek.ENT/rek.GRAND_TOTAL) as ENT,
					(rek.WIB/rek.GRAND_TOTAL) as WIB
					FROM (SELECT wtl.C_HOST,wtl.KODE_DIVISI,wtl.WITEL,
					(SELECT 
					wit.TOTAL
					FROM t_pros_witel_form wit WHERE wit.K_SEG = 'BOH' AND wit.C_HOST=wtl.C_HOST AND wit.BULAN=:bulan AND wit.TAHUN=:tahun) BOH,
					(SELECT 
					wit.TOTAL
					FROM t_pros_witel_form wit WHERE wit.K_SEG = 'SOH' AND wit.C_HOST=wtl.C_HOST AND wit.BULAN=:bulan AND wit.TAHUN=:tahun) SOH,
					(SELECT 
					wit.TOTAL
					FROM t_pros_witel_form wit WHERE wit.K_SEG = 'OOH' AND wit.C_HOST=wtl.C_HOST AND wit.BULAN=:bulan AND wit.TAHUN=:tahun) OOH,
					(SELECT 
					wit.TOTAL
					FROM t_pros_witel_form wit WHERE wit.K_SEG = 'NOH' AND wit.C_HOST=wtl.C_HOST AND wit.BULAN=:bulan AND wit.TAHUN=:tahun) NOH,
					(SELECT 
					wit.TOTAL
					FROM t_pros_witel_form wit WHERE wit.K_SEG = 'CON' AND wit.C_HOST=wtl.C_HOST AND wit.BULAN=:bulan AND wit.TAHUN=:tahun) CON,
					(SELECT 
					wit.TOTAL
					FROM t_pros_witel_form wit WHERE wit.K_SEG = 'ENT' AND wit.C_HOST=wtl.C_HOST AND wit.BULAN=:bulan AND wit.TAHUN=:tahun) ENT,
					(SELECT 
					wit.TOTAL
					FROM t_pros_witel_form wit WHERE wit.K_SEG = 'WIB' AND wit.C_HOST=wtl.C_HOST AND wit.BULAN=:bulan AND wit.TAHUN=:tahun) WIB,
					(SELECT 
					SUM(wit.TOTAL) as TOTAL
					FROM t_pros_witel_form wit WHERE wit.C_HOST=wtl.C_HOST AND wit.BULAN=:bulan AND wit.TAHUN=:tahun) GRAND_TOTAL
					FROM m_witel wtl) rek ) sub2) sub3
					JOIN t_pros_witel el ON el.C_HOST =  sub3.C_HOST AND el.BULAN=:bulan AND el.TAHUN=:tahun
					JOIN t_pros_divisi si ON si.C_KODE_DIVISI = sub3.KODE_DIVISI AND si.TAHUN=:tahun AND si.BULAN=:bulan) sub4 ) sub5
					JOIN t_pros_divisi pd ON pd.C_KODE_DIVISI = sub5.KODE_DIVISI AND pd.TAHUN=:tahun AND pd.BULAN=:bulan
					GROUP BY sub5.KODE_DIVISI";
		$sql2 = Yii::app()->db->createCommand($query2);
		$sql2->bindParam(':tahun',$tahun);
		$sql2->bindParam(':bulan',$bulan);
		$data2 = $sql2->queryAll();
		$querytotseg = "SELECT  
						SUM(coalesce((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = map.JENIS AND seg.SEGMENT = 'CON'),0) * (coalesce((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) ,0) * dis.PROSENTASE )) as CONSUMER,
						SUM(coalesce((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = map.JENIS AND seg.SEGMENT = 'ENT'),0) * (coalesce((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) ,0) * dis.PROSENTASE )) as ENTERPRISE,
						SUM(coalesce((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = map.JENIS AND seg.SEGMENT = 'WIB'),0) * (coalesce((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) ,0) * dis.PROSENTASE )) as WIB,
						SUM(coalesce((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = map.JENIS AND seg.SEGMENT = 'DIGITAL'),0) * (coalesce((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) ,0) * dis.PROSENTASE )) as DIGITAL,
						SUM(coalesce((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = map.JENIS AND seg.SEGMENT = 'MOBILE'),0) * (coalesce((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) ,0) * dis.PROSENTASE )) as MOBILE,

						SUM(coalesce((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = map.JENIS AND seg.SEGMENT = 'CON'),0) * (coalesce((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) ,0) * dis.PROSENTASE )) +
						SUM(coalesce((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = map.JENIS AND seg.SEGMENT = 'ENT'),0) * (coalesce((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) ,0) * dis.PROSENTASE )) +
						SUM(coalesce((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = map.JENIS AND seg.SEGMENT = 'WIB'),0) * (coalesce((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) ,0) * dis.PROSENTASE )) +
						SUM(coalesce((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = map.JENIS AND seg.SEGMENT = 'DIGITAL'),0) * (coalesce((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) ,0) * dis.PROSENTASE )) +
						SUM(coalesce((select seg.PROSENTASE from m_pres_segment seg where seg.ORI_SEG = map.JENIS AND seg.SEGMENT = 'MOBILE'),0) * (coalesce((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan) ,0) * dis.PROSENTASE )) as TOTAL_SEGMEN,

						SUM(coalesce((select rel.REALISASI from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN=:bulan),0) * dis.PROSENTASE) as PERSONEL_COST, 
						SUM(coalesce((select SUM(coalesce(rel.REALISASI,0)) from t_realsap rel where rel.TAHUN=:tahun and rel.BULAN<=:bulan),0) * dis.PROSENTASE) as PERSONEL_COST_SD_NOW,
						SUM(dis.PROSENTASE) as PROSENTASE

						from t_pros_divisi dis
						JOIN m_mapping_cfu_fu map ON dis.C_KODE_DIVISI = map.KODE_DIVISI
						WHERE dis.BULAN = :bulan AND dis.TAHUN = :tahun"; 
		$sqltotseg = Yii::app()->db->createCommand($querytotseg);
		$sqltotseg->bindParam(':bulan',$bulan);
		$sqltotseg->bindParam(':tahun',$tahun);
		$totseg = $sqltotseg->queryRow();

		$querytot1 = "SELECT SUM(ini.CONSUMER) CON,SUM(ini.ENTERPRISE) ENT,SUM(ini.WIB) WIB,SUM(ini.DIGITAL) DIG,SUM(ini.MOBILE) MOB,SUM(ini.TOTAL)  TOTAL FROM (".$query.") ini";
		$tot1 = Yii::app()->db->createCommand($querytot1);
		$tot1->bindParam(':bulan',$bulan);
		$tot1->bindParam(':tahun',$tahun);
		$tot1 = $tot1->queryRow();

		$querytot2 = "SELECT SUM(ini.CONSUMER) CON,SUM(ini.ENTERPRISE) ENT,SUM(ini.WIB) WIB,SUM(ini.DIGITAL) DIG,SUM(ini.MOBILE) MOB,(SUM(ini.CONSUMER)+SUM(ini.ENTERPRISE)+SUM(ini.WIB)+SUM(ini.DIGITAL)+SUM(ini.MOBILE)) TOTAL FROM (".$query2.") ini";
		$tot2 = Yii::app()->db->createCommand($querytot2);
		$tot2->bindParam(':bulan',$bulan);
		$tot2->bindParam(':tahun',$tahun);
		$tot2 = $tot2->queryRow();

		$totorg = Yii::app()->db->createCommand("SELECT 
		count(atk.NIK) as JML_ORG
		FROM m_mapping_atk_".$bulan."_".$tahun." atk 
		JOIN m_segmentasi seg ON seg.KODE = atk.SEGMENTASI_KODE
		JOIN t_payslip_".$bulan."_".$tahun." py ON py.NIK = atk.NIK")->queryScalar();
		// var_dump($tot2);exit();
		$this->render('reportdivisi',array(
			'tahun'=>$tahun,
			'bulan'=>$bulan,
			'data'=>$data,
			'data2'=>$data2,
			'totseg'=>$totseg,
			'tot1'=>$tot1,
			'tot2'=>$tot2,
			'totorg'=>$totorg
		));
	}

	public function actionReportglobal($tahun,$bulan)
	{
		$tsoh = TProsCfuFu::model()->findByAttributes(array('SEGMENTASI_KODE'=>'T-SOH','TAHUN'=>$tahun,'BULAN'=>$bulan)); 
		$tnoh = TProsCfuFu::model()->findByAttributes(array('SEGMENTASI_KODE'=>'T-NOH','TAHUN'=>$tahun,'BULAN'=>$bulan)); 
		$tboh = TProsCfuFu::model()->findByAttributes(array('SEGMENTASI_KODE'=>'T-BSO','TAHUN'=>$tahun,'BULAN'=>$bulan)); 
		$tooh = TProsCfuFu::model()->findByAttributes(array('SEGMENTASI_KODE'=>'T-OOH','TAHUN'=>$tahun,'BULAN'=>$bulan)); 
		$tcon = TProsCfuFu::model()->findByAttributes(array('SEGMENTASI_KODE'=>'T-CFU-CON','TAHUN'=>$tahun,'BULAN'=>$bulan)); 
		$tent = TProsCfuFu::model()->findByAttributes(array('SEGMENTASI_KODE'=>'T-CFU-ENT','TAHUN'=>$tahun,'BULAN'=>$bulan)); 
		$twib = TProsCfuFu::model()->findByAttributes(array('SEGMENTASI_KODE'=>'T-CFU-WIB','TAHUN'=>$tahun,'BULAN'=>$bulan)); 
		
		$ncon = TProsCfuFu::model()->findByAttributes(array('SEGMENTASI_KODE'=>'N-CFU-CON','TAHUN'=>$tahun,'BULAN'=>$bulan)); 
		$nent = TProsCfuFu::model()->findByAttributes(array('SEGMENTASI_KODE'=>'N-CFU-ENT','TAHUN'=>$tahun,'BULAN'=>$bulan)); 
		$nwib = TProsCfuFu::model()->findByAttributes(array('SEGMENTASI_KODE'=>'N-CFU-WIB','TAHUN'=>$tahun,'BULAN'=>$bulan)); 
		$ndig = TProsCfuFu::model()->findByAttributes(array('SEGMENTASI_KODE'=>'N-CFU-DIG','TAHUN'=>$tahun,'BULAN'=>$bulan)); 
		$nmobile = TProsCfuFu::model()->findByAttributes(array('SEGMENTASI_KODE'=>'N-CFU-MOB','TAHUN'=>$tahun,'BULAN'=>$bulan)); 
		$sqlfu = Yii::app()->db->createCommand("SELECT SUM(a.PROSENTASE) FROM t_pros_cfu_fu a JOIN m_segmentasi seg ON a.SEGMENTASI_KODE = seg.KODE WHERE seg.TYPE = :fu AND a.BULAN = :bulan AND a.TAHUN = :tahun;");
		$fu = 'FU';
		$sqlfu->bindParam(':fu',$fu);
		$sqlfu->bindParam(':bulan',$bulan);
		$sqlfu->bindParam(':tahun',$tahun);
		$prosfu = $sqlfu->queryScalar();

		$realmonth = TRealsap::model()->findByAttributes(array('TAHUN'=>$tahun,'BULAN'=>$bulan));
		// var_dump($realmonth);exit();
		$j1 = $tsoh->PROSENTASE*$realmonth->REALISASI;
		$j2 = $tnoh->PROSENTASE*$realmonth->REALISASI;
		$j3 = $tboh->PROSENTASE*$realmonth->REALISASI;
		$j4 = $tooh->PROSENTASE*$realmonth->REALISASI;
		$j5 = $tcon->PROSENTASE*$realmonth->REALISASI;
		$j6 = $tent->PROSENTASE*$realmonth->REALISASI;
		$j7 = $twib->PROSENTASE*$realmonth->REALISASI;
		$j8 = $ncon->PROSENTASE*$realmonth->REALISASI;
		$j9 = $nent->PROSENTASE*$realmonth->REALISASI;
		$j10 = $nwib->PROSENTASE*$realmonth->REALISASI;
		$j11 = $ndig->PROSENTASE*$realmonth->REALISASI;
		$j12 = $nmob->PROSENTASE*$realmonth->REALISASI;
		$j13 = $prosfu*$realmonth->REALISASI;

		$total = $j1+$j2+$j3+$j4+$j5+$j6+$j7+$j8+$j9+$j10+$j11+$j12+$j13;

		$jml1 = Yii::app()->db->createCommand("SELECT 
		count(atk.NIK) as JML_ORG
		FROM m_mapping_atk_".$bulan."_".$tahun." atk 
		JOIN m_segmentasi seg ON seg.KODE = atk.SEGMENTASI_KODE
		JOIN t_payslip_".$bulan."_".$tahun." py ON py.NIK = atk.NIK
		WHERE TREG_NTREG = 'NON TREG';")->queryScalar();
		$jml2 = Yii::app()->db->createCommand("SELECT 
		count(atk.NIK) as JML_ORG
		FROM m_mapping_atk_".$bulan."_".$tahun." atk 
		JOIN m_segmentasi seg ON seg.KODE = atk.SEGMENTASI_KODE
		JOIN t_payslip_".$bulan."_".$tahun." py ON py.NIK = atk.NIK
		WHERE TREG_NTREG = 'TREG';")->queryScalar();
		$jml3 = Yii::app()->db->createCommand("SELECT 
		count(atk.NIK) as JML_ORG
		FROM m_mapping_atk_".$bulan."_".$tahun." atk 
		JOIN m_segmentasi seg ON seg.KODE = atk.SEGMENTASI_KODE
		JOIN t_payslip_".$bulan."_".$tahun." py ON py.NIK = atk.NIK
		WHERE seg.K_SEG = 'SOH' AND TREG_NTREG = 'TREG';")->queryScalar();
		$jml4 = Yii::app()->db->createCommand("SELECT 
		count(atk.NIK) as JML_ORG
		FROM m_mapping_atk_".$bulan."_".$tahun." atk 
		JOIN m_segmentasi seg ON seg.KODE = atk.SEGMENTASI_KODE
		JOIN t_payslip_".$bulan."_".$tahun." py ON py.NIK = atk.NIK
		WHERE seg.K_SEG = 'NOH' AND TREG_NTREG = 'TREG';")->queryScalar();
		$jml5 = Yii::app()->db->createCommand("SELECT 
		count(atk.NIK) as JML_ORG
		FROM m_mapping_atk_".$bulan."_".$tahun." atk 
		JOIN m_segmentasi seg ON seg.KODE = atk.SEGMENTASI_KODE
		JOIN t_payslip_".$bulan."_".$tahun." py ON py.NIK = atk.NIK
		WHERE seg.K_SEG = 'BOH' AND TREG_NTREG = 'TREG';")->queryScalar();
		$jml6 = Yii::app()->db->createCommand("SELECT 
		count(atk.NIK) as JML_ORG
		FROM m_mapping_atk_".$bulan."_".$tahun." atk 
		JOIN m_segmentasi seg ON seg.KODE = atk.SEGMENTASI_KODE
		JOIN t_payslip_".$bulan."_".$tahun." py ON py.NIK = atk.NIK
		WHERE seg.K_SEG = 'OOH' AND TREG_NTREG = 'TREG';")->queryScalar();
		$jml7 = Yii::app()->db->createCommand("SELECT 
		count(atk.NIK) as JML_ORG
		FROM m_mapping_atk_".$bulan."_".$tahun." atk 
		JOIN m_segmentasi seg ON seg.KODE = atk.SEGMENTASI_KODE
		JOIN t_payslip_".$bulan."_".$tahun." py ON py.NIK = atk.NIK
		WHERE seg.K_SEG = 'CON' AND TREG_NTREG = 'TREG';")->queryScalar();
		$jml8 = Yii::app()->db->createCommand("SELECT 
		count(atk.NIK) as JML_ORG
		FROM m_mapping_atk_".$bulan."_".$tahun." atk 
		JOIN m_segmentasi seg ON seg.KODE = atk.SEGMENTASI_KODE
		JOIN t_payslip_".$bulan."_".$tahun." py ON py.NIK = atk.NIK
		WHERE seg.K_SEG = 'ENT' AND TREG_NTREG = 'TREG';")->queryScalar();
		$jml9 = Yii::app()->db->createCommand("SELECT 
		count(atk.NIK) as JML_ORG
		FROM m_mapping_atk_".$bulan."_".$tahun." atk 
		JOIN m_segmentasi seg ON seg.KODE = atk.SEGMENTASI_KODE
		JOIN t_payslip_".$bulan."_".$tahun." py ON py.NIK = atk.NIK
		WHERE seg.K_SEG = 'WIB' AND TREG_NTREG = 'TREG';")->queryScalar();
		$jml10 = Yii::app()->db->createCommand("SELECT 
		count(atk.NIK) as JML_ORG
		FROM m_mapping_atk_".$bulan."_".$tahun." atk 
		JOIN m_segmentasi seg ON seg.KODE = atk.SEGMENTASI_KODE
		JOIN t_payslip_".$bulan."_".$tahun." py ON py.NIK = atk.NIK
		WHERE seg.K_SEG = 'CON' AND TREG_NTREG = 'NON TREG';")->queryScalar();
		$jml11 = Yii::app()->db->createCommand("SELECT 
		count(atk.NIK) as JML_ORG
		FROM m_mapping_atk_".$bulan."_".$tahun." atk 
		JOIN m_segmentasi seg ON seg.KODE = atk.SEGMENTASI_KODE
		JOIN t_payslip_".$bulan."_".$tahun." py ON py.NIK = atk.NIK
		WHERE seg.K_SEG = 'ENT' AND TREG_NTREG = 'NON TREG';")->queryScalar();
		$jml12 = Yii::app()->db->createCommand("SELECT 
		count(atk.NIK) as JML_ORG
		FROM m_mapping_atk_".$bulan."_".$tahun." atk 
		JOIN m_segmentasi seg ON seg.KODE = atk.SEGMENTASI_KODE
		JOIN t_payslip_".$bulan."_".$tahun." py ON py.NIK = atk.NIK
		WHERE seg.K_SEG = 'WIB' AND TREG_NTREG = 'NON TREG';")->queryScalar();
		$jml13 = Yii::app()->db->createCommand("SELECT 
		count(atk.NIK) as JML_ORG
		FROM m_mapping_atk_".$bulan."_".$tahun." atk 
		JOIN m_segmentasi seg ON seg.KODE = atk.SEGMENTASI_KODE
		JOIN t_payslip_".$bulan."_".$tahun." py ON py.NIK = atk.NIK
		WHERE seg.K_SEG = 'DIG' AND TREG_NTREG = 'NON TREG';")->queryScalar();
		$jml14 = Yii::app()->db->createCommand("SELECT 
		count(atk.NIK) as JML_ORG
		FROM m_mapping_atk_".$bulan."_".$tahun." atk 
		JOIN m_segmentasi seg ON seg.KODE = atk.SEGMENTASI_KODE
		JOIN t_payslip_".$bulan."_".$tahun." py ON py.NIK = atk.NIK
		WHERE seg.K_SEG = 'MOB' AND TREG_NTREG = 'NON TREG';")->queryScalar();
		$jml15 = Yii::app()->db->createCommand("SELECT 
		count(atk.NIK) as JML_ORG
		FROM m_mapping_atk_".$bulan."_".$tahun." atk 
		JOIN m_segmentasi seg ON seg.KODE = atk.SEGMENTASI_KODE
		JOIN t_payslip_".$bulan."_".$tahun." py ON py.NIK = atk.NIK
		WHERE seg.K_SEG = 'FU' AND TREG_NTREG = 'NON TREG';")->queryScalar();
		$jml16 = Yii::app()->db->createCommand("SELECT 
		count(atk.NIK) as JML_ORG
		FROM m_mapping_atk_".$bulan."_".$tahun." atk 
		JOIN m_segmentasi seg ON seg.KODE = atk.SEGMENTASI_KODE
		JOIN t_payslip_".$bulan."_".$tahun." py ON py.NIK = atk.NIK")->queryScalar();

		$this->render('reportglobal',array(
			'tahun'=>$tahun,
			'bulan'=>$bulan,
			'tsoh'=>$tsoh,
			'tnoh'=>$tnoh,
			'tboh'=>$tboh,
			'tooh'=>$tooh,
			'tcon'=>$tcon,
			'tent'=>$tent,
			'twib'=>$twib,
			'ncon'=>$ncon,
			'nent'=>$nent,
			'nwib'=>$nwib,
			'ndig'=>$ndig,
			'nmobile'=>$nmobile,
			'prosfu'=>$prosfu,
			'realmonth'=>$realmonth,
			'total'=>$total,
			'jml1'=>$jml1,
			'jml2'=>$jml2,
			'jml3'=>$jml3,
			'jml4'=>$jml4,
			'jml5'=>$jml5,
			'jml6'=>$jml6,
			'jml7'=>$jml7,
			'jml8'=>$jml8,
			'jml9'=>$jml9,
			'jml10'=>$jml10,
			'jml11'=>$jml11,
			'jml12'=>$jml12,
			'jml13'=>$jml13,
			'jml14'=>$jml14,
			'jml15'=>$jml15,
			'jml16'=>$jml16,
		));
	}

	public function actionReportglobalnow($tahun,$bulan)
	{
		$sqldis = "SELECT DISTINCT(t.BULAN) FROM t_pros_cfu_fu t WHERE t.TAHUN = :tahun AND t.BULAN <= :bulan ORDER BY t.BULAN";
		$cmddis = Yii::app()->db->createCommand($sqldis);
		$cmddis->bindParam(':tahun',$tahun);
		$cmddis->bindParam(':bulan',$bulan);
		$blndis = $cmddis->queryAll();

		$sqlrealsapnow = "SELECT SUM(t.REALISASI) FROM t_realsap t WHERE t.TAHUN = :tahun AND t.BULAN <= :bulan";
		$cmdrealsapnow = Yii::app()->db->createCommand($sqlrealsapnow);
		$cmdrealsapnow->bindParam(':tahun',$tahun);
		$cmdrealsapnow->bindParam(':bulan',$bulan);
		$realsapnow = $cmdrealsapnow->queryScalar();

		$j1 = 0;
		$j2 = 0;
		$j3 = 0;
		$j4 = 0;
		$j5 = 0;
		$j6 = 0;
		$j7 = 0;
		$j8 = 0;
		$j9 = 0;
		$j10 = 0;
		$j11 = 0;
		$j12 = 0;
		$j13 = 0;

		foreach ($blndis as $key => $value) {

			$tsoh = TProsCfuFu::model()->findByAttributes(array('SEGMENTASI_KODE'=>'T-SOH','TAHUN'=>$tahun,'BULAN'=>$value['BULAN'])); 
			$tnoh = TProsCfuFu::model()->findByAttributes(array('SEGMENTASI_KODE'=>'T-NOH','TAHUN'=>$tahun,'BULAN'=>$value['BULAN'])); 
			$tboh = TProsCfuFu::model()->findByAttributes(array('SEGMENTASI_KODE'=>'T-BSO','TAHUN'=>$tahun,'BULAN'=>$value['BULAN'])); 
			$tooh = TProsCfuFu::model()->findByAttributes(array('SEGMENTASI_KODE'=>'T-OOH','TAHUN'=>$tahun,'BULAN'=>$value['BULAN'])); 
			$tcon = TProsCfuFu::model()->findByAttributes(array('SEGMENTASI_KODE'=>'T-CFU-CON','TAHUN'=>$tahun,'BULAN'=>$value['BULAN'])); 
			$tent = TProsCfuFu::model()->findByAttributes(array('SEGMENTASI_KODE'=>'T-CFU-ENT','TAHUN'=>$tahun,'BULAN'=>$value['BULAN'])); 
			$twib = TProsCfuFu::model()->findByAttributes(array('SEGMENTASI_KODE'=>'T-CFU-WIB','TAHUN'=>$tahun,'BULAN'=>$value['BULAN'])); 
			
			$ncon = TProsCfuFu::model()->findByAttributes(array('SEGMENTASI_KODE'=>'N-CFU-CON','TAHUN'=>$tahun,'BULAN'=>$value['BULAN'])); 
			$nent = TProsCfuFu::model()->findByAttributes(array('SEGMENTASI_KODE'=>'N-CFU-ENT','TAHUN'=>$tahun,'BULAN'=>$value['BULAN'])); 
			$nwib = TProsCfuFu::model()->findByAttributes(array('SEGMENTASI_KODE'=>'N-CFU-WIB','TAHUN'=>$tahun,'BULAN'=>$value['BULAN'])); 
			$ndig = TProsCfuFu::model()->findByAttributes(array('SEGMENTASI_KODE'=>'N-CFU-DIG','TAHUN'=>$tahun,'BULAN'=>$value['BULAN'])); 
			$nmobile = TProsCfuFu::model()->findByAttributes(array('SEGMENTASI_KODE'=>'N-CFU-MOB','TAHUN'=>$tahun,'BULAN'=>$value['BULAN'])); 
			$sqlfu = Yii::app()->db->createCommand("SELECT SUM(a.PROSENTASE) FROM t_pros_cfu_fu a JOIN m_segmentasi seg ON a.SEGMENTASI_KODE = seg.KODE WHERE seg.TYPE = :fu AND a.BULAN = :bulan AND a.TAHUN = :tahun;");
			$fu = 'FU';
			$sqlfu->bindParam(':fu',$fu);
			$sqlfu->bindParam(':bulan',$value['BULAN']);
			$sqlfu->bindParam(':tahun',$tahun);
			$prosfu = $sqlfu->queryScalar();

			$realmonth = TRealsap::model()->findByAttributes(array('TAHUN'=>$tahun),array('select'=>'SUM(REALISASI) as REALISASI','condition'=>'BULAN = :bulan','params'=>array(':bulan'=>$value['BULAN'])));

			$j1 += $tsoh->PROSENTASE*$realmonth->REALISASI;
			$j2 += $tnoh->PROSENTASE*$realmonth->REALISASI;
			$j3 += $tboh->PROSENTASE*$realmonth->REALISASI;
			$j4 += $tooh->PROSENTASE*$realmonth->REALISASI;
			$j5 += $tcon->PROSENTASE*$realmonth->REALISASI;
			$j6 += $tent->PROSENTASE*$realmonth->REALISASI;
			$j7 += $twib->PROSENTASE*$realmonth->REALISASI;
			$j8 += $ncon->PROSENTASE*$realmonth->REALISASI;
			$j9 += $nent->PROSENTASE*$realmonth->REALISASI;
			$j10 += $nwib->PROSENTASE*$realmonth->REALISASI;
			$j11 += $ndig->PROSENTASE*$realmonth->REALISASI;
			$j12 += $nmob->PROSENTASE*$realmonth->REALISASI;
			$j13 += $prosfu*$realmonth->REALISASI;

		}
		$total = $j1+$j2+$j3+$j4+$j5+$j6+$j7+$j8+$j9+$j10+$j11+$j12+$j13;

		$this->render('reportglobalnow',array(
			'tahun'=>$tahun,
			'bulan'=>$bulan,
			'j1'=>$j1,
			'j2'=>$j2,
			'j3'=>$j3,
			'j4'=>$j4,
			'j5'=>$j5,
			'j6'=>$j6,
			'j7'=>$j7,
			'j8'=>$j8,
			'j9'=>$j9,
			'j10'=>$j10,
			'j11'=>$j11,
			'j12'=>$j12,
			'j13'=>$j13,
			'total'=>$total,
			'realsapnow'=>$realsapnow
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return CDINAS the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=TPayslip::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param CDINAS $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='cdinas-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionDeletecost($tahun,$bulan)
	{
		$transaction = Yii::app()->db->beginTransaction();
		try{
			$querydelsap = "DELETE FROM t_realsap WHERE BULAN = :bulan AND TAHUN = :tahun";
            $sqldelsap = Yii::app()->db->createCommand($querydelsap);
            $sqldelsap->bindParam(':bulan',$bulan);
			$sqldelsap->bindParam(':tahun',$tahun);
			$sqldelsap->query();

			$querydel = "DELETE FROM t_payslip WHERE BULAN = :bulan AND TAHUN = :tahun";
            $sqldel = Yii::app()->db->createCommand($querydel);
            $sqldel->bindParam(':bulan',$bulan);
			$sqldel->bindParam(':tahun',$tahun);
			$sqldel->query();

            $querydel = "DELETE FROM m_hr_current WHERE BULAN = :bulan AND TAHUN = :tahun";
            $sqldel = Yii::app()->db->createCommand($querydel);
            $sqldel->bindParam(':bulan',$bulan);
			$sqldel->bindParam(':tahun',$tahun);
			$sqldel->query();

			$del1 = "DELETE FROM t_pros_cfu_fu  WHERE BULAN = :bulan AND TAHUN = :tahun";
			$sqldel1 = Yii::app()->db->createCommand($del1);
			$sqldel1->bindParam(':bulan',$bulan);
			$sqldel1->bindParam(':tahun',$tahun);
			$sqldel1->query();

			$del2 = "DELETE FROM t_pros_divisi WHERE TAHUN = :tahun AND BULAN = :bulan";
			$sqldel2 = Yii::app()->db->createCommand($del2);
			$sqldel2->bindParam(':bulan',$bulan);
			$sqldel2->bindParam(':tahun',$tahun);
			$sqldel2->query();

			$del3 = "DELETE FROM t_pros_witel WHERE TAHUN = :tahun AND BULAN = :bulan";
			$sqldel3 = Yii::app()->db->createCommand($del3);
			$sqldel3->bindParam(':bulan',$bulan);
			$sqldel3->bindParam(':tahun',$tahun);
			$sqldel3->query();

			$del4 = "DELETE atk FROM m_mapping_atk atk
						JOIN m_hr_current hr ON atk.NIK = hr.NIK AND atk.BULAN = hr.BULAN AND atk.TAHUN = hr.TAHUN
			 			JOIN m_mapping_cfu_fu seg ON seg.KODE_DIVISI = hr.KODE_DIVISI WHERE (hr.KODE_DIVISI <> '' || hr.KODE_DIVISI IS NOT NULL) AND hr.KODE_DIVISI NOT IN ('DIV-TR1','DIV-TR2','DIV-TR3','DIV-TR4','DIV-TR5','DIV-TR6','DIV-TR7') AND hr.BULAN = :bulan AND hr.TAHUN = :tahun";
			$sqldel4 = Yii::app()->db->createCommand($del4);
			$sqldel4->bindParam(':bulan',$bulan);
			$sqldel4->bindParam(':tahun',$tahun);
			$sqldel4->query();

			$del5 = "DELETE atk FROM m_mapping_atk atk
						JOIN m_hr_current hr ON atk.NIK = hr.NIK AND atk.BULAN = hr.BULAN AND atk.TAHUN = hr.TAHUN
			 			WHERE hr.KODE_DIVISI IN ('DIV-TR1','DIV-TR2','DIV-TR3','DIV-TR4','DIV-TR5','DIV-TR6','DIV-TR7') 
			 			AND hr.BULAN = :bulan AND hr.TAHUN = :tahun";
			$sqldel5 = Yii::app()->db->createCommand($del5);
			$sqldel5->bindParam(':bulan',$bulan);
			$sqldel5->bindParam(':tahun',$tahun);
			$sqldel5->query();

			$del6 = "DELETE FROM t_pros_witel_form WHERE BULAN = :bulan AND TAHUN = :tahun";
			$sqldel6 = Yii::app()->db->createCommand($del6);
			$sqldel6->bindParam(':bulan',$bulan);
			$sqldel6->bindParam(':tahun',$tahun);
			$sqldel6->query();

			$namatbl1 = 'm_hr_current_'.$bulan.'_'.$tahun;
			$cektab1 = Yii::app()->db->createCommand("SHOW TABLES LIKE :par")
												->bindParam(':par',$namatbl1)
												->queryAll();
			if (count($cektab1)>0) {
				$drop1 = "DROP TABLE ".$namatbl1."";
				$sqldrop1 = Yii::app()->db->createCommand($drop1);
				$sqldrop1->query();
			}

			$namatbl2 = 't_payslip_'.$bulan.'_'.$tahun;
			$cektab2 = Yii::app()->db->createCommand("SHOW TABLES LIKE :par")
												->bindParam(':par',$namatbl2)
												->queryAll();
			if (count($cektab2)>0) {
				$drop2 = "DROP TABLE ".$namatbl2."";
				$sqldrop2 = Yii::app()->db->createCommand($drop2);
				$sqldrop2->query();
			}

			$namatbl3 = 'm_mapping_atk_'.$bulan.'_'.$tahun;
			$cektab3 = Yii::app()->db->createCommand("SHOW TABLES LIKE :par")
										->bindParam(':par',$namatbl3)
										->queryAll();

			if (count($cektab3)>0) {
				$drop3 = "DROP TABLE ".$namatbl3."";
				$sqldrop3 = Yii::app()->db->createCommand($drop3);
				$sqldrop3->query();
			}

		    $transaction->commit();
			$this->redirect(array('index'));
		}
		catch(Exception $e){
		    $transaction->rollBack();
		    throw new CHttpException(null,"ERROR: ".$e->getMessage());
		}
	}

	public function actionInformasi()
	{
		$this->render('informasi');
	}

	public function actionPopshowman()
	{
		$id = Yii::app()->request->getParam('id');
		$bulan = Yii::app()->request->getParam('bulan');
		$tahun = Yii::app()->request->getParam('tahun');
		$this->renderPartial('_popshowman',array(
			'id'=>$id,
			'bulan'=>$bulan,
			'tahun'=>$tahun
		));
	}

	public function actionShowman()
	{
		$this->layout = 'iframe';
		$id = Yii::app()->request->getParam('id');
		$bulan = Yii::app()->request->getParam('bulan');
		$tahun = Yii::app()->request->getParam('tahun');
		$model = new MHrCurrent('search');
		if (isset($_REQUEST['MHrCurrent'])) {
			$model->unsetAttributes();
			$model->attributes = $_REQUEST['MHrCurrent'];
		}

		$this->render('showman',array('model'=>$model,'id'=>$id,'bulan'=>$bulan,'tahun'=>$tahun));
	}
}
