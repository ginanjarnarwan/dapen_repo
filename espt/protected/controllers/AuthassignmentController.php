<?php

class AuthassignmentController extends Controller
{
	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow',  // allow all users to perform 'index' and 'view' actions
				'actions'=>array('index','view','create','update','delete','Showman','popshowman','deletes'),
				// 'users'=>array('@'),
				'roles'=>array('Admin')
			),
			array('deny',  // deny all users
				'users'=>array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model=new Authassignment;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Authassignment']))
		{
			$model->attributes=$_POST['Authassignment'];
			if($model->save())
				$this->redirect(array('index'));
		}

		$this->render('create',array(
			'model'=>$model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Authassignment']))
		{
			$model->attributes=$_POST['Authassignment'];
			if($model->save())
				$this->redirect(array('index'));
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete()
	{
		$id = Yii::app()->request->getParam('id');
		$model = Authassignment::model()->findByAttributes(array('userid'=>$id));
		$model->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	public function actionDeletes()
	{
		$id = Yii::app()->request->getParam('id');
		$model = Authassignment::model()->findByAttributes(array('userid'=>$id));
		$model->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('index'));
	}

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		$model=new Authassignment('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Authassignment']))
			$model->attributes=$_GET['Authassignment'];

		$this->render('index',array(
			'model'=>$model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Authassignment the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Authassignment::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Authassignment $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='authassignment-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionPopshowman()
	{
		$temp = Yii::app()->request->getParam('temp');
		$this->renderPartial('_popshowman',array(
			'temp'=>$temp,
		));
	}

	public function actionShowman()
	{
		$this->layout = 'iframe';
		$modcek = TProsCfuFu::model()->find(array('select'=>'t.BULAN,t.TAHUN','order'=>'t.TAHUN DESC,t.BULAN DESC','limit'=>'1'));
		$bulan = $modcek->BULAN;
		$tahun = $modcek->TAHUN;
		$temp = Yii::app()->request->getParam('temp');
		$id = 'ALL';
		$model = new MHrCurrent('search');
		if (isset($_REQUEST['MHrCurrent'])) {
			$model->unsetAttributes();
			$model->attributes = $_REQUEST['MHrCurrent'];
		}

		$this->render('showman',array('model'=>$model,'id'=>$id,'bulan'=>$bulan,'tahun'=>$tahun,'temp'=>$temp));
	}
}
