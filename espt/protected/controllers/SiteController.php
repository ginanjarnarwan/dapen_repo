<?php

class SiteController extends Controller {

    /**
     * Declares class-based actions.
     */
    public function actions() {
        return array(
            // captcha action renders the CAPTCHA image displayed on the contact page
            'captcha' => array(
                'class' => 'CCaptchaAction',
                'backColor' => 0xFFFFFF,
            ),
            // page action renders "static" pages stored under 'protected/views/site/pages'
            // They can be accessed via: index.php?r=site/page&view=FileName
            'page' => array(
                'class' => 'CViewAction',
            ),
        );
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     */
    public function actionIndex() {
        // renders the view file 'protected/views/site/index.php'
        // using the default layout 'protected/views/layouts/main.php'
        if (Yii::app()->user->isGuest) {
            // $this->render('index');
            $this->redirect(array('login'));
        } else {
            $nik = Yii::app()->user->id;

            $tabel_cek = array();
            $tahun_cek = array();
            $tahun_current = date("Y");
            $tahun_min = date("Y") - 5;
            for ($i=$tahun_current; $i >= $tahun_min; $i--) { 
                $namatbl = 'rpt_spt1721_'.$i;
                $cektab = Yii::app()->db->createCommand("SHOW TABLES LIKE :par")
                                            ->bindParam(':par',$namatbl)
                                            ->queryAll();
                if (count($cektab) > 0 ){
                    $tabel_cek[] = $namatbl;
                    $tahun_cek[] = $i;
                }

                $namatbl2 = 'bukti_potong_nonfinal_'.$i;
                $cektab2 = Yii::app()->db->createCommand("SHOW TABLES LIKE :par")
                                            ->bindParam(':par',$namatbl2)
                                            ->queryAll();
                if (count($cektab2) > 0 ){
                    $tabel_cek2[] = $namatbl2;
                    $tahun_cek2[] = $i;
                }

                $namatbl3 = 'bukti_potong_final_'.$i;
                $cektab3 = Yii::app()->db->createCommand("SHOW TABLES LIKE :par")
                                            ->bindParam(':par',$namatbl3)
                                            ->queryAll();
                if (count($cektab3) > 0 ){
                    $tabel_cek3[] = $namatbl3;
                    $tahun_cek3[] = $i;
                }

                $namatbl4 = 'rpt_spt1770s_'.$i;
                $cektab4 = Yii::app()->db->createCommand("SHOW TABLES LIKE :par")
                                            ->bindParam(':par',$namatbl4)
                                            ->queryAll();
                if (count($cektab4) > 0 ){
                    $tabel_cek4[] = $namatbl4;
                    $tahun_cek4[] = $i;
                }

                $namatbl5 = 'ssp_'.$i;
                $cektab5 = Yii::app()->db->createCommand("SHOW TABLES LIKE :par")
                                            ->bindParam(':par',$namatbl5)
                                            ->queryAll();
                if (count($cektab5) > 0 ){
                    $tabel_cek5[] = $namatbl5;
                    $tahun_cek5[] = $i;
                }
            }

            //start 1.query spt1721-a1
            $data=null;
            if(count($tabel_cek)>0){
                $query = 'select a.* from (';
                $a = 1;
                $jml = count($tabel_cek);
                foreach ($tabel_cek as $key => $value) {
                    $query .= " SELECT * FROM ".$value." where N_NIK=:nik ";
                    if ($jml-$a == 0) {
                        $query .= " ) a ORDER BY a.N_TAHUN ";
                    }else{
                        $query .= " UNION ";
                    }
                    $a++;
                }
                $sql = Yii::app()->db->createCommand($query);
                $sql->bindParam(':nik',$nik);
                $data = $sql->queryAll();
            }
            //end

            //start 2. query bukti potong non final
            $datanonfinal=null;
            if(count($tabel_cek2)){
                $query2 = 'select a.* from (';
                $a = 1;
                $jml2 = count($tabel_cek2);
                foreach ($tabel_cek2 as $key => $value) {
                    $query2 .= " SELECT * FROM ".$value." where nik=:nik GROUP BY nomor_bukti_potong";
                    if ($jml2-$a == 0) {
                        $query2 .= " ) a ORDER BY a.tahun_pajak ";
                    }else{
                        $query2 .= " UNION ";
                    }
                    $a++;
                }
                $sql2 = Yii::app()->db->createCommand($query2);
                $sql2->bindParam(':nik',$nik);
                $datanonfinal = $sql2->queryAll();
            }
            //end

            //start 3. query bukti potong final
            $datafinal = null;
            if(count($tabel_cek3)){
                $query3 = 'select a.* from (';
                $a = 1;
                $jml3 = count($tabel_cek3);
                foreach ($tabel_cek3 as $key => $value) {
                    $query3 .= " SELECT * FROM ".$value." where nik=:nik GROUP BY nomor_bukti_potong";
                    if ($jml3-$a == 0) {
                        $query3 .= " ) a ORDER BY a.tahun_pajak ";
                    }else{
                        $query3 .= " UNION ";
                    }
                    $a++;
                }
                $sql3 = Yii::app()->db->createCommand($query3);
                $sql3->bindParam(':nik',$nik);
                $datafinal = $sql3->queryAll();
            }
            //end

            //start 4. query 1770s
            $data1770s = null;
            if(count($tabel_cek4)){
                $query4 = 'select a.* from (';
                $a = 1;
                $jml4 = count($tabel_cek4);
                foreach ($tabel_cek4 as $key => $value) {
                    $query4 .= " SELECT * FROM ".$value." where n_nik=:nik ";
                    if ($jml4-$a == 0) {
                        $query4 .= " ) a ORDER BY a.n_tahun_pajak ";
                    }else{
                        $query4 .= " UNION ";
                    }
                    $a++;
                }
                $sql4 = Yii::app()->db->createCommand($query4);
                $sql4->bindParam(':nik',$nik);
                $data1770s = $sql4->queryAll();
            }
            //end

             //start 5. query ssp
            $datassp = null;
            if(count($tabel_cek5)){
                $query5 = 'select a.* from (';
                $a = 1;
                $jml5 = count($tabel_cek5);
                foreach ($tabel_cek5 as $key => $value) {
                    $query5 .= " SELECT * FROM ".$value." where n_nik=:nik ";
                    if ($jml5-$a == 0) {
                        $query5 .= " ) a ORDER BY a.n_tahun_pajak ";
                    }else{
                        $query5 .= " UNION ";
                    }
                    $a++;
                }
                $sql5 = Yii::app()->db->createCommand($query5);
                $sql5->bindParam(':nik',$nik);
                $datassp = $sql5->queryAll();
            }
            //end

            $thnbs = max($tahun_cek);
            $query="SELECT * FROM rpt_spt1721_".$thnbs." where n_nik=:nik";
            $res2 = Yii::app()->db->createCommand($query)
                    ->bindParam(':nik',$nik)
                    ->queryAll();
            $query2 ="SELECT efin FROM efin_npwp where nik=:nik";
            $res3 = Yii::app()->db->createCommand($query2)
                    ->bindParam(':nik',$nik)
                    ->queryAll();
            $efin = Yii::app()->db->createCommand($query2)
                    ->bindParam(':nik',$nik)
                    ->queryScalar();
            if(!empty($res2)){
                foreach ($res2[0] as $key => $value) {
                    $data2[] = $value;
                }
            }
            if (!empty($res3)) {
                foreach ($res3[0] as $key => $value) {
                    $data3[] = $value;
                }
            }
            $res2 = isset($data2)?array(0=>$data2) : null;
            $res3 = isset($data3)?array(0=>$data3) : null;

            $bukti_potong=  " 1 -".$res2[0][6]." . ".$res2[0][3]." - ".$res2[0][0];
            $query3 ="SELECT * FROM t_par_fin_".$thnbs."";
            $res4 = Yii::app()->db->createCommand($query3)
                    ->queryAll();
            foreach ($res4[0] as $key => $value) {
                $data4[] = $value;
            }
            $res4 = array(0=>$data4);

            $stat_k = "";
            $stat_tk = "";
            $stat = $res2[0][24];
            if (strstr($stat, "TK"))
            {
                $stat_tk = str_replace("TK/", "TK/", $stat);
            }
            else
            {
                $stat_k = str_replace("K/", "K/", $stat);
            }

            $queryresume = "SELECT 
                            t.n_nik,
                            t.v_nama,
                            (select s.V_NPWP_PRIBADI from rpt_spt1721_".$thnbs." s where s.N_NIK = t.n_nik limit 1) AS NPWP_PEGAWAI,
                            CASE 
                                WHEN t.n_jumlah_pph_terutang-t.n_pph_dipotong > 0 THEN 'Kurang Bayar'
                                WHEN t.n_jumlah_pph_terutang-t.n_pph_dipotong < 0 THEN 'Lebih Bayar'
                                WHEN t.n_jumlah_pph_terutang-t.n_pph_dipotong = 0 THEN 'Nihil'
                            END AS status_spt,
                            t.n_jumlah_pph_terutang-t.n_pph_dipotong as selisih,
                            (select s.V_NPWP_KANTOR from rpt_spt1721_".$thnbs." s where s.N_NIK = t.n_nik and s.V_NAMA_PEMOTONG = 'Dapen') AS NPWP_DAPEN,
                            (select s.V_NPWP_KANTOR from rpt_spt1721_".$thnbs." s where s.N_NIK = t.n_nik and s.V_NAMA_PEMOTONG = 'PT. Telekomunikasi Indonesia,Tbk') AS NPWP_Telkom
                            FROM rpt_spt1770s_".$thnbs." t WHERE t.n_nik = :nik";

            // $queryresume = "select s.* from rpt_spt1721_".$thnbs." s where s.N_NIK = :nik limit 1";


            $resume = Yii::app()->db->createCommand($queryresume)
                        ->bindParam(':nik',$nik)
                        ->queryRow();

            $this->render('index',array(
                'data'=>$data,
                'stat_k'=>$stat_k,
                'stat_tk'=>$stat_tk,
                'res2'=>$res2,
                'res3'=>$res3,
                'res4'=>$res4,
                'bukti_potong'=>$bukti_potong,
                'datafinal'=>$datafinal,
                'datanonfinal'=>$datanonfinal,
                'data1770s'=>$data1770s,
                'datassp'=>$datassp,
                'efin'=>$efin,
                'resume'=>$resume
            ));
        }
    }

    /**
     * This is the action to handle external exceptions.
     */
    public function actionError() {
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    /**
     * Displays the contact page
     */
    public function actionContact() {
        $model = new ContactForm;
        if (isset($_POST['ContactForm'])) {
            $model->attributes = $_POST['ContactForm'];
            if ($model->validate()) {
                $name = '=?UTF-8?B?' . base64_encode($model->name) . '?=';
                $subject = '=?UTF-8?B?' . base64_encode($model->subject) . '?=';
                $headers = "From: $name <{$model->email}>\r\n" .
                        "Reply-To: {$model->email}\r\n" .
                        "MIME-Version: 1.0\r\n" .
                        "Content-Type: text/plain; charset=UTF-8";

                mail(Yii::app()->params['adminEmail'], $subject, $model->body, $headers);
                Yii::app()->user->setFlash('contact', 'Thank you for contacting us. We will respond to you as soon as possible.');
                $this->refresh();
            }
        }
        $this->render('contact', array('model' => $model));
    }

    /**
     * Displays the login page
     */
    public function actionLogin() {
        $this->layout = 'login';
        if (Yii::app()->user->isGuest) {
            $model = new LoginForm;
            // if (Yii::app()->user->getState('attempts-login') > 3) { //make the captcha required if the unsuccessful attemps are more of thee
            //     $model->scenario = 'withCaptcha';
            // }

            // if it is ajax validation request
            if (isset($_POST['ajax']) && $_POST['ajax'] === 'login-form') {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }


            // collect user input data
            if (isset($_POST['LoginForm'])) {
                $model->attributes = $_POST['LoginForm'];
                if ($model->validate() && $model->login()){
                    Yii::app()->user->setState('attempts-login', 0);
					MyApp::setLog('sukses');
                    $this->redirect(Yii::app()->user->returnUrl);
                }else{
                    Yii::app()->user->setState('attempts-login', Yii::app()->user->getState('attempts-login', 0) + 1);
                    MyApp::setLog('gagal');
                    // if (Yii::app()->user->getState('attempts-login') > 3) { 
                    //     $model->scenario = 'withCaptcha'; //useful only for view
                    // }
                }
            }
            // display the login form
            // var_dump($model);exit();
            $this->renderPartial('login', array('model' => $model));
        }else $this->redirect(array('/site/index')); 
    }

    public function actionRegister(){
        $this->layout = 'login';
        if (Yii::app()->user->isGuest) {
            $model = new Userapp;

            if (isset($_POST['ajax']) && $_POST['ajax'] === 'Userapp') {
                echo CActiveForm::validate($model);
                Yii::app()->end();
            }

            if (isset($_POST['Userapp'])) {
                $model->attributes = $_POST['Userapp'];
                if ($_POST['Userapp']['telkomnik'] <> '' && $model->validate()) {
                    $request = $_POST['Userapp'];
                    $model = Userapp::model()->findByAttributes(array('telkomnik'=>$_POST['Userapp']['telkomnik']));
                    $model->email = $request['email'];
                    $model->nomor_handphone = $request['nomor_handphone'];
                    $model->password = md5($request['password']);
                    $model->password2 = md5($request['password2']);
                    if ($model->create_date == null || $model->create_date == '') {
                        $model->create_date = date('Y-m-d H-i-s');
                    }
                    $model->activation_date = date('Y-m-d H-i-s');
                    $model->status = 'true';
                    if($model->save()){
                        Yii::app()->user->setFlash('success', "Data saved!");
						MyApp::setLog('sukses');
                        $this->redirect(array('/site/login'));
                    }
					MyApp::setLog('gagal');
                }
				MyApp::setLog('gagal');
                $model->save();
            }

            $this->renderPartial('register', array('model' => $model));
        }else $this->redirect(array('/site/index'));
    }

    /**
     * Logs out the current user and redirect to homepage.
     */
    public function actionLogout() {
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->homeUrl);
    }

}
