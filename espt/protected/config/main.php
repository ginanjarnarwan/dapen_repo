<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');
Yii::setPathOfAlias('bootstrap', dirname(__FILE__).'/../extensions/Qyiistrap');
Yii::setPathOfAlias('download_path', dirname(__FILE__).'/../../download/upload');
// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'SPT Pensiun',
	'theme'=>'appui',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
	),
	'aliases' => array(
		// 'template_dir' => 'webroot.document.template',
		'upload_dir' => 'webroot.document.upload',
	),
	'modules'=>array(
		// uncomment the following to enable the Gii tool

		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'1',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
		// 'budget',
		// 'cshman',
		// 'myadmin',
		// 'tax'

	),

	// application components
	'components'=>array(
		'clientScript'=>array(
			'packages'=>array(
				'jquery'=>array(
					'baseUrl'=>"js/",
					'js'=>array('jquery-2.2.3.min.js')
				)
			)
		),
		'user'=>array(
			// enable cookie-based authentication
        	'class' => 'WebUser',
			'allowAutoLogin'=>true,
		),

		// uncomment the following to enable URLs in path-format
		/*
		'urlManager'=>array(
			'urlFormat'=>'path',
			'rules'=>array(
				'<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
			),
		),
		*/
		
		'db'=>array(
			'connectionString' => 'mysql:host=localhost;dbname=dapentel_espt;',
			'emulatePrepare' => true,
			'username'=>'dapentel_admin',
			'password'=>'Closehead23',
			'charset' => 'utf8',
			'class' => 'CDbConnection',
			/* 'attributes'=>array(
				PDO::MYSQL_ATTR_LOCAL_INFILE => true
			), */
		),
		'authManager'=>array(
            'class'=>'CDbAuthManager',
            'connectionID'=>'db',
        ),

		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>YII_DEBUG ? null : 'site/error',
		),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				/*array(
			        'class'=>'CWebLogRoute',
			        'levels'=>'trace, info, error, warning',
			      ),*/
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				*/
			),
		),

	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		'adminEmail'=>'webmaster@example.com',
		'startingYear'=>2000,
		'currentDate'=>getdate()
	),
);
