<?php

/**
 * This is the model class for table "download_log".
 *
 * The followings are the available columns in table 'download_log':
 * @property integer $id
 * @property string $telkomnik
 * @property string $sarana
 * @property string $update_by
 * @property string $datetime
 * @property string $keterangan
 * @property string $jenis_form
 * @property string $nomor_bukti
 */
class DownloadLog extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'download_log';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('telkomnik, sarana', 'length', 'max'=>255),
			array('update_by, datetime', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, telkomnik, sarana, update_by, datetime, keterangan, jenis_form, nomor_bukti', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'telkomnik' => 'Telkomnik',
			'sarana' => 'Sarana',
			'update_by' => 'Update By',
			'datetime' => 'Datetime',
			'keterangan' => 'Keterangan',
			'jenis_form' => 'Jenis Form',
			'nomor_bukti' => 'Nomor Bukti',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('telkomnik',$this->telkomnik,true);
		$criteria->compare('sarana',$this->sarana,true);
		$criteria->compare('update_by',$this->update_by,true);
		$criteria->compare('datetime',$this->datetime,true);
		$criteria->compare('keterangan',$this->keterangan,true);
		$criteria->compare('jenis_form',$this->jenis_form,true);
		$criteria->compare('nomor_bukti',$this->nomor_bukti,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return DownloadLog the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
