<?php

/**
 * This is the model class for table "m_hr_current".
 *
 * The followings are the available columns in table 'm_hr_current':
 * @property integer $ID
 * @property string $NIK
 * @property string $NAMA
 * @property string $PERSA
 * @property string $SUBAREA
 * @property string $BAND
 * @property string $NAMA_POSISI
 * @property string $NAMA_UNIT
 * @property string $C_HOST
 * @property string $KODE_LOKER
 * @property string $KODE_DIVISI
 * @property string $DIVISI
 */
class MHrCurrent extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public $fileupload;
	public function tableName()
	{
		return 'm_hr_current';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			// array('fileupload', 'required'),
			array('NIK', 'length', 'max'=>9),
			array('NAMA', 'length', 'max'=>70),
			array('PERSA, SUBAREA, C_HOST', 'length', 'max'=>10),
			array('BAND', 'length', 'max'=>3),
			array('fileupload', 'file', 'types' => 'csv', 'allowEmpty' => false, 'on'=>'upload_xls'),
			array('NAMA_POSISI, NAMA_UNIT, KODE_LOKER, KODE_DIVISI, DIVISI', 'length', 'max'=>255),
			array('BULAN,TAHUN,CREATED_BY,CREATED_TIME','safe'),
			array(
                'CREATED_BY',
                'default',
                'value' => Yii::app()->user->id,
                'setOnEmpty' => false,
                'on' => 'insert'
            ),
            array(
                'CREATED_TIME',
                'default',
                'value' => new CDbExpression('NOW()'),
                'setOnEmpty' => false,
                'on' => 'insert'
            ),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, NIK, NAMA, PERSA, SUBAREA, BAND, NAMA_POSISI, NAMA_UNIT, C_HOST, KODE_LOKER, KODE_DIVISI, DIVISI', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'NIK' => 'Nik',
			'NAMA' => 'Nama',
			'PERSA' => 'Persa',
			'SUBAREA' => 'Subarea',
			'BAND' => 'Band',
			'NAMA_POSISI' => 'Nama Posisi',
			'NAMA_UNIT' => 'Nama Unit',
			'C_HOST' => 'C Host',
			'KODE_LOKER' => 'Kode Loker',
			'KODE_DIVISI' => 'Kode Divisi',
			'DIVISI' => 'Divisi',
			'TAHUN' => 'Tahun',
			'BULAN' => 'Bulan',
			'fileupload' => 'File HR Current (.csv)'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('NIK',$this->NIK,true);
		$criteria->compare('NAMA',$this->NAMA,true);
		$criteria->compare('PERSA',$this->PERSA,true);
		$criteria->compare('SUBAREA',$this->SUBAREA,true);
		$criteria->compare('BAND',$this->BAND,true);
		$criteria->compare('NAMA_POSISI',$this->NAMA_POSISI,true);
		$criteria->compare('NAMA_UNIT',$this->NAMA_UNIT,true);
		$criteria->compare('C_HOST',$this->C_HOST,true);
		$criteria->compare('KODE_LOKER',$this->KODE_LOKER,true);
		$criteria->compare('KODE_DIVISI',$this->KODE_DIVISI,true);
		$criteria->compare('DIVISI',$this->DIVISI,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function detailkaryawansegmen($id,$bulan,$tahun)
	{
		$condition='';
		$par = array();
		if (!empty($this->NIK)) {
			$condition = "AND atk.NIK = :nik";
			$par[":nik"] = $this->NIK;
		}
		if (!empty($this->NAMA)) {
			$condition = "AND lower(hr.nama) LIKE :nama";
			$par[":nama"] = '%'.strtolower($this->NAMA).'%';
		}
		if (!empty($this->DIVISI)) {
			$condition = "AND lower(hr.DIVISI) LIKE :div";
			$par[":div"] = '%'.strtolower($this->DIVISI).'%';
		}
		if (!empty($this->NAMA_UNIT)) {
			$condition = "AND lower(hr.NAMA_UNIT) LIKE :unit";
			$par[":unit"] = '%'.strtolower($this->NAMA_UNIT).'%';
		}
		if (!empty($this->NAMA_POSISI)) {
			$condition = "AND lower(hr.NAMA_POSISI) LIKE :pos";
			$par[":pos"] = '%'.strtolower($this->NAMA_POSISI).'%';
		}
		if (!empty($this->BAND)) {
			$condition = "AND lower(hr.BAND) LIKE :band";
			$par[":band"] = '%'.strtolower($this->BAND).'%';
		}
		if ($id == "TREG") {
			$sql = "SELECT 
					atk.NIK,
					hr.NAMA,
					hr.DIVISI,
					hr.NAMA_UNIT,
					hr.NAMA_POSISI,
					hr.BAND
					FROM m_mapping_atk_".$bulan."_".$tahun." atk 
					JOIN m_segmentasi seg ON seg.KODE = atk.SEGMENTASI_KODE
					JOIN t_payslip_".$bulan."_".$tahun." py ON py.NIK = atk.NIK
					JOIN m_hr_current_".$bulan."_".$tahun." hr ON hr.NIK = py.NIK
					WHERE seg.TREG_NTREG = 'TREG' ".$condition."
					ORDER BY hr.KODE_DIVISI ASC,hr.NAMA_UNIT ASC,hr.BAND ASC  
					";
		}
		elseif ($id == "NONTREG") {
			$sql = "SELECT 
					atk.NIK,
					hr.NAMA,
					hr.DIVISI,
					hr.NAMA_UNIT,
					hr.NAMA_POSISI,
					hr.BAND
					FROM m_mapping_atk_".$bulan."_".$tahun." atk 
					JOIN m_segmentasi seg ON seg.KODE = atk.SEGMENTASI_KODE
					JOIN t_payslip_".$bulan."_".$tahun." py ON py.NIK = atk.NIK
					JOIN m_hr_current_".$bulan."_".$tahun." hr ON hr.NIK = py.NIK
					WHERE seg.TREG_NTREG = 'NON TREG' ".$condition."
					ORDER BY hr.KODE_DIVISI ASC,hr.NAMA_UNIT ASC,hr.BAND ASC  
					";	
		}
		elseif ($id == "ALL") {
			$sql = "SELECT 
					atk.NIK,
					hr.NAMA,
					hr.DIVISI,
					hr.NAMA_UNIT,
					hr.NAMA_POSISI,
					hr.BAND
					FROM m_mapping_atk_".$bulan."_".$tahun." atk 
					JOIN m_segmentasi seg ON seg.KODE = atk.SEGMENTASI_KODE
					JOIN t_payslip_".$bulan."_".$tahun." py ON py.NIK = atk.NIK
					JOIN m_hr_current_".$bulan."_".$tahun." hr ON hr.NIK = py.NIK
					WHERE seg.TREG_NTREG IS NOT NULL ".$condition."
					ORDER BY hr.KODE_DIVISI ASC,hr.NAMA_UNIT ASC,hr.BAND ASC  
					";	
		}
		elseif ($id == "SOH") {
			$sql = "SELECT 
					atk.NIK,
					hr.NAMA,
					hr.DIVISI,
					hr.NAMA_UNIT,
					hr.NAMA_POSISI,
					hr.BAND
					FROM m_mapping_atk_".$bulan."_".$tahun." atk 
					JOIN m_segmentasi seg ON seg.KODE = atk.SEGMENTASI_KODE
					JOIN t_payslip_".$bulan."_".$tahun." py ON py.NIK = atk.NIK
					JOIN m_hr_current_".$bulan."_".$tahun." hr ON hr.NIK = py.NIK
					WHERE seg.TREG_NTREG = 'TREG' ".$condition."
					AND seg.K_SEG = 'SOH'
					ORDER BY hr.KODE_DIVISI ASC,hr.NAMA_UNIT ASC,hr.BAND ASC  
					";	
		}
		elseif ($id == "NOH") {
			$sql = "SELECT 
					atk.NIK,
					hr.NAMA,
					hr.DIVISI,
					hr.NAMA_UNIT,
					hr.NAMA_POSISI,
					hr.BAND
					FROM m_mapping_atk_".$bulan."_".$tahun." atk 
					JOIN m_segmentasi seg ON seg.KODE = atk.SEGMENTASI_KODE
					JOIN t_payslip_".$bulan."_".$tahun." py ON py.NIK = atk.NIK
					JOIN m_hr_current_".$bulan."_".$tahun." hr ON hr.NIK = py.NIK
					WHERE seg.TREG_NTREG = 'TREG' ".$condition."
					AND seg.K_SEG = 'NOH'
					ORDER BY hr.KODE_DIVISI ASC,hr.NAMA_UNIT ASC,hr.BAND ASC  
					";	
		}
		elseif ($id == "BOH") {
			$sql = "SELECT 
					atk.NIK,
					hr.NAMA,
					hr.DIVISI,
					hr.NAMA_UNIT,
					hr.NAMA_POSISI,
					hr.BAND
					FROM m_mapping_atk_".$bulan."_".$tahun." atk 
					JOIN m_segmentasi seg ON seg.KODE = atk.SEGMENTASI_KODE
					JOIN t_payslip_".$bulan."_".$tahun." py ON py.NIK = atk.NIK
					JOIN m_hr_current_".$bulan."_".$tahun." hr ON hr.NIK = py.NIK
					WHERE seg.TREG_NTREG = 'TREG' ".$condition."
					AND seg.K_SEG = 'BOH'
					ORDER BY hr.KODE_DIVISI ASC,hr.NAMA_UNIT ASC,hr.BAND ASC  
					";	
		}
		elseif ($id == "OOH") {
			$sql = "SELECT 
					atk.NIK,
					hr.NAMA,
					hr.DIVISI,
					hr.NAMA_UNIT,
					hr.NAMA_POSISI,
					hr.BAND
					FROM m_mapping_atk_".$bulan."_".$tahun." atk 
					JOIN m_segmentasi seg ON seg.KODE = atk.SEGMENTASI_KODE
					JOIN t_payslip_".$bulan."_".$tahun." py ON py.NIK = atk.NIK
					JOIN m_hr_current_".$bulan."_".$tahun." hr ON hr.NIK = py.NIK
					WHERE seg.TREG_NTREG = 'TREG' ".$condition."
					AND seg.K_SEG = 'OOH'
					ORDER BY hr.KODE_DIVISI ASC,hr.NAMA_UNIT ASC,hr.BAND ASC  
					";	
		}
		elseif ($id == "TCON") {
			$sql = "SELECT 
					atk.NIK,
					hr.NAMA,
					hr.DIVISI,
					hr.NAMA_UNIT,
					hr.NAMA_POSISI,
					hr.BAND
					FROM m_mapping_atk_".$bulan."_".$tahun." atk 
					JOIN m_segmentasi seg ON seg.KODE = atk.SEGMENTASI_KODE
					JOIN t_payslip_".$bulan."_".$tahun." py ON py.NIK = atk.NIK
					JOIN m_hr_current_".$bulan."_".$tahun." hr ON hr.NIK = py.NIK
					WHERE seg.TREG_NTREG = 'TREG' ".$condition."
					AND seg.K_SEG = 'CON'
					ORDER BY hr.KODE_DIVISI ASC,hr.NAMA_UNIT ASC,hr.BAND ASC  
					";	
		}
		elseif ($id == "TENT") {
			$sql = "SELECT 
					atk.NIK,
					hr.NAMA,
					hr.DIVISI,
					hr.NAMA_UNIT,
					hr.NAMA_POSISI,
					hr.BAND
					FROM m_mapping_atk_".$bulan."_".$tahun." atk 
					JOIN m_segmentasi seg ON seg.KODE = atk.SEGMENTASI_KODE
					JOIN t_payslip_".$bulan."_".$tahun." py ON py.NIK = atk.NIK
					JOIN m_hr_current_".$bulan."_".$tahun." hr ON hr.NIK = py.NIK
					WHERE seg.TREG_NTREG = 'TREG' ".$condition."
					AND seg.K_SEG = 'ENT'
					ORDER BY hr.KODE_DIVISI ASC,hr.NAMA_UNIT ASC,hr.BAND ASC  
					";	
		}
		elseif ($id == "TWIB") {
			$sql = "SELECT 
					atk.NIK,
					hr.NAMA,
					hr.DIVISI,
					hr.NAMA_UNIT,
					hr.NAMA_POSISI,
					hr.BAND
					FROM m_mapping_atk_".$bulan."_".$tahun." atk 
					JOIN m_segmentasi seg ON seg.KODE = atk.SEGMENTASI_KODE
					JOIN t_payslip_".$bulan."_".$tahun." py ON py.NIK = atk.NIK
					JOIN m_hr_current_".$bulan."_".$tahun." hr ON hr.NIK = py.NIK
					WHERE seg.TREG_NTREG = 'TREG' ".$condition."
					AND seg.K_SEG = 'WIB'
					ORDER BY hr.KODE_DIVISI ASC,hr.NAMA_UNIT ASC,hr.BAND ASC  
					";	
		}
		elseif ($id == "CON") {
			$sql = "SELECT 
					atk.NIK,
					hr.NAMA,
					hr.DIVISI,
					hr.NAMA_UNIT,
					hr.NAMA_POSISI,
					hr.BAND
					FROM m_mapping_atk_".$bulan."_".$tahun." atk 
					JOIN m_segmentasi seg ON seg.KODE = atk.SEGMENTASI_KODE
					JOIN t_payslip_".$bulan."_".$tahun." py ON py.NIK = atk.NIK
					JOIN m_hr_current_".$bulan."_".$tahun." hr ON hr.NIK = py.NIK
					WHERE seg.TREG_NTREG = 'NON TREG' ".$condition."
					AND seg.K_SEG = 'CON'
					ORDER BY hr.KODE_DIVISI ASC,hr.NAMA_UNIT ASC,hr.BAND ASC  
					";	
		}
		elseif ($id == "ENT") {
			$sql = "SELECT 
					atk.NIK,
					hr.NAMA,
					hr.DIVISI,
					hr.NAMA_UNIT,
					hr.NAMA_POSISI,
					hr.BAND
					FROM m_mapping_atk_".$bulan."_".$tahun." atk 
					JOIN m_segmentasi seg ON seg.KODE = atk.SEGMENTASI_KODE
					JOIN t_payslip_".$bulan."_".$tahun." py ON py.NIK = atk.NIK
					JOIN m_hr_current_".$bulan."_".$tahun." hr ON hr.NIK = py.NIK
					WHERE seg.TREG_NTREG = 'NON TREG' ".$condition."
					AND seg.K_SEG = 'ENT'
					ORDER BY hr.KODE_DIVISI ASC,hr.NAMA_UNIT ASC,hr.BAND ASC  
					";	
		}
		elseif ($id == "WIB") {
			$sql = "SELECT 
					atk.NIK,
					hr.NAMA,
					hr.DIVISI,
					hr.NAMA_UNIT,
					hr.NAMA_POSISI,
					hr.BAND
					FROM m_mapping_atk_".$bulan."_".$tahun." atk 
					JOIN m_segmentasi seg ON seg.KODE = atk.SEGMENTASI_KODE
					JOIN t_payslip_".$bulan."_".$tahun." py ON py.NIK = atk.NIK
					JOIN m_hr_current_".$bulan."_".$tahun." hr ON hr.NIK = py.NIK
					WHERE seg.TREG_NTREG = 'NON TREG' ".$condition."
					AND seg.K_SEG = 'WIB'
					ORDER BY hr.KODE_DIVISI ASC,hr.NAMA_UNIT ASC,hr.BAND ASC  
					";	
		}
		elseif ($id == "DIG") {
			$sql = "SELECT 
					atk.NIK,
					hr.NAMA,
					hr.DIVISI,
					hr.NAMA_UNIT,
					hr.NAMA_POSISI,
					hr.BAND
					FROM m_mapping_atk_".$bulan."_".$tahun." atk 
					JOIN m_segmentasi seg ON seg.KODE = atk.SEGMENTASI_KODE
					JOIN t_payslip_".$bulan."_".$tahun." py ON py.NIK = atk.NIK
					JOIN m_hr_current_".$bulan."_".$tahun." hr ON hr.NIK = py.NIK
					WHERE seg.TREG_NTREG = 'NON TREG' ".$condition."
					AND seg.K_SEG = 'DIG'
					ORDER BY hr.KODE_DIVISI ASC,hr.NAMA_UNIT ASC,hr.BAND ASC  
					";	
		}elseif ($id == "MOB") {
			$sql = "SELECT 
					atk.NIK,
					hr.NAMA,
					hr.DIVISI,
					hr.NAMA_UNIT,
					hr.NAMA_POSISI,
					hr.BAND
					FROM m_mapping_atk_".$bulan."_".$tahun." atk 
					JOIN m_segmentasi seg ON seg.KODE = atk.SEGMENTASI_KODE
					JOIN t_payslip_".$bulan."_".$tahun." py ON py.NIK = atk.NIK
					JOIN m_hr_current_".$bulan."_".$tahun." hr ON hr.NIK = py.NIK
					WHERE seg.TREG_NTREG = 'NON TREG' ".$condition."
					AND seg.K_SEG = 'MOB'
					ORDER BY hr.KODE_DIVISI ASC,hr.NAMA_UNIT ASC,hr.BAND ASC  
					";	
		}elseif ($id == "FU") {
			$sql = "SELECT 
					atk.NIK,
					hr.NAMA,
					hr.DIVISI,
					hr.NAMA_UNIT,
					hr.NAMA_POSISI,
					hr.BAND
					FROM m_mapping_atk_".$bulan."_".$tahun." atk 
					JOIN m_segmentasi seg ON seg.KODE = atk.SEGMENTASI_KODE
					JOIN t_payslip_".$bulan."_".$tahun." py ON py.NIK = atk.NIK
					JOIN m_hr_current_".$bulan."_".$tahun." hr ON hr.NIK = py.NIK
					WHERE seg.TREG_NTREG = 'NON TREG' ".$condition."
					AND seg.K_SEG = 'FU'
					ORDER BY hr.KODE_DIVISI ASC,hr.NAMA_UNIT ASC,hr.BAND ASC  
					";	
		}
		$query = Yii::app()->db->createCommand($sql);
		$query->params = $par;

		$count_sql = Yii::app()->db->createCommand("SELECT COUNT(*) FROM (" . $sql . ") t");
		$count_sql->params = $par;
        $count = $count_sql->queryScalar();
        
        return new CSqlDataProvider($query, array(
            'keyField' => 'CC',
            'totalItemCount' => $count,
            // 'pagination' => false,
        ));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MHrCurrent the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
