<?php

/**
 * This is the model class for table "m_mapping_atk_master".
 *
 * The followings are the available columns in table 'm_mapping_atk_master':
 * @property integer $ID
 * @property string $OBJID_DIVISI
 * @property string $KODE_DIVISI
 * @property string $NAMA_DIVISI
 * @property string $OBJID_UNIT
 * @property string $KODE_UNIT
 * @property string $NAMA_UNIT
 * @property string $OBJID_POSISI
 * @property string $KODE_POSISI
 * @property string $NAMA_POSISI
 * @property string $OBJID_JOB
 * @property string $KODE_JOB
 * @property string $NAMA_JOB
 * @property string $KODE_PAY_SCALE_AREA
 * @property string $NAMA_PAY_SCALE_AREA
 * @property string $KODE_PAY_SCALE_TYPE
 * @property string $Staff
 * @property string $BAND_POSISI
 * @property string $KELAS_POSISI
 * @property string $KODE_PERSONAL_AREA
 * @property string $NAMA_PERSONAL_AREA
 * @property string $KODE_PERSONAL_SUBAREA
 * @property string $NAMA_PERSONAL_SUBAREA
 * @property string $KODE_PAYROLL_AREA
 * @property string $NAMA_PAYROLL_AREA
 * @property string $KODE_BUSINESS_AREA
 * @property string $NAMA_BUSINESS_AREA
 * @property string $COST_CENTER
 * @property string $KODE_NAMA
 * @property string $TOTAL_TGC
 * @property string $CFU_FU_iterasi1
 * @property string $CFU_FU_iterasi2
 * @property string $CFU_iterasi3
 * @property string $CFU_summary
 * @property string $CFU_FINAL
 * @property string $CFU
 * @property string $Senior_Overhead
 * @property string $Final_Segment
 * @property string $Sqm
 */
class MMappingAtkMaster extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public $fileupload;
	public function tableName()
	{
		return 'm_mapping_atk_master';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('OBJID_DIVISI, KODE_DIVISI, NAMA_DIVISI, OBJID_UNIT, KODE_UNIT, NAMA_UNIT, OBJID_POSISI, KODE_POSISI, NAMA_POSISI, OBJID_JOB, KODE_JOB, NAMA_JOB, KODE_PAY_SCALE_AREA, NAMA_PAY_SCALE_AREA, KODE_PAY_SCALE_TYPE, Staff, BAND_POSISI, KELAS_POSISI, KODE_PERSONAL_AREA, NAMA_PERSONAL_AREA, KODE_PERSONAL_SUBAREA, NAMA_PERSONAL_SUBAREA, KODE_PAYROLL_AREA, NAMA_PAYROLL_AREA, KODE_BUSINESS_AREA, NAMA_BUSINESS_AREA, COST_CENTER, KODE_NAMA, TOTAL_TGC, CFU_FU_iterasi1, CFU_FU_iterasi2, CFU_iterasi3, CFU_summary, CFU_FINAL, CFU, Senior_Overhead, Final_Segment, Sqm', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, OBJID_DIVISI, KODE_DIVISI, NAMA_DIVISI, OBJID_UNIT, KODE_UNIT, NAMA_UNIT, OBJID_POSISI, KODE_POSISI, NAMA_POSISI, OBJID_JOB, KODE_JOB, NAMA_JOB, KODE_PAY_SCALE_AREA, NAMA_PAY_SCALE_AREA, KODE_PAY_SCALE_TYPE, Staff, BAND_POSISI, KELAS_POSISI, KODE_PERSONAL_AREA, NAMA_PERSONAL_AREA, KODE_PERSONAL_SUBAREA, NAMA_PERSONAL_SUBAREA, KODE_PAYROLL_AREA, NAMA_PAYROLL_AREA, KODE_BUSINESS_AREA, NAMA_BUSINESS_AREA, COST_CENTER, KODE_NAMA, TOTAL_TGC, CFU_FU_iterasi1, CFU_FU_iterasi2, CFU_iterasi3, CFU_summary, CFU_FINAL, CFU, Senior_Overhead, Final_Segment, Sqm', 'safe', 'on'=>'search'),
			array('fileupload','safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'OBJID_DIVISI' => 'Objid Divisi',
			'KODE_DIVISI' => 'Kode Divisi',
			'NAMA_DIVISI' => 'Nama Divisi',
			'OBJID_UNIT' => 'Objid Unit',
			'KODE_UNIT' => 'Kode Unit',
			'NAMA_UNIT' => 'Nama Unit',
			'OBJID_POSISI' => 'Objid Posisi',
			'KODE_POSISI' => 'Kode Posisi',
			'NAMA_POSISI' => 'Nama Posisi',
			'OBJID_JOB' => 'Objid Job',
			'KODE_JOB' => 'Kode Job',
			'NAMA_JOB' => 'Nama Job',
			'KODE_PAY_SCALE_AREA' => 'Kode Pay Scale Area',
			'NAMA_PAY_SCALE_AREA' => 'Nama Pay Scale Area',
			'KODE_PAY_SCALE_TYPE' => 'Kode Pay Scale Type',
			'Staff' => 'Staff',
			'BAND_POSISI' => 'Band Posisi',
			'KELAS_POSISI' => 'Kelas Posisi',
			'KODE_PERSONAL_AREA' => 'Kode Personal Area',
			'NAMA_PERSONAL_AREA' => 'Nama Personal Area',
			'KODE_PERSONAL_SUBAREA' => 'Kode Personal Subarea',
			'NAMA_PERSONAL_SUBAREA' => 'Nama Personal Subarea',
			'KODE_PAYROLL_AREA' => 'Kode Payroll Area',
			'NAMA_PAYROLL_AREA' => 'Nama Payroll Area',
			'KODE_BUSINESS_AREA' => 'Kode Business Area',
			'NAMA_BUSINESS_AREA' => 'Nama Business Area',
			'COST_CENTER' => 'Cost Center',
			'KODE_NAMA' => 'Kode Nama',
			'TOTAL_TGC' => 'Total Tgc',
			'CFU_FU_iterasi1' => 'Cfu Fu Iterasi1',
			'CFU_FU_iterasi2' => 'Cfu Fu Iterasi2',
			'CFU_iterasi3' => 'Cfu Iterasi3',
			'CFU_summary' => 'Cfu Summary',
			'CFU_FINAL' => 'Cfu Final',
			'CFU' => 'Cfu',
			'Senior_Overhead' => 'Senior Overhead',
			'Final_Segment' => 'Final Segment',
			'Sqm' => 'Sqm',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('OBJID_DIVISI',$this->OBJID_DIVISI,true);
		$criteria->compare('KODE_DIVISI',$this->KODE_DIVISI,true);
		$criteria->compare('NAMA_DIVISI',$this->NAMA_DIVISI,true);
		$criteria->compare('OBJID_UNIT',$this->OBJID_UNIT,true);
		$criteria->compare('KODE_UNIT',$this->KODE_UNIT,true);
		$criteria->compare('NAMA_UNIT',$this->NAMA_UNIT,true);
		$criteria->compare('OBJID_POSISI',$this->OBJID_POSISI,true);
		$criteria->compare('KODE_POSISI',$this->KODE_POSISI,true);
		$criteria->compare('NAMA_POSISI',$this->NAMA_POSISI,true);
		$criteria->compare('OBJID_JOB',$this->OBJID_JOB,true);
		$criteria->compare('KODE_JOB',$this->KODE_JOB,true);
		$criteria->compare('NAMA_JOB',$this->NAMA_JOB,true);
		$criteria->compare('KODE_PAY_SCALE_AREA',$this->KODE_PAY_SCALE_AREA,true);
		$criteria->compare('NAMA_PAY_SCALE_AREA',$this->NAMA_PAY_SCALE_AREA,true);
		$criteria->compare('KODE_PAY_SCALE_TYPE',$this->KODE_PAY_SCALE_TYPE,true);
		$criteria->compare('Staff',$this->Staff,true);
		$criteria->compare('BAND_POSISI',$this->BAND_POSISI,true);
		$criteria->compare('KELAS_POSISI',$this->KELAS_POSISI,true);
		$criteria->compare('KODE_PERSONAL_AREA',$this->KODE_PERSONAL_AREA,true);
		$criteria->compare('NAMA_PERSONAL_AREA',$this->NAMA_PERSONAL_AREA,true);
		$criteria->compare('KODE_PERSONAL_SUBAREA',$this->KODE_PERSONAL_SUBAREA,true);
		$criteria->compare('NAMA_PERSONAL_SUBAREA',$this->NAMA_PERSONAL_SUBAREA,true);
		$criteria->compare('KODE_PAYROLL_AREA',$this->KODE_PAYROLL_AREA,true);
		$criteria->compare('NAMA_PAYROLL_AREA',$this->NAMA_PAYROLL_AREA,true);
		$criteria->compare('KODE_BUSINESS_AREA',$this->KODE_BUSINESS_AREA,true);
		$criteria->compare('NAMA_BUSINESS_AREA',$this->NAMA_BUSINESS_AREA,true);
		$criteria->compare('COST_CENTER',$this->COST_CENTER,true);
		$criteria->compare('KODE_NAMA',$this->KODE_NAMA,true);
		$criteria->compare('TOTAL_TGC',$this->TOTAL_TGC,true);
		$criteria->compare('CFU_FU_iterasi1',$this->CFU_FU_iterasi1,true);
		$criteria->compare('CFU_FU_iterasi2',$this->CFU_FU_iterasi2,true);
		$criteria->compare('CFU_iterasi3',$this->CFU_iterasi3,true);
		$criteria->compare('CFU_summary',$this->CFU_summary,true);
		$criteria->compare('CFU_FINAL',$this->CFU_FINAL,true);
		$criteria->compare('CFU',$this->CFU,true);
		$criteria->compare('Senior_Overhead',$this->Senior_Overhead,true);
		$criteria->compare('Final_Segment',$this->Final_Segment,true);
		$criteria->compare('Sqm',$this->Sqm,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MMappingAtkMaster the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
