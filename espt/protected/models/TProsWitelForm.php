<?php

/**
 * This is the model class for table "t_pros_witel_form".
 *
 * The followings are the available columns in table 't_pros_witel_form':
 * @property integer $ID
 * @property integer $BULAN
 * @property integer $TAHUN
 * @property string $KODE_DIVISI
 * @property string $C_HOST
 * @property string $K_SEG
 * @property string $TOTAL
 */
class TProsWitelForm extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 't_pros_witel_form';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('BULAN, TAHUN', 'numerical', 'integerOnly'=>true),
			array('KODE_DIVISI, C_HOST, K_SEG', 'length', 'max'=>255),
			array('TOTAL', 'length', 'max'=>60),
			array('CREATED_BY,CREATED_TIME','safe'),
			array(
                'CREATED_BY',
                'default',
                'value' => Yii::app()->user->id,
                'setOnEmpty' => false,
                'on' => 'insert'
            ),
            array(
                'CREATED_TIME',
                'default',
                'value' => new CDbExpression('NOW()'),
                'setOnEmpty' => false,
                'on' => 'insert'
            ),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, BULAN, TAHUN, KODE_DIVISI, C_HOST, K_SEG, TOTAL', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'BULAN' => 'Bulan',
			'TAHUN' => 'Tahun',
			'KODE_DIVISI' => 'Kode Divisi',
			'C_HOST' => 'C Host',
			'K_SEG' => 'K Seg',
			'TOTAL' => 'Total',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('BULAN',$this->BULAN);
		$criteria->compare('TAHUN',$this->TAHUN);
		$criteria->compare('KODE_DIVISI',$this->KODE_DIVISI,true);
		$criteria->compare('C_HOST',$this->C_HOST,true);
		$criteria->compare('K_SEG',$this->K_SEG,true);
		$criteria->compare('TOTAL',$this->TOTAL,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TProsWitelForm the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
