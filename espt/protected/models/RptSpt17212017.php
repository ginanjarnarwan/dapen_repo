<?php

/**
 * This is the model class for table "rpt_spt1721_2017".
 *
 * The followings are the available columns in table 'rpt_spt1721_2017':
 * @property string $N_URUT
 * @property string $N_NIK
 * @property string $V_NAMA_KARYAWAN
 * @property string $N_TAHUN
 * @property string $N_BULANAWAL
 * @property string $V_BULANAWAL
 * @property string $N_BULANAKHIR
 * @property string $V_BULANAKHIR
 * @property string $C_PAYAREA
 * @property string $C_KODE_PSA
 * @property string $C_NAMA_PSA
 * @property string $C_KODE_KPP
 * @property string $V_NAMA_KPP
 * @property string $V_NPWP_KANTOR
 * @property string $V_NPWP_PRIBADI
 * @property string $V_JABATAN
 * @property string $V_ALAMAT
 * @property string $V_ALAMAT_KANTOR
 * @property string $C_BISNIS_AREA
 * @property string $V_BISNIS_AREA
 * @property string $C_KODE_FIN
 * @property string $V_NAMA_FIN
 * @property string $C_STATUS_NIKAH
 * @property string $C_GENDER
 * @property string $V_ADT
 * @property string $N_01
 * @property string $N_02
 * @property string $N_03
 * @property string $N_04
 * @property string $N_05
 * @property string $N_06
 * @property string $N_07
 * @property string $N_08
 * @property string $N_09
 * @property string $N_10
 * @property string $N_11
 * @property string $N_12
 * @property string $N_13
 * @property string $N_14
 * @property string $N_15
 * @property string $N_16
 * @property string $N_17
 * @property string $N_18
 * @property string $N_19
 * @property string $N_20
 * @property string $N_21
 * @property string $N_22
 * @property string $N_23
 * @property string $N_24
 * @property string $N_NIK_TTD
 * @property string $V_NAMA_TTD
 * @property string $V_JABATAN_TTD
 * @property string $V_KOTA_TTD
 * @property string $D_TGL_UBAH
 * @property string $IDOPERATOR
 * @property string $TEMPLATE_TTD
 * @property string $V_NAMA_PEMOTONG
 * @property string $KODE_PAJAK
 */
class RptSpt17212017 extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'rpt_spt1721_2017';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('N_URUT, C_NAMA_PSA', 'length', 'max'=>60),
			array('N_NIK, C_KODE_FIN, N_NIK_TTD', 'length', 'max'=>6),
			array('V_NAMA_KARYAWAN, V_NAMA_TTD, V_KOTA_TTD', 'length', 'max'=>50),
			array('N_TAHUN, C_KODE_PSA, C_KODE_KPP, C_BISNIS_AREA, V_ADT', 'length', 'max'=>4),
			array('N_BULANAWAL, N_BULANAKHIR, C_PAYAREA', 'length', 'max'=>2),
			array('V_BULANAWAL, V_BULANAKHIR', 'length', 'max'=>10),
			array('V_NAMA_KPP', 'length', 'max'=>40),
			array('V_NPWP_KANTOR, V_NPWP_PRIBADI', 'length', 'max'=>15),
			array('V_JABATAN, V_ALAMAT, V_ALAMAT_KANTOR, V_BISNIS_AREA, V_NAMA_FIN, V_JABATAN_TTD', 'length', 'max'=>100),
			array('C_STATUS_NIKAH, C_GENDER', 'length', 'max'=>1),
			array('N_01, N_02, N_03, N_04, N_05, N_06, N_07, N_08, N_09, N_10, N_11, N_12, N_13, N_14, N_15, N_16, N_17, N_18, N_19, N_20, N_21, N_22, N_23, N_24', 'length', 'max'=>12),
			array('IDOPERATOR', 'length', 'max'=>8),
			array('V_NAMA_PEMOTONG, KODE_PAJAK', 'length', 'max'=>255),
			array('D_TGL_UBAH, TEMPLATE_TTD', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('N_URUT, N_NIK, V_NAMA_KARYAWAN, N_TAHUN, N_BULANAWAL, V_BULANAWAL, N_BULANAKHIR, V_BULANAKHIR, C_PAYAREA, C_KODE_PSA, C_NAMA_PSA, C_KODE_KPP, V_NAMA_KPP, V_NPWP_KANTOR, V_NPWP_PRIBADI, V_JABATAN, V_ALAMAT, V_ALAMAT_KANTOR, C_BISNIS_AREA, V_BISNIS_AREA, C_KODE_FIN, V_NAMA_FIN, C_STATUS_NIKAH, C_GENDER, V_ADT, N_01, N_02, N_03, N_04, N_05, N_06, N_07, N_08, N_09, N_10, N_11, N_12, N_13, N_14, N_15, N_16, N_17, N_18, N_19, N_20, N_21, N_22, N_23, N_24, N_NIK_TTD, V_NAMA_TTD, V_JABATAN_TTD, V_KOTA_TTD, D_TGL_UBAH, IDOPERATOR, TEMPLATE_TTD, V_NAMA_PEMOTONG, KODE_PAJAK', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'N_URUT' => 'N Urut',
			'N_NIK' => 'N Nik',
			'V_NAMA_KARYAWAN' => 'V Nama Karyawan',
			'N_TAHUN' => 'N Tahun',
			'N_BULANAWAL' => 'N Bulanawal',
			'V_BULANAWAL' => 'V Bulanawal',
			'N_BULANAKHIR' => 'N Bulanakhir',
			'V_BULANAKHIR' => 'V Bulanakhir',
			'C_PAYAREA' => 'C Payarea',
			'C_KODE_PSA' => 'C Kode Psa',
			'C_NAMA_PSA' => 'C Nama Psa',
			'C_KODE_KPP' => 'C Kode Kpp',
			'V_NAMA_KPP' => 'V Nama Kpp',
			'V_NPWP_KANTOR' => 'V Npwp Kantor',
			'V_NPWP_PRIBADI' => 'V Npwp Pribadi',
			'V_JABATAN' => 'V Jabatan',
			'V_ALAMAT' => 'V Alamat',
			'V_ALAMAT_KANTOR' => 'V Alamat Kantor',
			'C_BISNIS_AREA' => 'C Bisnis Area',
			'V_BISNIS_AREA' => 'V Bisnis Area',
			'C_KODE_FIN' => 'C Kode Fin',
			'V_NAMA_FIN' => 'V Nama Fin',
			'C_STATUS_NIKAH' => 'C Status Nikah',
			'C_GENDER' => 'C Gender',
			'V_ADT' => 'V Adt',
			'N_01' => 'N 01',
			'N_02' => 'N 02',
			'N_03' => 'N 03',
			'N_04' => 'N 04',
			'N_05' => 'N 05',
			'N_06' => 'N 06',
			'N_07' => 'N 07',
			'N_08' => 'N 08',
			'N_09' => 'N 09',
			'N_10' => 'N 10',
			'N_11' => 'N 11',
			'N_12' => 'N 12',
			'N_13' => 'N 13',
			'N_14' => 'N 14',
			'N_15' => 'N 15',
			'N_16' => 'N 16',
			'N_17' => 'N 17',
			'N_18' => 'N 18',
			'N_19' => 'N 19',
			'N_20' => 'N 20',
			'N_21' => 'N 21',
			'N_22' => 'N 22',
			'N_23' => 'N 23',
			'N_24' => 'N 24',
			'N_NIK_TTD' => 'N Nik Ttd',
			'V_NAMA_TTD' => 'V Nama Ttd',
			'V_JABATAN_TTD' => 'V Jabatan Ttd',
			'V_KOTA_TTD' => 'V Kota Ttd',
			'D_TGL_UBAH' => 'D Tgl Ubah',
			'IDOPERATOR' => 'Idoperator',
			'TEMPLATE_TTD' => 'Template Ttd',
			'V_NAMA_PEMOTONG' => 'V Nama Pemotong',
			'KODE_PAJAK' => 'Kode Pajak',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('N_URUT',$this->N_URUT,true);
		$criteria->compare('N_NIK',$this->N_NIK,true);
		$criteria->compare('V_NAMA_KARYAWAN',$this->V_NAMA_KARYAWAN,true);
		$criteria->compare('N_TAHUN',$this->N_TAHUN,true);
		$criteria->compare('N_BULANAWAL',$this->N_BULANAWAL,true);
		$criteria->compare('V_BULANAWAL',$this->V_BULANAWAL,true);
		$criteria->compare('N_BULANAKHIR',$this->N_BULANAKHIR,true);
		$criteria->compare('V_BULANAKHIR',$this->V_BULANAKHIR,true);
		$criteria->compare('C_PAYAREA',$this->C_PAYAREA,true);
		$criteria->compare('C_KODE_PSA',$this->C_KODE_PSA,true);
		$criteria->compare('C_NAMA_PSA',$this->C_NAMA_PSA,true);
		$criteria->compare('C_KODE_KPP',$this->C_KODE_KPP,true);
		$criteria->compare('V_NAMA_KPP',$this->V_NAMA_KPP,true);
		$criteria->compare('V_NPWP_KANTOR',$this->V_NPWP_KANTOR,true);
		$criteria->compare('V_NPWP_PRIBADI',$this->V_NPWP_PRIBADI,true);
		$criteria->compare('V_JABATAN',$this->V_JABATAN,true);
		$criteria->compare('V_ALAMAT',$this->V_ALAMAT,true);
		$criteria->compare('V_ALAMAT_KANTOR',$this->V_ALAMAT_KANTOR,true);
		$criteria->compare('C_BISNIS_AREA',$this->C_BISNIS_AREA,true);
		$criteria->compare('V_BISNIS_AREA',$this->V_BISNIS_AREA,true);
		$criteria->compare('C_KODE_FIN',$this->C_KODE_FIN,true);
		$criteria->compare('V_NAMA_FIN',$this->V_NAMA_FIN,true);
		$criteria->compare('C_STATUS_NIKAH',$this->C_STATUS_NIKAH,true);
		$criteria->compare('C_GENDER',$this->C_GENDER,true);
		$criteria->compare('V_ADT',$this->V_ADT,true);
		$criteria->compare('N_01',$this->N_01,true);
		$criteria->compare('N_02',$this->N_02,true);
		$criteria->compare('N_03',$this->N_03,true);
		$criteria->compare('N_04',$this->N_04,true);
		$criteria->compare('N_05',$this->N_05,true);
		$criteria->compare('N_06',$this->N_06,true);
		$criteria->compare('N_07',$this->N_07,true);
		$criteria->compare('N_08',$this->N_08,true);
		$criteria->compare('N_09',$this->N_09,true);
		$criteria->compare('N_10',$this->N_10,true);
		$criteria->compare('N_11',$this->N_11,true);
		$criteria->compare('N_12',$this->N_12,true);
		$criteria->compare('N_13',$this->N_13,true);
		$criteria->compare('N_14',$this->N_14,true);
		$criteria->compare('N_15',$this->N_15,true);
		$criteria->compare('N_16',$this->N_16,true);
		$criteria->compare('N_17',$this->N_17,true);
		$criteria->compare('N_18',$this->N_18,true);
		$criteria->compare('N_19',$this->N_19,true);
		$criteria->compare('N_20',$this->N_20,true);
		$criteria->compare('N_21',$this->N_21,true);
		$criteria->compare('N_22',$this->N_22,true);
		$criteria->compare('N_23',$this->N_23,true);
		$criteria->compare('N_24',$this->N_24,true);
		$criteria->compare('N_NIK_TTD',$this->N_NIK_TTD,true);
		$criteria->compare('V_NAMA_TTD',$this->V_NAMA_TTD,true);
		$criteria->compare('V_JABATAN_TTD',$this->V_JABATAN_TTD,true);
		$criteria->compare('V_KOTA_TTD',$this->V_KOTA_TTD,true);
		$criteria->compare('D_TGL_UBAH',$this->D_TGL_UBAH,true);
		$criteria->compare('IDOPERATOR',$this->IDOPERATOR,true);
		$criteria->compare('TEMPLATE_TTD',$this->TEMPLATE_TTD,true);
		$criteria->compare('V_NAMA_PEMOTONG',$this->V_NAMA_PEMOTONG,true);
		$criteria->compare('KODE_PAJAK',$this->KODE_PAJAK,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return RptSpt17212017 the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
