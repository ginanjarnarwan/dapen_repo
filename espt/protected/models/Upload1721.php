<?php
class Upload1721 extends CFormModel {

    public $tahun;
    public $fileupload;
    public $assrole;

    public function rules() {
        return array(
            array('tahun, fileupload', 'required'),
            array('fileupload', 'file', 'types' => 'xls,xlsx', 'allowEmpty' => false, 'on'=>'insert'),
            array('fileupload', 'file', 'types' => 'xls,xlsx', 'allowEmpty' => false, 'on'=>'update'),
        );
    }

    public function attributeLabels() {
        return array(
            'tahun' => 'Tahun',
            'fileupload' => 'File Upload',
            'assrole' => 'Nama Pemotong'
        );
    }

}
?>