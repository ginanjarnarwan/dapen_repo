<?php

/**
 * This is the model class for table "t_payslip".
 *
 * The followings are the available columns in table 't_payslip':
 * @property string $NIK
 * @property string $NAMA
 * @property string $ESGRP_TXT
 * @property string $PERSA
 * @property string $SUBAREA
 * @property string $BAND
 * @property string $ROLE_CATEGORY
 * @property string $NAMA_POSISI
 * @property string $NAMA_UNIT
 * @property string $BASE_SALARY
 * @property string $BANTUAN_KEMAHALAN
 * @property string $TUNJANGAN_PAJAK
 * @property string $INSENTIF_PREMIUM
 * @property string $TUNJANGAN_ASKEDIR
 * @property string $BBP
 * @property string $ADJUSTMENT_BBP
 * @property string $UANG_LEMBUR
 * @property string $INSENTIF_JABATAN
 * @property string $ADJUSTMENT_INSENTIF_JBT
 * @property string $BPJS_KES
 * @property string $TUNJANGAN_DPLK
 * @property string $JKK_JAMSOSTEK
 * @property string $JHT_JAMSOSTEK
 * @property string $JKM_JAMSOSTEK
 * @property string $BPJS_JP
 * @property string $RAPEL_BPJS_JP
 * @property string $POTONGAN_ASKEDIR
 * @property string $SIMPANAN_KOPERASI
 * @property string $SUMBANGAN_KEMATIAN
 * @property string $POTONGAN_TASPEN
 * @property string $RAPEL_TASPEN
 * @property string $POTONGAN_IDP
 * @property string $TABUNGAN_WAJIB_PERUMAHAN
 * @property string $ANGSURAN_TETAP_KOPEGTEL
 * @property string $ANGSURAN_LAIN_KOPEGTEL
 * @property string $POTONGAN_INFAQ
 * @property string $POTONGAN_DANA_PERSEMBAHAN
 * @property string $POTONGAN_DANA_PUNIA
 * @property string $POTONGAN_ZAKAT
 * @property string $POT_TMB_DANA_PERSEMBAHAN
 * @property string $POT_TMB_DANA_PUNIA
 * @property string $IURAN_ORG_KARYAWAN
 * @property string $POT_BAKTI_U_NEGRI
 * @property string $POT_DPLK_EE
 * @property string $POT_DPLK_TITIPAN
 * @property string $POT_LAIN_LAIN
 * @property string $POTAONGAN_CIA_TCASH
 * @property string $POT_BPJS_KES
 * @property string $POT_JHT_JAMSOSTEK
 * @property string $POT_JHT_JAMSOSTEK_EE
 * @property string $POT_JKK_JKM_JAMSOSTEK
 * @property string $POT_BPJS_JP
 * @property string $RPL_POT_BPJS_JP
 * @property string $POT_BPJS_JP_EE
 * @property string $ANGSURAN_KOPTEL
 * @property string $ANGSURAN_LAIN_KOPTEL
 * @property integer $BULAN
 * @property integer $TAHUN
 */
class TPayslip extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public $fileupload,$total;

	public function tableName()
	{
		return 't_payslip';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			// array('fileupload', 'required'),
			array('BULAN, TAHUN', 'numerical', 'integerOnly'=>true),
			array('NIK, NAMA', 'length', 'max'=>255),
			array('TGC', 'length', 'max'=>60),
			array('fileupload,CREATED_BY,CREATED_TIME', 'safe'),
			array('fileupload', 'file', 'types' => 'csv', 'allowEmpty' => false, 'on'=>'upload_xls'),
			array(
                'CREATED_BY',
                'default',
                'value' => Yii::app()->user->id,
                'setOnEmpty' => false,
                'on' => 'insert'
            ),
            array(
                'CREATED_TIME',
                'default',
                'value' => new CDbExpression('NOW()'),
                'setOnEmpty' => false,
                'on' => 'insert'
            ),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('NIK, NAMA, TGC, BULAN, TAHUN', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'NIK' => 'Nik',
			'NAMA' => 'Nama',
			'ESGRP_TXT' => 'Esgrp Txt',
			'PERSA' => 'Persa',
			'SUBAREA' => 'Subarea',
			'BAND' => 'Band',
			'ROLE_CATEGORY' => 'Role Category',
			'NAMA_POSISI' => 'Nama Posisi',
			'NAMA_UNIT' => 'Nama Unit',
			'BASE_SALARY' => 'Base Salary',
			'BANTUAN_KEMAHALAN' => 'Bantuan Kemahalan',
			'TUNJANGAN_PAJAK' => 'Tunjangan Pajak',
			'INSENTIF_PREMIUM' => 'Insentif Premium',
			'TUNJANGAN_ASKEDIR' => 'Tunjangan Askedir',
			'BBP' => 'Bbp',
			'ADJUSTMENT_BBP' => 'Adjustment Bbp',
			'UANG_LEMBUR' => 'Uang Lembur',
			'INSENTIF_JABATAN' => 'Insentif Jabatan',
			'ADJUSTMENT_INSENTIF_JBT' => 'Adjustment Insentif Jbt',
			'BPJS_KES' => 'Bpjs Kes',
			'TUNJANGAN_DPLK' => 'Tunjangan Dplk',
			'JKK_JAMSOSTEK' => 'Jkk Jamsostek',
			'JHT_JAMSOSTEK' => 'Jht Jamsostek',
			'JKM_JAMSOSTEK' => 'Jkm Jamsostek',
			'BPJS_JP' => 'Bpjs Jp',
			'RAPEL_BPJS_JP' => 'Rapel Bpjs Jp',
			'POTONGAN_ASKEDIR' => 'Potongan Askedir',
			'SIMPANAN_KOPERASI' => 'Simpanan Koperasi',
			'SUMBANGAN_KEMATIAN' => 'Sumbangan Kematian',
			'POTONGAN_TASPEN' => 'Potongan Taspen',
			'RAPEL_TASPEN' => 'Rapel Taspen',
			'POTONGAN_IDP' => 'Potongan Idp',
			'TABUNGAN_WAJIB_PERUMAHAN' => 'Tabungan Wajib Perumahan',
			'ANGSURAN_TETAP_KOPEGTEL' => 'Angsuran Tetap Kopegtel',
			'ANGSURAN_LAIN_KOPEGTEL' => 'Angsuran Lain Kopegtel',
			'POTONGAN_INFAQ' => 'Potongan Infaq',
			'POTONGAN_DANA_PERSEMBAHAN' => 'Potongan Dana Persembahan',
			'POTONGAN_DANA_PUNIA' => 'Potongan Dana Punia',
			'POTONGAN_ZAKAT' => 'Potongan Zakat',
			'POT_TMB_DANA_PERSEMBAHAN' => 'Pot Tmb Dana Persembahan',
			'POT_TMB_DANA_PUNIA' => 'Pot Tmb Dana Punia',
			'IURAN_ORG_KARYAWAN' => 'Iuran Org Karyawan',
			'POT_BAKTI_U_NEGRI' => 'Pot Bakti U Negri',
			'POT_DPLK_EE' => 'Pot Dplk Ee',
			'POT_DPLK_TITIPAN' => 'Pot Dplk Titipan',
			'POT_LAIN_LAIN' => 'Pot Lain Lain',
			'POTAONGAN_CIA_TCASH' => 'Potaongan Cia Tcash',
			'POT_BPJS_KES' => 'Pot Bpjs Kes',
			'POT_JHT_JAMSOSTEK' => 'Pot Jht Jamsostek',
			'POT_JHT_JAMSOSTEK_EE' => 'Pot Jht Jamsostek Ee',
			'POT_JKK_JKM_JAMSOSTEK' => 'Pot Jkk Jkm Jamsostek',
			'POT_BPJS_JP' => 'Pot Bpjs Jp',
			'RPL_POT_BPJS_JP' => 'Rpl Pot Bpjs Jp',
			'POT_BPJS_JP_EE' => 'Pot Bpjs Jp Ee',
			'ANGSURAN_KOPTEL' => 'Angsuran Koptel',
			'ANGSURAN_LAIN_KOPTEL' => 'Angsuran Lain Koptel',
			'BULAN' => 'Bulan',
			'TAHUN' => 'Tahun',
			'fileupload'=>'File Payslip (.csv)'
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('NIK',$this->NIK,true);
		$criteria->compare('NAMA',$this->NAMA,true);
		$criteria->compare('BULAN',$this->BULAN);
		$criteria->compare('TAHUN',$this->TAHUN);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function listPerBulan()
	{
		$criteria = new CDbCriteria;
		$criteria->select = 'BULAN,TAHUN,sum(coalesce(TGC,0)) as total';
		$criteria->group = 'BULAN,TAHUN';
		// $criteria->params = array(':tahun'=>$this->TAHUN,':bulan'=>$this->BULAN);
		$criteria->compare('BULAN',$this->BULAN);
		$criteria->compare('TAHUN',date("Y"));
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TPayslip the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
