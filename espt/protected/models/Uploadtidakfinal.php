<?php
class Uploadtidakfinal extends CFormModel {

    public $tahun;
    public $fileupload;

    public function rules() {
        return array(
            array('tahun, fileupload', 'required'),
            array('fileupload', 'file', 'types' => array('xls','xlsx'), 'allowEmpty' => false, 'on'=>'upload_xls'),
        );
    }

    public function attributeLabels() {
        return array(
            'tahun' => 'Tahun',
            'fileupload' => 'File Upload'
        );
    }

}
?>