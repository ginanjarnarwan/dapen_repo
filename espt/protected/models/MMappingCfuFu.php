<?php

/**
 * This is the model class for table "m_mapping_cfu_fu".
 *
 * The followings are the available columns in table 'm_mapping_cfu_fu':
 * @property integer $ID
 * @property string $KODE_DIVISI
 * @property string $DIVISI
 * @property string $CFU_FU
 * @property string $JENIS
 */
class MMappingCfuFu extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'm_mapping_cfu_fu';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('KODE_DIVISI', 'length', 'max'=>10),
			array('DIVISI, CFU_FU, JENIS, KODE_SEGMENTASI', 'length', 'max'=>255),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, KODE_DIVISI, DIVISI, CFU_FU, JENIS', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'KODE_DIVISI' => 'Kode Divisi',
			'DIVISI' => 'Divisi',
			'CFU_FU' => 'Cfu Fu',
			'JENIS' => 'Jenis',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('KODE_DIVISI',$this->KODE_DIVISI,true);
		$criteria->compare('DIVISI',$this->DIVISI,true);
		$criteria->compare('CFU_FU',$this->CFU_FU,true);
		$criteria->compare('JENIS',$this->JENIS,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return MMappingCfuFu the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
