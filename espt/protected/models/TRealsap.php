<?php

/**
 * This is the model class for table "t_realsap".
 *
 * The followings are the available columns in table 't_realsap':
 * @property integer $ID
 * @property integer $TAHUN
 * @property integer $BULAN
 * @property string $REALISASI
 */
class TRealsap extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public $total,$TAHUNFILTER,$BULANFILTER;
	public function tableName()
	{
		return 't_realsap';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TAHUN, BULAN, REALISASI', 'required'),
			array('TAHUN, BULAN', 'numerical', 'integerOnly'=>true),
			array('REALISASI', 'length', 'max'=>60),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('CREATED_BY,CREATED_TIME,TAHUNFILTER,BULANFILTER', 'safe'),
			array(
                'CREATED_BY',
                'default',
                'value' => Yii::app()->user->id,
                'setOnEmpty' => false,
                'on' => 'insert'
            ),
            array(
                'CREATED_TIME',
                'default',
                'value' => new CDbExpression('NOW()'),
                'setOnEmpty' => false,
                'on' => 'insert'
            ),
			array('ID, TAHUN, BULAN, REALISASI', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'TAHUN' => 'Tahun',
			'BULAN' => 'Bulan',
			'REALISASI' => 'Realisasi',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;
		$criteria->order = 'TAHUN,BULAN DESC';
		$criteria->compare('ID',$this->ID);
		$criteria->compare('TAHUN',$this->TAHUN);
		$criteria->compare('BULAN',$this->BULAN);
		$criteria->compare('REALISASI',$this->REALISASI,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function listPerBulan($blnshow)
	{
		$criteria = new CDbCriteria;
		$criteria->order = 'TAHUN,BULAN ASC';
		$criteria->select = 'BULAN,TAHUN,REALISASI as total';
		$par = array();
		if (empty($this->TAHUNFILTER) && empty($this->BULANFILTER)) {
			$tahun = date("Y");
			$bulan = $this->BULAN;
			$criteria->condition = 'BULAN <> :bulan';
			$par[':bulan'] = $blnshow;
		}else{
			$bulan = $this->BULANFILTER;
			$tahun = $this->TAHUNFILTER;
		}
		$criteria->params = $par;
		// $criteria->join = "LEFT JOIN t_payslip ON "
		$criteria->compare('BULAN',$bulan);
		$criteria->compare('TAHUN',$tahun);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	public function listNow()
	{
		$criteria = new CDbCriteria;
		$criteria->order = 'TAHUN,BULAN ASC';
		$criteria->select = 'BULAN,TAHUN,REALISASI as total';
		// $criteria->join = "LEFT JOIN t_payslip ON "
		$criteria->compare('BULAN',$this->BULAN);
		$criteria->compare('TAHUN',$this->TAHUN);
		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TRealsap the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
