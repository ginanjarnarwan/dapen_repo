<?php

/**
 * This is the model class for table "t_pros_cfu_fu".
 *
 * The followings are the available columns in table 't_pros_cfu_fu':
 * @property integer $ID
 * @property integer $TAHUN
 * @property integer $BULAN
 * @property string $BASE_SALARY
 * @property string $BANTUAN_KEMAHALAN
 * @property string $INSENTIF_PREMIUM
 * @property string $INSENTIF_JABATAN
 * @property string $ADJUSTMENT_INSENTIF_JBT
 * @property string $SEGMENTASI_KODE
 * @property string $TREG_NTREG
 * @property string $SEGMENTASI
 * @property string $JUMLAH
 * @property string $TOTAL
 * @property string $PROSENTASE
 */
class TProsCfuFu extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 't_pros_cfu_fu';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('TAHUN, BULAN', 'numerical', 'integerOnly'=>true),
			array('BASE_SALARY, BANTUAN_KEMAHALAN, INSENTIF_PREMIUM, INSENTIF_JABATAN, ADJUSTMENT_INSENTIF_JBT, JUMLAH, TOTAL, PROSENTASE', 'length', 'max'=>60),
			array('SEGMENTASI_KODE, TREG_NTREG, SEGMENTASI', 'length', 'max'=>255),
			array('CREATED_BY,CREATED_TIME','safe'),
			array(
                'CREATED_BY',
                'default',
                'value' => Yii::app()->user->id,
                'setOnEmpty' => false,
                'on' => 'insert'
            ),
            array(
                'CREATED_TIME',
                'default',
                'value' => new CDbExpression('NOW()'),
                'setOnEmpty' => false,
                'on' => 'insert'
            ),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('ID, TAHUN, BULAN, BASE_SALARY, BANTUAN_KEMAHALAN, INSENTIF_PREMIUM, INSENTIF_JABATAN, ADJUSTMENT_INSENTIF_JBT, SEGMENTASI_KODE, TREG_NTREG, SEGMENTASI, JUMLAH, TOTAL, PROSENTASE', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'ID' => 'ID',
			'TAHUN' => 'Tahun',
			'BULAN' => 'Bulan',
			'BASE_SALARY' => 'Base Salary',
			'BANTUAN_KEMAHALAN' => 'Bantuan Kemahalan',
			'INSENTIF_PREMIUM' => 'Insentif Premium',
			'INSENTIF_JABATAN' => 'Insentif Jabatan',
			'ADJUSTMENT_INSENTIF_JBT' => 'Adjustment Insentif Jbt',
			'SEGMENTASI_KODE' => 'Segmentasi Kode',
			'TREG_NTREG' => 'Treg Ntreg',
			'SEGMENTASI' => 'Segmentasi',
			'JUMLAH' => 'Jumlah',
			'TOTAL' => 'Total',
			'PROSENTASE' => 'Prosentase',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('ID',$this->ID);
		$criteria->compare('TAHUN',$this->TAHUN);
		$criteria->compare('BULAN',$this->BULAN);
		$criteria->compare('BASE_SALARY',$this->BASE_SALARY,true);
		$criteria->compare('BANTUAN_KEMAHALAN',$this->BANTUAN_KEMAHALAN,true);
		$criteria->compare('INSENTIF_PREMIUM',$this->INSENTIF_PREMIUM,true);
		$criteria->compare('INSENTIF_JABATAN',$this->INSENTIF_JABATAN,true);
		$criteria->compare('ADJUSTMENT_INSENTIF_JBT',$this->ADJUSTMENT_INSENTIF_JBT,true);
		$criteria->compare('SEGMENTASI_KODE',$this->SEGMENTASI_KODE,true);
		$criteria->compare('TREG_NTREG',$this->TREG_NTREG,true);
		$criteria->compare('SEGMENTASI',$this->SEGMENTASI,true);
		$criteria->compare('JUMLAH',$this->JUMLAH,true);
		$criteria->compare('TOTAL',$this->TOTAL,true);
		$criteria->compare('PROSENTASE',$this->PROSENTASE,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return TProsCfuFu the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
