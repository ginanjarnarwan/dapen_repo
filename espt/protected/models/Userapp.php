<?php

/**
 * This is the model class for table "userapp".
 *
 * The followings are the available columns in table 'userapp':
 * @property integer $id
 * @property string $telkomnik
 * @property string $nama
 * @property string $tanggal_lahir
 * @property string $jenis_kelamin
 * @property string $alamat
 * @property string $email
 * @property string $nomor_handphone
 * @property string $ptkp
 * @property string $jumlah_ptkp
 * @property string $npwp
 * @property string $nama_jabatan
 * @property string $karyawan_asing
 * @property string $kode_negara_domisili
 * @property string $password
 * @property string $create_date
 * @property string $activation_date
 * @property string $status
 */
class Userapp extends CActiveRecord
{
	public $password2;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'userapp';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('telkomnik, nomor_handphone', 'length', 'max'=>16),
			array('nama, tanggal_lahir, jenis_kelamin, alamat, ptkp, jumlah_ptkp, npwp, nama_jabatan, karyawan_asing, kode_negara_domisili, password, status', 'length', 'max'=>255),
			array('email', 'length', 'max'=>100),
			array('create_date, activation_date', 'safe'),
			array('telkomnik, tanggal_lahir, nomor_handphone, password, password2', 'required'),
			array('telkomnik, tanggal_lahir, password, password2', 'validateRegister'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, telkomnik, nama, tanggal_lahir, jenis_kelamin, alamat, email, nomor_handphone, ptkp, jumlah_ptkp, npwp, nama_jabatan, karyawan_asing, kode_negara_domisili, password, create_date, activation_date, status', 'safe', 'on'=>'search'),
		);
	}

	public function validateRegister(){
		$usr = Userapp::model()->findByAttributes(array('telkomnik'=>$this->telkomnik));
		if (!empty($usr)) {
			if (date_format(date_create($usr->tanggal_lahir),'Y-m-d') == date_format(date_create($this->tanggal_lahir),'Y-m-d')) {
				if ($this->password != $this->password2) {
					$this->addError('password', 'Password tidak sama');		
					$this->addError('password2', 'Password tidak sama');		
				}
			}else{
				$this->addError('tanggal_lahir', 'Tanggal lahir tidak sesuai dengan data di sistem');	
			}
		}else{
			$this->addError('telkomnik', 'NIK tidak terdaftar');
		}
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'telkomnik' => 'Telkomnik',
			'nama' => 'Nama',
			'tanggal_lahir' => 'Tanggal Lahir',
			'jenis_kelamin' => 'Jenis Kelamin',
			'alamat' => 'Alamat',
			'email' => 'Email',
			'nomor_handphone' => 'Nomor Handphone',
			'ptkp' => 'Ptkp',
			'jumlah_ptkp' => 'Jumlah Ptkp',
			'npwp' => 'Npwp',
			'nama_jabatan' => 'Nama Jabatan',
			'karyawan_asing' => 'Karyawan Asing',
			'kode_negara_domisili' => 'Kode Negara Domisili',
			'password' => 'Password',
			'password2' => 'Ulangi Password',
			'create_date' => 'Create Date',
			'activation_date' => 'Activation Date',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('telkomnik',$this->telkomnik,true);
		$criteria->compare('nama',$this->nama,true);
		$criteria->compare('tanggal_lahir',$this->tanggal_lahir,true);
		$criteria->compare('jenis_kelamin',$this->jenis_kelamin,true);
		$criteria->compare('alamat',$this->alamat,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('nomor_handphone',$this->nomor_handphone,true);
		$criteria->compare('ptkp',$this->ptkp,true);
		$criteria->compare('jumlah_ptkp',$this->jumlah_ptkp,true);
		$criteria->compare('npwp',$this->npwp,true);
		$criteria->compare('nama_jabatan',$this->nama_jabatan,true);
		$criteria->compare('karyawan_asing',$this->karyawan_asing,true);
		$criteria->compare('kode_negara_domisili',$this->kode_negara_domisili,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('create_date',$this->create_date,true);
		$criteria->compare('activation_date',$this->activation_date,true);
		$criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Userapp the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
