<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $id
 * @property string $nik
 * @property string $password
 */
class User extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public $V_NAMA_KARYAWAN;
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('nik', 'required'),
			array('nik', 'length', 'max'=>9),
			array('password', 'safe'),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, nik, password, V_NAMA_KARYAWAN', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'nik' => 'NIK',
			'password' => 'Password',
			'V_NAMA_KARYAWAN' => 'Nama',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.
		$tabel_cek = array();
        $tahun_cek = array();
        $tahun_current = date("Y");
        $tahun_min = date("Y") - 5;
        for ($i=$tahun_current; $i >= $tahun_min; $i--) { 
            $namatbl = 'rpt_spt1721_'.$i;
            $cektab = Yii::app()->db->createCommand("SHOW TABLES LIKE :par")
                                        ->bindParam(':par',$namatbl)
                                        ->queryAll();
            if (count($cektab) > 0 ){
                $tabel_cek[] = $namatbl;
                $tahun_cek[] = $i;
            }
        }
		$criteria=new CDbCriteria;
		$par = array();
        if(count($tabel_cek) > 0){
        	$criteria->select = 't.*,x.V_NAMA_KARYAWAN';
        	$criteria->join = 'JOIN rpt_spt1721_'.max($tahun_cek).' x ON t.nik=x.N_NIK';
        	if (!empty($this->V_NAMA_KARYAWAN)) {
        		$criteria->condition = " x.V_NAMA_KARYAWAN LIKE :nama";
        		$par[":nama"] = "%".$this->V_NAMA_KARYAWAN."%";
        	}
        }

        $criteria->params = $par;
		$criteria->compare('id',$this->id);
		$criteria->compare('nik',$this->nik,true);
		$criteria->compare('password',$this->password,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
