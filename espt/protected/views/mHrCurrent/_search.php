<?php
/* @var $this MHrCurrentController */
/* @var $model MHrCurrent */
/* @var $form CActiveForm */
?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'htmlOptions'=>array('class'=>'form-horizontal')
)); ?>

<div class="form-group">
	<?php echo $form->labelEx($model,'ID', array('class'=>'col-sm-2 control-label')); ?>
	<div class="col-sm-10">
		<?php echo $form->textField($model,'ID',array('class' => 'form-control')); ?>
	</div>
</div>
<div class="form-group">
	<?php echo $form->labelEx($model,'NIK', array('class'=>'col-sm-2 control-label')); ?>
	<div class="col-sm-10">
		<?php echo $form->textField($model,'NIK',array('class' => 'form-control', 'size'=>9,'maxlength'=>9)); ?>
	</div>
</div>
<div class="form-group">
	<?php echo $form->labelEx($model,'NAMA', array('class'=>'col-sm-2 control-label')); ?>
	<div class="col-sm-10">
		<?php echo $form->textField($model,'NAMA',array('class' => 'form-control', 'size'=>60,'maxlength'=>70)); ?>
	</div>
</div>
<div class="form-group">
	<?php echo $form->labelEx($model,'PERSA', array('class'=>'col-sm-2 control-label')); ?>
	<div class="col-sm-10">
		<?php echo $form->textField($model,'PERSA',array('class' => 'form-control', 'size'=>10,'maxlength'=>10)); ?>
	</div>
</div>
<div class="form-group">
	<?php echo $form->labelEx($model,'SUBAREA', array('class'=>'col-sm-2 control-label')); ?>
	<div class="col-sm-10">
		<?php echo $form->textField($model,'SUBAREA',array('class' => 'form-control', 'size'=>10,'maxlength'=>10)); ?>
	</div>
</div>
<div class="form-group">
	<?php echo $form->labelEx($model,'BAND', array('class'=>'col-sm-2 control-label')); ?>
	<div class="col-sm-10">
		<?php echo $form->textField($model,'BAND',array('class' => 'form-control', 'size'=>3,'maxlength'=>3)); ?>
	</div>
</div>
<div class="form-group">
	<?php echo $form->labelEx($model,'NAMA_POSISI', array('class'=>'col-sm-2 control-label')); ?>
	<div class="col-sm-10">
		<?php echo $form->textField($model,'NAMA_POSISI',array('class' => 'form-control', 'size'=>60,'maxlength'=>255)); ?>
	</div>
</div>
<div class="form-group">
	<?php echo $form->labelEx($model,'NAMA_UNIT', array('class'=>'col-sm-2 control-label')); ?>
	<div class="col-sm-10">
		<?php echo $form->textField($model,'NAMA_UNIT',array('class' => 'form-control', 'size'=>60,'maxlength'=>255)); ?>
	</div>
</div>
<div class="form-group">
	<?php echo $form->labelEx($model,'C_HOST', array('class'=>'col-sm-2 control-label')); ?>
	<div class="col-sm-10">
		<?php echo $form->textField($model,'C_HOST',array('class' => 'form-control', 'size'=>10,'maxlength'=>10)); ?>
	</div>
</div>
<div class="form-group">
	<?php echo $form->labelEx($model,'KODE_LOKER', array('class'=>'col-sm-2 control-label')); ?>
	<div class="col-sm-10">
		<?php echo $form->textField($model,'KODE_LOKER',array('class' => 'form-control', 'size'=>60,'maxlength'=>255)); ?>
	</div>
</div>
<div class="form-group">
	<?php echo $form->labelEx($model,'KODE_DIVISI', array('class'=>'col-sm-2 control-label')); ?>
	<div class="col-sm-10">
		<?php echo $form->textField($model,'KODE_DIVISI',array('class' => 'form-control', 'size'=>60,'maxlength'=>255)); ?>
	</div>
</div>
<div class="form-group">
	<?php echo $form->labelEx($model,'DIVISI', array('class'=>'col-sm-2 control-label')); ?>
	<div class="col-sm-10">
		<?php echo $form->textField($model,'DIVISI',array('class' => 'form-control', 'size'=>60,'maxlength'=>255)); ?>
	</div>
</div>
<div class="form-group">
	<?php echo $form->labelEx($model,'OBJID_POSISI', array('class'=>'col-sm-2 control-label')); ?>
	<div class="col-sm-10">
		<?php echo $form->textField($model,'OBJID_POSISI',array('class' => 'form-control', 'size'=>60,'maxlength'=>255)); ?>
	</div>
</div>
<div class="form-group">
	<?php echo $form->labelEx($model,'BULAN', array('class'=>'col-sm-2 control-label')); ?>
	<div class="col-sm-10">
		<?php echo $form->textField($model,'BULAN',array('class' => 'form-control')); ?>
	</div>
</div>
<div class="form-group">
	<?php echo $form->labelEx($model,'TAHUN', array('class'=>'col-sm-2 control-label')); ?>
	<div class="col-sm-10">
		<?php echo $form->textField($model,'TAHUN',array('class' => 'form-control')); ?>
	</div>
</div>
	<div class="row">
		<div class="col-sm-12">
			<?php echo CHtml::submitButton('Search', array(
				'class'=>'btn btn-sm btn-primary'
			)); ?>
		</div>
	</div>
<?php $this->endWidget(); ?>
