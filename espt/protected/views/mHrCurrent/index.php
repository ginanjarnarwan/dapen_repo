<?php
/* @var $this MHrCurrentController */
/* @var $model MHrCurrent */

$this->breadcrumbs=array(
	'Mhr Currents'=>array('index'),
	'Manage',
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$('#mhr-current-grid').yiiGridView('update', {
data: $(this).serialize()
});
return false;
});
");
?>
<div class="row">
	<div class="col-md-12">
		<div class="box">
			<div class="box-header with-border">
				<h3 class="box-title">Manage Mhr Currents</h3>
				<div class="pull-right">
					<?php echo CHtml::link("Create", Yii::app()->controller->createUrl("create"), array("class" => "btn btn-sm btn-primary")); ?> 
				</div>
			</div>
			<div class="box-body">
				<div class="alert alert-danger text-center">
					<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?> 
				</div>
				<div class="search-form" style="display:none">
					<div class="box box-solid box-primary">
						<div class="box-body">
							<?php $this->renderPartial('_search',array(
								'model'=>$model,
							)); ?>
						</div>
					</div>
				</div><!-- search-form -->
				<?php $this->widget('zii.widgets.grid.CGridView', array(
				'id'=>'mhr-current-grid',
				'dataProvider'=>$model->search(),
				'filter'=>$model,
				'enableSorting' => false,
				'itemsCssClass' => 'table table-bordered table-striped',
				'pagerCssClass'=>'paging',
				'pager'=>array(
					'class'=>'CLinkPager',
					'header'=>'',
					'selectedPageCssClass'=>'active',
					'htmlOptions'=>array(
						'class'=>'pagination pagination-sm'
					)
				),
				'columns'=>array(
						'ID',
		'NIK',
		'NAMA',
		'PERSA',
		'SUBAREA',
		'BAND',
		/*
		'NAMA_POSISI',
		'NAMA_UNIT',
		'C_HOST',
		'KODE_LOKER',
		'KODE_DIVISI',
		'DIVISI',
		'OBJID_POSISI',
		'BULAN',
		'TAHUN',
		*/
				array(
				'class'=>'CButtonColumn',
				'buttons' => array(
				'view' => array(
				'label' => '<i class="fa fa-external-link"></i>',
				'imageUrl' => FALSE,
				'options' => array('class' => 'btn btn-primary btn-condensed btn-sm')
				),
				'update' => array(
				'label' => '<i class="fa fa-pencil"></i>',
				'imageUrl' => FALSE,
				'options' => array('class' => 'btn btn-warning btn-condensed btn-sm')
				),
				'delete' => array(
				'label' => '<i class="fa fa-trash"></i>',
				'imageUrl' => FALSE,
				'options' => array('class' => 'btn btn-danger btn-condensed btn-sm')
				),
				),
				'htmlOptions' => array('style' => 'width:130px;text-align:center;')
				),
				),
				)); ?>
			</div>
		</div>
	</div>
</div>
