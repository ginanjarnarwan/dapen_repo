<?php
/* @var $this MHrCurrentController */
/* @var $model MHrCurrent */

$this->breadcrumbs=array(
	'Mhr Currents'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List MHrCurrent', 'url'=>array('index')),
array('label'=>'Manage MHrCurrent', 'url'=>array('admin')),
);
?>
<div class="row">
	<div class="col-md-12">
		<div class="box box-info">
			<div class="box-header with-border">
				<h3 class="box-title">Create MHrCurrent</h3>
			</div>
			<?php $this->renderPartial('_form', array('model'=>$model)); ?>		</div>
	</div>
</div>
