<?php
/* @var $this MHrCurrentController */
/* @var $model MHrCurrent */

$this->breadcrumbs=array(
	'Mhr Currents'=>array('index'),
	$model->ID,
);

$this->menu=array(
	array('label'=>'List MHrCurrent', 'url'=>array('index')),
	array('label'=>'Create MHrCurrent', 'url'=>array('create')),
	array('label'=>'Update MHrCurrent', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete MHrCurrent', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MHrCurrent', 'url'=>array('admin')),
);
?>
<div class="row">
	<div class="col-sm-12">
		<div class="box box-info">
			<div class="box-header with-border">
				<h3 class="box-title">View MHrCurrent #<?php echo $model->ID; ?></h3>
			</div>
			<?php $this->widget('zii.widgets.CDetailView', array(
			'data'=>$model,
			'attributes'=>array(
					'ID',
		'NIK',
		'NAMA',
		'PERSA',
		'SUBAREA',
		'BAND',
		'NAMA_POSISI',
		'NAMA_UNIT',
		'C_HOST',
		'KODE_LOKER',
		'KODE_DIVISI',
		'DIVISI',
		'OBJID_POSISI',
		'BULAN',
		'TAHUN',
			),
			)); ?>
		</div>
	</div>
</div>
