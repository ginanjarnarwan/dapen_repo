<?php
/* @var $this MHrCurrentController */
/* @var $model MHrCurrent */
/* @var $form CActiveForm */
?>

<div class="box-body">

    <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'mhr-current-form',
	'enableAjaxValidation'=>false,
        'htmlOptions'=>array('class'=>'form-horizontal')
)); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

            <div class="form-group">
            <?php echo $form->labelEx($model,'NIK', array('class'=>'col-sm-2 control-label')); ?>
            <div class="col-md-10">
                <?php echo $form->textField($model, 'NIK', array('class' => 'form-control')); ?>
                <?php echo $form->error($model,'NIK', array('class'=>'alert alert-danger')); ?>
            </div>
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'NAMA', array('class'=>'col-sm-2 control-label')); ?>
            <div class="col-md-10">
                <?php echo $form->textField($model, 'NAMA', array('class' => 'form-control')); ?>
                <?php echo $form->error($model,'NAMA', array('class'=>'alert alert-danger')); ?>
            </div>
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'PERSA', array('class'=>'col-sm-2 control-label')); ?>
            <div class="col-md-10">
                <?php echo $form->textField($model, 'PERSA', array('class' => 'form-control')); ?>
                <?php echo $form->error($model,'PERSA', array('class'=>'alert alert-danger')); ?>
            </div>
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'SUBAREA', array('class'=>'col-sm-2 control-label')); ?>
            <div class="col-md-10">
                <?php echo $form->textField($model, 'SUBAREA', array('class' => 'form-control')); ?>
                <?php echo $form->error($model,'SUBAREA', array('class'=>'alert alert-danger')); ?>
            </div>
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'BAND', array('class'=>'col-sm-2 control-label')); ?>
            <div class="col-md-10">
                <?php echo $form->textField($model, 'BAND', array('class' => 'form-control')); ?>
                <?php echo $form->error($model,'BAND', array('class'=>'alert alert-danger')); ?>
            </div>
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'NAMA_POSISI', array('class'=>'col-sm-2 control-label')); ?>
            <div class="col-md-10">
                <?php echo $form->textField($model, 'NAMA_POSISI', array('class' => 'form-control')); ?>
                <?php echo $form->error($model,'NAMA_POSISI', array('class'=>'alert alert-danger')); ?>
            </div>
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'NAMA_UNIT', array('class'=>'col-sm-2 control-label')); ?>
            <div class="col-md-10">
                <?php echo $form->textField($model, 'NAMA_UNIT', array('class' => 'form-control')); ?>
                <?php echo $form->error($model,'NAMA_UNIT', array('class'=>'alert alert-danger')); ?>
            </div>
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'C_HOST', array('class'=>'col-sm-2 control-label')); ?>
            <div class="col-md-10">
                <?php echo $form->textField($model, 'C_HOST', array('class' => 'form-control')); ?>
                <?php echo $form->error($model,'C_HOST', array('class'=>'alert alert-danger')); ?>
            </div>
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'KODE_LOKER', array('class'=>'col-sm-2 control-label')); ?>
            <div class="col-md-10">
                <?php echo $form->textField($model, 'KODE_LOKER', array('class' => 'form-control')); ?>
                <?php echo $form->error($model,'KODE_LOKER', array('class'=>'alert alert-danger')); ?>
            </div>
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'KODE_DIVISI', array('class'=>'col-sm-2 control-label')); ?>
            <div class="col-md-10">
                <?php echo $form->textField($model, 'KODE_DIVISI', array('class' => 'form-control')); ?>
                <?php echo $form->error($model,'KODE_DIVISI', array('class'=>'alert alert-danger')); ?>
            </div>
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'DIVISI', array('class'=>'col-sm-2 control-label')); ?>
            <div class="col-md-10">
                <?php echo $form->textField($model, 'DIVISI', array('class' => 'form-control')); ?>
                <?php echo $form->error($model,'DIVISI', array('class'=>'alert alert-danger')); ?>
            </div>
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'OBJID_POSISI', array('class'=>'col-sm-2 control-label')); ?>
            <div class="col-md-10">
                <?php echo $form->textField($model, 'OBJID_POSISI', array('class' => 'form-control')); ?>
                <?php echo $form->error($model,'OBJID_POSISI', array('class'=>'alert alert-danger')); ?>
            </div>
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'BULAN', array('class'=>'col-sm-2 control-label')); ?>
            <div class="col-md-10">
                <?php echo $form->textField($model, 'BULAN', array('class' => 'form-control')); ?>
                <?php echo $form->error($model,'BULAN', array('class'=>'alert alert-danger')); ?>
            </div>
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'TAHUN', array('class'=>'col-sm-2 control-label')); ?>
            <div class="col-md-10">
                <?php echo $form->textField($model, 'TAHUN', array('class' => 'form-control')); ?>
                <?php echo $form->error($model,'TAHUN', array('class'=>'alert alert-danger')); ?>
            </div>
        </div>

        </div>

<div class="box-footer">
    <?php echo CHtml::link('Cancel', Yii::app()->controller->createUrl('index'), array('class' => 'btn btn-sm btn-default')); ?> 
    <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-sm btn-info pull-right')); ?> 
</div>

<?php $this->endWidget(); ?>
