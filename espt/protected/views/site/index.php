<!-- First Row -->

<!-- Second Row -->
<div class="row">
    <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1 style="margin-left:16px;">Home</h1>
                </div>
            </div>
            <div class="col-sm-6 hidden-xs">
                <div class="header-section">
                    <ul class="breadcrumb breadcrumb-top">
                        <li class="active"><a href="<?php echo Yii::app()->createUrl('site/index')?>">/Home</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <h2 class="text-warning" style="margin-left:16px;">List SPT</h2>
    <?php 
      $this->renderPartial('_content',array(
        'data'=>$data,
        'stat_k'=>$stat_k,
        'stat_tk'=>$stat_tk,
        'res2'=>$res2,
        'res3'=>$res3,
        'res4'=>$res4,
        'bukti_potong'=>$bukti_potong,
        'datafinal'=>$datafinal,
        'datanonfinal'=>$datanonfinal,
        'data1770s'=>$data1770s,
        'datassp'=>$datassp,
        'efin'=>$efin,
        'resume'=>$resume
      ));
    ?>
</div>
<div id="dlg" class="modal fade" role="dialog"></div>
<script>
$('.btn-print').click(function(e){
    e.preventDefault();
    var link = $(this).attr('href');
    $("#dlg").modal('show');
    $("#dlg").load(link, function (response, status, xhr) {
        if (status == "error") {
            $("#dlg").html(response);
        }
    });
    event.preventDefault();
});
</script>