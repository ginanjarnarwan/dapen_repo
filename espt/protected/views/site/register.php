<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0, user-scalable=no">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>E-SPT</title>
  <!-- CORE CSS-->
  
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.1/css/materialize.min.css">

<style type="text/css">
html,
body {
    height: 100%;
}
html {
    display: table;
    margin: auto;
}
body {
    display: table-cell;
    vertical-align: middle;
}
.margin {
  margin: 0 !important;
}
</style>
  
</head>

<body class="blue">

  <div id="login-page" class="row">
    <div class="col s12 z-depth-6 card-panel">
      <?php $form=$this->beginWidget('CActiveForm', array(
            'id'=>'userapp-form',
            'enableAjaxValidation'=>true,
            'htmlOptions'=>array('class'=>'form-horizontal'),
            'clientOptions'=>array(
              'validateOnSubmit'=>true,
            ),
        )); ?>
        <div class="row">
          <div class="input-field col s12 center">
            <h4>Register E-SPT</h4>
          </div>
        </div>
        <div class="row margin">
          <div class="input-field col s12">
            <i class="mdi-social-person-outline prefix"></i>
            <?php echo $form->textField($model, 'telkomnik', array('class' => 'validate')); ?>
            <?php echo $form->labelEx($model,'telkomnik', array('class'=>'center-align')); ?>
            <?php echo $form->error($model,'telkomnik', array('class'=>'mdi-alert-error blue-text', 'style'=>'margin-left:3rem')); ?>
            <!-- <input id="username" type="text" class="validate">
            <label for="username" class="center-align">NIK</label> -->
          </div>
        </div>
        <div class="row margin">
          <div class="input-field">
            <i class="mdi-action-event prefix"></i>
            <?php echo $form->textField($model, 'tanggal_lahir', array('class' => 'validate datepicker')); ?>
            <?php echo $form->labelEx($model,'tanggal_lahir', array('class'=>'center-align active')); ?>
            <?php echo $form->error($model,'tanggal_lahir', array('class'=>'mdi-alert-error blue-text', 'style'=>'margin-left:3rem')); ?>
          </div>
        </div>
        <div class="row margin">
          <div class="input-field col s12">
            <i class="mdi-communication-email prefix"></i>
            <?php echo $form->textField($model, 'email', array('class' => 'validate')); ?>
            <?php echo $form->labelEx($model,'email', array('class'=>'center-align')); ?>
            <?php echo $form->error($model,'email', array('class'=>'mdi-alert-error blue-text', 'style'=>'margin-left:3rem')); ?>
          </div>
        </div>
        <div class="row margin">
          <div class="input-field col s12">
            <i class="mdi-communication-call prefix"></i>
            <?php echo $form->textField($model, 'nomor_handphone', array('class' => 'validate')); ?>
            <?php echo $form->labelEx($model,'nomor_handphone', array('class'=>'center-align')); ?>
            <?php echo $form->error($model,'nomor_handphone', array('class'=>'mdi-alert-error blue-text', 'style'=>'margin-left:3rem')); ?>
          </div>
        </div>
        <div class="row margin">
          <div class="input-field col s12">
            <i class="mdi-action-lock-outline prefix"></i>
            <?php echo $form->passwordField($model, 'password', array('class' => 'validate')); ?>
            <?php echo $form->labelEx($model,'password', array('class'=>'center-align')); ?>
            <?php echo $form->error($model,'password', array('class'=>'mdi-alert-error blue-text', 'style'=>'margin-left:3rem')); ?>
          </div>
        </div>
        <div class="row margin">
          <div class="input-field col s12">
          

            <i class="mdi-action-lock prefix"></i>
            <?php echo $form->passwordField($model, 'password2', array('class' => 'validate')); ?>
            <?php echo $form->labelEx($model,'password2', array('class'=>'center-align')); ?>
            <?php echo $form->error($model,'password2', array('class'=>'mdi-alert-error blue-text', 'style'=>'margin-left:3rem')); ?>
          </div>
        </div>
        <div class="row">
          <div class="input-field col s12">
            <button type="submit" class="btn waves-effect waves-light col s12">Register Now</button>
          </div>
          <div class="input-field col s12">
            <p class="margin center medium-small sign-up">Already have an account? <a href="<?php echo Yii::app()->createUrl('site/login') ?>">Login</a></p>
          </div>
        </div>
        <?php $this->endWidget(); ?>
    </div>
  </div>


  <!-- jQuery Library -->
 <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
  <!--materialize js-->
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.1/js/materialize.min.js"></script>
  <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl . '/js/jquery.mask.js' ?>"></script>



  <script>
    $(document).ready(function(){
      $('#Userapp_nomor_handphone').mask('0000000000000');
    });

    $('.datepicker').pickadate({
    selectMonths: true, // Creates a dropdown to control month
    selectYears: 100, // Creates a dropdown of 15 years to control year,
    format:'mm/dd/yyyy',
    max: new Date(2000,7,14),
    clear: 'Clear',
    close: 'Ok',
    closeOnSelect: false // Close upon selecting a date,
  });
  </script>
</body>

</html>