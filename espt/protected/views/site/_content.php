<div class="col-sm-12 col-lg-12">
        <div class="block full">
            <!-- Block Tabs Title -->
            <div class="block-title">
                <ul class="nav nav-tabs" data-toggle="tabs">
                    <!-- <li class="active"><a href="#block-tabs-1770s">1770s</a></li> -->
                    <li class="active"><a href="#block-tabs-a1">1721-A1</a></li>
                    <li class=""><a href="#block-tabs-vi">Bukti Potong Tidak Final</a></li>
                    <li class=""><a href="#block-tabs-vii">Bukti Potong Final</a></li>
                    <li class=""><a href="#block-tabs-viii">SSP</a></li>
                </ul>
            </div>
            <!-- END Block Tabs Title -->

            <!-- Tabs Content (hide dulu menu belum siap) -->
            <div class="tab-content">
                <!-- <div class="tab-pane active" id="block-tabs-1770s">
                    <div class="widget">
                        <div class="widget-content border-bottom">
                            <h4><i class="fa fa-file-text-o"></i> Form Lampiran (SPT 1770-S)</h4>
                        </div>
                        <div class="widget-content widget-content-full">
                            <table class="table table-striped table-borderless table-vcenter">
                              <thead>
                             
                                <tr>
                                  <th>NIK</th>
                                  <th>Nama</th>
                                  <th>Tahun</th>
                                  <th>EFIN</th>
                                  <th style="text-align: right;">Jumlah Neto</th>
                                  <th style="text-align: right;">Jumlah PPh</th>
                                  <th></th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php for ($i=0; $i < count($data1770s) ; $i++) { ?>
                                <tr>
                                  <td><?php echo ucfirst($data1770s[$i]['n_nik']); ?></td>
                                  <td><?php echo strtoupper($data1770s[$i]['v_nama']); ?></td>
                                  <td><?php echo $data1770s[$i]['n_tahun_pajak']; ?></td>
                                  <td><?php echo $efin;?></td>
                                  <td align="right"><?php echo number_format($data1770s[$i]['n_jumlah_neto'],0,",","."); ?></td>
                                  <td align="right"><?php echo number_format($data1770s[$i]['n_jumlah_pph_terutang'],0,",","."); ?></td>
                                  
                                  <td><a href="<?php echo Yii::app()->createUrl('/cetakform/popspt1770s',array(
                                        'tahun'=>$data1770s[$i]['n_tahun_pajak'],
                                        'nik'=>$data1770s[$i]['n_nik'],
                                        )); ?>" class="btn btn-xs btn-outline btn-info btn-print"><i class="fa fa-paper-plane"></i></a></td>
                                </tr>
                                <?php } ?>
                                
                              </tbody>
                            </table>
                        </div>
                        <hr>
                        <div class="widget">
                            <div class="widget-content border-bottom">
                                <h4><i class="fa fa-file-text-o"></i> Resume Pajak untuk E-Filling</h4>
                            </div>
                            <div class="widget-content border-bottom">
                                <table id="firstname" border="0" style=" font-size: 20; border border-spacing: 5px; width:90%;padding: 1%">
                                <tr style="padding: 5px;">
                                    <th>No.</th>
                                    <th>Item</th>
                                    <th>Detail</th>
                                </tr>   
                                 <tr>
                                    <td class="index">1.</td>
                                    <td>Nama  </td>
                                    <td class="value">: &nbsp<?php echo  $resume['v_nama']; ?></td>
                                </tr>
                                <tr>
                                    <td class="index">2.</td>
                                    <td>NPWP Pegawai  </td>
                                    <td class="value">: &nbsp<?php echo  $resume['NPWP_PEGAWAI']; ?></td>
                                </tr>
                                <tr>
                                    <td class="index">3.</td>
                                    <td>NPWP Telkom  </td>
                                    <td class="value">: &nbsp<?php echo  $resume['NPWP_Telkom']; ?></td>
                                </tr>
                                <tr>
                                    <td class="index">4.</td>
                                    <td>NPWP Dapen  </td>
                                    <td class="value">: &nbsp<?php echo  $resume['NPWP_DAPEN']; ?></td>
                                </tr>
                                <tr>
                                    <td class="index">5.</td>
                                    <td>Status SPT  </td>
                                    <td class="value">: &nbsp<?php echo  $resume['status_spt']; ?></td>
                                </tr>
                                <tr>
                                    <td class="index">6.</td>
                                    <td>EFIN</td>
                                    <td class="value">:  &nbsp <?php echo  $res3[0][0] ?></td>
                                </tr>
                                </table>
                            </div>
                        </div>
                    </div>
                </div> -->

                <div class="tab-pane active" id="block-tabs-a1">
                    <div class="widget">
                        <div class="widget-content border-bottom">
                           <!--  <span class="pull-right text-muted">2013</span> -->
                            <h4><i class="fa fa-file-text-o"></i> Form Lampiran (SPT 1721-A1)</h4>
                        </div>
                        <!-- <div class="widget-content border-bottom themed-background-muted">

                        </div> -->
                        <div class="widget-content widget-content-full">
                            <table class="table table-striped table-borderless table-vcenter">
                              <thead>
                             
                                <tr>
                                  <th>NIK</th>
                                  <th>Tahun</th>
                                  <th>Bulan Akhir</th>
                                  <th>Kode KPP</th>
                                  <th>Nama KPP</th>
                                  <th>Nama Pemotong</th>
                                  <th></th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php for ($i=0; $i < count($data) ; $i++) { ?>
                                <tr>
                                  <td><?php echo ucfirst($data[$i]['N_NIK']); ?></td>
                                  <td><?php echo $data[$i]['N_TAHUN']; ?></td>
                                  <td><?php echo ucwords (strtolower($data[$i]['V_BULANAKHIR'])); ?></td>
                                  <td><?php echo $data[$i]['C_KODE_KPP']; ?></td>
                                  <td><?php echo $data[$i]['V_NAMA_KPP']; ?></td>
                                  <td><?php echo $data[$i]['V_NAMA_PEMOTONG']; ?></td>
                                  
                                  <td><a href="<?php echo Yii::app()->createUrl('/cetakform/popsptsprint',array(
                                        'tahun'=>$data[$i]['N_TAHUN'],
                                        'nik'=>$data[$i]['N_NIK'],
                                        'bulan'=>$data[$i]['N_BULANAWAL'],
                                        'bulanakhir'=>$data[$i]['N_BULANAKHIR'],
                                        'kode'=>$data[$i]['C_KODE_KPP'],
                                        'namapemotong'=>$data[$i]['V_NAMA_PEMOTONG'],
                                        )); ?>" class="btn btn-xs btn-outline btn-info btn-print"><i class="fa fa-paper-plane"></i></a></td>
                                </tr>
                                <?php } ?>
                                
                              </tbody>
                            </table>
                        </div>
                    </div>

                        <hr>
                        <div class="widget">
                            <div class="widget-content border-bottom">
                                <h4><i class="fa fa-file-text-o"></i> Resume Pajak untuk E-Filling</h4>
                            </div>
                            <div class="widget-content border-bottom">
                                <table id="firstname" border="0" style=" font-size: 20; border border-spacing: 5px; width:90%;padding: 1%">
                                <tr style="padding: 5px;">
                                    <th>No.</th>
                                    <th>Item</th>
                                    <th>Detail</th>
                                </tr>   
                                 <tr>
                                    <td class="index">1.</td>
                                    <td>Nama  </td>
                                    <td class="value">: &nbsp<?php echo  $resume['V_NAMA_KARYAWAN']; ?></td>
                                </tr>
                                <tr>
                                    <td class="index">2.</td>
                                    <td>NPWP Pegawai  </td>
                                    <td class="value">: &nbsp<?php echo  $resume['V_NPWP_PRIBADI']; ?></td>
                                </tr>
                                <tr>
                                    <td class="index">3.</td>
                                    <td>NPWP Telkom  </td>
                                    <td class="value">: &nbsp<?php echo  $resume['V_NPWP_KANTOR']; ?></td>
                                </tr>
                                <tr>
                                    <td class="index">4.</td>
                                    <td>EFIN</td>
                                    <td class="value">:  &nbsp <?php echo  $res3[0][0] ?></td>
                                </tr>
                                </table>
                            </div>
                        </div>
                </div>

                <div class="tab-pane" id="block-tabs-vi">
                    <div class="widget">
                        <div class="widget-content border-bottom">
                           <!--  <span class="pull-right text-muted">2013</span> -->
                            <h4><i class="fa fa-file-text-o"></i> Form Lampiran Bukti Potong Tidak Final (SPT 1721-VI)</h4>
                        </div>
                        <!-- <div class="widget-content border-bottom themed-background-muted">

                        </div> -->
                        <div class="widget-content widget-content-full">
                            <table class="table table-striped table-borderless table-vcenter">
                              <thead>
                             
                                <tr>
                                  <th>NIK</th>
                                  <th>Nama</th>
                                  <th>Tahun</th>
                                  <th>Nomor Bukti Potong</th>
                                  <th></th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php 
                                for ($i=0; $i < count($datanonfinal) ; $i++) {
                                ?>
                                    
                                <tr>
                                  <td><?php echo $datanonfinal[$i]['nik']; ?></td>
                                  <td><?php echo $datanonfinal[$i]['nama']; ?></td>
                                  <td><?php echo $datanonfinal[$i]['tahun_pajak']; ?></td>
                                  <td><?php echo $datanonfinal[$i]['nomor_bukti_potong']; ?></td>
                                  
                                  <td><a href="<?php echo Yii::app()->createUrl('/cetakform/popspttidakfinal',array(
                                        'tahun'=>$datanonfinal[$i]['tahun_pajak'],
                                        'nobukti'=>$datanonfinal[$i]['nomor_bukti_potong'],
                                        'npwp_pemotong'=>$datanonfinal[$i]['npwp_pemotong'],
                                        )); ?>" class="btn btn-xs btn-outline btn-info btn-print"><i class="fa fa-paper-plane"></i></a></td>
                                </tr>
                                <?php } ?>
                                
                                <!-- endBlock(kel) -->
                              </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="block-tabs-vii">
                    <div class="widget">
                        <div class="widget-content border-bottom">
                           <!--  <span class="pull-right text-muted">2013</span> -->
                            <h4><i class="fa fa-file-text-o"></i> Form Lampiran Bukti Potong Final (SPT 1721-VII)</h4>
                        </div>
                        <!-- <div class="widget-content border-bottom themed-background-muted">

                        </div> -->
                        <div class="widget-content widget-content-full">
                            <table class="table table-striped table-borderless table-vcenter">
                              <thead>
                             
                                <tr>
                                  <th>NIK</th>
                                  <th>Nama</th>
                                  <th>Tahun</th>
                                  <th>Nomor Bukti Potong</th>
                                  <th>Nama Pemotong</th>
                                  <th></th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php 
                                for ($i=0; $i < count($datafinal) ; $i++) {
                                ?>
                                    
                                <tr>
                                  <td><?php echo $datafinal[$i]['nik']; ?></td>
                                  <td><?php echo $datafinal[$i]['nama']; ?></td>
                                  <td><?php echo $datafinal[$i]['tahun_pajak']; ?></td>
                                  <td><?php echo $datafinal[$i]['nomor_bukti_potong']; ?></td>
                                  <td><?php echo $datafinal[$i]['nama_pemotong']; ?></td>
                                  
                                  <td><a href="<?php echo Yii::app()->createUrl('/cetakform/popsptfinal',array(
                                        'tahun'=>$datafinal[$i]['tahun_pajak'],
                                        'nobukti'=>$datafinal[$i]['nomor_bukti_potong'],
                                        'npwp_pemotong'=>$datafinal[$i]['npwp_pemotong'],
                                        )); ?>" class="btn btn-xs btn-outline btn-info btn-print"><i class="fa fa-paper-plane"></i></a></td>
                                </tr>
                                <?php } ?>
                                
                                <!-- endBlock(kel) -->
                              </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="tab-pane" id="block-tabs-viii">
                    <div class="widget">
                        <div class="widget-content border-bottom">
                           <!--  <span class="pull-right text-muted">2013</span> -->
                            <h4><i class="fa fa-file-text-o"></i> Surat Setoran Pajak (SSP)</h4>
                        </div>
                        <!-- <div class="widget-content border-bottom themed-background-muted">

                        </div> -->
                        <div class="widget-content widget-content-full">
                            <table class="table table-striped table-borderless table-vcenter">
                              <thead>
                             
                                <tr>
                                  <th>NIK</th>
                                  <th>NPWP</th>
                                  <th>Tahun</th>
                                  <th>NTPN</th>
                                  <th>Jumlah</th>
                                  <th></th>
                                </tr>
                              </thead>
                              <tbody>
                                <?php 
                                for ($i=0; $i < count($datassp) ; $i++) {
                                ?>
                                    
                                <tr>
                                  <td><?php echo $datassp[$i]['n_nik']; ?></td>
                                  <td><?php echo $datassp[$i]['n_npwp']; ?></td>
                                  <td><?php echo $datassp[$i]['n_tahun_pajak']; ?></td>
                                  <td><?php echo $datassp[$i]['ntpn']; ?></td>
                                  <td><?php echo $datassp[$i]['jumlah']; ?></td>
                                  
                                  <td><a href="<?php echo Yii::app()->createUrl('/cetakform/downloadssp',array(
                                        'nik'=>$datassp[$i]['n_nik'],
                                        'npwp'=>$datassp[$i]['n_npwp'],
                                        'tahun'=>$datassp[$i]['n_tahun_pajak'],
                                        'id'=>$datassp[$i]['id'],
                                        )); ?>" class="btn btn-xs btn-outline btn-info"><i class="fa fa-download"></i></a></td>
                                </tr>
                                <?php } ?>
                                
                                <!-- endBlock(kel) -->
                              </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END Tabs Content -->
        </div>
        <!-- Chart Widget -->
    </div>