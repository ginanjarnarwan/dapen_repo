<?php
$numbers = range(1, 8);
shuffle($numbers);
$status = MyApp::statusProses($data->TAHUN,$data->BULAN);
// var_dump($numbers);exit;
$blmmap = "SELECT 
			cr.NIK,cr.NAMA,cr.OBJID_POSISI,cr.KODE_DIVISI,cr.DIVISI,cr.NAMA_POSISI,cr.NAMA_UNIT,'N#A' AS FINAL_SEGMENTASI 
			FROM 
			t_payslip py
			LEFT JOIN m_hr_current cr ON cr.BULAN = py.BULAN AND cr.TAHUN = py.TAHUN AND cr.NIK = py.NIK 
			LEFT JOIN m_mapping_atk atk ON atk.NIK = py.NIK AND atk.TAHUN = py.TAHUN AND atk.BULAN = py.BULAN
			where cr.TAHUN = :tahun AND cr.BULAN = :bulan AND (atk.NIK = '' || atk.NIK is null) AND cr.KODE_DIVISI IN ('DIV-TR1','DIV-TR2','DIV-TR3','DIV-TR4','DIV-TR5','DIV-TR6','DIV-TR7');";
$sqlmap = Yii::app()->db->createCommand($blmmap);
$sqlmap->bindParam(":bulan",$data->BULAN);
$sqlmap->bindParam(":tahun",$data->TAHUN);
$map = $sqlmap->queryAll();

$blmdv = "SELECT cr.NIK,cr.NAMA,cr.OBJID_POSISI,cr.KODE_DIVISI,cr.DIVISI,cr.NAMA_POSISI,cr.NAMA_UNIT FROM m_hr_current cr 
					WHERE (cr.KODE_DIVISI = '' OR cr.KODE_DIVISI IS NULL) AND cr.BULAN = :bulan AND cr.TAHUN = :tahun";
$sqldv = Yii::app()->db->createCommand($blmdv);
$sqldv->bindParam(":bulan",$data->BULAN);
$sqldv->bindParam(":tahun",$data->TAHUN);
$dv = $sqldv->queryAll();
$badges = MyApp::stsBadges($data->TAHUN,$data->BULAN);
?>
<style type="text/css">
	.print td{
		border: 1px solid #000000;
	}
</style>
<script>
	$(document).ready(function() {
	  $("#export_blmmap_<?php echo $data->BULAN.'_'.$data->TAHUN;?>").click(function(e) {
	    e.preventDefault();

	    //getting data from our table
	    var data_type = 'data:application/vnd.ms-excel';
	    var table_div = document.getElementById('isi_blmmap_<?php echo $data->BULAN.'_'.$data->TAHUN;?>');
	    var table_html = table_div.outerHTML.replace(/ /g, '%20');

	    var a = document.createElement('a');
	    a.href = data_type + ', ' + table_html;
	    a.download = 'NIK TREG FINAL SEGEMENTASI NA PERIODE <?php echo strtoupper(MyApp::convertBulan($data->BULAN))." ".$data->TAHUN."-"?>' + Math.floor((Math.random() * 9999999) + 1000000) + '.xls';
	    a.click();
	  });
	  $("#export_blmdv_<?php echo $data->BULAN.'_'.$data->TAHUN;?>").click(function(e) {
	    e.preventDefault();

	    //getting data from our table
	    var data_type = 'data:application/vnd.ms-excel';
	    var table_div = document.getElementById('isi_blmdv_<?php echo $data->BULAN.'_'.$data->TAHUN;?>');
	    var table_html = table_div.outerHTML.replace(/ /g, '%20');

	    var a = document.createElement('a');
	    a.href = data_type + ', ' + table_html;
	    a.download = 'NIK NON TREG KODE DIVISI KOSONG PERIODE <?php echo strtoupper(MyApp::convertBulan($data->BULAN))." ".$data->TAHUN."-"?>' + Math.floor((Math.random() * 9999999) + 1000000) + '.xls';
	    a.click();
	  });
	});
</script>
<a href="#" id=""></a>
<div class="col-sm-6 col-md-4">
	<div class="panel panel-info <?php //echo Myapp::kelasBackground($numbers);?>">
		<div class="panel-heading">
			<h4 class="panel-title"><?php echo MyApp::convertbulan($data->BULAN).' - '.$data->TAHUN?> <?php echo $status[1];?></h4>
		</div>
		<div class="panel-body">
			<i class="fa fa-money"></i> Real SAP : <?php echo MyApp::buatrp($data->total);?>
			<ul class="list-group">
				<li class="list-group-item"><?php echo $badges['lbsap']; ?>Input Real SAP</li>
				<li class="list-group-item"><?php echo $badges['lbpayslip']; ?>Upload Payslip</li>
				<li class="list-group-item"><?php echo $badges['lbhrcur']; ?>Upload HR Current</li>
				<li class="list-group-item"><?php echo $badges['lbgen']; ?>Generate Report</li>
			</ul>
		</div>
		<div class="panel-footer">
			<center>
				<?php if ($status[0]){?>
				<?php if(Yii::app()->user->roles == 'Admin'){?>
				<a class="btn btn-sm btn-primary" disabled><i class="fa fa-cog"></i> Generate</a>
				<?php } ?>
				<a class="btn btn-sm btn-success" href="<?php echo Yii::app()->controller->createUrl('reportglobal',array('tahun'=>$data->TAHUN,'bulan'=>$data->BULAN));?>"><i class="fa fa-paper-plane"></i> Report</a>
				<?php if(Yii::app()->user->roles == 'Admin'){?>
				<a class="btn btn-sm btn-danger del" href="<?php echo Yii::app()->controller->createUrl('deletecost',array('tahun'=>$data->TAHUN,'bulan'=>$data->BULAN));?>"><i class="fa fa-remove"></i> Delete</a>
				<?php } ?>
				<?php } else{ ?>
				<?php if(Yii::app()->user->roles == 'Admin'){?>
				<a class="subm btn btn-sm btn-primary" href="<?php echo Yii::app()->controller->createUrl('proses',array('tahun'=>$data->TAHUN,'bulan'=>$data->BULAN));?>"><i class="fa fa-cog"></i> Generate</a>
				<?php } ?>
				<a class="btn btn-sm btn-success" disabled><i class="fa fa-paper-plane"></i> Report</a>
				<?php if(Yii::app()->user->roles == 'Admin'){?>
				<a class="btn btn-sm btn-danger del" href="<?php echo Yii::app()->controller->createUrl('deletecost',array('tahun'=>$data->TAHUN,'bulan'=>$data->BULAN));?>"><i class="fa fa-remove"></i> Delete</a>
				<?php } ?>
				<?php } ?>
				<!-- <button class="btn btn-sm btn-danger">Button</button> -->
			</center>
		</div>
	</div>
</div>
<div class="col-md-12" style="display:none;">
	<?php
	if (!empty($map)) {
	?>	
		<table class="print" id="isi_blmmap_<?php echo $data->BULAN.'_'.$data->TAHUN;?>">
			<tr>
				<td><b>NIK</b></td>
				<td><b>Nama</b></td>
				<td><b>Objid Pos</b></td>
				<td><b>Kode Divisi</b></td>
				<td><b>Divisi</b></td>
				<td><b>Posisi</b></td>
				<td><b>Unit</b></td>
				<td><b>Final Segmentasi</b></td>
			</tr>
	<?php
		foreach ($map as $key => $value) {
	?>
			<tr>
				<td><?php echo $value['NIK'];?></td>
				<td><?php echo $value['NAMA'];?></td>
				<td><?php echo $value['OBJID_POSISI'];?></td>
				<td><?php echo $value['KODE_DIVISI'];?></td>
				<td><?php echo $value['DIVISI'];?></td>
				<td><?php echo $value['NAMA_POSISI'];?></td>
				<td><?php echo $value['NAMA_UNIT'];?></td>
				<td><?php echo $value['FINAL_SEGMENTASI'];?></td>
			</tr>
	<?php
		}
	?>
		</table>
	<?php
	}
	?>

	<?php
	if (!empty($dv)) {
	?>	
		<table class="print" id="isi_blmdv_<?php echo $data->BULAN.'_'.$data->TAHUN;?>">
			<tr>
				<td><b>NIK</b></td>
				<td><b>Nama</b></td>
				<td><b>Objid Pos</b></td>
				<td><b>Kode Divisi</b></td>
				<td><b>Divisi</b></td>
				<td><b>Posisi</b></td>
				<td><b>Unit</b></td>
			</tr>
	<?php
		foreach ($dv as $key => $value) {
	?>
			<tr>
				<td><?php echo $value['NIK'];?></td>
				<td><?php echo $value['NAMA'];?></td>
				<td><?php echo $value['OBJID_POSISI'];?></td>
				<td><?php echo $value['KODE_DIVISI'];?></td>
				<td><?php echo $value['DIVISI'];?></td>
				<td><?php echo $value['NAMA_POSISI'];?></td>
				<td><?php echo $value['NAMA_UNIT'];?></td>
			</tr>
	<?php
		}
	?>
		</table>
	<?php
	}
	?>
</div>