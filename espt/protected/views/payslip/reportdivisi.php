<?php 
//var_dump($tot1,$tot2);exit();
?>
<style>
	#parent {
		height: 500px;
	}
	
	#fixTable {
		width: 1800px !important;
	}
</style>
<script>
	$(document).ready(function() {
		$("#fixTable").tableHeadFixer({"left" : 3}); 
	});
	$(document).ready(function() {
	  $("#btnExport").click(function(e) {
	    e.preventDefault();

	    //getting data from our table
	    var data_type = 'data:application/vnd.ms-excel';
	    var table_div = document.getElementById('isi');
	    var table_html = table_div.outerHTML.replace(/ /g, '%20');

	    var a = document.createElement('a');
	    a.href = data_type + ', ' + table_html;
	    a.download = 'REALISASI BEBAN PERSONNEL PER DIVISI PERIODE <?php echo strtoupper(MyApp::convertBulan($bulan))." ".$tahun."-"?>' + Math.floor((Math.random() * 9999999) + 1000000) + '.xls';
	    a.click();
	  });
	});
</script>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/dist/css/bots/bootstrap.css');?>
<div class="row">
<div class="col-md-12">
	<h4 class="m-b-lg">Report <?php echo MyApp::convertBulan($bulan).' '.$tahun?></h4>
</div>
<div class="col-md-12">
	<div class="widget">
		<div class="m-b-lg nav-tabs-horizontal">
			<!-- tabs list -->
			<ul class="nav nav-tabs" role="tablist">
				<li role="presentation"><a href="<?php echo Yii::app()->controller->createUrl('reportglobal',array('tahun'=>$tahun,'bulan'=>$bulan));?>" aria-controls="tab-2" role="tab" >TREG/NON TREG</a></li>
				<li role="presentation" class="active"><a href="#" aria-controls="tab-1" role="tab" >Divisi</a></li>
				<li role="presentation"><a href="<?php echo Yii::app()->controller->createUrl('report',array('tahun'=>$tahun,'bulan'=>$bulan));?>" aria-controls="tab-3" role="tab" >Witel</a></li>
			</ul><!-- .nav-tabs -->
			<ul class="nav nav-tabs" role="tablist">
				<li role="presentation" class="active"><a href="#" aria-controls="tab-2" role="tab"><?php echo MyApp::convertBulan($bulan); ?></a></li>
				<li role="presentation"><a href="<?php echo Yii::app()->controller->createUrl('reportdivisinow',array('tahun'=>$tahun,'bulan'=>$bulan));?>" aria-controls="tab-1" role="tab" >Sampai Dengan <?php echo MyApp::convertBulan($bulan); ?></a></li>
			</ul>

			<!-- Tab panes -->
			<div class="tab-content p-md">
				<div role="tabpanel" class="tab-pane in active fade" id="isi">
					<h4 class="m-b-md">REALISASI BEBAN PERSONNEL PER DIVISI PERIODE <?php echo strtoupper(MyApp::convertBulan($bulan)).' '.$tahun?></h4>
					<div style="text-align:right;margin-bottom:10px;">
						<a href="#" class="btn btn-info" id="btnExport"><i class="fa fa-file-excel-o"></i> Export Excel</a>
					</div>
					<div id="parent">
						<!-- <div class="table-responsive"> -->
							<table class="tabel" id="fixTable">
								<thead>
									<tr>
										<th style="background:#6895a0; color:#ffffff">No</th>
										<th style="background:#6895a0; color:#ffffff">Kode Divisi</th>
										<th style="background:#6895a0; color:#ffffff" width="30px">Nama Divisi</th>
										<th style="background:#6895a0; color:#ffffff" width="20px">CFU FU</th>
										<th style="background:#6895a0; color:#ffffff;text-align:right;" width="20px">Jumlah Orang</th>
										<th style="background:#6895a0; color:#ffffff;text-align:right;">Prosentase</th>
										<th style="background:#6895a0; color:#ffffff;text-align:right;">Realisasi <?php echo MyApp::convertBulan($bulan);?></th>
										<!-- <th style="background:#6895a0; color:#ffffff">s/d <?php echo MyApp::convertBulan($bulan);?></th> -->
										<th style="background:#6895a0; color:#ffffff;text-align:right;">Consumer</th>
										<th style="background:#6895a0; color:#ffffff;text-align:right;">Enterprise</th>
										<th style="background:#6895a0; color:#ffffff;text-align:right;">WIB</th>
										<th style="background:#6895a0; color:#ffffff;text-align:right;">Digital</th>
										<th style="background:#6895a0; color:#ffffff;text-align:right;">Mobile</th>
										<th style="background:#6895a0; color:#ffffff;text-align:right;">Total</th>
									</tr>
								</thead>
								<tbody>
									<?php
									$no = 1;
									foreach ($data as $key => $value) {
									?>
									<tr>
										<td style="background:#ffca70"><?php echo $no;?></td>
										<td style="background:#ffca70"><?php echo $value['C_KODE_DIVISI'];?></td>
										<td style="background:#ffca70"><?php echo $value['divisi'];?></td>
										<td style="background:#ffca70"><?php echo $value['CFU_FU'];?></td>
										<td style="background:#ffca70;text-align:right;"><?php echo $value['JML_ORG'];?></td>
										<td style="background:#ffca70;text-align:right;"><?php echo round(($value['PROSENTASE'] * 100),2).'%';?></td>
										<td style="background:#ffca70;text-align:right;"><?php echo MyApp::buatuang($value['PERSONEL_COST']);?></td>
										<!-- <td style="background:#ffca70;text-align:right;"><?php echo MyApp::buatuang($value['PERSONEL_COST_SD_NOW']);?></td> -->
										<td style="background:#b2efff;text-align:right;"><?php echo MyApp::buatuang($value['CONSUMER']);?></td>
										<td style="background:#b2efff;text-align:right;"><?php echo MyApp::buatuang($value['ENTERPRISE']);?></td>
										<td style="background:#b2efff;text-align:right;"><?php echo MyApp::buatuang($value['WIB']);?></td>
										<td style="background:#b2efff;text-align:right;"><?php echo MyApp::buatuang($value['DIGITAL']);?></td>
										<td style="background:#b2efff;text-align:right;"><?php echo MyApp::buatuang($value['MOBILE']);?></td>
										<td style="background:#b2efff;text-align:right;"><?php echo MyApp::buatuang($value['TOTAL']);?></td>
									</tr>
									<?php
									$no++;
									}
									foreach ($data2 as $key => $value) {
									?>
									<tr>
										<td style="background:#ffca70"><?php echo $no;?></td>
										<td style="background:#ffca70"><?php echo $value['KODE_DIVISI'];?></td>
										<td style="background:#ffca70"><?php echo $value['V_SHORT_DIVISI'];?></td>
										<td style="background:#ffca70"><?php echo '';?></td>
										<td style="background:#ffca70;text-align:right;"><?php echo $value['JML_ORG'];?></td>
										<td style="background:#ffca70;text-align:right;"><?php echo round(($value['PROSENTASE'] * 100),2).'%';?></td>
										<td style="background:#ffca70;text-align:right;"><?php echo MyApp::buatuang($value['PERSONEL_COST_DIVISI']);?></td>
										<!-- <td style="background:#ffca70;text-align:right;"><?php echo MyApp::buatuang($value['PERSONEL_COST_SD_NOW']);?></td> -->
										<td style="background:#b2efff;text-align:right;"><?php echo MyApp::buatuang($value['CONSUMER']);?></td>
										<td style="background:#b2efff;text-align:right;"><?php echo MyApp::buatuang($value['ENTERPRISE']);?></td>
										<td style="background:#b2efff;text-align:right;"><?php echo MyApp::buatuang($value['WIB']);?></td>
										<td style="background:#b2efff;text-align:right;"><?php echo MyApp::buatuang($value['DIGITAL']);?></td>
										<td style="background:#b2efff;text-align:right;"><?php echo MyApp::buatuang($value['MOBILE']);?></td>
										<td style="background:#b2efff;text-align:right;"><?php echo MyApp::buatuang($value['TOTAL']);?></td>
									</tr>
									<?php	
									$no++;
									}
									?>
									<tr>
										<td style="background:#6895a0; color:#ffffff"></td>
										<td style="background:#6895a0; color:#ffffff"></td>
										<td style="background:#6895a0; color:#ffffff">TOTAL</td>
										<td style="background:#6895a0; color:#ffffff"></td>
										<td style="background:#6895a0; color:#ffffff;text-align:right;"><?php echo $totorg;?></td>
										<td style="background:#6895a0;text-align:right; color:#ffffff"><?php echo (round($totseg['PROSENTASE'])*100) .'%';?></td>
										<td style="background:#6895a0;text-align:right; color:#ffffff"><?php echo MyApp::buatuang($totseg['PERSONEL_COST']);?></td>
										<!-- <td style="background:#6895a0;text-align:right; color:#ffffff"><?php echo MyApp::buatuang($totseg['PERSONEL_COST_SD_NOW']);?></td> -->
										<td style="background:#6895a0;text-align:right; color:#ffffff"><?php echo MyApp::buatuang($tocon = $tot1['CON']+$tot2['CON']);?></td>
										<td style="background:#6895a0;text-align:right; color:#ffffff"><?php echo MyApp::buatuang($toent = $tot1['ENT']+$tot2['ENT']);?></td>
										<td style="background:#6895a0;text-align:right; color:#ffffff"><?php echo MyApp::buatuang($towib = $tot1['WIB']+$tot2['WIB']);?></td>
										<td style="background:#6895a0;text-align:right; color:#ffffff"><?php echo MyApp::buatuang($todig = $tot1['DIG']+$tot2['DIG']);?></td>
										<td style="background:#6895a0;text-align:right; color:#ffffff"><?php echo MyApp::buatuang($tomob = $tot1['MOB']+$tot2['MOB']);?></td>
										<td style="background:#6895a0;text-align:right; color:#ffffff"><?php echo MyApp::buatuang($totot = $tot1['TOTAL']+$tot2['TOTAL']);?></td>
									</tr>
									<tr>
										<td style="background:#6895a0; color:#ffffff"></td>
										<td style="background:#6895a0; color:#ffffff"></td>
										<td style="background:#6895a0; color:#ffffff"></td>
										<td style="background:#6895a0; color:#ffffff"></td>
										<td style="background:#6895a0; color:#ffffff"></td>
										<td style="background:#6895a0;text-align:right; color:#ffffff"></td>
										<td style="background:#6895a0;text-align:right; color:#ffffff"></td>
										<!-- <td style="background:#6895a0;text-align:right; color:#ffffff"></td> -->
										<td style="background:#6895a0;text-align:right; color:#ffffff"><?php echo round(($tocon/$totot)*100,2).'%';?></td>
										<td style="background:#6895a0;text-align:right; color:#ffffff"><?php echo round(($toent/$totot)*100,2).'%';?></td>
										<td style="background:#6895a0;text-align:right; color:#ffffff"><?php echo round(($towib/$totot)*100,2).'%';?></td>
										<td style="background:#6895a0;text-align:right; color:#ffffff"><?php echo round(($todig/$totot)*100,2).'%';?></td>
										<td style="background:#6895a0;text-align:right; color:#ffffff"><?php echo round(($tomob/$totot)*100,2).'%';?></td>
										<td style="background:#6895a0;text-align:right; color:#ffffff"></td>
									</tr>
								</tbody>
							</table>
						<!-- </div> -->
					</div>
				</div><!-- .tab-pane  -->
			</div><!-- .tab-content  -->
		</div><!-- .nav-tabs-horizontal -->
	</div>
</div>
</div>