<div class="row">
	<div class="col-md-12">
		<div class="widget p-lg">

			<h3 class="m-b-lg">Form Cost Allocation</h3>
			<?php
			/* @var $this TRealsapController */
			/* @var $realsap TRealsap */
			/* @var $form CActiveForm */
			?>

			<div class="box-body">
				<?php
				    foreach(Yii::app()->user->getFlashes() as $key => $message) {
				        echo '<div class="alert alert-'.$key.'" role="alert">
									<span>'.$message.'</span>
								</div>';
				    }
				?>

			<?php $form=$this->beginWidget('CActiveForm', array(
				'id'=>'form-upload',
				'enableAjaxValidation'=>false,
			        'htmlOptions'=>array('class'=>'form-horizontal','enctype' => 'multipart/form-data',)
			)); ?>

			    <p class="note">Fields with <span class="required">*</span> are required.</p>

			    <?php echo $form->errorSummary($realsap); ?>
			        <div class="form-group">
			            <?php echo $form->labelEx($realsap,'TAHUN', array('class'=>'col-sm-2 control-label')); ?>
			            <div class="col-md-10">
			                <?php echo $form->dropDownList($realsap, 'TAHUN', array('2017'=>'2017', '2018'=>'2018', '2019'=>'2019', '2020'=>'2020', '2021'=>'2021', '2022'=>'2022'), array('prompt'=>'Pilih Tahun','class'=>'form-control')); ?>
			                <?php echo $form->error($realsap,'TAHUN', array('class'=>'alert alert-danger')); ?>
			            </div>
			        </div>

			        <div class="form-group">
			            <?php echo $form->labelEx($realsap,'BULAN', array('class'=>'col-sm-2 control-label')); ?>
			            <div class="col-md-10">
			                <?php echo $form->dropdownlist($realsap, 'BULAN',array('1'=>'Januari','2'=>'Februari','3'=>'Maret','4'=>'April','5'=>'Mei','6'=>'Juni','7'=>'Juli','8'=>'Agustus','9'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember'), array('class' => 'form-control','prompt'=>'Pilih Bulan')); ?>
			                <?php echo $form->error($realsap,'BULAN', array('class'=>'alert alert-danger')); ?>
			            </div>
			        </div>
			    <fieldset>
			    	<legend>Realisasi SAP</legend>
			        <div class="form-group">
			            <?php echo $form->labelEx($realsap,'REALISASI', array('class'=>'col-sm-2 control-label')); ?>
			            <div class="col-md-10">
			                <?php echo $form->textField($realsap, 'REALISASI', array('class' => 'form-control uang')); ?>
			                <?php echo $form->error($realsap,'REALISASI', array('class'=>'alert alert-danger')); ?>
			            </div>
			        </div>
				</fieldset>
				<fieldset>
					<legend>Upload Payslip</legend>
					<div class="form-group <?=(strlen($form->error($payslip,'fileupload')) > 0 ? 'has-error' : '' )?>">
                        <?php echo $form->labelEx($payslip, 'fileupload', array('class' => 'control-label col-sm-2')); ?>
                        <div class="col-md-10">
                            <?php echo $form->fileField($payslip, 'fileupload', array('class'=>'form-control input-sm')); ?>
                            <!-- <span class="help-block">(File berupa excel) >> <?php echo CHtml::link("Sample format", Yii::app()->request->baseUrl . "/document/download/upload_peserta_lulus.xls", array("target"=>"_blank")); ?></span> -->
                            <?php echo $form->error($payslip,'upload'); ?>
                        </div>
                    </div>
				</fieldset>
				<fieldset>
					<legend>Upload HR Current</legend>
					<div class="form-group <?=(strlen($form->error($payslip,'fileupload')) > 0 ? 'has-error' : '' )?>">
                        <?php echo $form->labelEx($hrcur, 'fileupload', array('class' => 'control-label col-sm-2')); ?>
                        <div class="col-md-10">
                            <?php echo $form->fileField($hrcur, 'fileupload', array('class'=>'form-control input-sm')); ?>
                            <!-- <span class="help-block">(File berupa excel) >> <?php echo CHtml::link("Sample format", Yii::app()->request->baseUrl . "/document/download/upload_peserta_lulus.xls", array("target"=>"_blank")); ?></span> -->
                            <?php echo $form->error($hrcur,'upload'); ?>
                        </div>
                    </div>
				</fieldset>

			</div>

			<div class="box-footer">
			    <?php echo CHtml::link('Cancel', Yii::app()->controller->createUrl('index'), array('class' => 'btn btn-sm btn-default')); ?> 
			    <?php echo CHtml::submitButton($realsap->isNewRecord ? 'Submit' : 'Submit', array('class' => 'subm btn btn-sm btn-info pull-right')); ?> 
			</div>

			<?php $this->endWidget(); ?>
	
		</div>
	</div>
</div>
<script>
$('.subm').click(function(e){
    tomb = $(this);
    e.preventDefault();
    swal({
      title: "Apakah anda yakin akan submit?",
      // text: "You will not be able to recover this imaginary file!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#10c469",
      confirmButtonText: "Yes",
      closeOnConfirm: false,
      html: false
    }, function(){
      swal({
        title : "<i class='fa fa-spinner fa-pulse fa-3x fa-fw'></i>",
        html : "<i class='fa fa-spinner fa-pulse fa-3x fa-fw'></i>",
        showCancelButton: false,
        showConfirmButton: false
      });
      $('#form-upload').submit();
    });
});
</script>