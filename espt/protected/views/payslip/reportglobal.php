<style>
	#parent {
		height: 500px;
	}
	
	#fixTable {
		width: 1800px !important;
	}
	.tabel-cost{
		width: 100%;
	}
	.tabel-cost td {
	    border: 1px solid #fff;
	    padding: 2px 3px;
	}
	tr.noborder{
		border:none !important;
	}
	td.noborder{
		border:none !important;
	}
	table.noborder{
		border:none !important;
	}
	.kiri{
		text-align: left;
	}
	.abu{
		background:#c4c4c4;
	}
	.abu2{
		background: #e0e0e0;
	}
	.merahmuda{
		background: #ffafaf;
	}
	.birumuda{
		background: #b1d3e0;
	}
	.ijo{
		background: #8af7bf;
	}
	.abutua{
		background: #9b9b9b;
	}
	.teksputih{
		color: #fff;
	}
	.merahmuda2{
		background: #fcc7c7;
	}
	.merahmuda3{
		background: #fce0e0;
	}
	.birumuda2{
		background: #c3dae2;
	}
	.birumuda3{
		background: #d7e7ed;
	}
</style>
<script>
	$(document).ready(function() {
		$("#fixTable").tableHeadFixer({"left" : 3}); 
	});
	$(document).ready(function() {
	  $("#btnExport").click(function(e) {
	    e.preventDefault();

	    //getting data from our table
	    var data_type = 'data:application/vnd.ms-excel';
	    var table_div = document.getElementById('isi');
	    var table_html = table_div.outerHTML.replace(/ /g, '%20');

	    var a = document.createElement('a');
	    a.href = data_type + ', ' + table_html;
	    a.download = 'REALISASI BEBAN PERSONNEL PERIODE <?php echo strtoupper(MyApp::convertBulan($bulan))." ".$tahun."-"?>' + Math.floor((Math.random() * 9999999) + 1000000) + '.xls';
	    a.click();
	  });
	});
	showmodal = function(e){
	    var jenis = $(e).attr("jenis");
	    var id = $(e).attr("rep");
	    var bulan = $(e).attr("bulan");
	    var tahun = $(e).attr("tahun");
	    if(jenis == 'man'){
	        var link = '<?php echo Yii::app()->controller->createUrl("popshowman");?>&id='+id+'&bulan='+bulan+'&tahun='+tahun;
	    }
	    $("#dlg").modal('show');
	    $("#dlg").load(link, function (response, status, xhr) {
	        if (status == "error") {
	            $("#dlg").html(response);
	        }
	    });
	    event.preventDefault();
	}
</script>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/dist/css/bots/bootstrap.css');?>
<div id="dlg" class="modal fade" role="dialog"></div>
<div class="row">
<div class="col-md-12">
	<h4 class="m-b-lg">Report <?php echo MyApp::convertBulan($bulan).' '.$tahun?></h4>
</div>
<div class="col-md-12">
	<div class="widget">
		<div class="m-b-lg nav-tabs-horizontal">
			<!-- tabs list -->
			<ul class="nav nav-tabs" role="tablist">
				<li role="presentation" class="active"><a href="#" aria-controls="tab-2" role="tab" >TREG/NON TREG</a></li>
				<li role="presentation"><a href="<?php echo Yii::app()->controller->createUrl('reportdivisi',array('tahun'=>$tahun,'bulan'=>$bulan));?>" aria-controls="tab-1" role="tab" >Divisi</a></li>
				<li role="presentation"><a href="<?php echo Yii::app()->controller->createUrl('report',array('tahun'=>$tahun,'bulan'=>$bulan));?>" aria-controls="tab-3" role="tab" >Witel</a></li>
			</ul><!-- .nav-tabs -->
			<ul class="nav nav-tabs" role="tablist">
				<li role="presentation" class="active"><a href="#" aria-controls="tab-2" role="tab"><?php echo MyApp::convertBulan($bulan); ?></a></li>
				<li role="presentation"><a href="<?php echo Yii::app()->controller->createUrl('reportglobalnow',array('tahun'=>$tahun,'bulan'=>$bulan));?>" aria-controls="tab-1" role="tab" >Sampai Dengan <?php echo MyApp::convertBulan($bulan); ?></a></li>
			</ul><!-- .nav-tabs -->

			<!-- Tab panes -->
			<div class="tab-content p-md">
				<div role="tabpanel" class="tab-pane in active fade" id="isi">
					<h4 class="m-b-md">REALISASI BEBAN PERSONNEL PERIODE <?php echo strtoupper(MyApp::convertBulan($bulan)).' '.$tahun?></h4>
					<div style="text-align:right;margin-bottom:10px;">
						<a href="#" class="btn btn-info" id="btnExport"><i class="fa fa-file-excel-o"></i> Export Excel</a>
						<!-- <a href="" class="btn btn-success"><i class="fa fa-file-pdf-o"></i> Export PDF</a> -->
						<br/>
						<br/>
						<h5>Realisasi Personnel Cost <?php echo MyApp::convertBulan($bulan).' '.$tahun.' : '.MyApp::buatrp($realmonth->REALISASI);?></h5>
						<table class="tabel-cost noborder">
							<tr>
								<td rowspan="2" class="noborder"></td>
								<td rowspan="2" class="noborder"></td>
								<td rowspan="2" class="noborder"></td>
								<td rowspan="2" class="noborder"></td>
								<td rowspan="2" style="text-align:center;" class="abutua teksputih" style="background: #9b9b9b;color:#ffffff"><b>Total Personnel Cost</b></td>
								<td rowspan="2" class="noborder"></td>
								<td colspan="6" style="text-align:center;background: #8af7bf;" class="ijo"><b>Personnel Cost Allocation</b></td>
							</tr>
							<tr>
								<td class="ijo" style="background: #8af7bf;"><b>Consumer</b></td>
								<td class="ijo" style="background: #8af7bf;"><b>Enterprise</b></td>
								<td class="ijo" style="background: #8af7bf;"><b>WIB</b></td>
								<td class="ijo" style="background: #8af7bf;"><b>Digital</b></td>
								<td class="ijo" style="background: #8af7bf;"><b>Mobile</b></td>
								<td class="ijo" style="background: #8af7bf;"><b>Total</b></td>
							</tr>
							<tr>
								<td rowspan="20" align="middle" class="merahmuda" style="background: #ffafaf; vertical-align:middle; cursor:pointer;" jenis="man" rep="TREG" bulan="<?php echo $bulan;?>" tahun="<?php echo $tahun;?>" onclick="showmodal(this)"><b>TREG<br><?php echo "(".$jml2.")";?></b></td>
								<td rowspan="2" class="noborder"></td>
								<td rowspan="2" class="kiri merahmuda" style="background: #ffafaf;cursor:pointer;" jenis="man" rep="SOH" bulan="<?php echo $bulan;?>" tahun="<?php echo $tahun;?>" onclick="showmodal(this)"><b>SENIOR OVERHEAD<br><?php echo "(".$jml3.")";?></b></td>
								<td rowspan="2"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($realmonth->REALISASI*$tsoh->PROSENTASE);?></td>
								<td class="noborder"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($soh1 = ($realmonth->REALISASI*$tsoh->PROSENTASE*0.25));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($soh2 = ($realmonth->REALISASI*$tsoh->PROSENTASE*0.25));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($soh3 = ($realmonth->REALISASI*$tsoh->PROSENTASE*0.25));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($soh4 = ($realmonth->REALISASI*$tsoh->PROSENTASE*0));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($soh5 = ($realmonth->REALISASI*$tsoh->PROSENTASE*0.25));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><b><?php echo MyApp::buatuang($soh1+$soh2+$soh3+$soh4+$soh5);?></b></td>
							</tr>
							<tr>
								<td class="merahmuda3" style="background: #fce0e0;"><?php echo round($tsoh->PROSENTASE*100,2).'%';?></td>
								<td class="noborder"></td>
								<td class="merahmuda3" style="background: #fce0e0;">25%</td>
								<td class="merahmuda3" style="background: #fce0e0;">25%</td>
								<td class="merahmuda3" style="background: #fce0e0;">25%</td>
								<td class="merahmuda3" style="background: #fce0e0;">0%</td>
								<td class="merahmuda3" style="background: #fce0e0;">25%</td>
								<td class="merahmuda3" style="background: #fce0e0;">100%</td>
							</tr>
							<tr>
								<td colspan="11" class="noborder"></td>
							</tr>
							<tr>
								<!-- <td rowspan="2"></td> -->
								<td rowspan="2" class="noborder"></td>
								<td rowspan="2" class="kiri merahmuda" style="background:#ffafaf;cursor:pointer" jenis="man" rep="NOH" bulan="<?php echo $bulan;?>" tahun="<?php echo $tahun;?>" onclick="showmodal(this)"><b>OVERHEAD (NETWORK) <br><?php echo "(".$jml4.")";?></b></td>
								<td rowspan="2"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($realmonth->REALISASI*$tnoh->PROSENTASE);?></td>
								<td class="noborder"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($noh1 = ($realmonth->REALISASI*$tnoh->PROSENTASE*0.59));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($noh2 = ($realmonth->REALISASI*$tnoh->PROSENTASE*0.20));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($noh3 = ($realmonth->REALISASI*$tnoh->PROSENTASE*0.21));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($noh4 = ($realmonth->REALISASI*$tnoh->PROSENTASE*0));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($noh5 = ($realmonth->REALISASI*$tnoh->PROSENTASE*0));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><b><?php echo MyApp::buatuang($noh1+$noh2+$noh3+$noh4+$noh5);?></b></td>
							</tr>
							<tr>
								<td class="merahmuda3" style="background: #fce0e0;"><?php echo round($tnoh->PROSENTASE*100,2).'%';?></td>
								<td class="noborder"></td>
								<td class="merahmuda3" style="background: #fce0e0;">59%</td>
								<td class="merahmuda3" style="background: #fce0e0;">20%</td>
								<td class="merahmuda3" style="background: #fce0e0;">21%</td>
								<td class="merahmuda3" style="background: #fce0e0;">0%</td>
								<td class="merahmuda3" style="background: #fce0e0;">0%</td>
								<td class="merahmuda3" style="background: #fce0e0;">100%</td>
							</tr>
							<tr>
								<td colspan="11" class="noborder"></td>
							</tr><tr>
								<!-- <td rowspan="2"></td> -->
								<td rowspan="2" class="noborder"></td>
								<td rowspan="2" class="kiri merahmuda" style="background: #ffafaf;cursor:pointer" jenis="man" rep="BOH" bulan="<?php echo $bulan;?>" tahun="<?php echo $tahun;?>" onclick="showmodal(this)"><b>Overhead (Non-Network Bizz Supp) <br><?php echo "(".$jml5.")";?></b></td>
								<td rowspan="2"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($realmonth->REALISASI*$tboh->PROSENTASE);?></td>
								<td class="noborder"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($boh1 = ($realmonth->REALISASI*$tboh->PROSENTASE*((100/3)/100)));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($boh2 = ($realmonth->REALISASI*$tboh->PROSENTASE*((100/3)/100)));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($boh3 = ($realmonth->REALISASI*$tboh->PROSENTASE*((100/3)/100)));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($boh4 = ($realmonth->REALISASI*$tboh->PROSENTASE*0));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($boh5 = ($realmonth->REALISASI*$tboh->PROSENTASE*0));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><b><?php echo MyApp::buatuang($boh1+$boh2+$boh3+$boh4+$boh5);?></b></td>
							</tr>
							<tr>
								<td class="merahmuda3" style="background: #fce0e0;"><?php echo round($tboh->PROSENTASE*100,2).'%';?></td>
								<td class="noborder"></td>
								<td class="merahmuda3" style="background: #fce0e0;">33%</td>
								<td class="merahmuda3" style="background: #fce0e0;">33%</td>
								<td class="merahmuda3" style="background: #fce0e0;">33%</td>
								<td class="merahmuda3" style="background: #fce0e0;">0%</td>
								<td class="merahmuda3" style="background: #fce0e0;">0%</td>
								<td class="merahmuda3" style="background: #fce0e0;">100%</td>
							</tr>
							<tr>
								<td colspan="11" class="noborder"></td>
							</tr><tr>
								<!-- <td rowspan="2"></td> -->
								<td rowspan="2" class="noborder"></td>
								<td rowspan="2" class="kiri merahmuda" style="background: #ffafaf;cursor:pointer;" jenis="man" rep="OOH" bulan="<?php echo $bulan;?>" tahun="<?php echo $tahun;?>" onclick="showmodal(this)"><b>Overhead (Non-Network Others) <br><?php echo "(".$jml6.")";?></b></td>
								<td rowspan="2"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($realmonth->REALISASI*$tooh->PROSENTASE);?></td>
								<td class="noborder"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($ooh1 = ($realmonth->REALISASI*$tooh->PROSENTASE*((66.7300380228137)/100)));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($ooh2 = ($realmonth->REALISASI*$tooh->PROSENTASE*((28.7072243346008)/100)));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($ooh3 = ($realmonth->REALISASI*$tooh->PROSENTASE*((4.56273764258555)/100)));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($ooh4 = ($realmonth->REALISASI*$tooh->PROSENTASE*0));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($ooh5 = ($realmonth->REALISASI*$tooh->PROSENTASE*0));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><b><?php echo MyApp::buatuang($ooh1+$ooh2+$ooh3+$ooh4+$ooh5);?></b></td>
							</tr>
							<tr>
								<td class="merahmuda3" style="background: #fce0e0;"><?php echo round($tooh->PROSENTASE*100,2).'%';?></td>
								<td class="noborder"></td>
								<td class="merahmuda3" style="background: #fce0e0;">67%</td>
								<td class="merahmuda3" style="background: #fce0e0;">29%</td>
								<td class="merahmuda3" style="background: #fce0e0;">5%</td>
								<td class="merahmuda3" style="background: #fce0e0;">0%</td>
								<td class="merahmuda3" style="background: #fce0e0;">0%</td>
								<td class="merahmuda3" style="background: #fce0e0;">100%</td>
							</tr>
							<tr>
								<td colspan="11" class="noborder"></td>
							</tr><tr>
								<!-- <td rowspan="2"></td> -->
								<td rowspan="2" class="noborder"></td>
								<td rowspan="2" class="kiri merahmuda" style="background: #ffafaf;cursor:pointer;" jenis="man" rep="TCON" bulan="<?php echo $bulan;?>" tahun="<?php echo $tahun;?>" onclick="showmodal(this)"><b>CFU Consumer<br><?php echo "(".$jml7.")";?></b></td>
								<td rowspan="2"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($realmonth->REALISASI*$tcon->PROSENTASE);?></td>
								<td class="noborder"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($tcon1 = ($realmonth->REALISASI*$tcon->PROSENTASE*((100)/100)));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($tcon2 = ($realmonth->REALISASI*$tcon->PROSENTASE*((0)/100)));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($tcon3 = ($realmonth->REALISASI*$tcon->PROSENTASE*((0)/100)));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($tcon4 = ($realmonth->REALISASI*$tcon->PROSENTASE*0));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($tcon5 = ($realmonth->REALISASI*$tcon->PROSENTASE*0));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><b><?php echo MyApp::buatuang($tcon1+$tcon2+$tcon3+$tcon4+$tcon5);?></b></td>
							</tr>
							<tr>
								<td class="merahmuda3" style="background: #fce0e0;"><?php echo round($tcon->PROSENTASE*100,2).'%';?></td>
								<td class="noborder"></td>
								<td class="merahmuda3" style="background: #fce0e0;">100%</td>
								<td class="merahmuda3" style="background: #fce0e0;">0%</td>
								<td class="merahmuda3" style="background: #fce0e0;">0%</td>
								<td class="merahmuda3" style="background: #fce0e0;">0%</td>
								<td class="merahmuda3" style="background: #fce0e0;">0%</td>
								<td class="merahmuda3" style="background: #fce0e0;">100%</td>
							</tr>
							<tr>
								<td colspan="11" class="noborder"></td>
							</tr><tr>
								<!-- <td rowspan="2"></td> -->
								<td rowspan="2" class="noborder"></td>
								<td rowspan="2" class="kiri merahmuda" style="background: #ffafaf;cursor:pointer;" jenis="man" rep="TENT" bulan="<?php echo $bulan;?>" tahun="<?php echo $tahun;?>" onclick="showmodal(this)"><b>CFU Enterprise<br><?php echo "(".$jml8.")";?></b></td>
								<td rowspan="2"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($realmonth->REALISASI*$tent->PROSENTASE);?></td>
								<td class="noborder"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($tent1 = ($realmonth->REALISASI*$tent->PROSENTASE*((0)/100)));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($tent2 = ($realmonth->REALISASI*$tent->PROSENTASE*((100)/100)));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($tent3 = ($realmonth->REALISASI*$tent->PROSENTASE*((0)/100)));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($tent4 = ($realmonth->REALISASI*$tent->PROSENTASE*0));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($tent5 = ($realmonth->REALISASI*$tent->PROSENTASE*0));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><b><?php echo MyApp::buatuang($tent1+$tent2+$tent3+$tent4+$tent5);?></b></td>
							</tr>
							<tr>
								<td class="merahmuda3" style="background: #fce0e0;"><?php echo round($tent->PROSENTASE*100,2).'%';?></td>
								<td class="noborder"></td>
								<td class="merahmuda3" style="background: #fce0e0;">0%</td>
								<td class="merahmuda3" style="background: #fce0e0;">100%</td>
								<td class="merahmuda3" style="background: #fce0e0;">0%</td>
								<td class="merahmuda3" style="background: #fce0e0;">0%</td>
								<td class="merahmuda3" style="background: #fce0e0;">0%</td>
								<td class="merahmuda3" style="background: #fce0e0;">100%</td>
							</tr>
							<tr>
								<td colspan="11" class="noborder"></td>
							</tr><tr>
								<!-- <td rowspan="2"></td> -->
								<td rowspan="2" class="noborder"></td>
								<td rowspan="2" class="kiri merahmuda" style="background: #ffafaf;cursor:pointer;" jenis="man" rep="TWIB" bulan="<?php echo $bulan;?>" tahun="<?php echo $tahun;?>" onclick="showmodal(this)"><b>CFU WIB<br><?php echo "(".$jml9.")";?></b></td>
								<td rowspan="2"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($realmonth->REALISASI*$twib->PROSENTASE);?></td>
								<td class="noborder"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($twib1 = ($realmonth->REALISASI*$twib->PROSENTASE*((0)/100)));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($twib2 = ($realmonth->REALISASI*$twib->PROSENTASE*((0)/100)));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($twib3 = ($realmonth->REALISASI*$twib->PROSENTASE*((100)/100)));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($twib4 = ($realmonth->REALISASI*$twib->PROSENTASE*0));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($twib5 = ($realmonth->REALISASI*$twib->PROSENTASE*0));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><b><?php echo MyApp::buatuang($twib1+$twib2+$twib3+$twib4+$twib5);?></b></td>
							</tr>
							<tr>
								<td class="merahmuda3" style="background: #fce0e0;"><?php echo round($twib->PROSENTASE*100,2).'%';?></td>
								<td class="noborder"></td>
								<td class="merahmuda3" style="background: #fce0e0;">0%</td>
								<td class="merahmuda3" style="background: #fce0e0;">0%</td>
								<td class="merahmuda3" style="background: #fce0e0;">100%</td>
								<td class="merahmuda3" style="background: #fce0e0;">0%</td>
								<td class="merahmuda3" style="background: #fce0e0;">0%</td>
								<td class="merahmuda3" style="background: #fce0e0;">100%</td>
							</tr>
							<tr class="noborder">
								<td class="noborder"></td>
							</tr>
							<tr>
								<td rowspan="13" align="middle" class="birumuda" style="background: #b1d3e0; vertical-align:middle; cursor:pointer;" jenis="man" rep="NONTREG" bulan="<?php echo $bulan;?>" tahun="<?php echo $tahun;?>" onclick="showmodal(this)"><b>NON TREG<br><?php echo "(".$jml1.")";?></b></td>
								<td rowspan="2" class="noborder"></td>
								<td rowspan="2" class="kiri birumuda" style="background: #b1d3e0;cursor:pointer;" jenis="man" rep="CON" bulan="<?php echo $bulan;?>" tahun="<?php echo $tahun;?>" onclick="showmodal(this)"><b>CFU CONSUMER<br><?php echo "(".$jml10.")";?></b></td>
								<td rowspan="2"></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($realmonth->REALISASI*$ncon->PROSENTASE);?></td>
								<td class="noborder"></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($ncon1= ($realmonth->REALISASI*$ncon->PROSENTASE*((100)/100)));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($ncon2= ($realmonth->REALISASI*$ncon->PROSENTASE*((0)/100)));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($ncon3= ($realmonth->REALISASI*$ncon->PROSENTASE*((0)/100)));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($ncon4= ($realmonth->REALISASI*$ncon->PROSENTASE*0));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($ncon5= ($realmonth->REALISASI*$ncon->PROSENTASE*0));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><b><?php echo MyApp::buatuang($ncon1+$ncon2+$ncon3+$ncon4+$ncon5);?></b></td>
							</tr>
							<tr>
								<td class="birumuda3" style="background: #d7e7ed;"><?php echo round($ncon->PROSENTASE*100,2).'%';?></td>
								<td class="noborder"></td>
								<td class="birumuda3" style="background: #d7e7ed;">100%</td>
								<td class="birumuda3" style="background: #d7e7ed;">0%</td>
								<td class="birumuda3" style="background: #d7e7ed;">0%</td>
								<td class="birumuda3" style="background: #d7e7ed;">0%</td>
								<td class="birumuda3" style="background: #d7e7ed;">0%</td>
								<td class="birumuda3" style="background: #d7e7ed;">100%</td>
							</tr>
							<tr>
								<!-- <td rowspan="2"></td> -->
								<td rowspan="2" class="noborder"></td>
								<td rowspan="2" class="kiri birumuda" style="background: #b1d3e0;cursor:pointer;" jenis="man" rep="ENT" bulan="<?php echo $bulan;?>" tahun="<?php echo $tahun;?>" onclick="showmodal(this)"><b>CFU ENTERPRISE<br><?php echo "(".$jml11.")";?></b></td>
								<td rowspan="2"></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($realmonth->REALISASI*$nent->PROSENTASE);?></td>
								<td class="noborder"></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($nent1= ($realmonth->REALISASI*$nent->PROSENTASE*((0)/100)));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($nent2= ($realmonth->REALISASI*$nent->PROSENTASE*((100)/100)));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($nent3= ($realmonth->REALISASI*$nent->PROSENTASE*((0)/100)));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($nent4= ($realmonth->REALISASI*$nent->PROSENTASE*0));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($nent5= ($realmonth->REALISASI*$nent->PROSENTASE*0));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><b><?php echo MyApp::buatuang($nent1+$nent2+$nent3+$nent4+$nent5);?></b></td>
							</tr>
							<tr>
								<td class="birumuda3" style="background: #d7e7ed;"><?php echo round($nent->PROSENTASE*100,2).'%';?></td>
								<td class="noborder"></td>
								<td class="birumuda3" style="background: #d7e7ed;">0%</td>
								<td class="birumuda3" style="background: #d7e7ed;">100%</td>
								<td class="birumuda3" style="background: #d7e7ed;">0%</td>
								<td class="birumuda3" style="background: #d7e7ed;">0%</td>
								<td class="birumuda3" style="background: #d7e7ed;">0%</td>
								<td class="birumuda3" style="background: #d7e7ed;">100%</td>
							</tr>
							<tr>
								<!-- <td rowspan="2"></td> -->
								<td rowspan="2" class="noborder"></td>
								<td rowspan="2" class="kiri birumuda" style="background: #b1d3e0;cursor:pointer;" jenis="man" rep="WIB" bulan="<?php echo $bulan;?>" tahun="<?php echo $tahun;?>" onclick="showmodal(this)"><b>CFU WHOLESALE & INT'L BROADBAND<br><?php echo "(".$jml12.")";?></b></td>
								<td rowspan="2"></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($realmonth->REALISASI*$nwib->PROSENTASE);?></td>
								<td class="noborder"></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($nwib1= ($realmonth->REALISASI*$nwib->PROSENTASE*((0)/100)));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($nwib2= ($realmonth->REALISASI*$nwib->PROSENTASE*((0)/100)));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($nwib3= ($realmonth->REALISASI*$nwib->PROSENTASE*((100)/100)));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($nwib4= ($realmonth->REALISASI*$nwib->PROSENTASE*0));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($nwib5= ($realmonth->REALISASI*$nwib->PROSENTASE*0));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><b><?php echo MyApp::buatuang($nwib1+$nwib2+$nwib3+$nwib4+$nwib5);?></b></td>
							</tr>
							<tr>
								<td class="birumuda3" style="background: #d7e7ed;"><?php echo round($nwib->PROSENTASE*100,2).'%';?></td>
								<td class="noborder"></td>
								<td class="birumuda3" style="background: #d7e7ed;">0%</td>
								<td class="birumuda3" style="background: #d7e7ed;">0%</td>
								<td class="birumuda3" style="background: #d7e7ed;">100%</td>
								<td class="birumuda3" style="background: #d7e7ed;">0%</td>
								<td class="birumuda3" style="background: #d7e7ed;">0%</td>
								<td class="birumuda3" style="background: #d7e7ed;">100%</td>
							</tr>
							<tr>
								<!-- <td rowspan="2"></td> -->
								<td rowspan="2" class="noborder"></td>
								<td rowspan="2" class="kiri birumuda" style="background: #b1d3e0;cursor:pointer;" jenis="man" rep="DIG" bulan="<?php echo $bulan;?>" tahun="<?php echo $tahun;?>" onclick="showmodal(this)"><b>CFU DIGITAL<br><?php echo "(".$jml13.")";?></b></td>
								<td rowspan="2"></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($realmonth->REALISASI*$ndig->PROSENTASE);?></td>
								<td class="noborder"></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($ndig1= ($realmonth->REALISASI*$ndig->PROSENTASE*((0)/100)));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($ndig2= ($realmonth->REALISASI*$ndig->PROSENTASE*((0)/100)));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($ndig3= ($realmonth->REALISASI*$ndig->PROSENTASE*((0)/100)));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($ndig4= ($realmonth->REALISASI*$ndig->PROSENTASE*(100/100)));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($ndig5= ($realmonth->REALISASI*$ndig->PROSENTASE*0));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><b><?php echo MyApp::buatuang($ndig1+$ndig2+$ndig3+$ndig4+$ndig5);?></b></td>
							</tr>
							<tr>
								<td class="birumuda3" style="background: #d7e7ed;"><?php echo round($ndig->PROSENTASE*100,2).'%';?></td>
								<td class="noborder"></td>
								<td class="birumuda3" style="background: #d7e7ed;">0%</td>
								<td class="birumuda3" style="background: #d7e7ed;">0%</td>
								<td class="birumuda3" style="background: #d7e7ed;">0%</td>
								<td class="birumuda3" style="background: #d7e7ed;">100%</td>
								<td class="birumuda3" style="background: #d7e7ed;">0%</td>
								<td class="birumuda3" style="background: #d7e7ed;">100%</td>
							</tr>
							<tr>
								<!-- <td rowspan="2"></td> -->
								<td rowspan="2" class="noborder"></td>
								<td rowspan="2" class="kiri birumuda" style="background: #b1d3e0;cursor:pointer;" jenis="man" rep="MOB" bulan="<?php echo $bulan;?>" tahun="<?php echo $tahun;?>" onclick="showmodal(this)"><b>CFU MOBILE<br><?php echo "(".$jml14.")";?></b></td>
								<td rowspan="2"></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($realmonth->REALISASI*$nmob->PROSENTASE);?></td>
								<td class="noborder"></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($nmob1= ($realmonth->REALISASI*$nmob->PROSENTASE*((0)/100)));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($nmob2= ($realmonth->REALISASI*$nmob->PROSENTASE*((0)/100)));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($nmob3= ($realmonth->REALISASI*$nmob->PROSENTASE*((0)/100)));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($nmob4= ($realmonth->REALISASI*$nmob->PROSENTASE*(100/100)));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($nmob5= ($realmonth->REALISASI*$nmob->PROSENTASE*0));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><b><?php echo MyApp::buatuang($nmob1+$nmob2+$nmob3+$nmob4+$nmob5);?></b></td>
							</tr>
							<tr>
								<td class="birumuda3" style="background: #d7e7ed;"><?php echo round($nmob->PROSENTASE*100,2).'%';?></td>
								<td class="noborder"></td>
								<td class="birumuda3" style="background: #d7e7ed;">0%</td>
								<td class="birumuda3" style="background: #d7e7ed;">0%</td>
								<td class="birumuda3" style="background: #d7e7ed;">0%</td>
								<td class="birumuda3" style="background: #d7e7ed;">0%</td>
								<td class="birumuda3" style="background: #d7e7ed;">100%</td>
								<td class="birumuda3" style="background: #d7e7ed;">100%</td>
							</tr>
							<tr>
								<td colspan="11" class="noborder"></td>
							</tr>
							<tr>
								<!-- <td rowspan="2"></td> -->
								<td rowspan="2" class="noborder"></td>
								<td rowspan="2" class="kiri birumuda" style="background: #b1d3e0;cursor:pointer;" jenis="man" rep="FU" bulan="<?php echo $bulan;?>" tahun="<?php echo $tahun;?>" onclick="showmodal(this)"><b>FU<br><?php echo "(".$jml15.")";?></b></td>
								<td rowspan="2"></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($realmonth->REALISASI*$prosfu);?></td>
								<td class="noborder"></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($fu1= ($realmonth->REALISASI*$prosfu*((37)/100)));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($fu2= ($realmonth->REALISASI*$prosfu*((28)/100)));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($fu3= ($realmonth->REALISASI*$prosfu*((21)/100)));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($fu4= ($realmonth->REALISASI*$prosfu*((0)/100)));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($fu5= ($realmonth->REALISASI*$prosfu*((14)/100)));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><b><?php echo MyApp::buatuang($fu1+$fu2+$fu3+$fu4+$fu5);?></b></td>
							</tr>
							<tr>
								<td class="birumuda3" style="background: #d7e7ed;"><?php echo round($prosfu*100,2).'%';?></td>
								<td class="noborder"></td>
								<td class="birumuda3" style="background: #d7e7ed;">37%</td>
								<td class="birumuda3" style="background: #d7e7ed;">28%</td>
								<td class="birumuda3" style="background: #d7e7ed;">21%</td>
								<td class="birumuda3" style="background: #d7e7ed;">0%</td>
								<td class="birumuda3" style="background: #d7e7ed;">14%</td>
								<td class="birumuda3" style="background: #d7e7ed;">100%</td>
							</tr>
							<tr class="noborder">
								<td colspan="12" class="noborder"></td>
							</tr>
							<tr>
								<td rowspan="2" class="noborder"></td>
								<td rowspan="2" class="noborder"></td>
								<td rowspan="2" class="kiri abu" style="background:#c4c4c4;"><b>TOTAL<br><?php echo "(".$jml16.")";?></b></td>
								<td rowspan="2"></td>
								<td class="abu" style="background:#c4c4c4;"><?php echo MyApp::buatuang($total);?></td>
								<td class="noborder"></td>
								<td class="abu" style="background:#c4c4c4;"><?php echo MyApp::buatuang($x1 = $soh1+$noh1+$boh1+$ooh1+$tcon1+$tent1+$twib1+$ncon1+$nent1+$nwib1+$ndig1+$nmob1+$fu1);?></td>
								<td class="abu" style="background:#c4c4c4;"><?php echo MyApp::buatuang($x2 = $soh2+$noh2+$boh2+$ooh2+$tcon2+$tent2+$twib2+$ncon2+$nent2+$nwib2+$ndig2+$nmob2+$fu2);?></td>
								<td class="abu" style="background:#c4c4c4;"><?php echo MyApp::buatuang($x3 = $soh3+$noh3+$boh3+$ooh3+$tcon3+$tent3+$twib3+$ncon3+$nent3+$nwib3+$ndig3+$nmob3+$fu3);?></td>
								<td class="abu" style="background:#c4c4c4;"><?php echo MyApp::buatuang($x4 = $soh4+$noh4+$boh4+$ooh4+$tcon4+$tent4+$twib4+$ncon4+$nent4+$nwib4+$ndig4+$nmob4+$fu4);?></td>
								<td class="abu" style="background:#c4c4c4;"><?php echo MyApp::buatuang($x5 = $soh5+$noh5+$boh5+$ooh5+$tcon5+$tent5+$twib5+$ncon5+$nent5+$nwib5+$ndig5+$nmob5+$fu5);?></td>
								<td class="abu" style="background:#c4c4c4;"><b><?php echo MyApp::buatuang($x1+$x2+$x3+$x4+$x5);?></b></td>
							</tr>
							<tr>
								<td class="abu2" style="background: #e0e0e0;"><?php echo round(($total/$realmonth->REALISASI) *100,2).'%';?></td>
								<td class="noborder"></td>
								<td class="abu2" style="background: #e0e0e0;"><?php echo round(($ase1 = $x1/$total) *100,2).'%';?></td>
								<td class="abu2" style="background: #e0e0e0;"><?php echo round(($ase2 = $x2/$total) *100,2).'%';?></td>
								<td class="abu2" style="background: #e0e0e0;"><?php echo round(($ase3 = $x3/$total) *100,2).'%';?></td>
								<td class="abu2" style="background: #e0e0e0;"><?php echo round(($ase4 = $x4/$total) *100,2).'%';?></td>
								<td class="abu2" style="background: #e0e0e0;"><?php echo round(($ase5 = $x5/$total) *100,2).'%';?></td>
								<td class="abu2" style="background: #e0e0e0;"><?php echo round(($ase1+$ase2+$ase3+$ase4+$ase5)*100,2).'%';?></td>
							</tr>
						</table>
					</div>
				</div><!-- .tab-pane  -->
			</div><!-- .tab-content  -->
		</div><!-- .nav-tabs-horizontal -->
	</div>
</div>
</div>