<style>
	#parent {
		height: 500px;
	}
	
	#fixTable {
		width: 1800px !important;
	}
	.tabel-cost{
		width: 100%;
	}
	.tabel-cost td {
	    border: 1px solid #fff;
	    padding: 2px 3px;
	}
	tr.noborder{
		border:none !important;
	}
	td.noborder{
		border:none !important;
	}
	table.noborder{
		border:none !important;
	}
	.kiri{
		text-align: left;
	}
	.abu{
		background:#c4c4c4;
	}
	.abu2{
		background: #e0e0e0;
	}
	.merahmuda{
		background: #ffafaf;
	}
	.birumuda{
		background: #b1d3e0;
	}
	.ijo{
		background: #8af7bf;
	}
	.abutua{
		background: #9b9b9b;
	}
	.teksputih{
		color: #fff;
	}
	.merahmuda2{
		background: #fcc7c7;
	}
	.merahmuda3{
		background: #fce0e0;
	}
	.birumuda2{
		background: #c3dae2;
	}
	.birumuda3{
		background: #d7e7ed;
	}
</style>
<script>
	$(document).ready(function() {
		$("#fixTable").tableHeadFixer({"left" : 3}); 
	});
	$(document).ready(function() {
	  $("#btnExport").click(function(e) {
	    e.preventDefault();

	    //getting data from our table
	    var data_type = 'data:application/vnd.ms-excel';
	    var table_div = document.getElementById('isi');
	    var table_html = table_div.outerHTML.replace(/ /g, '%20');

	    var a = document.createElement('a');
	    a.href = data_type + ', ' + table_html;
	    a.download = 'REALISASI BEBAN PERSONNEL HINGGA <?php echo strtoupper(MyApp::convertBulan($bulan))." ".$tahun."-"?>' + Math.floor((Math.random() * 9999999) + 1000000) + '.xls';
	    a.click();
	  });
	});
</script>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/dist/css/bots/bootstrap.css');?>
<div class="row">
<div class="col-md-12">
	<h4 class="m-b-lg">Report <?php echo MyApp::convertBulan($bulan).' '.$tahun?></h4>
</div>
<div class="col-md-12">
	<div class="widget">
		<div class="m-b-lg nav-tabs-horizontal">
			<!-- tabs list -->
			<ul class="nav nav-tabs" role="tablist">
				<li role="presentation" class="active"><a href="#" aria-controls="tab-2" role="tab" >TREG/NON TREG</a></li>
				<li role="presentation"><a href="<?php echo Yii::app()->controller->createUrl('reportdivisi',array('tahun'=>$tahun,'bulan'=>$bulan));?>" aria-controls="tab-1" role="tab" >Divisi</a></li>
				<li role="presentation"><a href="<?php echo Yii::app()->controller->createUrl('report',array('tahun'=>$tahun,'bulan'=>$bulan));?>" aria-controls="tab-3" role="tab" >Witel</a></li>
			</ul><!-- .nav-tabs -->
			<ul class="nav nav-tabs" role="tablist">
				<li role="presentation"><a href="<?php echo Yii::app()->controller->createUrl('reportglobal',array('tahun'=>$tahun,'bulan'=>$bulan));?>" aria-controls="tab-2" role="tab"><?php echo MyApp::convertBulan($bulan); ?></a></li>
				<li role="presentation" class="active"><a href="#" aria-controls="tab-1" role="tab" >Sampai Dengan <?php echo MyApp::convertBulan($bulan); ?></a></li>
			</ul>

			<!-- Tab panes -->
			<div class="tab-content p-md">
				<div role="tabpanel" class="tab-pane in active fade" id="isi">
					<h4 class="m-b-md">REALISASI BEBAN PERSONNEL HINGGA <?php echo strtoupper(MyApp::convertBulan($bulan)).' '.$tahun?></h4>
					<div style="text-align:right;margin-bottom:10px;">
						<a href="#" class="btn btn-info" id="btnExport"><i class="fa fa-file-excel-o"></i> Export Excel</a>
						<!-- <a href="" class="btn btn-success"><i class="fa fa-file-pdf-o"></i> Export PDF</a> -->
						<br/>
						<br/>
						<h5>Realisasi Personnel Cost Hingga <?php echo MyApp::convertBulan($bulan).' '.$tahun.' : '.MyApp::buatrp($realsapnow);?></h5>
						<table class="tabel-cost noborder">
							<tr>
								<td rowspan="2" class="noborder"></td>
								<td rowspan="2" class="noborder"></td>
								<td rowspan="2" class="noborder"></td>
								<td rowspan="2" class="noborder"></td>
								<td rowspan="2" style="text-align:center;" class="abutua teksputih" style="background: #9b9b9b;color:#ffffff"><b>Total Personnel Cost</b></td>
								<td rowspan="2" class="noborder"></td>
								<td colspan="6" style="text-align:center;background: #8af7bf;" class="ijo"><b>Personnel Cost Allocation</b></td>
							</tr>
							<tr>
								<td class="ijo" style="background: #8af7bf;"><b>Consumer</b></td>
								<td class="ijo" style="background: #8af7bf;"><b>Enterprise</b></td>
								<td class="ijo" style="background: #8af7bf;"><b>WIB</b></td>
								<td class="ijo" style="background: #8af7bf;"><b>Digital</b></td>
								<td class="ijo" style="background: #8af7bf;"><b>Mobile</b></td>
								<td class="ijo" style="background: #8af7bf;"><b>Total</b></td>
							</tr>
							<tr>
								<td rowspan="20" align="middle" class="merahmuda" style="background: #ffafaf; vertical-align:middle;"><b>TREG</b></td>
								<td rowspan="2" class="noborder"></td>
								<td rowspan="2" class="kiri merahmuda" style="background: #ffafaf;"><b>SENIOR OVERHEAD</b></td>
								<td rowspan="2"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($j1);?></td>
								<td class="noborder"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($soh1 = ($j1*0.25));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($soh2 = ($j1*0.25));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($soh3 = ($j1*0.25));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($soh4 = ($j1*0));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($soh5 = ($j1*0.25));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><b><?php echo MyApp::buatuang($soh1+$soh2+$soh3+$soh4+$soh5);?></b></td>
							</tr>
							<tr>
								<td class="merahmuda3" style="background: #fce0e0;"><?php //echo round($tsoh->PROSENTASE*100,2).'%';?></td>
								<td class="noborder"></td>
								<td class="merahmuda3" style="background: #fce0e0;">25%</td>
								<td class="merahmuda3" style="background: #fce0e0;">25%</td>
								<td class="merahmuda3" style="background: #fce0e0;">25%</td>
								<td class="merahmuda3" style="background: #fce0e0;">0%</td>
								<td class="merahmuda3" style="background: #fce0e0;">25%</td>
								<td class="merahmuda3" style="background: #fce0e0;">100%</td>
							</tr>
							<tr>
								<td colspan="11" class="noborder"></td>
							</tr>
							<tr>
								<!-- <td rowspan="2"></td> -->
								<td rowspan="2" class="noborder"></td>
								<td rowspan="2" class="kiri merahmuda" style="background:#ffafaf"><b>OVERHEAD (NETWORK)</b></td>
								<td rowspan="2"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($j2);?></td>
								<td class="noborder"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($noh1 = ($j2*0.59));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($noh2 = ($j2*0.20));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($noh3 = ($j2*0.21));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($noh4 = ($j2*0));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($noh5 = ($j2*0));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><b><?php echo MyApp::buatuang($noh1+$noh2+$noh3+$noh4+$noh5);?></b></td>
							</tr>
							<tr>
								<td class="merahmuda3" style="background: #fce0e0;"><?php //echo round($tnoh->PROSENTASE*100,2).'%';?></td>
								<td class="noborder"></td>
								<td class="merahmuda3" style="background: #fce0e0;">59%</td>
								<td class="merahmuda3" style="background: #fce0e0;">20%</td>
								<td class="merahmuda3" style="background: #fce0e0;">21%</td>
								<td class="merahmuda3" style="background: #fce0e0;">0%</td>
								<td class="merahmuda3" style="background: #fce0e0;">0%</td>
								<td class="merahmuda3" style="background: #fce0e0;">100%</td>
							</tr>
							<tr>
								<td colspan="11" class="noborder"></td>
							</tr><tr>
								<!-- <td rowspan="2"></td> -->
								<td rowspan="2" class="noborder"></td>
								<td rowspan="2" class="kiri merahmuda" style="background: #ffafaf;"><b>Overhead (Non-Network Bizz Supp)</b></td>
								<td rowspan="2"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($j3);?></td>
								<td class="noborder"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($boh1 = ($j3*((100/3)/100)));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($boh2 = ($j3*((100/3)/100)));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($boh3 = ($j3*((100/3)/100)));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($boh4 = ($j3*0));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($boh5 = ($j3*0));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><b><?php echo MyApp::buatuang($boh1+$boh2+$boh3+$boh4+$boh5);?></b></td>
							</tr>
							<tr>
								<td class="merahmuda3" style="background: #fce0e0;"><?php //echo round($tboh->PROSENTASE*100,2).'%';?></td>
								<td class="noborder"></td>
								<td class="merahmuda3" style="background: #fce0e0;">33%</td>
								<td class="merahmuda3" style="background: #fce0e0;">33%</td>
								<td class="merahmuda3" style="background: #fce0e0;">33%</td>
								<td class="merahmuda3" style="background: #fce0e0;">0%</td>
								<td class="merahmuda3" style="background: #fce0e0;">0%</td>
								<td class="merahmuda3" style="background: #fce0e0;">100%</td>
							</tr>
							<tr>
								<td colspan="11" class="noborder"></td>
							</tr><tr>
								<!-- <td rowspan="2"></td> -->
								<td rowspan="2" class="noborder"></td>
								<td rowspan="2" class="kiri merahmuda" style="background: #ffafaf;"><b>Overhead (Non-Network Others)</b></td>
								<td rowspan="2"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($j4);?></td>
								<td class="noborder"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($ooh1 = ($j4*((66.7300380228137)/100)));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($ooh2 = ($j4*((28.7072243346008)/100)));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($ooh3 = ($j4*((4.56273764258555)/100)));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($ooh4 = ($j4*0));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($ooh5 = ($j4*0));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><b><?php echo MyApp::buatuang($ooh1+$ooh2+$ooh3+$ooh4+$ooh5);?></b></td>
							</tr>
							<tr>
								<td class="merahmuda3" style="background: #fce0e0;"><?php echo round($tooh->PROSENTASE*100,2).'%';?></td>
								<td class="noborder"></td>
								<td class="merahmuda3" style="background: #fce0e0;">67%</td>
								<td class="merahmuda3" style="background: #fce0e0;">29%</td>
								<td class="merahmuda3" style="background: #fce0e0;">5%</td>
								<td class="merahmuda3" style="background: #fce0e0;">0%</td>
								<td class="merahmuda3" style="background: #fce0e0;">0%</td>
								<td class="merahmuda3" style="background: #fce0e0;">100%</td>
							</tr>
							<tr>
								<td colspan="11" class="noborder"></td>
							</tr><tr>
								<!-- <td rowspan="2"></td> -->
								<td rowspan="2" class="noborder"></td>
								<td rowspan="2" class="kiri merahmuda" style="background: #ffafaf;"><b>CFU Consumer</b></td>
								<td rowspan="2"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($j5);?></td>
								<td class="noborder"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($tcon1 = ($j5*((100)/100)));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($tcon2 = ($j5*((0)/100)));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($tcon3 = ($j5*((0)/100)));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($tcon4 = ($j5*0));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($tcon5 = ($j5*0));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><b><?php echo MyApp::buatuang($tcon1+$tcon2+$tcon3+$tcon4+$tcon5);?></b></td>
							</tr>
							<tr>
								<td class="merahmuda3" style="background: #fce0e0;"><?php //echo round($tcon->PROSENTASE*100,2).'%';?></td>
								<td class="noborder"></td>
								<td class="merahmuda3" style="background: #fce0e0;">100%</td>
								<td class="merahmuda3" style="background: #fce0e0;">0%</td>
								<td class="merahmuda3" style="background: #fce0e0;">0%</td>
								<td class="merahmuda3" style="background: #fce0e0;">0%</td>
								<td class="merahmuda3" style="background: #fce0e0;">0%</td>
								<td class="merahmuda3" style="background: #fce0e0;">100%</td>
							</tr>
							<tr>
								<td colspan="11" class="noborder"></td>
							</tr><tr>
								<!-- <td rowspan="2"></td> -->
								<td rowspan="2" class="noborder"></td>
								<td rowspan="2" class="kiri merahmuda" style="background: #ffafaf;"><b>CFU Enterprise</b></td>
								<td rowspan="2"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($j6);?></td>
								<td class="noborder"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($tent1 = ($j6*((0)/100)));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($tent2 = ($j6*((100)/100)));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($tent3 = ($j6*((0)/100)));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($tent4 = ($j6*0));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($tent5 = ($j6*0));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><b><?php echo MyApp::buatuang($tent1+$tent2+$tent3+$tent4+$tent5);?></b></td>
							</tr>
							<tr>
								<td class="merahmuda3" style="background: #fce0e0;"><?php //echo round($tent->PROSENTASE*100,2).'%';?></td>
								<td class="noborder"></td>
								<td class="merahmuda3" style="background: #fce0e0;">0%</td>
								<td class="merahmuda3" style="background: #fce0e0;">100%</td>
								<td class="merahmuda3" style="background: #fce0e0;">0%</td>
								<td class="merahmuda3" style="background: #fce0e0;">0%</td>
								<td class="merahmuda3" style="background: #fce0e0;">0%</td>
								<td class="merahmuda3" style="background: #fce0e0;">100%</td>
							</tr>
							<tr>
								<td colspan="11" class="noborder"></td>
							</tr><tr>
								<!-- <td rowspan="2"></td> -->
								<td rowspan="2" class="noborder"></td>
								<td rowspan="2" class="kiri merahmuda" style="background: #ffafaf;"><b>CFU WIB</b></td>
								<td rowspan="2"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($j7);?></td>
								<td class="noborder"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($twib1 = ($j7*((0)/100)));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($twib2 = ($j7*((0)/100)));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($twib3 = ($j7*((100)/100)));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($twib4 = ($j7*0));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><?php echo MyApp::buatuang($twib5 = ($j7*0));?></td>
								<td class="merahmuda2" style="background: #fcc7c7;"><b><?php echo MyApp::buatuang($twib1+$twib2+$twib3+$twib4+$twib5);?></b></td>
							</tr>
							<tr>
								<td class="merahmuda3" style="background: #fce0e0;"><?php //echo round($twib->PROSENTASE*100,2).'%';?></td>
								<td class="noborder"></td>
								<td class="merahmuda3" style="background: #fce0e0;">0%</td>
								<td class="merahmuda3" style="background: #fce0e0;">0%</td>
								<td class="merahmuda3" style="background: #fce0e0;">100%</td>
								<td class="merahmuda3" style="background: #fce0e0;">0%</td>
								<td class="merahmuda3" style="background: #fce0e0;">0%</td>
								<td class="merahmuda3" style="background: #fce0e0;">100%</td>
							</tr>
							<tr class="noborder">
								<td class="noborder"></td>
							</tr>
							<tr>
								<td rowspan="13" align="middle" class="birumuda" style="background: #b1d3e0; vertical-align:middle;"><b>NON TREG</b></td>
								<td rowspan="2" class="noborder"></td>
								<td rowspan="2" class="kiri birumuda" style="background: #b1d3e0;"><b>CFU CONSUMER</b></td>
								<td rowspan="2"></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($j8);?></td>
								<td class="noborder"></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($ncon1= ($j8*((100)/100)));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($ncon2= ($j8*((0)/100)));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($ncon3= ($j8*((0)/100)));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($ncon4= ($j8*0));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($ncon5= ($j8*0));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><b><?php echo MyApp::buatuang($ncon1+$ncon2+$ncon3+$ncon4+$ncon5);?></b></td>
							</tr>
							<tr>
								<td class="birumuda3" style="background: #d7e7ed;"><?php //echo round($ncon->PROSENTASE*100,2).'%';?></td>
								<td class="noborder"></td>
								<td class="birumuda3" style="background: #d7e7ed;">100%</td>
								<td class="birumuda3" style="background: #d7e7ed;">0%</td>
								<td class="birumuda3" style="background: #d7e7ed;">0%</td>
								<td class="birumuda3" style="background: #d7e7ed;">0%</td>
								<td class="birumuda3" style="background: #d7e7ed;">0%</td>
								<td class="birumuda3" style="background: #d7e7ed;">100%</td>
							</tr>
							<tr>
								<!-- <td rowspan="2"></td> -->
								<td rowspan="2" class="noborder"></td>
								<td rowspan="2" class="kiri birumuda" style="background: #b1d3e0;"><b>CFU ENTERPRISE</b></td>
								<td rowspan="2"></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($j9);?></td>
								<td class="noborder"></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($nent1= ($j9*((0)/100)));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($nent2= ($j9*((100)/100)));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($nent3= ($j9*((0)/100)));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($nent4= ($j9*0));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($nent5= ($j9*0));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><b><?php echo MyApp::buatuang($nent1+$nent2+$nent3+$nent4+$nent5);?></b></td>
							</tr>
							<tr>
								<td class="birumuda3" style="background: #d7e7ed;"><?php //echo round($nent->PROSENTASE*100,2).'%';?></td>
								<td class="noborder"></td>
								<td class="birumuda3" style="background: #d7e7ed;">0%</td>
								<td class="birumuda3" style="background: #d7e7ed;">100%</td>
								<td class="birumuda3" style="background: #d7e7ed;">0%</td>
								<td class="birumuda3" style="background: #d7e7ed;">0%</td>
								<td class="birumuda3" style="background: #d7e7ed;">0%</td>
								<td class="birumuda3" style="background: #d7e7ed;">100%</td>
							</tr>
							<tr>
								<!-- <td rowspan="2"></td> -->
								<td rowspan="2" class="noborder"></td>
								<td rowspan="2" class="kiri birumuda" style="background: #b1d3e0;"><b>CFU WHOLESALE & INT'L BROADBAND</b></td>
								<td rowspan="2"></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($j10);?></td>
								<td class="noborder"></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($nwib1= ($j10*((0)/100)));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($nwib2= ($j10*((0)/100)));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($nwib3= ($j10*((100)/100)));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($nwib4= ($j10*0));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($nwib5= ($j10*0));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><b><?php echo MyApp::buatuang($nwib1+$nwib2+$nwib3+$nwib4+$nwib5);?></b></td>
							</tr>
							<tr>
								<td class="birumuda3" style="background: #d7e7ed;"><?php //echo round($nwib->PROSENTASE*100,2).'%';?></td>
								<td class="noborder"></td>
								<td class="birumuda3" style="background: #d7e7ed;">0%</td>
								<td class="birumuda3" style="background: #d7e7ed;">0%</td>
								<td class="birumuda3" style="background: #d7e7ed;">100%</td>
								<td class="birumuda3" style="background: #d7e7ed;">0%</td>
								<td class="birumuda3" style="background: #d7e7ed;">0%</td>
								<td class="birumuda3" style="background: #d7e7ed;">100%</td>
							</tr>
							<tr>
								<!-- <td rowspan="2"></td> -->
								<td rowspan="2" class="noborder"></td>
								<td rowspan="2" class="kiri birumuda" style="background: #b1d3e0;"><b>CFU DIGITAL</b></td>
								<td rowspan="2"></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($j11);?></td>
								<td class="noborder"></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($ndig1= ($j11*((0)/100)));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($ndig2= ($j11*((0)/100)));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($ndig3= ($j11*((0)/100)));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($ndig4= ($j11*(100/100)));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($ndig5= ($j11*0));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><b><?php echo MyApp::buatuang($ndig1+$ndig2+$ndig3+$ndig4+$ndig5);?></b></td>
							</tr>
							<tr>
								<td class="birumuda3" style="background: #d7e7ed;"><?php //echo round($ndig->PROSENTASE*100,2).'%';?></td>
								<td class="noborder"></td>
								<td class="birumuda3" style="background: #d7e7ed;">0%</td>
								<td class="birumuda3" style="background: #d7e7ed;">0%</td>
								<td class="birumuda3" style="background: #d7e7ed;">0%</td>
								<td class="birumuda3" style="background: #d7e7ed;">100%</td>
								<td class="birumuda3" style="background: #d7e7ed;">0%</td>
								<td class="birumuda3" style="background: #d7e7ed;">100%</td>
							</tr>
							<tr>
								<!-- <td rowspan="2"></td> -->
								<td rowspan="2" class="noborder"></td>
								<td rowspan="2" class="kiri birumuda" style="background: #b1d3e0;"><b>CFU MOBILE</b></td>
								<td rowspan="2"></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($j12);?></td>
								<td class="noborder"></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($nmob1= ($j12*((0)/100)));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($nmob2= ($j12*((0)/100)));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($nmob3= ($j12*((0)/100)));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($nmob4= ($j12*(100/100)));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($nmob5= ($j12*0));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><b><?php echo MyApp::buatuang($nmob1+$nmob2+$nmob3+$nmob4+$nmob5);?></b></td>
							</tr>
							<tr>
								<td class="birumuda3" style="background: #d7e7ed;"><?php //echo round($nmob->PROSENTASE*100,2).'%';?></td>
								<td class="noborder"></td>
								<td class="birumuda3" style="background: #d7e7ed;">0%</td>
								<td class="birumuda3" style="background: #d7e7ed;">0%</td>
								<td class="birumuda3" style="background: #d7e7ed;">0%</td>
								<td class="birumuda3" style="background: #d7e7ed;">0%</td>
								<td class="birumuda3" style="background: #d7e7ed;">100%</td>
								<td class="birumuda3" style="background: #d7e7ed;">100%</td>
							</tr>
							<tr>
								<td colspan="11" class="noborder"></td>
							</tr>
							<tr>
								<!-- <td rowspan="2"></td> -->
								<td rowspan="2" class="noborder"></td>
								<td rowspan="2" class="kiri birumuda" style="background: #b1d3e0;"><b>FU</b></td>
								<td rowspan="2"></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($j13);?></td>
								<td class="noborder"></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($fu1= ($j13*((37)/100)));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($fu2= ($j13*((28)/100)));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($fu3= ($j13*((21)/100)));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($fu4= ($j13*((0)/100)));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><?php echo MyApp::buatuang($fu5= ($j13*((14)/100)));?></td>
								<td class="birumuda2" style="background: #c3dae2;"><b><?php echo MyApp::buatuang($fu1+$fu2+$fu3+$fu4+$fu5);?></b></td>
							</tr>
							<tr>
								<td class="birumuda3" style="background: #d7e7ed;"><?php //echo round($prosfu*100,2).'%';?></td>
								<td class="noborder"></td>
								<td class="birumuda3" style="background: #d7e7ed;">37%</td>
								<td class="birumuda3" style="background: #d7e7ed;">28%</td>
								<td class="birumuda3" style="background: #d7e7ed;">21%</td>
								<td class="birumuda3" style="background: #d7e7ed;">0%</td>
								<td class="birumuda3" style="background: #d7e7ed;">14%</td>
								<td class="birumuda3" style="background: #d7e7ed;">100%</td>
							</tr>
							<tr class="noborder">
								<td colspan="12" class="noborder"></td>
							</tr>
							<tr>
								<td rowspan="2" class="noborder"></td>
								<td rowspan="2" class="noborder"></td>
								<td rowspan="2" class="kiri abu" style="background:#c4c4c4;"><b>TOTAL</b></td>
								<td rowspan="2"></td>
								<td class="abu" style="background:#c4c4c4;"><?php echo MyApp::buatuang($total);?></td>
								<td class="noborder"></td>
								<td class="abu" style="background:#c4c4c4;"><?php echo MyApp::buatuang($x1 = $soh1+$noh1+$boh1+$ooh1+$tcon1+$tent1+$twib1+$ncon1+$nent1+$nwib1+$ndig1+$nmob1+$fu1);?></td>
								<td class="abu" style="background:#c4c4c4;"><?php echo MyApp::buatuang($x2 = $soh2+$noh2+$boh2+$ooh2+$tcon2+$tent2+$twib2+$ncon2+$nent2+$nwib2+$ndig2+$nmob2+$fu2);?></td>
								<td class="abu" style="background:#c4c4c4;"><?php echo MyApp::buatuang($x3 = $soh3+$noh3+$boh3+$ooh3+$tcon3+$tent3+$twib3+$ncon3+$nent3+$nwib3+$ndig3+$nmob3+$fu3);?></td>
								<td class="abu" style="background:#c4c4c4;"><?php echo MyApp::buatuang($x4 = $soh4+$noh4+$boh4+$ooh4+$tcon4+$tent4+$twib4+$ncon4+$nent4+$nwib4+$ndig4+$nmob4+$fu4);?></td>
								<td class="abu" style="background:#c4c4c4;"><?php echo MyApp::buatuang($x5 = $soh5+$noh5+$boh5+$ooh5+$tcon5+$tent5+$twib5+$ncon5+$nent5+$nwib5+$ndig5+$nmob5+$fu5);?></td>
								<td class="abu" style="background:#c4c4c4;"><b><?php echo MyApp::buatuang($x1+$x2+$x3+$x4+$x5);?></b></td>
							</tr>
							<tr>
								<td class="abu2" style="background: #e0e0e0;"><?php //echo round(($total/$realmonth->REALISASI) *100,2).'%';?></td>
								<td class="noborder"></td>
								<td class="abu2" style="background: #e0e0e0;"><?php echo round(($ase1 = $x1/$total) *100,2).'%';?></td>
								<td class="abu2" style="background: #e0e0e0;"><?php echo round(($ase2 = $x2/$total) *100,2).'%';?></td>
								<td class="abu2" style="background: #e0e0e0;"><?php echo round(($ase3 = $x3/$total) *100,2).'%';?></td>
								<td class="abu2" style="background: #e0e0e0;"><?php echo round(($ase4 = $x4/$total) *100,2).'%';?></td>
								<td class="abu2" style="background: #e0e0e0;"><?php echo round(($ase5 = $x5/$total) *100,2).'%';?></td>
								<td class="abu2" style="background: #e0e0e0;"><?php echo round(($ase1+$ase2+$ase3+$ase4+$ase5)*100,2).'%';?></td>
							</tr>
						</table>
					</div>
				</div><!-- .tab-pane  -->
			</div><!-- .tab-content  -->
		</div><!-- .nav-tabs-horizontal -->
	</div>
</div>
</div>