<style type="text/css">
	.bg-info, .info {
	    background-color: #35b8e0 !important;
	    color: #fff;
	    border-radius: 10px;
	}
</style>
<div class="tab-content p-md">
	<div role="tabpanel" class="tab-pane fade active in" id="colors">
		<h3 class="m-b-lg">Cost Allocation</h3>
		<div class="row">
			<?php
			    foreach(Yii::app()->user->getFlashes() as $key => $message) {
			        echo '<div class="alert alert-'.$key.'" role="alert">
								<span>'.$message.'</span>
							</div>';
			    }
			?>
			<div class="pull-right">
				<?php if(Yii::app()->user->roles == 'Admin'){?>
				<?php echo CHtml::link('<i class="fa fa-plus"></i> Tambah',array('payslip/inputall'),array('class'=>'btn btn-primary', 'style'=>'margin-right:10px;')); ?>
				<?php } ?>
				<!-- <a class="btn btn-primary" style="margin-right:10px;"><i class="fa fa-plus"></i> Tambah payslip per bulan</a> -->
			</div>
			<br/>
			<br/>
			<br/>
		</div>
		<div class="row">
			<?php
			$this->widget('zii.widgets.CListView', array(
				'dataProvider'=>$model2->listNow(),
				'itemView'=>'_view_item2',
				'summaryText'=>'', 
			));
			?>
		</div><!-- .row -->
		<center><a class="btn btn-rounded btn-info btn-outline" id="but"><?php echo !$filter? 'Show More...' : 'Hide...';?></a></center>

		<div class="row" style="<?php echo !$filter ? 'display:none' : ''; ?>" id="hidden_content">
		<br>
			<div class="col-sm-12">
				<div class="widget bg-info">
					<div class="widget-body">
						<div class="m-b-l">
						<?php $form=$this->beginWidget('CActiveForm', array(
								'id'=>'form-upload',
								'enableAjaxValidation'=>false,
							        'htmlOptions'=>array('class'=>'form-inline','enctype' => 'multipart/form-data',)
							)); ?>
								<center>
								<div class="form-group">
									<label for="exampleInputName2"><b>Filter :</b></label>
								</div>
								<br>
								<div class="form-group">
									<label for="exampleInputName2">Tahun</label>
									<?php echo $form->dropDownList($model, 'TAHUNFILTER', array('2017'=>'2017', '2018'=>'2018', '2019'=>'2019', '2020'=>'2020', '2021'=>'2021', '2022'=>'2022'), array('name'=>'TRealsapex[TAHUNFILTER]','prompt'=>'Pilih Tahun','class'=>'form-control')); ?>
				                	<?php echo $form->error($model,'TAHUN', array('class'=>'alert alert-danger')); ?>
								</div>
								<div class="form-group">
									<label for="exampleInputEmail2">Bulan</label>
									<?php echo $form->dropdownlist($model, 'BULANFILTER',array('1'=>'Januari','2'=>'Februari','3'=>'Maret','4'=>'April','5'=>'Mei','6'=>'Juni','7'=>'Juli','8'=>'Agustus','9'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember'), array('name'=>'TRealsapex[BULANFILTER]','class' => 'form-control','prompt'=>'Pilih Bulan')); ?>
				                	<?php echo $form->error($model,'BULAN', array('class'=>'alert alert-danger')); ?>
								</div>
								<button type="submit" class="btn btn-default">Filter</button>
								</center>
							<?php $this->endWidget(); ?>
						</div>
					</div>
				</div>
        	</div>
			<?php
			$this->widget('zii.widgets.CListView', array(
				'dataProvider'=>$model->listPerBulan($blnshow),
				'itemView'=>'_view_item',
			));
			?>
		</div><!-- .row -->
	</div><!-- .tab-pane -->
</div>
<script>
<?php if ($filter) {?>
$('html, body').animate({ scrollTop: $('#hidden_content').offset().top }, 'slow');
<?php } ?>
$(document).ready(function() {
  $("#but").click(function(){
  	  $("#but").text($("#hidden_content").is(":hidden")?"Hide...":"Show More...");
      $("#hidden_content").slideToggle("slow");
  });
});
$('.subm').click(function(e){
	tomb = $(this);
	e.preventDefault();
	swal({
	  title: "Apakah anda yakin akan melakukan generate report?",
	  // text: "You will not be able to recover this imaginary file!",
	  type: "warning",
	  showCancelButton: true,
	  confirmButtonColor: "#10c469",
	  confirmButtonText: "Yes",
	  closeOnConfirm: false,
	  html: false
	}, function(){
	  swal({
	  	title : "<i class='fa fa-spinner fa-pulse fa-3x fa-fw'></i>",
	  	html : "<i class='fa fa-spinner fa-pulse fa-3x fa-fw'></i>",
	  	showCancelButton: false,
  		showConfirmButton: false
	  });
	  window.location.href = tomb.attr('href');
	});
});
$('.del').click(function(e){
	tomb = $(this);
	e.preventDefault();
	swal({
	  title: "Apakah anda yakin akan menghapus?",
	  // text: "You will not be able to recover this imaginary file!",
	  type: "warning",
	  showCancelButton: true,
	  confirmButtonColor: "#10c469",
	  confirmButtonText: "Yes",
	  closeOnConfirm: false,
	  html: false
	}, function(){
	  swal({
	  	title : "<i class='fa fa-spinner fa-pulse fa-3x fa-fw'></i>",
	  	html : "<i class='fa fa-spinner fa-pulse fa-3x fa-fw'></i>",
	  	showCancelButton: false,
  		showConfirmButton: false
	  });
	  window.location.href = tomb.attr('href');
	});
});
</script>