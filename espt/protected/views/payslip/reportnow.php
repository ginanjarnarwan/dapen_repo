<style>
	#parent {
		height: 500px;
	}
	
	#fixTable {
		width: 1800px !important;
	}
</style>
<script>
	$(document).ready(function() {
		$("#fixTable").tableHeadFixer({"left" : 2}); 
	});
	$(document).ready(function() {
	  $("#btnExport").click(function(e) {
	    e.preventDefault();

	    //getting data from our table
	    var data_type = 'data:application/vnd.ms-excel';
	    var table_div = document.getElementById('isi');
	    var table_html = table_div.outerHTML.replace(/ /g, '%20');

	    var a = document.createElement('a');
	    a.href = data_type + ', ' + table_html;
	    a.download = 'REALISASI BEBAN PERSONNEL PER WITEL PERIODE <?php echo strtoupper(MyApp::convertBulan($bulan))." ".$tahun."-"?>' + Math.floor((Math.random() * 9999999) + 1000000) + '.xls';
	    a.click();
	  });
	});
</script>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/dist/css/bots/bootstrap.css');?>
<div class="row">
<div class="col-md-12">
	<h4 class="m-b-lg">Report <?php echo MyApp::convertBulan($bulan).' '.$tahun?></h4>
</div>
<div class="col-md-12">
	<div class="widget">
		<div class="m-b-lg nav-tabs-horizontal">
			<!-- tabs list -->
			<ul class="nav nav-tabs" role="tablist">
				<li role="presentation"><a href="<?php echo Yii::app()->controller->createUrl('reportglobal',array('tahun'=>$tahun,'bulan'=>$bulan));?>" aria-controls="tab-2" role="tab" >TREG/NON TREG</a></li>
				<li role="presentation"><a href="<?php echo Yii::app()->controller->createUrl('reportdivisi',array('tahun'=>$tahun,'bulan'=>$bulan));?>" aria-controls="tab-1" role="tab" >Divisi</a></li>
				<li role="presentation" class="active"><a href="#" aria-controls="tab-3" role="tab" >Witel</a></li>
			</ul><!-- .nav-tabs -->
			<ul class="nav nav-tabs" role="tablist">
				<li role="presentation"><a href="<?php echo Yii::app()->controller->createUrl('report',array('tahun'=>$tahun,'bulan'=>$bulan));?>" aria-controls="tab-2" role="tab"><?php echo MyApp::convertBulan($bulan); ?></a></li>
				<li role="presentation" class="active"><a href="#" aria-controls="tab-1" role="tab" >Sampai Dengan <?php echo MyApp::convertBulan($bulan); ?></a></li>
			</ul>

			<!-- Tab panes -->
			<div class="tab-content p-md">
				<div role="tabpanel" class="tab-pane in active fade" id="isi">
					<h4 class="m-b-md">REALISASI BEBAN PERSONNEL PER WITEL HINGGA BULAN <?php echo strtoupper(MyApp::convertBulan($bulan)).' '.$tahun?></h4>
					<div style="text-align:right;margin-bottom:10px;">
						<a href="#" class="btn btn-info" id="btnExport"><i class="fa fa-file-excel-o"></i> Export Excel</a>
					</div>
					<div id="parent">
						<!-- <div class="table-responsive"> -->
							<table class="tabel" id="fixTable">
								<thead>
									<tr>
										<th rowspan="2">#</th>
										<th rowspan="2">HOST</th>
										<th rowspan="2">NAMA WITEL</th>
										<th rowspan="2" style="text-align:right;">REALISASI HINGGA <?php echo strtoupper(MyApp::convertBulan($bulan));?></th>
										<th colspan="6" style="text-align:center; background:#ff8787;">CFU</th>
									</tr>
									<tr>
										<th style="background:#f7be62;text-align:right;">CONSUMER</th>
										<th style="background:#f7be62;text-align:right;">ENTERPRISE</th>
										<th style="background:#f7be62;text-align:right;">WIB</th>
										<th style="background:#f7be62;text-align:right;">DIGITAL</th>
										<th style="background:#f7be62;text-align:right;">MOBILE</th>
										<th style="background:#ff8787;text-align:right;">TOTAL</th>
									</tr>
									<!-- <tr>
										<th></th>
										<th></th>
										<th colspan="2"></th>
										<th style="background:#05cdff;">Biz Supp OH</th>
										<th style="background:#05cdff;">Consumer</th>
										<th style="background:#05cdff;">Enterprise</th>
										<th style="background:#05cdff;">Wholesale & Int</th>
										<th style="background:#05cdff;">Network OH</th>
										<th style="background:#05cdff;">Others OH</th>
										<th style="background:#05cdff;">Senior OH</th>
										<th style="background:#60a0ff;"></th>
										<th style="background:#f7be62;"></th>
										<th style="background:#f7be62;"></th>
										<th style="background:#f7be62;"></th>
										<th style="background:#f7be62;"></th>
										<th style="background:#f7be62;"></th>
										<th style="background:#ff8787;"></th>
									</tr> -->
								</thead>
								<tbody>
									<?php
									$no = 1;
									foreach ($tregarray as $key => $value) {
									// $boh = MPresSegment::model()->findByAttributes(array('ORI_SEG'=>'TREG','SEGMENT'=>'BOH'));
									// $con = MPresSegment::model()->findByAttributes(array('ORI_SEG'=>'TREG','SEGMENT'=>'CON'));
									// $ent = MPresSegment::model()->findByAttributes(array('ORI_SEG'=>'TREG','SEGMENT'=>'ENT'));
									// $wib = MPresSegment::model()->findByAttributes(array('ORI_SEG'=>'TREG','SEGMENT'=>'WIB'));
									// $noh = MPresSegment::model()->findByAttributes(array('ORI_SEG'=>'TREG','SEGMENT'=>'NOH'));
									// $ooh = MPresSegment::model()->findByAttributes(array('ORI_SEG'=>'TREG','SEGMENT'=>'OOH'));
									// $soh = MPresSegment::model()->findByAttributes(array('ORI_SEG'=>'TREG','SEGMENT'=>'SOH'));
									?>
									<tr style="background:#d1a1a1;">
										<td></td>
										<td></td>
										<td><?php echo $value['V_SHORT_DIVISI']; ?></td>
										<!-- <td></td> -->
										<td style="text-align:right"><?php echo MyApp::buatuang($value['PERSONEL_COST_DIVISI']); ?></td>
										<td style="text-align:right"></td>
										<td style="text-align:right"></td>
										<td style="text-align:right"></td>
										<td style="text-align:right"></td>
										<td style="text-align:right"></td>
										<td></td>
									</tr>
									<?php
									foreach ($value['WITEL'] as $key2 => $value2) {
									?>
									<tr>
										<td style="text-align:center"><?php echo $no; ?></td>
										<td><?php echo $value2['C_HOST']; ?></td>
										<td><?php echo $value2['WITEL']; ?></td>
										<!-- <td style="text-align:right"><?php echo round($value2['PROSENTASE'] * 100,2).'%'; ?></td> -->
										<td style="text-align:right"><?php echo MyApp::buatuang($value2['PERSONEL_COST_WITEL']); ?></td>
										<td style="background:#f7be62; text-align:right"><?php echo MyApp::buatuang($value2['CONSUMER']); ?></td>
										<td style="background:#f7be62; text-align:right"><?php echo MyApp::buatuang($value2['ENTERPRISE']); ?></td>
										<td style="background:#f7be62; text-align:right"><?php echo MyApp::buatuang($value2['WIB']); ?></td>
										<td style="background:#f7be62; text-align:right"><?php echo MyApp::buatuang($value2['DIGITAL']); ?></td>
										<td style="background:#f7be62; text-align:right"><?php echo MyApp::buatuang($value2['MOBILE']); ?></td>
										<td style="background:#ff8787; text-align:right"><?php echo MyApp::buatuang($value2['TOTAL']); ?></td>
									</tr>
									<?php
										$no++;
									}
									?>
									<?php
									}
									?>
									<tr>
										<td style="background:#6895a0;text-align:right; color:#ffffff"></td>
										<td style="background:#6895a0;text-align:right; color:#ffffff"></td>
										<td style="background:#6895a0;text-align:right; color:#ffffff"></td>
										<!-- <td style="background:#6895a0;text-align:right; color:#ffffff"></td> -->
										<td style="background:#6895a0;text-align:right; color:#ffffff"><?php echo MyApp::buatuang($tot['TOTAL_PERSONEL']); ?></td>
										<td style="background:#6895a0;text-align:right; color:#ffffff"><?php echo MyApp::buatuang($tot['TOTAL_CONSUMER']); ?></td>
										<td style="background:#6895a0;text-align:right; color:#ffffff"><?php echo MyApp::buatuang($tot['TOTAL_ENTERPRISE']); ?></td>
										<td style="background:#6895a0;text-align:right; color:#ffffff"><?php echo MyApp::buatuang($tot['TOTAL_WIB']); ?></td>
										<td style="background:#6895a0;text-align:right; color:#ffffff"><?php echo MyApp::buatuang($tot['TOTAL_DIGITAL']); ?></td>
										<td style="background:#6895a0;text-align:right; color:#ffffff"><?php echo MyApp::buatuang($tot['TOTAL_MOBILE']); ?></td>
										<td style="background:#6895a0;text-align:right; color:#ffffff"><?php echo MyApp::buatuang($tot['GRAND_TOTAL']); ?></td>
									</tr>
									<tr>
										<td style="background:#6895a0;text-align:right; color:#ffffff"></td>
										<td style="background:#6895a0;text-align:right; color:#ffffff"></td>
										<td style="background:#6895a0;text-align:right; color:#ffffff"></td>
										<!-- <td style="background:#6895a0;text-align:right; color:#ffffff"></td> -->
										<td style="background:#6895a0;text-align:right; color:#ffffff"></td>
										<td style="background:#6895a0;text-align:right; color:#ffffff"><?php echo round(($tot['TOTAL_CONSUMER']/$tot['GRAND_TOTAL'])*100,2).'%'; ?></td>
										<td style="background:#6895a0;text-align:right; color:#ffffff"><?php echo round(($tot['TOTAL_ENTERPRISE']/$tot['GRAND_TOTAL'])*100,2).'%'; ?></td>
										<td style="background:#6895a0;text-align:right; color:#ffffff"><?php echo round(($tot['TOTAL_WIB']/$tot['GRAND_TOTAL'])*100,2).'%'; ?></td>
										<td style="background:#6895a0;text-align:right; color:#ffffff"><?php echo round(($tot['TOTAL_DIGITAL']/$tot['GRAND_TOTAL'])*100,2).'%'; ?></td>
										<td style="background:#6895a0;text-align:right; color:#ffffff"><?php echo round(($tot['TOTAL_MOBILE']/$tot['GRAND_TOTAL'])*100,2).'%'; ?></td>
										<td style="background:#6895a0;text-align:right; color:#ffffff"><?php echo round(($tot['GRAND_TOTAL']/$tot['GRAND_TOTAL'])*100,2).'%'; ?></td>
									</tr>
								</tbody>
							</table>
						<!-- </div> -->
					</div>
				</div><!-- .tab-pane  -->
			</div><!-- .tab-content  -->
		</div><!-- .nav-tabs-horizontal -->
	</div>
</div>
</div>