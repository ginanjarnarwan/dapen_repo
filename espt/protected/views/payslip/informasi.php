<style>
	#parent {
		height: 500px;
	}
	
	#fixTable {
		width: 1800px !important;
	}
	.tabel-cost{
		width: 100%;
	}
	.tabel-cost td {
	    border: 1px solid #fff;
	    padding: 2px 3px;
	}
	tr.noborder{
		border:none !important;
	}
	td.noborder{
		border:none !important;
	}
	table.noborder{
		border:none !important;
	}
	.kiri{
		text-align: left;
	}
	.abu{
		background:#c4c4c4;
	}
	.abu2{
		background: #e0e0e0;
	}
	.merahmuda{
		background: #ffafaf;
	}
	.birumuda{
		background: #b1d3e0;
	}
	.ijo{
		background: #8af7bf;
	}
	.abutua{
		background: #9b9b9b;
	}
	.teksputih{
		color: #fff;
	}
	.merahmuda2{
		background: #fcc7c7;
	}
	.merahmuda3{
		background: #fce0e0;
	}
	.birumuda2{
		background: #c3dae2;
	}
	.birumuda3{
		background: #d7e7ed;
	}
</style>
<?php Yii::app()->clientScript->registerCssFile(Yii::app()->theme->baseUrl . '/dist/css/bots/bootstrap.css');?>
<div class="row">
<div class="col-md-12">
	<h4 class="m-b-lg">Informasi </h4>
</div>
<div class="col-md-12">
	<div class="widget">
		<div class="m-b-lg nav-tabs-horizontal">
			<div class="tab-content p-md">
				<div role="tabpanel" class="tab-pane in active fade" id="isi">
					<h4 class="m-b-md">Personnel Cost Allocation Summary</h4>
					<div style="text-align:right;margin-bottom:10px;">
						<!-- <a href="" class="btn btn-success"><i class="fa fa-file-pdf-o"></i> Export PDF</a> -->
						<br/>
						<br/>
						<table class="tabel-cost noborder">
							<tr>
								<td rowspan="2" class="noborder"></td>
								<td rowspan="2" class="noborder"></td>
								<td width="20%" rowspan="2" class="noborder"></td>
								<td rowspan="2" class="noborder"></td>
								<td rowspan="2" width="30%" style="text-align:center;" class="abutua teksputih" style="background: #9b9b9b;color:#ffffff"><b>Posisi</b></td>
								<td rowspan="2" class="noborder"></td>
								<td colspan="5" style="text-align:center;background: #8af7bf;" class="ijo"><b>Personnel Cost Allocation</b></td>
							</tr>
							<tr>
								<td class="ijo" style="background: #8af7bf;"><b>Consumer</b></td>
								<td class="ijo" style="background: #8af7bf;"><b>Enterprise</b></td>
								<td class="ijo" style="background: #8af7bf;"><b>WIB</b></td>
								<td class="ijo" style="background: #8af7bf;"><b>Digital</b></td>
								<td class="ijo" style="background: #8af7bf;"><b>Mobile</b></td>
								<!-- <td class="ijo" style="background: #8af7bf;"><b>Total</b></td> -->
							</tr>
							<tr>
								<td rowspan="14" align="middle" class="merahmuda" style="background: #ffafaf; vertical-align:middle;"><b>TREG</b></td>
								<td rowspan="2" class="noborder"></td>
								<td rowspan="2" class="kiri merahmuda" style="background: #ffafaf;"><b>SENIOR OVERHEAD</b></td>
								<td rowspan="2"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"></td>
								<td class="noborder"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"></td>
							</tr>
							<tr>
								<td class="merahmuda3" style="background: #fce0e0;text-align:left;">EVP, Deputy EVP, GM Witel, Deputy GM Witel, SM Business Planning & Performance, SM GA</td>
								<td class="noborder"></td>
								<td class="merahmuda3" style="background: #fce0e0;">25%</td>
								<td class="merahmuda3" style="background: #fce0e0;">25%</td>
								<td class="merahmuda3" style="background: #fce0e0;">25%</td>
								<td class="merahmuda3" style="background: #fce0e0;">0%</td>
								<td class="merahmuda3" style="background: #fce0e0;">25%</td>
							</tr>
							<tr>
								<td colspan="10" class="noborder"></td>
							</tr>
							<tr>
								<!-- <td rowspan="2"></td> -->
								<td rowspan="2" class="noborder"></td>
								<td rowspan="2" class="kiri merahmuda" style="background:#ffafaf"><b>OVERHEAD (NETWORK)</b></td>
								<td rowspan="2"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"></td>
								<td class="noborder"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"></td>
							</tr>
							<tr>
								<td class="merahmuda3" style="background: #fce0e0;text-align:left;">Seluruh karyawan di unit pengelola Network di Treg, Witel dan Datel</td>
								<td class="noborder"></td>
								<td class="merahmuda3" style="background: #fce0e0;">59%</td>
								<td class="merahmuda3" style="background: #fce0e0;">20%</td>
								<td class="merahmuda3" style="background: #fce0e0;">21%</td>
								<td class="merahmuda3" style="background: #fce0e0;">0%</td>
								<td class="merahmuda3" style="background: #fce0e0;">0%</td>
							</tr>
							<tr>
								<td colspan="10" class="noborder"></td>
							</tr><tr>
								<!-- <td rowspan="2"></td> -->
								<td rowspan="2" class="noborder"></td>
								<td rowspan="2" class="kiri merahmuda" style="background: #ffafaf;"><b>Overhead (Non-Network Bizz Supp) </b></td>
								<td rowspan="2"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"></td>
								<td class="noborder"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"></td>
							</tr>
							<tr>
								<td class="merahmuda3" style="background: #fce0e0;text-align:left;">Seluruh karyawan di unit Business Planning & Performance, Manager War Room Witel, Asman Suport Datel (kecuali SM BPP)</td>
								<td class="noborder"></td>
								<td class="merahmuda3" style="background: #fce0e0;">33%</td>
								<td class="merahmuda3" style="background: #fce0e0;">33%</td>
								<td class="merahmuda3" style="background: #fce0e0;">33%</td>
								<td class="merahmuda3" style="background: #fce0e0;">0%</td>
								<td class="merahmuda3" style="background: #fce0e0;">0%</td>
							</tr>
							<tr>
								<td colspan="11" class="noborder"></td>
							</tr><tr>
								<!-- <td rowspan="2"></td> -->
								<td rowspan="2" class="noborder"></td>
								<td rowspan="2" class="kiri merahmuda" style="background: #ffafaf;"><b>Overhead (Non-Network Others)</b></td>
								<td rowspan="2"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"></td>
								<td class="noborder"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"></td>
							</tr>
							<tr>
								<td class="merahmuda3" style="background: #fce0e0;text-align:left;">Seluruh karyawan di unit pengelola HC, Payment Collection & Finance, GA di Treg dan Witel (kecuali SM GA) </td>
								<td class="noborder"></td>
								<td class="merahmuda3" style="background: #fce0e0;">67%</td>
								<td class="merahmuda3" style="background: #fce0e0;">29%</td>
								<td class="merahmuda3" style="background: #fce0e0;">5%</td>
								<td class="merahmuda3" style="background: #fce0e0;">0%</td>
								<td class="merahmuda3" style="background: #fce0e0;">0%</td>
							</tr>
							<tr>
								<td colspan="11" class="noborder"></td>
							</tr><tr>
								<!-- <td rowspan="2"></td> -->
								<td rowspan="2" class="noborder"></td>
								<td rowspan="2" class="kiri merahmuda" style="background: #ffafaf;"><b>Non-Overhead Staff</b></td>
								<td rowspan="2"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"></td>
								<td class="noborder"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"></td>
								<td class="merahmuda2" style="background: #fcc7c7;"></td>
							</tr>
							<tr>
								<td class="merahmuda3" style="background: #fce0e0;text-align:left;">Seluruh karyawan di unit pengelola segmen Consumer, Enterprise dan Wholesale di Treg, Witel dan Datel</td>
								<td class="noborder"></td>
								<td class="merahmuda3" style="background: #fce0e0;">67%</td>
								<td class="merahmuda3" style="background: #fce0e0;">29%</td>
								<td class="merahmuda3" style="background: #fce0e0;">5%</td>
								<td class="merahmuda3" style="background: #fce0e0;">0%</td>
								<td class="merahmuda3" style="background: #fce0e0;">0%</td>
							</tr>
							
							<tr class="noborder">
								<td class="noborder"></td>
							</tr>
							<tr>
								<td rowspan="5" align="middle" class="birumuda" style="background: #b1d3e0; vertical-align:middle;"><b>NON TREG</b></td>
								<td rowspan="2" class="noborder"></td>
								<td rowspan="2" class="kiri birumuda" style="background: #b1d3e0;"><b>CFU</b></td>
								<td rowspan="2"></td>
								<td class="birumuda2" style="background: #c3dae2;"></td>
								<td class="noborder"></td>
								<td class="birumuda2" style="background: #c3dae2;"></td>
								<td class="birumuda2" style="background: #c3dae2;"></td>
								<td class="birumuda2" style="background: #c3dae2;"></td>
								<td class="birumuda2" style="background: #c3dae2;"></td>
								<td class="birumuda2" style="background: #c3dae2;"></td>
							</tr>
							<tr>
								<td class="birumuda3" style="background: #d7e7ed;text-align:left;">Seluruh karyawan di Direktorat CFU dan Divisi terkait</td>
								<td class="noborder"></td>
								<td colspan="5" class="birumuda3" style="background: #d7e7ed;text-align:center;">Sesuai dengan CFU terkait</td>
								<!-- <td class="birumuda3" style="background: #d7e7ed;">0%</td>
								<td class="birumuda3" style="background: #d7e7ed;">0%</td>
								<td class="birumuda3" style="background: #d7e7ed;">0%</td>
								<td class="birumuda3" style="background: #d7e7ed;">0%</td>
								<td class="birumuda3" style="background: #d7e7ed;">100%</td> -->
							</tr>
							<tr>
								<td colspan="11" class="noborder"></td>
							</tr>
							<tr>
								<!-- <td rowspan="2"></td> -->
								<td rowspan="2" class="noborder"></td>
								<td rowspan="2" class="kiri birumuda" style="background: #b1d3e0;"><b>FU</b></td>
								<td rowspan="2"></td>
								<td class="birumuda2" style="background: #c3dae2;"></td>
								<td class="noborder"></td>
								<td class="birumuda2" style="background: #c3dae2;"></td>
								<td class="birumuda2" style="background: #c3dae2;"></td>
								<td class="birumuda2" style="background: #c3dae2;"></td>
								<td class="birumuda2" style="background: #c3dae2;"></td>
								<td class="birumuda2" style="background: #c3dae2;"></td>
							</tr>
							<tr>
								<td class="birumuda3" style="background: #d7e7ed;text-align:left;">Seluruh karyawan di Direktorat FU dan Divisi/Center terkait</td>
								<td class="noborder"></td>
								<td class="birumuda3" style="background: #d7e7ed;">37%</td>
								<td class="birumuda3" style="background: #d7e7ed;">28%</td>
								<td class="birumuda3" style="background: #d7e7ed;">21%</td>
								<td class="birumuda3" style="background: #d7e7ed;">0%</td>
								<td class="birumuda3" style="background: #d7e7ed;">14%</td>
							</tr>
							<tr class="noborder">
								<td colspan="12" class="noborder"></td>
							</tr>
							<!-- <tr>
								<td rowspan="2" class="noborder"></td>
								<td rowspan="2" class="noborder"></td>
								<td rowspan="2" class="kiri abu" style="background:#c4c4c4;"><b>TOTAL</b></td>
								<td rowspan="2"></td>
								<td class="abu" style="background:#c4c4c4;"></td>
								<td class="noborder"></td>
								<td class="abu" style="background:#c4c4c4;"></td>
								<td class="abu" style="background:#c4c4c4;"></td>
								<td class="abu" style="background:#c4c4c4;"></td>
								<td class="abu" style="background:#c4c4c4;"></td>
								<td class="abu" style="background:#c4c4c4;"></td>
								<td class="abu" style="background:#c4c4c4;"><b></b></td>
							</tr>
							<tr>
								<td class="abu2" style="background: #e0e0e0;"></td>
								<td class="noborder"></td>
								<td class="abu2" style="background: #e0e0e0;"></td>
								<td class="abu2" style="background: #e0e0e0;"></td>
								<td class="abu2" style="background: #e0e0e0;"></td>
								<td class="abu2" style="background: #e0e0e0;"></td>
								<td class="abu2" style="background: #e0e0e0;"></td>
								<td class="abu2" style="background: #e0e0e0;"></td>
							</tr> -->
						</table>
					</div>
				</div><!-- .tab-pane  -->
			</div><!-- .tab-content  -->
		</div><!-- .nav-tabs-horizontal -->
	</div>
</div>
</div>