<script type="text/javascript">
    $(document).ready(function() {
      $("#tombsampel").click(function(e) {
        e.preventDefault();

        //getting data from our table
        var data_type = 'data:application/vnd.ms-excel';
        var table_div = document.getElementById('sampel');
        var table_html = table_div.outerHTML.replace(/ /g, '%20');

        var a = document.createElement('a');
        a.href = data_type + ', ' + table_html;
        a.download = 'sampel_upload_atk.xls';
        a.click();
      });
    });
</script>
<div class="col-md-12">
    <div class="widget">
        <header class="widget-header">
            <h4 class="widget-title">Upload Master Segmen TREG</h4>
        </header><!-- .widget-header -->
        <hr class="widget-separator">
        <div class="widget-body">
            <div class="m-b-xl">
                <h4 class="m-b-md"></h4>
                <?php $form = $this->beginWidget('CActiveForm', array(
                    'id'=>"form-upload",
                    // 'action' => Yii::app()->createUrl('beasiswaPerson/generatePerson'),
                    'htmlOptions' => array(
                        'enctype' => 'multipart/form-data',
                        'class' => 'form-horizontal'
                    )
                )); ?>
                    <div class="form-group <?=(strlen($form->error($model,'fileupload')) > 0 ? 'has-error' : '' )?>">
                        <?php echo $form->labelEx($model, 'fileupload', array('class' => 'control-label col-md-3')); ?>
                        <div class="col-md-9">
                            <?php echo $form->fileField($model, 'fileupload', array('class'=>'form-control input-sm')); ?>
                            <span class="help-block">(File berupa csv) contoh file >> <a href="#" id="tombsampel"><u>klik di sini</u></a></span>
                        </div>
                    </div>
                            <?php echo $form->error($model,'upload'); ?>
                    <center>
                    <?php echo CHtml::htmlButton('<i class="fa fa-upload"></i> ' . Yii::t('mds', 'Upload File'),array(
                        'class'=>'btn btn-primary btn-sm subm',
                        'type'=>'submit'
                    )); ?>
                    </center>
                <?php $this->endWidget(); ?>
            </div>
        </div><!-- .widget-body -->
    </div><!-- .widget -->
</div>
<div style="display:none;">
    <table class="print" id="sampel">
        <tr>
            <td><b>NIK</b></td>
            <td><b>Nama</b></td>
            <td><b>Objid Pos</b></td>
            <td><b>Kode Divisi</b></td>
            <td><b>Divisi</b></td>
            <td><b>Posisi</b></td>
            <td><b>Unit</b></td>
            <td><b>Final Segmentasi</b></td>
        </tr>
    </table>
</div>
<script>
$('.subm').click(function(e){
    tomb = $(this);
    e.preventDefault();
    swal({
      title: "Apakah anda yakin akan mengupload?",
      // text: "You will not be able to recover this imaginary file!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#10c469",
      confirmButtonText: "Yes",
      closeOnConfirm: false,
      html: false
    }, function(){
      swal({
        title : "<i class='fa fa-spinner fa-pulse fa-3x fa-fw'></i>",
        html : "<i class='fa fa-spinner fa-pulse fa-3x fa-fw'></i>",
        showCancelButton: false,
        showConfirmButton: false
      });
      $('#form-upload').submit();
    });
});
</script>