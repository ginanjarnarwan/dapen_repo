<div class="col-md-12">
    <div class="widget">
        <header class="widget-header">
            <h4 class="widget-title">Upload HR Current</h4>
        </header><!-- .widget-header -->
        <hr class="widget-separator">
        <div class="widget-body">
            <div class="m-b-xl">
                <h4 class="m-b-md"></h4>
                <?php $form = $this->beginWidget('CActiveForm', array(
                    'id'=>"form-upload",
                    // 'action' => Yii::app()->createUrl('beasiswaPerson/generatePerson'),
                    'htmlOptions' => array(
                        'enctype' => 'multipart/form-data',
                        'class' => 'form-horizontal'
                    )
                )); ?>
                    <div class="form-group">
                        <?php echo $form->labelEx($model, 'TAHUN', array('class'=>'control-label col-md-3')); ?>
                        <div class="col-md-9">
                            <?php echo $form->dropDownList($model, 'TAHUN', array('2017'=>'2017', '2018'=>'2018', '2019'=>'2019', '2020'=>'2020', '2021'=>'2021', '2022'=>'2022'), array('prompt'=>'Pilih Tahun','class'=>'form-control','options'=>array($tahun=>array('selected'=>true))));
                            ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <?php echo $form->labelEx($model, 'BULAN', array('class'=>'control-label col-md-3')); ?>
                        <div class="col-md-9">
                            <?php echo $form->dropDownList($model, 'BULAN', array('1'=>'JANUARI', '2'=>'FEBRUARI', '3'=>'MARET', '4'=>'APRIL', '5'=>'MEI', '6'=>'JUNI', '7'=>'JULI', '8'=>'AGUSTUS', '9'=>'SEPTEMBER', '10'=>'OKTOBER', '11'=>'NOVEMBER', '12'=>'DESEMBER'), array('prompt'=>'Pilih Bulan','class'=>'form-control','options'=>array($bulan=>array('selected'=>true))));
                            ?>
                        </div>
                    </div>
                    <div class="form-group <?=(strlen($form->error($model,'fileupload')) > 0 ? 'has-error' : '' )?>">
                        <?php echo $form->labelEx($model, 'fileupload', array('class' => 'control-label col-md-3')); ?>
                        <div class="col-md-9">
                            <?php echo $form->fileField($model, 'fileupload', array('class'=>'form-control input-sm')); ?>
                            <!-- <span class="help-block">(File berupa excel) >> <?php echo CHtml::link("Sample format", Yii::app()->request->baseUrl . "/document/download/upload_peserta_lulus.xls", array("target"=>"_blank")); ?></span> -->
                            <?php echo $form->error($model,'upload'); ?>
                        </div>
                    </div>
                    <center>
                    <?php echo CHtml::htmlButton('<i class="fa fa-upload"></i> ' . Yii::t('mds', 'Upload File'),array(
                        'class'=>'btn btn-primary btn-sm subm',
                        'type'=>'submit'
                    )); ?>
                    </center>
                <?php $this->endWidget(); ?>
            </div>
        </div><!-- .widget-body -->
    </div><!-- .widget -->
</div>
<script>
$('.subm').click(function(e){
    tomb = $(this);
    e.preventDefault();
    swal({
      title: "Apakah anda yakin akan mengupload?",
      // text: "You will not be able to recover this imaginary file!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#10c469",
      confirmButtonText: "Yes",
      closeOnConfirm: false,
      html: false
    }, function(){
      swal({
        title : "<i class='fa fa-spinner fa-pulse fa-3x fa-fw'></i>",
        html : "<i class='fa fa-spinner fa-pulse fa-3x fa-fw'></i>",
        showCancelButton: false,
        showConfirmButton: false
      });
      $('#form-upload').submit();
    });
});
</script>