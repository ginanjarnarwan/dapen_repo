<?php
/* @var $this TRealsapController */
/* @var $model TRealsap */
/* @var $form CActiveForm */
?>

<div class="box-body">

    <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'trealsap-form',
	'enableAjaxValidation'=>false,
        'htmlOptions'=>array('class'=>'form-horizontal')
)); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

            <div class="form-group">
            <?php echo $form->labelEx($model,'TAHUN', array('class'=>'col-sm-2 control-label')); ?>
            <div class="col-md-10">
                <?php echo $form->dropDownList($model, 'TAHUN', array('2017'=>'2017', '2018'=>'2018', '2019'=>'2019', '2020'=>'2020', '2021'=>'2021', '2022'=>'2022'), array('prompt'=>'Pilih Tahun','class'=>'form-control')); ?>
                <?php echo $form->error($model,'TAHUN', array('class'=>'alert alert-danger')); ?>
            </div>
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'BULAN', array('class'=>'col-sm-2 control-label')); ?>
            <div class="col-md-10">
                <?php echo $form->dropdownlist($model, 'BULAN',array('1'=>'Januari','2'=>'Februari','3'=>'Maret','4'=>'April','5'=>'Mei','6'=>'Juni','7'=>'Juli','8'=>'Agustus','9'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember'), array('class' => 'form-control','prompt'=>'Pilih Bulan')); ?>
                <?php echo $form->error($model,'BULAN', array('class'=>'alert alert-danger')); ?>
            </div>
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'REALISASI', array('class'=>'col-sm-2 control-label')); ?>
            <div class="col-md-10">
                <?php echo $form->textField($model, 'REALISASI', array('class' => 'form-control uang')); ?>
                <?php echo $form->error($model,'REALISASI', array('class'=>'alert alert-danger')); ?>
            </div>
        </div>

        </div>

<div class="box-footer">
    <?php echo CHtml::link('Cancel', Yii::app()->controller->createUrl('index'), array('class' => 'btn btn-sm btn-default')); ?> 
    <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-sm btn-info pull-right')); ?> 
</div>

<?php $this->endWidget(); ?>
