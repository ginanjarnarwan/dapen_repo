<?php
/* @var $this TRealsapController */
/* @var $model TRealsap */

$this->breadcrumbs=array(
	'Trealsaps'=>array('index'),
	'Manage',
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$('#trealsap-grid').yiiGridView('update', {
data: $(this).serialize()
});
return false;
});
");
?>
<div class="row">
<div class="col-md-12">
	<div class="widget p-lg">
		<h4 class="m-b-lg">Manage Realisasi SAP</h4>
		<div class="pull-right">
			<?php echo CHtml::link("Create", Yii::app()->controller->createUrl("create"), array("class" => "btn btn-sm btn-primary")); ?> 
		</div>
		<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn btn-outline btn-info')); ?> 
		<div class="search-form" style="display:none">
			<div class="box box-solid box-primary">
				<div class="box-body">
					<?php $this->renderPartial('_search',array(
						'model'=>$model,
					)); ?>
				</div>
			</div>
		</div><!-- search-form -->
		<?php $this->widget('zii.widgets.grid.CGridView', array(
				'id'=>'trealsap-grid',
				'dataProvider'=>$model->search(),
				// 'filter'=>false,
				'enableSorting' => false,
				'itemsCssClass' => 'table table-bordered table-striped',
				'pagerCssClass'=>'paging',
				'pager'=>array(
					'class'=>'CLinkPager',
					'header'=>'',
					'selectedPageCssClass'=>'active',
					'htmlOptions'=>array(
						'class'=>'pagination pagination-sm'
					)
				),
				'columns'=>array(
		'TAHUN',
		array(
			'name'=> 'BULAN',
			'value'=>'MyApp::convertBulan($data->BULAN)',
		),
		array(
			'name'=>'REALISASI',
			'value'=>'MyApp::buatuang($data->REALISASI)',
			'htmlOptions'=>array(
				'style'=>'text-align:right',
				),
		),
				array(
				'class'=>'CButtonColumn',
				'template' => '{view}{update}{deletes}',
				'buttons' => array(
				'view' => array(
				'label' => '<i class="fa fa-external-link"></i>',
				'imageUrl' => FALSE,
				'options' => array('class' => 'btn btn-primary btn-condensed btn-sm')
				),
				'update' => array(
				'label' => '<i class="fa fa-pencil"></i>',
				'imageUrl' => FALSE,
				'options' => array('class' => 'btn btn-warning btn-condensed btn-sm')
				),
				'deletes' => array(
					'url' => 'Yii::app()->controller->createUrl("deletes",array("id"=>$data->ID))',
					'label' => '<i class="fa fa-trash"></i>',
					'imageUrl' => FALSE,
					'options' => array('class' => 'btn btn-danger btn-condensed btn-sm del-btn')
				),
				),
				'htmlOptions' => array('style' => 'width:130px;text-align:center;')
				),
				),
				)); ?>
	</div><!-- .widget -->
</div>
</div>
