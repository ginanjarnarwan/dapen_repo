<?php
/* @var $this TRealsapController */
/* @var $model TRealsap */

$this->breadcrumbs=array(
	'Trealsaps'=>array('index'),
	$model->ID,
);

$this->menu=array(
	array('label'=>'List TRealsap', 'url'=>array('index')),
	array('label'=>'Create TRealsap', 'url'=>array('create')),
	array('label'=>'Update TRealsap', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete TRealsap', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage TRealsap', 'url'=>array('admin')),
);
?>
<div class="row">
	<div class="col-sm-12">
		<div class="box box-info">
			<div class="box-header with-border">
				<h3 class="box-title">View TRealsap #<?php echo $model->ID; ?></h3>
			</div>
			<?php $this->widget('zii.widgets.CDetailView', array(
			'data'=>$model,
			'attributes'=>array(
					'ID',
		'TAHUN',
		'BULAN',
		'REALISASI',
			),
			)); ?>
		</div>
	</div>
</div>
