<?php
/* @var $this TRealsapController */
/* @var $model TRealsap */

$this->breadcrumbs=array(
	'Trealsaps'=>array('index'),
	$model->ID=>array('view','id'=>$model->ID),
	'Update',
);
?>
<div class="row">
	<div class="col-md-12">
		<div class="widget p-lg">
			<h3 class="m-b-lg">Update Realisasi</h3>
			<?php $this->renderPartial('_form', array('model'=>$model)); ?>		</div>
	</div>
</div>
