<?php
/* @var $this TRealsapController */
/* @var $model TRealsap */
/* @var $form CActiveForm */
?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'htmlOptions'=>array('class'=>'form-horizontal')
)); ?>

<div class="form-group">
	<?php echo $form->labelEx($model,'ID', array('class'=>'col-sm-2 control-label')); ?>
	<div class="col-sm-10">
		<?php echo $form->textField($model,'ID',array('class' => 'form-control')); ?>
	</div>
</div>
<div class="form-group">
	<?php echo $form->labelEx($model,'TAHUN', array('class'=>'col-sm-2 control-label')); ?>
	<div class="col-sm-10">
		<?php echo $form->dropDownList($model, 'TAHUN', array('2017'=>'2017', '2018'=>'2018', '2019'=>'2019', '2020'=>'2020', '2021'=>'2021', '2022'=>'2022'), array('prompt'=>'Pilih Tahun','class'=>'form-control')); ?>
	</div>
</div>
<div class="form-group">
	<?php echo $form->labelEx($model,'BULAN', array('class'=>'col-sm-2 control-label')); ?>
	<div class="col-sm-10">
		<?php echo $form->dropdownlist($model, 'BULAN',array('1'=>'Januari','2'=>'Februari','3'=>'Maret','4'=>'April','5'=>'Mei','6'=>'Juni','7'=>'Juli','8'=>'Agustus','9'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember'), array('class' => 'form-control','prompt'=>'Pilih Bulan')); ?>
	</div>
</div>
<div class="form-group">
	<?php echo $form->labelEx($model,'REALISASI', array('class'=>'col-sm-2 control-label')); ?>
	<div class="col-sm-10">
		<?php echo $form->textField($model,'REALISASI',array('class' => 'form-control', 'size'=>60,'maxlength'=>60)); ?>
	</div>
</div>
	<div class="row">
		<div class="col-sm-12">
			<?php echo CHtml::submitButton('Search', array(
				'class'=>'btn btn-sm btn-primary'
			)); ?>
		</div>
	</div>
<?php $this->endWidget(); ?>
