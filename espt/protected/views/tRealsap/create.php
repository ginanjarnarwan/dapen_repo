<?php
/* @var $this TRealsapController */
/* @var $model TRealsap */

$this->breadcrumbs=array(
	'Trealsaps'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List TRealsap', 'url'=>array('index')),
array('label'=>'Manage TRealsap', 'url'=>array('admin')),
);
?>
<div class="row">
	<div class="col-md-12">
		<div class="widget p-lg">
			<h3 class="m-b-lg">Create Realisasi</h3>
			<?php $this->renderPartial('_form', array('model'=>$model)); ?>	
		</div>
	</div>
</div>
