<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="<?php echo Yii::app()->baseUrl;?>/style/default.css" rel="stylesheet" type="text/css">

<title>1770 S-II (LAMPIRAN II)</title>
</head>
	
<style>
.bgabu {
    background:#999 !important;
}
@media print {
.bgabu {
    background:#999 !important;
    -webkit-print-color-adjust: exact; 
}}
.table-detail { width:100%; border:1px solid #000; padding:5px; border-collapse:collapse; }
.table-detail th, .table-detail td { border:1px solid #000; padding:2px; }
.table-detail .index { text-align:center; }
.table-detail .value { text-align:right; }
.pad-kecil tr td {
    padding: 0px;
}
table td{word-wrap:break-word;}
body{
    margin: 0px;
    font-family: "Lato", "Helvetica Neue", Helvetica, Arial, sans-serif !important;
}
.btn-success {
    color: #fff;
    background-color: #5cb85c;
    border-color: #4cae4c;
}
.btn {
    display: inline-block;
    margin-bottom: 0;
    font-weight: normal;
    text-align: center;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    background-image: none;
    border: 1px solid transparent;
    white-space: nowrap;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    border-radius: 4px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}
.btn-success:active:hover, .btn-success.active:hover, .open>.dropdown-toggle.btn-success:hover, .btn-success:active:focus, .btn-success.active:focus, .open>.dropdown-toggle.btn-success:focus, .btn-success:active.focus, .btn-success.active.focus, .open>.dropdown-toggle.btn-success.focus {
    color: #fff;
    background-color: #398439;
    border-color: #255625;
}
.btn-success:hover {
    color: #fff;
    background-color: #449d44;
    border-color: #398439;
}
</style>
<body>
<center><button id="btnExportpdf" class="btn btn-outline btn-success" onClick="this.style.display='none';window.print();this.style.display='';"><i class="fa fa-file-pdf-o"></i> Print Pdf</button></center>
<form name="spt" id="id_spt" method="post">
<table border="0" cellpadding="0" cellspacing="0" class="normal_teks" width="100%" height="100%" style="margin-left:auto;margin-right:auto;">
	<tr>
		<td><img src="<?php echo Yii::app()->baseUrl;?>/images/kotak.jpg"></td>
		<td></td>
		<td><img src="<?php echo Yii::app()->baseUrl;?>/images/kotak.jpg"></td>
	</tr>
	<tr>
		<td></td>
		<td height="931" valign="top">
		<table border="0" cellpadding="0" cellspacing="0" class="normal_teks" style="margin-top:0px;">
			<tr>
				<td colspan="2" style="font-size:14px;border:1px solid #000000; border-left: none; border-top: none; text-align:center; width:20%;" valign="top">
                <p style="font-weight: 100px;font-size: 30px;margin-bottom: 0px;">1770 S - II</p>
                <!-- <img src="<?php echo Yii::app()->baseUrl;?>/images/logoPajak.jpg"> -->
				  <b>KEMENTERIAN KEUANGAN RI<br />
        DIREKTORAT JENDERAL PAJAK</b></td>
				<td width="23" style="border:1px solid #000; width:55%; border-top:none;">
                <div style="text-align:center; font-size:20px; font-weight:bold;">
                	LAMPIRAN - II
                </div>
                <div style="text-align:center; font-size:16px; font-weight:bold;">
                    PAJAK PENGHASILAN WAJIB PAJAK ORANG PRIBADI
                </div>
                <div style="border-top:2px solid #000; text-align:left; margin-top: 5px; font-size: 11px; font-weight:500; padding:8px 10px 6px 10px;">
                    <ul style="margin-top: 0px;margin-bottom: 0px;">
                        <li>PENGHASILAN YANG DIKENAKAN PPh FINAL DAN/ATAU BERSIFAT FINAL</li>
                        <li>HARTA PADA AKHIR TAHUN</li>
                        <li>KEWAJIBAN/UTANG PADA AKHIR TAHUN</li>
                        <li>DAFTAR SUSUNAN ANGGOTA KELUARGA</li>
                    </ul>
                </div>
                </td>
				<td valign="top" style="width:20%; border:1px solid #000; border-top:none; border-right:none; padding-left:10px; font-size: 11px;">
                        <p style="font-weight: 100px;font-size: 30px;margin-bottom: 0px;text-align: center;">2017</p>
                </td>
			</tr>
            <tr>
            	<td colspan="6" style="padding-bottom:5px; padding-top:10px; border-top:1px solid #000; font-weight:bold; font-size:14px;">IDENTITAS</td>
            </tr>
            <tr>
            <td colspan="6">
            <table style="width:100%; border:1px solid #000;">
            <tr>
            <td style="width:50%;">
            	<table style="font-size:9px; width:100%;">
                <tr>
                    <td style="width:2px"></td>
                    <td style="width:147px">NPWP</td>
                    <td style="width:2px"> : </td>
                    <td colspan="6" style="border-bottom: 1px solid #000;width:240px;"><?php echo $final[0]['npwp']; ?></td>
                </tr>
                <tr>
                    <td></td>
                    <td>NAMA WAJIB PAJAK</td>
                    <td> : </td>
                    <td colspan="6" style="border-bottom: 1px solid #000;"><?php echo $final[0]['nama']; ?></td>
                </tr>
                </table>
            </td>
            </tr>
            </table>
            </td>
            </tr>
            <tr>
                <td colspan="6" style="padding-bottom:5px; padding-top:10px; border-top:1px solid #000; font-weight:bold; font-size:14px;">BAGIAN A : PENGHASILAN YANG DIKENAKAN PPh FINAL DAN/ATAU BERSIFAT FINAL</td>
            </tr>
            <tr>
                <td colspan="6">
                    <table class="normal_teks table-detail" style="font-size: 11px;">
                        <tr>
                            <th width="12px">NO</th>
                            <th>JENIS PENGHASILAN</th>
                            <th colspan="2">DASAR PENGENAAN PAJAK/ <br>PENGHASILAN BRUTO</th>
                            <th>PPh TERUTANG (Rp)</th>
                        </tr>
                        <tr style="font-size: 9px;">
                            <td align="center">(1)</td>
                            <td align="center">(2)</td>
                            <td colspan="2" align="center">(3)</td>
                            <td align="center">(4)</td>
                        </tr>
                        <tr>
                            <td class="index">1.</td>
                            <td>Bunga Deposito, Tabungan, Diskonto SBI, SURAT BERHARGA NEGARA</td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">2.</td>
                            <td>Bunga/Diskonto Obligasi </td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">3.</td>
                            <td>Penjualan Saham di Bursa Efek</td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">4.</td>
                            <td>Hadiah Undian</td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">5.</td>
                            <td>Pesangon, Tunjangan Hari Tua dan Tebusan Pensiun, yang dibayarkan sekaligus</td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">6.</td>
                            <td>Honorarium atas Beban APBN/APBD</td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">7.</td>
                            <td>Pengalihan Hak atas Tanah dan/atau Bangunan</td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">8.</td>
                            <td>Sewa atas Tanah dan/atau Bangunan</td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">9.</td>
                            <td>Bangunan yang Diterima Dalam Rangka Bangun guna serah</td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">10.</td>
                            <td>Bunga Simpanan Yang Dibayarkan Oleh Koperasi kepada anggota Koperasi</td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">11.</td>
                            <td>Penghasilan Dari Transaksi Derivatif</td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">12.</td>
                            <td>Deviden</td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">13.</td>
                            <td>Penghasilan Isteri Dari Satu Pemberi Kerja</td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">14.</td>
                            <td>Penghasilan Lain yang dikenakan Pajak Final dan/atau bersifat Final</td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                        </tr>
                        
                        <tr>
                            <td class="index"></td>
                            <td>JUMLAH BAGIAN A</td>
                            <td class="bgabu"></td>
                            <td>JBA</td>
                            <td class="value"></td>
                        </tr>
                    </table>
                </td>    
            </tr>
			
            <tr>
                <td colspan="6" style="padding-bottom:5px; padding-top:10px; border-top:1px solid #000; font-weight:bold; font-size:14px;">BAGIAN B : HARTA PADA AKHIR TAHUN</td>
            </tr>
            <tr>
                <td colspan="6">
                    <table class="normal_teks table-detail" style="font-size: 11px;word-wrap: break-word;">
                        <tr>
                            <th width="12px">NO</th>
                            <th>JENIS HARTA</th>
                            <th colspan="2">TAHUN <br>PEROLEHAN</th>
                            <th>HARGA PEROLEHAN (Rp)</th>
                            <th>KETERANGAN</th>
                        </tr>
                        <tr style="font-size: 9px;">
                            <td align="center">(1)</td>
                            <td align="center">(2)</td>
                            <td colspan="2" align="center">(3)</td>
                            <td align="center">(4)</td>
                            <td align="center">(4)</td>
                        </tr>
                        <tr>
                            <td class="index">1.</td>
                            <td></td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="index">2.</td>
                            <td></td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="index">3.</td>
                            <td></td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="index">4.</td>
                            <td></td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="index">5.</td>
                            <td></td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="index">6.</td>
                            <td></td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="index">7.</td>
                            <td></td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="index"></td>
                            <td>JUMLAH BAGIAN B</td>
                            <td class="bgabu"></td>
                            <td>JBB</td>
                            <td class="value"></td>
                            <td></td>
                        </tr>
                    </table>
                </td>    
            </tr>

            <tr>
                <td colspan="6" style="padding-bottom:5px; padding-top:10px; border-top:1px solid #000; font-weight:bold; font-size:14px;">BAGIAN C : KEWAJIBAN/UTANG PADA AKHIR TAHUN</td>
            </tr>
            <tr>
                <td colspan="6">
                    <table class="normal_teks table-detail" style="font-size: 11px;">
                        <tr>
                            <th>NO</th>
                            <th>NAMA <br>PEMBERI PINJAMAN</th>
                            <th>ALAMAT <br>PEMBERI PINJAMAN</th>
                            <th colspan="2">TAHUN <br> PEMINJAMAN</th>
                            <th>JUMLAH</th>
                        </tr>
                        <tr style="font-size: 9px;">
                            <td align="center">(1)</td>
                            <td align="center">(2)</td>
                            <td align="center">(3)</td>
                            <td align="center" colspan="2">(4)</td>
                            <td align="center">(5)</td>
                        </tr>
                        <tr>
                            <td class="index">1.</td>
                            <td></td>
                            <td></td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">2.</td>
                            <td></td>
                            <td></td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">3.</td>
                            <td></td>
                            <td></td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">4.</td>
                            <td></td>
                            <td></td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">5.</td>
                            <td></td>
                            <td></td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">6.</td>
                            <td></td>
                            <td></td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">dst.</td>
                            <td></td>
                            <td></td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index"></td>
                            <td colspan="2">JUMLAH BAGIAN C</td>
                            <td style="background:#999;" class="bgabu"></td>
                            <td>JBC</td>
                            <td class="value"></td>
                        </tr>
                    </table>
                </td>    
            </tr>

            <tr>
                <td colspan="6" style="padding-bottom:5px; padding-top:10px; border-top:1px solid #000; font-weight:bold; font-size:14px;">BAGIAN D : DAFTAR SUSUNAN ANGGOTA KELUARGA</td>
            </tr>
            <tr>
                <td colspan="6">
                    <table class="normal_teks table-detail" style="font-size: 11px;">
                        <tr>
                            <th>NO</th>
                            <th>NAMA</th>
                            <th>TANGGAL LAHIR</th>
                            <th>HUBUNGAN KELUARGA</th>
                            <th>KETERANGAN</th>
                        </tr>
                        <tr style="font-size: 9px;">
                            <td align="center">(1)</td>
                            <td align="center">(2)</td>
                            <td align="center">(3)</td>
                            <td align="center">(4)</td>
                            <td align="center">(5)</td>
                        </tr>
                        <tr>
                            <td class="index">1.</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="index">2.</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="index">3.</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="index">4.</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="index">5.</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>
                </td>    
            </tr>
            
		</table>
		</td>
	</tr>
	<tr>
		<td><img src="<?php echo Yii::app()->baseUrl;?>/images/kotak.jpg"></td>
		<td></td>
		<td><img src="<?php echo Yii::app()->baseUrl;?>/images/kotak.jpg"></td>
	</tr>
</table>
</form>
</body>
</html>