<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="<?php echo Yii::app()->baseUrl;?>/style/default.css" rel="stylesheet" type="text/css">

<title>1770 S-I (LAMPIRAN I)</title>
</head>
	
<style>
@media print {
.bgabu {
    background:#999 !important;
    -webkit-print-color-adjust: exact; 
}}
.table-detail { width:100%; border:1px solid #000; padding:5px; border-collapse:collapse; }
.table-detail th, .table-detail td { border:1px solid #000; padding:2px; }
.table-detail .index { text-align:center; }
.table-detail .value { text-align:right; }
.pad-kecil tr td {
    padding: 0px;
}
body{
    margin: 0px;
    font-family: "Lato", "Helvetica Neue", Helvetica, Arial, sans-serif !important;
}
.btn-success {
    color: #fff;
    background-color: #5cb85c;
    border-color: #4cae4c;
}
.btn {
    display: inline-block;
    margin-bottom: 0;
    font-weight: normal;
    text-align: center;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    background-image: none;
    border: 1px solid transparent;
    white-space: nowrap;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    border-radius: 4px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}
.btn-success:active:hover, .btn-success.active:hover, .open>.dropdown-toggle.btn-success:hover, .btn-success:active:focus, .btn-success.active:focus, .open>.dropdown-toggle.btn-success:focus, .btn-success:active.focus, .btn-success.active.focus, .open>.dropdown-toggle.btn-success.focus {
    color: #fff;
    background-color: #398439;
    border-color: #255625;
}
.btn-success:hover {
    color: #fff;
    background-color: #449d44;
    border-color: #398439;
}
</style>
<body>
<center><button id="btnExportpdf" class="btn btn-outline btn-success" onClick="this.style.display='none';window.print();this.style.display='';"><i class="fa fa-file-pdf-o"></i> Print Pdf</button></center>
<form name="spt" id="id_spt" method="post">
<table border="0" cellpadding="0" cellspacing="0" class="normal_teks" width="100%" height="100%" style="margin-left:auto;margin-right:auto;">
	<tr>
		<td><img src="<?php echo Yii::app()->baseUrl;?>/images/kotak.jpg"></td>
		<td></td>
		<td><img src="<?php echo Yii::app()->baseUrl;?>/images/kotak.jpg"></td>
	</tr>
	<tr>
		<td></td>
		<td height="931" valign="top">
		<table border="0" cellpadding="0" cellspacing="0" class="normal_teks" style="margin-top:0px;">
			<tr>
				<td colspan="2" style="font-size:14px;border:1px solid #000000; border-left: none; border-top: none; text-align:center; width:20%;" valign="top">
                <p style="font-weight: 100px;font-size: 30px;margin-bottom: 0px;">1770 S - I</p>
                <!-- <img src="<?php echo Yii::app()->baseUrl;?>/images/logoPajak.jpg"> -->
				  <b>KEMENTERIAN KEUANGAN RI<br />
        DIREKTORAT JENDERAL PAJAK</b></td>
				<td width="23" style="border:1px solid #000; width:55%; border-top:none;">
                <div style="text-align:center; font-size:20px; font-weight:bold;">
                	LAMPIRAN - I
                </div>
                <div style="text-align:center; font-size:16px; font-weight:bold;">
                    PAJAK PENGHASILAN WAJIB PAJAK ORANG PRIBADI
                </div>
                <div style="border-top:2px solid #000; text-align:left; margin-top: 5px; font-size: 11px; font-weight:500; padding:8px 10px 6px 10px;">
                    <ul style="margin-top: 0px;margin-bottom: 0px;">
                        <li>PENGHASILAN NETO DALAM NEGERI LAINNYA</li>
                        <li>PENGHASILAN YANG TIDAK TERMASUK OBJEK PAJAK</li>
                        <li>DAFTAR PEMOTONGAN/PEMUNGUTAN PPh OLEH PIHAK LAIN DAN PPh YANG DITANGGUNG PEMERINTAH</li>
                    </ul>
                </div>
                </td>
				<td valign="top" style="width:20%; border:1px solid #000; border-top:none; border-right:none; padding-left:10px; font-size: 11px;">
                        <p style="font-weight: 100px;font-size: 30px;margin-bottom: 0px;text-align: center;">2017</p>
                </td>
			</tr>
            <tr>
            	<td colspan="6" style="padding-bottom:5px; padding-top:10px; border-top:1px solid #000; font-weight:bold; font-size:14px;">IDENTITAS</td>
            </tr>
            <tr>
            <td colspan="6">
            <table style="width:100%; border:1px solid #000;">
            <tr>
            <td style="width:50%;">
            	<table style="font-size:9px; width:100%;">
                <tr>
                    <td style="width:2px"></td>
                    <td style="width:147px">NPWP</td>
                    <td style="width:2px"> : </td>
                    <td colspan="6" style="border-bottom: 1px solid #000;width:240px;"><?php echo $final[0]['npwp']; ?></td>
                </tr>
                <tr>
                    <td></td>
                    <td>NAMA</td>
                    <td> : </td>
                    <td colspan="6" style="border-bottom: 1px solid #000;"><?php echo $final[0]['nama']; ?></td>
                </tr>
                </table>
            </td>
            </tr>
            </table>
            </td>
            </tr>
            <tr>
                <td colspan="6" style="padding-bottom:5px; padding-top:10px; border-top:1px solid #000; font-weight:bold; font-size:14px;">BAGIAN A : PENGHASILAN NETO DALAM NEGERI LAINNYA<br>(Tidak Termasuk Penghasilan Dikenakan PPh Final dan/atau Bersifat Final)</td>
            </tr>
            <tr>
                <td colspan="6">
                    <table class="normal_teks table-detail" style="font-size: 11px;">
                        <tr>
                            <th width="12px">NO</th>
                            <th width="500px" colspan="2">JENIS PENGHASILAN</th>
                            <th>JUMLAH PENGHASILAN (Rp)</th>
                        </tr>
                        <tr style="font-size: 9px;">
                            <td align="center">(1)</td>
                            <td colspan="2" align="center">(2)</td>
                            <td align="center">(3)</td>
                        </tr>
                        <tr>
                            <td class="index">1.</td>
                            <td colspan="2">Bunga</td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">2.</td>
                            <td colspan="2">Royalti</td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">3.</td>
                            <td colspan="2">Sewa</td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">4.</td>
                            <td colspan="2">Penghargaan dan Hadiah</td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">5.</td>
                            <td colspan="2">Keuntungan dari Penjualan / Pengalihan Harta</td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">6.</td>
                            <td colspan="2">Penghasilan Lainnya</td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index"></td>
                            <td>JUMLAH BAGIAN A</td>
                            <td>JBA</td>
                            <td class="value"></td>
                        </tr>
                    </table>
                </td>    
            </tr>
			
            <tr>
                <td colspan="6" style="padding-bottom:5px; padding-top:10px; border-top:1px solid #000; font-weight:bold; font-size:14px;">BAGIAN B : PENGHASILAN YANG TIDAK TERMASUK OBJEK PAJAK</td>
            </tr>
            <tr>
                <td colspan="6">
                    <table class="normal_teks table-detail" style="font-size: 11px;word-wrap: break-word;;">
                        <tr>
                            <th width="12px">NO</th>
                            <th width="500px" colspan="2">JENIS PENGHASILAN</th>
                            <th>JUMLAH PENGHASILAN (Rp)</th>
                        </tr>
                        <tr style="font-size: 9px;">
                            <td align="center">(1)</td>
                            <td colspan="2" align="center">(2)</td>
                            <td align="center">(3)</td>
                        </tr>
                        <tr>
                            <td class="index">1.</td>
                            <td colspan="2">Bantuan / Sumbangan / Hibah</td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">2.</td>
                            <td colspan="2">Warisan</td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">3.</td>
                            <td colspan="2">Bagian Laba Anggota Perseroan Komanditer Tidak Atas Saham, Persekutuan, Perkumpulan, Firma, Kongsi</td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">4.</td>
                            <td colspan="2">Klaim Asuransi Kesehatan, Kecelakaan, Jiwa, Dwiguna, Beasiswa</td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">5.</td>
                            <td colspan="2">Beasiswa</td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">6.</td>
                            <td colspan="2">Penghasilan Lainnya yang Tidak Termasuk Objek Pajak</td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index"></td>
                            <td>JUMLAH BAGIAN B</td>
                            <td>JBB</td>
                            <td class="value"></td>
                        </tr>
                    </table>
                </td>    
            </tr>

            <tr>
                <td colspan="6" style="padding-bottom:5px; padding-top:10px; border-top:1px solid #000; font-weight:bold; font-size:14px;">BAGIAN C : DAFTAR PEMOTONGAN/PEMUNGUTAN PPh OLEH PIHAK LAIN <br>DAN PPh YANG DITANGGUNG PEMERINTAH</td>
            </tr>
            <tr>
                <td colspan="6">
                    <table class="normal_teks table-detail" style="font-size: 11px;">
                        <tr>
                            <th rowspan="3">NO</th>
                            <th>NAMA PEMOTONG/</th>
                            <th>NPWP PEMOTONG/</th>
                            <th colspan="2">BUKTI PEMOTONGAN/</th>
                            <th colspan="2">JENIS PAJAK/</th>
                            <th>JUMLAH PPh YANG</th>
                        </tr>
                        <tr>
                            <th rowspan="2">PEMUNGUT PAJAK</th>
                            <th rowspan="2">PEMUNGUT PAJAK</th>
                            <th colspan="2">PEMUNGUTAN</th>
                            <th colspan="2">PPh PASAL 21/</th>
                            <th rowspan="2">DIPOTONG / DIPUNGUT</th>
                        </tr>
                        <tr>
                            <th>NOMOR</th>
                            <th>TANGGAL</th>
                            <th colspan="2">22/23/24/26/DTP</th>
                        </tr>
                        <tr style="font-size: 9px;">
                            <td align="center">(1)</td>
                            <td align="center">(2)</td>
                            <td align="center">(3)</td>
                            <td align="center">(4)</td>
                            <td align="center">(5)</td>
                            <td align="center" colspan="2">(6)</td>
                            <td align="center">(7)</td>
                        </tr>
                        <tr>
                            <td class="index">1.</td>
                            <td>PT Telekomunikasi Indonesia</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index"></td>
                            <td colspan="4">JUMLAH BAGIAN C</td>
                            <td style="background:#999;" class="bgabu"></td>
                            <td>JBC</td>
                            <td class="value"></td>
                        </tr>
                    </table>
                </td>    
            </tr>
            
		</table>
		</td>
	</tr>
	<tr>
		<td><img src="<?php echo Yii::app()->baseUrl;?>/images/kotak.jpg"></td>
		<td></td>
		<td><img src="<?php echo Yii::app()->baseUrl;?>/images/kotak.jpg"></td>
	</tr>
</table>
</form>
</body>
</html>