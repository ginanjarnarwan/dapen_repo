<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="<?php echo Yii::app()->baseUrl;?>/style/default.css" rel="stylesheet" type="text/css">
<title>FORM SPT</title>
</head>

<body>
<input type="button" name="print" value="Print" onClick="this.style.display='none';document.getElementById('id_print').style.display='none';window.print();this.style.display='';document.getElementById('id_print').style.display='';">

<br style="page-break-after:always">
<form name="spt" id="id_spt" method="post">
<table border="0" cellpadding="0" cellspacing="0" width="100%" class="normal_teks">
	<tr>
		<td><img src="<?php echo Yii::app()->baseUrl;?>/images/kotak.jpg"></td>
		<td></td>
		<td><img src="<?php echo Yii::app()->baseUrl;?>/images/kotak.jpg"></td>
	</tr>
	<tr>
		<td></td>
		<td>
		<table height="931" border="0" cellpadding="0" cellspacing="0" class="normal_teks" width="100%">
			<tr>
				<td width="15" style="BORDER-LEFT: 1px solid #000000;BORDER-TOP: 1px solid #000000;BORDER-BOTTOM: 1px solid #000;"><img src="<?php echo Yii::app()->baseUrl;?>/images/form.jpg"></td>
				<td width="178" align="center" style="BORDER-RIGHT: 1px solid #000000;BORDER-TOP: 1px solid #000000;BORDER-BOTTOM: 1px solid #000;"><font size="4"><b>1721 - A1</b></font><br/>
				  DEPARTEMEN KEUANGAN RI<br />
        DIREKTORAT JENDERAL PAJAK</td>
				<!-- <b>1721 - A1</b></font><br/>
				  DEPARTEMEN KEUANGAN RI<br />
        DIREKTORAT JENDERAL PAJAK</td> -->
				<td colspan="4" style="BORDER-RIGHT: 1px solid #000000;BORDER-TOP: 1px solid #000000;BORDER-BOTTOM: 1px solid #000;"><center><font size="2">BUKTI PEMOTONGAN PAJAK PENGHASILAN PASAL 21 <br /> BAGI PEGAWAI TETAP ATAU PENERIMA PENSIUN ATAU TUNJANGAN HARI TUA/JAMINAN HARI TUA</font></center></td>
				<td width="14" style="BORDER-RIGHT: 1px solid #000000;BORDER-TOP: 1px solid #000000;BORDER-BOTTOM: 1px solid #000;"><img src="<?php echo Yii::app()->baseUrl;?>/images/thn_miring.jpg"></td>
				<td colspan="2" style="BORDER-RIGHT: 1px solid #000000;BORDER-TOP: 1px solid #000000;BORDER-BOTTOM: 1px solid #000;">
				&nbsp;&nbsp;1. Lembar 1 untuk Pegawai<br>
				&nbsp;&nbsp;2. Lembar 2 untuk Pemotong Pajak<br>
				 <?php
				  $temp[0]= substr($data1[0][3],0,1);
				  $temp[1]= substr($data1[0][3],1,1);
				  $temp[2]= substr($data1[0][3],2,1);
				  $temp[3]= substr($data1[0][3],3,1);
						?>
				<table width="50%" border="0" cellpadding="0" cellspacing="0">
				<tr>
				<td width="5">&nbsp;</td>
				<td height="40" width="20" align="center" style="BORDER-RIGHT: 1px solid #000;BORDER-TOP: 1px solid #000;BORDER-LEFT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;"><b><?php echo $temp[0];?></b></td>
				<td width="20" align="center" style="BORDER-RIGHT: 1px solid #000;BORDER-TOP: 1px solid #000;BORDER-BOTTOM: 1px solid #000;"><b><?php echo $temp[1];?></b></td>
				<td width="20" align="center" style="BORDER-RIGHT: 1px solid #000;BORDER-TOP: 1px solid #000;BORDER-BOTTOM: 1px solid #000;"><b><?php echo $temp[2];?></b></td>
				<td width="20" align="center" style="BORDER-RIGHT: 1px solid #000;BORDER-TOP: 1px solid #000;BORDER-BOTTOM: 1px solid #000;"><b><?php echo $temp[3];?></b></td>
				</tr>
				</table>			  </td>
			</tr>
			<tr>
				<td style="border-left:1px solid #000">&nbsp;</td>
				<td class="normal_teks" colspan="2">NOMOR URUT</td>
				<td class="normal_teks" width="12">:</td>
				<td colspan="3" class="normal_teks">
				<table border="0" cellpadding="1" cellspacing="1" class="normal_teks">
              <tr>
              <?php
              $a = (0+1);
              $view = strlen($a);
           		for ($x=0;$x <= $view;$x++){
           			$tampil = substr($a,$x,1);
           			?>
              <td width="15" align="center" style="BORDER-LEFT: 1px solid #000000;BORDER-BOTTOM: 1px solid #000000;BORDER-RIGHT: 1px solid #000000;BORDER-TOP: 1px solid #000;"><?php echo $tampil;?></td>
            	<?php
            	}
              ?>
              </tr>
              </table>				</td>
				<td class="normal_teks" colspan="2" style="border-right:1px solid #000">&nbsp;</td>
			</tr>
			<tr>
				<td style="border-left:1px solid #000">&nbsp;</td>
				<td colspan="2" class="normal_teks">NPWP PEMOTONG PAJAK</td>
				<td class="normal_teks">:</td>
				<td  colspan="3" class="normal_teks">
					<table border="0" cellpadding="1" cellspacing="1" class="normal_teks">
              <tr>
              <?php
              $view = strlen($data1[0][13]);
           		for ($x=0;$x <= $view;$x++){
           			$tampil = substr($data1[0][13],$x,1);
           			?>
              <td width="15" align="center" style="BORDER-LEFT: 1px solid #000000;BORDER-BOTTOM: 1px solid #000000;BORDER-RIGHT: 1px solid #000000;BORDER-TOP: 1px solid #000;"><?php echo $tampil;?></td>
            	<?php
            	}
              ?>
              </tr>
              </table>				</td>
				<td class="normal_teks" colspan="2" style="border-right:1px solid #000">&nbsp;</td>
			</tr>
			<tr>
				<td style="border-left:1px solid #000">&nbsp;</td>
				<td colspan="2" class="normal_teks">NAMA PEMOTONG PAJAK</td>
				<td class="normal_teks">:</td>
				<td class="normal_teks" colspan="3">
					<table border="0" cellpadding="1" cellspacing="1" class="normal_teks">
              <tr>
              <?php
              $view = strlen("PT. TELEKOMUNIKASI INDONESIA, Tbk");
           		for ($x=0;$x <= $view;$x++){
           			$tampil = substr("PT.TELEKOMUNIKASI INDONESIA,Tbk",$x,1);
           			?>
              <td width="15" align="center" style="BORDER-LEFT: 1px solid #000000;BORDER-BOTTOM: 1px solid #000000;BORDER-RIGHT: 1px solid #000000;BORDER-TOP: 1px solid #000;"><?php echo $tampil;?></td>
            	<?php
            	}
              ?>
              </tr>
              </table>				</td>
				<td class="normal_teks" colspan="2" style="border-right:1px solid #000">&nbsp;</td>
			</tr>
			<tr>
				<td style="border-left:1px solid #000">&nbsp;</td>
				<td colspan="2" class="normal_teks">NAMA PEGAWAI ATAU PENERIMA PENSIUN/THT/JHT </td>
				<td class="normal_teks">:</td>
				<td class="normal_teks" colspan="3">
					<table border="0" cellpadding="1" cellspacing="1" class="normal_teks">
              <tr>
              <?php
              $gab = $data1[0][2];
              $view = strlen($gab);
           		for ($x=0;$x <= $view;$x++){
           			$tampil = substr($gab,$x,1);
           			?>
              <td width="15" align="center" style="BORDER-LEFT: 1px solid #000000;BORDER-BOTTOM: 1px solid #000000;BORDER-RIGHT: 1px solid #000000;BORDER-TOP: 1px solid #000;"><?php echo $tampil;?></td>
            	<?php
            	}
              ?>
              </tr>
              </table>				</td>
				<td class="normal_teks" colspan="2" style="border-right:1px solid #000">&nbsp;</td>
			</tr>
			<tr>
				<td style="border-left:1px solid #000">&nbsp;</td>
				<td colspan="2" class="normal_teks">NPWP PEGAWAI ATAU PENERIMA PENSIUN/THT/JHT </td>
				<td class="normal_teks">:</td>
				<td class="normal_teks" colspan="3">
					<table border="0" cellpadding="1" cellspacing="1" class="normal_teks">
              <tr>
              <?php
             $gab = $data1[0][14];
              $view = strlen($gab);
           		for ($x=0;$x <= $view;$x++){
           			$tampil = substr($gab,$x,1);
           			?>
              <td width="15" align="center" style="BORDER-LEFT: 1px solid #000000;BORDER-BOTTOM: 1px solid #000000;BORDER-RIGHT: 1px solid #000000;BORDER-TOP: 1px solid #000;"><?php echo $tampil;?></td>
            	<?php
            	}
              ?>
              </tr>
              </table>				</td>
				<td class="normal_teks" colspan="2" style="border-right:1px solid #000">&nbsp;</td>
			</tr>
			<tr>
				<td style="border-left:1px solid #000">&nbsp;</td>
				<td colspan="2" class="normal_teks">ALAMAT  PEGAWAI ATAU PENERIMA PENSIUN/THT/JHT </td>
				<td class="normal_teks">:</td>
				<td class="normal_teks" colspan="3">
					<table border="0" cellpadding="0" cellspacing="1" class="normal_teks_2">
      	<tr>
              <?php
              //$gab = $seluruh;
              $gab = $data1[0][16];
			  $view = strlen($gab);
			  if($view <= 30){
					for ($x=0;$x <= $view;$x++){
           			$tampil = substr($gab,$x,1);
           			?>
					<td width="15" align="center" style="BORDER-LEFT: 1px solid #000000;BORDER-BOTTOM: 1px solid #000000;BORDER-RIGHT: 1px solid #000000;BORDER-TOP: 1px solid #000;"><?php echo $tampil;?></td>
            		<?php
					}
								  echo "</tr>";
			  }

			  if($view > 30){
					for ($x=0;$x <= 30;$x++){
           			$tampil = substr($gab,$x,1);
           			?>
					<td width="15" align="center" style="BORDER-LEFT: 1px solid #000000;BORDER-BOTTOM: 1px solid #000000;BORDER-RIGHT: 1px solid #000000;BORDER-TOP: 1px solid #000;"><?php echo $tampil;?></td>
					<?php
            		}
          echo "</tr>";
          echo "<tr>";
					for ($x2=31;$x2 <= $view;$x2++){
           			$tampil = substr($gab,$x2,1);
           			?>
					<td width="15" align="center" style="BORDER-LEFT: 1px solid #000000;BORDER-BOTTOM: 1px solid #000000;BORDER-RIGHT: 1px solid #000000;BORDER-TOP: 1px solid #000;"><?php echo $tampil;?></td>
					<?php
            		}
           echo "</tr>";
			 }
					?>
              </table>				</td>
				<td class="normal_teks" colspan="2" style="border-right:1px solid #000">&nbsp;</td>
			</tr>
			<tr>
				<td style="border-left:1px solid #000">&nbsp;</td>
				<td colspan="2" class="normal_teks">STATUS, JENIS KELAMIN DAN KARYAWAN ASING </td>
				<td class="normal_teks">:</td>
				<td class="normal_teks" colspan="3">
					<?php
						  $c1="";$c2="";
						  if ($data1[0][22]==1)
							$c1="Kawin";
						elseif ($data1[0][22]==0)
							$c1="Tidak Kawin";
						 if ($data1[0][23]==1)
							$c2="Laki-Laki";
						elseif ($data1[0][23]==0)
							$c2="Perempuan";
						  ?>
						  <label>
						  <img src="<?php echo Yii::app()->baseUrl?>/images/cross.JPG" width="13" height="13" />
						  <?php echo $c1?>
						  <img src="<?php echo Yii::app()->baseUrl?>/images/cross.JPG" width="13" height="13" />
						  <?php echo $c2?></label>				</td>
				<td class="normal_teks" colspan="2" style="border-right:1px solid #000">&nbsp;</td>
			</tr>
			<tr>
				<td style="border-left:1px solid #000">&nbsp;</td>
				<td colspan="2" class="normal_teks">JUMLAH TANGGUNGAN KELUARGA UNTUK PTKP </td>
				<td class="normal_teks">:</td>
				<td class="normal_teks" colspan="3">
					<table border="0" cellpadding="1" cellspacing="1" class="normal_teks">
              <tr>
              <?php
              $gab = $data1[0][24];
              $view = strlen($gab);
           		for ($x=0;$x <= $view;$x++){
           			$tampil = substr($gab,$x,1);
           			?>
              <td width="15" align="center" style="BORDER-LEFT: 1px solid #000000;BORDER-BOTTOM: 1px solid #000000;BORDER-RIGHT: 1px solid #000000;BORDER-TOP: 1px solid #000;"><?php echo $tampil;?></td>
            	<?php
            	}
              ?>
              </tr>
              </table>				</td>
				<td class="normal_teks" colspan="2" style="border-right:1px solid #000">&nbsp;</td>
			</tr>
			<tr>
				<td class="normal_teks" style="border-left:1px solid #000">&nbsp;</td>
				<td colspan="2" class="normal_teks">JABATAN</td>
				<td class="normal_teks">:</td>
				<td class="normal_teks" colspan="3">
					<table border="0" cellpadding="1" cellspacing="1" class="normal_teks">
              <tr>
              <?php
              $gab = "PEGAWAI TETAP";
              $view = strlen($gab);
           		for ($x=0;$x <= $view;$x++){
           			$tampil = substr($gab,$x,1);
           			?>
              <td width="15" align="center" style="BORDER-LEFT: 1px solid #000000;BORDER-BOTTOM: 1px solid #000000;BORDER-RIGHT: 1px solid #000000;BORDER-TOP: 1px solid #000;"><?php echo $tampil;?></td>
            	<?php
            	}
              ?>
              </tr>
              </table>				</td>
				<td class="normal_teks" colspan="2" style="border-right:1px solid #000">&nbsp;</td>
			</tr>
			<tr>
				<td class="normal_teks" style="border-left:1px solid #000">&nbsp;</td>
				<td colspan="2" class="normal_teks">MASA PEROLEHAN PENGHASILAN</td>
				<td class="normal_teks">:</td>
				<td class="normal_teks" colspan="3">
					<table border="0" cellpadding="1" cellspacing="1" class="normal_teks">
              <tr>
              <?php
              $gab = $data1[0][5]." s.d ".$data1[0][7];
              $view = strlen($gab);
           		for ($x=0;$x <= $view;$x++){
           			$tampil = substr($gab,$x,1);
           			?>
              <td width="15" align="center" style="BORDER-LEFT: 1px solid #000000;BORDER-BOTTOM: 1px solid #000000;BORDER-RIGHT: 1px solid #000000;BORDER-TOP: 1px solid #000;"><?php echo $tampil;?></td>
            	<?php
            	}
              ?>
              </tr>
              </table>				</td>
				<td class="normal_teks" colspan="2" style="border-right:1px solid #000">&nbsp;</td>
			</tr>
			<tr>
				<td colspan="9" class="normal_teks" style="border-bottom:1px solid #000000; border-top:1px solid #000000">&nbsp;</td>
			</tr>
			<tr bordercolor="#999999">
				<td class="normal_teks" align="center" style="border-left:1px solid #000"><b>A.</b></td>
				<td colspan="4" class="normal_teks"><b>&nbsp;RINCIAN PENGHASILAN DAN PENGHITUNGAN PPh PASAL 21 SEBAGAI BERIKUT:</b></td>
				<td width="71" class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td  colspan="2" width="26%" class="normal_teks" align="center" bgcolor="#999999" style="border:1px solid #000000"><b>RUPIAH</b></td>
			</tr>
			<tr bordercolor="#999999">
				<td class="normal_teks" style="border-left:1px solid #000">&nbsp;</td>
				<td colspan="4" class="normal_teks"><b>PENGHASILAN BRUTO :</b></td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td width="33" class="normal_teks"></td>
				<td width="148" class="normal_teks" style="border-right:1px solid #000">&nbsp;</td>
			</tr>
			<tr>
				<td class="normal_teks" style="border-left:1px solid #000">&nbsp;</td>
				<td colspan="3" class="normal_teks">1.  Gaji/ pensiun atau THT/JHT</td>
				<td width="219" class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks" align="center" style="border:1px solid #000000">1</td>
				<td class="normal_teks" style="border:1px solid #000000" align="right"><?php echo number_format($data1[0][25],0,",",".")?>		</td>
			</tr>
			<tr bordercolor="#999999">
				<td class="normal_teks" style="border-left:1px solid #000">&nbsp;</td>
				<td colspan="3" class="normal_teks">2. Tunjangan PPh</td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks" align="center" style="border:1px solid #000">2</td>
				<td class="normal_teks" align="right" style="border:1px solid #000"><?php echo  number_format($data1[0][26],0,",",".")?></td>
			</tr>
			<tr bordercolor="#999999">
				<td class="normal_teks" style="border-left:1px solid #000">&nbsp;</td>
				<td colspan="3" class="normal_teks">3. Tunjangan lainnya, uang lembur, dan sebagainya</td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks" align="center" style="border:1px solid #000">3</td>
				<td class="normal_teks" align="right" style="border:1px solid #000"><?php echo  number_format($data1[0][27],0,",",".")?></td>
			</tr>
			<tr bordercolor="#999999">
				<td class="normal_teks" style="border-left:1px solid #000">&nbsp;</td>
				<td colspan="3" class="normal_teks">4. Honorarium dan imbalan lain sejenisnya</td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks" align="center" style="border:1px solid #000">4</td>
				<td class="normal_teks" align="right" style="border:1px solid #000"><?php echo  number_format($data1[0][28],0,",",".")?></td>
			</tr>
			<tr bordercolor="#999999">
				<td class="normal_teks" style="border-left:1px solid #000">&nbsp;</td>
				<td colspan="3" class="normal_teks">5. Premi asuransi yang dibayar pemberi kerja</td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks" align="center" style="border:1px solid #000">5</td>
				<td class="normal_teks" align="right" style="border:1px solid #000"><?php echo  number_format($data1[0][29],0,",",".")?></td>
			</tr>
			<tr bordercolor="#999999">
				<td class="normal_teks" style="border-left:1px solid #000">&nbsp;</td>
				<td colspan="4" class="normal_teks">6. Penerimaan dalam bentuk natura dan kenikmatan lainnya yang dikenakan pemotongan</td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks" align="center" style="border:1px solid #000">6</td>
				<td class="normal_teks" align="right" style="border:1px solid #000"><?php echo  number_format($data1[0][30],0,",",".")?></td>
			</tr>
			<tr>
				<td class="normal_teks" style="border-left:1px solid #000">&nbsp;</td>
				<td colspan="3" class="normal_teks">7. Jumlah (1 s.d. 6)</td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks" align="center" style="border:1px solid #000">7</td>
				<td class="normal_teks" align="right" style="border:1px solid #000"><?php echo  number_format($data1[0][31],0,",",".")?> </td>
			</tr>
			<tr bordercolor="#999999">
				<td class="normal_teks" style="border-left:1px solid #000">&nbsp;</td>
				<td colspan="4" class="normal_teks">8. Tantiem, bonus, grafitifikasi, jasa produksi, dah THR</td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks" align="center" style="border:1px solid #000">8</td>
				<td class="normal_teks" align="right" style="border:1px solid #000"><?php echo  number_format($data1[0][32],0,",",".")?></td>
			</tr>
			<tr>
				<td class="normal_teks" style="border-left:1px solid #000">&nbsp;</td>
				<td colspan="2" class="normal_teks">9. Jumlah penghasilan bruto (7 + 8)</td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks" align="center" style="border:1px solid #000">9</td>
				<td class="normal_teks" align="right" style="border:1px solid #000"><?php echo  number_format($data1[0][33],0,",",".")?></td>
			</tr>
			<tr>
				<td class="normal_teks" style="border-left:1px solid #000">&nbsp;</td>
				<td colspan="2" class="normal_teks"><b>PENGURANGAN :</b></td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks" style="border-right:1px solid #000">&nbsp;</td>
			</tr>
			<tr>
				<td class="normal_teks" style="border-left:1px solid #000">&nbsp;</td>
				<td colspan="4" class="normal_teks">10. Biaya jabatan/biaya pensiun atas penghasilan pada angka 7</td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks" align="center" style="border:1px solid #000">10</td>
				<td class="normal_teks" align="right" style="border:1px solid #000"><?php echo  number_format($data1[0][34],0,",",".")?></td>
			</tr>
			<tr>
				<td class="normal_teks" style="border-left:1px solid #000">&nbsp;</td>
				<td colspan="4" class="normal_teks">11. Biaya jabatan/biaya pensiun atas penghasilan pada angka 8</td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks" align="center" style="border:1px solid #000">11</td>
				<td class="normal_teks" align="right" style="border:1px solid #000"><?php echo  number_format($data1[0][35],0,",",".")?></td>
			</tr>
			<tr>
				<td class="normal_teks" style="border-left:1px solid #000">&nbsp;</td>
				<td colspan="2" class="normal_teks">12. Iuran pensiun atau iuran THT/JHT</td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks" align="center" style="border:1px solid #000">12</td>
				<td class="normal_teks" align="right" style="border:1px solid #000"><?php echo  number_format($data1[0][36],0,",",".")?></td>
			</tr>
			<tr>
				<td class="normal_teks" style="border-left:1px solid #000">&nbsp;</td>
				<td colspan="2" class="normal_teks">13. Jumlah pengurangan(10 + 11 + 12)</td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks" align="center" style="border:1px solid #000">13</td>
				<td class="normal_teks" align="right" style="border:1px solid #000"><?php echo  number_format($data1[0][37],0,",",".")?></td>
			</tr>
			<tr>
				<td class="normal_teks" style="border-left:1px solid #000">&nbsp;</td>
				<td colspan="2"><b>PENGHITUNGAN PPh PASAL 21 :</b></td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks" style="border-right:1px solid #000">&nbsp;</td>
			</tr>
			<tr>
				<td class="normal_teks" style="border-left:1px solid #000">&nbsp;</td>
				<td colspan="2">14. Jumlah penghasilan neto (9 - 13)</td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks" align="center" style="border:1px solid #000">14</td>
				<td class="normal_teks" align="right" style="border:1px solid #000"><?php echo  number_format($data1[0][38],0,",",".")?></td>
			</tr>
			<tr>
				<td class="normal_teks" style="border-left:1px solid #000">&nbsp;</td>
				<td colspan="2">15. Penghasilan neto masa sebelumnya</td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks" align="center" style="border:1px solid #000">15</td>
				<td class="normal_teks" align="right" style="border:1px solid #000"><?php echo  number_format($data1[0][39],0,",",".")?></td>
			</tr>
			<tr>
				<td class="normal_teks" style="border-left:1px solid #000">&nbsp;</td>
				<td colspan="4">16. Jumlah penghasilan neto untuk penghitungan PPh Pasal 21 (setahun/disetahunkan)</td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks" align="center" style="border:1px solid #000">16</td>
				<td class="normal_teks" align="right" style="border:1px solid #000"><?php echo  number_format($data1[0][40],0,",",".")?></td>
			</tr>
			<tr>
				<td class="normal_teks" style="border-left:1px solid #000">&nbsp;</td>
				<td colspan="2">17. Penghasilan tidak kena pajak (PTKP)</td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks" align="center" style="border:1px solid #000">17</td>
				<td class="normal_teks" align="right" style="border:1px solid #000"><?php echo  number_format($data1[0][41],0,",",".")?></td>
			</tr>
			<tr>
				<td class="normal_teks" style="border-left:1px solid #000">&nbsp;</td>
				<td colspan="2">18. Penghasilan kena pajak setahun/disetahunkan (16 - 17)</td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks" align="center" style="border:1px solid #000">18</td>
				<td class="normal_teks" align="right" style="border:1px solid #000"><?php echo  number_format($data1[0][42],0,",",".")?></td>
			</tr>
			<tr>
				<td class="normal_teks" style="border-left:1px solid #000">&nbsp;</td>
				<td colspan="2">19. PPh Pasal 21 atas penghasilan kena pajak setahun/disetahunkan</td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks" align="center" style="border:1px solid #000">19</td>
				<td class="normal_teks" align="right" style="border:1px solid #000"><?php echo  number_format($data1[0][43],0,",",".")?></td>
			</tr>
			<tr>
				<td class="normal_teks" style="border-left:1px solid #000">&nbsp;</td>
				<td colspan="2">20. PPh Pasal 21 yang telah dipotong masa sebelumnya</td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks" align="center" style="border:1px solid #000">20</td>
				<td class="normal_teks" align="right" style="border:1px solid #000"><?php echo  number_format($data1[0][44],0,",",".")?></td>
			</tr>
			<tr>
				<td class="normal_teks" style="border-left:1px solid #000">&nbsp;</td>
				<td colspan="2">21. PPh Pasal 21 terutang</td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks" align="center" style="border:1px solid #000">21</td>
				<td class="normal_teks" align="right" style="border:1px solid #000"><?php echo  number_format($data1[0][45],0,",",".")?></td>
			</tr>
			<tr>
				<td class="normal_teks" style="border-left:1px solid #000">&nbsp;</td>
				<td colspan="2">22. PPh Pasal 21 dan PPh Pasal 26 yang telah dipotong dan dilunasi</td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks" align="center" style="border:1px solid #000">22</td>
				<td class="normal_teks" align="right" style="border:1px solid #000"><?php echo  number_format($data1[0][48],0,",",".")?></td>
			</tr>
			<tr>
				<td class="normal_teks" style="border-left:1px solid #000">&nbsp;</td>
				<td colspan="4" class="normal_teks" style="padding-left:15px">
				<table class="normal_teks" width="100%">
					<tr>
						<td>22a.</td>
						<td>Dipotong dan dilunasi denga SSP PPh Pasal 21 Ditanggung Pemerintah</td>
					</tr>
					<tr>
						<td>22b.</td>
						<td>Dipotong dan dilunasi SSP</td>
					</tr>
				</table>				</td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td colspan="2" class="normal_teks">
					<table class="normal_teks" width="100%" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td align="center" width="18%" style="border:1px solid #000000">22a</td>
						<td width="82%" align="right" style="border:1px solid #000000">&nbsp;</td>
					</tr>
					<tr>
						<td align="center" style="border:1px solid #000000">22b</td>
						<td style="border:1px solid #000000" align="right"><?php echo  number_format($data1[0][48],0,",",".")?></td>
					</tr>
			  		</table>				</td>
				<td class="normal_teks" style="border-right:1px solid #000">&nbsp;</td>
			</tr>
			<tr>
				<td class="normal_teks" style="border-left:1px solid #000">&nbsp;</td>
				<td colspan="2" class="normal_teks">23. Jumlah PPh Pasal 21 :<br>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox">&nbsp;&nbsp;&nbsp;(a) yang kurang dipotong (23-24) <br/>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox">&nbsp;&nbsp;&nbsp;(b) yang lebih dipotong (24-23)				</td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks" align="center" style="border:1px solid #000">23</td>
				<td class="normal_teks" align="right" style="border:1px solid #000">NIHIL</td>
			</tr>
			<tr>
				<td class="normal_teks" style="border-left:1px solid #000">&nbsp;</td>
				<td colspan="4" class="normal_teks">24. Jumlah tersebut pada angka 23 telah<br>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox">&nbsp;&nbsp;&nbsp;(a) dipotong dari pembayaran gaji &nbsp;&nbsp&nbsp;&nbsp;&nbsp;bulan <input type="text" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" class="button_0" maxlength="1" size="1"><input type="text" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" class="button_0" maxlength="1" size="1"> tahun <input type="text" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" class="button_0" maxlength="1" size="1"><input type="text" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" class="button_0" maxlength="1" size="1"><input type="text" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" class="button_0" maxlength="1" size="1"><input type="text" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" class="button_0" maxlength="1" size="1"><br>
              &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox">&nbsp;&nbsp;&nbsp;(b) diperhitungkan dgn PPH pasal 21 &nbsp;bulan <input type="text" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" class="button_0" maxlength="1" size="1"><input type="text" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" class="button_0" maxlength="1" size="1"> tahun <input type="text" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" class="button_0" maxlength="1" size="1"><input type="text" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" class="button_0" maxlength="1" size="1"><input type="text" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" class="button_0" maxlength="1" size="1"><input type="text" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" class="button_0" maxlength="1" size="1">				</td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks" align="center" style="border:1px solid #000">24</td>
				<td class="normal_teks" align="right" style="border:1px solid #000">&nbsp;</td>
			</tr>
			<tr>
				<td class="normal_teks" align="center" style="border-left:1px solid #000"><b>B.</b></td>
				<td colspan="2"><b>TANDA TANGAN DAN CAP PERUSAHAAN</b></td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks"></td>
				<td class="normal_teks" align="center">&nbsp;</td>
				<td class="normal_teks" style="border-right:1px solid #000">&nbsp;</td>
			</tr>
			<tr>
				<td height="32" style="border-left:1px solid #000">&nbsp;</td>
				<td height="32" colspan="2"><input type="checkbox"> PEMOTONG PAJAK <input type="checkbox"> KUASA </td>
				<td colspan="4">
				  <table border="0" cellpadding="1" cellspacing="1" class="header_teks_2" style="padding-left:50px">
				    <tr>
				      <?php
              $waktu = $data2[0][3];
              $gab = ", ".$waktu;
              $view = strlen($gab);
           		for ($x=9;$x <= $view;$x++){
           			$tampil = substr($gab,$x,1);
           			?>
				      <td width="15" align="center" style="BORDER-LEFT: 1px solid #000000;BORDER-BOTTOM: 1px solid #000000;BORDER-RIGHT: 1px solid #000000;BORDER-TOP: 1px solid #000;"><?php echo $tampil;?></td>
            	  <?php
            	}
              ?>
			        </tr>
		            </table></td>
					<td colspan="2" rowspan="3" style="border-bottom:1px solid #000; border-right:1px solid #000">&nbsp;
				      <table cellpadding="0" cellspacing="0" class="normal_teks_2" align="center">
					<tr>
						<td align="center">TANDA TANGAN DAN CAP PERUSAHAAN<br/>
						  <?php
			  		if ($data2[0][4]!="")
                        { 
                        $gambar = $data2[0][4];
                        ?>
                          <img src="<?php echo Yii::app()->baseUrl."/".$gambar ?>" width="80" height="50" align="absmiddle" style="border:1px solid #000"/>
                          <?php
                        }
                        else
                        {
                        echo "<br>";
                        }
			  			?>				  </td>
					</tr>
			              </table></td>
			</tr>
			<tr>
				<td height="20" class="normal_teks" style="border-left:1px solid #000">&nbsp;</td>
				<td>Nama Lengkap</td>
				<td colspan="5">
				  <table border="0" cellpadding="1" cellspacing="1" class="header_teks_2" style="padding-left:50px">
				    <tr>
				      <?php
              $gab = $data2[0][1];
              $view = strlen($gab);
           		for ($x=0;$x <= $view;$x++){
           			$tampil = substr($gab,$x,1);
           			?>
				      <td width="15" align="center" style="BORDER-LEFT: 1px solid #000000;BORDER-BOTTOM: 1px solid #000000;BORDER-RIGHT: 1px solid #000000;BORDER-TOP: 1px solid #000;"><?php echo $tampil;?></td>
            	  <?php
            	}
              ?>
			        </tr>
		            </table></td>
			</tr>
			<tr>
				<td class="normal_teks" height="20" style="border-left:1px solid #000; border-bottom:1px solid #000">&nbsp;</td>
				<td style="border-bottom:1px solid #000">NPWP</td>
				<td width="259" style="border-bottom:1px solid #000">
					<table border="0" cellpadding="1" cellspacing="1" class="header_teks_2" style="padding-left:50">
              <tr>
              <?php
              $gab = "093493815423000";
              $view = strlen($gab);
           		for ($x=0;$x <= $view;$x++){
           			$tampil = substr($gab,$x,1);
           			?>
              <td width="15" align="center" style="BORDER-LEFT: 1px solid #000000;BORDER-BOTTOM: 1px solid #000000;BORDER-RIGHT: 1px solid #000000;BORDER-TOP: 1px solid #000;"><?php echo $tampil;?></td>
            	<?php
            	}
              ?>
              </tr>
              </table>				</td>
				<td colspan="4" class="normal_teks" style="border-bottom:1px solid #000">&nbsp;</td>
		    </tr>
		</table>
		</td>
		<td></td>
	</tr>
	<tr>
		<td><img src="<?php echo Yii::app()->baseUrl;?>/images/kotak.jpg"></td>
		<td></td>
		<td><img src="<?php echo Yii::app()->baseUrl;?>/images/kotak.jpg"></td>
	</tr>
</table>
</form>
<script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl;?>/js/vendor/jquery-2.2.4.min.js"></script>
</body>
</html>