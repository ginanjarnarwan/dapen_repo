<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="<?php echo Yii::app()->baseUrl;?>/style/default.css" rel="stylesheet" type="text/css">

<title>FORM 1770 S</title>
</head>
	
<style>
@media print {
    .bgabu {
        background:#999 !important;
        -webkit-print-color-adjust: exact; 
    }
    .show { 
        page-break-inside:avoid; page-break-after:auto 
    }
}
.show { 
    page-break-inside:avoid; page-break-after:auto 
}
.table-detail { width:100%; border:1px solid #000; padding:5px; border-collapse:collapse; }
.table-detail th, .table-detail td { border:1px solid #000; padding:2px; }
.table-detail .index { text-align:center; }
.table-detail .value { text-align:right; }
.pad-kecil tr td {
    padding: 0px;
}
body{
    margin: 0px;
    font-family: "Lato", "Helvetica Neue", Helvetica, Arial, sans-serif !important;
}
.btn-success {
    color: #fff;
    background-color: #5cb85c;
    border-color: #4cae4c;
}
.btn {
    display: inline-block;
    margin-bottom: 0;
    font-weight: normal;
    text-align: center;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    background-image: none;
    border: 1px solid transparent;
    white-space: nowrap;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    border-radius: 4px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}
.btn-success:active:hover, .btn-success.active:hover, .open>.dropdown-toggle.btn-success:hover, .btn-success:active:focus, .btn-success.active:focus, .open>.dropdown-toggle.btn-success:focus, .btn-success:active.focus, .btn-success.active.focus, .open>.dropdown-toggle.btn-success.focus {
    color: #fff;
    background-color: #398439;
    border-color: #255625;
}
.btn-success:hover {
    color: #fff;
    background-color: #449d44;
    border-color: #398439;
}
</style>
<body>
<center><button id="btnExportpdf" class="btn btn-outline btn-success" onClick="this.style.display='none';window.print();this.style.display='';"><i class="fa fa-file-pdf-o"></i> Print Pdf</button></center>
<div class="show">
<table border="0" cellpadding="0" cellspacing="0" class="normal_teks" width="100%" height="931" style="margin-left:auto;margin-right:auto;">
	<tr style="height:0px;">
		<td><img src="<?php echo Yii::app()->baseUrl;?>/images/kotak.jpg"></td>
		<td></td>
		<td><img src="<?php echo Yii::app()->baseUrl;?>/images/kotak.jpg"></td>
	</tr>
	<tr>
		<td></td>
		<td height="931" valign="top">
		<table border="0" cellpadding="0" cellspacing="0" class="normal_teks" style="margin-top:0px;">
			<tr>
				<td colspan="2" style="font-size:14px;border:1px solid #000000; border-left: none; border-top: none; text-align:center; width:20%;">
                <p style="font-weight: 100px;font-size: 45px;margin-bottom: 0px;">1770 S</p>
                <!-- <img src="<?php echo Yii::app()->baseUrl;?>/images/logoPajak.jpg"> -->
				  <b>KEMENTERIAN KEUANGAN RI<br />
        DIREKTORAT JENDERAL PAJAK</b></td>
				<td width="23" style="border:1px solid #000; width:55%; border-top:none;">
                <div style="text-align:center; font-size:20px; font-weight:bold;">
                	SPT TAHUNAN
                </div>
                <div style="text-align:center; font-size:16px; font-weight:bold;">
                    PAJAK PENGHASILAN WAJIB PAJAK  ORANG PRIBADI
                </div>
                <div style="border-top:2px solid #000; text-align:left; margin-top: 5px; font-size: 11px; font-weight:500; padding:8px 10px 6px 10px;">
                	MEMPUNYAI PENGHASILAN :
                    <ul style="margin-top: 0px;margin-bottom: 0px;">
                        <li>DARI SATU ATAU LEBIH PEMBERI KERJA</li>
                        <li>DALAM NEGERI LAINNYA</li>
                        <li>YANG DIKENAKAN PPh FINAL DAN/ATAU BERSIFAT FINAL</li>
                    </ul>
                </div>
                </td>
				<td valign="top" style="width:20%; border:1px solid #000; border-top:none; border-right:none; padding-left:10px; font-size: 11px;">
                        <p style="font-weight: 100px;font-size: 45px;margin-bottom: 0px;text-align: center;"><?php echo $tahun;?></p>
                        <div style="text-align: center;">
                            <div style="width: 15px;height:15px;border: 1px solid #000;float: left;"></div> SPT PEMBETULAN KE ..
                        </div>
                </td>
			</tr>
            <tr>
            	<td colspan="6" style="padding-bottom:5px; padding-top:10px; border-top:1px solid #000; font-weight:bold; font-size:14px;">IDENTITAS</td>
            </tr>
            <tr>
            <td colspan="6">
            <table style="width:100%; border:1px solid #000;">
            <tr>
            <td style="width:50%;">
            	<table style="font-size:9px; width:100%;">
                <tr>
                    <td style="width:2px"></td>
                    <td style="width:147px">NPWP</td>
                    <td style="width:2px"> : </td>
                    <td colspan="6" style="border-bottom: 1px solid #000;width:240px;"><?php echo $data1721['V_NPWP_PRIBADI']; ?></td>
                </tr>
                <tr>
                    <td></td>
                    <td>NAMA</td>
                    <td> : </td>
                    <td colspan="6" style="border-bottom: 1px solid #000;"><?php echo $data1721['V_NAMA_KARYAWAN']; ?></td>
                </tr>
                <tr>
                    <td></td>
                    <td>PEKERJAAN</td>
                    <td> : </td>
                    <td style="border-bottom: 1px solid #000; width: 30%;"><?php echo $data1721['V_JABATAN']; ?></td>
                    <td style="width:10px;"></td>
                    <td style="width:2px"></td>
                    <td style="width:50px">KLU</td>
                    <td style="width:2px"> : </td>
                    <td style="border-bottom: 1px solid #000;"><?php //echo $final[0]['nik']; ?></td>
                </tr>
                <tr>
                    <td></td>
                    <td>NO. TELEPON</td>
                    <td> : </td>
                    <td style="border-bottom: 1px solid #000;"><?php //echo $final[0]['alamat']; ?></td>
                    <td style="width:10px;"></td>
                    <td style="width:2px"></td>
                    <td style="width:50px">NO. FAKS</td>
                    <td style="width:2px"> : </td>
                    <td style="border-bottom: 1px solid #000;"><?php //echo $final[0]['nik']; ?></td>
                </tr>
                <tr>
                    <td></td>
                    <td style="font-size: 9px;">STATUS KEWAJIBAN <br>PEKERJAAN SUAMI-ISTRI</td>
                    <td> : </td>
                    <?php
                        $stat_k = "";
                        $stat_tk = "";
                        $stat = $data1721['V_ADT'];
                        
                        if (strstr($stat, "TK"))
                        {
                            $stat_tk = str_replace("TK/", "", $stat);
                        }
                        else
                        {
                            $stat_k = str_replace("K/", "", $stat);
                        }
                    ?>
                    <td colspan="6" style="border-bottom: 1px solid #000;" valign="bottom">K / <?php echo $stat_k; ?>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    TK / <?php echo $stat_tk; ?>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    HB / </td>
                </tr>
                <tr>
                    <td></td>
                    <td>NPWP ISTRI/SUAMI</td>
                    <td> : </td>
                    <td colspan="6" style="border-bottom: 1px solid #000;"><?php //echo $final[0]['nama']; ?></td>
                </tr>
                <tr>
                    <td></td>
                    <td colspan="8" style="font-size: 9px;">Permohonan perubahan data disampaikan terpisah dari pelaporan SPT Tahaunan PPh Orang Pribadi ini, dengan menggunakan formulir perubahan Data Wajib Pajak dan dilengkapi dokumen yang disyaratkan</td>
                </tr>
                </table>
            </td>
            </tr>
            </table>
            </td>
            </tr>
            
            <tr>
            	<td colspan="6" style="padding-top:10px; padding-bottom:5px; font-weight:bold; font-size:14px;"></td>
            </tr>
            <tr>
                <td colspan="6">
                    <table class="normal_teks table-detail" style="font-size: 11px;">
                        <tr>
                            <th colspan="2">URAIAN</th>
                            <th>JUMLAH (Rp)</th>
                        </tr>
                        <!-- <tr>
                            <th colspan="2" style="text-align:justify">
                                KODE OBJEK PAJAK : 
                                <img src="<?php $data1=null; echo Yii::app()->baseUrl;?>/images/cross.JPG" width="13" height="13" /> 21-100-01
                                &nbsp;
                                <img src="<?php echo Yii::app()->baseUrl;?>/images/cross0.JPG" width="13" height="13" /> 21-100-02
                            </th>
                            <th style="background:#999"></th>
                        </tr> -->
                        <tr>
                            <th colspan="2" style="text-align:justify">A. PENGHASILAN NETO</th>
                            <th style="background:#999" class="bgabu"></th>
                        </tr>
                        <tr>
                            <td class="index">1.</td>
                            <td>PENGHASILAN NETO DALAM NEGERI SEHUBUNGAN DENGAN PEKERJAAN</td>
                            <td class="value"><?php echo  number_format($data['n_penghasilan_neto_pekerjaan'],0,",",".").",00"?></td>
                        </tr>
                        <tr>
                            <td class="index">2.</td>
                            <td>PENGHASILAN NETO DALAM NEGERI LAINNYA</td>
                            <td class="value"><?php echo   number_format($data['n_penghasilan_neto_lain'],0,",",".").",00"?></td>
                        </tr>
                        <tr>
                            <td class="index">3.</td>
                            <td>PENGHASILAN NETO LUAR NEGERI</td>
                            <td class="value"><?php echo   number_format($data['n_penghasilan_neto_luar_negeri'],0,",",".").",00"?></td>
                        </tr>
                        <tr>
                            <td class="index">4.</td>
                            <td>JUMLAH PENGHASILAN NETO (1+2+3)</td>
                            <td class="value"><?php echo   number_format($data['n_jumlah_neto'],0,",",".").",00"?></td>
                        </tr>
                        <tr>
                            <td class="index">5.</td>
                            <td>ZAKAT/SUMBANGAN KEAGAMAAN YANG SIFATNYA WAJIB</td>
                            <td class="value"><?php echo  number_format($data['n_zakat_sumbangan'],0,",",".").",00"?></td>
                        </tr>
                        <tr>
                            <td class="index">6.</td>
                            <td>JUMLAH PENGHASILAN NETO SETELAH PENGURANGAN ZAKAT/SUMBANGAN KEAGAMAAN YANG </td>
                            <td class="value"><?php echo  number_format($data['n_jumlah_neto_dikurangi_zakat'],0,",",".").",00"?></td>
                        </tr>
                        
                        <tr>
                            <th colspan="2" style="text-align:justify">B. PENGHASILAN KENA PAJAK</th>
                            <th style="background:#999" class="bgabu"></th>
                        </tr>
                        <tr>
                            <td class="index">7.</td>
                            <td>PENGHASILAN TIDAK KENA PAJAK</td>
                            <td class="value"><?php echo  number_format($data['n_ptkp'],0,",",".").",00"?></td>
                        </tr>
                        <tr>
                            <td class="index">8.</td>
                            <td>PENGHASILAN KENA PAJAK (6-7)</td>
                            <td class="value"><?php echo   number_format($data['n_pkp'],0,",",".").",00"?></td>
                        </tr>
                        
                        <tr>
                            <th colspan="2" style="text-align:justify">C. PPh TERUTANG</th>
                            <th style="background:#999" class="bgabu"></th>
                        </tr>
                        <tr>
                            <td class="index">9.</td>
                            <td>PPh TERUTANG (TARIF PASAL 17 UU PPh x ANGKA 8)</td>
                            <td class="value"><?php echo   number_format($data['n_pph_terutang'],0,",",".").",00"?></td>
                        </tr>
                        <tr>
                            <td class="index">10.</td>
                            <td>PENGEMBALIAN / PENGURANGAN PPh PASAL 24 YANG TELAH DIKREDITKAN</td>
                            <td class="value"><?php echo   number_format($data['n_pengembalian_kredit'],0,",",".").",00"?></td>
                        </tr>
                        <tr>
                            <td class="index">11.</td>
                            <td>JUMLAH PPh TERUTANG (9+10)</td>
                            <td class="value"><?php echo   number_format($data['n_jumlah_pph_terutang'],0,",",".").",00"?></td>
                        </tr>
                        <tr>
                            <th colspan="2" style="text-align:justify">D. KREDIT PAJAK</th>
                            <th style="background:#999" class="bgabu"></th>
                        </tr>
                        <tr>
                            <td class="index">12.</td>
                            <td>PPh YANG DIPOTONG/DIPUNGUT PIHAK LAIN/DITANGGUNG PEMERINTAH DAN/ATAU KREDIT PAJAK LUAR NEGERI DAN/ATAU TERUTANG DI LUAR NEGERI  [Diisi dari Formulir 1770 S-I Jumlah Bagian C Kolom (7)]</td>
                            <td class="value"><?php echo   number_format($data['n_pph_dipotong'],0,",",".").",00"?></td>
                        </tr>
                        <tr>
                            <td class="index">13</td>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0" class="noborder" style="border:none;margin-top:0px;font-size: 11px;">
                                <style type="text/css">.noborder td{border: none;}</style>
                                    <tr>
                                        <td style="border-bottom: 1px solid #000;">a.</td>
                                        <td style="border-bottom: 1px solid #000;">
                                            <?php
                                                $cross1 = "cross0";
                                                $cross2 = "cross0";
                                                $hsl = $data['n_jumlah_pph_terutang']-$data['n_pph_dipotong'];
                                                if ($hsl>0)
                                                {
                                                    $cross1 = "cross";
                                                }
                                                if ($hsl<0) {
                                                    $cross2 = "cross";
                                                }
                                                
                                            ?>
                                              
                                              <img src="<?php echo Yii::app()->baseUrl;?>/images/<?php echo $cross1; ?>.JPG" width="13" height="13" />
                                        </td>
                                        <td style="border-bottom: 1px solid #000;">PPh YANG HARUS DIBAYAR SENDIRI</td>
                                        <td rowspan="2">(11-12)</td>
                                    </tr>
                                    <tr>
                                        <td>b.</td>
                                        <td>
                                            <img src="<?php echo Yii::app()->baseUrl;?>/images/<?php echo $cross2; ?>.JPG" width="13" height="13" />
                                        </td>
                                        <td>PPh YANG LEBIH DIPOTONG/DIPUNGUT</td>
                                    </tr>
                                </table>
                            </td>
                            <td class="value"><?php echo number_format(abs($hsl),0,",",".").",00"?></td>
                        </tr>
                        <tr>
                            <td rowspan="2" class="index">14.</td>
                            <td rowspan="2">
                                <table border="0" cellpadding="0" cellspacing="0" class="noborder" style="border:none;margin-top:0px;font-size: 11px;width: 100%;">
                                    <tr>
                                        <td rowspan="2" >PPh YANG DIBAYAR SENDIRI</td>
                                        <td style="border-bottom: 1px solid #000;">a.</td>
                                        <td style="border-bottom: 1px solid #000;">PPh PASAL 25</td>
                                    </tr>
                                    <tr>
                                        <td>b.</td>
                                        <td>STP PPh Pasal 25 (Hanya Pokok Pajak)</td>
                                    </tr>
                                </table>
                            </td>
                            <td class="normal_teks" align="right" style="border:1px solid #000"></td>
                        </tr>
                        <tr>
                            <td class="normal_teks" align="right" style="border:1px solid #000"></td>
                        </tr>
                        <tr>
                            <td class="index">15.</td>
                            <td>JUMLAH KREDIT PAJAK (14a + 14b)</td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <th colspan="2" style="text-align:justify">E. PPh KURANG/LEBIH BAYAR</th>
                            <th style="background:#999" class="bgabu"></th>
                        </tr>
                        <tr>
                            <td class="index">16</td>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0" class="noborder" style="border:none;margin-top:0px;font-size: 11px;">
                                <style type="text/css">.noborder td{border: none;}</style>
                                    <tr>
                                        <td style="border-bottom: 1px solid #000;">a.</td>
                                        <td style="border-bottom: 1px solid #000;">
                                              
                                              <img src="<?php echo Yii::app()->baseUrl;?>/images/<?php echo 'cross0'; ?>.JPG" width="13" height="13" />
                                        </td>
                                        <td style="border-bottom: 1px solid #000;">PPh YANG KURANG DIBAYAR (PPh PASAL 29)</td>
                                        <td rowspan="2">(13-15)</td>
                                        <td style="text-align: center;">TGL LUNAS</td>
                                    </tr>
                                    <tr>
                                        <td>b.</td>
                                        <td>
                                            <img src="<?php echo Yii::app()->baseUrl;?>/images/<?php echo 'cross0'; ?>.JPG" width="13" height="13" />
                                        </td>
                                        <td>PPh YANG LEBIH DIBAYAR (PPh PASAL 28 A)</td>
                                        <td style="text-align: center;">________</td>
                                    </tr>
                                </table>
                            </td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">17.</td>
                            <td colspan="2">
                                <table border="0" cellpadding="0" cellspacing="0" class="noborder" style="border:none;margin-top:0px;font-size: 11px;">
                                    <tr>
                                        <td rowspan="2">PERMOHONAN : PPh Lebih Bayar pada 16b mohon</td>
                                        <td>a.</td>
                                        <td><img src="<?php echo Yii::app()->baseUrl;?>/images/<?php echo 'cross0'; ?>.JPG" width="13" height="13" /></td>
                                        <td>DIRESTITUSIKAN</td>
                                        <td>c.</td>
                                        <td><img src="<?php echo Yii::app()->baseUrl;?>/images/<?php echo 'cross0'; ?>.JPG" width="13" height="13" /></td>
                                        <td>DIKEMBALIKAN DG SKPPKP PASAL 17 C (WP PATUH )</td>
                                    </tr>
                                    <tr>
                                        <td>b.</td>
                                        <td><img src="<?php echo Yii::app()->baseUrl;?>/images/<?php echo 'cross0'; ?>.JPG" width="13" height="13" /></td>
                                        <td>DIPERHITUNGKAN DENGAN UTANG PAJAK</td>
                                        <td>d.</td>
                                        <td><img src="<?php echo Yii::app()->baseUrl;?>/images/<?php echo 'cross0'; ?>.JPG" width="13" height="13" /></td>
                                        <td>DIKEMBALIKAN DG SKPPKP PASAL 17 D (WP TERTENTU)</td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <th colspan="2" style="text-align:justify">F. ANGSURAN PPh PASAL 25 TAHUN PAJAK BERIKUTNYA</th>
                            <th style="background:#999" class="bgabu"></th>
                        </tr>
                        <tr>
                            <td class="index" valign="top">18.</td>
                            <td>
                                <table border="0" cellpadding="0" cellspacing="0" class="noborder" style="border:none;margin-top:0px;font-size: 11px;">
                                    <tr>
                                        <td colspan="3">ANGSURAN PPh PASAL 25 TAHUN PAJAK BERIKUTNYA SEBESAR</td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">DIHITUNG BERDASARKAN</td>
                                    </tr>
                                    <tr>
                                        <td>a.</td>
                                        <td><img src="<?php echo Yii::app()->baseUrl;?>/images/<?php echo 'cross0'; ?>.JPG" width="13" height="13" /></td>
                                        <td>1/12 x JUMLAH PADA ANGKA 13 </td>
                                    </tr>
                                    <tr>
                                        <td>b.</td>
                                        <td><img src="<?php echo Yii::app()->baseUrl;?>/images/<?php echo 'cross0'; ?>.JPG" width="13" height="13" /></td>
                                        <td>PENGHITUNGAN DALAM LAMPIRAN TERSENDIRI </td>
                                    </tr>
                                </table>
                            </td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <th colspan="2" style="text-align:justify">G. LAMPIRAN</th>
                            <th style="background:#999" class="bgabu"></th>
                        </tr>
                        <tr>
                            <td colspan="3">
                                <table border="0" cellpadding="0" cellspacing="0" class="noborder" style="border:none;margin-top:0px;font-size: 11px;">
                                    <tr>
                                        <td>a.</td>
                                        <td><img src="<?php echo Yii::app()->baseUrl;?>/images/<?php echo 'cross0'; ?>.JPG" width="13" height="13" /></td>
                                        <td>Fotokopi Formulir 1721-A1 atau 1721-A2 atau Bukti Potong PPh Pasal 21</td>
                                        <td>d.</td>
                                        <td><img src="<?php echo Yii::app()->baseUrl;?>/images/<?php echo 'cross0'; ?>.JPG" width="13" height="13" /></td>
                                        <td>Perhitungan PPh Terutang Bagi WP Kawin Pisah Harta dan/atau Mempunyai NPWP sendiri</td>
                                    </tr>
                                    <tr>
                                        <td>b.</td>
                                        <td><img src="<?php echo Yii::app()->baseUrl;?>/images/<?php echo 'cross0'; ?>.JPG" width="13" height="13" /></td>
                                        <td>Surat Setoran Pajak Lembar ke-3 PPh Pasal 29 </td>
                                        <td>e.</td>
                                        <td><img src="<?php echo Yii::app()->baseUrl;?>/images/<?php echo 'cross0'; ?>.JPG" width="13" height="13" /></td>
                                        <td style="border-bottom: dotted;"></td>
                                    </tr>
                                    <tr>
                                        <td>c.</td>
                                        <td><img src="<?php echo Yii::app()->baseUrl;?>/images/<?php echo 'cross0'; ?>.JPG" width="13" height="13" /></td>
                                        <td>Surat Kuasa Khusus (Bila dikuasakan)</td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        
                    </table>
                </td>    
            </tr>
			
            
		</table>
		</td>
	</tr>
	<tr style="height:0px;">
		<td><img src="<?php echo Yii::app()->baseUrl;?>/images/kotak.jpg"></td>
		<td></td>
		<td><img src="<?php echo Yii::app()->baseUrl;?>/images/kotak.jpg"></td>
	</tr>
</table>
</div>
<br>
<div class="show">
<table border="0" cellpadding="0" cellspacing="0" class="normal_teks" width="100%" height="931" style="margin-left:auto;margin-right:auto;">
    <tr style="height:0px;">
        <td><img src="<?php echo Yii::app()->baseUrl;?>/images/kotak.jpg"></td>
        <td></td>
        <td><img src="<?php echo Yii::app()->baseUrl;?>/images/kotak.jpg"></td>
    </tr>
    <tr>
        <td></td>
        <td height="931" valign="top">
        <table border="0" cellpadding="0" cellspacing="0" class="normal_teks" style="margin-top:0px;">
            <tr>
                <td colspan="2" style="font-size:14px;border:1px solid #000000; border-left: none; border-top: none; text-align:center; width:20%;" valign="top">
                <p style="font-weight: 100px;font-size: 30px;margin-bottom: 0px;">1770 S - I</p>
                <!-- <img src="<?php echo Yii::app()->baseUrl;?>/images/logoPajak.jpg"> -->
                  <b>KEMENTERIAN KEUANGAN RI<br />
        DIREKTORAT JENDERAL PAJAK</b></td>
                <td width="23" style="border:1px solid #000; width:55%; border-top:none;">
                <div style="text-align:center; font-size:20px; font-weight:bold;">
                    LAMPIRAN - I
                </div>
                <div style="text-align:center; font-size:16px; font-weight:bold;">
                    PAJAK PENGHASILAN WAJIB PAJAK ORANG PRIBADI
                </div>
                <div style="border-top:2px solid #000; text-align:left; margin-top: 5px; font-size: 11px; font-weight:500; padding:8px 10px 6px 10px;">
                    <ul style="margin-top: 0px;margin-bottom: 0px;">
                        <li>PENGHASILAN NETO DALAM NEGERI LAINNYA</li>
                        <li>PENGHASILAN YANG TIDAK TERMASUK OBJEK PAJAK</li>
                        <li>DAFTAR PEMOTONGAN/PEMUNGUTAN PPh OLEH PIHAK LAIN DAN PPh YANG DITANGGUNG PEMERINTAH</li>
                    </ul>
                </div>
                </td>
                <td valign="top" style="width:20%; border:1px solid #000; border-top:none; border-right:none; padding-left:10px; font-size: 11px;">
                        <p style="font-weight: 100px;font-size: 30px;margin-bottom: 0px;text-align: center;"><?php echo $tahun; ?></p>
                </td>
            </tr>
            <tr>
                <td colspan="6" style="padding-bottom:5px; padding-top:10px; border-top:1px solid #000; font-weight:bold; font-size:14px;">IDENTITAS</td>
            </tr>
            <tr>
            <td colspan="6">
            <table style="width:100%; border:1px solid #000;">
            <tr>
            <td style="width:50%;">
                <table style="font-size:9px; width:100%;">
                <tr>
                    <td style="width:2px"></td>
                    <td style="width:147px">NPWP</td>
                    <td style="width:2px"> : </td>
                    <td colspan="6" style="border-bottom: 1px solid #000;width:240px;"><?php echo $data1721['V_NPWP_PRIBADI']; ?></td>
                </tr>
                <tr>
                    <td></td>
                    <td>NAMA</td>
                    <td> : </td>
                    <td colspan="6" style="border-bottom: 1px solid #000;"><?php echo $data1721['V_NAMA_KARYAWAN']; ?></td>
                </tr>
                </table>
            </td>
            </tr>
            </table>
            </td>
            </tr>
            <tr>
                <td colspan="6" style="padding-bottom:5px; padding-top:10px; border-top:1px solid #000; font-weight:bold; font-size:14px;">BAGIAN A : PENGHASILAN NETO DALAM NEGERI LAINNYA<br>(Tidak Termasuk Penghasilan Dikenakan PPh Final dan/atau Bersifat Final)</td>
            </tr>
            <tr>
                <td colspan="6">
                    <table class="normal_teks table-detail" style="font-size: 11px;">
                        <tr>
                            <th width="12px">NO</th>
                            <th width="500px" colspan="2">JENIS PENGHASILAN</th>
                            <th>JUMLAH PENGHASILAN (Rp)</th>
                        </tr>
                        <tr style="font-size: 9px;">
                            <td align="center">(1)</td>
                            <td colspan="2" align="center">(2)</td>
                            <td align="center">(3)</td>
                        </tr>
                        <tr>
                            <td class="index">1.</td>
                            <td colspan="2">Bunga</td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">2.</td>
                            <td colspan="2">Royalti</td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">3.</td>
                            <td colspan="2">Sewa</td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">4.</td>
                            <td colspan="2">Penghargaan dan Hadiah</td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">5.</td>
                            <td colspan="2">Keuntungan dari Penjualan / Pengalihan Harta</td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">6.</td>
                            <td colspan="2">Penghasilan Lainnya</td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index"></td>
                            <td>JUMLAH BAGIAN A</td>
                            <td>JBA</td>
                            <td class="value"></td>
                        </tr>
                    </table>
                </td>    
            </tr>
            
            <tr>
                <td colspan="6" style="padding-bottom:5px; padding-top:10px; border-top:1px solid #000; font-weight:bold; font-size:14px;">BAGIAN B : PENGHASILAN YANG TIDAK TERMASUK OBJEK PAJAK</td>
            </tr>
            <tr>
                <td colspan="6">
                    <table class="normal_teks table-detail" style="font-size: 11px;word-wrap: break-word;;">
                        <tr>
                            <th width="12px">NO</th>
                            <th width="500px" colspan="2">JENIS PENGHASILAN</th>
                            <th>JUMLAH PENGHASILAN (Rp)</th>
                        </tr>
                        <tr style="font-size: 9px;">
                            <td align="center">(1)</td>
                            <td colspan="2" align="center">(2)</td>
                            <td align="center">(3)</td>
                        </tr>
                        <tr>
                            <td class="index">1.</td>
                            <td colspan="2">Bantuan / Sumbangan / Hibah</td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">2.</td>
                            <td colspan="2">Warisan</td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">3.</td>
                            <td colspan="2">Bagian Laba Anggota Perseroan Komanditer Tidak Atas Saham, Persekutuan, Perkumpulan, Firma, Kongsi</td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">4.</td>
                            <td colspan="2">Klaim Asuransi Kesehatan, Kecelakaan, Jiwa, Dwiguna, Beasiswa</td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">5.</td>
                            <td colspan="2">Beasiswa</td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">6.</td>
                            <td colspan="2">Penghasilan Lainnya yang Tidak Termasuk Objek Pajak</td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index"></td>
                            <td>JUMLAH BAGIAN B</td>
                            <td>JBB</td>
                            <td class="value"></td>
                        </tr>
                    </table>
                </td>    
            </tr>

            <tr>
                <td colspan="6" style="padding-bottom:5px; padding-top:10px; border-top:1px solid #000; font-weight:bold; font-size:14px;">BAGIAN C : DAFTAR PEMOTONGAN/PEMUNGUTAN PPh OLEH PIHAK LAIN <br>DAN PPh YANG DITANGGUNG PEMERINTAH</td>
            </tr>
            <tr>
                <td colspan="6">
                    <table class="normal_teks table-detail" style="font-size: 11px;">
                        <tr>
                            <th rowspan="3">NO</th>
                            <th>NAMA PEMOTONG/</th>
                            <th>NPWP PEMOTONG/</th>
                            <th colspan="2">BUKTI PEMOTONGAN/</th>
                            <th colspan="2">JENIS PAJAK/</th>
                            <th>JUMLAH PPh YANG</th>
                        </tr>
                        <tr>
                            <th rowspan="2">PEMUNGUT PAJAK</th>
                            <th rowspan="2">PEMUNGUT PAJAK</th>
                            <th colspan="2">PEMUNGUTAN</th>
                            <th colspan="2">PPh PASAL 21/</th>
                            <th rowspan="2">DIPOTONG / DIPUNGUT</th>
                        </tr>
                        <tr>
                            <th>NOMOR</th>
                            <th>TANGGAL</th>
                            <th colspan="2">22/23/24/26/DTP</th>
                        </tr>
                        <tr style="font-size: 9px;">
                            <td align="center">(1)</td>
                            <td align="center">(2)</td>
                            <td align="center">(3)</td>
                            <td align="center">(4)</td>
                            <td align="center">(5)</td>
                            <td align="center" colspan="2">(6)</td>
                            <td align="center">(7)</td>
                        </tr>
                        <?php
                        $no=1;
                        $jml=0;
                        foreach ($tdkfinal as $key => $value) {
                        ?>
                            <tr>
                                <td><?php echo $no;?></td>
                                <td><?php echo $value['nama_pemotong'];?></td>
                                <td align="right"><?php echo $value['npwp_pemotong'];?></td>
                                <td align="right"><?php echo $value['nomor_bukti_potong'];?></td>
                                <td><?php echo date("d-m-y",strtotime($value['tanggal_bukti_potong']));?></td>
                                <td colspan="2">PPh 21</td>
                                <td align="right"><?php echo number_format($value['jumlah_pph'],0,",",".").",00";?></td>
                            </tr>
                        <?php
                        $jml = $jml+$value['jumlah_pph'];
                        $no++;
                        }
                        ?>
                        <tr>
                            <td class="index"></td>
                            <td colspan="4">JUMLAH BAGIAN C</td>
                            <td style="background:#999;" class="bgabu"></td>
                            <td>JBC</td>
                            <td align="right"><?php echo number_format($jml,0,",",".").",00";?></td>
                        </tr>
                    </table>
                </td>    
            </tr>
            
        </table>
        </td>
    </tr>
    <tr style="height:0px;">
        <td><img src="<?php echo Yii::app()->baseUrl;?>/images/kotak.jpg"></td>
        <td></td>
        <td><img src="<?php echo Yii::app()->baseUrl;?>/images/kotak.jpg"></td>
    </tr>
</table>
</div>
<br>
<div class="show">
<table border="0" cellpadding="0" cellspacing="0" class="normal_teks" width="100%" height="931" style="margin-left:auto;margin-right:auto;">
    <tr>
        <td><img src="<?php echo Yii::app()->baseUrl;?>/images/kotak.jpg"></td>
        <td></td>
        <td><img src="<?php echo Yii::app()->baseUrl;?>/images/kotak.jpg"></td>
    </tr>
    <tr>
        <td></td>
        <td height="931" valign="top">
        <table border="0" cellpadding="0" cellspacing="0" class="normal_teks" style="margin-top:0px;">
            <tr>
                <td colspan="2" style="font-size:14px;border:1px solid #000000; border-left: none; border-top: none; text-align:center; width:20%;" valign="top">
                <p style="font-weight: 100px;font-size: 30px;margin-bottom: 0px;">1770 S - II</p>
                <!-- <img src="<?php echo Yii::app()->baseUrl;?>/images/logoPajak.jpg"> -->
                  <b>KEMENTERIAN KEUANGAN RI<br />
        DIREKTORAT JENDERAL PAJAK</b></td>
                <td width="23" style="border:1px solid #000; width:55%; border-top:none;">
                <div style="text-align:center; font-size:20px; font-weight:bold;">
                    LAMPIRAN - II
                </div>
                <div style="text-align:center; font-size:16px; font-weight:bold;">
                    PAJAK PENGHASILAN WAJIB PAJAK ORANG PRIBADI
                </div>
                <div style="border-top:2px solid #000; text-align:left; margin-top: 5px; font-size: 11px; font-weight:500; padding:8px 10px 6px 10px;">
                    <ul style="margin-top: 0px;margin-bottom: 0px;">
                        <li>PENGHASILAN YANG DIKENAKAN PPh FINAL DAN/ATAU BERSIFAT FINAL</li>
                        <li>HARTA PADA AKHIR TAHUN</li>
                        <li>KEWAJIBAN/UTANG PADA AKHIR TAHUN</li>
                        <li>DAFTAR SUSUNAN ANGGOTA KELUARGA</li>
                    </ul>
                </div>
                </td>
                <td valign="top" style="width:20%; border:1px solid #000; border-top:none; border-right:none; padding-left:10px; font-size: 11px;">
                        <p style="font-weight: 100px;font-size: 30px;margin-bottom: 0px;text-align: center;"><?php echo $tahun; ?></p>
                </td>
            </tr>
            <tr>
                <td colspan="6" style="padding-bottom:5px; padding-top:10px; border-top:1px solid #000; font-weight:bold; font-size:14px;">IDENTITAS</td>
            </tr>
            <tr>
            <td colspan="6">
            <table style="width:100%; border:1px solid #000;">
            <tr>
            <td style="width:50%;">
                <table style="font-size:9px; width:100%;">
                <tr>
                    <td style="width:2px"></td>
                    <td style="width:147px">NPWP</td>
                    <td style="width:2px"> : </td>
                    <td colspan="6" style="border-bottom: 1px solid #000;width:240px;"><?php echo $data1721['V_NPWP_PRIBADI']; ?></td>
                </tr>
                <tr>
                    <td></td>
                    <td>NAMA WAJIB PAJAK</td>
                    <td> : </td>
                    <td colspan="6" style="border-bottom: 1px solid #000;"><?php echo $data1721['V_NAMA_KARYAWAN']; ?></td>
                </tr>
                </table>
            </td>
            </tr>
            </table>
            </td>
            </tr>
            <tr>
                <td colspan="6" style="padding-bottom:5px; padding-top:10px; border-top:1px solid #000; font-weight:bold; font-size:14px;">BAGIAN A : PENGHASILAN YANG DIKENAKAN PPh FINAL DAN/ATAU BERSIFAT FINAL</td>
            </tr>
            <tr>
                <td colspan="6">
                    <table class="normal_teks table-detail" style="font-size: 11px;">
                        <tr>
                            <th width="12px">NO</th>
                            <th>JENIS PENGHASILAN</th>
                            <th colspan="2">DASAR PENGENAAN PAJAK/ <br>PENGHASILAN BRUTO</th>
                            <th>PPh TERUTANG (Rp)</th>
                        </tr>
                        <tr style="font-size: 9px;">
                            <td align="center">(1)</td>
                            <td align="center">(2)</td>
                            <td colspan="2" align="center">(3)</td>
                            <td align="center">(4)</td>
                        </tr>
                        <tr>
                            <td class="index">1.</td>
                            <td>Bunga Deposito, Tabungan, Diskonto SBI, SURAT BERHARGA NEGARA</td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">2.</td>
                            <td>Bunga/Diskonto Obligasi </td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">3.</td>
                            <td>Penjualan Saham di Bursa Efek</td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">4.</td>
                            <td>Hadiah Undian</td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">5.</td>
                            <td>Pesangon, Tunjangan Hari Tua dan Tebusan Pensiun, yang dibayarkan sekaligus</td>
                            <td colspan="2" class="value"><?php echo  number_format($brutolamp2,0,",",".").",00"?></td>
                            <td class="value"><?php echo  number_format($pphlamp2,0,",",".").",00"?></td>
                        </tr>
                        <tr>
                            <td class="index">6.</td>
                            <td>Honorarium atas Beban APBN/APBD</td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">7.</td>
                            <td>Pengalihan Hak atas Tanah dan/atau Bangunan</td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">8.</td>
                            <td>Sewa atas Tanah dan/atau Bangunan</td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">9.</td>
                            <td>Bangunan yang Diterima Dalam Rangka Bangun guna serah</td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">10.</td>
                            <td>Bunga Simpanan Yang Dibayarkan Oleh Koperasi kepada anggota Koperasi</td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">11.</td>
                            <td>Penghasilan Dari Transaksi Derivatif</td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">12.</td>
                            <td>Deviden</td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">13.</td>
                            <td>Penghasilan Isteri Dari Satu Pemberi Kerja</td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">14.</td>
                            <td>Penghasilan Lain yang dikenakan Pajak Final dan/atau bersifat Final</td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                        </tr>
                        
                        <tr>
                            <td class="index"></td>
                            <td>JUMLAH BAGIAN A</td>
                            <td class="bgabu"></td>
                            <td>JBA</td>
                            <td class="value"></td>
                        </tr>
                    </table>
                </td>    
            </tr>
            
            <tr>
                <td colspan="6" style="padding-bottom:5px; padding-top:10px; border-top:1px solid #000; font-weight:bold; font-size:14px;">BAGIAN B : HARTA PADA AKHIR TAHUN</td>
            </tr>
            <tr>
                <td colspan="6">
                    <table class="normal_teks table-detail" style="font-size: 11px;word-wrap: break-word;">
                        <tr>
                            <th width="12px">NO</th>
                            <th>JENIS HARTA</th>
                            <th colspan="2">TAHUN <br>PEROLEHAN</th>
                            <th>HARGA PEROLEHAN (Rp)</th>
                            <th>KETERANGAN</th>
                        </tr>
                        <tr style="font-size: 9px;">
                            <td align="center">(1)</td>
                            <td align="center">(2)</td>
                            <td colspan="2" align="center">(3)</td>
                            <td align="center">(4)</td>
                            <td align="center">(4)</td>
                        </tr>
                        <tr>
                            <td class="index">1.</td>
                            <td></td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="index">2.</td>
                            <td></td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="index">3.</td>
                            <td></td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="index">4.</td>
                            <td></td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="index">5.</td>
                            <td></td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="index">6.</td>
                            <td></td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="index">7.</td>
                            <td></td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="index"></td>
                            <td>JUMLAH BAGIAN B</td>
                            <td class="bgabu"></td>
                            <td>JBB</td>
                            <td class="value"></td>
                            <td></td>
                        </tr>
                    </table>
                </td>    
            </tr>

            <tr>
                <td colspan="6" style="padding-bottom:5px; padding-top:10px; border-top:1px solid #000; font-weight:bold; font-size:14px;">BAGIAN C : KEWAJIBAN/UTANG PADA AKHIR TAHUN</td>
            </tr>
            <tr>
                <td colspan="6">
                    <table class="normal_teks table-detail" style="font-size: 11px;">
                        <tr>
                            <th>NO</th>
                            <th>NAMA <br>PEMBERI PINJAMAN</th>
                            <th>ALAMAT <br>PEMBERI PINJAMAN</th>
                            <th colspan="2">TAHUN <br> PEMINJAMAN</th>
                            <th>JUMLAH</th>
                        </tr>
                        <tr style="font-size: 9px;">
                            <td align="center">(1)</td>
                            <td align="center">(2)</td>
                            <td align="center">(3)</td>
                            <td align="center" colspan="2">(4)</td>
                            <td align="center">(5)</td>
                        </tr>
                        <tr>
                            <td class="index">1.</td>
                            <td></td>
                            <td></td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">2.</td>
                            <td></td>
                            <td></td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">3.</td>
                            <td></td>
                            <td></td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">4.</td>
                            <td></td>
                            <td></td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">5.</td>
                            <td></td>
                            <td></td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">6.</td>
                            <td></td>
                            <td></td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index">dst.</td>
                            <td></td>
                            <td></td>
                            <td colspan="2"></td>
                            <td class="value"></td>
                        </tr>
                        <tr>
                            <td class="index"></td>
                            <td colspan="2">JUMLAH BAGIAN C</td>
                            <td style="background:#999;" class="bgabu"></td>
                            <td>JBC</td>
                            <td class="value"></td>
                        </tr>
                    </table>
                </td>    
            </tr>

            <tr>
                <td colspan="6" style="padding-bottom:5px; padding-top:10px; border-top:1px solid #000; font-weight:bold; font-size:14px;">BAGIAN D : DAFTAR SUSUNAN ANGGOTA KELUARGA</td>
            </tr>
            <tr>
                <td colspan="6">
                    <table class="normal_teks table-detail" style="font-size: 11px;">
                        <tr>
                            <th>NO</th>
                            <th>NAMA</th>
                            <th>TANGGAL LAHIR</th>
                            <th>HUBUNGAN KELUARGA</th>
                            <th>KETERANGAN</th>
                        </tr>
                        <tr style="font-size: 9px;">
                            <td align="center">(1)</td>
                            <td align="center">(2)</td>
                            <td align="center">(3)</td>
                            <td align="center">(4)</td>
                            <td align="center">(5)</td>
                        </tr>
                        <tr>
                            <td class="index">1.</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="index">2.</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="index">3.</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="index">4.</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                        <tr>
                            <td class="index">5.</td>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </table>
                </td>    
            </tr>
            
        </table>
        </td>
    </tr>
    <tr>
        <td><img src="<?php echo Yii::app()->baseUrl;?>/images/kotak.jpg"></td>
        <td></td>
        <td><img src="<?php echo Yii::app()->baseUrl;?>/images/kotak.jpg"></td>
    </tr>
</table>
</div>
</body>
</html>