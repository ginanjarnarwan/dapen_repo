<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="<?php echo Yii::app()->baseUrl;?>/style/default.css" rel="stylesheet" type="text/css">

<title>FORM SPT TIDAK FINAL</title>
</head>
	
<style>
.table-detail { width:100%; border:1px solid #000; padding:5px; border-collapse:collapse; }
.table-detail th, .table-detail td { border:1px solid #000; padding:2px; }
.table-detail .index { text-align:center; }
.table-detail .value { text-align:right; }
.pad-kecil tr td {
    padding: 0px;
}
body{
    margin: 0px;
    font-family: "Lato", "Helvetica Neue", Helvetica, Arial, sans-serif !important;
}
.btn-success {
    color: #fff;
    background-color: #5cb85c;
    border-color: #4cae4c;
}
.btn {
    display: inline-block;
    margin-bottom: 0;
    font-weight: normal;
    text-align: center;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    background-image: none;
    border: 1px solid transparent;
    white-space: nowrap;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    border-radius: 4px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}
.btn-success:active:hover, .btn-success.active:hover, .open>.dropdown-toggle.btn-success:hover, .btn-success:active:focus, .btn-success.active:focus, .open>.dropdown-toggle.btn-success:focus, .btn-success:active.focus, .btn-success.active.focus, .open>.dropdown-toggle.btn-success.focus {
    color: #fff;
    background-color: #398439;
    border-color: #255625;
}
.btn-success:hover {
    color: #fff;
    background-color: #449d44;
    border-color: #398439;
}
</style>
<body>
<center>
    <script src="https://code.jquery.com/jquery-1.11.3.js"></script>
    <?php 
    if(Yii::app()->user->roles == "Admin_witel"){
    ?>
    <div id="form_sarana">
        Pengiriman SPT dilakukan via: 
        <select class="form-control" name="sarana" id="sarana">
            <option value="Whatsapp">Whatsapp</option>
            <option value="Email">Email</option>
        </select>
    </div>
    <?php
    }
    ?>
    <button id="btnExportpdf" class="btn btn-outline btn-success" onclick="printPdf('<?php echo Yii::app()->user->roles; ?>')">
        <i class="fa fa-file-pdf-o"></i> 
        Print Pdf
    </button>
    <script>
    function printPdf(roles){
        $("#btnExportpdf").css("display","none");
        $("#form_sarana").css("display","none");

        var sarana      = 'Pribadi';
        if(roles=='Admin_witel'){
            sarana      = $("#sarana").val();
        }
        var telkomnik   = '<?php echo $tdkfinal[0]['nik']; ?>';
        var jenis_form  = '1721-VI';
        var nomor_bukti = '<?php echo implode(' ',str_split($tdkfinal[0]['nomor_bukti_potong'])); ?>';
        var base_url    = '<?php echo Yii::app()->getBaseUrl(true); ?>';
        var url         = base_url+"/index.php?r=cetakform/downlog";

        var dummy = new iframeform(url);
        dummy.addParameter('telkomnik',telkomnik);
        dummy.addParameter('sarana',sarana);
        dummy.addParameter('jenis_form',jenis_form);
        dummy.addParameter('nomor_bukti',nomor_bukti);
        dummy.send();
        
        window.print();
    }
    function iframeform(url){
        var object = this;
        object.time = new Date().getTime();
        object.form = $('<form action="'+url+'" target="iframe'+object.time+'" method="post" style="display:none;" id="form'+object.time+'" name="form'+object.time+'"></form>');

        object.addParameter = function(parameter,value)
        {
            $("<input type='hidden' />")
             .attr("name", parameter)
             .attr("value", value)
             .appendTo(object.form);
        }

        object.send = function()
        {
            var iframe = $('<iframe data-time="'+object.time+'" style="display:none;" id="iframe'+object.time+'"></iframe>');
            $( "body" ).append(iframe); 
            $( "body" ).append(object.form);
            object.form.submit();
            iframe.load(function(){  $('#form'+$(this).data('time')).remove();  $(this).remove(); window.close();  });
        }
    }
    </script>
</center>
<form name="spt" id="id_spt" method="post">
<table border="0" cellpadding="0" cellspacing="0" class="normal_teks" width="100%" height="100%" style="margin-left:auto;margin-right:auto;">
	<tr>
		<td><img src="<?php echo Yii::app()->baseUrl;?>/images/kotak.jpg"></td>
		<td></td>
		<td><img src="<?php echo Yii::app()->baseUrl;?>/images/kotak.jpg"></td>
	</tr>
	<tr>
		<td></td>
		<td height="931" valign="top">
		<table border="0" cellpadding="0" cellspacing="0" class="normal_teks" style="margin-top:0px;">
			<tr>
				<td colspan="2" style="font-size:14px;border:1px solid #000000; border-left: none; border-top: none; text-align:center; width:15%;">
                <img src="<?php echo Yii::app()->baseUrl;?>/images/logoPajak.jpg">
                <br />
				  <b>KEMENTERIAN KEUANGAN RI<br />
        DIREKTORAT JENDERAL PAJAK</b></td>
				<td width="23" style="border:1px solid #000; width:60%; border-top:none;">
                <div style="padding:10px; text-align:center; font-size:16px; font-weight:bold; height:50%;">
                	BUKTI PEMOTONGAN PAJAK PENGHASILAN PASAL 21 (TIDAK FINAL)<br />
                ATAU PASAL 26</div>
                <div style="border-top:2px solid #000; text-align:left; margin-top: 20px; font-size:12px; font-weight:500; padding:8px 10px 6px 10px;">
                	NOMOR : &nbsp;&nbsp; <?php echo implode(' ',str_split($tdkfinal[0]['nomor_bukti_potong']));?>
                </div>
                </td>
				<td style="width:20%; border:1px solid #000; border-top:none; border-right:none; padding-left:10px; font-size:12px;">
                <span style="font-size:14px; font-weight:700; margin-left:30px;">
                	FORMULIR 1721 - VI
                </span>
                <br />
                Lembar ke-1 : untuk Penerima Penghasilan<br />
                Lembar ke-2 : untuk Pemotong<br />
                </td>
			</tr>
            <tr>
            	<td colspan="6" style="padding-bottom:5px; padding-top:10px; border-top:1px solid #000; font-weight:bold; font-size:14px;">A. IDENTITAS PENERIMA PENGHASILAN YANG DIPOTONG</td>
            </tr>
            <tr>
            <td colspan="6">
            <table style="width:100%; border:1px solid #000;">
            <tr>
            <td style="width:50%;">
            	<table style="font-size:11px; width:100%;">
                <tr>
                    <td style="width:2px">1.</td>
                    <td style="width:147px">NPWP</td>
                    <td style="width:2px"> : </td>
                    <td style="border-bottom: 1px solid #000;width:240px;"><?php echo $tdkfinal[0]['npwp']; ?></td>
                    <td style="width:10px;"></td>
                    <td style="width:2px">2.</td>
                    <td style="width:132px">NIK / NO PASPOR</td>
                    <td style="width:2px"> : </td>
                    <td style="border-bottom: 1px solid #000;"><?php echo $tdkfinal[0]['nik']; ?></td>
                </tr>
                <tr>
                    <td>3.</td>
                    <td>NAMA</td>
                    <td> : </td>
                    <td colspan="6" style="border-bottom: 1px solid #000;"><?php echo $tdkfinal[0]['nama']; ?></td>
                </tr>
                <tr>
                    <td>4.</td>
                    <td>ALAMAT</td>
                    <td> : </td>
                    <td colspan="6" style="border-bottom: 1px solid #000;"><?php echo $tdkfinal[0]['alamat']; ?></td>
                </tr>
                <tr>
                    <td>5.</td>
                    <td>WAJIB PAJAK LUAR NEGERI</td>
                    <td> : </td>
                    <td>
                          <span><img src="<?php echo Yii::app()->baseUrl;?>/images/<?php echo $tdkfinal[0]['wp_luar_negeri'] == 'Y' ? 'cross' : 'cross0'; ?>.JPG" width="20" height="20" /> YA</span>
                    </td>
                    <td></td>
                    <td>6.</td>
                    <td>KODE NEGARA DOMISILI</td>
                    <td> : </td>
                    <td style="border-bottom: 1px solid #000;"><?php echo $tdkfinal[0]['kode_negara'];?></td>
                </tr>
                </table>
            </td>
            <!-- <td>
            	<table style="font-size:10px; width:100%;">
                <tr>
                    <td>6.</td>
                    <td>KODE NEGARA DOMISILI</td>
                    <td> : </td>
                    <td>_____</td>
                </tr>
                </table>
            </td> -->
            </tr>
            </table>
            </td>
            </tr>
            
            <tr>
            	<td colspan="6" style="padding-top:10px; padding-bottom:5px; font-weight:bold; font-size:14px;">B. PPh PASAL 21 DAN/ ATAU PASAL 26 YANG DIPOTONG</td>
            </tr>
            <tr>
                <td colspan="6">
                    <table class="normal_teks table-detail" style="font-size:11px;">
                        <tr>
                            <th>KODE OBJEK PAJAK</th>
                            <th>JUMLAH PENGHASILAN BRUTO (Rp)</th>
                            <th>DASAR PENGENAAN PAJAK (Rp)</th>
                            <th>TARIF LEBIH TINGGI 20% (TIDAK BER-NPWP)</th>
                            <th>TARIF (%)</th>
                            <th>PPh DIPOTONG (Rp)</th>
                        </tr>
                        <tr style="background:#d6d6d6;">
                            <td style="text-align:center;">(1)</td>
                            <td style="text-align:center;">(2)</td>
                            <td style="text-align:center;">(3)</td>
                            <td style="text-align:center;">(4)</td>
                            <td style="text-align:center;">(5)</td>
                            <td style="text-align:center;">(6)</td>
                        </tr>
                        <?php
                        foreach ($tdkfinal as $key => $value) {
                        ?>
                        <tr>
                            <td style="text-align:center;"><?php echo $value['kode_pajak'];?></td>
                            <td style="text-align:right;"><?php echo number_format($value['jumlah_bruto'],0,",",".");?></td>
                            <td style="text-align:right;"><?php echo number_format($value['jumlah_dpp'],0,",",".");?></td>
                            <td style="text-align:center;"><img src="<?php echo Yii::app()->baseUrl;?>/images/<?php echo $value['tanpa_npwp'] == 'Y' ? 'cross' : 'cross0'; ?>.JPG" width="20" height="20" /></td>
                            <td style="text-align:center;"><?php echo $value['tarif'];?></td>
                            <td style="text-align:right;"><?php echo number_format($value['jumlah_pph'],0,",",".");?></td>
                        </tr>
                        <?php } ?>
                    </table>
                </td>    
            </tr>
			
            <tr>
            	<td colspan="6" style="padding-top:10px; padding-bottom:5px; font-weight:bold; font-size:14px;">C. IDENTITAS PEMOTONG</td>
            </tr>
            <tr>
            <td colspan="6">
            <table class="normal_teks" style="width:100%; border:1px solid #000; font-size:11px;">
            	<tr>
                	<th style="text-align:left;">1.</th>
                    <th style="text-align:left;">NPWP</th>
                    <td style="width:60%"><?php echo $tdkfinal[0]['npwp_pemotong'];?></td>
                    
                    <th style="width:30%">3. TANGGAL & TANDA TANGAN</th>
                    <td rowspan="2" style="border:1px solid #000">
						<?php                        
                        if (!empty($paraf))
                        { 
                        $gambar = $paraf['V_PATH_TTD'];
                        ?>
                          <img src="<?php echo  Yii::app()->baseUrl."/".$gambar ?>" width="140" align="absmiddle"/>
                          <?php                        }
                        else
                        {
                        echo "<br>";
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                	<th style="text-align:left;">2.</th>
                    <th style="text-align:left;">NAMA</th>
                    <td><?php echo $tdkfinal[0]["nama_pemotong"]; ?></td>
                    <th>
                    	<?php echo date("d-m-Y",strtotime($tdkfinal[0]["tanggal_bukti_potong"])); ?>
                        
                       
                    </th>
                  </tr>
            </table>
            </td>
            </tr>
            <tr>
                <td colspan="6" style="padding-top:10px; padding-bottom:5px; font-weight:bold;"></td>
            </tr>
            <tr>
                <td colspan="6">
                    <table style="width:100%; border:1px solid #000;border-collapse: collapse;">
                        <tr>
                            <th style="border:1px solid #000;background:#d6d6d6;font-size:14px;">KODE OBJEK PAJAK PENGHASILAN PASAL 21 (TIDAK FINAL) ATAU PASAL 26</th>
                        </tr>
                        <tr>
                            <td>
                                <table style="font-size:11px;width:100%;" class="pad-kecil">
                                    <?php
                                    foreach ($kodeobjek as $key => $value) {
                                    ?>
                                    <tr><td colspan="3" height="10"></td></tr>
                                    <tr>
                                        <td></td>
                                        <td colspan="2"><?php echo $value['kategori']?></td>
                                    </tr>
                                        <?php
                                        $no = 1;
                                        foreach ($value['value'] as $key2 => $value2) {
                                        ?>
                                        <tr>
                                            <td><?php echo $no; ?>.</td>
                                            <td width="15%"><?php echo $value2['kode_objek_pajak'];?></td>
                                            <td><?php echo $value2['deskripsi'];?></td>
                                        </tr>
                                        <?php $no++;?>
                                        <?php } ?>
                                    <?php } ?>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
		</table>
		</td>
		<td></td>
	</tr>
	<tr>
		<td><img src="<?php echo Yii::app()->baseUrl;?>/images/kotak.jpg"></td>
		<td></td>
		<td><img src="<?php echo Yii::app()->baseUrl;?>/images/kotak.jpg"></td>
	</tr>
</table>
</form>
</body>
</html>