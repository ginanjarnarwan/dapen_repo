<script language="javascript" type="text/javascript">
function money_format(number)
    {
        
		if (isNaN(number)) return "";
        var str = new String(number);
        var result = "" ,len = str.length;           
        for(var i=len-1;i>=0;i--)
        {           
            if ((i+1)%3 == 0 && i+1!= len) result += ".";
            result += str.charAt(len-1-i);
        }       
        return result;
    }

function int_format(string)
    {
        var result = "";
		var arr_string = string.split(".");
		var result = arr_string.join("");
		return result;
    }
	 
function cek_status_input(form) {

	 //masukkan data ke variable
	 var isi_51 = int_format(document.getElementById('isian_51').value);
	 var isi_52 = int_format(document.getElementById('isian_52').value);
	 var isi_53 = int_format(document.getElementById('isian_53').value);
	 var isi_54 = int_format(document.getElementById('isian_54').value);
	 var isi_55 = int_format(document.getElementById('isian_55').value);
	 var isi_56 = int_format(document.getElementById('isian_56').value);
	 var isi_57 = int_format(document.getElementById('isian_57').value);
	 //aturan tampilkan
	 document.getElementById('isian_51').value = money_format(isi_51);
	 document.getElementById('isian_52').value = money_format(isi_52);
	 document.getElementById('isian_53').value = money_format(isi_53);
	 document.getElementById('isian_54').value = money_format(isi_54);
	 document.getElementById('isian_55').value = money_format(isi_55);
	 document.getElementById('isian_56').value = money_format(isi_56);
	 document.getElementById('isian_57').value = money_format(isi_57);
	 //jumlahkan
	 document.formulir.isian_58.value = Math.ceil(isi_51)+Math.ceil(isi_52)+Math.ceil(isi_53)+Math.ceil(isi_54)+Math.ceil(isi_55)+Math.ceil(isi_56)+Math.ceil(isi_57);
	 document.formulir.isian_58_view.value = money_format(document.getElementById('isian_58').value);
	 
	 //masukkan ke variable
	 var isi_1 = int_format(document.getElementById('isian_1').value);
	 var isi_2 = int_format(document.getElementById('isian_58').value);
	 var isi_3 = int_format(document.getElementById('isian_3').value);
	 //format keluaran
	 document.getElementById('isian_1').value = money_format(isi_1);
	 document.getElementById('isian_2_view').value = money_format(isi_2);
	 document.getElementById('isian_3').value = money_format(isi_3);
	 
	 document.formulir.isian_2.value = Math.ceil(isi_2);
   document.formulir.isian_2_view.value = money_format(document.getElementById('isian_2').value);
	 document.formulir.isian_4.value = Math.ceil(isi_1) + Math.ceil(isi_2) + Math.ceil(isi_3);
	 document.formulir.isian_4_view.value = money_format(document.getElementById('isian_4').value);
	
	 //masukkan ke variable
	 var isi_4 = int_format(document.getElementById('isian_4').value);
	 var isi_5 = int_format(document.getElementById('isian_5').value);
	 //format keluaran
	 document.getElementById('isian_4').value = money_format(isi_4);
	 document.getElementById('isian_5').value = money_format(isi_5);
	 document.formulir.isian_6.value = Math.abs(Math.ceil(isi_4) - Math.ceil(isi_5));
	 document.formulir.isian_6_view.value = money_format(document.getElementById('isian_6').value);
	 
	 //masukkan ke variable
	 var isi_6 = int_format(document.getElementById('isian_6').value);
	 var isi_7 = int_format(document.getElementById('isian_7').value);
	 //format keluaran
	 document.getElementById('isian_6').value = money_format(isi_6);
	 document.getElementById('isian_7').value = money_format(isi_7);
	 document.formulir.isian_8.value = Math.abs(Math.ceil(isi_6) - Math.ceil(isi_7));
	 document.formulir.isian_8_view.value = money_format(document.getElementById('isian_8').value);

	 //masukkan ke variable
	 var isi_59 = int_format(document.getElementById('isian_59').value);
	 var isi_60 = int_format(document.getElementById('isian_60').value);
	 var isi_61 = int_format(document.getElementById('isian_61').value);
	 var isi_62 = int_format(document.getElementById('isian_62').value);
	 var isi_63 = int_format(document.getElementById('isian_63').value);
	 //format tampilan
	 document.getElementById('isian_59').value = money_format(isi_59);
	 document.getElementById('isian_60').value = money_format(isi_60);
	 document.getElementById('isian_61').value = money_format(isi_61);
	 document.getElementById('isian_62').value = money_format(isi_62);
	 document.getElementById('isian_63').value = money_format(isi_63);
	 document.formulir.isian_64.value = Math.ceil(isi_59)+Math.ceil(isi_60)+Math.ceil(isi_61)+Math.ceil(isi_62)+Math.ceil(isi_63);
	 document.formulir.isian_64_view.value = money_format(document.getElementById('isian_64').value);

	 //masukkan ke variable
	 var isi_71 = int_format(document.getElementById('isian_71').value);
	 var isi_78 = int_format(document.getElementById('isian_78').value);
	 var isi_85 = int_format(document.getElementById('isian_85').value);
	 var isi_92 = int_format(document.getElementById('isian_92').value);
	 var isi_99 = int_format(document.getElementById('isian_99').value);
	 //format tampilan
	 document.getElementById('isian_71').value = money_format(isi_71);
	 document.getElementById('isian_78').value = money_format(isi_78);
	 document.getElementById('isian_85').value = money_format(isi_85);
	 document.getElementById('isian_92').value = money_format(isi_92);
	 document.getElementById('isian_99').value = money_format(isi_99);
	 document.formulir.isian_100.value = Math.ceil(isi_71)+Math.ceil(isi_78)+Math.ceil(isi_85)+Math.ceil(isi_92)+Math.ceil(isi_99);
	 document.formulir.isian_100_view.value = money_format(document.getElementById('isian_100').value);

	 //masukkan ke variable
	 var isi_9 = int_format(document.getElementById('isian_9').value);
	 var isi_10 = int_format(document.getElementById('isian_10').value);
	 //format keluaran
	 document.getElementById('isian_9').value = money_format(isi_9);
	 document.getElementById('isian_10').value = money_format(isi_10);
	 document.formulir.isian_11.value = Math.ceil(isi_9) + Math.ceil(isi_10);
	 document.formulir.isian_11_view.value = money_format(document.getElementById('isian_11').value);
	  
	 //masukkan ke variable 
	 var isi_11 = int_format(document.getElementById('isian_11').value);
	 var isi_12 = int_format(document.getElementById('isian_100').value);
	 //format keluaran
	 document.getElementById('isian_11').value = money_format(isi_11);
	 document.getElementById('isian_12_view').value = money_format(Math.ceil(isi_12));
	 document.formulir.isian_13.value = Math.abs(Math.ceil(isi_11) - Math.ceil(isi_12));
	 document.formulir.isian_13_view.value = money_format(document.getElementById('isian_13').value);
	 
	 //masukkin ke variable
	 var isi_14a = int_format(document.getElementById('isian_14a').value);
	 var isi_14b = int_format(document.getElementById('isian_14b').value);
	 var isi_14c = int_format(document.getElementById('isian_14c').value);
	 //format keluaran
	 document.getElementById('isian_14a').value = money_format(isi_14a);
	 document.getElementById('isian_14b').value = money_format(isi_14b);
	 document.getElementById('isian_14c').value = money_format(isi_14c);
	 document.formulir.isian_15.value = Math.ceil(isi_14a) +  Math.ceil(isi_14b) + Math.ceil(isi_14c);
	 document.formulir.isian_15_view.value = money_format(document.getElementById('isian_15').value);
	 //masukin ke variable
	 var isi_13 = int_format(document.getElementById('isian_13').value);
	 var isi_15 = int_format(document.getElementById('isian_15').value);
	 //format keluaran
	 document.getElementById('isian_13').value = money_format(isi_13);
	 document.getElementById('isian_15').value = money_format(isi_15);
	 document.formulir.isian_16.value = Math.abs(Math.ceil(isi_13) - Math.ceil(isi_15));
	 document.formulir.isian_16_view.value = money_format(document.getElementById('isian_16').value);
}
function cek_status_input5(form) {
	 //masukkin ke variable
	 var isi_3_2 = int_format(document.getElementById('isian_3_2').value);
	 var isi_3_4 = int_format(document.getElementById('isian_3_4').value);
	 var isi_3_6 = int_format(document.getElementById('isian_3_6').value);
	 var isi_3_8 = int_format(document.getElementById('isian_3_8').value);
	 var isi_3_10 = int_format(document.getElementById('isian_3_10').value);
	 var isi_3_12 = int_format(document.getElementById('isian_3_12').value);
	 var isi_3_14 = int_format(document.getElementById('isian_3_14').value);
	 var isi_3_16 = int_format(document.getElementById('isian_3_16').value);
	 var isi_3_18 = int_format(document.getElementById('isian_3_18').value);
	 var isi_3_20 = int_format(document.getElementById('isian_3_20').value);
	 var isi_3_22 = int_format(document.getElementById('isian_3_22').value);
	 var isi_3_24 = int_format(document.getElementById('isian_3_24').value);
	 //format tampilan
	 document.getElementById('isian_3_2').value = money_format(isi_3_2);
	 document.getElementById('isian_3_4').value = money_format(isi_3_4);
	 document.getElementById('isian_3_6').value = money_format(isi_3_6);
	 document.getElementById('isian_3_8').value = money_format(isi_3_8);
	 document.getElementById('isian_3_10').value = money_format(isi_3_10);
	 document.getElementById('isian_3_12').value = money_format(isi_3_12);
	 document.getElementById('isian_3_14').value = money_format(isi_3_14);
	 document.getElementById('isian_3_16').value = money_format(isi_3_16);
	 document.getElementById('isian_3_18').value = money_format(isi_3_18);
	 document.getElementById('isian_3_20').value = money_format(isi_3_20);
	 document.getElementById('isian_3_22').value = money_format(isi_3_22);
	 document.getElementById('isian_3_24').value = money_format(isi_3_24);
	 document.formulir.isian_3_25.value = Math.ceil(isi_3_2)+Math.ceil(isi_3_4)+Math.ceil(isi_3_6)+Math.ceil(isi_3_8)+Math.ceil(isi_3_10)+																	Math.ceil(isi_3_12)+Math.ceil(isi_3_14)+Math.ceil(isi_3_16)+Math.ceil(isi_3_18)+Math.ceil(isi_3_20)+
	 																			Math.ceil(isi_3_22)+Math.ceil(isi_3_24);
	 document.formulir.isian_3_25_view.value = money_format(document.getElementById('isian_3_25').value);
	 
	 //buat yg Dasar pengenaan pajak penghasilan bruto
	 //masukkin ke variable
	 var isi_3_1 = int_format(document.getElementById('isian_3_1').value);
	 var isi_3_3 = int_format(document.getElementById('isian_3_3').value);
	 var isi_3_5 = int_format(document.getElementById('isian_3_5').value);
	 var isi_3_7 = int_format(document.getElementById('isian_3_7').value);
	 var isi_3_9 = int_format(document.getElementById('isian_3_9').value);
	 var isi_3_11 = int_format(document.getElementById('isian_3_11').value);
	 var isi_3_13 = int_format(document.getElementById('isian_3_13').value);
	 var isi_3_15 = int_format(document.getElementById('isian_3_15').value);
	 var isi_3_17 = int_format(document.getElementById('isian_3_17').value);
	 var isi_3_19 = int_format(document.getElementById('isian_3_19').value);
	 var isi_3_21 = int_format(document.getElementById('isian_3_21').value);
	 var isi_3_23 = int_format(document.getElementById('isian_3_23').value);
	 //format tampilan
	 document.getElementById('isian_3_1').value = money_format(isi_3_1);
	 document.getElementById('isian_3_3').value = money_format(isi_3_3);
	 document.getElementById('isian_3_5').value = money_format(isi_3_5);
	 document.getElementById('isian_3_7').value = money_format(isi_3_7);
	 document.getElementById('isian_3_9').value = money_format(isi_3_9);
	 document.getElementById('isian_3_11').value = money_format(isi_3_11);
	 document.getElementById('isian_3_13').value = money_format(isi_3_13);
	 document.getElementById('isian_3_15').value = money_format(isi_3_15);
	 document.getElementById('isian_3_17').value = money_format(isi_3_17);
	 document.getElementById('isian_3_19').value = money_format(isi_3_19);
	 document.getElementById('isian_3_21').value = money_format(isi_3_21);
	 document.getElementById('isian_3_23').value = money_format(isi_3_23);
	 document.formulir.isian_3_25b.value = Math.ceil(isi_3_1)+Math.ceil(isi_3_3)+Math.ceil(isi_3_5)+Math.ceil(isi_3_7)+Math.ceil(isi_3_9)+																	Math.ceil(isi_3_11)+Math.ceil(isi_3_13)+Math.ceil(isi_3_15)+Math.ceil(isi_3_17)+Math.ceil(isi_3_19)+
	 																			Math.ceil(isi_3_21)+Math.ceil(isi_3_23);
	 document.formulir.isian_3_25b_view.value = money_format(document.getElementById('isian_3_25b').value);
}
function cek_status_input6(form) {

	 //masukkan ke variable
	 var isi_3_29 = int_format(document.getElementById('isian_3_29').value);
	 var isi_3_34 = int_format(document.getElementById('isian_3_34').value);
	 var isi_3_39 = int_format(document.getElementById('isian_3_39').value);
	 var isi_3_44 = int_format(document.getElementById('isian_3_44').value);
	 var isi_3_49 = int_format(document.getElementById('isian_3_49').value);
	 //format tampilan
	 document.getElementById('isian_3_29').value = money_format (isi_3_29);
	 document.getElementById('isian_3_34').value = money_format (isi_3_34);
	 document.getElementById('isian_3_39').value = money_format (isi_3_39);
	 document.getElementById('isian_3_44').value = money_format (isi_3_44);
	 document.getElementById('isian_3_49').value = money_format (isi_3_49);
	 document.formulir.isian_3_51.value = Math.ceil(isi_3_29)+Math.ceil(isi_3_34)+Math.ceil(isi_3_39)+Math.ceil(isi_3_44)+Math.ceil(isi_3_49);
	 document.formulir.isian_3_51_view.value = money_format(document.getElementById('isian_3_51').value);
	 //masukkan ke variable
     var isi_3_65 = int_format(document.getElementById('isian_3_65').value);
	 var isi_3_70 = int_format(document.getElementById('isian_3_70').value);
	 var isi_3_75 = int_format(document.getElementById('isian_3_75').value);
	 var isi_3_80 = int_format(document.getElementById('isian_3_80').value);
	 var isi_3_85 = int_format(document.getElementById('isian_3_85').value);
	 //format tampilan
	 document.getElementById('isian_3_65').value = money_format(isi_3_65);
	 document.getElementById('isian_3_70').value = money_format(isi_3_70);
	 document.getElementById('isian_3_75').value = money_format(isi_3_75);
	 document.getElementById('isian_3_80').value = money_format(isi_3_80);
	 document.getElementById('isian_3_85').value = money_format(isi_3_85);
	 document.formulir.isian_3_86.value = Math.ceil(isi_3_65)+Math.ceil(isi_3_70)+Math.ceil(isi_3_75)+Math.ceil(isi_3_80)+Math.ceil(isi_3_85);
	 document.formulir.isian_3_86_view.value = money_format(document.getElementById('isian_3_86').value);
}
function set_15()
{
//a=document.getElementById('id_11a').value;
//b=document.getElementById('id_12a').value;
//c=document.getElementById('id_14').value;
//if (a=="")
//	a=0;
//if (b=="")
//	b=0;
//document.getElementById('id_13a').value=parseInt(a)+parseInt(b);
//
//if (c=="")
//	c=0;
//document.getElementById('id_15').value=parseInt(document.getElementById('id_13a').value)+parseInt(c)+parseInt(document.getElementById('id_1').value);
a1=document.getElementById('lam11a').value;
a2=document.getElementById('lam12a').value;
a3=document.getElementById('lam13a').value;
a4=document.getElementById('lam14a').value;
a5=document.getElementById('lam15a').value;
a6=document.getElementById('lam16a').value;
a7=document.getElementById('lam17a').value;
a8=document.getElementById('lam18a').value;
a9=document.getElementById('lam19a').value;
a10=document.getElementById('lam20a').value;
b1=document.getElementById('lam21a').value;
b2=document.getElementById('lam22a').value;
c1=document.getElementById('lam31').value;
c2=document.getElementById('lam32').value;
c3=document.getElementById('lam33').value;
c4=document.getElementById('lam34').value;
c5=document.getElementById('lam35').value;
if (a1=="")
	a1=0;
if (a2=="")
	a2=0;
if (a3=="")
	a3=0;
if (a4=="")
	a4=0;
if (a5=="")
	a5=0;
if (a6=="")
	a6=0;
if (a7=="")
	a7=0;
if (a8=="")
	a8=0;
if (a9=="")
	a9=0;
if (a10=="")
	a10=0;
if (b1=="")
	b1=0;
if (b2=="")
	b2=0;
if (c1=="")
	c1=0;
if (c2=="")
	c2=0;
if (c3=="")
	c3=0;
if (c4=="")
	c4=0;
if (c5=="")
	c5=0;
document.getElementById('jum_1a').value=parseInt(a1)+parseInt(a2)+parseInt(a3)+parseInt(a4)+parseInt(a5)+parseInt(a6)+parseInt(a7)+parseInt(a8)+parseInt(a9)+parseInt(a10);
document.getElementById('jum_2a').value=parseInt(b1)+parseInt(b2);
document.getElementById('jum_3').value=parseInt(c1)+parseInt(c2)+parseInt(c3)+parseInt(c4)+parseInt(c5);
document.getElementById('id_11a').value=parseInt(document.getElementById('jum_1a').value);
document.getElementById('id_12a').value=parseInt(document.getElementById('jum_2a').value);
document.getElementById('id_13a').value=parseInt(document.getElementById('id_11a').value)+parseInt(document.getElementById('id_12a').value);

document.getElementById('id_14').value=parseInt(document.getElementById('jum_3').value);

document.getElementById('id_15').value=parseInt(document.getElementById('id_13a').value)+parseInt(document.getElementById('id_14').value)+
parseInt(document.getElementById('id_1').value);
}
function set_16()
{
//d=document.getElementById('id_11b').value;
//e=document.getElementById('id_12b').value;
//if (d=="")
//	d=0;
//if (e=="")
//	e=0;
//document.getElementById('id_13b').value=parseInt(d)+parseInt(e);
//document.getElementById('id_16').value=parseInt(document.getElementById('id_13b').value)+parseInt(document.getElementById('id_6').value);
a1=document.getElementById('lam11b').value;
a2=document.getElementById('lam12b').value;
a3=document.getElementById('lam13b').value;
a4=document.getElementById('lam14b').value;
a5=document.getElementById('lam15b').value;
a6=document.getElementById('lam16b').value;
a7=document.getElementById('lam17b').value;
a8=document.getElementById('lam18b').value;
a9=document.getElementById('lam19b').value;
a10=document.getElementById('lam20b').value;
b1=document.getElementById('lam21b').value;
b2=document.getElementById('lam22b').value;
if (a1=="")
	a1=0;
if (a2=="")
	a2=0;
if (a3=="")
	a3=0;
if (a4=="")
	a4=0;
if (a5=="")
	a5=0;
if (a6=="")
	a6=0;
if (a7=="")
	a7=0;
if (a8=="")
	a8=0;
if (a9=="")
	a9=0;
if (a10=="")
	a10=0;
if (b1=="")
	b1=0;
if (b2=="")
	b2=0;
document.getElementById('jum_1b').value=parseInt(a1)+parseInt(a2)+parseInt(a3)+parseInt(a4)+parseInt(a5)+parseInt(a6)+parseInt(a7)+parseInt(a8)+parseInt(a9)+parseInt(a10);
document.getElementById('jum_2b').value=parseInt(b1)+parseInt(b2);
document.getElementById('id_11b').value=parseInt(document.getElementById('jum_1b').value);
document.getElementById('id_12b').value=parseInt(document.getElementById('jum_2b').value);
document.getElementById('id_13b').value=parseInt(document.getElementById('jum_1b').value)+parseInt(document.getElementById('jum_2b').value);
document.getElementById('id_16').value=parseInt(document.getElementById('id_13b').value)+parseInt(document.getElementById('id_6').value);
}
function set_17()
{
a1=document.getElementById('lam41').value;
a2=document.getElementById('lam42').value;
a3=document.getElementById('lam43').value;
a4=document.getElementById('lam44').value;
a5=document.getElementById('lam45').value;
if (a1=="")
	a1=0;
if (a2=="")
	a2=0;
if (a3=="")
	a3=0;
if (a4=="")
	a4=0;
if (a5=="")
	a5=0;
document.getElementById('lam4').value=parseInt(a1)+parseInt(a2)+parseInt(a3)+parseInt(a4)+parseInt(a5);
document.getElementById('id_17').value=parseInt(a1)+parseInt(a2)+parseInt(a3)+parseInt(a4)+parseInt(a5);
}
function set_18()
{
a1=document.getElementById('lam51').value;
a2=document.getElementById('lam52').value;
a3=document.getElementById('lam53').value;
a4=document.getElementById('lam54').value;
if (a1=="")
	a1=0;
if (a2=="")
	a2=0;
if (a3=="")
	a3=0;
if (a4=="")
	a4=0;
document.getElementById('lam5').value=parseInt(a1)+parseInt(a2)+parseInt(a3)+parseInt(a4);
document.getElementById('id_18').value=parseInt(a1)+parseInt(a2)+parseInt(a3)+parseInt(a4);
}
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="../style/default.css" rel="stylesheet" type="text/css">
<title></title>
</head>
<body onLoad="cek_status_input(this)">
<?php
$data = array();
?>
<form name="formulir" id="formulir" action="" method="POST">
<table width="100%" cellpadding="0" cellspacing="0" border="0">
<tr>
<td valign="top" width="2"><img src="../images/kotak_2.jpg"></td>
<td valign="top" width="2"></td>
<td valign="top" width="2"><img src="../images/kotak_2.jpg"></td>
</tr>
<td></td><td>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="header_teks_2">
  <tr>
    <td width="23%" rowspan="2">
    <table border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td width="2%"><img src="../images/form.jpg"></td><td align="center" class="header_teks_2"><h2>1770 S</h2><br>DEPARTEMEN KEUANGAN RI<br>DIREKTORAT JENDERAL PAJAK</td>
    </tr>
    </table>
    </td>
    <td width="57%" style="BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;"><div align="center"><span class="header_teks">SPT TAHUNAN <br>
      PAJAK PENGHASILAN WAJIB PAJAK ORANG PRIBADI</span></div></td>
    <td width="10%" rowspan="2">
      <table width="90%" border="0" cellspacing="0" cellpadding="0">
      <tr>
      <td><img src="../images/tahun_pajak.jpg"></td>
      <td>
       <table width="100%" border="0" cellspacing="0" cellpadding="0">
       <tr>
				<?php
				$arr='2017';
				for ($i=0;$i<strlen($arr);$i++)
				{
				$par[$i]=substr($arr,$i,1);
				?>
				<td align="center" width="15" height="25" style="BORDER-TOP: 1px solid #000;BORDER-LEFT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><b><?php echo $par[$i];?></b></td>
				<?php
				}
				?>
				</tr>
				</table>
				</td>
        </tr>
      </table></td>
  </tr>
  <tr>
    <td style="BORDER-TOP: 1px solid #000;BORDER-LEFT: 1px solid #000;BORDER-RIGHT: 1px solid #000;">&nbsp;MEMPUNYAI PENGHASILAN :<BR>&nbsp;&#8226; DARI SATU ATAU LEBIH PEMBERI KERJA<BR>&nbsp;&#8226; DALAM NEGERI LAIINYA<BR>&nbsp;&#8226; YANG DIKENAKAN PPh FINAL DAN/ATAU BERSIFAT FINAL</td>
  </tr>
  <tr>
  <td colspan="3" align="center" class="normal_teks_2" style="BORDER-TOP: 1px solid #000;BORDER-BOTTOM: 1px solid #000;"><b>PERHATIAN </b>&nbsp;&nbsp; &#8226 ISI DENGAN HUrUF CETAK/DIKETIK DENGAN TINTA HITAM &#8226 BERI TANDA "X" PADA kotak_2 PILIHAN YANG SESUAI</font></td></tr>
  <tr>
    <td colspan="3" style="BORDER-LEFT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr class="header_teks_2">
        <td rowspan="5" width="5%" style="BORDER-RIGHT: 1px solid #000;"><img src="../images/identitas.jpg"></td>
        <td width="20%">&nbsp;NPWP</td>
        <td width="1%">:</td>
        <td width="77%"><table border="0" cellspacing="1" cellpadding="1" class="normal_teks">
            <tr>
						  <?php
						$arr=$data[0][4];
						for ($i=0;$i<strlen($arr);$i++)
						{$a=$i+1;
							$rec="a".$a;
							$par[$i]=substr($arr,($a-1),1);
							if ( ($a==3) or ($a==6) or ($a==9) or ($a==10) or ($a==13))
							echo "<td>&nbsp;</td>";
							echo "<td align=center width=15 style='BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;BORDER-TOP: 1px solid #000;BORDER-BOTTOM: 1px solid #000;'>".$par[$i]."</td>";
				}
				?>
            </tr>
            </table>
        </td>
        </tr>
        <tr class="header_teks_2">
        <td>&nbsp;NAMA WAJIB PAJAK </td>
        <td>:</td>
        <td>
        		<table border="0" cellspacing="1" cellpadding="1" class="normal_teks">
            <tr>
						  <?php
						//$arr=$data[0][2]." / ".
						$arr = $data[0][3];
						for ($i=0;$i<strlen($arr);$i++)
						{
							$tampil = substr($arr,$i,1);
							echo "<td align=center width=15 style='BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;BORDER-TOP: 1px solid #000;BORDER-BOTTOM: 1px solid #000;'>".$tampil."</td>";
						}
						?>
            </tr>
            </table>
						</td>
        </tr>
        <tr class="header_teks_2">
        <td>&nbsp;PEKERJAAN</td>
        <td>:</td>
        <td><table border="0" cellspacing="1" cellpadding="1" class="normal_teks">
            <tr>
						  <?php
						$arr="PEGAWAI TETAP PT TELEKOMUNIKASI INDONESIA";
						for ($i=0;$i<strlen($arr);$i++)
						{
							$tampil = substr($arr,$i,1);
							echo "<td align=center width=15 style='BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;BORDER-TOP: 1px solid #000;BORDER-BOTTOM: 1px solid #000;'>".$tampil."</td>";
						}
						?>
            </tr>
            </table>
        </td>
        </tr>
         <tr class="header_teks_2">
        <td>&nbsp;NO TELEPON</td>
        <td>:</td>
        <td>
        <table border="0" cellspacing="" cellpadding="0" class="normal_teks_2">
            <tr>
						  <?php
						for ($i=0;$i<12;$i++)
						{
							echo "<td align=center width=6 style='BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;BORDER-TOP: 1px solid #000;BORDER-BOTTOM: 1px solid #000;'>"?>
							<input type="text" maxlength="1" size="1" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" class="button_telepon">
							<?php "</td>";
						}
						?>
						<td>&nbsp;NO FAX&nbsp;</td>
						<?php
						for ($i=0;$i<10;$i++)
						{
							echo "<td align=center width=6 style='BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;BORDER-TOP: 1px solid #000;BORDER-BOTTOM: 1px solid #000;'>"?>
							<input type="text" maxlength="1" size="1" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" class="button_telepon">
							<?php "</td>";
						}
						?>
            </tr>
            </table>
          
        </td>
        </tr>
        <tr class="header_teks_2">
        <td>&nbsp;PERUBAHAN DATA </td>
        <td>:</td>
        <td><input type="checkbox">&nbsp;LAMPIRAN TERSENDIRI<input type="checkbox">&nbsp;TIDAK ADA</td>
        </tr>
        </table></td>
  </tr>
</table>
<table width="100%" border="0" cellspacing="0" cellpadding="0" class="header_teks_2">
  <tr>
    <td colspan="6" style="BORDER-LEFT: 1px solid #000;BORDER-RIGHT: 1px solid #000;">*) Pengisian kolom-kolom yang berisi nilai rupiah harus tanpa nilai desimal</td>
    <td align="center" style="BORDER-RIGHT: 1px solid #000;">*) RUPIAH </td>
  </tr>
  <tr>
    <td width="5%" style="BORDER-TOP: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;"><img src="../images/Peng_neto.jpg"></td>
    <td width="80%" colspan="5" style="BORDER-TOP: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;">
    		<table width="100%" border="0" cellspacing="1" cellpadding="1" class="header_teks_2">
    		<tr><td width="2%">1</td><td width="98%">PENGHASILAN NETO DALAM NEGERI SEHUBUNGAN DENGAN PERKERJAAN</td></tr>
    		<tr><td>&nbsp;</td><td>(diisi akumulasi jumlah penghasilan neto pada setiap Formulir 1721-A1 dan / atau 1721-A2 angka 14 yang dilampirkan)</td></tr>
    		<tr><td>2</td><td>PENGHASILAN NETO DALAM NEGERI LAINNYA</td></tr>
    		<tr><td>&nbsp;</td><td>(diisi sesuai dengan Formulir 1770 S-I jumlah bagian A)</td></tr>
    		<tr><td>3</td><td>PENGHASILAN NETO LUAR NEGERI</td></tr>
    		<tr><td>&nbsp;</td><td>(apabila memiliki penghasilan dari luar negeri agar diisi dari Lampiran Tersendiri)</td></tr>
    		<tr><td>4</td><td>JUMLAH PENGHASILAN NETO (1+2+3)</td></tr>
    		<tr><td>5</td><td>ZAKAT ATAS PENGHASILAN YANG MENJADI OBJEK PAJAK</td></tr>
    		<tr><td>6</td><td>JUMLAH PENGHASILAN NETO SETELAH PENGURANGAN ZAKAT ATAS PENGHASILAN (4 - 5)</td></tr>
    		</table>
		</td>
    <td width="18%" style="BORDER-TOP: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;"><div align="right">
    <table width="100%" cellpadding="1" cellspacingt="0" border="0" class="normal_teks">
    <tr>
    <td width="4%" style="BORDER-TOP: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-LEFT: 1px solid #000;">1</td>
    <td width="96%" align="right">
    <input type="text" size="11" class="button" value=<?php echo  number_format($data[0][13],0,",","."); ?> readonly="readonly">
    <input type="hidden" name="isian_1" value=<?php echo  $data[0][13];?>></td>
    </tr>
    <tr>
    <td style="BORDER-TOP: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-LEFT: 1px solid #000;">2</td>
    <td width="96%" align="right">
    <input type="text" name="isian_2_view" class="button" size="11" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" onKeyUp="cek_status_input(this);">
    <input type="hidden" name="isian_2" class="button" size="11" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" onKeyUp="cek_status_input(this);">
    </td>
    </tr>
    <tr>
    <td style="BORDER-TOP: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-LEFT: 1px solid #000;">3</td>
    <td align="right">
    <input type="text" size="11" class="button" name="isian_3" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" onKeyUp="cek_status_input(this);">
    </td>
    </tr>
    <tr>
    <td style="BORDER-TOP: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-LEFT: 1px solid #000;">4</td>
    <td align="right">
    <input type="text" name="isian_4_view" readonly="readonly" class="button" size="11" onKeyUp="cek_status_input(this);">
    <input type="hidden" name="isian_4" readonly="readonly" class="button" size="11" onKeyUp="cek_status_input(this);">
    </td></tr>
    <tr><td style="BORDER-TOP: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-LEFT: 1px solid #000;">5</td>
    <td align="right">
		<input type="text" name="isian_5" class="button" value="0" size="11" readonly="readonly">
    </td></tr>
    <tr><td style="BORDER-TOP: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-LEFT: 1px solid #000;">6</td><td align="right">
    <input type="text" name="isian_6_view" class="button" size="11" readonly="readonly">
    <input type="hidden" name="isian_6" class="button" size="11" readonly="readonly">
    </td></tr>
    </table>
    </div></td>
    </tr>
    <input type="hidden" id="id_1" value="<?php echo $data[0][13]?>"/>
    <tr>
    <td width="5%" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;"><img src="../images/peng_pajak.jpg"></td>
    <td width="88%" colspan="5" style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;">
    		<table width="100%" border="0" cellspacing="1" cellpadding="1" class="header_teks_2">
    		<tr><td width="2%">7</td><td width="30%">PENGHASILAN TIDAK KENA PAJAK</td>
    		 <td>
    		<?php
    		if ($data[0][11] != ""){
    		for($x=1;$x < 2;$x++){
    		?>
    		<?php echo  $data[0][11]; ?>&nbsp;<img src="../images/cross.JPG" width="13" height="13" />&nbsp;
    		<?php
    		}
    		}
    		?>
    		    </td></tr>
    		<tr><td>8</td><td>PENGHASILAN KENA PAJAK(6 - 7)</td><td>&nbsp;</td></tr>
    		</table>
		</td>
    <td style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" width="10%"><div align="right">
    <table width="100%" cellpadding="1" cellspacingt="0" border="0" class="normal_teks">
    <tr><td width="4%" style="BORDER-TOP: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-LEFT: 1px solid #000;">7</td><td width="96%" align="right"><input type="text" size="11" readonly="readonly" class="button" value=<?php echo  number_format($data[0][12],0,",",".");?>><input type="hidden" name="isian_7" size="11" class="button" readonly="readonly" value=<?php echo  $data[0][12];?>></td></tr>
    <tr><td style="BORDER-TOP: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-LEFT: 1px solid #000;">8</td><td align="right">
    <input type="text" name="isian_8_view" class="button" size="11" readonly="readonly">
    <input type="hidden" name="isian_8" class="button" size="11" readonly="readonly"></td></tr>
    </table>
	  </div></td>
    </tr>
    <tr>
    <td width="5%" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;"><img src="../images/pph_terutang.jpg"></td>
    <td width="88%" colspan="5" style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;">
    		<table width="100%" border="0" cellspacing="1" cellpadding="1" class="header_teks_2">
    		<tr><td width="2%">9</td><td width="98%">PPh TERUTANG (TRIF PASAL 17 UU PPh x ANGKA 8)</td></tr>
    		<tr><td>10</td><td>PENGEMBALIAN /PENGURANGAN PPh PASAL 24 YANG TELAH DIKREDITKAN</td></tr>
    		<tr><td>11</td><td>JUMLAH PPh TERUTANG (9+10)</td></tr>
    		</table>
		</td>
    <td style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" width="10%"><div align="right">
    <table width="100%" cellpadding="1" cellspacingt="0" border="0" class="normal_teks">
    <tr><td width="4%" style="BORDER-TOP: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-LEFT: 1px solid #000;">9</td><td width="96%" align="right"><input type="text" size="11" class="button" readonly="readonly" value=<?php echo  number_format($data[0][14],0,",",".");?>><input type="hidden" name="isian_9" size="11" value=<?php echo  $data[0][14];?>></td></tr>
    <tr><td style="BORDER-TOP: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-LEFT: 1px solid #000;">10</td><td align="right">
    <input type="text" name="isian_10" class="button" size="11" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" onKeyUp="cek_status_input(this);"></td></tr>
     <tr><td style="BORDER-TOP: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-LEFT: 1px solid #000;">11</td><td align="right">
    <input type="text" name="isian_11_view" class="button" size="11" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" readonly="readonly" onKeyUp="cek_status_input(this);">
    <input type="hidden" name="isian_11" class="button" size="11" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" readonly="readonly" onKeyUp="cek_status_input(this);"></td></tr>
    </table>
	  </div><input type="hidden" id="id_6" value="<?php echo $data[0][14]?>"/></td>
    </tr>
    
    <tr>
    <td width="5%" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;"><img src="../images/kredit_pajak.jpg"></td>
    <td width="88%" colspan="5" style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;">
    		<table width="100%" border="0" cellspacing="0" cellpadding="0" class="header_teks_2">
    		<tr><td width="2%" valign="top">12</td><td width="98%" colspan="2">PPh YANG DIPOTONG/DIPUNGUT PIHAK LAIN/DITANGGUNG PEMRINTAH DAN/ATAU KREDIT PAJAK LUAR NEGERI DAN/ATAU TERUTANG DI LUAR NEGERI (diisi dari Formulir 1770 S-I jumlah Bagian C kolom (7))</td></tr>
    		<tr><td width="2%" valign="top">13</td><td width="43%">a. <input type="checkbox">&nbsp;PPh YANG HARUS DIBAYAR SENDIRI<br>
    									     b. <input type="checkbox">&nbsp;PPh YANG LEBIH DIPOTONG/DIPUNGUT<br>
    		    </td><td width="55%">(11 - 12)</td></tr>
    		<tr><td width="2%" valign="top">14</td><td width="43%" valign="top">PPh YANG DIBAYAR SENDIRI</td>
    		                          <td width="55%">a. PPh PASAL 25<br>
    		                          								b. STP PPh Pasal 25 (hanya pokok pajak)<br>
    		                          								c. FISKAL LUAR NEGERI
    		                          </td></tr>
    		<tr><td width="2%">15</td><td width="98%" colspan="2">JUMLAH KREDIT PAJAK (14a + 14b + 14c)</td></tr>
    		</table>
		</td>
    <td style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" width="10%"><div align="right">
    <table width="100%" cellpadding="1" cellspacingt="0" border="0" class="normal_teks">
    <tr><td width="4%" style="BORDER-TOP: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-LEFT: 1px solid #000;">12</td>
    <td width="96%" align="right">
    <input type="text" name="isian_12_view" class="button" size="11" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" onKeyUp="cek_status_input(this);">
    <input type="hidden" name="isian_12" class="button" size="11" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" onKeyUp="cek_status_input(this);"></td>
	</tr>
    <tr><td style="BORDER-TOP: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-LEFT: 1px solid #000;">13</td><td align="right">
    <input type="text" name="isian_13_view" class="button" size="11" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" readonly="readonly" onKeyUp="cek_status_input(this);">
    <input type="hidden" name="isian_13" class="button" size="11" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" readonly="readonly" onKeyUp="cek_status_input(this);"></td></tr>
     <tr><td style="BORDER-TOP: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-LEFT: 1px solid #000;">14a</td><td align="right">
    <input type="text" name="isian_14a" class="button" size="11" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" onKeyUp="cek_status_input(this);"></td></tr>
     <tr><td style="BORDER-TOP: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-LEFT: 1px solid #000;">14b</td><td align="right">
    <input type="text" name="isian_14b" class="button" size="11" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" onKeyUp="cek_status_input(this);"></td></tr>
     <tr><td style="BORDER-TOP: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-LEFT: 1px solid #000;">14c</td><td align="right">
    <input type="text" name="isian_14c" class="button" size="11" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" onKeyUp="cek_status_input(this);"></td></tr>
     <tr><td style="BORDER-TOP: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-LEFT: 1px solid #000;">15</td><td align="right">
    <input type="text" name="isian_15_view" class="button" size="11" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" readonly="readonly" onKeyUp="cek_status_input(this);">
    <input type="hidden" name="isian_15" class="button" size="11" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" readonly="readonly" onKeyUp="cek_status_input(this);"></td></tr>
    </table>
	  </div><input type="hidden" id="id_6" value="<?php echo $data[0][14]?>"/></td>
    </tr>
    
     <tr>
    <td width="5%" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;"><img src="../images/pph_kurang_lebih.jpg"></td>
    <td width="88%" colspan="5" style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;">
    		<table width="100%" border="0" cellspacing="0" cellpadding="0" class="header_teks_2">
    		<tr><td width="2%" valign="top">16</td><td width="43%" valign="top">a. <input type="checkbox">&nbsp;PPh YANG KURANG DIBAYAR (PPh PASAL 29)<br>
    									     b. <input type="checkbox">&nbsp;PPh YANG LEBIH DIBAYAR (PPh PASAL 28 A)<br>
    		    </td><td width="7%">(13-15)</td><td width="40%" align="center">TGL LUNAS : <br>&nbsp;tgl &nbsp;<input size="1" type="text" maxlength="1" class="button_0" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;"><input size="1" type="text" maxlength="1" class="button_0" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;"> &nbsp;bln&nbsp;<input size="1" type="text" maxlength="1" class="button_0" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;"><input size="1" type="text" maxlength="1" class="button_0" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;">&nbsp;thn&nbsp;<input size="1" type="text" maxlength="1" class="button_0" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;"><input size="1" type="text" maxlength="1" class="button_0" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;"></td></tr>
    		<tr><td width="2%" valign="top">17</td><td colspan="2" valign="top">PERMOHONAN PPh lebih bayar pada 16b mohon</td><td width="10%" valign="top">
    		a. <input type="checkbox">&nbsp;DIRESTITUSIKAN<br>
    		b. <input type="checkbox">&nbsp;DIPERHITUNGKAN DENGAN UTANG PAJAK
    		</td></tr>
    		</table>
		</td>
    <td style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" width="10%"><div align="right">
 <table width="100%" cellpadding="1" cellspacingt="0" border="0" class="normal_teks">
    <tr><td width="4%" style="BORDER-TOP: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-LEFT: 1px solid #000;">16</td>
    <td width="96%" align="right"><input type="text" name="isian_16_view" class="button" size="11" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" readonly="readonly">
    <input type="hidden" name="isian_16" class="button" size="11" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" readonly="readonly"></td></tr>
    <tr><td style="BORDER-TOP: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-LEFT: 1px solid #000;">17</td><td align="right">
    <input type="text" name="isian_17" size="11"class="button" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" onKeyUp="this.value=money_format(int_format(this.value))"></td></tr>
    </table>
	  </div></td>
    </tr>
    
     <tr>
    <td width="5%" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;"><img src="../images/angsuran.jpg"></td>
    <td width="88%" colspan="5" style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;">
    		<table width="100%" border="0" cellspacing="0" cellpadding="0" class="header_teks_2">
    		<tr><td width="2%" valign="top">18</td><td valign="top">ANGSURAN PPh PASAL 25 TAHUN PAJAK BERIKUTNYA SEBESAR</td></tr>
    		<tr><td width="2%" valign="top">&nbsp;</td><td valign="top">DIHITUNG BERDASARKAN<br>
    		a. <input type="checkbox">&nbsp;1/12 x JUMLAH PADA ANGKA 13<br>
    		b. <input type="checkbox">&nbsp;PENGHITUNGAN DALAM LAMPIRAN TERSENDIRI
    		</td>
    		</table>
		</td>
    <td style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" width="10%"><div align="right">
		<table width="100%" cellpadding="1" cellspacingt="0" border="0" class="normal_teks">
    <tr><td width="4%" style="BORDER-TOP: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-LEFT: 1px solid #000;">18</td>
    <td width="96%" align="right"><input type="text" name="isian_18" class="button" size="11" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" onKeyUp="this.value=money_format(int_format(this.value))"></td></tr>
    </table>
	  </div></td>
    </tr>
    
    <tr>
    <td width="2%" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;"><img src="../images/lampiran.jpg"></td>
    <td width="88%" colspan="6" style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;">
    		<table width="100%" border="0" cellspacing="0" cellpadding="0" class="header_teks_2">
    		<tr><td width="2%" valign="top">a</td><td valign="top"><input type="checkbox">&nbsp;Fotokopi Formulir 1721-A1 atau 1721-A2 atau bukti potong PPh Pasal 21</td><td>d.</td><td><input type="checkbox">&nbsp;Surat kuasa khusus (bila dikuasakan)</td></tr>
    		<tr><td width="2%" valign="top">b</td><td valign="top"><input type="checkbox">&nbsp;Daftar susunan keluarga yang menjadi tanggungan wajib pajak</td><td>e.</td><td><input type="checkbox">....</td></tr>
    		<tr><td width="2%" valign="top">c</td><td valign="top"><input type="checkbox">&nbsp;Surat setoran pajak lembar ke-3 PPh pasal 29</td><td>&nbsp;</td><td>&nbsp;</td></tr>
    		</table>
		</td>
    </tr>
    
    <tr>
    <td colspan="7" width="90%" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;">
		<table width="100%" border="0" cellspacing="0" cellpadding="1" class="header_teks_2">
    		<tr>
			<td colspan="5" align="center" style="BORDER-BOTTOM: 1px solid #000;" bgcolor="#999999"><b>PERNYATAAN</b></td>
			</tr>
    		<tr>
			<td colspan="5" align="left">Dengan menyadari sepenuhnya akan segala akibat termasuk sanksi-sanksi sesuai dengan ketentuan peraturan perundang-undangan yang berlaku, saya menyatakan bahwa yang telah saya beritahukan diatas beserta lampiran-lampirannya adalah benar, lengkap dan jelas.</td>
			</tr>
    		<tr>
			<td colspan="2" width="75%" align="center"><input type="checkbox">&nbsp; WAJIB PAJAK&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<input type="checkbox">&nbsp; KUASA&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;TGL&nbsp;&nbsp;<?php echo  date("d",mktime(0,0,0,date("d"))) ?>&nbsp;&nbsp;BLN&nbsp;&nbsp;<?php echo  date("m",mktime(0,0,0,date("m"))) ?>&nbsp;&nbsp;THN&nbsp;&nbsp;<?php echo  date("Y",mktime(0,0,0,date("y"))) ?></td>
   		    <td colspan="2" rowspan="2" style="BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;BORDER-TOP: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="center">&nbsp;</td>
   		    <td width="1%">&nbsp;</td></tr>
    		<tr><td valign="top" width="20%">Nama Lengkap</td><td>
    		<table cellpadding="1" cellspacing="1">
    		<tr>
    		<?php
    		 $gab = strlen($data[0][3]);
    		 for ($x=0;$x <= $gab;$x++){
    		 $view = substr($data[0][3],$x,1);
    		 ?>
    		 <td width="15" class="header_teks_2" align="center" style="BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;BORDER-TOP: 1px solid #000;BORDER-BOTTOM: 1px solid #000;"><?php echo  $view; ?></td>
    		<?php
    		 }
    		 ?>
    		 </tr>
    		 </table>
    		 </td></tr>
    		<tr><td valign="top">NPWP</td><td>
    		<table cellpadding="1" cellspacing="1">
    		<tr>
    		<?php
    		 $gab = strlen($data[0][4]);
    		 for ($x=0;$x <= $gab;$x++){
    		 $view = substr($data[0][4],$x,1);
    		 ?>
    		 <td width="15" class="header_teks_2" align="center" style="BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;BORDER-TOP: 1px solid #000;BORDER-BOTTOM: 1px solid #000;"><?php echo  $view; ?></td>
    		<?php
    		 }
    		 ?>
    		 </tr>
    		 </table>
    		</td></tr>
    		</table>
    		</td>
    </tr>
    </table>
	</td><td></td></tr>
<tr>
<td valign="bottom"><img src="../images/kotak_2.jpg"></td>
<td></td>
<td valign="bottom"><img src="../images/kotak_2.jpg"></td>
</tr>

<!--**************************************************************** lembar ke dua ********************************************-->
<tr>
<td colspan="3">&nbsp;</td>
</tr>
<tr>
<td valign="top" width="2"><img src="../images/kotak_2.jpg"></td>
<td></td>
<td valign="top" width="2"><img src="../images/kotak_2.jpg"></td>
</tr>
<td></td><td>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="header_teks_2">
  <tr>
  <td width="26%" rowspan="2">
     <table width="100%"border="0" cellpadding="0" cellspacing="0">
     <tr>
     <td width="2%"><img src="../images/form.jpg"></td>
     <td align="center" class="header_teks_2"><h2>1770 S-I</h2><br>DEPARTEMEN KEUANGAN RI<br>DIREKTORAT JENDERAL PAJAK</td>
     </tr>
     </table>
  </td>
  <td width="60%" style="BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;"><div align="center"><span class="header_teks">LAMPIRAN - I <br>
      SPT TAHUNAN PPh WAJIB PAJAK ORANG PRIBADI</span></div></td>
  <td width="16%" rowspan="2" align="left" valign="middle">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
      <td><img src="../images/tahun_pajak.jpg"></td>
      <td>
         <table width="90%" border="0" cellspacing="0" cellpadding="0">
         <tr>
				 <?php
				 $arr=$data[0][0];
				 for ($i=0;$i<strlen($arr);$i++)
				 {
				 $par[$i]=substr($arr,$i,1);
				 ?>
				 <td align="center" width="7" height="25" style="BORDER-TOP: 1px solid #000;BORDER-LEFT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><b><?php echo $par[$i];?></b></td>
				 <?php
				 }
				 ?>
				 </tr>
				 </table>
		  </td>
      </tr>
      </table>
  </td>
  </tr>
  <tr>
  <td class="normal_teks_2" style="BORDER-TOP: 1px solid #000;BORDER-LEFT: 1px solid #000;BORDER-RIGHT: 1px solid #000;">&nbsp;&#8226; PENGHASILAN NETO DALAM NEGERI LAINNYA<BR>&nbsp;&#8226; PENGHASILAN YANG TIDAK TERMASUK OBJEK PAJAK<BR>&nbsp;&#8226; DAFTAR PEMOTONGAN/PEMUNGUTAN PPh OLEH PIHAK LAIN DAN PPh YANG DITANGGUNG PEMERINTAH</td>
  </tr>
  <tr>
  <td colspan="3" align="center" class="normal_teks_2" style="BORDER-LEFT: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-TOP: 1px solid #000;BORDER-BOTTOM: 1px solid #000;">
     <table width="100%" cellpadding="0" cellspacing="0" border="0" class="normal_teks">
     <tr><td width="25%">&nbsp;NPWP</td><td width="1%">:</td><td width="74%">
     <table cellpadding="1" cellspacing="1" border="0" class="normal_teks">
     <tr>
     <?php
     $gab = $data[0][4];
      for($x=0;$x < strlen($data[0][4]);$x++){
      	$view = substr($gab,$x,1);
      ?>
      <td width="15" align="center" style="BORDER-LEFT: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-TOP: 1px solid #000;BORDER-BOTTOM: 1px solid #000;"><?php echo  $view; ?></td>
      <?php
      }
     ?>
     </tr>
     </table>
     </td></tr>
     <tr><td width="25%">&nbsp;NAMA WAJIB PAJAK</td><td width="1%">:</td><td width="74%">
     <table cellpadding="1" cellspacing="1" border="0" class="normal_teks">
     <tr>
     <?php
     $gab = $data[0][3];
      for($x=0;$x < strlen($data[0][3]);$x++){
      	$view = substr($gab,$x,1);
      ?>
      <td width="15" align="center" style="BORDER-LEFT: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-TOP: 1px solid #000;BORDER-BOTTOM: 1px solid #000;"><?php echo  $view; ?></td>
      <?php
      }
     ?>
     </tr>
     </table>
     </td></tr>
     </table>
  </td></tr>
  <tr>
    <td colspan="8">
      <table border="0" cellspacing="0" cellpadding="0">
      <tr class="header_teks_2">
        <td><br>&nbsp;<b>BAGIAN A :</b></td>
        <td><br>&nbsp;<b>PENGHASILAN NETO DALAM NEGERI LAINNYA</b></td>
      </tr>
      <tr class="header_teks_2">
        <td>&nbsp;</td>
        <td>&nbsp;<b>(tidak termasuk penghasilan dikenakan PPh final dan/atau bersifat final)</b></td>
      </tr>
			</table>
			<br>
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="normal_teks">
  <tr>
    <td width="2%" style="BORDER-TOP: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;" align="center"><b>NO</b></td>
    <td width="80%" colspan="6" style="BORDER-TOP: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="center"><b>JENIS PENGHASILAN</b></td>
    <td width="18%" style="BORDER-TOP: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="center"><b>JUMLAH PENGHASILAN<br>(rupiah)</b></td>
  </tr>
  <tr>
    <td width="2%" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;" align="center">(1)</td>
    <td width="80%" colspan="6" style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="center">(2)</td>
    <td width="18%" style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="center">(3)</td>
  </tr>
  <tr>
    <td width="2%" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;" align="center">1</td>
    <td width="80%" colspan="6" style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="left">&nbsp;Bunga</td>
    <td width="18%" style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="right">
    <input type="text" name="isian_51" class="button" onKeyPress="if(event.keyCode &lt; 45 || event.keyCode &gt; 57) event.returnValue = false;" onKeyUp="cek_status_input(this);" ></td>
  </tr>
  <tr>
    <td width="2%" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;" align="center">2</td>
    <td width="80%" colspan="6" style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="left">&nbsp;Dividen</td>
    <td width="18%" style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="right">
    <input type="text" name="isian_52" class="button" onKeyPress="if(event.keyCode &lt; 45 || event.keyCode &gt; 57) event.returnValue = false;" onKeyUp="cek_status_input(this);" ></td>
  </tr>
  <tr>
    <td width="2%" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;" align="center">3</td>
    <td width="80%" colspan="6" style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="left">&nbsp;Royalti</td>
    <td width="18%" style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="right">
    <input type="text" name="isian_53" class="button" onKeyPress="if(event.keyCode &lt; 45 || event.keyCode &gt; 57) event.returnValue = false;" onKeyUp="cek_status_input(this);"></td>
  </tr>
  <tr>
    <td width="2%" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;" align="center">4</td>
    <td width="80%" colspan="6" style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="left">&nbsp;Sewa</td>
    <td width="18%" style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="right">
    <input type="text" name="isian_54" class="button" onKeyPress="if(event.keyCode &lt; 45 || event.keyCode &gt; 57) event.returnValue = false;" onKeyUp="cek_status_input(this);"></td>
  </tr>
  <tr>
    <td width="2%" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;" align="center">5</td>
    <td width="80%" colspan="6" style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="left">&nbsp;Penghargaan dan hadiah</td>
    <td width="18%" style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="right">
    <input type="text" name="isian_55" class="button" onKeyPress="if(event.keyCode &lt; 45 || event.keyCode &gt; 57) event.returnValue = false;" onKeyUp="cek_status_input(this);"></td>
  </tr>
  <tr>
    <td width="2%" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;" align="center">6</td>
    <td width="80%" colspan="6" style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="left">&nbsp;Keuntungan dari penjualan / pengalihan harta</td>
    <td width="18%" style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="right">
    <input type="text" name="isian_56" class="button" onKeyPress="if(event.keyCode &lt; 45 || event.keyCode &gt; 57) event.returnValue = false;" onKeyUp="cek_status_input(this);"></td>
  </tr>
  <tr>
    <td width="2%" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;" align="center">7</td>
    <td width="80%" colspan="6" style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="left">&nbsp;Penghasilan Lainnya</td>
    <td width="18%" style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="right">
    <input type="text" name="isian_57" class="button" onKeyPress="if(event.keyCode &lt; 45 || event.keyCode &gt; 57) event.returnValue = false;" onKeyUp="cek_status_input(this);"></td>
  </tr>
  <tr>
    <td width="2%" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;" align="center">&nbsp;</td>
    <td width="77%" colspan="5" style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="center">JUMLAH BAGIAN A</td>
    <td width="3%" align="center" style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;">JBA</td>
    <td width="18%" style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="right">
    <input type="text" name="isian_58_view" class="button" onKeyPress="if(event.keyCode &lt; 45 || event.keyCode &gt; 57) event.returnValue = false;" readonly="readonly" onKeyUp="cek_status_input(this);">
    <input type="hidden" name="isian_58" class="button" onKeyPress="if(event.keyCode &lt; 45 || event.keyCode &gt; 57) event.returnValue = false;" readonly="readonly" onKeyUp="cek_status_input(this);">
    </td>
  </tr>
 
    <tr>
    <td colspan="8">
  	 <table border="0" cellspacing="0" cellpadding="0">
      <tr class="header_teks_2">
        <td><br>&nbsp;<b>BAGIAN B :</b></td>
        <td><br>&nbsp;<b>PENGHASILAN YANG TIDAK TERMASUK OBJEK PAJAK</b></td>
      </tr>
			</table>
		<br>
	  </td>
    </tr>
    
   <tr>
    <td width="2%" style="BORDER-TOP: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;" align="center"><b>NO</b></td>
    <td width="80%" colspan="6" style="BORDER-TOP: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="center"><b>JENIS PENGHASILAN</b></td>
    <td width="18%" style="BORDER-TOP: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="center"><b>JUMLAH PENGHASILAN<br>(rupiah)</b></td>
  </tr>
  <tr>
    <td width="2%" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;" align="center">(1)</td>
    <td width="80%" colspan="6" style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="center">(2)</td>
    <td width="18%" style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="center">(3)</td>
  </tr>
  <tr>
    <td width="2%" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;" align="center">1</td>
    <td width="80%" colspan="6" style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="left">&nbsp;Bantuan / Sumbangan / Hibah</td>
    <td width="18%" style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="right">
    <input type="text" name="isian_59" class="button" onKeyPress="if(event.keyCode &lt; 45 || event.keyCode &gt; 57) event.returnValue = false;" onKeyUp="cek_status_input(this);"></td>
  </tr>
  <tr>
    <td width="2%" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;" align="center">2</td>
    <td width="80%" colspan="6" style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="left">&nbsp;Warisan</td>
    <td width="18%" style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="right">
    <input type="text" name="isian_60" class="button" onKeyPress="if(event.keyCode &lt; 45 || event.keyCode &gt; 57) event.returnValue = false;" onKeyUp="cek_status_input(this);"></td>
  </tr>
  <tr>
    <td width="2%" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;" align="center">3</td>
    <td width="80%" colspan="6" style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="left">&nbsp;Bagian Laba Anggota Perseroan Komanditer Tidak Atas Saham,<br>&nbsp;Persekutuan, Perkumpulan, Firma, Kongsi</td>
    <td width="18%" style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="right">
    <input type="text" name="isian_61" class="button" onKeyPress="if(event.keyCode &lt; 45 || event.keyCode &gt; 57) event.returnValue = false;" onKeyUp="cek_status_input(this);"></td>
  </tr>
  <tr>
    <td width="2%" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;" align="center">4</td>
    <td width="80%" colspan="6" style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="left">&nbsp;Klaim Asuransi Kesehatan, Kecelakaan, Jiwa, Dwiguna, Beasiswa</td>
    <td width="18%" style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="right">
    <input type="text" name="isian_62" class="button" onKeyPress="if(event.keyCode &lt; 45 || event.keyCode &gt; 57) event.returnValue = false;" onKeyUp="cek_status_input(this);"></td>
  </tr>
  <tr>
    <td width="2%" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;" align="center">5</td>
    <td width="80%" colspan="6" style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="left">&nbsp;Penghasilan Lainnya yang Tidak Termasuk Objek Pajak</td>
    <td width="18%" style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="right">
    <input type="text" name="isian_63" class="button" onKeyPress="if(event.keyCode &lt; 45 || event.keyCode &gt; 57) event.returnValue = false;" onKeyUp="cek_status_input(this);"></td>
  </tr>
  <tr>
    <td width="2%" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;" align="center">&nbsp;</td>
    <td width="77%" colspan="5" style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="center">JUMLAH BAGIAN B</td>
    <td width="3%" align="center" style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;">JBB</td>
    <td width="18%" style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;"  align="right">
    <input type="text" name="isian_64_view" class="button" onKeyPress="if(event.keyCode &lt; 45 || event.keyCode &gt; 57) event.returnValue = false;" readonly="readonly" onKeyUp="cek_status_input(this);">
    <input type="hidden" name="isian_64" class="button" onKeyPress="if(event.keyCode &lt; 45 || event.keyCode &gt; 57) event.returnValue = false;" readonly="readonly" onKeyUp="cek_status_input(this);">
    </td>
  </tr>
    
    <tr>
    <td colspan="8" style="BORDER-BOTTOM:1px solid #000;">
        <?php
    $nikna = $data[0][2];
    $queri1 = "select N_PPH from rpt_1770s_2013 where N_NIK = $nikna";
		$oci_dwh->parse($queri1);
		$oci_dwh->exec_stmt();
		$res_2 = $oci_dwh->fetch_rows();
		
    $queri2 = "select V_NPWP_KANTOR from rpt_spt1721_2012 where N_NIK = $nikna";
		$oci_dwh->parse($queri2);
		$oci_dwh->exec_stmt();
		$res_3 = $oci_dwh->fetch_rows();
    ?>
  	 <table border="0" cellspacing="0" cellpadding="0">
      <tr class="header_teks_2">
        <td><br>&nbsp;<b>BAGIAN C : DAFTAR PEMOTONGAN/PEMUNGUTAN PPh OLEH PIHAK LAIN DAN PPh YANG DITANGGUNG PEMERINTAH</b></td>
      </tr>
			</table>
		<br>
	  </td>
    </tr>
    
    <tr class="header_teks_2">
    <td width="2%" rowspan="2" align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;"><b>NO</b></td>
    <td width="20%" rowspan="2" align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><b>NAMA PEMOTONG/PEMUNGUT PAJAK</b></td>
    <td width="20%" rowspan="2" align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><b>NPWP PEMOTONG/PEMUNGUT PAJAK</b></td>
    <td colspan="2" width="28%" align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><b>BUKTI PEMOTONGAN/PEMUNGUTAN</b></td>
    <td width="10%" rowspan="2" align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><b>JENIS PAJAK :<br>PPh PASAL 21/22/23/24/26/DTP</b></td>
    <td width="20%" colspan="2" rowspan="2" align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><b>JUMLAH PPh YANG<br>DIPOTONG/DIPUNGUT</b></td>
    </tr>
    <tr class="header_teks_2">
    <td align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><b>NOMOR</b></td>
    <td align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><b>TANGGAL</b></td>
    </tr>
    <tr class="header_teks_2">
    <td align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;">(1)</td>
    <td align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;">(2)</td>
    <td align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;">(3)</td>
    <td align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;">(4)</td>
    <td align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;">(5)</td>
    <td align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;">(6)</td>
    <td colspan="2" align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;">(7)</td>
    </tr>
    <tr class="header_teks_2">
    <td align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;"><input type="teks" maxlength="2" class="button_1" name="isian_65" size="2" onKeyPress="if(event.keyCode &lt; 45 || event.keyCode &gt; 57) event.returnValue = false;" value="1" readonly="readonly"></td>
    <td align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" class="button_2" name="isian_66" value="PT. TELKOM, Tbk" readonly="readonly"></td>
    <td align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" class="button_2" name="isian_67" value="<?php echo $res_3->value[0][0];?>" readonly="readonly"></td>
    <td align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" class="button_2" name="isian_68" size="10" onKeyPress="if(event.keyCode &lt; 45 || event.keyCode &gt; 57) event.returnValue = false;"></td>
    <td align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" class="button_2" name="isian_69" size="15" onKeyPress="if(event.keyCode &lt; 45 || event.keyCode &gt; 57) event.returnValue = false;"></td>
    <td align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" class="button_1" name="isian_70" size="10" value="21"><?php /* <select class="button_2" name="isian_70">
     																																											 <option><font color="white">silakan pilih disini</font></option>
    																																											 <option value="21">21</option>
    																																											 <option value="22">22</option>
    																																											 <option value="23">23</option>
    																																											 <option value="24">24</option>
    																																											 <option value="26">26</option>
    																																											 <option value="DTP">DTP</option>
    																																											 </select></td> */ ?>
    <td colspan="2" align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" class="button" size="20" name="isian_71_view" onKeyPress="if(event.keyCode &lt; 45 || event.keyCode &gt; 57) event.returnValue = false;" readonly="readonly" value="<?php echo number_format($res_2->value[0][0],0,',','.');?>">
    <input type="hidden" class="button" size="20" name="isian_71" onKeyPress="if(event.keyCode &lt; 45 || event.keyCode &gt; 57) event.returnValue = false;" readonly="readonly" value="<?php echo $res_2->value[0][0];?>"></td>
    </tr>
    <tr class="header_teks_2">
    <td align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;"><input type="teks" maxlength="2" class="button_1" name="isian_72" size="2" onKeyPress="if(event.keyCode &lt; 45 || event.keyCode &gt; 57) event.returnValue = false;" ></td>
    <td align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" class="button_2" name="isian_73"></td>
    <td align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" class="button_2" name="isian_74" onKeyPress="if(event.keyCode &lt; 45 || event.keyCode &gt; 57) event.returnValue = false;"></td>
    <td align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" class="button_2" name="isian_75" size="10" onKeyPress="if(event.keyCode &lt; 45 || event.keyCode &gt; 57) event.returnValue = false;"></td>
    <td align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" class="button_2" name="isian_76" size="15" onKeyPress="if(event.keyCode &lt; 45 || event.keyCode &gt; 57) event.returnValue = false;"></td>
    <td align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" class="button_1" name="isian_77" size="10"><?php /* <select class="button_2" name="isian_77">
    																																											 <option><font color="white">silakan pilih disini</font></option>
    																																											 <option value="21">21</option>
    																																											 <option value="22">22</option>
    																																											 <option value="23">23</option>
    																																											 <option value="24">24</option>
    																																											 <option value="26">26</option>
    																																											 <option value="DTP">DTP</option>
    																																											 </select></td> */ ?>
    <td colspan="2" align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" class="button" size="20" name="isian_78" onKeyPress="if(event.keyCode &lt; 45 || event.keyCode &gt; 57) event.returnValue = false;" onKeyUp="cek_status_input(this)"></td>
    </tr>
    <tr class="header_teks_2">
    <td align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;"><input type="teks" maxlength="2" class="button_1" name="isian_79" size="2" onKeyPress="if(event.keyCode &lt; 45 || event.keyCode &gt; 57) event.returnValue = false;" ></td>
    <td align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" class="button_2" name="isian_80"></td>
    <td align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" class="button_2" name="isian_81" onKeyPress="if(event.keyCode &lt; 45 || event.keyCode &gt; 57) event.returnValue = false;"></td>
    <td align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" class="button_2" name="isian_82" size="10" onKeyPress="if(event.keyCode &lt; 45 || event.keyCode &gt; 57) event.returnValue = false;"></td>
    <td align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" class="button_2" name="isian_83" size="15" onKeyPress="if(event.keyCode &lt; 45 || event.keyCode &gt; 57) event.returnValue = false;"></td>
    <td align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" class="button_1" name="isian_84" size="10"><?php /* <select class="button_2" name="isian_84">
    																																											 <option><font color="white">silakan pilih disini</font></option>
    																																											 <option value="21">21</option>
    																																											 <option value="22">22</option>
    																																											 <option value="23">23</option>
    																																											 <option value="24">24</option>
    																																											 <option value="26">26</option>
    																																											 <option value="DTP">DTP</option>
    																																											 </select></td> */ ?>
    <td colspan="2" align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" class="button" size="20" name="isian_85" onKeyPress="if(event.keyCode &lt; 45 || event.keyCode &gt; 57) event.returnValue = false;" onKeyUp="cek_status_input(this)"></td>
    </tr>
    <tr class="header_teks_2">
    <td align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;"><input type="teks" maxlength="2" class="button_1" name="isian_86" size="2" onKeyPress="if(event.keyCode &lt; 45 || event.keyCode &gt; 57) event.returnValue = false;" ></td>
    <td align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" class="button_2" name="isian_87"></td>
    <td align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" class="button_2" name="isian_88" onKeyPress="if(event.keyCode &lt; 45 || event.keyCode &gt; 57) event.returnValue = false;"></td>
    <td align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" class="button_2" name="isian_89" size="10" onKeyPress="if(event.keyCode &lt; 45 || event.keyCode &gt; 57) event.returnValue = false;"></td>
    <td align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" class="button_2" name="isian_90" size="15" onKeyPress="if(event.keyCode &lt; 45 || event.keyCode &gt; 57) event.returnValue = false;"></td>
    <td align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" class="button_1" name="isian_91" size="10">
    																																											<?php /* <select class="button_2" name="isian_91">
    																																											 <option><font color="white">silakan pilih disini</font></option>
    																																											 <option value="21">21</option>
    																																											 <option value="22">22</option>
    																																											 <option value="23">23</option>
    																																											 <option value="24">24</option>
    																																											 <option value="26">26</option>
    																																											 <option value="DTP">DTP</option>
    																																											 </select></td> */ ?>
    <td colspan="2" align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" class="button" size="20" name="isian_92" onKeyPress="if(event.keyCode &lt; 45 || event.keyCode &gt; 57) event.returnValue = false;" onKeyUp="cek_status_input(this)"></td>
    </tr>
    <tr class="header_teks_2">
    <td align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;"><input type="teks" maxlength="2" class="button_1" name="isian_93" size="2" onKeyPress="if(event.keyCode &lt; 45 || event.keyCode &gt; 57) event.returnValue = false;" ></td>
    <td align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" class="button_2" name="isian_94"></td>
    <td align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" class="button_2" name="isian_95" onKeyPress="if(event.keyCode &lt; 45 || event.keyCode &gt; 57) event.returnValue = false;"></td>
    <td align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" class="button_2" name="isian_96" size="10" onKeyPress="if(event.keyCode &lt; 45 || event.keyCode &gt; 57) event.returnValue = false;"></td>
    <td align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" class="button_2" name="isian_97" size="15" onKeyPress="if(event.keyCode &lt; 45 || event.keyCode &gt; 57) event.returnValue = false;"></td>
    <td align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" class="button_1" name="isian_98" size="10">
    																																											<?php /* <select class="button_2" name="isian_98">
    																																											 <option><font color="white">silakan pilih disini</font></option>
    																																											 <option value="21">21</option>
    																																											 <option value="22">22</option>
    																																											 <option value="23">23</option>
    																																											 <option value="24">24</option>
    																																											 <option value="26">26</option>
    																																											 <option value="DTP">DTP</option>
    																																											 </select></td> */ ?>
    <td colspan="2" align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" class="button" size="20" name="isian_99" onKeyPress="if(event.keyCode &lt; 45 || event.keyCode &gt; 57) event.returnValue = false;" onKeyUp="cek_status_input(this)"></td>
    </tr>
    <tr class="header_teks_2">
    <td align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;">&nbsp;</td>
    <td align="center" colspan="4" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;">JUMLAH BAGIAN C</td>
    <td align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;">JBC</td>
    <td colspan="2" align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" class="button" size="20" name="isian_100_view" onKeyPress="if(event.keyCode &lt; 45 || event.keyCode &gt; 57) event.returnValue = false;">
    <input type="hidden" class="button" size="20" name="isian_100" onKeyPress="if(event.keyCode &lt; 45 || event.keyCode &gt; 57) event.returnValue = false;"></td>
    </tr>
    <tr><td colspan="8">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
    <tr><td width="75%" class="header_teks_3">Catatan :<br>
    			  *) - DTP : ditanggung Pemerintah<br>
    			  &nbsp;&nbsp;&nbsp; - Kolom (6) diisi dengan pilihan PPh Pasal 21/22/23/24/26/DTP (contoh :  ditulis 21, 22, 23, 24, 26, DTP)<br>
    			  &nbsp;&nbsp;&nbsp; - Jika terdapat kredit pajak PPh Pasal 24, <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;maka jumlah yang diisi adalah maksimum yang dapat dikreditkan sesuai lampiran tersendiri<br>
    			  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; (lihat buku petunjuk tentang Lampiran I Bagian C dan Induk SPT angka 3)
    			       
    </td>
    <td width="25%" valign="top" align="right" class="header_teks_3">Pindahkan Jumlah Bagian C ke Formulir Induk 1770 S Bagian D angka (12)</td>
    </tr>
    </table>
    </td></tr>
    <tr><td colspan="8">&nbsp;</td></tr>
    </table>
	</td>
	</tr>
	</table>
		</td><td></td></tr>
<tr>
<td valign="bottom"><img src="../images/kotak_2.jpg"></td>
<td></td>
<td valign="bottom"><img src="../images/kotak_2.jpg"></td>
</tr>

<!--**************************************************************** lembar ke tiga *******************************************-->
<tr>
<td colspan="3"><br></td>
</tr>
<tr>
<td colspan="3"><br></td>
</tr>
<tr>
<td colspan="3"><br></td>
</tr>
<tr>
<td colspan="3"><br></td>
</tr>
<tr>
<td valign="top" width="2"><img src="../images/kotak_2.jpg"></td>
<td></td>
<td valign="top" width="2"><img src="../images/kotak_2.jpg"></td>
</tr>
<td></td><td>
  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="header_teks_2">
  <tr><td colspan="3">&nbsp;</td></tr>
  <tr>
  <td width="27%" rowspan="2">
     <table border="0" cellpadding="0" cellspacing="0">
     <tr>
     <td width="2%"><img src="../images/form.jpg"></td>
     <td align="center" class="header_teks_2"><h2>1770 S-II</h2><br>DEPARTEMEN KEUANGAN RI<br>DIREKTORAT JENDERAL PAJAK</td>
     </tr>
     </table>
  </td>
  <td width="63%" style="BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;"><div align="center"><span class="header_teks">LAMPIRAN - II <br>
      SPT TAHUNAN PPh WAJIB PAJAK ORANG PRIBADI</span></div></td>
  <td width="10%" rowspan="2" align="left" valign="middle">
      <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
      <td><img src="../images/tahun_pajak.jpg"></td>
      <td>
         <table width="90%" border="0" cellspacing="0" cellpadding="0">
         <tr>
				 <?php
				 $arr=$data[0][0];
				 for ($i=0;$i<strlen($arr);$i++)
				 {
				 $par[$i]=substr($arr,$i,1);
				 ?>
				 <td align="center" width="15" hight="25" style="BORDER-TOP: 1px solid #000;BORDER-LEFT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><b><?php echo $par[$i];?></b></td>
				 <?php
				 }
				 ?>
				 </tr>
				 </table>
		  </td>
      </tr>
      </table>
  </td>
  </tr>
  <tr>
  <td class="normal_teks_2" style="BORDER-TOP: 1px solid #000;BORDER-LEFT: 1px solid #000;BORDER-RIGHT: 1px solid #000;">&nbsp;&#8226; PENGHASILAN YANG DIKENAKAN PPh FINAL DAN /ATAU BERSIFAT FINAL<BR>&nbsp;&#8226; DAFTAR HARTA PADA AKHIR TAHUN<BR>&nbsp;&#8226; DAFTAR KEWAJIBAN/UTANG PADA AKHIR TAHUN</td>
  </tr>
  <tr>
  <td colspan="3" align="center" class="normal_teks_2" style="BORDER-LEFT: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-TOP: 1px solid #000;BORDER-BOTTOM: 1px solid #000;">
     <table width="100%" cellpadding="0" cellspacing="0" border="0" class="normal_teks">
     <tr><td width="25%">&nbsp;NPWP</td><td width="1%">:</td><td width="74%">
     <table cellpadding="1" cellspacing="1" border="0" class="normal_teks">
     <tr>
     <?php
     $gab = $data[0][4];
      for($x=0;$x < strlen($data[0][4]);$x++){
      	$view = substr($gab,$x,1);
      ?>
      <td width="15" align="center" style="BORDER-LEFT: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-TOP: 1px solid #000;BORDER-BOTTOM: 1px solid #000;"><?php echo  $view; ?></td>
      <?php
      }
     ?>
     </tr>
     </table>
     </td></tr>
     <tr><td width="25%">&nbsp;NAMA WAJIB PAJAK</td><td width="1%">:</td><td width="74%">
     <table cellpadding="1" cellspacing="1" border="0" class="normal_teks">
     <tr>
     <?php
     $gab = $data[0][3];
      for($x=0;$x < strlen($data[0][3]);$x++){
      	$view = substr($gab,$x,1);
      ?>
      <td width="15" align="center" style="BORDER-LEFT: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-TOP: 1px solid #000;BORDER-BOTTOM: 1px solid #000;"><?php echo  $view; ?></td>
      <?php
      }
     ?>
     </tr>
     </table>
     </td></tr>
     </table>
  </td></tr>
  <tr>
    <td colspan="3">
      <table border="0" cellspacing="0" cellpadding="0">
      <tr class="header_teks_2">
        <td><br>&nbsp;<b>BAGIAN A :</b></td>
        <td><br>&nbsp;<b>PENGHASILAN YANG DIKENAKAN PPh FINAL DAN/ATAU BERSIFAT FINAL</b></td>
      </tr>
			</table>
			<br>
<table width="100%" border="0" cellspacing="0" cellpadding="1" class="normal_teks">
<tr>
<td colspan="4">
  <table width="100%" border="0" cellspacing="0" cellpadding="1" class="normal_teks">	
  <tr>
    <td width="3%" style="BORDER-TOP: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;" align="center"><b>NO</b></td>
    <td width="74%" style="BORDER-TOP: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="center"><b>SUMBER/JENIS PENGHASILAN</b></td>
    <td width="8%" style="BORDER-TOP: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="center"><b>DASAR PENGENAAN PAJAK/PENGHASILAN BRUTO</b></td>
    <td width="15%" style="BORDER-TOP: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="center"><b>PPh TERUTANG<br>(rupiah)</b></td>
  </tr>
  <tr>
    <td style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;" align="center">(1)</td>
    <td style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="center">(2)</td>
    <td style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="center">(3)</td>
    <td style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" width="10%" align="center">(4)</td>
  </tr>
  <tr>
    <td style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;" align="center">1</td>
    <td style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="left">&nbsp;Bunga Deposito, Tabungan dan Diskonto SBI</td>
    <td style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="left"><input type="text" name="isian_3_1" size="20" class="button" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" onKeyUp="cek_status_input5(this)"></td>
    <td style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" width="10%" align="right"><input type="text" name="isian_3_2" size="20" class="button" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" onKeyUp="cek_status_input5(this)"></td>
  </tr>
  <tr>
    <td style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;" align="center">2</td>
    <td style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="left">&nbsp;Bunga/Diskonto Obligasi yang Dilaporkan Perdagangannya di Bursa Efek</td>
    <td style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="left"><input type="text" name="isian_3_3" size="20" class="button" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" onKeyUp="cek_status_input5(this)"></td>
    <td style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" width="10%" align="right"><input type="text" name="isian_3_4" size="20" class="button" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" onKeyUp="cek_status_input5(this)"></td>
  </tr>
  <tr>
    <td style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;" align="center">3</td>
    <td style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="left">&nbsp;Penjualan Saham di Bursa Efek</td>
    <td style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="left"><input type="text" name="isian_3_5" size="20" class="button" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" onKeyUp="cek_status_input5(this)"></td>
    <td style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" width="10%" align="right"><input type="text" name="isian_3_6" size="20" class="button" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" onKeyUp="cek_status_input5(this)"></td>
  </tr>
   <tr>
    <td style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;" align="center">4</td>
    <td style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="left">&nbsp;Hadiah Utama</td>
    <td style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="left"><input type="text" name="isian_3_7" size="20" class="button" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" onKeyUp="cek_status_input5(this)"></td>
    <td style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" width="10%" align="right"><input type="text" name="isian_3_8" size="20" class="button" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" onKeyUp="cek_status_input5(this)"></td>
  </tr>
    <tr>
    <td style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;" align="center">5</td>
    <td style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="left">&nbsp;Pesangon, Tunjangan Hari Tua dan Tebusan Pensiun yang Dibayar<br>&nbsp;Sekaligus</td>
    <td style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="left"><input type="text" name="isian_3_9" size="20" class="button" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" onKeyUp="cek_status_input5(this)"></td>
    <td style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" width="10%" align="right"><input type="text" name="isian_3_10" size="20" class="button" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" onKeyUp="cek_status_input5(this)"></td>
  </tr>
    <tr>
    <td style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;" align="center">6</td>
    <td style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="left">&nbsp;Honorarium atas Beban APBN/APBD</td>
    <td style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="left"><input type="text" name="isian_3_11" size="20" class="button" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" onKeyUp="cek_status_input5(this)"></td>
    <td style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" width="10%" align="right"><input type="text" name="isian_3_12" size="20" class="button" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" onKeyUp="cek_status_input5(this)"></td>
  </tr>
  <tr>
    <td style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;" align="center">7</td>
    <td style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="left">&nbsp;Pengalihan Hak atas Tanah dan/atau Bangunan</td>
    <td style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="left"><input type="text" name="isian_3_13" size="20" class="button" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" onKeyUp="cek_status_input5(this)"></td>
    <td style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" width="10%" align="right"><input type="text" name="isian_3_14" size="20" class="button" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" onKeyUp="cek_status_input5(this)"></td>
  </tr>
  <tr>
    <td style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;" align="center">8</td>
    <td style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="left">&nbsp;Sewa atas Tanah dan/atau Bangunan</td>
    <td style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="left"><input type="text" name="isian_3_15" size="20" class="button" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" onKeyUp="cek_status_input5(this)"></td>
    <td style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" width="10%" align="right"><input type="text" name="isian_3_16" size="20" class="button" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" onKeyUp="cek_status_input5(this)"></td>
  </tr>
  <tr>
    <td style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;" align="center">9</td>
    <td style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="left">&nbsp;Bangunan yang Diterima Dalam Rangka Bangun Guna Serah</td>
    <td style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="left"><input type="text" name="isian_3_17" size="20" class="button" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" onKeyUp="cek_status_input5(this)"></td>
    <td style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" width="10%" align="right"><input type="text" name="isian_3_18" size="20" class="button" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" onKeyUp="cek_status_input5(this)"></td>
  </tr>
  <tr>
    <td style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;" align="center">10</td>
    <td style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="left">&nbsp;Penghasilan yang Dikenakan Pajak Bersifat Final :</td>
    <td style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="left">&nbsp;</td>
    <td style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" width="10%" align="right">&nbsp;</td>
  </tr>
  <tr>
    <td style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;" align="center">&nbsp;</td>
    <td style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="left">&nbsp;10a.&nbsp;&nbsp;Penghasilan Isteri dari Satu Pemberi Kerja</td>
    <td style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="left"><input type="text" name="isian_3_19" size="20" class="button" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" onKeyUp="cek_status_input5(this)"></td>
    <td style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" width="10%" align="right"><input type="text" name="isian_3_20" size="20" class="button" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" onKeyUp="cek_status_input5(this)"></td>
  </tr>
  <tr>
    <td style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;" align="center">&nbsp;</td>
    <td style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="left">&nbsp;10b.&nbsp;&nbsp;Penghasilan Anak dari Pekerjaan</td>
    <td style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="left"><input type="text" name="isian_3_21" size="20" class="button" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" onKeyUp="cek_status_input5(this)"></td>
    <td style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" width="10%" align="right"><input type="text" name="isian_3_22" size="20" class="button" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" onKeyUp="cek_status_input5(this)"></td>
  </tr>
  <tr>
    <td style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;" align="center">11</td>
    <td style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="left">&nbsp;Penghasilan Lain yang dikenakan Pajak Final dan/atau Bersifat Final</td>
    <td style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="left"><input type="text" name="isian_3_23" size="20" class="button" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" onKeyUp="cek_status_input5(this)"></td>
    <td style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" width="10%" align="right"><input type="text" name="isian_3_24" size="20" class="button" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" onKeyUp="cek_status_input5(this)"></td>
  </tr>
  <tr>
    <td style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;BORDER-LEFT: 1px solid #000;" align="center">&nbsp;</td>
    <td style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="center">&nbsp;JUMLAH BAGIAN A</td>
    <td style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" align="right">
	<input type="text" name="isian_3_25b_view" size="20" class="button" readonly="readonly">
    <input type="hidden" name="isian_3_25b" size="20" class="button" readonly="readonly"></td>
    <td style="BORDER-RIGHT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;" width="10%" align="right">
    <input type="text" name="isian_3_25_view" size="20" class="button" readonly="readonly">
    <input type="hidden" name="isian_3_25" size="20" class="button" readonly="readonly"></td>
  </tr>
  </table>
 </td>
 </tr>
    <tr>
    <td colspan="4">
  	 <table border="0" cellspacing="0" cellpadding="0">
      <tr class="header_teks_2">
        <td><br>&nbsp;<b>BAGIAN B :</b></td>
        <td><br>&nbsp;<b>DAFTAR HARTA PADA AKHIR TAHUN</b></td>
      </tr>
			</table>
		<br>
	  </td>
    </tr>
    
   <tr>
   <td colspan="4" >
   <table cellpadding="1" cellspacing="0" border="0" width="100%" class="normal_teks">
   <tr>
   <td width="2%" align="center" style="BORDER-TOP: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-LEFT: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><b>NO</b></td>
   <td width="40%" align="center" style="BORDER-TOP: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><b>JENIS HARTA</b></td>
   <td width="15%" align="center" style="BORDER-TOP: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><b>TAHUN PEROLEHAN</b></td>
   <td width="25%" align="center" style="BORDER-TOP: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><b>HARGA PEROLEHAN<br>(Rupiah)</b></td>
   <td width="10%" align="center" style="BORDER-TOP: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><b>KETERANGAN</b></td>
   </tr>
   <tr>
   <td width="2%" align="center" style="BORDER-LEFT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;">(1)</td>
   <td width="58%" align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;">(2)</td>
   <td width="15%" align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;">(3)</td>
   <td width="15%" align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;">(4)</td>
   <td width="10%" align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;">(5)</td>
   </tr>
   <tr>
   <td width="2%" align="center" style="BORDER-LEFT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_26" size="1" maxlength="2" class="button_1" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;"></td>
   <td width="58%" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_27" size="40" class="button_2"></td>
   <td width="15%" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_28" size="8" class="button_2"></td>
   <td width="15%" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_29" size="20" class="button" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" onKeyUp="cek_status_input6(this)"></td>
   <td width="10%" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_30" size="15" class="button_2"></td>
   </tr>
   <tr>
   <td width="2%" align="center" style="BORDER-LEFT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_31" size="1" maxlength="2" class="button_1" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;"></td>
   <td width="58%" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_32" size="40" class="button_2"></td>
   <td width="15%" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_33" size="8" class="button_2"></td>
   <td width="15%" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_34" size="20" class="button" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" onKeyUp="cek_status_input6(this)"></td>
   <td width="10%" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_35" size="15" class="button_2"></td>
   </tr>
   <tr>
   <td width="2%" align="center" style="BORDER-LEFT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_36" size="1" maxlength="2" class="button_1" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;"></td>
   <td width="58%" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_37" size="40" class="button_2"></td>
   <td width="15%" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_38" size="8" class="button_2"></td>
   <td width="15%" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_39" size="20" class="button" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" onKeyUp="cek_status_input6(this)"></td>
   <td width="10%" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_40" size="15" class="button_2"></td>
   </tr>
   <tr>
   <td width="2%" align="center" style="BORDER-LEFT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_41" size="1" maxlength="2" class="button_1" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;"></td>
   <td width="58%" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_42" size="40" class="button_2"></td>
   <td width="15%" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_43" size="8" class="button_2"></td>
   <td width="15%" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_44" size="20" class="button" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" onKeyUp="cek_status_input6(this)"></td>
   <td width="10%" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_45" size="15" class="button_2"></td>
   </tr>
   <tr>
   <td width="2%" align="center" style="BORDER-LEFT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_46" size="1" maxlength="2" class="button_1" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;"></td>
   <td width="58%" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_47" size="40" class="button_2"></td>
   <td width="15%" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_48" size="8" class="button_2"></td>
   <td width="15%" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_49" size="20" class="button" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" onKeyUp="cek_status_input6(this)"></td>
   <td width="10%" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_50" size="15" class="button_2"></td>
   </tr>
   <tr>
   <td width="2%" align="center" style="BORDER-LEFT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;">&nbsp;</td>
   <td width="58%" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;" align="center">JUMLAH BAGIAN B</td>
   <td width="15%" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;" align="right">JBB&nbsp;</td>
   <td width="15%" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_51_view" size="20" class="button" readonly="readonly">
   <input type="hidden" name="isian_3_51" size="20" class="button" readonly="readonly"></td>
   <td width="10%" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;">&nbsp;</td>
   </tr>
   </table>
   </td> 
   </tr>
    <tr>
    <td colspan="4">
  	 <table border="0" cellspacing="0" cellpadding="0">
      <tr class="header_teks_2">
        <td><br>&nbsp;<b>BAGIAN C :</b></td>
        <td><br>&nbsp;<b>DAFTAR KEWAJIBAN/UTANG PADA AKHIR TAHUN</b></td>
      </tr>
			</table>
		<br>
	  </td>
    </tr>
    <tr>
   <td colspan="4" >
   <table cellpadding="1" cellspacing="0" border="0" width="100%" class="normal_teks">
   <tr>
   <td width="2%" align="center" style="BORDER-TOP: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-LEFT: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><b>NO</b></td>
   <td width="25%" align="center" style="BORDER-TOP: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><b>NAMA PEMBERI PINJAMAN</b></td>
   <td width="48%" align="center" style="BORDER-TOP: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><b>ALAMAT PEMBERI PINJAMAN</b></td>
   <td width="7%" align="center" style="BORDER-TOP: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><b>TAHUN PEMINJAMAN</b></td>
   <td width="20%" align="center" style="BORDER-TOP: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><b>JUMLAH</b></td>
   </tr>
   <tr>
   <td align="center" style="BORDER-LEFT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;">(1)</td>
   <td align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;">(2)</td>
   <td align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;">(3)</td>
   <td align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;">(4)</td>
   <td align="center" style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;">(5)</td>
   </tr>
   <tr>
   <td align="center" style="BORDER-LEFT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_61" size="1" maxlength="2" class="button_1" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;"></td>
   <td style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_62" size="20" class="button_2"></td>
   <td style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_63" size="47" class="button_2"></td>
   <td style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_64" size="7" class="button_1" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;"></td>
   <td style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_65" size="15" class="button" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" onKeyUp="cek_status_input6(this)"></td>
   </tr>
   <tr>
   <td align="center" style="BORDER-LEFT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_66" size="1" maxlength="2" class="button_1" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;"></td>
   <td style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_67" size="20" class="button_2"></td>
   <td style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_68" size="47" class="button_2"></td>
   <td style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_69" size="7" class="button_1" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;"></td>
   <td style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_70" size="15" class="button" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" onKeyUp="cek_status_input6(this)"></td>
   </tr>
   <tr>
   <td align="center" style="BORDER-LEFT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_71" size="1" maxlength="2" class="button_1" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;"></td>
   <td style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_72" size="20" class="button_2"></td>
   <td style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_73" size="47" class="button_2"></td>
   <td style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_74" size="7" class="button_1" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;"></td>
   <td style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_75" size="15" class="button" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" onKeyUp="cek_status_input6(this)"></td>
   </tr>
   <tr>
   <td align="center" style="BORDER-LEFT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_76" size="1" maxlength="2" class="button_1" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;"></td>
   <td style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_77" size="20" class="button_2"></td>
   <td style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_78" size="47" class="button_2"></td>
   <td style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_79" size="7" class="button_1" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;"></td>
   <td style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_80" size="15" class="button" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" onKeyUp="cek_status_input6(this)"></td>
   </tr>
   <tr>
   <td align="center" style="BORDER-LEFT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_81" size="1" maxlength="2" class="button_1" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;"></td>
   <td style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_82" size="20" class="button_2"></td>
   <td style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_83" size="47" class="button_2"></td>
   <td style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_84" size="7" class="button_1" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;"></td>
   <td style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_85" size="15" class="button" onKeyPress="if(event.keyCode < 45 || event.keyCode > 57) event.returnValue = false;" onKeyUp="cek_status_input6(this)"></td>
   </tr>
   <tr>
   <td align="center" style="BORDER-LEFT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;">&nbsp;</td>
   <td style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;" align="center">JUMLAH BAGIAN C</td>
   <td style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;">&nbsp;</td>
   <td style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;" align="right">JBC&nbsp;</td>
   <td style="BORDER-BOTTOM: 1px solid #000;BORDER-RIGHT: 1px solid #000;"><input type="text" name="isian_3_86_view" size="20" class="button" readonly="readonly">
   <input type="hidden" name="isian_3_86" size="20" class="button" readonly="readonly"></td>
   </tr>
   </table>
   </td> 
   </tr>
   	
    </table>
    </td></tr>
    </table>
   	</td><td></td></tr>
<tr>
<td valign="bottom"><img src="../images/kotak_2.jpg"></td>
<td></td>
<td valign="bottom"><img src="../images/kotak_2.jpg"></td></tr>
	</table>
</form>
</body>
</html>
