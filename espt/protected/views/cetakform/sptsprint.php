<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<link href="<?php echo Yii::app()->baseUrl;?>/style/default.css" rel="stylesheet" type="text/css">

<title>FORM SPT</title>
</head>
    
<style>
.table-detail { width:100%; border:1px solid #000; padding:5px; border-collapse:collapse; }
.table-detail th, .table-detail td { border:1px solid #000; padding:2px; }
.table-detail .index { text-align:center; }
.table-detail .value { text-align:right; }
body{
    margin: 0px;
    font-family: "Lato", "Helvetica Neue", Helvetica, Arial, sans-serif !important;
}
.btn-success {
    color: #fff;
    background-color: #5cb85c;
    border-color: #4cae4c;
}
.btn {
    display: inline-block;
    margin-bottom: 0;
    font-weight: normal;
    text-align: center;
    vertical-align: middle;
    -ms-touch-action: manipulation;
    touch-action: manipulation;
    cursor: pointer;
    background-image: none;
    border: 1px solid transparent;
    white-space: nowrap;
    padding: 6px 12px;
    font-size: 14px;
    line-height: 1.42857143;
    border-radius: 4px;
    -webkit-user-select: none;
    -moz-user-select: none;
    -ms-user-select: none;
    user-select: none;
}
.btn-success:active:hover, .btn-success.active:hover, .open>.dropdown-toggle.btn-success:hover, .btn-success:active:focus, .btn-success.active:focus, .open>.dropdown-toggle.btn-success:focus, .btn-success:active.focus, .btn-success.active.focus, .open>.dropdown-toggle.btn-success.focus {
    color: #fff;
    background-color: #398439;
    border-color: #255625;
}
.btn-success:hover {
    color: #fff;
    background-color: #449d44;
    border-color: #398439;
}
</style>
<body>
<center>
	<script src="https://code.jquery.com/jquery-1.11.3.js"></script>
	<?php 
	if(Yii::app()->user->roles == "Admin_witel"){
	?>
	<div id="form_sarana">
		Pengiriman SPT dilakukan via: 
		<select class="form-control" name="sarana" id="sarana">
			<option value="Whatsapp">Whatsapp</option>
			<option value="Email">Email</option>
		</select>
	</div>
	<?php
	}
	?>
	<button id="btnExportpdf" class="btn btn-outline btn-success" onclick="printPdf('<?php echo Yii::app()->user->roles; ?>')">
		<i class="fa fa-file-pdf-o"></i> 
		Print Pdf
	</button>
	<script>
	function printPdf(roles){
		$("#btnExportpdf").css("display","none");
		$("#form_sarana").css("display","none");

        var sarana      = 'Pribadi';
        if(roles=='Admin_witel'){
            sarana      = $("#sarana").val();
        }
        var telkomnik   = '<?php echo empty($data1[0][50]) ? $data1[0][1]:$data1[0][50]; ?>';
        var jenis_form  = '1721-A1';
        var nomor_bukti = '<?php echo $data1[0][0]; ?>';
        var base_url    = '<?php echo Yii::app()->getBaseUrl(true); ?>';
        var url         = base_url+"/index.php?r=cetakform/downlog";

        var dummy = new iframeform(url);
        dummy.addParameter('telkomnik',telkomnik);
        dummy.addParameter('sarana',sarana);
        dummy.addParameter('jenis_form',jenis_form);
        dummy.addParameter('nomor_bukti',nomor_bukti);
        dummy.send();
		
        window.print();
	}
    function iframeform(url){
        var object = this;
        object.time = new Date().getTime();
        object.form = $('<form action="'+url+'" target="iframe'+object.time+'" method="post" style="display:none;" id="form'+object.time+'" name="form'+object.time+'"></form>');

        object.addParameter = function(parameter,value)
        {
            $("<input type='hidden' />")
             .attr("name", parameter)
             .attr("value", value)
             .appendTo(object.form);
        }

        object.send = function()
        {
            var iframe = $('<iframe data-time="'+object.time+'" style="display:none;" id="iframe'+object.time+'"></iframe>');
            $( "body" ).append(iframe); 
            $( "body" ).append(object.form);
            object.form.submit();
            iframe.load(function(){  $('#form'+$(this).data('time')).remove();  $(this).remove(); window.close();  });
        }
    }
	</script>
</center>
<form name="spt" id="id_spt" method="post">
<table border="0" cellpadding="0" cellspacing="0" class="normal_teks" width="100%" height="100%" style="margin-left:auto;margin-right:auto;">
    <tr>
        <td><img src="<?php echo Yii::app()->baseUrl;?>/images/kotak.jpg"></td>
        <td></td>
        <td><img src="<?php echo Yii::app()->baseUrl;?>/images/kotak.jpg"></td>
    </tr>
    <tr>
        <td></td>
        <td>
        <table height="931" border="0" cellpadding="0" cellspacing="0" class="normal_teks" style="font-size: 12px">
            <tr>
                <td colspan="2" style="border:1px solid #000000; border-left: none; border-top: none; text-align:center; width:20%;">
                <img src="<?php echo Yii::app()->baseUrl;?>/images/logoPajak.jpg">
                <br />
                  KEMENTERIAN KEUANGAN RI<br />
        DIREKTORAT JENDERAL PAJAK</td>
                <td width="23" style="border:1px solid #000; width:60%; border-top:none;">
                <div style="padding:10px; text-align:center; font-size:14px; font-weight:bold; height:50%;">
                    BUKTI PEMOTONGAN PAJAK PENGHASILAN PASAL 21 <br />
                BAGI PEGAWAI TETAP ATAU PENERIMA PENSIUN ATAU TUNJANGAN HARI TUA/JAMINAN HARI TUA
                BERKALA</div>
                <div style="border-top:2px solid #000; text-align:left; margin-top: 20px; font-size:12px; font-weight:500; padding:8px 10px 6px 10px;">
                <?php
                    /*$view = strlen($res->jumrec);*/
                    $no_urut = $data1[0][0];
                    $view = strlen($no_urut);
                    /*for ($x=0; $x <= $view; $x++){
                    //$tampil = substr($res->jumrec,$x,1);
                    $tampil = substr($data1[0][0],$x,1);
                    }*/
                    
                    $bln_awal = $data1[0][4];
                    $bln_akhir = $data1[0][6];
                    if(strlen($bln_awal) < 2)
                    {
                        $bln_awal = "0".$bln_awal;
                    }
                    if(strlen($bln_akhir) < 2)
                    {
                        $bln_akhir = "0".$bln_akhir;
                    }
                ?>
                    <!-- NOMOR : &nbsp;&nbsp; 1 . 1 - <?php //echo $bln_akhir.$data1[0][3].'-'.$no_urut;?> -->
                    NOMOR : &nbsp;&nbsp;<?php echo $data1[0][0] ?>
                </div>
                </td>
                <td style="width:20%; border:1px solid #000; border-top:none; border-right:none; padding-left:10px;">
                <span style="font-size:14px; font-weight:700; margin-left:30px;">
                    FORMULIR 1721 - A1
                </span>
                <br />
                Lembar ke-1 : untuk Penerima Penghasilan<br />
                Lembar ke-2 : untuk Pemotong<br />
                <div style="text-align:center; width:200px; margin-top:20px;">
                    <span style="font-size:10px; font-weight:700;">MASA PEROLEHAN<br />PENGHASILAN [mm - mm]</span>
                    <br />
                    <span style="font-weight:bold;"><?php echo $bln_awal;?> - <?php echo $bln_akhir;?></span>
                </div>
                <?php
                  $temp[0]= substr($data1[0][3],0,1);
                  $temp[1]= substr($data1[0][3],1,1);
                  $temp[2]= substr($data1[0][3],2,1);
                  $temp[3]= substr($data1[0][3],3,1);
                ?>
                <!--<table width="50%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                <td width="5">&nbsp;</td>
                <td height="40" width="20" align="center" style="BORDER-RIGHT: 1px solid #000;BORDER-TOP: 1px solid #000;BORDER-LEFT: 1px solid #000;BORDER-BOTTOM: 1px solid #000;"><b><?php echo  $temp[0];?></b></td>
                <td width="20" align="center" style="BORDER-RIGHT: 1px solid #000;BORDER-TOP: 1px solid #000;BORDER-BOTTOM: 1px solid #000;"><b><?php echo  $temp[1];?></b></td>
                <td width="20" align="center" style="BORDER-RIGHT: 1px solid #000;BORDER-TOP: 1px solid #000;BORDER-BOTTOM: 1px solid #000;"><b><?php echo  $temp[2];?></b></td>
                <td width="20" align="center" style="BORDER-RIGHT: 1px solid #000;BORDER-TOP: 1px solid #000;BORDER-BOTTOM: 1px solid #000;"><b><?php echo  $temp[3];?></b></td>
                </tr>
                </table>-->
                </td>
            </tr>
            <tr>
                <td colspan="6" style="border-left:1px solid #000; border-right:1px solid #000">
                    <table style="font-size: 12px">
                        <tr>
                            <td class="normal_teks" colspan="2">NPWP PEMOTONG</td>
                            <td  colspan="4" class="normal_teks"> : <?php echo $data1[0][13]; ?>
                           
                            </td>
                        </tr>
                        <tr>
                            <td class="normal_teks" colspan="2">NAMA PEMOTONG</td>
                            <td class="normal_teks" colspan="4"> : <?php echo strtoupper($data1[0][56]);?> </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr>
                <td colspan="6" style="padding-bottom:5px; padding-top:10px; border-top:1px solid #000; font-weight:bold;">A. IDENTITAS PENERIMA PENGHASILAN YANG DIPOTONG</td>
            </tr>
            <tr>
            <td colspan="6">
            <table style="width:100%; border:1px solid #000;">
            <tr>
            <td style="width:50%;">
                <table style="font-size:10px; width:100%;">
                <tr>
                    <td>1.</td>
                    <td>NPWP</td>
                    <td> : </td>
                    <td><?php echo $data1[0][14]; ?></td>
                </tr>
                <tr>
                    <td>2.</td>
                    <td>NIK / NO PASPOR</td>
                    <td> : </td>
                    <td><?php echo empty($data1[0][50]) ? $data1[0][1]:$data1[0][50]; ?></td>
                </tr>
                <tr>
                    <td>3.</td>
                    <td>NAMA</td>
                    <td> : </td>
                    <td><?php echo $data1[0][2]; ?></td>
                </tr>
                <tr>
                    <td>4.</td>
                    <td>ALAMAT</td>
                    <td> : </td>
                    <td><?php echo $data1[0][16]; ?></td>
                </tr>
                <tr>
                    <td colspan="3"></td>
                    <td>_____</td>
                </tr>
                <tr>
                    <td>5.</td>
                    <td>JENIS KELAMIN</td>
                    <td> : </td>
                    <td>
                        <?php
                            $kel = $data1[0][23];
                            $crossL = "cross0";
                            $crossP = "cross0";
                            
                            if ($data1[0][23]==1)
                            {
                                $crossL = "cross";
                            }
                            else
                            {
                                $crossP = "cross";
                            }
                        ?>
                          
                          <img src="<?php echo Yii::app()->baseUrl;?>/images/<?php echo $crossL; ?>.JPG" width="13" height="13" /> LAKI-LAKI
                          &nbsp;&nbsp;
                          <img src="<?php echo Yii::app()->baseUrl;?>/images/<?php echo $crossP; ?>.JPG" width="13" height="13" /> PEREMPUAN
                    </td>
                </tr>
                </table>
            </td>
            <td>
                <table style="font-size:10px; width:100%;">
                <tr>
                    <td>6.</td>
                    <td colspan="3">STATUS / JUMLAH TANGGUNGAN KELUARGA UNTUK PTKP</td>
                </tr>
                <tr>
                    <td colspan="4" style="padding-left:25px;">
                        <?php
                            $statptkp = str_replace('-', '0', $data1[0][24]);
                            $statusnikah = strstr($statptkp, "/", true);
                        ?>
                        <?php echo ($statusnikah == 'K')?'<b>'.$statptkp.'</b>':'K / ' ?>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <?php echo ($statusnikah == 'TK')?'<b>'.$statptkp.'</b>':'TK / ' ?>
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        HB / 
                    </td>
                </tr>
                <tr>
                    <td>7.</td>
                    <td>NAMA JABATAN</td>
                    <td> : </td>
                    <td style="width:70%;"><?php echo $data1[0][15]; ?></td>
                </tr>
                <tr>
                    <td>8.</td>
                    <td>KARYAWAN ASING</td>
                    <td> : </td>
                    <td><img src="<?php echo Yii::app()->baseUrl;?>/images/cross0.JPG" width="13" height="13" /> YA</td>
                </tr>
                <tr>
                    <td>9.</td>
                    <td>KODE NEGARA DOMISILI</td>
                    <td> : </td>
                    <td>_____</td>
                </tr>
                </table>
            </td>
            </tr>
            </table>
            </td>
            </tr>
            
            <tr>
                <td colspan="6" style="padding-top:10px; padding-bottom:5px; font-weight:bold;">B. RINCIAN PENGHASILAN DAN PENGHITUNGAN PPh PASAL 21</td>
            </tr>
            <tr>
            <td colspan="6">
            <table class="normal_teks table-detail" style="font-size: 12px">
                <tr>
                    <th colspan="2">URAIAN</th>
                    <th>JUMLAH (Rp)</th>
                </tr>
                <tr>
                    <th colspan="2" style="text-align:justify">
                        KODE OBJEK PAJAK : 
                        <img src="<?php echo ($data1[0][57] == '21-100-01')?Yii::app()->baseUrl.'/images/cross.JPG':Yii::app()->baseUrl.'/images/cross0.JPG'?>" width="13" height="13" /> 21-100-01
                        &nbsp;
                        <img src="<?php echo ($data1[0][57] == '21-100-02')?Yii::app()->baseUrl.'/images/cross.JPG':Yii::app()->baseUrl.'/images/cross0.JPG'?>" width="13" height="13" /> 21-100-02
                    </th>
                    <th style="background:#999"></th>
                </tr>
                <tr>
                    <th colspan="2" style="text-align:justify">PENGHASILAN BRUTO :</th>
                    <th style="background:#999"></th>
                </tr>
                <tr>
                    <td class="index">1.</td>
                    <td>Gaji / pensiun atau THT/JHT</td>
                    <td class="value"><?php echo  number_format($data1[0][25],0,",",".")?></td>
                </tr>
                <tr>
                    <td class="index">2.</td>
                    <td>Tunjangan PPh</td>
                    <td class="value"><?php echo   number_format($data1[0][26],0,",",".")?></td>
                </tr>
                <tr>
                    <td class="index">3.</td>
                    <td>Tunjangan lainnya, uang lembur, dan sebagainya</td>
                    <td class="value"><?php echo   number_format($data1[0][27],0,",",".")?></td>
                </tr>
                <tr>
                    <td class="index">4.</td>
                    <td>Honorarium dan imbalan lain sejenisnya</td>
                    <td class="value"><?php echo   number_format($data1[0][28],0,",",".")?></td>
                </tr>
                <tr>
                    <td class="index">5.</td>
                    <td>Premi asuransi yang dibayar pemberi kerja</td>
                    <td class="value"><?php echo  number_format($data1[0][29],0,",",".")?></td>
                </tr>
                <tr>
                    <td class="index">6.</td>
                    <td>Penerimaan dalam bentuk natura dan kenikmatan lainnya yang dikenakan pemotongan PPh Pasal 21</td>
                    <td class="value"><?php echo  number_format($data1[0][30],0,",",".")?></td>
                </tr>
                <tr>
                    <td class="index">7.</td>
                    <td>Tantiem, bonus, grafitifikasi, jasa produksi, dah THR</td>
                    <td class="value"><?php echo  number_format($data1[0][31],0,",",".")?> </td>
                </tr>
                <tr>
                    <td class="index">8.</td>
                    <td>Jumlah penghasilan bruto (1 S.D 7)</td>
                    <td class="value"><?php echo  number_format($data1[0][32],0,",",".")?> </td>
                </tr>
                
                <tr>
                    <th colspan="2" style="text-align:justify">PENGURANGAN :</th>
                    <th style="background:#999"></th>
                </tr>
                <tr>
                    <td class="index">9.</td>
                    <td>Biaya jabatan/biaya pensiun</td>
                    <td class="value"><?php echo  number_format($data1[0][33],0,",",".")?></td>
                </tr>
                <tr>
                    <td class="index">10.</td>
                    <td>Iuran pensiun atau iuran THT/JHT</td>
                    <td class="value"><?php echo   number_format($data1[0][34],0,",",".")?></td>
                </tr>
                <tr>
                    <td class="index">11.</td>
                    <td>Jumlah pengurangan (9 S.D 10)</td>
                    <td class="value"><?php echo   number_format($data1[0][35],0,",",".")?></td>
                </tr>
                
                <tr>
                    <th colspan="2" style="text-align:justify">PENGHITUNGAN PASAL 21 :</th>
                    <th style="background:#999"></th>
                </tr>
                <tr>
                    <td class="index">12.</td>
                    <td>Jumlah penghasilan neto (8 - 11)</td>
                    <td class="value"><?php echo   number_format($data1[0][36],0,",",".")?></td>
                </tr>
                <tr>
                    <td class="index">13.</td>
                    <td>Penghasilan neto masa sebelumnya</td>
                    <td class="value"><?php echo   number_format($data1[0][37],0,",",".")?></td>
                </tr>
                <tr>
                    <td class="index">14.</td>
                    <td>Jumlah penghasilan neto untuk penghitungan PPh Pasal 21 (setahun/disetahunkan)</td>
                    <td class="value"><?php echo   number_format($data1[0][38],0,",",".")?></td>
                </tr>
                <tr>
                    <td class="index">15.</td>
                    <td>Penghasilan tidak kena pajak (PTKP)</td>
                    <td class="value"><?php echo   number_format($data1[0][39],0,",",".")?></td>
                </tr>
                <tr>
                    <td class="index">16.</td>
                    <td>Penghasilan kena pajak setahun/disetahunkan (14 - 15)</td>
                    <td class="value"><?php echo   number_format($data1[0][40],0,",",".")?></td>
                </tr>
                <tr>
                    <td class="index">17.</td>
                    <td>PPh Pasal 21 atas penghasilan kena pajak setahun/disetahunkan</td>
                    <td class="normal_teks" align="right" style="border:1px solid #000"><?php echo   number_format($data1[0][41],0,",",".")?></td>
                </tr>
                <tr>
                    <td class="index">18.</td>
                    <td>PPh Pasal 21 yang telah dipotong masa sebelumnya</td>
                    <td class="value"><?php echo   number_format($data1[0][42],0,",",".")?></td>
                </tr>
                <tr>
                    <td class="index">19.</td>
                    <td>PPh Pasal 21 terutang</td>
                    <td class="value"><?php                    $nilai21= $data1[0][43];
                    echo number_format($data1[0][43],0,",",".");?></td>
                </tr>
                <tr>
                    <td class="index">20.</td>
                    <td>PPh Pasal 21 dan PPh Pasal 26 yang telah dipotong dan dilunasi</td>
                    <td class="value"><?php                     $nilai22=$data1[0][44];
                     echo number_format($data1[0][44],0,",",".");?></td>
                </tr>
            </table>
            </td>
            </tr>
            
            <tr>
                <td colspan="6" style="padding-top:10px; padding-bottom:5px; font-weight:bold;">C. IDENTITAS PEMOTONG</td>
            </tr>
            <tr>
            <td colspan="6">
            <table class="normal_teks" style="width:100%; border:1px solid #000; font-size: 12px">
                <tr>
                    <th style="text-align:left;">1.</th>
                    <th style="text-align:left;">NPWP</th>
                    <td style="width:60%"><?php echo $data2[0][6]; ?></td>
                    
                    <th style="width:30%">3. TANGGAL & TANDA TANGAN</th>
                    <td rowspan="2">
                        <?php                        if ($data2[0][4]!="")
                        { 
                        $gambar = $data2[0][4];
                        ?>
                          <img src="<?php echo  Yii::app()->baseUrl."/".$gambar ?>" width="80" height="50" align="absmiddle" style="border:1px solid #000"/>
                          <?php                        }
                        else
                        {
                        echo "<br>";
                        }
                        ?>
                    </td>
                </tr>
                <tr>
                    <th style="text-align:left;">2.</th>
                    <th style="text-align:left;">NAMA</th>
                    <td><?php echo $data2[0][1]; ?></td>
                    <th>
                        <?php echo $data2[0][3]; ?>
                        
                       
                    </th>
                  </tr>
            </table>
            </td>
            </tr>
        </table>
        </td>
        <td></td>
    </tr>
    <tr>
        <td><img src="<?php echo Yii::app()->baseUrl;?>/images/kotak.jpg"></td>
        <td></td>
        <td><img src="<?php echo Yii::app()->baseUrl;?>/images/kotak.jpg"></td>
    </tr>
</table>
</form>
</body>
</html>