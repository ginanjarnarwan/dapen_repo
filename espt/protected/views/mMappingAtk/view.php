<?php
/* @var $this MMappingAtkController */
/* @var $model MMappingAtk */

$this->breadcrumbs=array(
	'Mmapping Atks'=>array('index'),
	$model->ID,
);

$this->menu=array(
	array('label'=>'List MMappingAtk', 'url'=>array('index')),
	array('label'=>'Create MMappingAtk', 'url'=>array('create')),
	array('label'=>'Update MMappingAtk', 'url'=>array('update', 'id'=>$model->ID)),
	array('label'=>'Delete MMappingAtk', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->ID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage MMappingAtk', 'url'=>array('admin')),
);
?>
<div class="row">
	<div class="col-sm-12">
		<div class="box box-info">
			<div class="box-header with-border">
				<h3 class="box-title">View MMappingAtk #<?php echo $model->ID; ?></h3>
			</div>
			<?php $this->widget('zii.widgets.CDetailView', array(
			'data'=>$model,
			'attributes'=>array(
					'ID',
		'NIK',
		'SEGMENTASI_KODE',
			),
			)); ?>
		</div>
	</div>
</div>
