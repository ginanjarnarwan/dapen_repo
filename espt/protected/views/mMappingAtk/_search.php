<?php
/* @var $this MMappingAtkController */
/* @var $model MMappingAtk */
/* @var $form CActiveForm */
?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'htmlOptions'=>array('class'=>'form-horizontal')
)); ?>

<div class="form-group">
	<?php echo $form->labelEx($model,'ID', array('class'=>'col-sm-2 control-label')); ?>
	<div class="col-sm-10">
		<?php echo $form->textField($model,'ID',array('class' => 'form-control')); ?>
	</div>
</div>
<div class="form-group">
	<?php echo $form->labelEx($model,'NIK', array('class'=>'col-sm-2 control-label')); ?>
	<div class="col-sm-10">
		<?php echo $form->textField($model,'NIK',array('class' => 'form-control')); ?>
	</div>
</div>
<div class="form-group">
	<?php echo $form->labelEx($model,'SEGMENTASI_KODE', array('class'=>'col-sm-2 control-label')); ?>
	<div class="col-sm-10">
		<?php echo $form->textField($model,'SEGMENTASI_KODE',array('class' => 'form-control', 'size'=>60,'maxlength'=>255)); ?>
	</div>
</div>
	<div class="row">
		<div class="col-sm-12">
			<?php echo CHtml::submitButton('Search', array(
				'class'=>'btn btn-sm btn-primary'
			)); ?>
		</div>
	</div>
<?php $this->endWidget(); ?>
