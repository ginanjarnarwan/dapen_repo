<?php
/* @var $this MMappingAtkController */
/* @var $model MMappingAtk */

$this->breadcrumbs=array(
	'Mmapping Atks'=>array('index'),
	$model->ID=>array('view','id'=>$model->ID),
	'Update',
);
?>
<div class="row">
	<div class="col-md-12">
		<div class="box box-info">
			<div class="box-header with-border">
				<h3 class="box-title">Update MMappingAtk <?php echo $model->ID; ?></h3>
			</div>
			<?php $this->renderPartial('_form', array('model'=>$model)); ?>		</div>
	</div>
</div>
