<?php
/* @var $this MMappingAtkController */
/* @var $model MMappingAtk */
/* @var $form CActiveForm */
?>

<div class="box-body">

    <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'mmapping-atk-form',
	'enableAjaxValidation'=>false,
        'htmlOptions'=>array('class'=>'form-horizontal')
)); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

            <div class="form-group">
            <?php echo $form->labelEx($model,'NIK', array('class'=>'col-sm-2 control-label')); ?>
            <div class="col-md-10">
                <?php echo $form->textField($model, 'NIK', array('class' => 'form-control')); ?>
                <?php echo $form->error($model,'NIK', array('class'=>'alert alert-danger')); ?>
            </div>
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'SEGMENTASI_KODE', array('class'=>'col-sm-2 control-label')); ?>
            <div class="col-md-10">
                <?php echo $form->textField($model, 'SEGMENTASI_KODE', array('class' => 'form-control')); ?>
                <?php echo $form->error($model,'SEGMENTASI_KODE', array('class'=>'alert alert-danger')); ?>
            </div>
        </div>

        </div>

<div class="box-footer">
    <?php echo CHtml::link('Cancel', Yii::app()->controller->createUrl('index'), array('class' => 'btn btn-sm btn-default')); ?> 
    <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-sm btn-info pull-right')); ?> 
</div>

<?php $this->endWidget(); ?>
