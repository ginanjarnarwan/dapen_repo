<?php
/* @var $this MMappingAtkController */
/* @var $model MMappingAtk */

$this->breadcrumbs=array(
	'Mmapping Atks'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List MMappingAtk', 'url'=>array('index')),
array('label'=>'Manage MMappingAtk', 'url'=>array('admin')),
);
?>
<div class="row">
	<div class="col-md-12">
		<div class="box box-info">
			<div class="box-header with-border">
				<h3 class="box-title">Create MMappingAtk</h3>
			</div>
			<?php $this->renderPartial('_form', array('model'=>$model)); ?>		</div>
	</div>
</div>
