<?php
/* @var $this CDINASController */
/* @var $model CDINAS */

$this->breadcrumbs=array(
	'Cdinases'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List CDINAS', 'url'=>array('index')),
array('label'=>'Manage CDINAS', 'url'=>array('admin')),
);
?>
<div class="col-md-12">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Create CDINAS</h3>        
        </div>
        <?php $this->renderPartial('_form', array('model'=>$model)); ?>    </div>
</div>