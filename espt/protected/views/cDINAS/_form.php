<?php
/* @var $this CDINASController */
/* @var $model CDINAS */
/* @var $form CActiveForm */
?>

<div class="box-body">

    <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'cdinas-form',
	'enableAjaxValidation'=>false,
        'htmlOptions'=>array('class'=>'form-horizontal')
)); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

            <div class="form-group">
            <?php echo $form->labelEx($model,'DINASID', array('class'=>'col-sm-2 control-label')); ?>
            <div class="col-md-10">
                <?php echo $form->textField($model, 'DINASID', array('class' => 'form-control')); ?>
                <?php echo $form->error($model,'DINASID', array('class'=>'alert alert-danger')); ?>
            </div>    
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'BAID', array('class'=>'col-sm-2 control-label')); ?>
            <div class="col-md-10">
                <?php echo $form->textField($model, 'BAID', array('class' => 'form-control')); ?>
                <?php echo $form->error($model,'BAID', array('class'=>'alert alert-danger')); ?>
            </div>    
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'NAMADINAS', array('class'=>'col-sm-2 control-label')); ?>
            <div class="col-md-10">
                <?php echo $form->textField($model, 'NAMADINAS', array('class' => 'form-control')); ?>
                <?php echo $form->error($model,'NAMADINAS', array('class'=>'alert alert-danger')); ?>
            </div>    
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'ACTIVE', array('class'=>'col-sm-2 control-label')); ?>
            <div class="col-md-10">
                <?php echo $form->textField($model, 'ACTIVE', array('class' => 'form-control')); ?>
                <?php echo $form->error($model,'ACTIVE', array('class'=>'alert alert-danger')); ?>
            </div>    
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'MAXVALUE', array('class'=>'col-sm-2 control-label')); ?>
            <div class="col-md-10">
                <?php echo $form->textField($model, 'MAXVALUE', array('class' => 'form-control')); ?>
                <?php echo $form->error($model,'MAXVALUE', array('class'=>'alert alert-danger')); ?>
            </div>    
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'BA_MJ', array('class'=>'col-sm-2 control-label')); ?>
            <div class="col-md-10">
                <?php echo $form->textField($model, 'BA_MJ', array('class' => 'form-control')); ?>
                <?php echo $form->error($model,'BA_MJ', array('class'=>'alert alert-danger')); ?>
            </div>    
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'BA_ID', array('class'=>'col-sm-2 control-label')); ?>
            <div class="col-md-10">
                <?php echo $form->textField($model, 'BA_ID', array('class' => 'form-control')); ?>
                <?php echo $form->error($model,'BA_ID', array('class'=>'alert alert-danger')); ?>
            </div>    
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'BA_IF', array('class'=>'col-sm-2 control-label')); ?>
            <div class="col-md-10">
                <?php echo $form->textField($model, 'BA_IF', array('class' => 'form-control')); ?>
                <?php echo $form->error($model,'BA_IF', array('class'=>'alert alert-danger')); ?>
            </div>    
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'IF_VALUE_2009', array('class'=>'col-sm-2 control-label')); ?>
            <div class="col-md-10">
                <?php echo $form->textField($model, 'IF_VALUE_2009', array('class' => 'form-control')); ?>
                <?php echo $form->error($model,'IF_VALUE_2009', array('class'=>'alert alert-danger')); ?>
            </div>    
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'BA_UBIS', array('class'=>'col-sm-2 control-label')); ?>
            <div class="col-md-10">
                <?php echo $form->textField($model, 'BA_UBIS', array('class' => 'form-control')); ?>
                <?php echo $form->error($model,'BA_UBIS', array('class'=>'alert alert-danger')); ?>
            </div>    
        </div>

                <div class="form-group">
            <?php echo $form->labelEx($model,'KODE_UNIT', array('class'=>'col-sm-2 control-label')); ?>
            <div class="col-md-10">
                <?php echo $form->textField($model, 'KODE_UNIT', array('class' => 'form-control')); ?>
                <?php echo $form->error($model,'KODE_UNIT', array('class'=>'alert alert-danger')); ?>
            </div>    
        </div>

        </div>

<div class="box-footer">
    <?php echo CHtml::link('Cancel', Yii::app()->controller->createUrl('index'), array('class' => 'btn btn-default')); ?> 
    <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-info pull-right')); ?> 
        
</div>

<?php $this->endWidget(); ?>
