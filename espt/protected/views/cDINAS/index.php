<?php
/* @var $this CDINASController */
/* @var $model CDINAS */

$this->breadcrumbs=array(
	'Cdinases'=>array('index'),
	'Manage',
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$('#cdinas-grid').yiiGridView('update', {
data: $(this).serialize()
});
return false;
});
");
?>
<div class="col-md-12">
    <div class="box">
        <div class="box-header with-border">
            <h3 class="box-title">Manage Cdinases</h3>
            <div class="pull-right">
                <?php echo CHtml::link("Create", Yii::app()->controller->createUrl("create"), array("class" => "btn btn-primary")); ?> 
            </div>
        </div>
        <div class="box-body">
            <?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?> 
            <div class="search-form" style="display:none">
                <?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
            </div><!-- search-form -->

            <?php $this->widget('zii.widgets.grid.CGridView', array(
            'id'=>'cdinas-grid',
            'dataProvider'=>$model->search(),
            'filter'=>$model,
            'enableSorting' => false,
            'itemsCssClass' => 'table table-bordered table-striped',
            'columns'=>array(
            		'DINASID',
		'BAID',
		'NAMADINAS',
		'ACTIVE',
		'MAXVALUE',
		'BA_MJ',
		/*
		'BA_ID',
		'BA_IF',
		'IF_VALUE_2009',
		'BA_UBIS',
		'KODE_UNIT',
		*/
            array(
            'class'=>'CButtonColumn',
            'buttons' => array(
                'view' => array(
                    'label' => '<i class="fa fa-external-link"></i>',
                    'imageUrl' => FALSE,
                    'options' => array('class' => 'btn btn-primary btn-condensed btn-sm')
                ),
                'update' => array(
                    'label' => '<i class="fa fa-pencil"></i>',
                    'imageUrl' => FALSE,
                    'options' => array('class' => 'btn btn-warning btn-condensed btn-sm')
                ),
                'delete' => array(
                    'label' => '<i class="fa fa-trash"></i>',
                    'imageUrl' => FALSE,
                    'options' => array('class' => 'btn btn-danger btn-condensed btn-sm')
                ),
                ),
                'htmlOptions' => array('style' => 'width:130px;text-align:center;')
            ),
            ),
            )); ?>
        </div>
    </div>
</div>