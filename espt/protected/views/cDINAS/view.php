<?php
/* @var $this CDINASController */
/* @var $model CDINAS */

$this->breadcrumbs=array(
	'Cdinases'=>array('index'),
	$model->DINASID,
);

$this->menu=array(
	array('label'=>'List CDINAS', 'url'=>array('index')),
	array('label'=>'Create CDINAS', 'url'=>array('create')),
	array('label'=>'Update CDINAS', 'url'=>array('update', 'id'=>$model->DINASID)),
	array('label'=>'Delete CDINAS', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->DINASID),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage CDINAS', 'url'=>array('admin')),
);
?>

<h1>View CDINAS #<?php echo $model->DINASID; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'DINASID',
		'BAID',
		'NAMADINAS',
		'ACTIVE',
		'MAXVALUE',
		'BA_MJ',
		'BA_ID',
		'BA_IF',
		'IF_VALUE_2009',
		'BA_UBIS',
		'KODE_UNIT',
	),
)); ?>
