<?php
/* @var $this CDINASController */
/* @var $model CDINAS */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'DINASID'); ?>
		<?php echo $form->textField($model,'DINASID'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'BAID'); ?>
		<?php echo $form->textField($model,'BAID'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'NAMADINAS'); ?>
		<?php echo $form->textField($model,'NAMADINAS',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'ACTIVE'); ?>
		<?php echo $form->textField($model,'ACTIVE'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'MAXVALUE'); ?>
		<?php echo $form->textField($model,'MAXVALUE'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'BA_MJ'); ?>
		<?php echo $form->textField($model,'BA_MJ'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'BA_ID'); ?>
		<?php echo $form->textField($model,'BA_ID',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'BA_IF'); ?>
		<?php echo $form->textField($model,'BA_IF'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'IF_VALUE_2009'); ?>
		<?php echo $form->textField($model,'IF_VALUE_2009'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'BA_UBIS'); ?>
		<?php echo $form->textField($model,'BA_UBIS',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'KODE_UNIT'); ?>
		<?php echo $form->textField($model,'KODE_UNIT',array('size'=>10,'maxlength'=>10)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->