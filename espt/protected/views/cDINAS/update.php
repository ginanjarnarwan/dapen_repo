<?php
/* @var $this CDINASController */
/* @var $model CDINAS */

$this->breadcrumbs=array(
	'Cdinases'=>array('index'),
	$model->DINASID=>array('view','id'=>$model->DINASID),
	'Update',
);
?>
<div class="col-md-12">
    <div class="box box-info">
        <div class="box-header with-border">
            <h3 class="box-title">Update CDINAS <?php echo $model->DINASID; ?></h3>        
        </div>
        <?php $this->renderPartial('_form', array('model'=>$model)); ?>    </div>
</div>