<script type="text/javascript">
    var basap = {};
    jQuery(document).ready(function($){
        parent.autoSizeFrame();
        reloadGrid();
    });
    function reloadGrid(){
        if(localStorage.basap != undefined){
            var id = "basap-grid";
            var basaps = JSON.parse(localStorage.getItem("basap"));
            $.each(basaps, function(key, value){
                $("#" + id).find("tbody > tr[id_basap=\'"+ key +"\']").addClass("danger");
                $("#" + id).find("tbody > tr[id_basap=\'"+ key +"\']").find("a").addClass("btn-danger");
                $("#" + id).find("tbody > tr[id_basap=\'"+ key +"\']").find("a").attr("is_pilih", "YA");
            });
            basap = basaps;
        }
    }
    function pilihBasap(obj){
        var status = 'YA';
        var index = $(obj).attr('value')
        basap[index] = index;
        $(obj).removeClass();
        $(obj).parents('tr').removeClass('danger');
        if($(obj).attr('is_pilih') == 'YA'){
            status = 'TIDAK';
            delete basap[index];
            $(obj).addClass('btn btn-sm btn-primary');
        }else{
            $(obj).parents('tr').addClass('danger');
            $(obj).addClass('btn btn-sm btn-danger');
        }
        $(obj).attr('is_pilih', status);
        localStorage.setItem('basap', JSON.stringify(basap));
    }
    function selesaiPilih(){
        parent.closeModal(localStorage.getItem("basap"));
        localStorage.removeItem("basap")
    }

</script>
<style media="screen">
.table > thead > tr > th {
    text-transform: uppercase;
    font-size: 10px;
}
.table > tbody > tr > td {
    font-size: 10px;
}
</style>
<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id' => 'basap-grid',
    'dataProvider' => $model->search(),
    'afterAjaxUpdate' =>'function(id, data){
        parent.autoSizeFrame();
        if(localStorage.basap != undefined){
            var basaps = JSON.parse(localStorage.getItem("basap"));
            $.each(basaps, function(key, value){
                $("#" + id).find("tbody > tr[id_basap=\'"+ key +"\']").addClass("danger");
                $("#" + id).find("tbody > tr[id_basap=\'"+ key +"\']").find("a").addClass("btn-danger");
            });
        }
    }',
    'cssFile' => false,
    'filter' => $model,
    'itemsCssClass' => 'table table-bordered table-condensed table-striped table-hover',
    'pagerCssClass'=>'paging',
    'rowHtmlOptionsExpression' => 'array("id_basap"=>$data->BASAP)',
    'pager'=>array(
        'class'=>'CLinkPager',
        'header'=>'',
        'selectedPageCssClass'=>'active',
        'htmlOptions'=>array(
            'class'=>'pagination pagination-sm'
        )
    ),
    'columns' => array(
        array('header' => 'No', 'value' => '$this->grid->dataProvider->pagination->currentPage * $this->grid->dataProvider->pagination->pageSize + ($row+1)',  'htmlOptions' => array('style' => 'width: 16px')),
        'BASAP',
        'BANAME',
        array(
            'type'=>'raw',
            'htmlOptions'=>array(
                'style'=>'text-align:center'
            ),
            'value'=>'CHtml::link("<i class=\"fa fa-check\"></i>", "javascript:void(0);", array(
                "is_pilih"=>"TIDAK",
                "class"=>"btn btn-sm btn-primary",
                "value"=>$data->BASAP,
                "onClick"=>"pilihBasap(this)"
            ))',
        )
    )
));?>
<?php echo CHtml::link('SELESAI', "javascript:void(0)", array(
    'onClick'=>'selesaiPilih()',
    'class'=>'btn btn-block btn-sm btn-success'
)) ?>
