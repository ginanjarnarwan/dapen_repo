<!-- <div class="col-md-12"> -->
    <!-- <div class="widget p-lg"> -->
        <h4 class="m-b-lg"><?php echo $id;?></h4>
        <!-- <div class="col-sm-12">
            <div class="widget bg-info">
                <div class="widget-body">
                    <div class="m-b-l"> -->
                    <?php $form=$this->beginWidget('CActiveForm', array(
                            'id'=>'form-search',
                            'enableAjaxValidation'=>false,
                                'htmlOptions'=>array('class'=>'form-inline','enctype' => 'multipart/form-data',)
                        )); ?>
                            <center>
                            <div class="form-group">
                                <label for="exampleInputName2"><b>Filter :</b></label>
                            </div>
                            <br>
                            <div class="form-group">
                                <label>NIK</label>
                                <?php //echo $form->dropDownList($model, 'NIK', array('2017'=>'2017', '2018'=>'2018', '2019'=>'2019', '2020'=>'2020', '2021'=>'2021', '2022'=>'2022'), array('name'=>'TRealsapex[TAHUNFILTER]','prompt'=>'Pilih Tahun','class'=>'form-control')); ?>
                                <?php echo $form->textField($model, 'NIK', array('class'=>'form-control')); ?>
                                <?php echo $form->error($model,'NIK', array('class'=>'alert alert-danger')); ?>
                            </div>
                            <div class="form-group">
                                <label>Nama</label>
                                <?php //echo $form->dropdownlist($model, 'NAMA',array('1'=>'Januari','2'=>'Februari','3'=>'Maret','4'=>'April','5'=>'Mei','6'=>'Juni','7'=>'Juli','8'=>'Agustus','9'=>'September','10'=>'Oktober','11'=>'November','12'=>'Desember'), array('name'=>'TRealsapex[BULANFILTER]','class' => 'form-control','prompt'=>'Pilih Bulan')); ?>
                                <?php echo $form->textField($model, 'NAMA', array('class'=>'form-control')); ?>
                                <?php echo $form->error($model,'NAMA', array('class'=>'alert alert-danger')); ?>
                            </div>
                            <div class="form-group">
                                <label>Divisi</label>
                                <?php echo $form->textField($model, 'DIVISI', array('class'=>'form-control')); ?>
                                <?php echo $form->error($model,'DIVISI', array('class'=>'alert alert-danger')); ?>
                            </div>
                            <div class="form-group">
                                <label>Unit</label>
                                <?php echo $form->textField($model, 'NAMA_UNIT', array('class'=>'form-control')); ?>
                                <?php echo $form->error($model,'NAMA_UNIT', array('class'=>'alert alert-danger')); ?>
                            </div>
                            <div class="form-group">
                                <label>Posisi</label>
                                <?php echo $form->textField($model, 'NAMA_POSISI', array('class'=>'form-control')); ?>
                                <?php echo $form->error($model,'NAMA_POSISI', array('class'=>'alert alert-danger')); ?>
                            </div>
                            <button type="submit" class="btn btn-default">Filter</button>
                            </center>
                        <?php $this->endWidget(); ?>
                    <!-- </div>
                </div>
            </div>
        </div> -->
        <div class="table-responsive">
            <?php
            $this->widget('zii.widgets.grid.CGridView', array(
                'id' => 'a-grid-id',
                'dataProvider' => $model->detailkaryawansegmen($id,$bulan,$tahun),
                // 'filter'=>$model,
                'itemsCssClass' => 'table table-bordered table-striped table-hover',
                'columns' => array(
                    array(
                        'type' => 'raw',
                        'header' => 'No',
                        'htmlOptions' => array(
                            'style' => 'text-align:right',
                        ),
                        'headerHtmlOptions' => array(
                            'style' => 'text-align:right'
                        ),
                        'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
                    ),
                    'NIK',
                    'NAMA',
                    'BAND',
                    'DIVISI',
                    'NAMA_POSISI',
                    'NAMA_UNIT',
                    array(
                        'header'=>'Pilih',
                        'type'=>'raw',
                        'value'=>'CHtml::tag("a", array("href"=>"javascript: void(0);", "nik"=>$data[\'NIK\'],"nama"=>$data[\'NAMA\'], "class"=>"pil", "onclick"=>"pilih(this);"),"<i class=\'fa fa-check\'></i>")',
                    ),
                ),
            ));
            ?>
        </div>
    <!-- </div> -->
<!-- </div> -->
<script>
function pilih(e)
{
    var nik = $(e).attr('nik');
    var nama = $(e).attr('nama');
    $('#<?php echo $temp;?>',parent.document).val(nik);
    $('#nama',parent.document).val(nama);
    $('.modal',parent.document).find('.close').trigger('click');
}
</script>