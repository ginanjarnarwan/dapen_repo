<?php
/* @var $this AuthassignmentController */
/* @var $model Authassignment */

$this->breadcrumbs=array(
	'Authassignments'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Authassignment', 'url'=>array('index')),
	array('label'=>'Create Authassignment', 'url'=>array('create')),
	array('label'=>'Update Authassignment', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Authassignment', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Authassignment', 'url'=>array('admin')),
);
?>
<div class="row">
	<div class="col-sm-12">
		<div class="box box-info">
			<div class="box-header with-border">
				<h3 class="box-title">View Authassignment #<?php echo $model->id; ?></h3>
			</div>
			<?php $this->widget('zii.widgets.CDetailView', array(
			'data'=>$model,
			'attributes'=>array(
					'id',
		'itemname',
		'userid',
		'bizrule',
		'data',
			),
			)); ?>
		</div>
	</div>
</div>
