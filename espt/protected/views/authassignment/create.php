<?php
/* @var $this AuthassignmentController */
/* @var $model Authassignment */

$this->breadcrumbs=array(
	'Authassignments'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List Authassignment', 'url'=>array('index')),
array('label'=>'Manage Authassignment', 'url'=>array('admin')),
);
?>
<div class="row">
	<div class="col-md-12">
		<div class="widget p-lg">
			<h3 class="box-title">Create Authassignment</h3>
			<?php $this->renderPartial('_form', array('model'=>$model)); ?>		
		</div>
	</div>
</div>
