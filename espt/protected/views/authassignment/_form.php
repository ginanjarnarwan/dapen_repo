<?php
/* @var $this AuthassignmentController */
/* @var $model Authassignment */
/* @var $form CActiveForm */
?>
<div id="dlg" class="modal fade" role="dialog"></div>
<div class="box-body">

    <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'authassignment-form',
	'enableAjaxValidation'=>false,
        'htmlOptions'=>array('class'=>'form-horizontal')
)); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

        <div class="form-group">
            <?php echo $form->labelEx($model,'itemname', array('class'=>'col-sm-2 control-label')); ?>
            <div class="col-md-10">
                <?php echo $form->dropDownList($model, 'itemname', array('Admin'=>'Admin','View'=>'View'), array('class' => 'form-control','prompt'=>'-- Pilih Role --','options'=>array($model->itemname=>array('selected'=>true)))); ?>
                <?php echo $form->error($model,'itemname', array('class'=>'alert alert-danger')); ?>
            </div>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model,'userid', array('class'=>'col-sm-2 control-label')); ?>
            <div class="col-md-10">
                <div class="input-group">
                    <?php echo $form->textField($model, 'userid', array('class' => 'form-control','readonly'=>'readonly')); ?>
                    <span class="input-group-btn">
                        <button class="btn btn-secondary" type="button" onclick="showmodal(this)" temp="Authassignment_userid"><i class="fa fa-search"></i></button>
                    </span>
                </div>
                <?php echo $form->error($model,'userid', array('class'=>'alert alert-danger')); ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Nama</label>
            <div class="col-md-10">
                <input type="text" readonly="readonly" name="nama" id="nama" class="form-control">
            </div>
        </div>

        <!-- <div class="form-group">
            <?php echo $form->labelEx($model,'bizrule', array('class'=>'col-sm-2 control-label')); ?>
            <div class="col-md-10">
                <?php echo $form->textField($model, 'bizrule', array('class' => 'form-control')); ?>
                <?php echo $form->error($model,'bizrule', array('class'=>'alert alert-danger')); ?>
            </div>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model,'data', array('class'=>'col-sm-2 control-label')); ?>
            <div class="col-md-10">
                <?php echo $form->textField($model, 'data', array('class' => 'form-control')); ?>
                <?php echo $form->error($model,'data', array('class'=>'alert alert-danger')); ?>
            </div>
        </div> -->

        </div>

<div class="box-footer">
    <?php echo CHtml::link('Cancel', Yii::app()->controller->createUrl('index'), array('class' => 'btn btn-sm btn-default')); ?> 
    <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-sm btn-info pull-right')); ?> 
</div>

<?php $this->endWidget(); ?>
<script>
showmodal = function(e){
        var temp = $(e).attr("temp");
        var link = '<?php echo Yii::app()->controller->createUrl("popshowman");?>&temp='+temp;
        $("#dlg").modal('show');
        $("#dlg").load(link, function (response, status, xhr) {
            if (status == "error") {
                $("#dlg").html(response);
            }
        });
        event.preventDefault();
    }
</script>