<?php
/* @var $this AuthassignmentController */
/* @var $model Authassignment */
/* @var $form CActiveForm */
?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'htmlOptions'=>array('class'=>'form-horizontal')
)); ?>

<div class="form-group">
	<?php echo $form->labelEx($model,'id', array('class'=>'col-sm-2 control-label')); ?>
	<div class="col-sm-10">
		<?php echo $form->textField($model,'id',array('class' => 'form-control')); ?>
	</div>
</div>
<div class="form-group">
	<?php echo $form->labelEx($model,'itemname', array('class'=>'col-sm-2 control-label')); ?>
	<div class="col-sm-10">
		<?php echo $form->textField($model,'itemname',array('class' => 'form-control', 'size'=>60,'maxlength'=>64)); ?>
	</div>
</div>
<div class="form-group">
	<?php echo $form->labelEx($model,'userid', array('class'=>'col-sm-2 control-label')); ?>
	<div class="col-sm-10">
		<?php echo $form->textField($model,'userid',array('class' => 'form-control', 'size'=>60,'maxlength'=>64)); ?>
	</div>
</div>
<!-- <div class="form-group">
	<?php echo $form->labelEx($model,'bizrule', array('class'=>'col-sm-2 control-label')); ?>
	<div class="col-sm-10">
		<?php echo $form->textArea($model,'bizrule',array('class' => 'form-control', 'rows'=>6, 'cols'=>50)); ?>
	</div>
</div>
<div class="form-group">
	<?php echo $form->labelEx($model,'data', array('class'=>'col-sm-2 control-label')); ?>
	<div class="col-sm-10">
		<?php echo $form->textArea($model,'data',array('class' => 'form-control', 'rows'=>6, 'cols'=>50)); ?>
	</div>
</div> -->
	<div class="row">
		<div class="col-sm-12">
			<?php echo CHtml::submitButton('Search', array(
				'class'=>'btn btn-sm btn-primary'
			)); ?>
		</div>
	</div>
<?php $this->endWidget(); ?>
