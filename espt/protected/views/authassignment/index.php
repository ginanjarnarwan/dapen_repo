<?php
/* @var $this AuthassignmentController */
/* @var $model Authassignment */

$this->breadcrumbs=array(
	'Authassignments'=>array('index'),
	'Manage',
);


Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
$('.search-form').toggle();
return false;
});
$('.search-form form').submit(function(){
$('#authassignment-grid').yiiGridView('update', {
data: $(this).serialize()
});
return false;
});
");
?>
<div class="row">
<div class="col-md-12">
	<div class="widget p-lg">
			<h4 class="box-title">Manage User</h4>
			<div class="pull-right">
				<?php echo CHtml::link("Create", Yii::app()->controller->createUrl("create"), array("class" => "btn btn-sm btn-primary")); ?> 
			</div>
				<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button btn btn-outline btn-info')); ?> 
				<div class="search-form" style="display:none">
					<div class="box box-solid box-primary">
						<div class="box-body">
							<?php $this->renderPartial('_search',array(
								'model'=>$model,
							)); ?>
						</div>
					</div>
				</div><!-- search-form -->
				<?php $this->widget('zii.widgets.grid.CGridView', array(
				'id'=>'authassignment-grid',
				'dataProvider'=>$model->search(),
				// 'filter'=>$model,
				'enableSorting' => false,
				'itemsCssClass' => 'table table-bordered table-striped',
				'pagerCssClass'=>'paging',
				'pager'=>array(
					'class'=>'CLinkPager',
					'header'=>'',
					'selectedPageCssClass'=>'active',
					'htmlOptions'=>array(
						'class'=>'pagination pagination-sm'
					)
				),
				'columns'=>array(
						'id',
		'itemname',
		'userid',
		array(
			'header'=>'Nama',
			'value'=>'MyApp::lookupnama($data->userid)',
		),
				array(
				'class'=>'CButtonColumn',
				'template' => '{view}{update}{deletes}',
				'buttons' => array(
				'view' => array(
				'label' => '<i class="fa fa-external-link"></i>',
				'imageUrl' => FALSE,
				'options' => array('class' => 'btn btn-primary btn-condensed btn-sm')
				),
				'update' => array(
				'label' => '<i class="fa fa-pencil"></i>',
				'imageUrl' => FALSE,
				'options' => array('class' => 'btn btn-warning btn-condensed btn-sm')
				),
				'deletes' => array(
					'url' => 'Yii::app()->controller->createUrl("deletes",array("id"=>$data->userid))',
					'label' => '<i class="fa fa-trash"></i>',
					'confirmation'=>'Apakah anda yakin akan menghapus?',
					'imageUrl' => FALSE,
					'options' => array('class' => 'btn btn-danger btn-condensed btn-sm del-btn','onclick'=>'del(this);')
				),
				),
				'htmlOptions' => array('style' => 'width:130px;text-align:center;')
				),
				),
				)); ?>
	</div>
</div>
</div>