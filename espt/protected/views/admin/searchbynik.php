<div class="col-md-12">
    <div class="block full">
        <!-- Main Sidebar Title -->
        <div class="block-title">
            <h2>Search By NIK</h2>
        </div>
        <form>
            <div class="form-group">
                <input type="text" id="search-term" name="search-term" class="form-control" placeholder="NIK..">
            </div>
            <div class="text-center">
                <button type="submit" class="btn btn-effect-ripple btn-effect-ripple btn-primary subm" style="overflow: hidden; position: relative;">Search</button>
            </div>
        </form>
    </div>
</div>
<div class="row">
    <div class="col-md-12">
        <div id="konten"></div>
    </div>
</div>
<script>
$('.subm').click(function(e){
    e.preventDefault();
    var nik = $("#search-term").val();
    var link = "<?php echo Yii::app()->controller->createUrl('resultfindbynik');?>&nik="+nik;
    $("#konten").html('<div class="loader" id="loader"><center><i style="color:#F27474;" class="fa fa-spinner fa-pulse fa-5x fa-fw"></i></center></div>');
    $("#konten").load(link, function (response, status, xhr) {
        if (status == "error") {
            $("#konten").html(response);
        }
    });
    event.preventDefault();
});
</script>