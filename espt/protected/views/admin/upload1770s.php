<?php if(Yii::app()->user->hasFlash('tidaksave')):?>
    <script type="text/javascript">
        $(document).ready(function(){
            $("#flashdlg").modal('show');
        });
    </script>
    <div id="flashdlg" class="modal fade" role="dialog">
        <div class="modal-dialog modifupload">
            <div class="modal-content">
              <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Error Report</h4>
              </div>
              <div class="modal-body">
                    <?php echo Yii::app()->user->getFlash('tidaksave'); ?>
              </div>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if(Yii::app()->user->hasFlash('berhasil')):?>
    <script type="text/javascript">
        $(document).ready(function(){
            swal("<?php echo Yii::app()->user->getFlash('berhasil'); ?>","","success");
        });
    </script>
<?php endif; ?>
<div class="row">
    <div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1 style="margin-left:16px;">Upload 1770-S</h1>
                </div>
            </div>
            <div class="col-sm-6 hidden-xs">
                <div class="header-section">
                    <ul class="breadcrumb breadcrumb-top">
                        <li><a href="<?php echo Yii::app()->createUrl('site/index')?>">Home</a></li>
                        <li class="active">Upload 1770-S</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="block full">
            <!-- Main Sidebar Title -->
            <div class="block-title">
                <h2>Form Upload</h2>
            </div>
            <?php $form = $this->beginWidget('CActiveForm', array(
                'id'=>"form-upload",
                'htmlOptions' => array(
                    'enctype' => 'multipart/form-data',
                    'class' => 'form-horizontal'
                )
            )); ?>
                <div class="form-group">
                    <?php echo $form->labelEx($model, 'tahun', array('class'=>'control-label col-md-3')); ?>
                    <div class="col-md-9">
                        <?php echo $form->dropDownList($model, 'tahun', $arraytahun, array('prompt'=>'Pilih Tahun','class'=>'form-control','options'=>array($tahun=>array('selected'=>true))));
                        ?>
                        <?php echo $form->error($model,'tahun'); ?>
                    </div>
                </div>
                <div class="form-group <?php echo (strlen($form->error($model,'fileupload')) > 0 ? 'has-error' : '' )?>">
                    <?php echo $form->labelEx($model, 'fileupload', array('class' => 'control-label col-md-3')); ?>
                    <div class="col-md-9">
                        <?php echo $form->fileField($model, 'fileupload', array('class'=>'form-control input-sm','onchange'=>'ValidateSingleInput(this)')); ?>
                        <span class="help-block">(File berupa excel) >> <?php echo CHtml::link("Sample format upload 1770-S", Yii::app()->request->baseUrl . "/document/download/sample_format_upload_1770_s.xls", array("target"=>"_blank")); ?></span>
                        <?php echo $form->error($model,'fileupload'); ?>
                    </div>
                </div>
                <center>
                <?php echo CHtml::htmlButton('<i class="fa fa-upload"></i> ' . Yii::t('mds', 'Upload File'),array(
                    'class'=>'btn btn-primary btn-sm subm',
                    'type'=>'submit'
                )); ?>
                </center>
            <?php $this->endWidget(); ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <div id="konten"></div>
        </div>
    </div>
</div>
<script>
$('.subm').click(function(e){
    tomb = $(this);
    e.preventDefault();
    swal({
      title: "Apakah anda yakin akan mengupload?",
      // text: "You will not be able to recover this imaginary file!",
      type: "warning",
      showCancelButton: true,
      confirmButtonColor: "#10c469",
      confirmButtonText: "Yes",
      closeOnConfirm: false,
      html: false
    }, function(){
      swal({
        title : "<i class='fa fa-spinner fa-pulse fa-3x fa-fw'></i>",
        html : "<i class='fa fa-spinner fa-pulse fa-3x fa-fw'></i>",
        showCancelButton: false,
        showConfirmButton: false
      });
      $('#form-upload').submit();
    });
});
var _validFileExtensions = [".xls", ".xlsx"];    
function ValidateSingleInput(oInput) {
    if (oInput.files[0].size > 25000000) {
                swal('Maaf maksimal size lampiran adalah 25 mb');
                oInput.value = "";
                return false;
        }
    if (oInput.type == "file") {
        var sFileName = oInput.value;
         if (sFileName.length > 0) {
            var blnValid = false;
            for (var j = 0; j < _validFileExtensions.length; j++) {
                var sCurExtension = _validFileExtensions[j];
                if (sFileName.substr(sFileName.length - sCurExtension.length, sCurExtension.length).toLowerCase() == sCurExtension.toLowerCase()) {
                    blnValid = true;
                    break;
                }
            }
             
            if (!blnValid) {
                swal("Maaf, " + sFileName + " tidak valid, ekstensi file yang diperbolehkan adalah: " + _validFileExtensions.join(", "));
                oInput.value = "";
                return false;
            }
        }
    }
    return true;
}
</script>