<div class="row">
    
    <h2 class="text-warning" style="margin-left:16px;">List SPT NIK <?php echo $nik;?></h2>
    <?php 
      $this->renderPartial('/site/_content',array(
        'data'=>$data,
        'stat_k'=>$stat_k,
        'stat_tk'=>$stat_tk,
        'res2'=>$res2,
        'res3'=>$res3,
        'res4'=>$res4,
        'bukti_potong'=>$bukti_potong,
        'datafinal'=>$datafinal,
        'datanonfinal'=>$datanonfinal,
        'data1770s'=>$data1770s,
        'datassp'=>$datassp,
        'efin'=>$efin,
        'resume'=>$resume
      ));
    ?>
</div>
<div id="dlg" class="modal fade" role="dialog"></div>
<script>
$('[data-toggle="tabs"] a, .enable-tabs a').click(function(e){ e.preventDefault(); $(this).tab('show'); });
$('.btn-print').click(function(e){
    e.preventDefault();
    var link = $(this).attr('href');
    $("#dlg").modal('show');
    $("#dlg").load(link, function (response, status, xhr) {
        if (status == "error") {
            $("#dlg").html(response);
        }
    });
    event.preventDefault();
});
</script>