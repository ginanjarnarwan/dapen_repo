<div class="content-header">
	<div class="row">
		<div class="col-sm-6">
			<div class="header-section">
				<h1>Dashboard</h1>
			</div>
		</div>
		<?php if (Yii::app()->user->roles == "Admin") {	?>
		
			<div class="col-sm-6 hidden-xs">
			<div class="header-section">
				<div class="btn-group pull-right">
					<a href="javascript:void(0)" data-toggle="dropdown" class="btn btn-primary dropdown-toggle" aria-expanded="false">
						Semua
						<span class="caret"></span>
					</a>
					<ul class="dropdown-menu text-left">
						<li class="dropdown-header">
							<strong>Semua</strong>
						</li>
						
							<li>
								<a href="javascript:void(0)">
									<i class="fa fa-pencil pull-right"></i>
									Edit
								</a>
							</li>
							<li>
								<a href="javascript:void(0)">
									<i class="fa fa-cog pull-right"></i>
									Settings
								</a>
							</li>
							<li class="divider"></li>
							<li class="disabled">
								<a href="javascript:void(0)">Disabled</a>
							</li>
						
					</ul>
				</div>
			</div>
		</div>

		<?php } ?>
		
	</div>
</div>


<div class="col-sm-12">
	<div class="block">
		<div class="block-title">
			<h2>Download</h2>
		</div>
		<div class="row">			
			<div class="col-sm-4 col-lg-4">
				<a href="javascript:void(0)" class="widget">	
					<div class="widget-content widget-content-mini text-right clearfix themed-background-flat">
						<div class="widget-icon pull-left">
							<i class="fa fa-2x fa-cloud-download text-light"></i>
						</div>
						
						<h2 class="widget-heading h3 text-light">
							<strong><span data-toggle="counter" data-to="<?php echo count($total_download); ?>"><?php echo count($total_download); ?></span></strong>
						</h2>
						<span class="text-light">Total Download</span>
					</div>
				</a>
			</div>

			<div class="col-sm-2 col-lg-2">
				<a href="javascript:void(0)" class="widget">	
					<div class="widget-content widget-content-mini text-right clearfix themed-background-flat">
						
						
						<h2 class="widget-heading h3 text-light">
							<strong><span data-toggle="counter" data-to="<?php echo count($total_gagal_download); ?>"><?php echo count($total_gagal_download); ?></span></strong>
						</h2>
						<span class="text-light">Gagal Download</span>
					</div>
				</a>
			</div>	

			<div class="col-sm-3 col-lg-3">
				<a href="javascript:void(0)" class="widget">	
					<div class="widget-content widget-content-mini text-right clearfix themed-background-flat">
						<div class="widget-icon pull-left">
							<i class="fa fa-2x fa-cloud-download text-light"></i>
						</div>
						
						<h2 class="widget-heading h3 text-light">
							<strong><span data-toggle="counter" data-to="<?php echo count($total_download_admin); ?>"><?php echo count($total_download_admin); ?></span></strong>
						</h2>
						<span class="text-light">Total Download oleh Admin</span>
					</div>
				</a>
			</div>

			<div class="col-sm-3 col-lg-3">
				<a href="javascript:void(0)" class="widget">	
					<div class="widget-content widget-content-mini text-right clearfix themed-background-flat">
						<div class="widget-icon pull-left">
							<i class="fa fa-2x fa-cloud-download text-light"></i>
						</div>
						
						<h2 class="widget-heading h3 text-light">
							<strong><span data-toggle="counter" data-to="<?php echo count($total_download_user); ?>"><?php echo count($total_download_user); ?></span></strong>
						</h2>
						<span class="text-light">Total Download oleh User</span>
					</div>
				</a>
			</div>

		</div>
	</div>
</div>

<?php if (Yii::app()->user->roles == "Admin"|| Yii::app()->user->roles == "Admin_dapen") {	?>

<div class="col-sm-12">
	<div class="block">
		<div class="block-title">
			<h2>User</h2>
		</div>
		<div class="row">
			<div class="col-sm-4 col-lg-4">
				<a href="javascript:void(0)" class="widget">
					<div class="widget-content widget-content-mini text-right clearfix block full">
						<div class="widget-icon pull-left themed-background-success">
							<i class="gi gi-user text-light-op"></i>
						</div>
						<h2 class="widget-heading h3 text-success">
							<strong><span data-toggle="counter" data-to="<?php echo count($total_user); ?>"><?php echo count($total_user); ?></span></strong>
						</h2>
						<span class="text-muted">Total User</span>
					</div>
				</a>
			</div>

			<div class="col-sm-4 col-lg-4">
				<div class="col-lg-12">
					<a href="javascript:void(0)" class="widget">	
						<div class="widget-content widget-content-mini text-right clearfix themed-background-flat">
							<div class="widget-icon pull-left">
								<i class="fa fa-2x fa-cloud-download text-light"></i>
							</div>
							
							<h2 class="widget-heading h3 text-light">
								<strong><span data-toggle="counter" data-to="<?php echo count($total_user_admin); ?>"><?php echo count($total_user_admin); ?></span></strong>
							</h2>
							<span class="text-light">Total User Admin</span>
						</div>
					</a>
				</div>
				
				<div class="col-sm-6 col-lg-6">
					<a href="javascript:void(0)" class="widget">	
						<div class="widget-content widget-content-mini text-right clearfix themed-background-flat">	
							<h2 class="widget-heading h3 text-light">
								<strong><span data-toggle="counter" data-to="<?php echo count($total_user_admin_register); ?>"><?php echo count($total_user_admin_register); ?></span></strong>
							</h2>
							<span class="text-light">Sudah Registrasi</span>
						</div>
					</a>
				</div>

				<div class="col-sm-6 col-lg-6">
					<a href="javascript:void(0)" class="widget">	
						<div class="widget-content widget-content-mini text-right clearfix themed-background-flat">	
							<h2 class="widget-heading h3 text-light">
								<strong><span data-toggle="counter" data-to="<?php echo count($total_user_admin)-count($total_user_admin_register); ?>"><?php echo count($total_user_admin)-count($total_user_admin_register); ?></span></strong>
							</h2>
							<span class="text-light">Belum Registrasi</span>
						</div>
					</a>
				</div>
				
			</div>

			<div class="col-sm-4 col-lg-4">
				<div class="col-lg-12">
					<a href="javascript:void(0)" class="widget">	
						<div class="widget-content widget-content-mini text-right clearfix themed-background-flat">
							<div class="widget-icon pull-left">
								<i class="fa fa-2x fa-cloud-download text-light"></i>
							</div>
							
							<h2 class="widget-heading h3 text-light">
								<strong><span data-toggle="counter" data-to="<?php echo count($total_user)-count($total_user_admin); ?>"><?php echo count($total_user)-count($total_user_admin); ?></span></strong>
							</h2>
							<span class="text-light">Total User Pensiunan</span>
						</div>
					</a>
				</div>

				<div class="col-sm-6 col-lg-6">
					<a href="javascript:void(0)" class="widget">	
						<div class="widget-content widget-content-mini text-right clearfix themed-background-flat">
							
							
							<h2 class="widget-heading h3 text-light">
								<strong><span data-toggle="counter" data-to="<?php echo count($total_user_register)-count($total_user_admin_register); ?>"><?php echo count($total_user_register)-count($total_user_admin_register); ?></span></strong>
							</h2>
							<span class="text-light">Sudah Registrasi</span>
						</div>
					</a>
				</div>

				<div class="col-sm-6 col-lg-6">
					<a href="javascript:void(0)" class="widget">	
						<div class="widget-content widget-content-mini text-right clearfix themed-background-flat">
							
							
							<h2 class="widget-heading h3 text-light">
								<strong><span data-toggle="counter" data-to="<?php echo count($total_user)-count($total_user_admin)-(count($total_user_register)-count($total_user_admin_register)); ?>"><?php echo count($total_user)-count($total_user_admin)-(count($total_user_register)-count($total_user_admin_register)); ?></span></strong>
							</h2>
							<span class="text-light">Belum Registrasi</span>
						</div>
					</a>
				</div>

			</div>
		</div>
	</div>
</div>					

<?php } ?>








