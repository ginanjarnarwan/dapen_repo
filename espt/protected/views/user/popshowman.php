<style type="text/css">
.modifupload {
    width: 70% !important;
    margin: 30px auto;
}
</style>
<script type="text/javascript">
</script>
<div class="modal-dialog modifupload">
	<div class="modal-content">
	  <div class="modal-header">
	    <button type="button" class="close" data-dismiss="modal">&times;</button>
	    <h4 class="modal-title">Data User</h4>
	  </div>
	  <div class="modal-body">
	  	<div class="loader" id="loader"><center><i style="color:#F27474;" class="fa fa-spinner fa-pulse fa-5x fa-fw"></i></center></div>
	    <iframe id="upload-frame" width="100%" height="250px" frameBorder="0" onload="setIframeHeight(this.id);if($('table',$('#upload-frame').contents()).length != 0) {$('#loader').hide();}" src="<?php echo Yii::app()->controller->createAbsoluteUrl('showman',array(
	    	'temp'=>$temp
		)); ?>"></iframe>
	  </div>
	</div>
</div>