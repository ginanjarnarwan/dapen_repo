<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>
<div id="dlg" class="modal fade" role="dialog"></div>
<div class="box-body">

    <?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'user-form',
	'enableAjaxValidation'=>false,
        'htmlOptions'=>array('class'=>'form-horizontal')
)); ?>

    <p class="note">Fields with <span class="required">*</span> are required.</p>

    <?php echo $form->errorSummary($model); ?>

        <div class="form-group">
            <?php echo $form->labelEx($model,'nik', array('class'=>'col-sm-2 control-label')); ?>
            <div class="col-md-10">
                <div class="input-group">
                    <?php echo $form->textField($model, 'nik', array('class' => 'form-control','readonly'=>'readonly')); ?>
                    <span class="input-group-btn">
                        <button class="btn btn-secondary btn-primary" type="button" onclick="showmodal(this)" temp="User_nik"><i class="fa fa-search"></i></button>
                    </span>
                </div>
                <?php echo $form->error($model,'nik', array('class'=>'alert alert-danger')); ?>
            </div>
        </div>
        <div class="form-group">
            <label class="col-sm-2 control-label">Nama</label>
            <div class="col-md-10">
                <input type="text" id="nama" class="form-control" readonly="readonly" value="<?php echo isset($namkar) ? $namkar : '';?>">
            </div>
        </div>
        <div class="form-group">
            <?php echo $form->labelEx($model,'password', array('class'=>'col-sm-2 control-label')); ?>
            <div class="col-md-10">
                <?php echo $form->textField($model, 'password', array('class' => 'form-control','value'=>'')); ?>
                <?php echo $form->error($model,'password', array('class'=>'alert alert-danger')); ?>
            </div>
        </div>

        </div>

<div class="box-footer">
    <?php echo CHtml::link('Cancel', Yii::app()->controller->createUrl('index'), array('class' => 'btn btn-sm btn-default')); ?> 
    <?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save', array('class' => 'btn btn-sm btn-info pull-right')); ?> 
</div>

<?php $this->endWidget(); ?>
<script type="text/javascript">
showmodal = function(e){
        var temp = $(e).attr("temp");
        var link = '<?php echo Yii::app()->controller->createUrl("popshowman");?>&temp='+temp;
        $("#dlg").modal('show');
        $("#dlg").load(link, function (response, status, xhr) {
            if (status == "error") {
                $("#dlg").html(response);
            }
        });
        event.preventDefault();
    }
</script>