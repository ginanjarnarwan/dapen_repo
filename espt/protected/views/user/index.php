<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	'Manage',
);
?>
<div class="row">
	<div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1 style="margin-left:16px;">Manage Password</h1>
                </div>
            </div>
            <div class="col-sm-6 hidden-xs">
                <div class="header-section">
                    <ul class="breadcrumb breadcrumb-top">
                        <li><a href="<?php echo Yii::app()->createUrl('site/index')?>">Home</a></li>
                        <li class="active">Manage Password</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="block full">
            <!-- Main Sidebar Title -->
            <div class="block-title">
                <h2></h2>
            </div>
			<div class="pull-right">
				<?php echo CHtml::link("Create", Yii::app()->controller->createUrl("create"), array("class" => "btn btn-sm btn-primary")); ?> 
			</div>
			<br>
			<div class="box-body">
				<?php $this->widget('zii.widgets.grid.CGridView', array(
				'id'=>'user-grid',
				'dataProvider'=>$model->search(),
				'filter'=>$model,
				'enableSorting' => false,
				'itemsCssClass' => 'table table-bordered table-striped',
				'pagerCssClass'=>'paging',
				'pager'=>array(
					'class'=>'CLinkPager',
					'header'=>'',
					'selectedPageCssClass'=>'active',
					'htmlOptions'=>array(
						'class'=>'pagination pagination-sm'
					)
				),
				'columns'=>array(
					array(
                        'type' => 'raw',
                        'header' => 'No',
                        'htmlOptions' => array(
                            'style' => 'text-align:right',
                        ),
                        'headerHtmlOptions' => array(
                            'style' => 'text-align:right'
                        ),
                        'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
                    ),
					'nik',
					'V_NAMA_KARYAWAN',
					array(
						'name'=>'password',
						'filter'=>false
					),
					array(
					'class'=>'CButtonColumn',
					'template'=>'{update}{delete}',
					'buttons' => array(
					'view' => array(
					'label' => '<i class="fa fa-external-link"></i>',
					'imageUrl' => FALSE,
					'options' => array('class' => 'btn btn-primary btn-condensed btn-sm')
					),
					'update' => array(
					'label' => '<i class="fa fa-pencil"></i>',
					'imageUrl' => FALSE,
					'options' => array('class' => 'btn btn-warning btn-condensed btn-sm')
					),
					'delete' => array(
					'label' => '<i class="fa fa-trash"></i>',
					'imageUrl' => FALSE,
					'options' => array('class' => 'btn btn-danger btn-condensed btn-sm')
					),
					),
					'htmlOptions' => array('style' => 'width:130px;text-align:center;')
					),
				),
				)); ?>
			</div>
		</div>
	</div>
</div>
