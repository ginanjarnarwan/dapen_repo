<?php
/* @var $this UserController */
/* @var $model User */
/* @var $form CActiveForm */
?>
<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
	'htmlOptions'=>array('class'=>'form-horizontal')
)); ?>

<div class="form-group">
	<?php echo $form->labelEx($model,'id', array('class'=>'col-sm-2 control-label')); ?>
	<div class="col-sm-10">
		<?php echo $form->textField($model,'id',array('class' => 'form-control')); ?>
	</div>
</div>
<div class="form-group">
	<?php echo $form->labelEx($model,'nik', array('class'=>'col-sm-2 control-label')); ?>
	<div class="col-sm-10">
		<?php echo $form->textField($model,'nik',array('class' => 'form-control', 'size'=>9,'maxlength'=>9)); ?>
	</div>
</div>
	<div class="row">
		<div class="col-sm-12">
			<?php echo CHtml::submitButton('Search', array(
				'class'=>'btn btn-sm btn-primary'
			)); ?>
		</div>
	</div>
<?php $this->endWidget(); ?>
