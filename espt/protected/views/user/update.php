<?php
/* @var $this UserController */
/* @var $model User */

$this->breadcrumbs=array(
	'Users'=>array('index'),
	'Create',
);

$this->menu=array(
array('label'=>'List User', 'url'=>array('index')),
array('label'=>'Manage User', 'url'=>array('admin')),
);
?>
<div class="row">
	<div class="content-header">
        <div class="row">
            <div class="col-sm-6">
                <div class="header-section">
                    <h1 style="margin-left:16px;">Update Password User</h1>
                </div>
            </div>
            <div class="col-sm-6 hidden-xs">
                <div class="header-section">
                    <ul class="breadcrumb breadcrumb-top">
                        <li><a href="<?php echo Yii::app()->createUrl('site/index')?>">Manage Password</a></li>
                        <li class="active">Update Password User</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="block full">
            <!-- Main Sidebar Title -->
            <div class="block-title">
                <h2></h2>
            </div>
            <?php $this->renderPartial('_form', array('model'=>$model,'namkar'=>$namkar)); ?>
        </div>
    </div>
</div>
