<!-- <div class="col-md-12"> -->
    <!-- <div class="widget p-lg"> -->
        <!-- <div class="col-sm-12">
            <div class="widget bg-info">
                <div class="widget-body">
                    <div class="m-b-l"> -->
                    <?php $form=$this->beginWidget('CActiveForm', array(
                            'id'=>'form-search',
                            'enableAjaxValidation'=>false,
                                'htmlOptions'=>array('class'=>'form-inline','enctype' => 'multipart/form-data',)
                        )); ?>
                            <center>
                            <div class="form-group">
                                <label for="exampleInputName2"><b>Filter :</b></label>
                            </div>
                            <br>
                            <div class="form-group">
                                <label>NIK</label>
                                <input type="text" name="Karyawan[nik]" class="form-control" value="<?php echo $nik_search; ?>">
                            </div>
                            <div class="form-group">
                                <label>Nama</label>
                                <input type="text" name="Karyawan[nama]" class="form-control" value="<?php echo $nama_search; ?>">
                            </div>
                            <button type="submit" class="btn btn-default">Filter</button>
                            </center>
                        <?php $this->endWidget(); ?>
                    <!-- </div>
                </div>
            </div>
        </div> -->
        <div class="table-responsive">
            <?php
            $this->widget('zii.widgets.grid.CGridView', array(
                'id' => 'a-grid-id',
                'dataProvider' => $data,
                // 'filter'=>$model,
                'itemsCssClass' => 'table table-bordered table-stripped table-hover',
                'columns' => array(
                    array(
                        'type' => 'raw',
                        'header' => 'No',
                        'htmlOptions' => array(
                            'style' => 'text-align:right',
                        ),
                        'headerHtmlOptions' => array(
                            'style' => 'text-align:right'
                        ),
                        'value' => '$this->grid->dataProvider->pagination->currentPage*$this->grid->dataProvider->pagination->pageSize + $row+1',
                    ),
                    'N_NIK',
                    'V_NAMA_KARYAWAN',
                    array(
                        'header'=>'Pilih',
                        'type'=>'raw',
                        'value'=>'CHtml::tag("a", array("href"=>"javascript: void(0);", "nik"=>$data[\'N_NIK\'],"nama"=>$data[\'V_NAMA_KARYAWAN\'], "class"=>"pil", "onclick"=>"pilih(this);"),"<i class=\'fa fa-check\'></i>")',
                    ),
                ),
            ));
            ?>
        </div>
    <!-- </div> -->
<!-- </div> -->
<script>
function pilih(e)
{
    var nik = $(e).attr('nik');
    var nama = $(e).attr('nama');
    $('#<?php echo $temp;?>',parent.document).val(nik);
    $('#nama',parent.document).val(nama);
    $('.modal',parent.document).find('.close').trigger('click');
}
</script>