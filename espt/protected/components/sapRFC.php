<?php
class sapRFC {

	function sapRFC($servConf, $sapUser, $sapPass) {
			 $this->sapConn = array (
//	            "SYSNR"=>$servConf["sysnr"],           // system number
      	        "CLIENT"=>$servConf["client"],         // client
                "USER"=>$sapUser,          // user
                "PASSWD"=>$sapPass,        // password
//                "CODEPAGE"=>$servConf["codePage"] //,
//		"TRACE"=>$servConf["trace"] //untuk tracing
				);   // codepage
	if (isset($servConf["sysnr"]))
		$this->sapConn["SYSNR"] = $servConf["sysnr"];
	if (isset($servConf["codePage"]))
		$this->sapConn['CODEPAGE'] = $servConf["codePage"];	
	if (isset($servConf["mshost"])) 
          $this->sapConn['MSHOST'] = $servConf["mshost"];
	else $this->sapConn['ASHOST'] = $servConf["sapServer"];
        if (isset($servConf["r3name"]))
          $this->sapConn['R3NAME'] = $servConf["r3name"];	
        if (isset($servConf['group']))
          $this->sapConn['GROUP'] = $servConf["group"];
	return $this->sapConn;	
	}
		
	function connect() {
		$rfc = @saprfc_open ($this->sapConn);
		if (!$rfc){
			Yii::app()->controller->redirect('index.php?r=site/saperror'); 
		}
		//return $rfc;
	   return $this->rfc = @saprfc_open ($this->sapConn) or die ("<font face=tahoma size=3><b><p align=center>...Kami Mohon maaf atas ketidaknyamananya... <br> saat ini Menu yang anda akses pada Sistem Presensi tidak bisa dilayani <br> dikarenakan GAGAL Koneksi dengan SAP <br><br> Terima Kasih");
	}
    function connectHeadless() {
        $this->rfc = @saprfc_open ($this->sapConn);
        if (!$this->rfc) $this->err = saprfc_error();
        return $this->rfc;
    }
	
	function functionDiscover($functionName) {
		$rfcDisc = @saprfc_function_discover($this->rfc, $functionName);
		if (!$rfcDisc){
			Yii::app()->controller->redirect('index.php?r=site/saperror');
		}
		$this->fce = @saprfc_function_discover($this->rfc, $functionName) or die ("fungsi $functionName tidak ditemukan");
	}
	function printDefinition() {
		saprfc_function_debug_info($this->fce);
	}
	function importParameter($importParamName, $importParamValue) {
		for ($i=0;$i<count($importParamName);$i++) {
			saprfc_import ($this->fce,$importParamName[$i],$importParamValue[$i]);
			
		}
	}
		
	function setInitTable($initTableName) {
		saprfc_table_init ($this->fce,$initTableName);
	}
	
	function executeSAP() {
		$this->rfc_rc = @saprfc_call_and_receive ($this->fce);
		if ($this->rfc_rc != SAPRFC_OK){
      	   if ($this->rfc == SAPRFC_EXCEPTION )
   		   		echo ("Exception raised: ".saprfc_exception($this->fce));
		   else
        	    echo ("Call error: ".saprfc_error($this->fce));
		}
		return $this->rfc_rc;
	}
	
	function fetch_rows($initTableName) {
	   $rows = saprfc_table_rows ($this->fce,$initTableName);
	   for ($i=1; $i<=$rows; $i++)
      	   $_dataRows[$i] = saprfc_table_read ($this->fce,$initTableName,$i);
	   return $_dataRows;	
	}
	
	function free() {
		saprfc_function_free($this->fce);
	}
	
	function close() {
		saprfc_close($this->rfc);
	}
	
	function insert($initTableName,$importParamValue){
		return saprfc_table_insert ($this->fce, $initTableName, $importParamValue, 1);
	}
	
	/*	Fungsi Export Asli
	function export($initTableName){
		return saprfc_export ($this->fce,$initTableName);
	}
	*/
	
	function export($exportParamName){
	   for ($i=1; $i<=count($exportParamName); $i++)
		$field = saprfc_export($this->fce,"E_CNAME");	
		return $field;
   	}
	
	
	function showAttribute(){
		return saprfc_attribute ($this->rfc);
	}
}
?>
