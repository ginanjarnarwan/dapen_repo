<?php

class COciConnection extends CDbConnection {

    protected function initConnection($pdo) {
        parent::initConnection($pdo);
        $stmt = $pdo->prepare("alter session set NLS_DATE_FORMAT='yyyy-mm-dd HH24:MI:SS'");
        $stmt->execute();
    }

    public function quoteValue($str) {
        return "'" . $str . "'";
    }

}
