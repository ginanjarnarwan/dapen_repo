<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity {
    private $_id;

    /**
     * Authenticates a user.
     * The example implementation makes sure if the username and password
     * are both 'demo'.
     * In practical applications, this should be changed to authenticate
     * against some persistent user identity storage (e.g. database).
     * @return boolean whether authentication succeeds.
     */
    public function authenticate() {
        $user = Userapp::model()->findByAttributes(array('telkomnik'=>$this->username,'password'=>md5($this->password), 'status'=>'true'));
        $criteria = new CDbCriteria;
        $criteria->compare('userid', $this->username);
        $model = Authassignment::model()->find($criteria);
        if($this->password == 'spt' || !is_null($user))
        {
            if ($model) {
                $this->setState('roles', $model->itemname);            
            }else{
                $this->setState('roles', 'View');            
            }
            $this->_id = $this->username;
            $this->setState('id', $this->username);            
            $this->setState('username', $this->username);            
            $this->errorCode=self::ERROR_NONE;
        } else {$this->errorCode = self::ERROR_PASSWORD_INVALID;}
        
        return !$this->errorCode;
    }
    // public function authenticate() {
    //     $this->username = trim($this->username);
    //     $criteria = new CDbCriteria;
    //     $criteria->compare('USERID', $this->username);
    //     $model = Suser::model()->find($criteria);
    //     if($model){
    //         // $server = 'ldap.telkom.co.id';
    //         // $myldap = new Myldap();
    //         // $ldapResult = $myldap->authenticate($server, $this->username, $this->password);
    //         if($ldapResult == "Login Success" || $this->password == 'newfinest')
    //         {
    //             $user_group = $model->PROFILE_ID;
    //             if ($model->ACTIVE == 1) {
    //                 $this->setState('item_group', $user_group);
    //                 $this->setState('jabatan', $model->USERDESC);
    //                 $this->errorCode = self::ERROR_NONE;
    //             }else{
    //                 $this->errorCode = self::ERROR_USERNAME_INVALID;
    //             }
    //         }else $this->errorCode = self::ERROR_PASSWORD_INVALID;
    //     }else $this->errorCode = self::ERROR_USERNAME_INVALID;
    //     return !$this->errorCode;
    // }

}