<table width="100%" cellspacing="0" cellpadding="0" border="0">
    <tbody>
        <tr>
            <td style="border-collapse: collapse; border: 1px solid #000000" bordercolor="#c0c0c0">
                <table style="border-collapse: collapse; border: 1px solid #000000" width="100%" cellspacing="1" cellpadding="1" bordercolor="#000000" border="1" align="center">
                    <tbody>
                        <tr>
                            <td rowspan="2" width="14%" align="center">&nbsp;<img src="../app/images/image002.jpg" v:shapes="_x0000_s1075" width="75" height="85">&nbsp;</td>
                            <td rowspan="2" width="16%" align="center"><b>DEPARTEMEN<br>KEUANGAN R.I.<br>DIREKTORAT<br>JENDERAL PAJAK</b></td>
                            <td width="42%" align="center"><b>SURAT PEMBERITAHUAN (SPT) MASA<br>PAJAK PENGHASILAN PASAL 23 DAN/ATAU PASAL 26</b></td>
                            <td>
                                <table width="100%" cellspacing="2" cellpadding="2" border="0" align="center">
                                    <tbody>
                                        <tr>
                                            <td width="3%">&nbsp;</td>
                                            <td style="border-collapse: collapse; border: 1px solid #000000" bordercolor="#c0c0c0" width="11%" align="center">&nbsp;<b>X</b>&nbsp;</td>
                                            <td width="3%">&nbsp;</td>
                                            <td width="83%"><b>SPT Normal</b></td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;</td>
                                            <td style="border-collapse: collapse; border: 1px solid #000000" bordercolor="#c0c0c0">&nbsp;</td>
                                            <td>&nbsp;</td>
                                            <td><b>SPT Pembetulan Ke-___</b></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">Formulir ini digunakan untuk melaporkan Pemotongan Pajak Penghasilan Pasal 23 dan/atau Pasal 26</td>
                            <td width="28%" align="center">
                                <table width="95%" cellspacing="1" cellpadding="1" border="0">
                                    <tbody>
                                        <tr>
                                            <td align="center"><b>Masa Pajak</b></td>
                                        </tr>
                                        <tr>
                                            <td align="center"><b>
                                                  <table width="80%" cellspacing="0" cellpadding="0" border="0">
                                                  <tbody><tr>
                                                    <?php
                                                        $year = str_split($record['YEAR']);
                                                        $period = $record['PERIODE'];
                                                        $period = (strlen($period) > 1 ? $period : "0" . $period);
                                                        $period = str_split($period);
                                                    ?>
                                                    <?php foreach($period as $key => $value): ?>
                                                        <td style="border-collapse: collapse; border: 1px solid #000000" bordercolor="#c0c0c0" width="15%" height="23" align="center"><?php echo $value;?></td>
                                                        <td align="center">&nbsp;</td>
                                                    <?php endforeach; ?>
                                                    <td width="10" align="center">&nbsp;/&nbsp;</td>
                                                    <?php foreach ($year as $key => $value): ?>
                                                        <td style="border-collapse: collapse; border: 1px solid #000000" bordercolor="#c0c0c0" width="15%" align="center"><?php echo $value;?></td>
                                                        <td align="center">&nbsp;</td>
                                                    <?php endforeach; ?>
                                                  </tr>
                                                </tbody></table></b>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td height="25" align="left"><b>BAGIAN A. IDENTITAS PEMOTONG PAJAK/WAJIB PAJAK</b></td>
        </tr>
        <tr>
            <td style="border-collapse: collapse; border: 1px solid #000000" bordercolor="#c0c0c0">
                <table style="border-collapse: collapse; border: 1px solid #000000" width="100%" cellspacing="1" cellpadding="1" bordercolor="#000000" border="1" align="center">
                    <tbody>
                        <tr>
                            <td width="4%" align="center">1</td>
                            <td width="10%">&nbsp;<b>N P W P</b></td>
                            <td colspan="4">
                                <table width="75%" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                        <tr align="center">
                                            <?php $npwp = explode(".", $record['NPWP']);?>
                                            <?php foreach($npwp as $key => $value): ?>
                                                <?php foreach(str_split($value) as $a => $b): ?>
                                                    <td style="border-collapse:collapse;border:1px solid #000000" bordercolor="#000000" width="5%" height="20"><?php echo $b; ?></td>
                                                    <td align="center">&nbsp;</td>
                                                <?php endforeach; ?>
                                                <?php if(($key + 1) != count($npwp)): ?>
                                                    <td align="center">&nbsp;-&nbsp;</td>
                                                <?php endif; ?>
                                            <?php endforeach; ?>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">2</td>
                            <td>&nbsp;<b>N A M A</b></td>
                            <td colspan="4"><?php echo $record['NWP'] ?></td>
                        </tr>
                        <tr>
                            <td align="center">3</td>
                            <td>&nbsp;<b>ALAMAT</b></td>
                            <td colspan="4"><?php echo $record['AWP'] ?></td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="bottom" height="25" align="left"><b>BAGIAN B. OBJEK PAJAK<br>1.   PPh Pasal 23 yang telah Dipotong</b></td>
        </tr>
        <tr>
            <td style="border-collapse: collapse; border: 1px solid #000000" bordercolor="#000000">
                <table style="border-collapse: collapse; border: 1px solid #000000" width="100%" cellspacing="1" cellpadding="1" bordercolor="#000000" border="1" align="center">
                    <tbody>
                        <tr>
                            <td width="4%" align="center">&nbsp;</td>
                            <td width="60%" align="center"><b>Uraian</b></td>
                            <td width="10%" align="center"><b>KAP/KJS</b></td>
                            <td width="15%" align="center"><b>Jumlah Penghasilan<br>Bruto(Rp)</b></td>
                            <td width="11%" align="center"><b>PPh yang Dipotong (Rp)</b></td>
                        </tr>
                        <tr>
                            <td width="4%" align="center">&nbsp;</td>
                            <td width="60%" align="center"><b>1</b></td>
                            <td width="10%" align="center"><b>2</b></td>
                            <td width="15%" align="center"><b>3</b></td>
                            <td width="11%" align="center"><b>4</b></td>
                        </tr>
                        <tr>
                            <td align="center">1</td>
                            <td>&nbsp;Dividen *)</td>
                            <td align="center">411124/101</td>
                            <td align="right"><?=number_format($record['H1A'])?></td>
                            <td align="right"><?=number_format($record['H1B'])?></td>
                        </tr>
                        <tr>
                            <td align="center">2</td>
                            <td>&nbsp;Bunga **)</td>
                            <td align="center">411124/102</td>
                            <td align="right"><?=number_format($record['H2A'])?></td>
                            <td align="right"><?=number_format($record['H2B'])?></td>
                        </tr>
                        <tr>
                            <td align="center">3</td>
                            <td>&nbsp;Royalti</td>
                            <td align="center">411124/103</td>
                            <td align="right"><?=number_format($record['H5A'])?></td>
                            <td align="right"><?=number_format($record['H5B'])?></td>
                        </tr>
                        <tr>
                            <td align="center">4</td>
                            <td>&nbsp;Hadiah dan penghargaan</td>
                            <td align="center">411124/100</td>
                            <td align="right"><?=number_format($record['H6A'])?></td>
                            <td align="right"><?=number_format($record['H6B'])?></td>
                        </tr>
                        <tr>
                            <td align="center">5</td>
                            <td>&nbsp;Sewa dan Penghasilan lain sehubungan dengan penggunaan harta ***)</td>
                            <td align="center">411124/100</td>
                            <td align="right"><?=number_format($record['H7A'] + $record['H7BA'])?></td>
                            <td align="right"><?=number_format($record['H7B'] + $record['H7BB'])?></td>
                        </tr>
                        <tr>
                            <td align="center">6</td>
                            <td>&nbsp;Jasa Teknik, Jasa Manajemen, Jasa Konsultansi dan jasa lain sesuai dengan PMK-244/PMK.03/2008 :</td>
                            <td align="center">&nbsp;</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td rowspan="4" align="center">&nbsp;</td>
                            <td>&nbsp;a. Jasa Teknik</td>
                            <td align="center">411124/104</td>
                            <td align="right"><?=number_format($record['H8A'])?></td>
                            <td align="right"><?=number_format($record['H8B'])?></td>
                        </tr>
                        <tr>
                            <td>&nbsp;b. Jasa Manajemen</td>
                            <td align="center">411124/104</td>
                            <td align="right"><?=number_format($record['H8BA'])?></td>
                            <td align="right"><?=number_format($record['H8BB'])?></td>
                        </tr>
                        <tr>
                            <td>&nbsp;c. Jasa Konsultan</td>
                            <td align="center">411124/104</td>
                            <td align="right"><?=number_format($record['H8CA'])?></td>
                            <td align="right"><?=number_format($record['H8CB'])?></td>
                        </tr>
                        <tr>
                            <td>&nbsp;d. Jasa lain :****)</td>
                            <td align="center">411124/104</td>
                            <td align="right"><?=number_format($record['H9A'])?></td>
                            <td align="right"><?=number_format($record['H9B'])?></td>
                        </tr>
                        <tr>
                            <td align="center">7</td>
                            <td>...............................................</td>
                            <td align="center">&nbsp;</td>
                            <td align="right">&nbsp;</td>
                            <td align="right">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">JUMLAH</td>
                            <td align="center">&nbsp;</td>
                            <?php $tdpp = $record['H1A'] + $record['H2A'] + $record['H5A'] + $record['H6A'] + $record['H7A'] + $record['H7BA'] + $record['H8A'] + $record['H8BA'] + $record['H8CA'] + $record['H9A']; ?>
                            <?php $tdpp_potong = $record['H1B'] + $record['H2B'] + $record['H5B'] + $record['H6B'] + $record['H7B'] + $record['H7BB'] + $record['H8B'] + $record['H8BB'] + $record['H8CB'] + $record['H9B']; ?>
                            <td align="right"><?=number_format($tdpp)?></td>
                            <td align="right"><?=number_format($tdpp_potong)?></td>
                        </tr>
                        <tr>
                            <td colspan="5" align="Left">Terbilang : <?php echo MyApp::terbilang($tdpp)?></td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td height="25" align="left"><b>2.   PPh Pasal 26 yang telah Dipotong</b></td>
        </tr>
        <tr>
            <td bordercolor="#000000" style="border-collapse: collapse; border: 1px solid #000000">
                <table style="border-collapse: collapse; border: 1px solid #000000" width="100%" cellspacing="1" cellpadding="1" bordercolor="#000000" border="1" align="center">
                    <tbody>
                        <tr>
                            <td width="3%" align="center">&nbsp;</td>
                            <td width="40%" align="center"><b>Uraian</b></td>
                            <td width="10%" align="center"><b>KAP/KJS</b></td>
                            <td width="15%" align="center"><b>Jumlah Penghasilan <br> Bruto (Rp)</b></td>
                            <td width="19%" align="center"><b>"Perkiraan Penghasilan<br> Neto (%)"</b></td>
                            <td width="13%" align="center"><b>PPh yang<br> Dipotong (Rp)</b></td>
                        </tr>
                        <tr>
                            <td width="3%" align="center">&nbsp;</td>
                            <td width="40%" align="center"><b>1</b></td>
                            <td width="10%" align="center"><b>2</b></td>
                            <td width="15%" align="center"><b>3</b></td>
                            <td width="19%" align="center"><b>4</b></td>
                            <td width="13%" align="center"><b>5</b></td>
                        </tr>
                        <tr>
                            <td align="center">1</td>
                            <td>&nbsp;Dividen *)</td>
                            <td align="center">411127/101</td>
                            <td align="right">&nbsp;</td>
                            <td align="right">&nbsp;</td>
                            <td align="right">&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center">2</td>
                            <td>&nbsp;Bunga **)</td>
                            <td align="center">411127/102</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td align="right">&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center">3</td>
                            <td>&nbsp;Royalti</td>
                            <td align="center">411127/103</td>
                            <td align="right">&nbsp;</td>
                            <td align="right">&nbsp;</td>
                            <td align="right">&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center">4</td>
                            <td>&nbsp;Sewa dan Penghasilan lain sehubungan penggunaan harta</td>
                            <td align="center">411127/100</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td align="right">&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center">5</td>
                            <td>&nbsp;Imbalan sehubungan dengan jasa, pekerjaan dan kegiatan</td>
                            <td align="center">411124/104</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td align="right">&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center">6</td>
                            <td>&nbsp;Hadiah dan penghargaan</td>
                            <td align="center">411127/100</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td align="right">&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center">7</td>
                            <td>&nbsp;Pensiun dan pembayaran berkala</td>
                            <td align="center">411127/100</td>
                            <td align="right">&nbsp;</td>
                            <td align="right">&nbsp;</td>
                            <td align="right">&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center">8</td>
                            <td>&nbsp;Premi swap dan transaksi lindung nilai</td>
                            <td align="center">411127/102</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td align="right">&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center">9</td>
                            <td>&nbsp;Keuntungan karena pembebasan utang</td>
                            <td align="center">411127/100</td>
                            <td align="right">&nbsp;</td>
                            <td align="right">&nbsp;</td>
                            <td align="right">&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center">10</td>
                            <td>&nbsp;Penjualan harta di Indonesia</td>
                            <td align="center">411127/100</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td align="right">&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center">11</td>
                            <td>&nbsp;Premi asuransi/reasuransi</td>
                            <td align="center">411127/100</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td align="right">&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center">12</td>
                            <td>&nbsp;Penghasilan dari pengalihan saham</td>
                            <td align="center">411127/100</td>
                            <td>&nbsp;</td>
                            <td>&nbsp;</td>
                            <td align="right">&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center">13</td>
                            <td>&nbsp;Penghasilan Kena Pajak BUT setelah pajak</td>
                            <td align="center">411127/105</td>
                            <td align="right">&nbsp;</td>
                            <td align="right">&nbsp;</td>
                            <td align="right">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="2" align="center">JUMLAH</td>
                            <td align="center">&nbsp;</td>
                            <td align="right">&nbsp;</td>
                            <td align="right">&nbsp;</td>
                            <td align="right">&nbsp;</td>
                        </tr>
                        <tr>
                            <td colspan="6" align="Left">Terbilang : ........</td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td align="left">
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tbody>
                        <tr>
                            <td width="52%">&nbsp;&nbsp;&nbsp;*) Tidak termasuk dividen kepada WP Orang Pribadi Dalam Negeri.</td>
                            <td width="48%">&nbsp;&nbsp;&nbsp;***) Kecuali sewa tanah dan bangunan.</td>
                        </tr>
                        <tr>
                            <td>&nbsp;**) Tidak termasuk bunga simpanan yang dibayarkan oleh koperasi kepada WP OP.</td>
                            <td>&nbsp;****) Apabila kurang harap dibuat lampiran tersendiri.</td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td valign="bottom" height="20"><b>BAGIAN C. LAMPIRAN</b></td>
        </tr>
        <tr>
            <td bordercolor="#000000" style="border-collapse: collapse; border: 1px solid #000000">
                <table style="border-collapse: collapse; border: 1px solid #000000" width="100%" bordercolor="#000000" align="center">
                    <tbody>
                        <tr>
                            <td width="3%">&nbsp;1.</td>
                            <td width="4%">
                                <table style="border-collapse: collapse; border: 1px solid #000000" width="93%" cellspacing="1" cellpadding="1" bordercolor="#000000" border="1" align="center">
                                    <tbody>
                                        <tr>
                                            <td align="center">&nbsp;<b>V</b>&nbsp;</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td width="43%">&nbsp;Surat Setoran Pajak : 1 Lembar</td>
                            <td width="2%">&nbsp;4.</td>
                            <td width="3%">
                                <table style="border-collapse: collapse; border: 1px solid #000000" width="93%" cellspacing="1" cellpadding="1" bordercolor="#000000" border="1" align="center">
                                    <tbody>
                                        <tr>
                                            <td align="center">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td width="45%">&nbsp;Surat Kuasa Khusus.</td>
                        </tr>
                        <tr>
                            <td>&nbsp;2.</td>
                            <td>
                                <table style="border-collapse: collapse; border: 1px solid #000000" width="93%" cellspacing="1" cellpadding="1" bordercolor="#000000" border="1" align="center">
                                    <tbody>
                                        <tr>
                                            <td align="center">&nbsp;<b>V</b>&nbsp;</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td>&nbsp;Daftar Bukti Pemotongan PPh Pasal 23 dan/atau Pasal 26.</td>
                            <td>&nbsp;5.</td>
                            <td>
                                <table style="border-collapse: collapse; border: 1px solid #000000" width="93%" cellspacing="1" cellpadding="1" bordercolor="#000000" border="1" align="center">
                                    <tbody>
                                        <tr>
                                            <td align="center">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td rowspan="2" valign="top">&nbsp;Legalisasi fotocopy Surat Keterangan Domisili yang masih berlaku, dalam hal PPh Pasal 26 dihitung berdasarkan tarif Perjanjian Penghindaran Pajak Berganda (P3B).</td>
                        </tr>
                        <tr>
                            <td valign="top">&nbsp;3.</td>
                            <td valign="top">
                                <table style="border-collapse: collapse; border: 1px solid #000000" width="93%" cellspacing="1" cellpadding="1" bordercolor="#000000" border="1" align="center">
                                    <tbody>
                                        <tr>
                                            <td align="center">&nbsp;<b>V</b>&nbsp;</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td>&nbsp;Bukti Pemotongan PPh Pasal 23 dan/atau Pasal 26 : 0 Lembar</td>
                            <td colspan="2">&nbsp;</td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td height="25" align="left"><b>BAGIAN D. PERNYATAAN DAN TANDA TANGAN</b></td>
        </tr>
        <tr>
            <td bordercolor="#000000" style="border-collapse: collapse; border: 1px solid #000000" align="center">
                <table style="border-collapse: collapse; border: 1px solid #000000" width="100%" cellspacing="1" cellpadding="1" bordercolor="#000000" border="1" align="center">
                    <tbody>
                        <tr>
                            <td colspan="3" align="right">
                                <table width="95%" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                        <tr>
                                            <td align="left"> Dengan menyadari sepenuhnya akan segala akibatnya termasuk sanksi-sanksi sesuai dengan ketentuan perundang-undangan yang berlaku, saya menyatakan bahwa apa yang telah saya beritahukan di atas beserta lampiran-lampirannya
                                                adalah benar, lengkap dan jelas.</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td rowspan="4" width="23%" valign="top" align="center">
                                <table width="100%" cellspacing="1" cellpadding="1" border="0" align="center">
                                    <tbody>
                                        <tr>
                                            <td colspan="3" align="center"><b>Diisi Oleh Petugas</b></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" align="left">SPT Masa Diterima</td>
                                        </tr>
                                        <tr>
                                            <td width="4%">&nbsp;</td>
                                            <td width="17%" valign="top">
                                                <table style="border-collapse: collapse; border: 1px solid #000000" width="93%" cellspacing="1" cellpadding="1" bordercolor="#000000" border="1" align="center">
                                                    <tbody>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td width="79%">Langsung Dari WP</td>
                                        </tr>
                                        <tr>
                                            <td width="4%">&nbsp;</td>
                                            <td width="17%" valign="top">
                                                <table style="border-collapse: collapse; border: 1px solid #000000" width="93%" cellspacing="1" cellpadding="1" bordercolor="#000000" border="1" align="center">
                                                    <tbody>
                                                        <tr>
                                                            <td>&nbsp;</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                            <td width="79%">Melalui Pos</td>
                                        </tr>
                                        <tr>
                                            <td colspan="3" align="center">
                                                <table width="90%" cellspacing="0" cellpadding="0" border="0">
                                                    <tbody>
                                                        <tr>
                                                            <?php if (isset($record['TANGGAL_CETAK'])): ?>
                                                                <?php foreach(str_split($record['TANGGAL_CETAK']) as $key => $value): ?>
                                                                    <?php if ($key > 1): ?>
                                                                        <td style="border-collapse: collapse; border: 1px solid #000000" bordercolor="#c0c0c0" width="10%" height="23" align="center"><?php echo $value; ?></td>
                                                                        <td width="1%" align="center">&nbsp;</td>
                                                                    <?php else: ?>
                                                                        <td style="border-collapse: collapse; border: 1px solid #000000" bordercolor="#c0c0c0" width="10%" height="23" align="center">...</td>
                                                                        <td width="1%" align="center">&nbsp;</td>
                                                                    <?php endif; ?>
                                                                <?php endforeach; ?>
                                                            <?php else: ?>
                                                                <td style="border-collapse: collapse; border: 1px solid #000000" bordercolor="#c0c0c0" width="10%" height="23" align="center">...</td>
                                                                <td width="1%" align="center">&nbsp;</td>
                                                                <td style="border-collapse: collapse; border: 1px solid #000000" bordercolor="#c0c0c0" width="10%" align="center">...</td>
                                                                <td width="1%" align="center">&nbsp;</td>
                                                                <td style="border-collapse: collapse; border: 1px solid #000000" bordercolor="#c0c0c0" width="10%" height="23" align="center">0</td>
                                                                <td width="1%" align="center">&nbsp;</td>
                                                                <td style="border-collapse: collapse; border: 1px solid #000000" bordercolor="#c0c0c0" width="10%" align="center">2</td>
                                                                <td width="1%" align="center">&nbsp;</td>
                                                                <td style="border-collapse: collapse; border: 1px solid #000000" bordercolor="#c0c0c0" width="10%" align="center">2</td>
                                                                <td width="1%" align="center">&nbsp;</td>
                                                                <td style="border-collapse: collapse; border: 1px solid #000000" bordercolor="#c0c0c0" width="10%" align="center">0</td>
                                                                <td width="1%" align="center">&nbsp;</td>
                                                                <td style="border-collapse: collapse; border: 1px solid #000000" bordercolor="#c0c0c0" width="10%" align="center">1</td>
                                                                <td width="1%" align="center">&nbsp;</td>
                                                                <td style="border-collapse: collapse; border: 1px solid #000000" bordercolor="#c0c0c0" width="10 align="center">5</td>
                                                            <?php endif; ?>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" valign="top" align="right">
                                <table width="95%" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <table width="100%" cellspacing="1" cellpadding="1" border="0" align="center">
                                                    <tbody>
                                                        <tr>
                                                            <td width="5%" valign="top">
                                                                <table style="border-collapse: collapse; border: 1px solid #000000" width="93%" cellspacing="1" cellpadding="1" bordercolor="#000000" border="1" align="center">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td align="center">&nbsp;<b>X</b>&nbsp;</td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                            <td width="42%" align="left">PEMOTONG PAJAK (PIMPINAN)</td>
                                                            <td width="2%">&nbsp;</td>
                                                            <td width="5%" valign="top">
                                                                <table style="border-collapse: collapse; border: 1px solid #000000" width="93%" cellspacing="1" cellpadding="1" bordercolor="#000000" border="1" align="center">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td>&nbsp;</td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                            </td>
                                                            <td width="45%" align="left"><b>Kuasa Wajib Pajak</b></td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td width="7%">&nbsp;Nama</td>
                            <td colspan="2" height="20" align="left">
                                <table width="90%" cellspacing="1" cellpadding="1" border="0">
                                    <tbody>
                                        <tr>
                                            <td bordercolor="#000000" style="border-collapse: collapse; border: 1px solid #000000" align="left">&nbsp;</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;NPWP</td>
                            <td colspan="2" align="left">
                                <table width="75%" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                        <tr align="center">
                                            <td style="border-collapse: collapse; border: 1px solid #000000" bordercolor="#000000" width="5%" height="20"></td>
                                            <td align="center">&nbsp;</td>
                                            <td style="border-collapse: collapse; border: 1px solid #000000" bordercolor="#000000" width="5%"></td>
                                            <td align="center">&nbsp;-&nbsp;</td>
                                            <td style="border-collapse: collapse; border: 1px solid #000000" bordercolor="#000000" width="5%"></td>
                                            <td align="center">&nbsp;</td>
                                            <td style="border-collapse: collapse; border: 1px solid #000000" bordercolor="#000000" width="5%"></td>
                                            <td align="center">&nbsp;</td>
                                            <td style="border-collapse: collapse; border: 1px solid #000000" bordercolor="#000000" width="5%"></td>
                                            <td align="center">&nbsp;-&nbsp;</td>
                                            <td style="border-collapse: collapse; border: 1px solid #000000" bordercolor="#000000" width="5%"></td>
                                            <td align="center">&nbsp;</td>
                                            <td style="border-collapse: collapse; border: 1px solid #000000" bordercolor="#000000" width="5%"></td>
                                            <td align="center">&nbsp;</td>
                                            <td style="border-collapse: collapse; border: 1px solid #000000" bordercolor="#000000" width="5%"></td>
                                            <td align="center">&nbsp;-&nbsp;</td>
                                            <td style="border-collapse: collapse; border: 1px solid #000000" bordercolor="#000000" width="5%"></td>
                                            <td align="center">&nbsp;-&nbsp;</td>
                                            <td style="border-collapse: collapse; border: 1px solid #000000" bordercolor="#000000" width="5%"></td>
                                            <td align="center">&nbsp;</td>
                                            <td style="border-collapse: collapse; border: 1px solid #000000" bordercolor="#000000" width="5%"></td>
                                            <td align="center">&nbsp;</td>
                                            <td style="border-collapse: collapse; border: 1px solid #000000" bordercolor="#000000" width="5%"></td>
                                            <td align="center">&nbsp;-&nbsp;</td>
                                            <td style="border-collapse: collapse; border: 1px solid #000000" bordercolor="#000000" width="5%"></td>
                                            <td align="center">&nbsp;</td>
                                            <td style="border-collapse: collapse; border: 1px solid #000000" bordercolor="#000000" width="5%"></td>
                                            <td align="center">&nbsp;</td>
                                            <td style="border-collapse: collapse; border: 1px solid #000000" bordercolor="#000000" width="5%"></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" valign="top" height="60" align="center">Tanda Tangan &amp; Cap</td>
                            <td width="40%" valign="top" align="left">
                                <table width="95%" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                        <tr>
                                            <td align="left">&nbsp;Tanggal : </td>
                                            <?php if (isset($record['TANGGAL_CETAK'])): ?>
                                                <?php foreach(str_split($record['TANGGAL_CETAK']) as $key => $value): ?>
                                                    <td style="border-collapse: collapse; border: 1px solid #000000" bordercolor="#c0c0c0" width="8%" height="23" align="center"><?php echo $value; ?></td>
                                                    <td width="1%" align="center">&nbsp;</td>
                                                <?php endforeach; ?>
                                            <?php else: ?>
                                                <td style="border-collapse: collapse; border: 1px solid #000000" bordercolor="#c0c0c0" width="8%" height="23" align="center">...</td>
                                                <td width="1%" align="center">&nbsp;</td>
                                                <td style="border-collapse: collapse; border: 1px solid #000000" bordercolor="#c0c0c0" width="8%" align="center">...</td>
                                                <td width="1%" align="center">&nbsp;</td>
                                                <td style="border-collapse: collapse; border: 1px solid #000000" bordercolor="#c0c0c0" width="8%" height="23" align="center">0</td>
                                                <td width="1%" align="center">&nbsp;</td>
                                                <td style="border-collapse: collapse; border: 1px solid #000000" bordercolor="#c0c0c0" width="8%" align="center">2</td>
                                                <td width="1%" align="center">&nbsp;</td>
                                                <td style="border-collapse: collapse; border: 1px solid #000000" bordercolor="#c0c0c0" width="8%" align="center">2</td>
                                                <td width="1%" align="center">&nbsp;</td>
                                                <td style="border-collapse: collapse; border: 1px solid #000000" bordercolor="#c0c0c0" width="8%" align="center">0</td>
                                                <td width="1%" align="center">&nbsp;</td>
                                                <td style="border-collapse: collapse; border: 1px solid #000000" bordercolor="#c0c0c0" width="8%" align="center">1</td>
                                                <td width="1%" align="center">&nbsp;</td>
                                                <td style="border-collapse: collapse; border: 1px solid #000000" bordercolor="#c0c0c0" width="8%" align="center">5</td>
                                            <?php endif; ?>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                            <td valign="top">
                                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                        <tr>
                                            <td width="50%" valign="top" align="left">&nbsp;Tanda tangan</td>
                                        </tr>
                                        <tr>
                                            <td height="65">&nbsp;</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </tbody>
                </table>
            </td>
        </tr>
        <tr>
            <td>&nbsp;<b>F.1.1.32.03</b></td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
    </tbody>
</table>
