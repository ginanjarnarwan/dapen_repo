<?php

/**
 * Description of MyApp
 *
 * @author galih
 */
class MyApp {
	public static function getRealIpAddr(){
		if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
		{
		  $ip=$_SERVER['HTTP_CLIENT_IP'];
		}
		elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
		{
		  $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
		}
		else
		{
		  $ip=$_SERVER['REMOTE_ADDR'];
		}
		return $ip;
	}
	public static function setLog($status){
		$tipe_function = Yii::app()->urlManager->parseUrl(Yii::app()->request);
		$telkomnik = '';
		if(isset(Yii::app()->user->username)){
			$telkomnik = Yii::app()->user->username;
		}
		
		$log = new AktifitasLog;
		$log->telkomnik = $telkomnik;
		$log->tipe_function = $tipe_function;
		$log->ip_user = MyApp::getRealIpAddr();
		$log->datetime = date('Y-m-d H:i:s');
		$log->url = Yii::app()->request->requestUri;
		$log->status = $status;
		$log->save();
	}

    public static function lookupnama($nik)
    {
      $modcek = TProsCfuFu::model()->find(array('select'=>'t.BULAN,t.TAHUN','order'=>'t.TAHUN DESC,t.BULAN DESC','limit'=>'1'));
      $bulan = $modcek->BULAN;
      $tahun = $modcek->TAHUN;
      $query = "SELECT NAMA FROM m_hr_current_".$bulan."_".$tahun." WHERE NIK = :nik limit 1";
      $sql = Yii::app()->db->createCommand($query);
      $sql->bindParam(':nik',$nik);
      $data = $sql->queryScalar();
      return $data;
    }
    
    public static function stsBadges($tahun,$bulan)
    {
      $lblok = '<span class="badge badge-success"><i class="fa fa-check"></i></span>';
      $lblnok = '<span class="badge badge-danger">!</i></span>';
      $sap = TRealsap::model()->findByAttributes(array('TAHUN'=>$tahun,'BULAN'=>$bulan));
      $payslip = TPayslip::model()->findAllByAttributes(array('TAHUN'=>$tahun,'BULAN'=>$bulan));
      $hrcur = MHrCurrent::model()->findAllByAttributes(array('TAHUN'=>$tahun,'BULAN'=>$bulan));
      $cfu = TProsCfuFu::model()->findAllByAttributes(array('TAHUN'=>$tahun,'BULAN'=>$bulan));
      $div = TProsDivisi::model()->findAllByAttributes(array('TAHUN'=>$tahun,'BULAN'=>$bulan));
      $witel = TProsWitel::model()->findAllByAttributes(array('TAHUN'=>$tahun,'BULAN'=>$bulan));
      $witel2 = TProsWitelForm::model()->findAllByAttributes(array('TAHUN'=>$tahun,'BULAN'=>$bulan));

      $gen = count($cfu) > 0 AND count($div) > 0 AND count($witel) > 0 AND count($witel2) > 0;
      
      $editsap = Yii::app()->user->roles == 'Admin' ? "<span class='badge'><a title='edit' style='color:white;' href='".Yii::app()->createUrl('tRealsap/edits',array('tahun'=>$tahun,'bulan'=>$bulan))."'><i class='fa fa-pencil'></i> edit</a></span>" : "";
      $editpayslip = Yii::app()->user->roles == 'Admin' ? "<span class='badge'><a title='upload' style='color:white;' href='".Yii::app()->createUrl('payslip/upload',array('tahun'=>$tahun,'bulan'=>$bulan))."'><i class='fa fa-upload'></i> upload</a></span>" : "";
      $edithr = Yii::app()->user->roles == 'Admin' ? "<span class='badge'><a title='upload' style='color:white;' href='".Yii::app()->createUrl('payslip/uploadhrcurrent',array('tahun'=>$tahun,'bulan'=>$bulan))."'><i class='fa fa-upload'></i> upload</a></span>" : "";
      
      if ($sap) 
        $lbsap = $lblok.$editsap;
      else
        $lbsap = $lblnok;

      if (count($payslip) > 0 OR $gen) 
        $lbpayslip = $lblok.$editpayslip;
      else
        $lbpayslip = $lblnok.$editpayslip;

      if (count($hrcur) > 0 OR $gen) 
        $lbhrcur = $lblok.$edithr;
      else
        $lbhrcur = $lblnok.$edithr;

      if ($gen)
        $lbgen = $lblok;
      else
        $lbgen = $lblnok;

      return array('lbsap'=>$lbsap,'lbpayslip'=>$lbpayslip,'lbhrcur'=>$lbhrcur,'lbgen'=>$lbgen);
    }

    public static function statusProses($tahun, $bulan)
    {
      $cfu = TProsCfuFu::model()->findAllByAttributes(array('TAHUN'=>$tahun,'BULAN'=>$bulan));
      $div = TProsDivisi::model()->findAllByAttributes(array('TAHUN'=>$tahun,'BULAN'=>$bulan));
      $witel = TProsWitel::model()->findAllByAttributes(array('TAHUN'=>$tahun,'BULAN'=>$bulan));
      $witel2 = TProsWitelForm::model()->findAllByAttributes(array('TAHUN'=>$tahun,'BULAN'=>$bulan));

      $cek = count($cfu) > 0 AND count($div) > 0 AND count($witel) > 0 AND count($witel2) > 0;
      if ($cek) {
        $ret = '<label class="label label-success">Telah Diproses</label>';
      }else{
        $ret = '<label class="label label-warning">Belum Diproses</label>';
      }
      return array($cek,$ret);
    }

    public static function buatrp($angka)
    {
       $jadi = "Rp " . number_format($angka,2,',','.');
      return $jadi;
    }

    public static function buatuang($angka)
    {
       $jadi = number_format($angka,0,',','.');
      return $jadi;
    }

    public static function convertBulan($a){
      $bulans = array(
            1=>"Januari",
            2=>"Februari",
            3=>"Maret",
            4=>"April",
            5=>"Mei",
            6=>"Juni",
            7=>"Juli",
            8=>"Agustus",
            9=>"September",
            10=>"Oktober",
            11=>"November",
            12=>"Desember"
        );
      return $bulans[$a];
    }

    public static function kelasBackground($a){
      $arr = array(
        1=>'panel-primary',
        2=>'panel-info',
        3=>'panel-success',
        4=>'panel-warning',
        5=>'panel-danger',
        6=>'panel-pink',
        7=>'panel-purple',
        8=>'panel-inverse'
      );
      return $arr[$a[0]];
    }

    public static function dropdownYear(){
    	$starting_year = Yii::app()->params['startingYear'];
    	$now_year = (int)date("Y");
    	$array = array();
    	for ($i=$starting_year; $i <= $now_year; $i++) {
    		$array[$i] = $i;
    	}
    	return $array;
    }

    public static function dropdownMonth(){
		$bulans = array(
            "01"=>"January",
            "02"=>"February",
            "03"=>"March",
            "04"=>"April",
            "05"=>"May",
            "06"=>"June",
            "07"=>"July",
            "08"=>"August",
            "09"=>"September",
            "10"=>"October",
            "11"=>"November",
            "12"=>"December"
        );
		return $bulans;
	}

    public static function potong_text($text,$lebih){
        if (strlen($text) > $lebih){
            $newtext = substr($text, 0, $lebih).'...';
        } else {
            $newtext = $text;
        }

        return $newtext;
    }

    public static function dpp($ppn,$kuitansi){
        if (!empty($ppn))
            $dpp = $kuitansi - $ppn;
        else
            $dpp = 0;

        return $dpp;
    }

    public static function ppnwapu($ppn,$kuitansi){
        if ($kuitansi > 10000000)
            $ppnwapu = $ppn;
        else
            $ppnwapu = "0";

        return $ppnwapu;
    }

    public static function byr($kuitansi,$pph,$denda,$ppnwapu){
        $byr = $kuitansi - $pph - $denda - $ppnwapu;

        return $kuitansi;
    }

    public static function tarifppn($tarifppn)
    {
        if (!empty($tarifppn))
            return $tarifppn.'%';
        else
            return '';
    }

    public static function terbilang($bilangan){
      $angka = array('0','0','0','0','0','0','0','0','0','0','0','0','0','0','0','0');
      $kata = array('','satu','dua','tiga','empat','lima','enam','tujuh','delapan','sembilan');
      $tingkat = array('','ribu','juta','milyar','triliun');
      $panjang_bilangan = strlen($bilangan);
      /* pengujian panjang bilangan */
      if ($panjang_bilangan > 15) {
        $kalimat = "Diluar Batas";
        return $kalimat;
      }

      /* mengambil angka-angka yang ada dalam bilangan,
         dimasukkan ke dalam array */
      for ($i = 1; $i <= $panjang_bilangan; $i++) {
        $angka[$i] = substr($bilangan,-($i),1);
      }

      $i = 1;
      $j = 0;
      $kalimat = "";


      /* mulai proses iterasi terhadap array angka */
      while ($i <= $panjang_bilangan) {

        $subkalimat = "";
        $kata1 = "";
        $kata2 = "";
        $kata3 = "";

    /* untuk ratusan */
        if ($angka[$i+2] != "0") {
            if ($angka[$i+2] == "1") {
                $kata1 = "seratus";
            } else {
                $kata1 = $kata[$angka[$i+2]] . " ratus";
            }
        }

        /* untuk puluhan atau belasan */
        if ($angka[$i+1] != "0") {
          if ($angka[$i+1] == "1") {
            if ($angka[$i] == "0") {
              $kata2 = "sepuluh";
            } elseif ($angka[$i] == "1") {
              $kata2 = "sebelas";
            } else {
              $kata2 = $kata[$angka[$i]] . " belas";
            }
          } else {
            $kata2 = $kata[$angka[$i+1]] . " puluh";
          }
        }

        /* untuk satuan */
        if ($angka[$i] != "0") {
          if ($angka[$i+1] != "1") {
            $kata3 = $kata[$angka[$i]];
          }
        }

        /* pengujian angka apakah tidak nol semua,
           lalu ditambahkan tingkat */
        if (($angka[$i] != "0") OR ($angka[$i+1] != "0") OR
            ($angka[$i+2] != "0")) {
          $subkalimat = "$kata1 $kata2 $kata3 " . $tingkat[$j] . " ";
        }

        /* gabungkan variabe sub kalimat (untuk satu blok 3 angka)
           ke variabel kalimat */
        $kalimat = $subkalimat . $kalimat;
        $i = $i + 3;
        $j = $j + 1;

      }

      /* mengganti satu ribu jadi seribu jika diperlukan */
      if (($angka[5] == "0") AND ($angka[6] == "0")) {
        $kalimat = str_replace("satu ribu","seribu",$kalimat);
      }

      return trim($kalimat);
    }

    public static function r_or_p($app_mj,$rev_mj)
    {
      if($app_mj == 1 or $rev_mj == 1){
        return 'Review';
      } else {
        return 'Preview';
      }
    }

    public static function list_form($userStatusID,$alurid,$alur_exc,$docid,$th)
    {//var_dump($userStatusID,$alurid,$alur_exc,$docid,$th);exit;
      if($userStatusID == 21){
        $isi = '<ul class="dropdown-menu" role="menu"><li><a href="javascript:void(0)" onclick="form_veri_pajak('.$docid.','.$th.')">Form Veri Pajak</a></li></ul>';
      } else {
        $isi = '<ul class="dropdown-menu" role="menu"><li><a href="javascript:void(0)" onclick="form_veri_pajak('.$docid.','.$th.')">Form Veri Pajak</a></li><li><a href="javascript:void(0)" onclick="form_veri_if('.$docid.','.$th.')">Form Veri IF</a></li><li><a href="javascript:void(0)" onclick="form_veri_vendor('.$docid.','.$th.')">Form Veri Vendor</a></li>';

        // Jika alur 17 (DTF) maka dokumen yang lewat Cash Operation hanya boleh di park di DTF
        if($alurid != $alur_exc){
          $isi .= '<li><a href="javascript:void(0)" onclick="memo_jurnal('.$docid.','.$th.')">Memo Jurnal</a></li>';
        }
        $isi .= '</ul>';
      }

      return $isi;
    }

    public static function baMemoJurnal($park_sap,$memo_jurnal,$ba,$sesi_user,$th,$docid,$i)
    {
      if ($park_sap == '1' and $memo_jurnal == 0){ // Perhatikan harusnya ==
        $isi = '<input style="cursor: pointer" type="text" size="4" id="ba'.$i.'" name="ba[]" value="'.$ba.'" maxlength="8" onfocus="blur();" onclick="search_bamj(\'ba'.$i.'\',\''.$sesi_user.'\',\''.$th.'\',\''.$docid.'\');"';
      } else {
        $isi = $ba;
      }

      return $isi;
    }

    public static function actTypeMemoJurnal($park_sap,$cc,$akun,$act_type,$th,$row,$memo_jurnal)
    {
      if ($park_sap == '1' and $memo_jurnal == 0){ //Perhatikan harusnya ==
        if (substr($akun,0,1) == '5'){
          $isi = '<input style="cursor: pointer" name="atype[]" id="atype'.$row.'" value="'.$act_type.'" size="8" maxlength="8" onfocus="blur();" onclick="open_atype(\''.$cc.'\',\''.$akun.'\',\''.$act_type.'\',\''.$th.'\',\'atype'.$row.'\');"';
        }
      } else {
        $isi = $act_type;
      }

      return $isi;
    }

    public static function show($what)
    {
        echo '<table border=0 width=70% cellspacing=0 cellpadding=0 bgcolor="#FFFF00" style="border: 1px solid" align=center>' .
            '<tr><td height=30 valign=middle align=center style="border-right: 2px solid; border-bottom: 2px solid">&nbsp;' .
            '<B><FONT face="verdana, helvetica" SIZE="2" COLOR="navy">' . $what .
            '</FONT></B>&nbsp;</td></tr></table>';
    }

    public static function showreturn($what)
    {
        return '<table border=0 width=70% cellspacing=0 cellpadding=0 bgcolor="#FFFF00" style="border: 1px solid" align=center>' .
            '<tr><td height=30 valign=middle align=center style="border-right: 2px solid; border-bottom: 2px solid">&nbsp;' .
            '<B><FONT face="verdana, helvetica" SIZE="2" COLOR="navy">' . $what .
            '</FONT></B>&nbsp;</td></tr></table>';
    }

    public static function lastday($month = '', $year = ''){
       if(empty($month)){
          $month = date('m');
       }
       if(empty($year)) {
          $year = date('Y');
       }
       $result = strtotime("{$year}-{$month}-01");
       $result = strtotime('-1 second', strtotime('+1 month', $result));
       return date('Ymd', $result);
    }

    public static function budgetlog($tolog,$statustype,$url)
    {
        $tahun = date("Y");
        $userID   = Yii::app()->user->id;
        $menuID   = $url;
        $statusID = Suser::model()->findByAttributes(array('USERID'=>$userID))->STATUSID;
        $note = json_encode($tolog);
        $maks = 3000;
        $arr = array();
        $ind = 0;
        for ($i=0; $i < strlen($note); $i+=$maks) {
            $arr[$ind] = substr($note, $i , $maks);
            if ($ind == 0) {
                $history = new Dhistorybudget;
                $history->YEAR = $tahun;
                $history->USERID = $userID;
                $history->MENUID = $menuID;
                $history->NOTE = new CDbExpression("to_clob('$arr[$ind]')");
                $history->DATEN = new CDbExpression("CURRENT_DATE");
                $history->STATUSID = $statusID;
                $history->STATUSTYPE = $statustype;
                $history->save();
            }else{
                $sql = "UPDATE DHISTORYBUDGET
                        SET NOTE =  CONCAT(NOTE,'$arr[$ind]')
                        WHERE TRANSACTIONID=:id";
                $cmd = Yii::app()->db->createCommand($sql);
                $idh = $history->TRANSACTIONID;
                $cmd->bindParam(':id',$idh);
                $cmd->execute();
            }
            $ind++;
        }
        // $return = array('TRANSACTIONID'=>$history->TRANSACTIONID);
        return isset($history->TRANSACTIONID) ? $history->TRANSACTIONID : null;
    }

}
