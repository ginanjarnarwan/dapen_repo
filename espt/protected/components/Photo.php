<?php
	class Photo 
	{
		public static function getHttpResponseCode($theURL) {
			$headers = array();
			$headers = @get_headers($theURL);
			return substr($headers[0], 9, 3);
		}
		
		public static function urlExists($url)
		{
			
			return (Photo::getHttpResponseCode($url) == 200);
		}
		
		public static function photoExists($nik)
		{
			try{
				$url = Photo::photoUrl1($nik);
				if(Photo::urlExists($url)) return true;
				return false;
			}catch(Exception $e)
			{
				return false;
			}
		}
		
		public static function photoExists2($nik)
		{
			$url = Photo::photoUrl2($nik);
			if (Photo::urlExists($url)) return true;
			return false;
		}
		
		public static function photoUrl($nik)
		{
			/*
			if (Photo::photoExists($nik)){
				return Photo::photoUrl1($nik);
			}else return Photo::photoUrl3();
			*/
			return Photo::photoUrl1($nik);
		}
		
		public static function photoUrl1($nik)
		{
			//return "http://photo.telkom.co.id/".$nik;
			return "http://pwb-esshr.aon.telkom.co.id/index.php?r=pwbPhoto/profilePhoto&nik=".$nik;
		}

		public static function photoUrl2($nik)
		{
			//return "http://10.2.15.232/drp/0PhotoNAS/".$nik.'.jpg';
			return "http://photo.telkom.co.id/".$nik;
		}
		
		public static function photoUrl3()
		{
			return "images/no-photo.jpg";
		}
		
		public static function shortName($name)
		{
			$exploded = explode(' ',$name);
			if (count($exploded)==0) return "?";
			return $exploded[0];
		}
		
	}