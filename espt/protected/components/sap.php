<?php
class sap extends CApplicationComponent{
    private $params;
    private $message;
    private $status;
    private $record;

	// function sap(){
 //        $this->params = array(
 //            "SYSNR"=>"00",
	// 		"CLIENT"=>"500",
	// 		"USER"=>"OSS1",
	// 		"PASSWD"=>"telkom2016",
	// 		"ASHOST"=>"10.62.170.135",
	// 		"LANG"=>"EN",
	// 		"CODEPAGE"=>"1100"
 //        );
 //        $this->status = "OK";
 //        $this->message = "Koneksi berhasil";
	// }

    function sap(){
        $this->params = array(
            "SYSNR"=>"02",
            "CLIENT"=>"100",
            "USER"=>"790126",
            "PASSWD"=>"telkom2016",
            "ASHOST"=>"10.62.170.135",
            "LANG"=>"EN",
            "CODEPAGE"=>"1100"
        );
        $this->status = "OK";
        $this->message = "Koneksi berhasil";
    }

    private function connect(){
        $conn = saprfc_open($this->params);
        if (!$conn) $this->message = saprfc_error();
        return $conn;
	}

     private function connect1(){
        $conn = saprfc_open($this->params);
        return $conn;
    }

    private function free($resource){
		saprfc_function_free($resource);
	}

    private function close($conn){
		saprfc_close($conn);
	}

    public function result(){
        $result = array(
            'status'=>$this->status,
            'message'=>$this->message,
            'record'=>$this->record
        );
        return $result;
	}

    public function getRecords($params){
        try{
            $conn = $this->connect();
            $is_function = saprfc_function_discover($conn, $params["FUNCTION"]);
    		if (!$is_function){
                throw new Exception("Function tidak tersedia");
            }

            if(count($params["FILTER"]) > 0) {
                foreach ($params["FILTER"] as $key => $value){
                    if(!saprfc_import($is_function, $key, $value)){
                        throw new Exception(saprfc_error($is_function));
                    }
                }
            }

            $table = saprfc_table_init($is_function, $params["TABLE"]);
            if (!$table){
                throw new Exception("Table > " . $params["TABLE"] . " : NOT FOUND");
            }
            $rfc_rc = saprfc_call_and_receive($is_function);
    		if($rfc_rc != SAPRFC_OK){
                if($rfc == SAPRFC_EXCEPTION){
                    throw new Exception("Exception raised: " . saprfc_exception($is_function));
                }else{
                    throw new Exception(saprfc_error($is_function));
                }
            }
            $return = saprfc_export($is_function, "RETURN");
    		$rows = saprfc_table_rows($is_function, $params["TABLE"]);
            $rec = array();
            for($i=1;$i<=$rows;$i++){
                $data = saprfc_table_read($is_function, $params["TABLE"], $i);
                $rec[] = $data;
            }
            $this->record = $rec;
            $this->free($is_function);
            $this->close($conn);
        }catch(Exception $e){
            $this->status = "NOT";
            $this->message = $e->getMessage();
        }
        return $this->result();
    }

    public function parkDoc($otv_stat,$if_stat,$data,$username,$password)
    {
        try {
            $this->params = array(
                "SYSNR"=>"02",
                "CLIENT"=>"100",
                "USER"=>$username,
                "PASSWD"=>$password,
                "ASHOST"=>"10.62.170.135",
                "LANG"=>"EN",
                "CODEPAGE"=>"1100"
                );
            // echo '<pre>'; var_dump($data);exit;
            $conn = $this->connect();
            if (!$conn) {$this_status[] = 'Login Failed'; return array('last_status'=>'1','this_status'=>$this_status);}

            $fce = saprfc_function_discover($conn,"ZRFC_FINEST_SA_POST");
            // var_dump(!$fce);exit;

            if (!$fce)
                throw new Exception("Discovering interface of function module failed");

            // saprfc_import ($fce,"BI_SESSION","");
            saprfc_import ($fce,"UPLDATA","");
            saprfc_import ($fce,"BUKRS","1000");
            saprfc_import ($fce,"WAERS","IDR");
            saprfc_table_init ($fce,"DATATABLE");
            saprfc_table_init ($fce,"LOGLINES");

            foreach ($data as $row) {
                saprfc_table_append($fce,"DATATABLE",$row);
            }

            $rfc_rc = saprfc_call_and_receive($fce);
            if ($rfc_rc != SAPRFC_OK) {
                if ($conn == SAPRFC_EXCEPTION)
                    throw new Exception("Exception raised: ".saprfc_exception($fce));
                else
                    throw new Exception(saprfc_error($fce));
            }

            $CMESSAGES = saprfc_export($fce,"CMESSAGES");
            $DOCNUM = saprfc_export($fce,"DOCNUM");

            $rows = saprfc_table_rows ($fce,"LOGLINES");
            for ($i=1;$i<=$rows;$i++) {
                $datarfc = saprfc_table_read ($fce,"LOGLINES",$i);
                $last_status = $datarfc["FELD1"];
                $this_status[] = $datarfc["FELD1"];
                //$last_status = str_replace("&", "", $LOGLINES[count($LOGLINES)-1]);
            }

            /*if (!empty($DOCNUM)){

            }else {
            $rows = saprfc_table_rows ($fce,"LOGLINES");
            for ($i=1;$i<=$rows;$i++)
                $LOGLINES[] = saprfc_table_read ($fce,"LOGLINES",$i);
                $DEBUG = saprfc_function_debug_info($fce);
                echo '<pre>';
                var_dump($LOGLINES);
            }
            exit;*/
            if (!empty($DOCNUM)){

            } else {
                if ($last_status != ""){

                } else {
                    $rows = saprfc_table_rows ($fce,"LOGLINES");
                    for ($i=1;$i<=$rows;$i++)
                        $LOGLINES[] = saprfc_table_read ($fce,"LOGLINES",$i);
                    $DEBUG = saprfc_function_debug_info($fce);
                }
            }

            saprfc_function_free($fce);
            saprfc_close($conn);

        } catch (Exception $e) {
            $this->status = "NOT";
            $this->message = $e->getMessage();
        }

        $result = array(
                'status'=>$this->status,
                'message'=>$this->message,
                'CMESSAGES'=>$CMESSAGES,
                'DOCNUM'=>$DOCNUM,
                'LOGLINES'=>$LOGLINES,
                'DEBUG'=>$DEBUG,
                'last_status'=>$last_status,
                'this_status'=>$this_status,
            );

        return $result;
    }

    public function postDoc($sapnodoc,$th,$username,$password)
    {
        try {
            $this->params = array(
                "SYSNR"=>"02",
                "CLIENT"=>"100",
                "USER"=>$username,
                "PASSWD"=>$password,
                "ASHOST"=>"10.62.170.135",
                "LANG"=>"EN",
                "CODEPAGE"=>"1100"
                );
            $conn = $this->connect();
            if (!$conn) {$this_status[] = 'Login Failed'; return array('last_status'=>'1','this_status'=>$this_status);}
            $fce = saprfc_function_discover($conn,"ZRFC_FINEST_POSTPARK");
            if (!$fce)
                throw new Exception("Discovering interface of function module failed");

            saprfc_import ($fce,"BELNR",$sapnodoc);
            saprfc_import ($fce,"BI_SESSION","");
            saprfc_import ($fce,"BUKRS","1000");
            saprfc_import ($fce,"GJAHR",$th);
            saprfc_table_init ($fce,"LOGLINES");

            $rfc_rc = saprfc_call_and_receive($fce);
            if ($rfc_rc != SAPRFC_OK) {
                if ($conn == SAPRFC_EXCEPTION)
                    throw new Exception("Exception raised: ".saprfc_exception($fce));
                else
                    throw new Exception(saprfc_error($fce));
            }

            $CMESSAGES = saprfc_export ($fce,"CMESSAGES");
            $DOCNUM = saprfc_export ($fce,"DOCNUM");

            $rows = saprfc_table_rows ($fce,"LOGLINES");
            for ($i=1;$i<=$rows;$i++){
                $datarfc = saprfc_table_read ($fce,"LOGLINES",$i);
                $last_status = $datarfc["FELD1"];
                $this_status[] = $datarfc["FELD1"];
            }

            if ($DOCNUM == $sapnodoc){

            } else {
                if ($last_status != ""){

                } else {
                    $rows = saprfc_table_rows ($fce,"LOGLINES");
                    for ($i=1;$i<=$rows;$i++)
                        $LOGLINES[] = saprfc_table_read ($fce,"LOGLINES",$i);
                    $DEBUG = saprfc_function_debug_info($fce);
                }
                saprfc_function_free($fce);
                saprfc_close($conn);
            }
        } catch (Exception $e) {
            $this->status = "NOT";
            $this->message = $e->getMessage();
        }

        $result = array(
                'status'=>$this->status,
                'message'=>$this->message,
                'CMESSAGES'=>$CMESSAGES,
                'DOCNUM'=>$DOCNUM,
                'LOGLINES'=>$LOGLINES,
                'DEBUG'=>$DEBUG,
                'last_status'=>$last_status,
                'this_status'=>$this_status,
            );
        return $result;
    }

}
?>
