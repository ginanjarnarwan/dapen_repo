<?php
class CetakSpt extends CWidget
{
    public $record;
    public function run()
    {
        $this->renderContent();
    }

    protected function renderContent()
    {
        $this->render('cetak_spt', array(
            'record'=>$this->record
        ));
    }
}
