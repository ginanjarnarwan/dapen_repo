<?php
class RFC {
    static $conf;

    /**
     * @var sapRFC $sapConn
     */
	static $sapConn;
	
	//init statemen
	public static function init()
	 {
	      if (empty(self::$sapConn))
		  {
		  	$sap_login = array (
						"CLIENT"=>"100",
						"SYSNR"=>"00",
						"R3NAME"=>"P00",
						"MSHOST"=>"dbsimtel.telkom.co.id",
						"GROUP"=>"FICO",
						"LANG"=>"EN",
						"CODEPAGE"=>"1100"
				);
		  	
		   $confP = array (
						"client"=>"100",
						"r3name"=>"P00",
						"trace"=>"-",
						"mshost"=>"dbsimtel.telkom.co.id",
						"group"=>"INSANI",
				);
				
		   $confDev = array (
						"client"=>"300",
						"sysnr"=>"D00",
						"trace"=>"-",
						"sapServer"=>"10.2.12.137",
						"group"=>"INSANI",
				);	
				
			$conf = array (
						"client"=>"100",
						//"r3name"=>"P00",
						"sysnr"=>"P00",
						"trace"=>"-",
						"sapServer"=>"10.2.12.156",
						"group"=>"INSANI",
				);				
			$userid_x = "esstelkom";
			$pass_x = "SIMTEL21";
				
		   	$userid_production='instela001';
		   	$password_production ='hrinsani';

		   //$userid_development='870018';
		   //$password_development ='akusayangayah88';
	
		   $sap = new sapRFC($sap_login,$userid_x,$pass_x);
		   //$sap = new sapRFC($confDev,$userid_production,$password_production);
		   $sap->connect();
		   self::$sapConn = $sap;
		   
		  }
    }
    public static function createConnectionProd()
    {
        $conf = array (
     						"client"=>"100",
     						"r3name"=>"P00",
     						"trace"=>"-",
     						"mshost"=>"dbsimtel.telkom.co.id",
     						"group"=>"INSANI",
     				);
        $userid_production='instela001';
        $password_production ='hrinsani';
        $sap = new sapRFC($conf,$userid_production,$password_production);
		$sap->connect();
        return $sap;
    }
		public static function initWithLogin($username,$passwd)
		{
	      if (empty(self::$sapConn))
		  {
		   $conf = array (
						"client"=>"100",
						"r3name"=>"P00",
						"trace"=>"-",
						"mshost"=>"dbsimtel.telkom.co.id",
						"group"=>"INSANI",
				);
				
		   $confDev = array (
						"client"=>"300",
						"sysnr"=>"D00",
						"trace"=>"-",
						"sapServer"=>"10.2.12.137",
						"group"=>"INSANI",
				);	
				

		   $userid=$username;
		   $password=$passwd;
	
		   $sap = new sapRFC($confDev,$userid,$password);
           $sukses = true;
		   if (!$sap->connectHeadless()) $sukses = false;
		   self::$sapConn = $sap;
           return $sukses;
          }
		}
	//to get data from return table
	private function fetch_rows($initTableName) {
	   $_dataRows = array();
	   $rows = saprfc_table_rows ($this->fce,$initTableName);
       for ($i=1; $i<=$rows; $i++)
      	   $_dataRows[$i] = saprfc_table_read ($this->fce,$initTableName,$i);
	   return $_dataRows;
	}
	// free connection
	private function free() {
		saprfc_function_free($this->fce);
	}
	
	
//==========================================definisi all function rfc ===============================================	
	public static function ZRFC_MB_CB($data)
	{
		self::init();
		$sap = self::$sapConn;
		$sap->functionDiscover('ZRFC_MB_CB');
		$paramNames[0] = "ACCGROUP";
		$paramNames[1] = "CCGROUP";
		$paramNames[2] = "GJAHR";
		$paramNames[3] = "KOKRS";
		$paramNames[4] = "PERFROM";
		$paramNames[5] = "PERTO";
		$paramNames[6] = "VERSN";
		
		$paramValues[0] = "E";
	    $paramValues[1] = $data['ccgroup'];
		$paramValues[2] = $data['_th_per'];
		$paramValues[3] = "1000";
		$paramValues[4] = $data['_bl_per_from'];
		$paramValues[5] = $data['_bl_per_to'];
		$paramValues[6] = "000";
		
		$sap->importParameter( $paramNames, $paramValues);
		$sap->setInitTable('RESTABLE');
		
		$hasil =array();
		if ($sap->executeSAP() != 2)
		{
			$hasil = $sap->fetch_rows('RESTABLE');
		}
		return $hasil;
	}



	public static function ZHR_RFC_DRP_CARI_NIK($data)
	{
		self::init();
		$sap = self::$sapConn;
		$sap->functionDiscover('ZHR_RFC_DRP_CARI_NIK');
		$paramNames[0] = "N_NIK";
		$paramNames[1] = "NAMA";
		$paramNames[2] = "DIVISI";
		
		$paramValues[0] = $data['nik'];
	    $paramValues[1] = $data['nama'];
		$paramValues[2] = $data['divisi'];
		
		$sap->importParameter( $paramNames, $paramValues);
		$sap->setInitTable('T_POSISI_SEKARANG');
		
		$hasil =array();
		if ($sap->executeSAP() != 2)
		{
			$hasil = $sap->fetch_rows('T_POSISI_SEKARANG');
		}
		return $hasil;
	}
	
	public static function BAPI_EMPLOYEE_GETDATA($data)
	{
		self::init();
		$sap = self::$sapConn;
		$sap->functionDiscover('BAPI_EMPLOYEE_GETDATA');
		$paramNames[0] = "EMPLOYEE_ID";
		
		$paramValues[0] = $data['nik'];
		
		$sap->importParameter( $paramNames, $paramValues);
		$sap->setInitTable('PERSONAL_DATA');
		
		$hasil =array();
		if ($sap->executeSAP() != 2)
		{
			$hasil = $sap->fetch_rows('PERSONAL_DATA');
		}
		return $hasil;
	}
	
	public static function ZHR_RFC_DRP_POSISI_SEKARANG($data)
	{
		self::init();
		$sap = self::$sapConn;
		$sap->functionDiscover('ZHR_RFC_DRP_POSISI_SEKARANG');
		$paramNames[0] = "N_NIK";
		
		$paramValues[0] = $data['nik'];
		
		$sap->importParameter( $paramNames, $paramValues);
		$sap->setInitTable('T_POSISI_SEKARANG');
		
		$hasil =array();
		if ($sap->executeSAP() != 2)
		{
			$hasil = $sap->fetch_rows('T_POSISI_SEKARANG');
		}
		return $hasil;
	}
	
	public static function ZHR_RFC_DRP_R_BASIC_PAY($data)
	{
		self::init();
		$sap = self::$sapConn;
		$sap->functionDiscover('ZHR_RFC_DRP_R_BASIC_PAY');
		$paramNames[0] = "N_NIK";
		
		$paramValues[0] = $data['nik'];
		
		$sap->importParameter( $paramNames, $paramValues);
		$sap->setInitTable('T_HIS_BASIC_PAY');
		
		$hasil =array();
		if ($sap->executeSAP() != 2)
		{
			$hasil = $sap->fetch_rows('T_HIS_BASIC_PAY');
		}
		return $hasil;
	}
	
	public static function ZHR_RFC_DRP_ORANG_TUA($data)
	{
		self::init();
		$sap = self::$sapConn;
		$sap->functionDiscover('ZHR_RFC_DRP_ORANG_TUA');
		$paramNames[0] = "N_NIK";
		
		$paramValues[0] = $data['nik'];
		
		$sap->importParameter( $paramNames, $paramValues);
		$sap->setInitTable('T_ORANG_TUA');
		
		$hasil =array();
		if ($sap->executeSAP() != 2)
		{
			$hasil = $sap->fetch_rows('T_ORANG_TUA');
		}
		return $hasil;
	}
	
	public static function ZHR_RFC_DRP_ISSU($data)
	{
		self::init();
		$sap = self::$sapConn;
		$sap->functionDiscover('ZHR_RFC_DRP_ISSU');
		$paramNames[0] = "N_NIK";
		
		$paramValues[0] = $data['nik'];
		
		$sap->importParameter( $paramNames, $paramValues);
		$sap->setInitTable('T_ISSU');
		
		$hasil =array();
		if ($sap->executeSAP() != 2)
		{
			$hasil = $sap->fetch_rows('T_ISSU');
		}
		return $hasil;
	}
	
	public static function ZHR_RFC_DRP_MERTUA($data)
	{
		self::init();
		$sap = self::$sapConn;
		$sap->functionDiscover('ZHR_RFC_DRP_MERTUA');
		$paramNames[0] = "N_NIK";
		
		$paramValues[0] = $data['nik'];
		
		$sap->importParameter( $paramNames, $paramValues);
		$sap->setInitTable('T_MERTUA');
		
		$hasil =array();
		if ($sap->executeSAP() != 2)
		{
			$hasil = $sap->fetch_rows('T_MERTUA');
		}
		return $hasil;
	}
	
	public static function ZHR_RFC_DRP_ANAK($data)
	{
		self::init();
		$sap = self::$sapConn;
		$sap->functionDiscover('ZHR_RFC_DRP_ANAK');
		$paramNames[0] = "N_NIK";
		
		$paramValues[0] = $data['nik'];
		
		$sap->importParameter( $paramNames, $paramValues);
		$sap->setInitTable('T_ANAK');
		
		$hasil =array();
		if ($sap->executeSAP() != 2)
		{
			$hasil = $sap->fetch_rows('T_ANAK');
		}
		return $hasil;
	}
		
	public static function ZHR_RFC_DRP_DINAS($data)
	{
		self::init();
		$sap = self::$sapConn;
		$sap->functionDiscover('ZHR_RFC_DRP_DINAS');
		$paramNames[0] = "N_NIK";
		
		$paramValues[0] = $data['nik'];
		
		$sap->importParameter( $paramNames, $paramValues);
		$sap->setInitTable('T_DINAS');
		
		$hasil =array();
		if ($sap->executeSAP() != 2)
		{
			$hasil = $sap->fetch_rows('T_DINAS');
		}
		return $hasil;
	}
	
	public static function ZHR_RFC_DRP_STATUS($data)
	{
		self::init();
		$sap = self::$sapConn;
		$sap->functionDiscover('ZHR_RFC_DRP_STATUS');
		$paramNames[0] = "N_NIK";
		
		$paramValues[0] = $data['nik'];
		
		$sap->importParameter( $paramNames, $paramValues);
		$sap->setInitTable('T_STATUS');
		
		$hasil =array();
		if ($sap->executeSAP() != 2)
		{
			$hasil = $sap->fetch_rows('T_STATUS');
		}
		return $hasil;
	}
	
	public static function ZHR_RFC_DRP_PENDIDIKAN($nik)
	{
		self::init();
		$sap = self::$sapConn;
		$sap->functionDiscover('ZHR_RFC_DRP_PENDIDIKAN');
		$paramNames[0] = "N_NIK";
		
		$paramValues[0] = $nik;
		
		$sap->importParameter( $paramNames, $paramValues);
		$sap->setInitTable('T_PENDIDIKAN');
		
		$hasil =array();
		if ($sap->executeSAP() != 2)
		{
			$hasil = $sap->fetch_rows('T_PENDIDIKAN');
		}
		return $hasil;
	}
	
	public static function ZHR_RFC_DRP_PELATIHAN($data)
	{
		self::init();
		$sap = self::$sapConn;
		$sap->functionDiscover('ZHR_RFC_DRP_PELATIHAN');
		$paramNames[0] = "N_NIK";
		
		$paramValues[0] = $data['nik'];
		
		$sap->importParameter( $paramNames, $paramValues);
		$sap->setInitTable('T_PELATIHAN');
		
		$hasil =array();
		if ($sap->executeSAP() != 2)
		{
			$hasil = $sap->fetch_rows('T_PELATIHAN');
		}
		return $hasil;
	}
	
	public static function ZHR_RFC_DRP_NKI($data)
	{
		self::init();
		$sap = self::$sapConn;
		$sap->functionDiscover('ZHR_RFC_DRP_NKI');
		$paramNames[0] = "N_NIK";
		
		$paramValues[0] = $data['nik'];
		
		$sap->importParameter( $paramNames, $paramValues);
		$sap->setInitTable('T_NKI');
		
		$hasil =array();
		if ($sap->executeSAP() != 2)
		{
			$hasil = $sap->fetch_rows('T_NKI');
		}
		return $hasil;
	}
	
	public static function ZHR_RFC_DRP_PENGHARGAAN($data)
	{
		self::init();
		$sap = self::$sapConn;
		$sap->functionDiscover('ZHR_RFC_DRP_PENGHARGAAN');
		$paramNames[0] = "N_NIK";
		
		$paramValues[0] = $data['nik'];
		
		$sap->importParameter( $paramNames, $paramValues);
		$sap->setInitTable('T_PENGHARGAAN');
		
		$hasil =array();
		if ($sap->executeSAP() != 2)
		{
			$hasil = $sap->fetch_rows('T_PENGHARGAAN');
		}
		return $hasil;
	}
	
	public static function ZHR_RFC_DRP_FASILITAS($data)
	{
		self::init();
		$sap = self::$sapConn;
		$sap->functionDiscover('ZHR_RFC_DRP_FASILITAS');
		$paramNames[0] = "N_NIK";
		
		$paramValues[0] = $data['nik'];
		
		$sap->importParameter( $paramNames, $paramValues);
		$sap->setInitTable('T_FASILITAS');
		
		$hasil =array();
		if ($sap->executeSAP() != 2)
		{
			$hasil = $sap->fetch_rows('T_FASILITAS');
		}
		return $hasil;
	}
	
	public static function ZHR_RFC_VIEW_TPK($data)
	{
		self::init();
		$sap = self::$sapConn;
		$sap->functionDiscover('ZHR_RFC_VIEW_TPK');
		$paramNames[0] = "NIK";
		
		$paramValues[0] = $data['nik'];
		
		$sap->importParameter( $paramNames, $paramValues);
		$sap->setInitTable('T_VIEW_TPK');
		
		$hasil =array();
		if ($sap->executeSAP() != 2)
		{
			$hasil = $sap->fetch_rows('T_VIEW_TPK');
		}
		return $hasil;
	}
	
	public static function ZHR_RFC_DRP_CUTI($data)
	{
		self::init();
		$sap = self::$sapConn;
		$sap->functionDiscover('ZHR_RFC_DRP_CUTI');
		$paramNames[0] = "N_NIK";
		
		$paramValues[0] = $data['nik'];
		
		$sap->importParameter( $paramNames, $paramValues);
		$sap->setInitTable('T_CUTI');
		
		$hasil =array();
		if ($sap->executeSAP() != 2)
		{
			$hasil = $sap->fetch_rows('T_CUTI');
		}
		return $hasil;
	}
	
	public static function ZHR_RFC_DRP_ALAMAT($data)
	{
		self::init();
		$sap = self::$sapConn;
		$sap->functionDiscover('ZHR_RFC_DRP_ALAMAT');
		$paramNames[0] = "N_NIK";
		
		$paramValues[0] = $data['nik'];
		
		$sap->importParameter( $paramNames, $paramValues);
		$sap->setInitTable('T_ALAMAT');
		
		$hasil =array();
		if ($sap->executeSAP() != 2)
		{
			$hasil = $sap->fetch_rows('T_ALAMAT');
		}
		return $hasil;
	}
	
	public static function ZHR_RFC_DRP_INFORMASI_LAIN($data)
	{
		self::init();
		$sap = self::$sapConn;
		$sap->functionDiscover('ZHR_RFC_DRP_INFORMASI_LAIN');
		$paramNames[0] = "N_NIK";
		
		$paramValues[0] = $data['nik'];
		
		$sap->importParameter( $paramNames, $paramValues);
		$sap->setInitTable('T_INFORMASI_LAIN');
	
		$hasil =array();
		if ($sap->executeSAP() != 2)
		{
			$hasil = $sap->fetch_rows('T_INFORMASI_LAIN');
		}
		return $hasil;
	}
	
	public static function ZHR_RFC_DRP_SERTIFIKASI($data)
	{
		self::init();
		$sap = self::$sapConn;
		$sap->functionDiscover('ZHR_RFC_DRP_SERTIFIKASI');
		$paramNames[0] = "N_NIK";
		
		$paramValues[0] = $data['nik'];
		
		$sap->importParameter( $paramNames, $paramValues);
		$sap->setInitTable('T_SERTIFIKASI');
	
		$hasil =array();
		if ($sap->executeSAP() != 2)
		{
			$hasil = $sap->fetch_rows('T_SERTIFIKASI');
		}
		return $hasil;
	}
        
        public static function ZHR_LSO_COURSE_BEGEND($begdate=null,$enddate=null,$nik=null,$istat)
        {
                if ((empty($begdate))and(empty($enddate))){$begdate = date('Ymd');$enddate = date('Ymd');}
                self::init();
                $sap = self::$sapConn;
                $sap->functionDiscover('ZHR_LSO_COURSE_LIST2');
                //$sap->printDefinition(); exit;
                $paramNames = array(); $paramValues=array();
                $paramNames[] = 'P_BEGDA'; $paramValues[] = $begdate;
                $paramNames[] = 'P_ENDDA'; $paramValues[] = $enddate;
                $paramNames[] = 'P_PERNR'; $paramValues[] = $nik;
                $paramNames[] = 'P_ISTAT'; $paramValues[] = $istat;//$paramValues[] = 1;


                //if (!empty($nik))
                //$paramNames[] = 'P_PERNR'; $paramValues[] = $nik;
                $sap->importParameter( $paramNames, $paramValues);
                $sap->setInitTable('T_LIST_COURSE');
                $sap->setInitTable('T_LIST_ATTENDEE');
                //$sap->printDefinition(); exit;
                $courseList=array(); $attendeeList= array();
                if ($sap->executeSAP() != 2)
                {
                $courseList= $sap->fetch_rows('T_LIST_COURSE');
                $attendeeList = $sap->fetch_rows('T_LIST_ATTENDEE');
                
              //  echo "<pre>";
              // var_dump($courseList);
              //  exit();
              //  echo "</pre>";
                
                
                } else echo "gagal";
                if ($courseList==null) $courseList = array();
                if ($attendeeList==null) $attendeeList = array();
                return compact('courseList','attendeeList');
                //$sap->printDefinition();
        }

}
?>