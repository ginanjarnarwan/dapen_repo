<?php

/**
 * Yii extension wrapping the jQuery UI Monthpicker Widget from Julien Poumailloux
 * {@link http://www.jpoumailloux.com/jquery-ui-monthpicker/}
 * {@link https://github.com/thebrowser/jquery.ui.monthpicker}
 * 
 * @author C.Yildiz <c@cba-solutions.org>
 *
 */
Yii::import('zii.widgets.jui.CJuiInputWidget');

/**
 * Base class.
 */
class EJuiTriMonthPicker extends CJuiInputWidget
{
	 /**
	 * @var CModel the data model associated with this widget.
	 */
	public $model=null;
	/**
	 * @var string the attribute associated with this widget.
	 * The name can contain square brackets (e.g. 'name[1]') which is used to collect tabular data input.
	 */
	public $attribute=null;
	/**
	 * @var string the name of the drop down list. This must be set if {@link model} is not set.
	 */
	public $name = null;
	/**
	 * @var string the selected input value(s). This is used only if {@link model} is not set.
	 */
	public $value = null;
	/**
	 * @var array the options for the jQuery UI MultiSelect Widget
	 */
	public $options = array();
	/**
	 * @var array additional HTML attributes for the drop down list
	 * Options like class, style etc. are adopted by the jQuery UI MultiSelect Widget
	 */
	public $htmlOptions = array();
	



	public function init()
	{
		// Put togehther options for plugin
		$cs = Yii::app()->getClientScript();
		$assets = Yii::app()->getAssetManager()->publish(dirname(__FILE__) . '/assets');
		$cs->registerScriptFile($assets . '/jquery.ui.trimonthpicker.js');
		
		// Default options for the jQuery widget
		$default_options = array(
			'monthNames' => array(
				// Yii::t('EJuiTriMonthPicker.EJuiTriMonthPicker','January'),
				// Yii::t('EJuiTriMonthPicker.EJuiTriMonthPicker','February'),
				// Yii::t('EJuiTriMonthPicker.EJuiTriMonthPicker','March'),
				// Yii::t('EJuiTriMonthPicker.EJuiTriMonthPicker','April'),
				// Yii::t('EJuiTriMonthPicker.EJuiMonthPicker','May'),
				// Yii::t('EJuiTriMonthPicker.EJuiMonthPicker','June'),
				// Yii::t('EJuiTriMonthPicker.EJuiMonthPicker','July'),
				// Yii::t('EJuiTriMonthPicker.EJuiMonthPicker','August'),
				// Yii::t('EJuiTriMonthPicker.EJuiMonthPicker','September'),
				// Yii::t('EJuiTriMonthPicker.EJuiMonthPicker','October'),
				// Yii::t('EJuiTriMonthPicker.EJuiMonthPicker','November'),
				// Yii::t('EJuiTriMonthPicker.EJuiMonthPicker','December'),
			),
			'monthNamesShort' => array(
				Yii::t('EJuiTriMonthPicker.EJuiTriMonthPicker','Triwulan  &nbsp;&nbsp;I'),
				Yii::t('EJuiTriMonthPicker.EJuiTriMonthPicker','Triwulan II'),
				Yii::t('EJuiTriMonthPicker.EJuiTriMonthPicker','Triwulan III'),
				Yii::t('EJuiTriMonthPicker.EJuiTriMonthPicker','Triwulan IV'),
				// Yii::t('EJuiTriMonthPicker.EJuiMonthPicker','Mei'),
				// Yii::t('EJuiTriMonthPicker.EJuiMonthPicker','Jun'),
				// Yii::t('EJuiTriMonthPicker.EJuiMonthPicker','Jul'),
				// Yii::t('EJuiTriMonthPicker.EJuiMonthPicker','Agu'),
				// Yii::t('EJuiTriMonthPicker.EJuiMonthPicker','Sep'),
				// Yii::t('EJuiTriMonthPicker.EJuiMonthPicker','Okt'),
				// Yii::t('EJuiTriMonthPicker.EJuiMonthPicker','Nov'),
				// Yii::t('EJuiTriMonthPicker.EJuiMonthPicker','Des'),
			),
			'prevText'=>Yii::t('EJuiTriMonthPicker.EJuiTriMonthPicker','Prev'),
			'nextText'=>Yii::t('EJuiTriMonthPicker.EJuiTriMonthPicker','Next'),
			'showOn'=>'focus',
			'yearRange'=>'-5:+15',
			'dateFormat'=>'yy-mm',
			'changeYear'=>true,
			'buttonImageOnly' => false,
			'buttonImage' => $assets."/images/calendar.png",
			'buttonText'=>Yii::t('EJuiTriMonthPicker.EJuiTriMonthPicker','Select Month'),
			//'onSelect'=>'js:function(input){$(this).focus(); }'
		);
		if(!empty($this->options)) $opt = array_merge($default_options, $this->options);
		else $opt = $default_options;
		$this->options = $opt;
		
		parent::init();
	}

	/**
	 * Run this widget.
	 * This method registers necessary javascript and renders the needed HTML code.
	 */
	public function run()
	{
		list($name, $id) = $this->resolveNameId();
		
		if($this->hasModel())
			echo CHtml::activeTextField($this->model, $this->attribute, $this->htmlOptions); 
		else
			echo CHtml::textField($this->name, $this->value, $this->htmlOptions);
			
		$joptions=CJavaScript::encode($this->options);
		$jscode = "jQuery('#{$id}').monthpicker({$joptions});";
		Yii::app()->getClientScript()->registerScript(__CLASS__ . '#' . $id, $jscode);
	}
}
