<?php
return array(
'January'=>'Januari',
'February'=>'Februari',
'March'=>'Maret',
'April'=>'April',
'May'=>'Mei',
'June'=>'Juni',
'July'=>'Juli',
'August'=>'Agustus',
'September'=>'September',
'October'=>'Oktober',
'November'=>'November',
'December'=>'Desember',
'I'=>'Triwulan I',
'II'=>'Triwulan II',
'III'=>'Triwulan III',
'IV'=>'Triwulan IV',
'Jul'=>'Jul',
'Aug'=>'Agu',
'Sep'=>'Sep',
'Oct'=>'Okt',
'Nov'=>'Nov',
'Dec'=>'Des',
'Select Month'=>'Pilih Bulan',
'Prev'=>'Kembali',
'Next'=>'Lanjut',
);
?>