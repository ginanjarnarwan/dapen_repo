<?php
class LazyPager extends CBasePager
{
    public function run()
    {
        $currentPage = $this->currentPage;
        $pageCount = $this->pageCount;

        if($currentPage < $pageCount)
        {
            list($controller, $action) = explode('/', Yii::app()->urlManager->parseUrl(Yii::app()->request));
            $theUrl = Yii::app()->request->url;
            echo '
                                <div class="loading" style="display: block; opacity: 0; margin-bottom: 30px;"></div>
                                <div class="load-msg">
                                        <input type="hidden" id="pgNumber" value="'.($currentPage+1).'"/>
                                        <span id="paginator"></span>
                                </div>
                        ';
            $cs = Yii::app()->getClientScript();
            $script = '
                                var inLoad = 0;

                                $("#paginator").click(function(e)
                                {
                                    if(inLoad == 0){
                                        var itemsList = $(this).parent().parent().parent().prev();
                                        var nextPage = $(this).prev().val()*1;
                                        var pageCount = '.$pageCount.'*1;
                                        var goLoad = $(this).parent().prev();

                                        var pgNumber = $(this).prev();

                                        goLoad.css("display", "block");
                                        goLoad.animate({opacity: 1}, 600);

                                        inLoad = 1;

                                        $.ajax({
                                                url:"'.$theUrl.'&ajax=1&page="+(nextPage+1),
                                                success:function(returnData)
                                                {
                                                        goLoad.animate({opacity: 0}, 600);
                                                        goLoad.css("display", "none");
                                                        itemsList.append(returnData); //append the content
                                                        pgNumber.val(nextPage+1);
                                                        inLoad = 0;

                                                        if(pgNumber.val() == pageCount)
                                                                $("#paginator").parent().attr("style", "display:none;");
                                                }
                                        });
                                    }
                                });
                        ';
            $cs->registerScript('#script', $script, CClientScript::POS_READY);
        }
    }
}