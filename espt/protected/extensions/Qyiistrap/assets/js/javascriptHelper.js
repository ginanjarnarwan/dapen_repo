$(document).on('hide.bs.modal', function () {
    $(".modal-load-background").attr('src', '');
});

$(document).on('shown.bs.modal', function () {
    $(".modal-load-background").css('background-image', 'url(images/gears.gif)');
});

$( document ).ready(function() {
    /*$('#menuLeft').affix({
        offset: {
            top: function () {
                return this.width <= 768 ? 290 : 0;
            }, bottom: 5
        }
    })*/
    $('[data-toggle="offcanvas"]').click(function () {
        $('.row-offcanvas').toggleClass('active')
    });
});

function getAllSelectedGrid(gridId){
    return selected = $(gridId).selGridView("getAllSelection");
}

function notification(url, div, type, message){
    $.ajax({
        type: "POST",
        url: url,
        data: {
            type : type,
            message : message
        },
        success: function(result){
            $(div).html(result);
        },
        error:function(){
            $(div).html('There is error while submit');
        },
        dataType: "html"
    });
}

function getDocHeight(doc) {
    doc = doc || document;
    // stackoverflow.com/questions/1145850/
    var body = doc.body, html = doc.documentElement;
    var height = Math.max( body.scrollHeight, body.offsetHeight,
    html.clientHeight, html.scrollHeight, html.offsetHeight );
    return height;
}

function setIframeHeight(id) {
    var ifrm = document.getElementById(id);
    var doc = ifrm.contentDocument? ifrm.contentDocument:
    ifrm.contentWindow.document;
    ifrm.style.visibility = 'hidden';
    ifrm.style.height = "10px"; // reset to minimal height ...
    // IE opt. for bing/msn needs a bit added or scrollbar appears
    //ifrm.style.height = getDocHeight( doc ) + 4 + 30 + "px";
    ifrm.style.height = getDocHeight( doc ) + 4 + "px";
    ifrm.style.visibility = 'visible';
}