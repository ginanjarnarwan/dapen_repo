var express = require('express')
var perubahan = express.Router()
var db = require('knex')(require('../config/db').mysql)
var moment = require('moment')
var bcrypt = require('bcrypt-nodejs')
var _ = require('underscore')
var _S = require('underscore.string')
var jwt = require('jwt-simple')
var eachAsync = require('each-async')

var authUtil = require('../util/auth')
var checkPhoto = require('../util/check-photo')
var checkDatePerubahan = require('../util/check-date-perubahan')

// perubahan.post('/biodata', (req, res) => {
//     var content = {
//         _ktp: req.body.ktp,
//         _npwp: req.body.npwp,
//         _tgl_lahir: checkDatePerubahan(req.body.tanggal_lahir), //moment(req.body.tgl_lahir, 'DD-MM-YYYY').format('YYYY-MM-DD'),
//         _tgl_meninggal:checkDatePerubahan(req.body.tanggal_meninggal), // moment(req.body.tgl_meninggal, 'DD-MM-YYYY').format('YYYY-MM-DD'),
//         _agama: req.body.agama,
//         _golongan_darah: req.body.golongan_darah
//     }

//     var contentInput = _.pick(content, (val, key, obj) => { return val != null })

//     if (req.body.tanggal_meninggal !== null) {
//       db('trx_biodata')
//       .insert({
//         tgl_meninggal: checkDatePerubahan(req.body.tanggal_meninggal),
//         tbl_pmp: req.body.id_pmp,
//         created_at: new Date(),
//         updated_at: new Date()
//       })
//       .then(() => {
//         res.send({
//           success: true
//         })
//       })
//     } else {
//       db('tbl_pmp')
//       .where('tbl_pmp.ID', req.body.id_pmp)
//       .update(contentInput)
//       .then((cb) => {
//           res.send({
//               success: true,
//               message: cb
//           })
//       })
//       .catch((err) => {
//           res.send({
//               success: false,
//               message: err
//           })
//       })
//     }


// 	console.log(contentInput)
// })

perubahan.post('/biodata', (req, res) => {

    db('trx_biodata')
    .insert({
      kode_laporan: req.body.kode_laporan,
      tbl_pmp: req.body.id_pmp,
      data_perubahan: req.body.data_perubahan,
      report_at: new Date(),
      report_by: decoded._username
    })
    .then(() => {
      res.send({
        success: true
      })
    })
    .catch((err) => {
      console.log(err)
      res.send({
          success: false,
          message: err
      })
    })



})

perubahan.post('/biodata-mobile', (req, res) => {
  let decoded = jwt.decode(req.body.token, 'ilhamgantengpisan')
  let ktp = null
  let npwp = null
  let golonganDarah = null
  let agama = null
  let gender = null
  let tglMeninggal = null
  let dataProcess = []


  if (req.body.ktp) {
    ktp = {
      kode_laporan: 6002,
      tbl_pmp: req.body.id_pmp,
      data_perubahan: req.body.ktp,
      report_at: new Date(),
      report_by: decoded.username
    }
  }

  if (req.body.npwp) {
    npwp = {
      tbl_pmp: req.body.id_pmp,
      kode_laporan: 6003,
      data_perubahan: req.body.npwp,
      report_at: new Date(),
			report_by: decoded.username
    }
  }

  if (req.body.golonganDarah) {
    golonganDarah = {
      tbl_pmp: req.body.id_pmp,
      kode_laporan: 6004,
      data_perubahan: req.body.golonganDarah,
      report_at: new Date(),
			report_by: decoded.username
    }
  }

  if (req.body.agama) {
    agama = {
      tbl_pmp: req.body.id_pmp,
      kode_laporan: 6005,
      data_perubahan: req.body.agama,
      report_at: new Date(),
			report_by: decoded.username
    }
  }

  if (req.body.gender) {
    gender = {
      tbl_pmp: req.body.id_pmp,
      kode_laporan: 6006,
      data_perubahan: req.body.gender,
      report_at: new Date(),
			report_by: decoded.username
    }
  }

  if (req.body.tglMeninggal) {
    tglMeninggal = {
      tbl_pmp: req.body.id_pmp,
      kode_laporan: 6007,
      data_perubahan: req.body.tglMeninggal,
      report_at: new Date(),
			report_by: decoded.username
    }
  }

  dataProcess = [ktp, npwp, golonganDarah, agama, gender, tglMeninggal]

  db('trx_biodata')
  .insert(dataProcess)
  .then(() => {
    res.send({
      success: true
    })
  })
  .catch((err) => {
    res.send({
        success: false,
        message: err
    })
  })

})

// perubahan.post('/alamat', (req, res) => {
//     var content = {
//         _jalan: req.body.jalan,
//         _komp: req.body.komp,
//         _blok: req.body.blok,
//         _no: req.body.no,
//         _rt: req.body.rt,
//         _rw: req.body.rw,
//         tbl_kelurahan_ID: req.body.kelurahan
//     }
//     var contentInput = _.pick(content, (val, key, obj) => { return val != null })

//     db('tbl_alamat')
//     .where('tbl_alamat.ID', req.body.id_alamat)
//     .update(contentInput)
//     .then((cb) => {
//         res.send({
//             success: true,
//             message: cb
//         })
//     })
//     .catch((err) => {
//         res.send({
//             success: false,
//             message: err
//         })
//     })
// })
perubahan.post('/alamat', (req, res) => {
    var decoded = jwt.decode(req.body.token, 'ilhamgantengpisan')
    var content = {
            id_alamat: req.body.id_alamat,
            _jalan: req.body.jalan,
            _komp: req.body.komp,
            _blok: req.body.blok,
            _no: req.body.no,
            _rt: req.body.rt,
            _rw: req.body.rw,
            tbl_pmp: Number(req.body.id_pmp),
            tbl_kelurahan_ID: req.body.kelurahan_id,
            _default: Number(req.body.default),
            _no_urut_alamat: Number(req.body._no_urut_alamat),
            reported_at: new Date(),
            reported_by: decoded._username,
            kode_laporan: req.body.kode
        }

    if (req.body.kode == 7003) {
        db('tbl_alamat')
        .where('tbl_pmp', req.body.id_pmp)
        .map((row) => {
            return {
                id_alamat: row.ID,
                _jalan: row._jalan,
                _komp: row._komp,
                _blok: row._blok,
                _no: row._no,
                _rt: row._rt,
                _rw: row._rw,
                tbl_pmp: row.tbl_pmp,
                tbl_kelurahan_ID: row.tbl_kelurahan_ID,
                _default: row._default,
                _no_urut_alamat: Number(row._no_urut_alamat),
                reported_at: new Date(),
                reported_by: decoded._username,
                kode_laporan: req.body.kode
            }
        })
        .then((dataAlamat) => {
            var yes = _.where(dataAlamat, { id_alamat: req.body.id_alamat })
            yes[0]._default = 1
            var no = _.reject(dataAlamat, { id_alamat: req.body.id_alamat })
            for ( i = 0 ; i < no.length ; i++) {
                no[i]._default = 0
            }
            var mergeResult = yes.concat(no)
            eachAsync(mergeResult, (item, index, done) => {
                db('trx_alamat')
                .insert(item)
                .then(() => {
                    res.send({
                        success: true
                    })
                })
            })
        })
    } else if (req.body.kode == 7002) {
        db.select(
            'a._no_urut_alamat'
        )
        .from('tbl_alamat as a')
        .where('a.tbl_pmp', req.body.id_pmp)
        .then(function(data){
            var noUrutAlamat = ( Number(_.values(_.max(data, function(o){ return o._no_urut_alamat })).toString()) );
            db('trx_alamat')
            .insert({
                id_alamat: req.body.id_alamat,
                _jalan: req.body.jalan,
                _komp: req.body.komp,
                _blok: req.body.blok,
                _no: req.body.no,
                _rt: req.body.rt,
                _rw: req.body.rw,
                tbl_pmp: Number(req.body.id_pmp),
                tbl_kelurahan_ID: req.body.kelurahan_id,
                _default: Number(req.body.default),
                _no_urut_alamat: noUrutAlamat + 1,
                reported_at: new Date(),
                reported_by: decoded._username,
                kode_laporan: req.body.kode
            })
            .then(() => {
                res.send({
                    success: true
                })
            })
        })
    } else  {
        // console.log(content)
        db('trx_alamat')
        .insert(content)
        .then(() => {
            res.send({
                success: true
            })
        })
        .catch((err) => console.log(err))

    }

})

perubahan.post('/kontak', (req, res) => {
    var decoded = jwt.decode(req.body.token, 'ilhamgantengpisan')
    db('trx_kontak')
    .insert({
        id_kontak: req.body.id_kontak,
        kontak_value: req.body.kontak_value,
        ref_tipe_kontak: req.body.tipe,
        tbl_alamat: req.body.id_alamat,
        no_urut_kontak: req.body.no_urut_kontak,
        reported_at: new Date(),
        reported_by: decoded._username
    })
    .then(() => {
        res.send({
            success: true
        })
    })
    .catch((err) => console.log(err))

})

perubahan.post('/test', (req, res) => {
    res.send(req.body)
    console.log(req.body)
})

module.exports = perubahan
