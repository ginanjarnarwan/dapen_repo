const express = require('express');
const knex = require('knex');
const konfigurasi = express.Router();
const db = knex(require('../config/db').mysql);
const moment = require('moment');
const jwt = require('jwt-simple');

const check_token_body = (req, res) => {
	const token = req.body.token;
	if (!token) {
		res.sendStatus(401);
		return
	}
	return jwt.decode(token, 'ilhamgantengpisan', null, null)
};

konfigurasi.get('/mobile', (req, res) => {
	// const token_data = check_token_body(req, res);
	
	db.select('*').from('tbl_konfigurasi as cfg')
		.where('cfg.category', 'MOBILE_CONFIG')
		.then((rows) => {
			res.send({
				success: true,
				rows: rows,
			})
		})
	
});

module.exports = konfigurasi;