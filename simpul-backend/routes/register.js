var express = require('express')
var register = express.Router()
var db = require('knex')(require('../config/db').mysql)
var moment = require('moment')
var bcrypt = require('bcrypt-nodejs')
var _ = require('underscore')
var jwt = require('jwt-simple')

var authUtil = require('../util/auth')

register.post('/', (req, res) => {
	// res.send(req.body.nik)
  db.select()
	.from('tbl_pmp')
	.where('_nik', req.body.nik)
	.whereBetween('_tgl_lahir', [moment(req.body.tgl_lahir, 'DD-MM-YYYY').subtract(1,'days').format('YYYY-MM-DDT00:00:00'), moment(req.body.tgl_lahir, 'DD-MM-YYYY').format('YYYY-MM-DDT23:59:59')])
	.then((dataPMP) => {

		if (dataPMP.length === 0) {

			res.send({
				success: false,
				msg: 'NIK tidak terdaftar atau tanggal lahir tidak sesuai'
			})
		} else {
			db.select('_kontak_value')
			.from('tbl_kontak')
			.whereRaw('ID LIKE ?',  [dataPMP[0]._naw + '.' + dataPMP[0]._nik + '%' ])
			.then((dataKontak) => {
				var arrKontak = _.pluck(dataKontak, '_kontak_value');
				if(arrKontak.indexOf(req.body.no_hp) > -1) {
					// PC ATAU PMP

					db.select()
					.from('tbl_user')
					.where('_username', req.body.nik)
					.then((dataUserSimpul) => {
						// ---------------
						// USER PMP
						// ----------------
						if (dataUserSimpul.length === 0)  {
							db('tbl_login_mobile')
							.insert({
								username: req.body.nik + '.' + moment(req.body.tgl_lahir, 'DD-MM-YYYY').format('DDMMYYYY'),
								password: bcrypt.hashSync(req.body.password),
								tbl_pmp: dataPMP[0].ID,
								email: req.body.email,
								status: 'PMP'
							})
							.then((dataLoginMobile) => {
								res.send({
									success: true,
									msg: 'berhasil, silahkan login dengan username: ' + req.body.nik + ' dan password: ' +  req.body.password
								})
							}).catch(() => {
								res.send({
									success: false,
									msg: `nik ${ req.body.nik } sudah terdaftar sebgai user`
								})
							})

						// ---------------
						// USER P2TEL
						// ----------------
						} else {
							db('tbl_login_mobile')
							.insert({
								username: req.body.nik + '.' + moment(req.body.tgl_lahir, 'DD-MM-YYYY').format('DDMMYYYY'),
								password: bcrypt.hashSync(req.body.password),
								tbl_pmp: dataPMP[0].ID,
								email: req.body.email,
								status: 'P2TEL'
							})
							.then((dataLoginMobile) => {
								res.send({
									success: true,
									msg: 'berhasil, silahkan login dengan username: ' + req.body.nik + '.' + moment(req.body.tgl_lahir, 'DD-MM-YYYY').format('DDMMYYYY') + ' dan password: ' +  req.body.password
								})
							}).catch(() => {
								res.send({
									success: false,
									msg: `nik ${ req.body.nik } sudah terdaftar sebgai user`
								})
							})
						}
					})
			} else {
				res.send({
					success: false,
					msg: 'nomor handphone tidak terdaftar harap hubungi p2tel setempat'
				})
			}
			})
		}
	})
})

module.exports = register;