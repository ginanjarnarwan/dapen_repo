const express = require('express');
const welcome = express.Router();
const serverConfig = require('../config/server');

welcome.get('/slide', (req, res) => {
	const {ip, port} = serverConfig.production;
  res.send({
    success: true,
    message: '',
    data: [
      {
        // url:'http://139.59.109.145:3000/mobile/welcome_slide/welcome-01.png',
        url:`http://${ip}:${port}/mobile/welcome_slide/welcome-01.png`,
        title: 'sebat',
        backgroundColor: '#fff' 
      },
      {
        // url: 'http://139.59.109.145:3000/mobile/welcome_slide/welcome-02.png',
				url:`http://${ip}:${port}/mobile/welcome_slide/welcome-02.png`,
        title: 'duabats',
        backgroundColor: '#fff'
      },
      {
        // url: 'http://139.59.109.145:3000/mobile/welcome_slide/welcome-03.png',
				url:`http://${ip}:${port}/mobile/welcome_slide/welcome-03.png`,
        title: 'tilubats',
        backgroundColor: '#fff'
      }
    ]
  })
});

welcome.get('/slide-2', (req, res) => {
	const {ip, port} = serverConfig.production;
  res.send({
    success: true,
    message: '',
    data: [
      {
        // url:'http://139.59.109.145:3000/mobile/welcome_slide_2/welcome-01.png',
				url:`http://${ip}:${port}/mobile/welcome_slide_2/welcome-01.png`,
        title: 'something' 
      },
      {
        // url: 'http://139.59.109.145:3000/mobile/welcome_slide_2/welcome-02.png',
				url:`http://${ip}:${port}/mobile/welcome_slide_2/welcome-02.png`,
        title: 'happen'
      }
    ]
  })
});

welcome.get('/dashboardslide', (req, res) => {
	const {ip, port} = serverConfig.production;
  res.send({
    success: true,
    message: '',
    data: [
      {
        // url: 'http://139.59.109.145:3000/mobile/dashboard_slide/image-01.png'
				url:`http://${ip}:${port}/mobile/dashboard_slide/image-01.png`,
      },
      {
        // url: 'http://139.59.109.145:3000/mobile/dashboard_slide/image-02.png'
				url:`http://${ip}:${port}/mobile/dashboard_slide/image-02.png`,
      }
    ]
  })
});

welcome.get('/popup', (req, res) => {
	const {ip, port} = serverConfig.production;
  res.send({
    success: true,
    message: '',
    data: [
      {
        // url: 'http://139.59.109.145:3000/mobile/dashboard_slide/image-03.png'
				url:`http://${ip}:${port}/mobile/dashboard_slide/image-03.png`,
      }
    ]
  })
});

module.exports = welcome;