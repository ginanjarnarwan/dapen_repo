var express = require('express')
var berita = express.Router()
var db = require('knex')(require('../config/db').mysql)
var moment = require('moment')
var bcrypt = require('bcrypt-nodejs')
var _ = require('underscore')
var _S = require('underscore.string')
var jwt = require('jwt-simple')

var checkPhotoBerita = require('../util/check-photo-berita')

berita.post('/list', (req, res) => {
  db('tbl_berita')
  .where('kategori_berita', req.body.kategori)       
  .map((row) => {
    return {
      id: row.id,
      kategori_berita: row.kategori_berita,
      date: moment(row.date).locale('id').format('DD MMMM YYYY'),
      title: row.title,
      content: row.content
//      photo: checkPhotoBerita(row.id)
    }
  })
  .then((data) => {
    res.send({
      success: true,
      data: data
    })
  })            
  .catch((err) => {
    res.send({
      success: false,
      message: err
    })
  })
})

berita.post('/detail', (req, res) => {
  db('tbl_berita')
  .where('id', req.body.id_berita)
  .map((row) => {
    return {
       title: row.title,
       content: row.content,
       date: moment(row.date).format('DD MMMM YYYY'),
       photo: checkPhotoBerita(row.id)
    }
  })   
  .then((data) => {
    res.send({
      success: true,
      data: data
    })
  })
  .catch((err) => {
    res.send({
      success: false,
      message: err
    })
  })
})

module.exports = berita