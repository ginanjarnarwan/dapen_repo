var express = require('express')
var listanggota = express.Router()
var db = require('knex')(require('../config/db').mysql)
var moment = require('moment')
var dateFns = require('date-fns')
var _ = require('underscore')
var _S = require('underscore.string')
var jwt = require('jwt-simple')
var NestHydrationJS = require('nesthydrationjs')()

var datulConfirmCheck = require('../util/datul-confirm-check')
var checkPhoto = require('../util/check-photo')
var checkStatusDatul = require('../util/check-status-datul')
var rowVariant = require('../util/row-variant')
// var datulConfig = require('../')

listanggota.post('/', (req, res) => {
	// const body = req.body
  // const token = req.body.token
  // if (!token) {
  //   res.sendStatus(401)
  //   return
  // }
	// var token = jwt.decode(req.body.token, 'ilhamgantengpisan')
	var trDatul     =    db.select(db.raw('ID, max(_tgl_datul) AS _tgl_datul, tbl_pmp'))
                      .from('tbl_transaksi_datul').groupBy('tbl_pmp').as('transaksi');
  var trDatulaw0  =    db.select(db.raw('ID, max(_tgl_datul) AS _tgl_datul, tbl_pmp'))
                      .from('tbl_transaksi_datul_aw0').groupBy('tbl_pmp').as('transaksiAW0');
  var trDatulM    =    db.select(db.raw('ID, max(_tgl_datul) AS _tgl_datul, tbl_pmp'))
                      .from('tbl_transaksi_datul_mandiri').groupBy('tbl_pmp').as('transaksiM');
  var ahliWaris   =   db.select(db.raw('_nik, count(_nik) AS jml_ahli_waris'))
                      .from('tbl_calon_pmp')
                      .whereNull('_tgl_meninggal')
                      .groupBy('_nik').as('tbl_calon_pmp')

	db.select(
		'pmp.ID as _ID',
    'pmp._naw as _naw',
    'pmp._nik as _nik',
    'tipe._relation as _type',
    'pmp._nama as _nama',
    'pmp._flag_hak as _flag_hak',
    // 'peserta._nama as _namaPeserta',
    'pmp._tgl_meninggal as _tgl_meninggal',
    'pmp._tgl_pensiun as _tgl_pensiun',
    'p2tel._nama_cabang as _namaMitra',
    'transaksi._tgl_datul as _tgl_datul',
    'transaksiAW0._tgl_datul as _tgl_datul_aw0',
    'transaksiM._tgl_datul as _tgl_datul_m'
	).from('tbl_pmp as pmp')
	.leftJoin('tbl_alamat as alamat', 'pmp.ID', 'alamat.tbl_pmp')
	.leftJoin('tbl_kelurahan as kelurahan', 'alamat.tbl_kelurahan_ID', 'kelurahan.ID')
	.leftJoin('ref_tipe_ahliwaris as tipe','pmp.ref_tipe_ahliwaris', 'tipe.ID')
  .leftJoin(trDatul, 'transaksi.tbl_pmp', 'pmp.ID')
  .leftJoin(trDatulaw0, 'transaksiAW0.tbl_pmp', 'pmp.ID')
  .leftJoin(trDatulM, 'transaksiM.tbl_pmp', 'pmp.ID')
  .leftJoin('tbl_cabang_p2tel as p2tel', 'kelurahan._tbl_cabang_p2tel', 'p2tel._kode_p2tel')
	// .where('kelurahan._tbl_cabang_p2tel', req.body.cabang_p2tel)
	.where(function() {
		if (req.body.cabang_p2tel === null) {

		} else {
			this.where('p2tel._kode_p2tel', req.body.cabang_p2tel)
		}
	})
	.where('alamat._default', 1)
	.map((row) => {
		return {
			id: row._ID,
			nama: row._nama,
			nik: row._nik,
			photo: checkPhoto(row._ID),
			status: checkStatusDatul(row._flag_hak, row._tgl_meninggal, row.tgl_pensiun, row._tgl_datul, row._tgl_datul_aw0, row._tgl_datul_m),
			tanggal_meninggal: row._tgl_meninggal,
			tanggal_pensiun: row._tgl_pensiun,
			tanggal_datul: moment(row._tgl_datul).format("D MMMM YYYY"),
			tanggal_datul_aw0: moment(row._tgl_datul_aw0).format("D MMMM YYYY"),
			tanggal_datul_m: moment(row._tgl_datul_m).format("D MMMM YYYY")
		}
	})
	.then((data) => {

		if (req.body.status === null) {
			res.send({
				success: true,
				message: '',
				data: data
			})
		} else {
			res.send({
				success: true,
	      message: '',
				data: _.where(data, {  status: Number(req.body.status) })

			})
		}

	}).catch((err) => {
    res.send({
      success: false,
      message: err
    })
  })
})


// engke hapus nya
listanggota.post('/web', (req, res) => {
	const body = req.body
  const token = req.body.token
  if (!token) {
    res.sendStatus(401)
    return
  }
	const token_data = jwt.decode(token, 'ilhamgantengpisan', null, null)

	var trDatul     =    db.select(db.raw('ID, max(_tgl_datul) AS _tgl_datul, tbl_pmp'))
                      .from('tbl_transaksi_datul').groupBy('tbl_pmp').as('transaksi');
  var trDatulaw0  =    db.select(db.raw('ID, max(_tgl_datul) AS _tgl_datul, tbl_pmp'))
                      .from('tbl_transaksi_datul_aw0').groupBy('tbl_pmp').as('transaksiAW0');
  var trDatulM    =    db.select(db.raw('ID, max(_tgl_datul) AS _tgl_datul, tbl_pmp'))
                      .from('tbl_transaksi_datul_mandiri').groupBy('tbl_pmp').as('transaksiM');
  var ahliWaris   =   db.select(db.raw('_nik, count(_nik) AS jml_ahli_waris'))
                      .from('tbl_calon_pmp')
                      .whereNull('_tgl_meninggal')
                      .groupBy('_nik').as('tbl_calon_pmp')

	db.select(
		'pmp.ID as _ID',
    'pmp._naw as _naw',
    'pmp._nik as _nik',
    'tipe._relation as _type',
    'pmp._nama as _nama',
    'pmp._flag_hak as _flag_hak',
    // 'peserta._nama as _namaPeserta',
    'pmp._tgl_meninggal as _tgl_meninggal',
    'pmp._tgl_pensiun as _tgl_pensiun',
    'p2tel._nama_cabang as _namaMitra',
    'transaksi._tgl_datul as _tgl_datul',
    'transaksiAW0._tgl_datul as _tgl_datul_aw0',
    'transaksiM._tgl_datul as _tgl_datul_m'
	).from('tbl_pmp as pmp')
	.leftJoin('tbl_alamat as alamat', 'pmp.ID', 'alamat.tbl_pmp')
	.leftJoin('tbl_kelurahan as kelurahan', 'alamat.tbl_kelurahan_ID', 'kelurahan.ID')
	.leftJoin('ref_tipe_ahliwaris as tipe','pmp.ref_tipe_ahliwaris', 'tipe.ID')
  .leftJoin(trDatul, 'transaksi.tbl_pmp', 'pmp.ID')
  .leftJoin(trDatulaw0, 'transaksiAW0.tbl_pmp', 'pmp.ID')
  .leftJoin(trDatulM, 'transaksiM.tbl_pmp', 'pmp.ID')
  .leftJoin('tbl_cabang_p2tel as p2tel', 'kelurahan._tbl_cabang_p2tel', 'p2tel._kode_p2tel')
	.where('kelurahan._tbl_cabang_p2tel', req.body.cabang_p2tel)
	.where('alamat._default', 1)
	.map((row) => {
		return {
			id: row._ID,
			nama: row._nama,
			nik: row._nik,
			photo: checkPhoto(row._ID),
			status: checkStatusDatul(row._flag_hak, row._tgl_meninggal, row.tgl_pensiun, row._tgl_datul, row._tgl_datul_aw0, row._tgl_datul_m),
			tanggal_meninggal: row._tgl_meninggal,
			tanggal_pensiun: row._tgl_pensiun,
			tanggal_datul: moment(row._tgl_datul).format("D MMMM YYYY"),
			tanggal_datul_aw0: moment(row._tgl_datul_aw0).format("D MMMM YYYY"),
			tanggal_datul_m: moment(row._tgl_datul_m).format("D MMMM YYYY")
		}
	})
	.then((data) => {
		let dataSend
		if (token_data.ref_user_level === 5) {
			dataSend = _.map(data, (obj) => {
				if (obj.status === 2) {
					obj.status = 1
				}
				return obj
			})
		} else {
			dataSend = data
		}
		if (req.body.status === null) {
			res.send({
				success: true,
				message: '',
				data: dataSend
			})
		} else {
			res.send({
				success: true,
	      message: '',
				data: _.where(dataSend, {  status: Number(req.body.status) })

			})
		}




	}).catch((err) => {
    res.send({
      success: false,
      message: err
    })
  })
})

listanggota.post('/dapentel', (req, res) => {
	// var token = jwt.decode(req.body.token, 'ilhamgantengpisan')
	var trDatul     =    db.select(db.raw('ID, max(_tgl_datul) AS _tgl_datul, tbl_pmp'))
                      .from('tbl_transaksi_datul').groupBy('tbl_pmp').as('transaksi');
  var trDatulaw0  =    db.select(db.raw('ID, max(_tgl_datul) AS _tgl_datul, tbl_pmp'))
                      .from('tbl_transaksi_datul_aw0').groupBy('tbl_pmp').as('transaksiAW0');
  var trDatulM    =    db.select(db.raw('ID, max(_tgl_datul) AS _tgl_datul, tbl_pmp'))
                      .from('tbl_transaksi_datul_mandiri').groupBy('tbl_pmp').as('transaksiM');
  var ahliWaris   =   db.select(db.raw('_nik, count(_nik) AS jml_ahli_waris'))
                      .from('tbl_calon_pmp')
                      .whereNull('_tgl_meninggal')
                      .groupBy('_nik').as('tbl_calon_pmp')

	db.select(
		'pmp.ID as _ID',
    'pmp._naw as _naw',
    'pmp._nik as _nik',
    'tipe._relation as _type',
    'pmp._nama as _nama',
    'pmp._flag_hak as _flag_hak',
    // 'peserta._nama as _namaPeserta',
    'pmp._tgl_meninggal as _tgl_meninggal',
    'pmp._tgl_pensiun as _tgl_pensiun',
    'p2tel._kode_p2tel as _kodeP2tel',
    'p2tel._nama_cabang as _namaMitra',
    'transaksi._tgl_datul as _tgl_datul',
    'transaksiAW0._tgl_datul as _tgl_datul_aw0',
    'transaksiM._tgl_datul as _tgl_datul_m'
	).from('tbl_pmp as pmp')
	.leftJoin('tbl_alamat as alamat', 'pmp.ID', 'alamat.tbl_pmp')
	.leftJoin('tbl_kelurahan as kelurahan', 'alamat.tbl_kelurahan_ID', 'kelurahan.ID')
	.leftJoin('ref_tipe_ahliwaris as tipe','pmp.ref_tipe_ahliwaris', 'tipe.ID')
  .leftJoin(trDatul, 'transaksi.tbl_pmp', 'pmp.ID')
  .leftJoin(trDatulaw0, 'transaksiAW0.tbl_pmp', 'pmp.ID')
  .leftJoin(trDatulM, 'transaksiM.tbl_pmp', 'pmp.ID')
  .leftJoin('tbl_cabang_p2tel as p2tel', 'kelurahan._tbl_cabang_p2tel', 'p2tel._kode_p2tel')
	.where('alamat._default', 1)
	.map((row) => {
		return {
			id: row._ID,
			nama: row._nama,
			nik: row._nik,
			photo: checkPhoto(row._ID),
			status: checkStatusDatul(row._flag_hak, row._tgl_meninggal, row._tgl_pensiun, row._tgl_datul, row._tgl_datul_aw0, row._tgl_datul_m),
			tanggal_meninggal: row._tgl_meninggal,
			tanggal_pensiun: row._tgl_pensiun,
			tanggal_datul: moment(row._tgl_datul).format("D MMMM YYYY"),
			tanggal_datul_aw0: moment(row._tgl_datul_aw0).format("D MMMM YYYY"),
			tanggal_datul_m: moment(row._tgl_datul_m).format("D MMMM YYYY"),
      kode_p2tel: row._kodeP2tel,
      nama_cabang: row._namaMitra
		}
	})
	.then((data) => {

		if (req.body.status === null) {
			res.send({
				success: true,
				message: '',
				data: data
			})
		} else {
			res.send({
				success: true,
	      message: '',
				data: _.where(data, {  status: Number(req.body.status) })

			})
		}




	}).catch((err) => {
    res.send({
      success: false,
      message: err
    })
  })
})

listanggota.post('/anggota', (req, res) => {
	var token = jwt.decode(req.body.token, 'ilhamgantengpisan')
	var trDatul     	=	db.select(db.raw('ID,  _tgl_datul, tbl_pmp'))
						.from('tbl_transaksi_datul').as('transaksi');
	var trDatulaw0  	=	db.select(db.raw('ID, _tgl_datul, tbl_pmp'))
						.from('tbl_transaksi_datul_aw0').as('transaksiAW0');
	var trDatulM    	=	db.select(db.raw('ID, _tgl_datul, tbl_pmp'))
						.from('tbl_transaksi_datul_mandiri').as('transaksiM');
	var ahliWaris   	=	db.select(db.raw('_nik, count(_nik) AS jml_ahli_waris'))
						.from('tbl_calon_pmp')
						.whereNull('_tgl_meninggal')
						.groupBy('_nik').as('tbl_calon_pmp')

	db.select(
		'pmp.ID as _ID',
		'pmp._naw as _naw',
		'pmp._nik as _nik',
		'tipe._relation as _type',
		'pmp._nama as _nama',
		'pmp._flag_hak as _flaghak',
		// 'peserta._nama as _namaPeserta',
		'pmp._tgl_meninggal as _tglmeninggal',
		'pmp._tgl_pensiun as _tglpensiun',
		'p2tel._nama_cabang as _namaMitra',
		'transaksi._tgl_datul as _tgldatul',
		'transaksiAW0._tgl_datul as _tgldatulaw0',
		'transaksiM._tgl_datul as _tgldatulm',
		'aw.ID as _ahliwaris__ID',
		'aw._nama as _ahliwaris__nama',
		'aw._nik as _ahliwaris__nik',
		'aw._tgl_meninggal as _ahliwaris__tglMeninggal',
		'aw.ref_tipe_ahliwaris as _ahliwaris__tipe',
		'aw._tgl_menikah_lagi as _ahliwaris_tglMenikahLagi',
		'aw._tgl_lahir as _ahliwaris__tglLahir',
		'aw._tgl_lulus as _ahliwaris__tglLulus',
		'aw._tgl_cerai as _ahliwaris__tglCerai'
	).from('tbl_pmp as pmp')
		.leftJoin('tbl_alamat as alamat', 'pmp.ID', 'alamat.tbl_pmp')
		.leftJoin('tbl_calon_pmp as aw', 'aw._nik', 'pmp._nik')
		.leftJoin('tbl_kelurahan as kelurahan', 'alamat.tbl_kelurahan_ID', 'kelurahan.ID')
		.leftJoin('ref_tipe_ahliwaris as tipe','pmp.ref_tipe_ahliwaris', 'tipe.ID')
		.leftJoin(trDatul, 'transaksi.tbl_pmp', 'pmp.ID')
		.leftJoin(trDatulaw0, 'transaksiAW0.tbl_pmp', 'pmp.ID')
		.leftJoin(trDatulM, 'transaksiM.tbl_pmp', 'pmp.ID')
		.leftJoin('tbl_cabang_p2tel as p2tel', 'kelurahan._tbl_cabang_p2tel', 'p2tel._kode_p2tel')
		// .limit(1000)
		//.where('kelurahan._tbl_cabang_p2tel', req.body.cabang_p2tel)
		.modify(function(qB){
		if(token.tbl_cabang_p2tel !== null) {
			qB.where('kelurahan._tbl_cabang_p2tel', token.tbl_cabang_p2tel )
		}
	})
	.where('alamat._default', 1)
	.then(NestHydrationJS.nest)
	.map((row) => {
		var jumlah_ahliwaris = _.map(_.reject(row.ahliwaris, function(o) {
								return ( o.tglMeninggal != null || (o.tipe == 4 && dateFns.differenceInDays(new Date(), o.tglLahir) > 9131) ||o.tglLulus != null || o.tglMenikahLagi != null || o.tglCerai != null )
							}), function(o){
								return  {
									id : `${ o._naw }${ o._nik }`,
									nama: o._nama
								}
							})
		return {
			id: row.ID,
			nama: row.nama,
			nik: row.nik,
			photo: checkPhoto(row.ID),
			cabang_p2tel: row.namaMitra,
			status: checkStatusDatul(row.flaghak, row.tglmeninggal, row.tglpensiun, row.tgldatul, row.tgldatulaw0, row.tgldatulm),
			tanggal_meninggal: row.tglmeninggal,
			tanggal_pensiun: row.tglpensiun,
			tanggal_datul: moment(row.tgldatul).format("D MMMM YYYY"),
			tanggal_datul_aw0: moment(row.tgldatulaw0).format("D MMMM YYYY"),
			tanggal_datul_m: moment(row.tgldatulm).format("D MMMM YYYY"),
			jumlah_ahliwaris: jumlah_ahliwaris,
			_rowVariant: rowVariant(jumlah_ahliwaris.length - 1)
		}
	})
	.then((data) => {
		let dataSend
		if (token.ref_user_level === 5) {
			dataSend = _.map(data, (obj) => {
				if (obj.status === 2) {
					obj.status = 1
				}
				return obj
			})
		} else {
			dataSend = data
		}
		if (req.body.status === null) {
			res.send({
				success: true,
				message: '',
				data: dataSend,
				total_row: dataSend.length
			})
		} else {
			res.send({
				success: true,
				message: '',
				data: _.where(dataSend, {  status: Number(req.body.status) }),
				total_row: _.where(dataSend, {  status: Number(req.body.status) }).length
			})
		}




	}).catch((err) => {
    res.send({
      success: false,
      message: err
    })
  })
})
module.exports = listanggota
