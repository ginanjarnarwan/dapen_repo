var express = require('express')
var api = express.Router()
var db = require('knex')(require('../config/db').mysql)
var moment = require('moment')
var dateFns = require('date-fns')
var _ = require('underscore')
var _S = require('underscore.string')
var jwt = require('jwt-simple')
var NestHydrationJS = require('nesthydrationjs')()

var datulConfirmCheck = require('../util/datul-confirm-check')
var checkPhoto = require('../util/check-photo')
var checkStatusDatul = require('../util/check-status-datul')
var rowVariant = require('../util/row-variant')


// api.post('/', function(req, res){
//   let token = jwt.decode(req.body.token, 'ilhamgantengpisan')
//   let trDatul     =    db.select(db.raw('ID, max(_tgl_datul) AS _tgl_datul, tbl_pmp'))
//                         .from('tbl_transaksi_datul').groupBy('tbl_pmp').as('transaksi');
//   let trDatulaw0  =    db.select(db.raw('ID, max(_tgl_datul) AS _tgl_datul, tbl_pmp'))
//                       .from('tbl_transaksi_datul_aw0').groupBy('tbl_pmp').as('transaksiAW0');
//   let trDatulM    =    db.select(db.raw('ID, max(_tgl_datul) AS _tgl_datul, tbl_pmp'))
//                       .from('tbl_transaksi_datul_mandiri').groupBy('tbl_pmp').as('transaksiM');
//   let ahliWaris   =   db.select(db.raw('_nik, count(_nik) AS jml_ahli_waris'))
//                         .from('tbl_calon_pmp').groupBy('_nik').as('tbl_calon_pmp')
// 	db.select(
// 		'aw.ID as id',
// 		'aw._nik as nik',
// 		'aw._naw as naw',
// 		'aw._nama as nama',
// 		'aw._flag_hak as flaghak',
// 		'aw._tgl_meninggal as dod',
// 		'aw._tgl_pensiun as dor',
// 		'awt._type_name as typename',
// 		'mt._nama_cabang as namamitra',
// 		'tr._tgl_datul as tgldatul',
//     'transaksiAW0._tgl_datul as _tgl_datul_aw0',
//     'transaksiM._tgl_datul as _tgl_datul_m'
// 	)
// 	.from('tbl_pmp AS aw')
// 	.leftJoin('tbl_alamat AS al', 'aw.ID','al.tbl_pmp')
//   .leftJoin(trDatul, 'transaksi.tbl_pmp', 'aw.ID')
//   .leftJoin(ahliWaris, 'tbl_calon_pmp._nik', 'aw._nik')
//   .leftJoin(trDatulaw0, 'transaksiAW0.tbl_pmp', 'aw.ID')
//   .leftJoin(trDatulM, 'transaksiM.tbl_pmp', 'aw.ID')
//   .leftJoin('ref_tipe_ahliwaris AS awt','aw.ref_tipe_ahliwaris', 'awt.ID')
//   .leftJoin('tbl_transaksi_datul AS tr','aw.ID','tr.tbl_pmp')
//   .leftJoin('tbl_kelurahan AS kl', 'al.tbl_kelurahan_ID','kl.ID')
//   .leftJoin('tbl_cabang_p2tel AS mt', 'kl._tbl_cabang_p2tel', 'mt._kode_p2tel')
//   .where('al._default', 1)
//   .where(function(){
//     if(req.body.kode_cabang === null || req.body.kode_cabang === '') {
//
//     } else {
//       this.where('mt._kode_p2tel', req.body.kode_cabang)
//     }
//   })
//   .where(function(){
//     if(req.body.search === null || req.body.search === '') {
//
//     } else {
//       this.where('aw._nik', '=' ,req.body.search)
//     }
//   })
//   .orderBy('nik','asc')
//   .map((row) => {
//     var jumlah_ahliwaris = _.map(_.reject(row.ahliwaris, function(o) {
// 			return ( o.tglMeninggal != null || (o.tipe == 4 && dateFns.differenceInDays(new Date(), o.tglLahir) > 9131) ||o.tglLulus != null || o.tglMenikahLagi != null || o.tglCerai != null )
// 		}), function(o){
// 			return  {
// 				id : `${ o._naw }${ o._nik }`,
// 				nama: o._nama
// 			}
// 		})
//     return {
//       id: row.id,
//       nama: row.nama,
//       nik: row.nik,
//       photo: checkPhoto(row.id),
//       cabang_p2tel: row.namamitra,
//       status: checkStatusDatul(row.flaghak, row.dod, row.dor, row.tgldatul, row._tgl_datul_aw0, row._tgl_datul_m),
//       tanggal_meninggal: row.dod,
//       tanggal_pensiun: row.dor,
//       tanggal_datul: moment(row._tgl_datul).format("D MMMM YYYY"),
//       tanggal_datul_aw0: moment(row._tgl_datul_aw0).format("D MMMM YYYY"),
//       tanggal_datul_m: moment(row._tgl_datul_m).format("D MMMM YYYY")
//     }
//   })
//
//     .then(function(data){
//     	if(_.isEmpty(data)) {
//         res.send({
//           success: true,
//           data: data,
//           message: 'Data tidak ditemukan'
//         })
//     	} else {
//         res.send({
//           success: true,
//           data: data
//         })
//     	}
//
//
//     })
//     .catch((error) => {
//     	res.send({
//         success: false,
//         message: error
//       })
//     })
// })

api.post('/', (req, res) => {
	var token = jwt.decode(req.body.token, 'ilhamgantengpisan')
	var trDatul     	=	db.select(db.raw('ID,  _tgl_datul, tbl_pmp'))
						.from('tbl_transaksi_datul').as('transaksi');
	var trDatulaw0  	=	db.select(db.raw('ID, _tgl_datul, tbl_pmp'))
						.from('tbl_transaksi_datul_aw0').as('transaksiAW0');
	var trDatulM    	=	db.select(db.raw('ID, _tgl_datul, tbl_pmp'))
						.from('tbl_transaksi_datul_mandiri').as('transaksiM');
	var ahliWaris   	=	db.select(db.raw('_nik, count(_nik) AS jml_ahli_waris'))
						.from('tbl_calon_pmp')
						.whereNull('_tgl_meninggal')
						.groupBy('_nik').as('tbl_calon_pmp')

	db.select(
		'pmp.ID as _ID',
		'pmp._naw as _naw',
		'pmp._nik as _nik',
		'tipe._relation as _type',
		'pmp._nama as _nama',
		'pmp._flag_hak as _flaghak',
		'pmp._tgl_meninggal as _tglmeninggal',
		'pmp._tgl_pensiun as _tglpensiun',
		'p2tel._nama_cabang as _namaMitra',
		'transaksi._tgl_datul as _tgldatul',
		'transaksiAW0._tgl_datul as _tgldatulaw0',
		'transaksiM._tgl_datul as _tgldatulm',
		'aw.ID as _ahliwaris__ID',
		'aw._nama as _ahliwaris__nama',
		'aw._nik as _ahliwaris__nik',
		'aw._tgl_meninggal as _ahliwaris__tglMeninggal',
		'aw.ref_tipe_ahliwaris as _ahliwaris__tipe',
		'aw._tgl_menikah_lagi as _ahliwaris_tglMenikahLagi',
		'aw._tgl_lahir as _ahliwaris__tglLahir',
		'aw._tgl_lulus as _ahliwaris__tglLulus',
		'aw._tgl_cerai as _ahliwaris__tglCerai'
	).from('tbl_pmp as pmp')
		.leftJoin('tbl_alamat as alamat', 'pmp.ID', 'alamat.tbl_pmp')
		.leftJoin('tbl_calon_pmp as aw', 'aw._nik', 'pmp._nik')
		.leftJoin('tbl_kelurahan as kelurahan', 'alamat.tbl_kelurahan_ID', 'kelurahan.ID')
		.leftJoin('ref_tipe_ahliwaris as tipe','pmp.ref_tipe_ahliwaris', 'tipe.ID')
		.leftJoin(trDatul, 'transaksi.tbl_pmp', 'pmp.ID')
		.leftJoin(trDatulaw0, 'transaksiAW0.tbl_pmp', 'pmp.ID')
		.leftJoin(trDatulM, 'transaksiM.tbl_pmp', 'pmp.ID')
		.leftJoin('tbl_cabang_p2tel as p2tel', 'kelurahan._tbl_cabang_p2tel', 'p2tel._kode_p2tel')
	.where('alamat._default', 1)
  .where(function(){
    if(req.body.kode_cabang === null || req.body.kode_cabang === '') {

    } else {
      this.where('p2tel._kode_p2tel', req.body.kode_cabang)
    }
  })
  .where(function(){
    if(req.body.search === null || req.body.search === '') {

    } else {
      this.where('pmp._nik', '=' ,req.body.search)
    }
  })
	.then(NestHydrationJS.nest)
	.map((row) => {
		var jumlah_ahliwaris = _.map(_.reject(row.ahliwaris, function(o) {
								return ( o.tglMeninggal != null || (o.tipe == 4 && dateFns.differenceInDays(new Date(), o.tglLahir) > 9131) ||o.tglLulus != null || o.tglMenikahLagi != null || o.tglCerai != null )
							}), function(o){
								return  {
									id : `${ o._naw }${ o._nik }`,
									nama: o._nama
								}
							})
		return {
			id: row.ID,
			nama: row.nama,
			nik: row.nik,
			photo: checkPhoto(row.ID),
			cabang_p2tel: row.namaMitra,
			status: checkStatusDatul(row.flaghak, row.tglmeninggal, row.tglpensiun, row.tgldatul, row.tgldatulaw0, row.tgldatulm),
			tanggal_meninggal: row.tglmeninggal,
			tanggal_pensiun: row.tglpensiun,
			tanggal_datul: moment(row.tgldatul).format("D MMMM YYYY"),
			tanggal_datul_aw0: moment(row.tgldatulaw0).format("D MMMM YYYY"),
			tanggal_datul_m: moment(row.tgldatulm).format("D MMMM YYYY"),
			jumlah_ahliwaris: jumlah_ahliwaris,
			_rowVariant: rowVariant(_.size(jumlah_ahliwaris) - 1)
		}
	})
	.then((data) => {

    if(_.isEmpty(data)) {
      res.send({
        success: true,
        data: data,
        message: 'Data tidak ditemukan'
      })
    } else {
      res.send({
        success: true,
        data: data
      })
    }


	}).catch((err) => {
    res.send({
      success: false,
      message: 'Data tidak ditemukan'
    })
  })
})
module.exports = api
