var express = require('express')
var dashboarddatul = express.Router()
var db = require('knex')(require('../config/db').mysql)
var moment = require('moment')
var _ = require('underscore')
var _S = require('underscore.string')
var jwt = require('jwt-simple')
var NestHydrationJS = require('nesthydrationjs')()

var datulConfirmCheck = require('../util/datul-confirm-check')
var checkPhoto = require('../util/check-photo')
var checkStatusDatul = require('../util/check-status-datul')

dashboarddatul.post('/', (req, res) => {
	// var token = jwt.decode(req.body.token, 'ilhamgantengpisan')
	var trDatul     =    db.select(db.raw('ID, max(_tgl_datul) AS _tgl_datul, tbl_pmp'))
                      .from('tbl_transaksi_datul').groupBy('tbl_pmp').as('transaksi');
  var trDatulaw0  =    db.select(db.raw('ID, max(_tgl_datul) AS _tgl_datul, tbl_pmp'))
                      .from('tbl_transaksi_datul_aw0').groupBy('tbl_pmp').as('transaksiAW0');
  var trDatulM    =    db.select(db.raw('ID, max(_tgl_datul) AS _tgl_datul, tbl_pmp'))
                      .from('tbl_transaksi_datul_mandiri').groupBy('tbl_pmp').as('transaksiM');
  var ahliWaris   =   db.select(db.raw('_nik, count(_nik) AS jml_ahli_waris'))
                      .from('tbl_calon_pmp')
                      .whereNull('_tgl_meninggal')
                      .groupBy('_nik').as('tbl_calon_pmp')

	db.select(
		'pmp.ID as _ID',
    'pmp._naw as _naw',
    'pmp._nik as _nik',
    'tipe._relation as _type',
    'pmp._nama as _nama',
    'pmp._flag_hak as _flag_hak',
    // 'peserta._nama as _namaPeserta',
    'pmp._tgl_meninggal as _tgl_meninggal',
    'pmp._tgl_pensiun as _tgl_pensiun',
    'p2tel._nama_cabang as _namaMitra',
    'transaksi._tgl_datul as _tgl_datul',
    'transaksiAW0._tgl_datul as _tgl_datul_aw0',
    'transaksiM._tgl_datul as _tgl_datul_m'
	).from('tbl_pmp as pmp')
	.leftJoin('tbl_alamat as alamat', 'pmp.ID', 'alamat.tbl_pmp')
	.leftJoin('tbl_kelurahan as kelurahan', 'alamat.tbl_kelurahan_ID', 'kelurahan.ID')
	.leftJoin('ref_tipe_ahliwaris as tipe','pmp.ref_tipe_ahliwaris', 'tipe.ID')
  .leftJoin(trDatul, 'transaksi.tbl_pmp', 'pmp.ID')
  .leftJoin(trDatulaw0, 'transaksiAW0.tbl_pmp', 'pmp.ID')
  .leftJoin(trDatulM, 'transaksiM.tbl_pmp', 'pmp.ID')
  .leftJoin('tbl_cabang_p2tel as p2tel', 'kelurahan._tbl_cabang_p2tel', 'p2tel._kode_p2tel')
	.where('kelurahan._tbl_cabang_p2tel', req.body.cabang_p2tel)
	.where('alamat._default', 1)
	.map((row) => {
		return {
			id: row._ID,
			nama: row._nama,
			nik: row._nik,
			photo: checkPhoto(row._ID),
			status: checkStatusDatul(row._flag_hak, row._tgl_meninggal, row.tgl_pensiun, row._tgl_datul, row._tgl_datul_aw0, row._tgl_datul_m),
			tgl_meninggal: row._tgl_meninggal,
			tgl_pensiun: row._tgl_pensiun,
			tgl_datul: row._tgl_datul,
			tgl_datul_aw0: row._tgl_datul_aw0,
			tgl_datul_m: row._tgl_datul_m
		}
	})
	.then((data) => {


		res.send({
			success: true,
      message: '',
			data: [
			  {
         label: 'total_anggota',
         number: data.length,
         status: null
        },
				{
          label: 'selesai_datul',
          number: _.where(data, {  status: 1 }).length,
          status: 1
        },
				{
          label: 'konfirmasi_p2tel',
          number: _.where(data, {  status: 3 }).length,
          status: 3
        },
				{
          label: 'konfirmasi_dapen',
          number: _.where(data, {  status: 2 }).length,
          status: 2
        },
				{
          label: 'belum_datul',
          number: _.where(data, {  status: 4 }).length,
          status: 4
        },
				{
          label: 'tidak_wajib_datul',
          number: _.where(data, {  status: 5 }).length,
          status: 5
        }
			]
			// data: data,
			// x: _.where(data, { flag_hak: 0 })
		})


	}).catch((err) => {
    res.send({
      success: false,
      message: err
    })
  })
})

dashboarddatul.post('/dapentel', (req, res) => {
	// var token = jwt.decode(req.body.token, 'ilhamgantengpisan')
	var trDatul     =    db.select(db.raw('ID, max(_tgl_datul) AS _tgl_datul, tbl_pmp'))
                      .from('tbl_transaksi_datul').groupBy('tbl_pmp').as('transaksi');
  var trDatulaw0  =    db.select(db.raw('ID, max(_tgl_datul) AS _tgl_datul, tbl_pmp'))
                      .from('tbl_transaksi_datul_aw0').groupBy('tbl_pmp').as('transaksiAW0');
  var trDatulM    =    db.select(db.raw('ID, max(_tgl_datul) AS _tgl_datul, tbl_pmp'))
                      .from('tbl_transaksi_datul_mandiri').groupBy('tbl_pmp').as('transaksiM');
  var ahliWaris   =   db.select(db.raw('_nik, count(_nik) AS jml_ahli_waris'))
                      .from('tbl_calon_pmp')
                      .whereNull('_tgl_meninggal')
                      .groupBy('_nik').as('tbl_calon_pmp')

	db.select(
		'pmp.ID as _ID',
    'pmp._naw as _naw',
    'pmp._nik as _nik',
    'tipe._relation as _type',
    'pmp._nama as _nama',
    'pmp._flag_hak as _flag_hak',
    // 'peserta._nama as _namaPeserta',
    'pmp._tgl_meninggal as _tgl_meninggal',
    'pmp._tgl_pensiun as _tgl_pensiun',
    'p2tel._kode_p2tel as _kodeP2tel',
    'p2tel._nama_cabang as _namaMitra',
    'transaksi._tgl_datul as _tgl_datul',
    'transaksiAW0._tgl_datul as _tgl_datul_aw0',
    'transaksiM._tgl_datul as _tgl_datul_m'
	).from('tbl_pmp as pmp')
	.leftJoin('tbl_alamat as alamat', 'pmp.ID', 'alamat.tbl_pmp')
	.leftJoin('tbl_kelurahan as kelurahan', 'alamat.tbl_kelurahan_ID', 'kelurahan.ID')
	.leftJoin('ref_tipe_ahliwaris as tipe','pmp.ref_tipe_ahliwaris', 'tipe.ID')
  .leftJoin(trDatul, 'transaksi.tbl_pmp', 'pmp.ID')
  .leftJoin(trDatulaw0, 'transaksiAW0.tbl_pmp', 'pmp.ID')
  .leftJoin(trDatulM, 'transaksiM.tbl_pmp', 'pmp.ID')
  .leftJoin('tbl_cabang_p2tel as p2tel', 'kelurahan._tbl_cabang_p2tel', 'p2tel._kode_p2tel')
	.where('alamat._default', 1)
	.where(function() {
		if (req.body.kode_p2tel === null) {

		} else {
			this.where('p2tel._kode_p2tel', req.body.kode_p2tel)
		}
	})
	.map((row) => {
		return {
			id: row._ID,
			nama: row._nama,
			nik: row._nik,
			photo: checkPhoto(row._ID),
			status: checkStatusDatul(row._flag_hak, row._tgl_meninggal, row.tgl_pensiun, row._tgl_datul, row._tgl_datul_aw0, row._tgl_datul_m),
			tgl_meninggal: row._tgl_meninggal,
			tgl_pensiun: row._tgl_pensiun,
			tgl_datul: row._tgl_datul,
			tgl_datul_aw0: row._tgl_datul_aw0,
			tgl_datul_m: row._tgl_datul_m,
      kode_p2tel: row._kodeP2tel,
      nama_cabang: row._namaMitra
		}
	})
	.then((data) => {


		res.send({
			success: true,
      message: '',
			data: [
			  {
         label: 'total_anggota',
         number: data.length,
         status: null
        },
				{
          label: 'selesai_datul',
          number: _.where(data, {  status: 1 }).length,
          status: 1
        },
				{
          label: 'konfirmasi_p2tel',
          number: _.where(data, {  status: 3 }).length,
          status: 3
        },
				{
          label: 'konfirmasi_dapen',
          number: _.where(data, {  status: 2 }).length,
          status: 2
        },
				{
          label: 'belum_datul',
          number: _.where(data, {  status: 4 }).length,
          status: 4
        },
				{
          label: 'tidak_wajib_datul',
          number: _.where(data, {  status: 5 }).length,
          status: 5
        }
			]
			// data: data,
			// x: _.where(data, { flag_hak: 0 })
		})


	}).catch((err) => {
    res.send({
      success: false,
      message: err
    })
  })
})

dashboarddatul.post('/web', (req, res) => {
	const body = req.body
  const token = req.body.token
  if (!token) {
    res.sendStatus(401)
    return
  }
  const token_data = jwt.decode(token, 'ilhamgantengpisan', null, null)
	var trDatul     =    db.select(db.raw('ID, max(_tgl_datul) AS _tgl_datul, tbl_pmp'))
                      .from('tbl_transaksi_datul').groupBy('tbl_pmp').as('transaksi');
  var trDatulaw0  =    db.select(db.raw('ID, max(_tgl_datul) AS _tgl_datul, tbl_pmp'))
                      .from('tbl_transaksi_datul_aw0').groupBy('tbl_pmp').as('transaksiAW0');
  var trDatulM    =    db.select(db.raw('ID, max(_tgl_datul) AS _tgl_datul, tbl_pmp'))
                      .from('tbl_transaksi_datul_mandiri').groupBy('tbl_pmp').as('transaksiM');
  var ahliWaris   =   db.select(db.raw('_nik, count(_nik) AS jml_ahli_waris'))
                      .from('tbl_calon_pmp')
                      .whereNull('_tgl_meninggal')
                      .groupBy('_nik').as('tbl_calon_pmp')

	db.select(
		'pmp.ID as _ID',
    'pmp._naw as _naw',
    'pmp._nik as _nik',
    'tipe._relation as _type',
    'pmp._nama as _nama',
    'pmp._flag_hak as _flag_hak',
    // 'peserta._nama as _namaPeserta',
    'pmp._tgl_meninggal as _tgl_meninggal',
    'pmp._tgl_pensiun as _tgl_pensiun',
    'p2tel._nama_cabang as _namaMitra',
    'transaksi._tgl_datul as _tgl_datul',
    'transaksiAW0._tgl_datul as _tgl_datul_aw0',
    'transaksiM._tgl_datul as _tgl_datul_m'
	).from('tbl_pmp as pmp')
	.leftJoin('tbl_alamat as alamat', 'pmp.ID', 'alamat.tbl_pmp')
	.leftJoin('tbl_kelurahan as kelurahan', 'alamat.tbl_kelurahan_ID', 'kelurahan.ID')
	.leftJoin('ref_tipe_ahliwaris as tipe','pmp.ref_tipe_ahliwaris', 'tipe.ID')
  .leftJoin(trDatul, 'transaksi.tbl_pmp', 'pmp.ID')
  .leftJoin(trDatulaw0, 'transaksiAW0.tbl_pmp', 'pmp.ID')
  .leftJoin(trDatulM, 'transaksiM.tbl_pmp', 'pmp.ID')
  .leftJoin('tbl_cabang_p2tel as p2tel', 'kelurahan._tbl_cabang_p2tel', 'p2tel._kode_p2tel')
	.where(function() {
		if (token_data.tbl_cabang_p2tel !== null) {
			this.where('kelurahan._tbl_cabang_p2tel', token_data.tbl_cabang_p2tel)
		}
	})
	.where('alamat._default', 1)
	.map((row) => {
		return {
			id: row._ID,
			nama: row._nama,
			nik: row._nik,
			photo: checkPhoto(row._ID),
			status: checkStatusDatul(row._flag_hak, row._tgl_meninggal, row.tgl_pensiun, row._tgl_datul, row._tgl_datul_aw0, row._tgl_datul_m),
			tgl_meninggal: row._tgl_meninggal,
			tgl_pensiun: row._tgl_pensiun,
			tgl_datul: row._tgl_datul,
			tgl_datul_aw0: row._tgl_datul_aw0,
			tgl_datul_m: row._tgl_datul_m
		}
	})
	.then((data) => {

		let dataSend;
		if (token_data.tbl_cabang_p2tel !== null) {
			dataSend = [
			  {
         label: 'total_anggota',
				 title: 'Total Anggota',
         number: data.length,
         status: null,
				 order: 1
        },
				{
         label: 'daftar_wajib_datul',
				 title: 'Daftar Wajib Datul',
         number: data.length - _.where(data, {  status: 5 }).length,
         status: 'dwd',
				 order: 2
        },
				{
          label: 'selesai_datul',
					title: 'Selesai Datul',
          number: (_.where(data, {  status: 1 }).length) + (_.where(data, {  status: 2 }).length),
          status: 1,
					order: 6,
        },
				{
          label: 'sudah_datul_-_menunggu_konfirmasi_p2tel',
					title: 'Sudah Datul - Menunggu Konfirmasi P2TEL',
					number: _.where(data, {  status: 3 }).length,
          status: 3,
					order: 5
        },
				{
          label: 'belum_datul',
					title: 'Belum Datul',
          number: _.where(data, {  status: 4 }).length,
          status: 4,
					order: 4
        },
				{
          label: 'tidak_wajib_datul',
					title: 'Tidak Wajib Datul',
          number: _.where(data, {  status: 5 }).length,
          status: 5,
					order: 3
        }
			]
		} else {
			dataSend = [
			  {
         label: 'total_anggota',
				 title: 'Total Anggota',
         number: data.length,
         status: null,
				 order: 1
        },
				{
         label: 'daftar_wajib_datul',
				 title: 'Daftar Wajib Datul',
         number: data.length - _.where(data, {  status: 5 }).length,
         status: 'dwd',
				 order: 2
        },
				{
          label: 'selesai_datul',
					title: 'Selesai Datul',
          number: _.where(data, {  status: 1 }).length,
          status: 1,
					order: 5
        },
				{
          label: 'sudah_datul_-_menunggu_konfirmasi_p2tel',
					title: 'Sudah Datul - Menunggu Konfirmasi P2TEL',
          number: _.where(data, {  status: 3 }).length,
          status: 3,
					order: 6
        },
				{
          label: 'sudah_datul_-_menunggu_konfirmasi_dapen',
					title: 'Sudah Datul - Menunggu Konfirmasi Dapentel',
          number: _.where(data, {  status: 2 }).length,
          status: 2,
					order: 7
        },
				{
          label: 'belum_datul',
					title: 'Belum Datul',
          number: _.where(data, {  status: 4 }).length,
          status: 4,
					order: 4
        },
				{
          label: 'tidak_wajib_datul',
					title: 'Tidak Wajib Datul',
          number: _.where(data, {  status: 5 }).length,
          status: 5,
					order: 3
        }
			]
		}

		res.send({
			success: true,
      message: '',
			data: dataSend
			// data: data,
			// x: _.where(data, { flag_hak: 0 })
		})


	}).catch((err) => {
    res.send({
      success: false,
      message: err
    })
  })
})

module.exports = dashboarddatul
