const express = require('express');
const db = require('knex')(require('../config/db').mysql);
var NestHydrationJS = require('nesthydrationjs')();
const cabang = express.Router();

cabang.get('/info/:id', (req, res) => {
	db.select(
		'tbl_p2tel.Nama as _nama',
		'tbl_p2tel.Posisi as _posisi',
		'tbl_kontak._kontak_value as _kontak__value'
		)
		.from('tbl_p2tel')
		.where('tbl_p2tel.id_cabang', req.params.id)
		.leftJoin('tbl_pmp', 'tbl_pmp._nik', 'tbl_p2tel.NIK')
		.leftJoin('tbl_alamat', 'tbl_alamat.tbl_pmp', 'tbl_pmp.id')
		.leftJoin('tbl_kontak', 'tbl_kontak.tbl_alamat', 'tbl_alamat.id')
		.then(NestHydrationJS.nest)
		.then((data) => {
			if (data){
				res.send({
					success: true,
					data
				})
			} else {
				res.send({
					success: true,
					data: null
				})
			}
		})
});

module.exports = cabang;
