const express = require('express');
const knex = require('knex');
const absen = express.Router();
const db = knex(require('../../config/db').absen);
const moment = require('moment');
const jwt = require('jwt-simple');

const check_token_body = (req, res) => {
	const token = req.body.token;
	if (!token) {
		res.sendStatus(401);
		return
	}
	return jwt.decode(token, 'ilhamgantengpisan', null, null)
};

absen.post('/absen', (req, res) => {
	const token_data = check_token_body(req, res);
	const body = req.body;
	db
		.select('*')
		.from('trans_absen')
		.where('id_pmp', body.id_pmp)
		.then((row) => {
			if (row.length !== 0){
				res.send({
					success: false,
					message: `pmp_id ${body.id_pmp} sudah absen`,
				})
			} else {
				if (!body.id_pmp || !body.tgl_absen || !body.petugas_absen || !body.cara_absen_id
					|| !body.absen_dengan){
					res.send({
						success: false,
						message: '(id_pmp, tgl_absen, petugas_absen, cara_absen_id, absen_dengan) required in body'
					})
				} else {
					db('trans_absen')
						.insert({
							id_pmp: body.id_pmp,
							tgl_absen: moment(body.tgl_absen).format('YYYY-MM-DD HH:mm:ss'),
							petugas_absen: body.petugas_absen,
							cara_absen_id: body.cara_absen_id,
							absen_dengan: body.absen_dengan,
							created_at: moment().format('YYYY-MM-DD HH:mm:ss'),
							created_by: 'system',
						})
						.then(() => {
							res.send({
								success: true,
							})
						})
						.catch((err) => {
							res.send({
								success: false,
								message: err,
							})
						})
				}
			}
		});
});

absen.post('/kunjungan', (req, res) => {
	const token_data = check_token_body(req, res);
	const body = req.body;
	db
		.select('*')
		.from('trans_absen')
		.where('id', body.absen_id)
		.then((row) => {
			if (row.length === 0){
				res.send({
					success: false,
					message: `pmp_id ${body.id_pmp} belum absen, absen terlebih dahulu`,
				})
			} else {
				if (!body.tgl_kunjungan || !body.petugas_kunjungan || !body.cara_kunjungan_id
					|| !body.kunjungan_dengan){
					res.send({
						success: false,
						message: '(tgl_kunjungan, petugas_kunjungan, cara_kunjungan_id, kunjungan_dengan) required in body'
					})
				} else {
					db('trans_absen')
						.where('id', body.absen_id)
						.update({
							tgl_kunjungan: moment(body.tgl_kunjungan).format('YYYY-MM-DD HH:mm:ss'),
							petugas_kunjungan: body.petugas_kunjungan,
							cara_kunjungan_id: body.cara_kunjungan_id,
							kunjungan_dengan: body.kunjungan_dengan,
							updated_at: moment().format('YYYY-MM-DD HH:mm:ss'),
							updated_by: 'system',
						})
						.then(() => {
							res.send({
								success: true,
							})
						})
						.catch((err) => {
							console.log(err);
							res.send({
								success: false,
								message: err,
							})
						})
				}
			}
		});
});

module.exports = absen;
