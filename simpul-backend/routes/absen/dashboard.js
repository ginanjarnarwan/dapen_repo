const express = require('express');
const knex = require('knex');
const dashboard = express.Router();
const db = knex(require('../../config/db').absen);
const checkAvatar = require('../../util/check-avatar');
const status_conv = require('../../util/absen/status-code');
const jwt = require('jwt-simple');

const MAX_ROWS = 30000;

const DEFAULT_DASHBOARD_COLS = [
	'pmp.ID as id',
	'pmp._nik as nik',
	'pmp._naw as naw',
	'pmp._nama as nama',
	'pmp._flag_hak as flaghak',
	'pmp._tgl_meninggal as tgl_meninggal',
	'pmp._tgl_pensiun as tgl_pensiun',
	'pmp.kategori_id as kategori_kunjungan',
	'tipe._relation as type',
	'p2tel._nama_cabang as nama_mitra',
	'abs.id as absen_id',
	'abs.tgl_absen as tgl_absen',
	'abs.tgl_kunjungan as tgl_kunjungan'
];

const check_token_body = (req, res) => {
	const token = req.body.token;
	if (!token) {
		res.sendStatus(401);
		return
	}
	return jwt.decode(token, 'ilhamgantengpisan', null, null)
};

dashboard.post('/count', (req, res) => {
	// const token = req.body.token;
	// if (!token) {
	// 	res.sendStatus(401);
	// 	return
	// }
	const token_data = check_token_body(req, res);
	
	let result_query = db
		.select(DEFAULT_DASHBOARD_COLS)
		.from('tbl_pmp as pmp')
		.leftJoin('ref_tipe_ahliwaris as tipe', 'pmp.ref_tipe_ahliwaris', 'tipe.ID')
		.leftJoin('tbl_alamat as alamat', 'pmp.ID', 'alamat.tbl_pmp')
		.leftJoin('tbl_kelurahan as kelurahan', 'alamat.tbl_kelurahan_ID', 'kelurahan.ID')
		.leftJoin('tbl_cabang_p2tel as p2tel', 'kelurahan._tbl_cabang_p2tel', 'p2tel._kode_p2tel')
		.leftOuterJoin('trans_absen as abs', 'abs.id_pmp', 'pmp.id')
		.where('alamat._default', 1);
	
	if (token_data.tbl_cabang_p2tel){
		result_query = result_query.where('p2tel._kode_p2tel', token_data.tbl_cabang_p2tel)
	}
	
	result_query
		.map((row) => {
			return Object.assign({}, row, status_conv.get_status_info(row))
		})
		.then((rows) => {
			const belum = rows.filter((row) => {
				return row.status_code === 0
			});
			
			const selesai_absen = rows.filter((row) => {
				return row.status_code === 1
			});
			
			const menunggu_kunjungan = rows.filter((row) => {
				return row.status_code === 2
			});
			
			const selesai_kunjungan = rows.filter((row) => {
				return row.status_code === 3
			});
			
			const pernah_absen = rows.filter((row) => {
				return row.tgl_absen !== null
			});
			
			const harus_dikunjungi = rows.filter((row) => {
				return row.kategori_kunjungan === 1;
			});
			
			const harus_dikunjungi_belum_absen = harus_dikunjungi.filter((row) => {
				return row.tgl_absen === null
				// return row.status_code === 0
			});
			
			res.send({
				success: true,
				rows: [
					{
						status_code: null,
						kategori_kunjungan: null,
						status_title: "Semua Data",
						count: rows.length,
						ref: "/absen/dashboard/byStatus",
					},
					{
						status_code: 0,
						kategori_kunjungan: null,
						status_title: status_conv.status_code_to_string(0),
						count: belum.length,
						ref: "/absen/dashboard/byStatus/0",
					},
					{
						status_code: null,
						kategori_kunjungan: null,
						status_title: "Pernah Absen",
						count: pernah_absen.length,
						ref: "/absen/dashboard/pernahAbsen",
					},
					{
						status_code: 1,
						kategori_kunjungan: 1,
						status_title: status_conv.status_code_to_string(1),
						count: selesai_absen.length,
						ref: "/absen/dashboard/byStatus/1",
					},
					{
						status_code: 2,
						kategori_kunjungan: 1,
						status_title: status_conv.status_code_to_string(2),
						count: menunggu_kunjungan.length,
						ref: "/absen/dashboard/byStatus/2",
					},
					{
						status_code: 3,
						kategori_kunjungan: 1,
						status_title: status_conv.status_code_to_string(3),
						count: selesai_kunjungan.length,
						ref: "/absen/dashboard/byStatus/3",
					},
					{
						status_code: null,
						kategori_kunjungan: 1,
						status_title: "Total Kunjungan",
						count: harus_dikunjungi.length,
						ref: "/absen/dashboard/totalKunjungan",
					},
					{
						status_code: 0,
						kategori_kunjungan: 1,
						status_title: "Belum Absen dan Kunjungan",
						count: harus_dikunjungi_belum_absen.length,
						ref: "/absen/dashboard/totalKunjungan/belumAbsen",
					},
				],
				message: 'success'
			})
		})
});

dashboard.post('/totalKunjungan', (req, res) => {
	const token_data = check_token_body(req, res);
	
	let page = parseInt(req.query.page) || 0;
	let per_page = parseInt(req.query.per_page) || MAX_ROWS;
	if (per_page >= MAX_ROWS) {per_page = MAX_ROWS}
	if (per_page < 1) {per_page = 1}
	if (page < 0) {page = 0}
	
	let result_query = db
		.select(DEFAULT_DASHBOARD_COLS)
		.from('tbl_pmp as pmp')
		.leftJoin('ref_tipe_ahliwaris as tipe', 'pmp.ref_tipe_ahliwaris', 'tipe.ID')
		.leftJoin('tbl_alamat as alamat', 'pmp.ID', 'alamat.tbl_pmp')
		.leftJoin('tbl_kelurahan as kelurahan', 'alamat.tbl_kelurahan_ID', 'kelurahan.ID')
		.leftJoin('tbl_cabang_p2tel as p2tel', 'kelurahan._tbl_cabang_p2tel', 'p2tel._kode_p2tel')
		.leftOuterJoin('trans_absen as abs', 'abs.id_pmp', 'pmp.id')
		.where('alamat._default', 1);
	
	if (token_data.tbl_cabang_p2tel){
		result_query = result_query.where('p2tel._kode_p2tel', token_data.tbl_cabang_p2tel)
	}
	
	result_query
		.where('pmp.kategori_id', 1)
		.limit(per_page).offset(page*per_page)
		.map((row) => {
			const formatted_row = {
				id: row.id,
				nik: row.nik,
				naw: row.naw,
				nama: row.nama,
				photo: checkAvatar(row.id),
				cabang_p2tel: row.nama_mitra,
				tanggal_meninggal: row.tgl_meninggal,
				tanggal_pensiun: row.tgl_pensiun,
				absen_id: row.absen_id,
				kategori_kunjungan: row.kategori_kunjungan,
			};
			return Object.assign({}, formatted_row, status_conv.get_status_info(row))
		})
		.then((rows) => {
			res.send({
				success: true,
				rows: rows,
				total_rows: rows.length,
				message: 'success',
				pagination: {
					page,
					per_page: rows.length < per_page ? rows.length : per_page,
					// per_page,
					total_page: Math.ceil(rows.length/per_page),
				},
			})
		})
});

dashboard.post('/totalKunjungan/belumAbsen', (req, res) => {
	const token_data = check_token_body(req, res);
	
	let page = parseInt(req.query.page) || 0;
	let per_page = parseInt(req.query.per_page) || MAX_ROWS;
	if (per_page >= MAX_ROWS) {per_page = MAX_ROWS}
	if (per_page < 1) {per_page = 1}
	if (page < 0) {page = 0}
	
	let result_query = db
		.select(DEFAULT_DASHBOARD_COLS)
		.from('tbl_pmp as pmp')
		.leftJoin('ref_tipe_ahliwaris as tipe', 'pmp.ref_tipe_ahliwaris', 'tipe.ID')
		.leftJoin('tbl_alamat as alamat', 'pmp.ID', 'alamat.tbl_pmp')
		.leftJoin('tbl_kelurahan as kelurahan', 'alamat.tbl_kelurahan_ID', 'kelurahan.ID')
		.leftJoin('tbl_cabang_p2tel as p2tel', 'kelurahan._tbl_cabang_p2tel', 'p2tel._kode_p2tel')
		.leftOuterJoin('trans_absen as abs', 'abs.id_pmp', 'pmp.id')
		.where('alamat._default', 1);
	
	if (token_data.tbl_cabang_p2tel){
		result_query = result_query.where('p2tel._kode_p2tel', token_data.tbl_cabang_p2tel)
	}
	
	result_query
		.where('pmp.kategori_id', 1)
		.whereNull('abs.tgl_absen')
		.limit(per_page).offset(page*per_page)
		.map((row) => {
			const formatted_row = {
				id: row.id,
				nik: row.nik,
				naw: row.naw,
				nama: row.nama,
				photo: checkAvatar(row.id),
				cabang_p2tel: row.nama_mitra,
				tanggal_meninggal: row.tgl_meninggal,
				tanggal_pensiun: row.tgl_pensiun,
				absen_id: row.absen_id,
				kategori_kunjungan: row.kategori_kunjungan,
			};
			return Object.assign({}, formatted_row, status_conv.get_status_info(row))
		})
		.then((rows) => {
			res.send({
				success: true,
				rows: rows,
				total_rows: rows.length,
				message: 'success',
				pagination: {
					page,
					per_page: rows.length < per_page ? rows.length : per_page,
					// per_page,
					total_page: Math.ceil(rows.length/per_page),
				},
			})
		})
});

dashboard.post('/pernahAbsen', (req, res) => {
	const token_data = check_token_body(req, res);
	
	let page = parseInt(req.query.page) || 0;
	let per_page = parseInt(req.query.per_page) || MAX_ROWS;
	if (per_page >= MAX_ROWS) {per_page = MAX_ROWS}
	if (per_page < 1) {per_page = 1}
	if (page < 0) {page = 0}
	
	let result_query = db
		.select(DEFAULT_DASHBOARD_COLS)
		.from('tbl_pmp as pmp')
		.leftJoin('ref_tipe_ahliwaris as tipe', 'pmp.ref_tipe_ahliwaris', 'tipe.ID')
		.leftJoin('tbl_alamat as alamat', 'pmp.ID', 'alamat.tbl_pmp')
		.leftJoin('tbl_kelurahan as kelurahan', 'alamat.tbl_kelurahan_ID', 'kelurahan.ID')
		.leftJoin('tbl_cabang_p2tel as p2tel', 'kelurahan._tbl_cabang_p2tel', 'p2tel._kode_p2tel')
		.leftOuterJoin('trans_absen as abs', 'abs.id_pmp', 'pmp.id')
		.where('alamat._default', 1);
	
	if (token_data.tbl_cabang_p2tel){
		result_query = result_query.where('p2tel._kode_p2tel', token_data.tbl_cabang_p2tel)
	}
	
	result_query
		.whereNotNull('abs.tgl_absen')
		.limit(per_page).offset(page*per_page)
		.map((row) => {
			const formatted_row = {
				id: row.id,
				nik: row.nik,
				naw: row.naw,
				nama: row.nama,
				photo: checkAvatar(row.id),
				cabang_p2tel: row.nama_mitra,
				tanggal_meninggal: row.tgl_meninggal,
				tanggal_pensiun: row.tgl_pensiun,
				absen_id: row.absen_id,
				kategori_kunjungan: row.kategori_kunjungan,
			};
			return Object.assign({}, formatted_row, status_conv.get_status_info(row))
		})
		.then((rows) => {
			res.send({
				success: true,
				rows: rows,
				total_rows: rows.length,
				message: 'success',
				pagination: {
					page,
					per_page: rows.length < per_page ? rows.length : per_page,
					// per_page,
					total_page: Math.ceil(rows.length/per_page),
				},
			})
		})
});

dashboard.post('/byStatus', (req, res) => {
	const token_data = check_token_body(req, res);
	
	let page = parseInt(req.query.page) || 0;
	let per_page = parseInt(req.query.per_page) || MAX_ROWS;
	if (per_page >= MAX_ROWS) {per_page = MAX_ROWS}
	if (per_page < 1) {per_page = 1}
	if (page < 0) {page = 0}
	
	let result_query = db
		.select(DEFAULT_DASHBOARD_COLS)
		.from('tbl_pmp as pmp')
		.leftJoin('ref_tipe_ahliwaris as tipe', 'pmp.ref_tipe_ahliwaris', 'tipe.ID')
		.leftJoin('tbl_alamat as alamat', 'pmp.ID', 'alamat.tbl_pmp')
		.leftJoin('tbl_kelurahan as kelurahan', 'alamat.tbl_kelurahan_ID', 'kelurahan.ID')
		.leftJoin('tbl_cabang_p2tel as p2tel', 'kelurahan._tbl_cabang_p2tel', 'p2tel._kode_p2tel')
		.leftOuterJoin('trans_absen as abs', 'abs.id_pmp', 'pmp.id')
		.where('alamat._default', 1);
	
	if (token_data.tbl_cabang_p2tel){
		result_query = result_query.where('p2tel._kode_p2tel', token_data.tbl_cabang_p2tel)
	}
	
	result_query
		.limit(per_page).offset(page*per_page)
		.map((row) => {
			const formatted_row = {
				id: row.id,
				nik: row.nik,
				naw: row.naw,
				nama: row.nama,
				photo: checkAvatar(row.id),
				cabang_p2tel: row.nama_mitra,
				tanggal_meninggal: row.tgl_meninggal,
				tanggal_pensiun: row.tgl_pensiun,
				absen_id: row.absen_id,
				kategori_kunjungan: row.kategori_kunjungan,
			};
			return Object.assign({}, formatted_row, status_conv.get_status_info(row))
		})
		.then((rows) => {
			res.send({
				success: true,
				rows: rows,
				total_rows: rows.length,
				message: 'success',
				pagination: {
					page,
					per_page: rows.length < per_page ? rows.length : per_page,
					// per_page,
					total_page: Math.ceil(rows.length/per_page),
				},
			})
		})
});

dashboard.post('/byStatus/:status_code', (req, res) => {
	const token_data = check_token_body(req, res);
	
	const status_code = Number(req.params.status_code);
	let page = parseInt(req.query.page) || 0;
	let per_page = parseInt(req.query.per_page) || MAX_ROWS;
	if (per_page >= MAX_ROWS) {per_page = MAX_ROWS}
	if (per_page < 1) {per_page = 1}
	if (page < 0) {page = 0}
	
	if (![0, 1, 2, 3].includes(status_code)){
		res.send({
			success: false,
			rows: [],
			total_data: 0,
			message: 'status_code not found'
		});
	} else {
		let result_query = db
			.select(DEFAULT_DASHBOARD_COLS)
			.from('tbl_pmp as pmp')
			.leftJoin('ref_tipe_ahliwaris as tipe', 'pmp.ref_tipe_ahliwaris', 'tipe.ID')
			.leftJoin('tbl_alamat as alamat', 'pmp.ID', 'alamat.tbl_pmp')
			.leftJoin('tbl_kelurahan as kelurahan', 'alamat.tbl_kelurahan_ID', 'kelurahan.ID')
			.leftJoin('tbl_cabang_p2tel as p2tel', 'kelurahan._tbl_cabang_p2tel', 'p2tel._kode_p2tel')
			.leftOuterJoin('trans_absen as abs', 'abs.id_pmp', 'pmp.id')
			.where('alamat._default', 1);
		
		if (token_data.tbl_cabang_p2tel){
			result_query = result_query.where('p2tel._kode_p2tel', token_data.tbl_cabang_p2tel)
		}
		
		// belum absen
		if (status_code === 0) {
			result_query = result_query
				.whereNull('abs.tgl_absen')
		}
		
		// selesai absen
		if (status_code === 1) {
			result_query = result_query
				.where('pmp.kategori_id', 2)
				.whereNotNull('abs.tgl_absen')
		}
		
		// menunggu kunjungan
		if (status_code === 2) {
			result_query = result_query
				.where('pmp.kategori_id', 1)
				.whereNotNull('abs.tgl_absen')
				.whereNull('abs.tgl_kunjungan')
		}
		
		// selesai kunjungan
		if (status_code === 3) {
			result_query = result_query
				.where('pmp.kategori_id', 1)
				.whereNotNull('abs.tgl_absen')
				.whereNotNull('abs.tgl_kunjungan')
		}
		
		result_query
			.limit(per_page).offset(page*per_page)
			.map((row) => {
				const formatted_row = {
					id: row.id,
					nik: row.nik,
					naw: row.naw,
					nama: row.nama,
					photo: checkAvatar(row.id),
					cabang_p2tel: row.nama_mitra,
					tanggal_meninggal: row.tgl_meninggal,
					tanggal_pensiun: row.tgl_pensiun,
					absen_id: row.absen_id,
					kategori_kunjungan: row.kategori_kunjungan,
				};
				return Object.assign({}, formatted_row, status_conv.get_status_info(row))
			})
			.then((rows) => {
				res.send({
					success: true,
					rows: rows,
					total_rows: rows.length,
					message: 'success',
					pagination: {
						page,
						per_page: rows.length < per_page ? rows.length : per_page,
						// per_page,
						total_page: Math.ceil(rows.length/per_page),
					},
				})
			})
	}
});

dashboard.post('/detailPmp/:id_pmp', (req, res) => {
	const token_data = check_token_body(req, res);
	
	const id_pmp = Number(req.params.id_pmp);
	
	let result_query = db
		.select(DEFAULT_DASHBOARD_COLS)
		.from('tbl_pmp as pmp')
		.leftJoin('ref_tipe_ahliwaris as tipe', 'pmp.ref_tipe_ahliwaris', 'tipe.ID')
		.leftJoin('tbl_alamat as alamat', 'pmp.ID', 'alamat.tbl_pmp')
		.leftJoin('tbl_kelurahan as kelurahan', 'alamat.tbl_kelurahan_ID', 'kelurahan.ID')
		.leftJoin('tbl_cabang_p2tel as p2tel', 'kelurahan._tbl_cabang_p2tel', 'p2tel._kode_p2tel')
		.leftOuterJoin('trans_absen as abs', 'abs.id_pmp', 'pmp.id')
		.where('alamat._default', 1)
		.where('pmp.ID', id_pmp);
	
	result_query
		.map((row) => {return Object.assign({
			photo: checkAvatar(row.id)}, row, status_conv.get_status_info(row))
		})
		.then((rows) => {
			res.send({
				success: true,
				rows
			})
		})
});

module.exports = dashboard;
