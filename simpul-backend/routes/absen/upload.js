const express = require('express');
const upload = express.Router();
const multer = require('multer');
const path = require('path');
const mkdirp = require('mkdirp');
const checkDokumenKunjungan = require('../../util/absen/check-dokumen');

const checkFileExt = (file) => {
	const filetypes = /jpeg|jpg|pdf/;
	const mimetype = filetypes.test(file.mimetype);
	const extname = filetypes.test(path.extname(file.originalname).toLowerCase());
	const ext = path.extname(file.originalname).toLowerCase()
	return {
		mimetype,
		extname,
		ext
	}
};

const storageDokumenKunjungan = multer.diskStorage({
	destination: function(req, file, cb) {
		const {mimetype, extname} = checkFileExt(file);
		if (mimetype && extname) {
			const filepath = `./public/absensi/dokumen/${req.body.id_pmp}`;
			mkdirp(filepath, err => cb(err, filepath));
		} else {
			cb("Error: File upload on destination callback");
		}
	},
	filename: function(req, file, cb) {
		// let dokumen_type;
		const {mimetype, extname, ext} = checkFileExt(file);
		if ( ['foto', 'form'].indexOf( req.body.dokumen_type ) > -1 ){
			let dokumen_type = req.body.dokumen_type
			const file_name = `${ req.body.id_pmp }_${ dokumen_type }_kunjungan${ ext }`;
			if (mimetype && extname) {
				cb(null, file_name);
			} else {
				cb("Error: File upload on filename callback")
			}
		} else {
			cb("Error: File upload on filename callbackx")
		}

	},
});

const uploadDokumenKunjungan = multer({
	storage: storageDokumenKunjungan,
	fileFilter: function (req, file, cb) {
		const {mimetype, extname} = checkFileExt(file);
		if (mimetype && extname) {
			return cb(null, true);
		} else {
			cb("Error: File upload on fileFilter callback");
		}
	}
}).single('dokumen');

upload.post('/fotoKunjungan', (req, res) => {
	uploadDokumenKunjungan(req, res, (err) => {
		if (err){
			console.log(err)
		} else {
			res.send({
				success: true
			})
		}
	})
});

upload.post('/formKunjungan', (req, res) => {
	uploadDokumenKunjungan(req, res, (err) => {
		if (err){
			res.send({
				success: false,
				msg: err,
				test: req.body
			})
		} else {
			res.send({
				success: true
			})
		}
	})
});

upload.get('/getDokumenKunjungan/:id', (req, res) => {
	res.send({
		paths: checkDokumenKunjungan(req.params.id)
	})
});

module.exports = upload;
