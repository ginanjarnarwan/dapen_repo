var express = require('express')
var report = express.Router()
var db = require('knex')(require('../config/db').mysql)
var moment = require('moment')
var bcrypt = require('bcrypt-nodejs')
var _ = require('underscore')
var _s = require('underscore.string')
var jwt = require('jwt-simple')
var NestHydrationJS = require('nesthydrationjs')()
var dateFns = require('date-fns')
var moment = require('moment')
var fs = require('fs')
var excel = require('node-excel-export')
var nodeExcel = require('excel-export')
var json2xlsx = require('json2xlsx-export')

var util = require('../util/old')
var authUtil = require('../util/auth')
var checkDate = require('../util/check-date')
var fieldMaker = require('../util/field-maker')
var checkGender = require('../util/check-gender')
var checkPhoto = require('../util/check-photo')
var utilLaporan = require('../util/util-laporan')
var datulConfig = require('../config/datul-config')
var checkStatusDatul = require('../util/check-status-datul')
const dwdAtauTidak = require('../util/dwd-atau-tidak')

report.post('/', (req, res) => {
	var token = jwt.decode(req.body.token, 'ilhamgantengpisan')
	db.select(
		'pmp._nama as nama'
	)
	.from('tbl_pmp as pmp')
	// .map((row) => {
	// 	return {
	// 		nama: row.nama
	// 	}
	// })
	.then((data) => {

		dataSample = [
			{ nama: 'ilham' },
			{ nama: 'lukman' }
		]
		const styles = {
        headerDark: {
          fill: {
            fgColor: {
              rgb: 'FFFFFFFF'
            }
          },
          font: {
            color: {
              rgb: 'FF000000'
            },
            sz: 12,
            bold: true
          }
        },
        cellPink: {
          fill: {
            fgColor: {
              rgb: 'FFFFCCFF'
            }
          }
        },
        cellGreen: {
          fill: {
            fgColor: {
              rgb: 'FF00FF00'
            }
          }
        }
      }
    const specification = {
        nama: {
          displayName: 'Nama',
          width: 200,
          headerStyle: styles.headerDark
        }
      }

      var filename = function() {
        if (token.tbl_cabang_p2tel == null || token.tbl_cabang_p2tel == '') {
          return 'Semua Cabang-' + moment().format('YYYYMMDD')
        } else {
          return  _s.strRightBack(token._cabang_p2tel,'-') + moment().format('-YYYYMMDD')
        }
      }

      const build = excel.buildExport(
        [{
          name: 'Report',
          data: dataSample,
          specification: specification
        }]
      )
      // res.attachment('REKAP-' + filename() + '.xls');
      // return
      res.setHeader('Content-Type', 'application/vnd.openxmlformats');
	  	res.setHeader("Content-Disposition", "attachment; filename=" + "Report.xlsx");
      res.end(build, 'binary');
	})

})

report.post('/excel2', function(req, res, next){
    var decoded = jwt.decode(req.body.token, 'ilhamgantengpisan')
    var trDatul     =    db.select(db.raw('ID, max(_tgl_datul) AS _tgl_datul, tbl_pmp'))
                        .from('tbl_transaksi_datul').groupBy('tbl_pmp').as('transaksi');
    var trDatulaw0  =    db.select(db.raw('ID, max(_tgl_datul) AS _tgl_datul, tbl_pmp'))
                        .from('tbl_transaksi_datul_aw0').groupBy('tbl_pmp').as('transaksiAW0');
    var trDatulM    =    db.select(db.raw('ID, max(_tgl_datul) AS _tgl_datul, tbl_pmp'))
                        .from('tbl_transaksi_datul_mandiri').groupBy('tbl_pmp').as('transaksiM');
    // var ahliWaris   =   db.select(db.raw('_nik, count(_nik) AS jml_ahli_waris'))
    //                     .from('tbl_calon_pmp')
    //                     .whereNull('_tgl_meninggal')
    //                     .groupBy('_nik').as('tbl_calon_pmp')

    var kontak = db('tbl_kontak').as('kontak')

  db.select(
    'pmp.ID as _ID',
    'pmp._naw as _naw',
    'pmp._nik as _nik',
    'tipe._relation as _type',
    'pmp._nama as _namaPmp',
		'pmp._flag_hak as _flagHak',
		'pmp._tgl_meninggal as _tglMeninggal',
		'pmp._tgl_pensiun as _tglPensiun',
    'peserta._nama as _namaPeserta',
    'p2tel._kode_p2tel as _kodep2tel',
    'p2tel._nama_cabang as _namaMitra',

    'alamat.ID AS _alamat__id',
    'alamat._jalan AS _alamat__jalan',
    'alamat._komp AS _alamat__komp',
    'alamat._blok AS _alamat__blok',
    'alamat._no AS _alamat__no',
    'alamat._rt AS _alamat__rt',
    'alamat._rw AS _alamat__rw',
    'kelurahan._nama_kelurahan AS _alamat__kelurahan',
    'kecamatan._nama_kecamatan AS _alamat__kecamatan',
    'kota._nama_kota AS _alamat__kota',
    'provinsi._nama_provinsi AS _alamat__provinsi',
    'kelurahan._kode_pos AS _alamat__kodepos',
    'kontak.ID as _kontak__ID',
    'kontak.ref_tipe_kontak as _kontak__kontakTipe',
    'kontak._kontak_value AS _kontak__kontakVal',
    'transaksi._tgl_datul as _tglDatul',
    'transaksiAW0._tgl_datul as _tglDatulAw0',
    'transaksiM._tgl_datul as _tglDatulM',
    // 'aw.ID as _ahliwaris__ID',
    // 'aw._nama as _ahliwaris__nama',
    // 'aw._nik as _ahliwaris__nik',
    // 'aw.ref_tipe_ahliwaris as _ahliwaris__tipe',
    // 'aw._tgl_menikah_lagi as _ahliwaris_tglMenikahLagi',
    // 'aw._tgl_lahir as _ahliwaris__tglLahir',
    // 'aw._tgl_cerai as _ahliwaris__tglCerai',
    'photo.ID as _photoID',
    'f._finger_1 AS _finger1'
    // 'tbl_calon_pmp.jml_ahli_waris as _jml_ahli_waris'
    )
    .from('tbl_pmp as pmp')
    .leftJoin('tbl_alamat as alamat', 'pmp.ID','alamat.tbl_pmp')
    // .leftJoin('tbl_calon_pmp as aw', 'aw._nik', 'pmp._nik')
    .leftJoin('tbl_photo as photo', 'pmp.ID', 'photo.ID')
    .leftJoin(trDatul, 'transaksi.tbl_pmp', 'pmp.ID')
    .leftJoin(trDatulaw0, 'transaksiAW0.tbl_pmp', 'pmp.ID')
    .leftJoin(trDatulM, 'transaksiM.tbl_pmp', 'pmp.ID')
    .leftJoin('tbl_peserta as peserta', 'pmp._nik', 'peserta._nik')
    .leftJoin('ref_tipe_ahliwaris as tipe','pmp.ref_tipe_ahliwaris', 'tipe.ID')
    .leftJoin(kontak, 'alamat.ID', 'kontak.tbl_alamat')
    .leftJoin('ref_tipe_kontak AS tipeKontak', 'tipeKontak.ID', 'kontak.ref_tipe_kontak')
    .leftJoin('tbl_kelurahan as kelurahan', 'alamat.tbl_kelurahan_ID','kelurahan.ID')
    .leftJoin('tbl_kecamatan as kecamatan', 'kecamatan.ID', 'kelurahan.tbl_kecamatan_ID')
    .leftJoin('tbl_kota as kota', 'kota.ID', 'kecamatan.tbl_kota_ID')
    .leftJoin('tbl_provinsi as provinsi', 'provinsi.ID', 'kota.tbl_provinsi_ID')
    .leftJoin('tbl_cabang_p2tel as p2tel', 'kelurahan._tbl_cabang_p2tel', 'p2tel._kode_p2tel')
    .leftJoin('tbl_finger_print AS f', 'f.id', 'pmp.id')
    // .whereNull('pmp._tgl_meninggal')
    // .whereNull('aw._tgl_meninggal')
    // .whereNull('aw._tgl_menikah_lagi')
    // .whereNull('aw._tgl_cerai')
    // .whereNull('aw._tgl_lulus')
    // .whereNotIn('aw.ID', function(){
    //     this.select('ID').from('tbl_pmp')
    // })
    .where(function(){
        if (decoded.tbl_cabang_p2tel) {
          this.where('p2tel._kode_p2tel', decoded.tbl_cabang_p2tel)
        } else {

        }
      }
    )
    // .where(function(){
    //     this.whereNotBetween('_tgl_pensiun', [datulConfig.start(), datulConfig.finish()] )
    //     .orWhere({_tgl_pensiun: null})
    // })
    .where('alamat._default', 1)
    // .modify(function(qB){
    //   if(req.user.dataValues.tbl_cabang_p2tel) {
    //     qB.where('kelurahan._tbl_cabang_p2tel', req.user.dataValues.tbl_cabang_p2tel )
    //   }
    // })
    .then(NestHydrationJS.nest)
    .map(function(row,index){
          return {
            NO: index + 1,
            id: row.ID,
            _nik: row.nik,
            _naw: row._naw,
            _nama: util.gabungNama(row.namaPmp, row.type, row.namaPeserta),
            _jalan: row.alamat[0].jalan,
            _komp: row.alamat[0].komp,
            _blok: row.alamat[0].blok,
            _noalamat: row.alamat[0].no,
            _rt: row.alamat[0].rt,
            _rw: row.alamat[0].rw,
            _alamat: row.alamat,
            _kelurahan: row.alamat[0].kelurahan,
            _kecamatan: row.alamat[0].kecamatan,
            _kota: row.alamat[0].kota,
            _provinsi: row.alamat[0].provinsi,
            _kodepos: row.alamat[0].kodepos,
            _telepon_rumah: _.pluck(_.filter(row.kontak, function(item){ return item.kontakTipe == 1 }), 'kontakVal'),
            _hp: _.pluck(_.filter(row.kontak, function(item){ return item.kontakTipe == 2 }), 'kontakVal'),
            _email: _.pluck(_.filter(row.kontak, function(item){ return item.kontakTipe == 3 }), 'kontakVal'),
            _fax: _.pluck(_.filter(row.kontak, function(item){ return item.kontakTipe == 4 }), 'kontakVal'),
            _kode_p2tel: row.kodep2tel,
            _cabang_p2tel: row.namaMitra,
            _status_datul: util.datulCheck(row.tglDatul),
            _status: util.checkStatus(row.pmp_status, row.peserta_status, row.calon_status),
            // _jml_ahli_waris: (_.filter(row.ahliwaris, function(item) { return (item.tipe === 4 && dateFns.differenceInDays(new Date(), item.tglLahir) < 9131) || (item.tipe === 2) || (item.tipe === 3) || (item.tipe === 5) || (item.tipe === 1)})).length - 1,
            // _datul_confirm_check: util.datulConfirmCheck(row.tglDatul, row.tglDatulAw0, row.tglDatulM, req.body._petugas_ID),
						_datul_confirm_check: checkStatusDatul(row.flagHak, row.tglMeninggal, row.tglPensiun, row.tglDatul, row.tglDatulAw0, row.tglDatulM),
						_photo: utilLaporan.checkPhoto(row.photoID),
            _finger: utilLaporan.checkFinger(row.finger1)
          }
    })
    .then(function(data){
			if (decoded.tbl_cabang_p2tel) {
				data = _.map(data, (obj) => {
					if (obj._datul_confirm_check === 2) {
						obj._datul_confirm_check = 1
					}
					return obj
				})
			}

			if (req.body.status !== null && req.body.status !== 20) {
				data = _.filter(data, (o) => {
					return o._datul_confirm_check === req.body.status
				})
			}

			if (req.body.status === 20) {
				data = _.filter(data, (o) => {
					return (o._datul_confirm_check === 5 || o._datul_confirm_check === 6 || o._datul_confirm_check === 7)
				})
			}


      const styles = {
        headerDark: {
          fill: {
            fgColor: {
              rgb: 'FFFFFFFF'
            }
          },
          font: {
            color: {
              rgb: 'FF000000'
            },
            sz: 12,
            bold: true
          }
        },
        cellPink: {
          fill: {
            fgColor: {
              rgb: 'FFFFCCFF'
            }
          }
        },
        cellGreen: {
          fill: {
            fgColor: {
              rgb: 'FF00FF00'
            }
          }
        }
      };

      const specification = {
        NO : {
          displayName: 'No',
          width: 30,
          headerStyle: styles.headerDark
        },
        _nik: {
          displayName: 'NIK',
          width: 110,
          headerStyle: styles.headerDark
        },
        _nama: {
          displayName: 'Nama',
          width: 200,
          headerStyle: styles.headerDark
        },
        _photo: {
          displayName: 'Data Foto',
          width: 100,
          headerStyle: styles.headerDark
        },
        _finger: {
          displayName: 'Sidik Jari',
          width: 100,
          headerStyle: styles.headerDark
        },
        _jalan: {
          displayName: 'JALAN',
          width: 120,
          headerStyle: styles.headerDark
        },
        _komp: {
          displayName: 'KOMP',
          width: 120,
          headerStyle: styles.headerDark
        },
        _blok: {
          displayName: 'BLOK',
          width: 120,
          headerStyle: styles.headerDark
        },
        _noalamat: {
          displayName: 'No Rumah',
          width: 50,
          headerStyle: styles.headerDark
        },
        _rt: {
          displayName: 'RT',
          width: 50,
          headerStyle: styles.headerDark
        },
        _rw: {
          displayName: 'RW',
          width: 50,
          headerStyle: styles.headerDark
        },
        _kelurahan: {
          displayName: 'KELURAHAN',
          width: 120,
          headerStyle: styles.headerDark
        },
        _kecamatan: {
          displayName: 'KECAMATAN',
          width: 120,
          headerStyle: styles.headerDark
        },
        _kota: {
          displayName: 'KOTA',
          width: 120,
          headerStyle: styles.headerDark
        },
        _provinsi: {
          displayName: 'PROVINSI',
          width: 120,
          headerStyle: styles.headerDark
        },
        _kodepos: {
          displayName: 'KODEPOS',
          width: 120,
          headerStyle: styles.headerDark
        },
        _telepon_rumah: {
          displayName: 'Telepon Rumah',
          width: 100,
          headerStyle: styles.headerDark
        },
        _hp: {
          displayName: 'HP',
          width: 100,
          headerStyle: styles.headerDark
        },
        _email: {
          displayName: 'Email',
          width: 100,
          headerStyle: styles.headerDark
        },
        _fax: {
          displayName: 'FAX',
          width: 100,
          headerStyle: styles.headerDark
        },
        // _jml_ahli_waris: {
        //   displayName: 'Jumlah Penerus',
        //   width: 110,
        //   headerStyle: styles.headerDark
        // },
        _cabang_p2tel: {
          displayName: 'Cabang P2TEL',
          width: 120,
          headerStyle: styles.headerDark
        },
        _status_datul: {
          displayName: 'Status Datul',
          width: 120,
          headerStyle: styles.headerDark
        },
        _datul_confirm_check: {
          displayName: 'Datul Konfirmasi',
          width: 100,
          headerStyle: styles.headerDark,
          cellFormat: function(value, row) {
            if (value == 1) {
              return 'Selesai Datul'
            } else if (value == 2) {
							return 'Menunggu Konfirmasi DAPENTEL'
						} else if (value == 3) {
							return 'Menunggu Konfirmasi P2TEL'
            } else if (value == 4) {
              return 'Belum Datul'
            } else if (value == 5) {
              return 'PMP Baru'
            } else if (value == 6) {
							return 'Tangguhan'
						} else if (valie == 7) {
							return 'Sudah Meninggal'
						}
          }
        }
      }

      var filename = function() {
        if (req.body.cabangP2tel == null || req.body.cabangP2tel == '') {
          return 'Semua Cabang-' + moment().format('YYYYMMDD')
        } else {
          return  _s.strRightBack(req.body.cabangP2tel,'-') + moment().format('-YYYYMMDD')
        }
      }

      // var something = json2xlsx(config)
      const something = excel.buildExport(
        [{
          name: 'Report',
          data: data,
          specification: specification
        }]
      );
      // console.log(something)
      // res.json(data)
      res.setHeader('Content-Type', 'application/vnd.openxmlformats');
      res.setHeader("Content-Disposition", "attachment; filename=" + "Report.xls")
      // res.attachment('REKAP-' + filename() + '.xls');
      res.end(something, 'binary');



    })
});

report.post('/excel-lagih', (req, res) => {
	var decoded = jwt.decode(req.body.token, 'ilhamgantengpisan')
	var trDatul     =    db.select(db.raw('ID, max(_tgl_datul) AS _tgl_datul, tbl_pmp'))
											.from('tbl_transaksi_datul').groupBy('tbl_pmp').as('transaksi');
	var trDatulaw0  =    db.select(db.raw('ID, max(_tgl_datul) AS _tgl_datul, tbl_pmp'))
											.from('tbl_transaksi_datul_aw0').groupBy('tbl_pmp').as('transaksiAW0');
	var trDatulM    =    db.select(db.raw('ID, max(_tgl_datul) AS _tgl_datul, tbl_pmp'))
											.from('tbl_transaksi_datul_mandiri').groupBy('tbl_pmp').as('transaksiM');
	// var ahliWaris   =   db.select(db.raw('_nik, count(_nik) AS jml_ahli_waris'))
	//                     .from('tbl_calon_pmp')
	//                     .whereNull('_tgl_meninggal')
	//                     .groupBy('_nik').as('tbl_calon_pmp')

	var kontak = db('tbl_kontak_prev').as('kontak')

db.select(
	'pmp.ID as _ID',
	'pmp._naw as _naw',
	'pmp._nik as _nik',
	'tipe._relation as _type',
	'pmp._nama as _namaPmp',
	'pmp._flag_hak as _flagHak',
	'pmp._tgl_meninggal as _tglMeninggal',
	'pmp._tgl_pensiun as _tglPensiun',
	'peserta._nama as _namaPeserta',
	'p2tel._kode_p2tel as _kodep2tel',
	'p2tel._nama_cabang as _namaMitra',

	'alamat.ID AS _alamat__id',
	'alamat._jalan AS _alamat__jalan',
	'alamat._komp AS _alamat__komp',
	'alamat._blok AS _alamat__blok',
	'alamat._no AS _alamat__no',
	'alamat._rt AS _alamat__rt',
	'alamat._rw AS _alamat__rw',
	'kelurahan._nama_kelurahan AS _alamat__kelurahan',
	'kecamatan._nama_kecamatan AS _alamat__kecamatan',
	'kota._nama_kota AS _alamat__kota',
	'provinsi._nama_provinsi AS _alamat__provinsi',
	'kelurahan._kode_pos AS _alamat__kodepos',
	'kontak.ID as _kontak__ID',
	'kontak.ref_tipe_kontak as _kontak__kontakTipe',
	'kontak._kontak_value AS _kontak__kontakVal',
	// 'transaksi._tgl_datul as _tglDatul',
	// 'transaksiAW0._tgl_datul as _tglDatulAw0',
	// 'transaksiM._tgl_datul as _tglDatulM',
	// 'aw.ID as _ahliwaris__ID',
	// 'aw._nama as _ahliwaris__nama',
	// 'aw._nik as _ahliwaris__nik',
	// 'aw.ref_tipe_ahliwaris as _ahliwaris__tipe',
	// 'aw._tgl_menikah_lagi as _ahliwaris_tglMenikahLagi',
	// 'aw._tgl_lahir as _ahliwaris__tglLahir',
	// 'aw._tgl_cerai as _ahliwaris__tglCerai',
	'photo.ID as _photoID',
	'f._finger_1 AS _finger1'
	// 'tbl_calon_pmp.jml_ahli_waris as _jml_ahli_waris'
	)
	.from('tbl_pmp_prev as pmp')
	.leftJoin('tbl_alamat_prev as alamat', 'pmp.ID','alamat.tbl_pmp')
	// .leftJoin('tbl_calon_pmp as aw', 'aw._nik', 'pmp._nik')
	.leftJoin('tbl_photo as photo', 'pmp.ID', 'photo.ID')
	// .leftJoin(trDatul, 'transaksi.tbl_pmp', 'pmp.ID')
	// .leftJoin(trDatulaw0, 'transaksiAW0.tbl_pmp', 'pmp.ID')
	// .leftJoin(trDatulM, 'transaksiM.tbl_pmp', 'pmp.ID')
	.leftJoin('tbl_peserta as peserta', 'pmp._nik', 'peserta._nik')
	.leftJoin('ref_tipe_ahliwaris as tipe','pmp.ref_tipe_ahliwaris', 'tipe.ID')
	.leftJoin(kontak, 'alamat.ID', 'kontak.tbl_alamat')
	.leftJoin('ref_tipe_kontak AS tipeKontak', 'tipeKontak.ID', 'kontak.ref_tipe_kontak')
	.leftJoin('tbl_kelurahan as kelurahan', 'alamat.tbl_kelurahan_ID','kelurahan.ID')
	.leftJoin('tbl_kecamatan_prev as kecamatan', 'kecamatan.ID', 'kelurahan.tbl_kecamatan_ID')
	.leftJoin('tbl_kota as kota', 'kota.ID', 'kecamatan.tbl_kota_ID')
	.leftJoin('tbl_provinsi as provinsi', 'provinsi.ID', 'kota.tbl_provinsi_ID')
	.leftJoin('tbl_cabang_p2tel as p2tel', 'kelurahan._tbl_cabang_p2tel', 'p2tel._kode_p2tel')
	.leftJoin('tbl_finger_print AS f', 'f.id', 'pmp.id')
	// .whereNull('pmp._tgl_meninggal')
	// .whereNull('aw._tgl_meninggal')
	// .whereNull('aw._tgl_menikah_lagi')
	// .whereNull('aw._tgl_cerai')
	// .whereNull('aw._tgl_lulus')
	// .whereNotIn('aw.ID', function(){
	//     this.select('ID').from('tbl_pmp')
	// })
	.where(function(){
			if (decoded.tbl_cabang_p2tel) {
				this.where('p2tel._kode_p2tel', decoded.tbl_cabang_p2tel)
			} else {

			}
		}
	)
	// .where(function(){
	// 		this.whereNotBetween('_tgl_pensiun', [datulConfig.start(), datulConfig.finish()] )
	// 		.orWhere({_tgl_pensiun: null})
	// })
	.where('alamat._default', 1)
	// .modify(function(qB){
	//   if(req.user.dataValues.tbl_cabang_p2tel) {
	//     qB.where('kelurahan._tbl_cabang_p2tel', req.user.dataValues.tbl_cabang_p2tel )
	//   }
	// })
	.then(NestHydrationJS.nest)
	.map(function(row,index){


				return {
					NO: index + 1,
					id: row.ID,
					_nik: row.nik,
					_naw: row._naw,
					_nama: util.gabungNama(row.namaPmp, row.type, row.namaPeserta),
					_jalan: row.alamat[0].jalan,
					_komp: row.alamat[0].komp,
					_blok: row.alamat[0].blok,
					_noalamat: row.alamat[0].no,
					_rt: row.alamat[0].rt,
					_rw: row.alamat[0].rw,
					_alamat: row.alamat,
					_kelurahan: row.alamat[0].kelurahan,
					_kecamatan: row.alamat[0].kecamatan,
					_kota: row.alamat[0].kota,
					_provinsi: row.alamat[0].provinsi,
					_kodepos: row.alamat[0].kodepos,
					_telepon_rumah: _.pluck(_.filter(row.kontak, function(item){ return item.kontakTipe == 1 }), 'kontakVal'),
					_hp: _.pluck(_.filter(row.kontak, function(item){ return item.kontakTipe == 2 }), 'kontakVal'),
					_email: _.pluck(_.filter(row.kontak, function(item){ return item.kontakTipe == 3 }), 'kontakVal'),
					_fax: _.pluck(_.filter(row.kontak, function(item){ return item.kontakTipe == 4 }), 'kontakVal'),
					_kode_p2tel: row.kodep2tel,
					_cabang_p2tel: row.namaMitra,
					_status_datul: util.datulCheck(row.tglDatul),
					_status: util.checkStatus(row.pmp_status, row.peserta_status, row.calon_status),
					// _jml_ahli_waris: (_.filter(row.ahliwaris, function(item) { return (item.tipe === 4 && dateFns.differenceInDays(new Date(), item.tglLahir) < 9131) || (item.tipe === 2) || (item.tipe === 3) || (item.tipe === 5) || (item.tipe === 1)})).length - 1,
					// _datul_confirm_check: util.datulConfirmCheck(row.tglDatul, row.tglDatulAw0, row.tglDatulM, req.body._petugas_ID),
					// _datul_confirm_check: checkStatusDatul(row.flagHhak, row.tglMeninggal, row.tgl_pensiun, row.tglDatul, row.tglDatulAw0, row.tglDatulM),
					_datul_confirm_check: dwdAtauTidak(row.flagHak, row.tglMeninggal, row.tglPensiun),
					_photo: utilLaporan.checkPhoto(row.photoID),
					_finger: utilLaporan.checkFinger(row.finger1)
				}
	})
	.then(function(data){

		if (req.body.status !== null && req.body.status !== 20) {
			data = _.filter(data, (o) => {
				return o._datul_confirm_check === req.body.status
			})
		}

		if (req.body.status === 20) {
			data = _.reject(data, (o) => {
				return o._datul_confirm_check === 10
			})
		}


		const styles = {
			headerDark: {
				fill: {
					fgColor: {
						rgb: 'FFFFFFFF'
					}
				},
				font: {
					color: {
						rgb: 'FF000000'
					},
					sz: 12,
					bold: true
				}
			},
			cellPink: {
				fill: {
					fgColor: {
						rgb: 'FFFFCCFF'
					}
				}
			},
			cellGreen: {
				fill: {
					fgColor: {
						rgb: 'FF00FF00'
					}
				}
			}
		};

		const specification = {
			NO : {
				displayName: 'No',
				width: 30,
				headerStyle: styles.headerDark
			},
			_nik: {
				displayName: 'NIK',
				width: 110,
				headerStyle: styles.headerDark
			},
			_nama: {
				displayName: 'Nama',
				width: 200,
				headerStyle: styles.headerDark
			},
			_photo: {
				displayName: 'Data Foto',
				width: 100,
				headerStyle: styles.headerDark
			},
			_finger: {
				displayName: 'Sidik Jari',
				width: 100,
				headerStyle: styles.headerDark
			},
			_jalan: {
				displayName: 'JALAN',
				width: 120,
				headerStyle: styles.headerDark
			},
			_komp: {
				displayName: 'KOMP',
				width: 120,
				headerStyle: styles.headerDark
			},
			_blok: {
				displayName: 'BLOK',
				width: 120,
				headerStyle: styles.headerDark
			},
			_noalamat: {
				displayName: 'No Rumah',
				width: 50,
				headerStyle: styles.headerDark
			},
			_rt: {
				displayName: 'RT',
				width: 50,
				headerStyle: styles.headerDark
			},
			_rw: {
				displayName: 'RW',
				width: 50,
				headerStyle: styles.headerDark
			},
			_kelurahan: {
				displayName: 'KELURAHAN',
				width: 120,
				headerStyle: styles.headerDark
			},
			_kecamatan: {
				displayName: 'KECAMATAN',
				width: 120,
				headerStyle: styles.headerDark
			},
			_kota: {
				displayName: 'KOTA',
				width: 120,
				headerStyle: styles.headerDark
			},
			_provinsi: {
				displayName: 'PROVINSI',
				width: 120,
				headerStyle: styles.headerDark
			},
			_kodepos: {
				displayName: 'KODEPOS',
				width: 120,
				headerStyle: styles.headerDark
			},
			_telepon_rumah: {
				displayName: 'Telepon Rumah',
				width: 100,
				headerStyle: styles.headerDark
			},
			_hp: {
				displayName: 'HP',
				width: 100,
				headerStyle: styles.headerDark
			},
			_email: {
				displayName: 'Email',
				width: 100,
				headerStyle: styles.headerDark
			},
			_fax: {
				displayName: 'FAX',
				width: 100,
				headerStyle: styles.headerDark
			},
			// _jml_ahli_waris: {
			//   displayName: 'Jumlah Penerus',
			//   width: 110,
			//   headerStyle: styles.headerDark
			// },
			_cabang_p2tel: {
				displayName: 'Cabang P2TEL',
				width: 120,
				headerStyle: styles.headerDark
			},
			// _status_datul: {
			// 	displayName: 'Status Datul',
			// 	width: 120,
			// 	headerStyle: styles.headerDark
			// },
			_datul_confirm_check: {
				displayName: 'Status PMP',
				width: 100,
				headerStyle: styles.headerDark,
				cellFormat: function(value, row) {
					if (value == 10) {
						return 'Daftar Wajib Datul'
					} else if (value == 11) {
						return 'Tangguhan'
					} else if (value == 12) {
						return 'Sudah Meninggal'
					} else if (value == 13) {
						return 'PMP Baru'
					}
				}
			}
		}

		var filename = function() {
			if (req.body.cabangP2tel == null || req.body.cabangP2tel == '') {
				return 'Semua Cabang-' + moment().format('YYYYMMDD')
			} else {
				return  _s.strRightBack(req.body.cabangP2tel,'-') + moment().format('-YYYYMMDD')
			}
		}

		// var something = json2xlsx(config)
		const something = excel.buildExport(
			[{
				name: 'Report',
				data: data,
				specification: specification
			}]
		);
		// console.log(something)
		// res.json(data)
		res.setHeader('Content-Type', 'application/vnd.openxmlformats');
		res.setHeader("Content-Disposition", "attachment; filename=" + "Report.xls")
		// res.attachment('REKAP-' + filename() + '.xls');
		res.end(something, 'binary');



	})
})

report.post('/excel', function(req, res){
	var token = jwt.decode(req.body.token, 'ilhamgantengpisan')
	db.select(
		'pmp._nama as nama',
		'pmp._nik as nik'
	)
	.from('tbl_pmp as pmp')
	.then((data) => {

    var conf ={};

    conf.name = "mysheet";
    conf.cols = [
	    {
	        caption:'Nama',
	        type:'string',
	        width: 28.7109375
	    },
	    {
	        caption:'NIK',
	        type:'string',
	        width:28.7109375
	    }
    ];
    conf.rows = _.map(data, (o) => { return _.values(o) })
    test = _.map(data, (o) => { return _.values(o) })
    var result = nodeExcel.execute(conf);
    // res.send(test)
    // console.log(result)
    res.setHeader('Content-Type', 'application/vnd.openxmlformats');
    res.setHeader("Content-Disposition", "attachment; filename=" + "Report.xlsx");
    res.end(result, 'binary');
	})
});

module.exports = report
