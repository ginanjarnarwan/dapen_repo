var express = require('express')
var datulmandiri = express.Router()
var db = require('knex')(require('../config/db').mysql)
var moment = require('moment')
var bcrypt = require('bcrypt-nodejs')
var _ = require('underscore')
var _S = require('underscore.string')
var jwt = require('jwt-simple')
var NestHydrationJS = require('nesthydrationjs')()
var dateFns = require('date-fns');

var authUtil = require('../util/auth')
var checkDate = require('../util/check-date')
var fieldMaker = require('../util/field-maker')
var checkGender = require('../util/check-gender')
var checkPhoto = require('../util/check-photo')

datulmandiri.post('/', (req, res) => {
	var token = jwt.decode(req.body.token, 'ilhamgantengpisan')
	db.select(
		'pmp.ID AS ID',
		'pmp._nik AS _nik',
		'pmp._naw AS _naw',
		'pmp._kategori AS _kategori',
		'pmp._ktp AS _ktp',
		'pmp._npwp AS _npwp',
		'pmp._tgl_lahir AS _tgl_lahir',
		'pmp._agama AS _agama_ID',
		'ref_agama._agama AS _agama_val',
		'pmp._golongan_darah AS _golongan_darah',
		'pmp._gender AS _gender'
	).from('tbl_pmp as pmp')
	.leftJoin('ref_agama', 'ref_agama.ID', 'pmp._agama')
	.where('pmp.ID', token.tbl_pmp)
	.map((row) => {
		return {
			ID: row.ID,
			NIK: row._nik,
			'NIK / NAW': `${ row._nik } / ${ row._naw }`,
			kategori : row._kategori,
			ktp: row._ktp,
			npwp: row._npwp,
			tanggal_lahir: checkDate(row._tgl_lahir),
			agama: row._agama_ID,
			agama_val: row._agama_val,
			golongan_darah: row._golongan_darah,
			jenis_kelamin: row._gender,
			jenis_kelamin_val: checkGender(row._gender)
		}
	})
	.then((data) => {
		db.select(
			'alamat.ID AS ID',
			'alamat._jalan AS jalan',
			'alamat._komp AS komp',
			'alamat._no as no',
			'alamat._rt as rt',
			'alamat._rw as rw',
			'kelurahan._nama_kelurahan as nama_kelurahan',
			'kecamatan._nama_kecamatan as nama_kecamatan',
			'kota._nama_kota as nama_kota',
			'provinsi._nama_provinsi as nama_provinsi',
			'kelurahan._kode_pos as kode_pos'
		).from('tbl_alamat as alamat')
		.leftJoin('tbl_kelurahan AS kelurahan', 'kelurahan.ID', 'alamat.tbl_kelurahan_ID')
		.leftJoin('tbl_kecamatan as kecamatan', 'kecamatan.ID', 'kelurahan.tbl_kecamatan_ID')
		.leftJoin('tbl_kota as kota', 'kota.ID', 'kecamatan.tbl_kota_ID')
		.leftJoin('tbl_provinsi as provinsi', 'provinsi.ID', 'kota.tbl_provinsi_ID')
		.where('.alamat.tbl_pmp', data[0].ID)
		.where('alamat._default', 1)
		.then((alamat) => {
			db.select(
				'tbl_kontak.ID AS ID',
				'ref_tipe_kontak._kontak_type_name AS tipe_kontak',
				'tbl_kontak._kontak_value AS kontak_value'
			).from('tbl_kontak')
			.leftJoin('ref_tipe_kontak', 'ref_tipe_kontak.ID', 'tbl_kontak.ref_tipe_kontak')
			.where('tbl_alamat', alamat[0].ID)
			.then((kontak) => {
				db.select(
					'ahliwaris.ID AS ID',
					'ahliwaris._nik as _nik',
					'ahliwaris._naw as _naw',
					'ahliwaris._nama AS _nama',
					'ahliwaris.ref_tipe_ahliwaris as ref_tipe_ahliwaris',
					'ahliwaris._tgl_lahir as _tgl_lahir',
					'ahliwaris._tgl_lulus as _tgl_lulus',
					'ahliwaris._tgl_menikah as _tgl_menikah',
					'ahliwaris._tgl_bekerja as _tgl_bekerja',
					'ahliwaris._tgl_menikah_lagi as _tgl_menikah_lagi',
					'ahliwaris._tgl_meninggal as _tgl_meninggal',
					'ahliwaris._tgl_cerai as _tgl_cerai',
					'ahliwaris._gender as _jenis_kelamin',
					'ahliwaris._agama as _agama_ID',
					'ref_agama._agama as _agama_val',
					'ahliwaris._ktp as _ktp',
					'ahliwaris._npwp as _npwp',
					'ahliwaris._golongan_darah as _golongan_darah'

				).from('tbl_calon_pmp as ahliwaris')
				.leftJoin('ref_agama', 'ref_agama.ID', 'ahliwaris._agama')
				.where('_nik', data[0].NIK)
				.whereNot('ahliwaris.ID', data[0].ID)
				.then((ahliwaris) => {

					var awFilter =  _.map(_.reject(ahliwaris, function(o) {
						return ( o._tgl_meninggal != null || (o.ref_tipe_ahliwaris == 4 && dateFns.differenceInDays(new Date(), o._tgl_lahir) > 9131) || o.tglLulus != null || o.tglMenikahLagi != null || o.tglCerai != null ) })
						, function(o){
						return  {
							ID : `${ o._naw }${ o._nik }`,
							nama: o._nama,
							photo : checkPhoto(o.ID),
							'NIK / NAW': `${ o._nik } / ${ o._naw }`,
							ktp: o._ktp,
							tgl_lahir: checkDate(o._tgl_lahir),
							agama_ID: o._agama_ID,
							agama_val: o._agama_val,
							golongan_darah: o._golongan_darah,
							jenis_kelamin: checkGender(o._jenis_kelamin)
						}
					});

					res.send({
						success: true,
						data: {
							data: data,
							alamat: alamat,
							kontak: kontak,
							ahliwaris: awFilter
						}
					})
				})
			})
		})
	})
})

datulmandiri.post('/test', (req, res) => {
	db.select(
		'pmp.ID AS ID',
		'pmp._nik AS _nik',
		'pmp._naw AS _naw',
		'pmp._kategori AS _kategori',
		'pmp._ktp AS _ktp',
		'pmp._npwp AS _npwp',
		'pmp._tgl_lahir AS _tgl_lahir',
		'pmp._agama AS _agama_ID',
		'ref_agama._agama AS _agama_val',
		'pmp._golongan_darah AS _golongan_darah',
		'pmp._gender AS _gender'
	).from('tbl_pmp as pmp')
	.leftJoin('ref_agama', 'ref_agama.ID', 'pmp._agama')
	.where('pmp.ID', req.body.id)
	.map((row) => {
		return {
			ID: row.ID,
			NIK: row._nik,
			'NIK / NAW': `${ row._nik } / ${ row._naw }`,
			kategori : row._kategori,
			ktp: row._ktp,
			npwp: row._npwp,
			tanggal_lahir: checkDate(row._tgl_lahir),
			agama: row._agama_ID,
			agama_val: row._agama_val,
			golongan_darah: row._golongan_darah,
			jenis_kelamin: row._gender,
			jenis_kelamin_val: checkGender(row._gender)
		}
	})
	.then((data) => {
		db.select(
			'alamat.ID AS ID',
			'alamat._jalan AS jalan',
			'alamat._komp AS komp',
			'alamat._no as no',
			'alamat._rt as rt',
			'alamat._rw as rw',
			'kelurahan._nama_kelurahan as nama_kelurahan',
			'kecamatan._nama_kecamatan as nama_kecamatan',
			'kota._nama_kota as nama_kota',
			'provinsi._nama_provinsi as nama_provinsi',
			'kelurahan._kode_pos as kode_pos'
		).from('tbl_alamat as alamat')
		.leftJoin('tbl_kelurahan AS kelurahan', 'kelurahan.ID', 'alamat.tbl_kelurahan_ID')
		.leftJoin('tbl_kecamatan as kecamatan', 'kecamatan.ID', 'kelurahan.tbl_kecamatan_ID')
		.leftJoin('tbl_kota as kota', 'kota.ID', 'kecamatan.tbl_kota_ID')
		.leftJoin('tbl_provinsi as provinsi', 'provinsi.ID', 'kota.tbl_provinsi_ID')
		.where('.alamat.tbl_pmp', data[0].ID)
		.where('alamat._default', 1)
		.then((alamat) => {
			db.select(
				'tbl_kontak.ID AS ID',
				'ref_tipe_kontak._kontak_type_name AS tipe_kontak',
				'tbl_kontak._kontak_value AS kontak_value'
			).from('tbl_kontak')
			.leftJoin('ref_tipe_kontak', 'ref_tipe_kontak.ID', 'tbl_kontak.ref_tipe_kontak')
			.where('tbl_alamat', alamat[0].ID)
			.then((kontak) => {
				db.select(
					'ahliwaris.ID AS ID',
					'ahliwaris._nik as _nik',
					'ahliwaris._naw as _naw',
					'ahliwaris._nama AS _nama',
					'ahliwaris.ref_tipe_ahliwaris as ref_tipe_ahliwaris',
					'ahliwaris._tgl_lahir as _tgl_lahir',
					'ahliwaris._tgl_lulus as _tgl_lulus',
					'ahliwaris._tgl_menikah as _tgl_menikah',
					'ahliwaris._tgl_bekerja as _tgl_bekerja',
					'ahliwaris._tgl_menikah_lagi as _tgl_menikah_lagi',
					'ahliwaris._tgl_meninggal as _tgl_meninggal',
					'ahliwaris._tgl_cerai as _tgl_cerai',
					'ahliwaris._gender as _jenis_kelamin',
					'ahliwaris._agama as _agama_ID',
					'ref_agama._agama as _agama_val',
					'ahliwaris._ktp as _ktp',
					'ahliwaris._npwp as _npwp',
					'ahliwaris._golongan_darah as _golongan_darah'

				).from('tbl_calon_pmp as ahliwaris')
				.leftJoin('ref_agama', 'ref_agama.ID', 'ahliwaris._agama')
				.where('_nik', data[0].NIK)
				.whereNot('ahliwaris.ID', data[0].ID)
				.then((ahliwaris) => {

					var awFilter =  _.map(_.reject(ahliwaris, function(o) {
						return ( o._tgl_meninggal != null || (o.ref_tipe_ahliwaris == 4 && dateFns.differenceInDays(new Date(), o._tgl_lahir) > 9131) || o.tglLulus != null || o.tglMenikahLagi != null || o.tglCerai != null ) })
						, function(o){
						return  {
							ID : `${ o._naw }${ o._nik }`,
							nama: o._nama,
							photo : checkPhoto(o.ID),
							'NIK / NAW': `${ o._nik } / ${ o._naw }`,
							ktp: o._ktp,
							tgl_lahir: checkDate(o._tgl_lahir),
							agama_ID: o._agama_ID,
							agama_val: o._agama_val,
							golongan_darah: o._golongan_darah,
							jenis_kelamin: checkGender(o._jenis_kelamin)
						}
					});

					res.send({
						success: true,
						data: {
							data: data,
							alamat: alamat,
							kontak: kontak,
							ahliwaris: awFilter
						}
					})
				})
			})
		})
	})
})

module.exports = datulmandiri;