var express = require('express')
var api = express.Router()
var db = require('knex')(require('../config/db').mysql)
var moment = require('moment')
var bcrypt = require('bcrypt-nodejs')
var _ = require('underscore')
var _S = require('underscore.string')
var jwt = require('jwt-simple')

var checkPhoto = require('../util/check-photo')
var checkDate = require('../util/check-date')


api.post('/', (req, res) => {
	db('tbl_catatan')
	.insert({
		val: req.body.val,
		code: req.body.code,
		id_pmp: req.body.id_pmp
	})
	.then(() => {
		res.send({
			success: true
		})
	})
})

module.exports = api