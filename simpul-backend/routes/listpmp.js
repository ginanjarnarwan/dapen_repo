var express = require('express')
var listpmp = express.Router()
var db = require('knex')(require('../config/db').mysql)
var moment = require('moment')
var _ = require('underscore')
var _S = require('underscore.string')
var jwt = require('jwt-simple')
var NestHydrationJS = require('nesthydrationjs')()

var datulConfirmCheck = require('../util/datul-confirm-check')
var checkPhoto = require('../util/check-photo')
var checkStatusDatul = require('../util/check-status-datul')

module.exports = listpmp