var express = require('express')
var upload = express.Router()
var db = require('knex')(require('../config/db').mysql)
var moment = require('moment')
var bcrypt = require('bcrypt-nodejs')
var _ = require('underscore')
var _S = require('underscore.string')
var jwt = require('jwt-simple')
var NestHydrationJS = require('nesthydrationjs')()
var dateFns = require('date-fns');
var multer = require('multer')
var path = require('path')

var authUtil = require('../util/auth')
var checkDate = require('../util/check-date')
var fieldMaker = require('../util/field-maker')
var checkGender = require('../util/check-gender')
var checkPhoto = require('../util/check-photo')
const checkAvatar = require('../util/check-avatar');

var storagePhoto = multer.diskStorage({
  destination: './public/photo_profile',
  filename: function(req, file, cb) {
    cb(null, req.body.id_pmp + '.jpg')
  },
  onError: function(err, next) {
    res.send({
      success: false,
      massage: err
    })
  }
})

var uploadPhoto = multer({ storage: storagePhoto })

upload.post('/photo', uploadPhoto.single('photo_profile'), (req, res) => {
  res.send({
    success: true
  })
})

var storageDokumen = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, './public/dokumen/' + req.body.type)
  },
  filename: function(req, file, cb) {
    cb(null, req.body.id_pmp + path.extname(file.originalname))
  },
  onError: function(err, next) {
    res.send({
      success: false,
      massage: err
    })
  }
})

var uploadDokumen = multer({ storage: storageDokumen })

upload.post('/dokumen', uploadDokumen.single('dokumen'), (req, res) => {
  // console.log(req.body)
  res.send({
    success: true
  })
})

const storageAvatar = multer.diskStorage({
	destination: function(req, file, cb) {
		cb(null, './public/avatar')
	},
	filename: function(req, file, cb) {
		// cb(null, `${req.body.id_pmp}_${Date.now()}.jpg`);
		cb(null, `${req.body.id_pmp}.jpg`);
	}
});

const uploadAvatar = multer({ storage: storageAvatar }).single('avatar');

upload.post('/avatar', (req, res) => {
	uploadAvatar(req, res, (err) => {
		if (err){
			console.log("error multer on post avatar api");
			console.log(err)
		} else {
			res.send({
				success: true
			})
		}
	})
});

upload.get('/avatar/:id', (req, res) => {
	res.send({
		path: checkAvatar(req.params.id)
	})
});

module.exports = upload;