var express = require('express')
var masterlokasi = express.Router()
var db = require('knex')(require('../config/db').mysql)
var NestHydrationJS = require('nesthydrationjs')()

masterlokasi.get('/', (req, res) => {
	db('tbl_provinsi')
	.then((data) => {
		res.send(data)
	})
})

masterlokasi.get('/kota/:id', (req,res) => {
	db.select()
	.from('tbl_kota')
	.where('tbl_provinsi_ID', req.params.id)
	.then((data) => {
		res.send(data)
	})
})

masterlokasi.get('/kecamatan/:id', (req,res) => {
	db.select(
		'tbl_kecamatan.ID As _kecamatanID',
		'tbl_kecamatan._nama_kecamatan AS _namaKecamatan',
		'tbl_kelurahan.ID AS _kelurahan__kelurahanID',
		'tbl_kelurahan._nama_kelurahan AS _kelurahan__namaKelurahan',
    'tbl_kelurahan._kode_pos AS _kelurahan_kodepos',
    'tbl_kelurahan._tbl_cabang_p2tel AS _kelurahan_p2tel'
	)
	.from('tbl_kecamatan')
	.where('tbl_kota_ID', req.params.id)
	.leftJoin('tbl_kelurahan', 'tbl_kelurahan.tbl_kecamatan_ID', 'tbl_kecamatan.ID')
	.then(NestHydrationJS.nest)
	.then((data) => {
		res.send(data)
	})
})

masterlokasi.get('/kelurahan/:id', (req, res) => {
	db.select()
	.from('tbl_kelurahan')
	.where('tbl_kecamatan_ID', req.params.id)
	.then((data) => {
		res.send(data)
	})
})

masterlokasi.get('/listcabang', (req, res) => {
  db.select(
   'tbl_cabang_p2tel._kode_p2tel as kode_p2tel',
   'tbl_cabang_p2tel._nama_cabang as nama_cabang'
  )
  .from('tbl_cabang_p2tel')
  .then((data) => {
    res.send({
      success: true,
      data: data
    })
  })
})

masterlokasi.get('/all', (req, res) => {
	db
		.select(
			'prov.ID as prov_id',
			'prov._nama_provinsi as prov_nama',
			'kota.ID as kota_id',
			'keca.ID as kecamatan_id',
			'kelu.ID as kelurahan_id'
		)
		.from('tbl_provinsi as prov')
		.leftJoin('tbl_kota as kota', 'kota.tbl_provinsi_ID', 'prov.ID')
		.leftJoin('tbl_kecamatan as keca', 'keca.tbl_kota_ID', 'kota.ID')
		.leftJoin('tbl_kelurahan as kelu', 'kelu.tbl_kecamatan_ID', 'keca.ID')
		.then((data) => {
			console.log('panjang gila');
			console.log(data.length);
			res.send({
				success: true,
				data: data
			})
		})
});

masterlokasi.get('/all/provinsi/:id', (req, res) => {
	db
		.select(
			'prov.ID as _prov__id',
			'prov._nama_provinsi as _prov__nama',
			'kota.ID as _prov__kota__id',
			'kota._nama_kota as _prov__kota__nama',
			'keca.ID as _prov__kota__kecamatan__id',
			'keca._nama_kecamatan as _prov__kota__kecamatan__nama',
			'kelu.ID as _prov__kota__kecamatan__kelurahan__id',
			'kelu._nama_kelurahan as _prov__kota__kecamatan__kelurahan__nama',
			'kelu._kode_pos as _prov__kota__kecamatan__kelurahan__kodepos'
		)
		.from('tbl_provinsi as prov')
		.leftJoin('tbl_kota as kota', 'kota.tbl_provinsi_ID', 'prov.ID')
		.leftJoin('tbl_kecamatan as keca', 'keca.tbl_kota_ID', 'kota.ID')
		.leftJoin('tbl_kelurahan as kelu', 'kelu.tbl_kecamatan_ID', 'keca.ID')
		.where('prov.ID', req.params.id)
		.then(NestHydrationJS.nest)
		.then((data) => {
			console.log('panjang gila');
			console.log(data.length);
			res.send({
				success: true,
				data: data
			})
		})
});




module.exports = masterlokasi;