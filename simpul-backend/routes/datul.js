var express = require('express')
var datul = express.Router()
var db = require('knex')(require('../config/db').mysql)
var moment = require('moment')
var bcrypt = require('bcrypt-nodejs')
var _ = require('underscore')
var _S = require('underscore.string')
var jwt = require('jwt-simple')
var eachAsync = require('each-async')
var NestHydrationJS = require('nesthydrationjs')()
var dateFns = require('date-fns');

var path = require('path');
var ejs = require('ejs');
var fs = require('fs');
var pdf = require('html-pdf');
var html = fs.readFileSync('./views/bukti_datul.ejs', 'utf-8');
var html2 = fs.readFileSync('./views/bukti_datul_2.ejs', 'utf-8');


var datulConfig = require('../config/datul-config')
var authUtil = require('../util/auth')
var checkPhoto = require('../util/check-photo')
var checkNewVal = require('../util/check-new-val')
var checkDokumen = require('../util/check-document')
var checkDate = require('../util/check-date')
var checkStatusDatul = require('../util/check-status-datul')
var checkZero = require('../util/check-zero')

datul.post('/mandiri', (req, res) => {
  db('tbl_transaksi_datul_mandiri')
	.insert({
		_wakil_datul: 'Peserta',
		_keterangan_datul: 'datul mandiri',
		_tgl_datul: new Date(),
		tbl_pmp: req.body.id_user,
		tbl_user: req.body.id_user,
		createdAt: new Date(),
		updatedAt: new Date()
	}).then(() => {
    res.send({
    success: true
    })
	}).catch((err) => {
    res.send({
      success: false,
      msg: err
    })
  })
})

datul.post('/anggota', (req,res) => {
  if (req.body.ahliwaris === 0) {
      db('tbl_transaksi_datul_aw0')
      .insert({
        _wakil_datul: 'P2TEL',
        _keterangan_datul: req.body.keterangan_datul,
        _tgl_datul: new Date(),
        tbl_pmp: req.body.id_pmp,
        tbl_user: req.body.id_user,
        createdAt: new Date(),
        updatedAt: new Date()
      }).then(() => {
    res.send({
        success: true
      })
    }).catch((err) => {
      res.send({
        success: false,
        msg: err
      })
    })
  } else {
     db('tbl_transaksi_datul')
      .insert({
        _wakil_datul: 'P2TEL',
        _keterangan_datul: req.body.keterangan_datul,
        _tgl_datul: new Date(),
        tbl_pmp: req.body.id_pmp,
        tbl_user: req.body.id_user,
        createdAt: new Date(),
        updatedAt: new Date()
      }).then(() => {
    res.send({
        success: true
      })
    }).catch((err) => {
      res.send({
        success: false,
        msg: err
      })
    })
  }
})

datul.post('/dapentel', (req, res) => {

  if (req.body.petugas_id === 1) {
    db('tbl_transaksi_datul')
      .insert({
        _wakil_datul: '',
        _keterangan_datul: req.body.keterangan_datul,
        _tgl_datul: new Date(),
        tbl_pmp: req.body.id_pmp,
        tbl_user: req.body.id_user,
        createdAt: new Date(),
        updatedAt: new Date()
      }).then(() => {
    res.send({
        success: true
      })
    }).catch((err) => {
      res.send({
        success: false,
        message: err
      })
    })
  } else {
    res.send({
      success: false,
      message: 'unauthorized user'
    })
  }

})

datul.post('/web', (req, res) => {
  var decoded = jwt.decode(req.body.token, 'ilhamgantengpisan')

  let biodataCheck = _.where(req.body.biodata, { confirm: true })
  let biodataUbah = _.map(req.body.biodata, (o) => {
    return {
      kode_laporan: o.kode,
      data_perubahan: o.new_value,
      tbl_pmp: req.body.id_pmp,
      report_at: new Date(),
      report_by: decoded._username
    }
  })
  if (biodataCheck.length !== 0) {
    eachAsync(_.filter(biodataUbah, (o) => { return o.data_perubahan !== null }), (item, index, done) => {

      db('trx_biodata')
      .insert(item)
      .then(() => {
        // done()
      })
    })
  }

  let alamatCheck = _.where(req.body.alamat, { confirm: true })
  let alamatUbah = {
    id_alamat: req.body.alamatinit.id,
    _jalan: checkNewVal(req.body.alamatinit.jalan, req.body.alamat.jalan.new_value, req.body.alamat.jalan.confirm),
    _komp: checkNewVal(req.body.alamatinit.komp, req.body.alamat.komp.new_value, req.body.alamat.komp.confirm),
    _blok: checkNewVal(req.body.alamatinit.blok, req.body.alamat.blok.new_value, req.body.alamat.blok.confirm),
    _no: checkNewVal(req.body.alamatinit.no, req.body.alamat.no.new_value, req.body.alamat.no.confirm),
    _rt: checkNewVal(req.body.alamatinit.rt, req.body.alamat.rt.new_value, req.body.alamat.rt.confirm),
    _rw: checkNewVal(req.body.alamatinit.rw, req.body.alamat.rw.new_value, req.body.alamat.rw.confirm),
    tbl_pmp: req.body.id_pmp,
    tbl_kelurahan_ID: checkNewVal(req.body.alamatinit.kodeKelurahan, req.body.alamat.kelurahan.new_value, req.body.alamat.wilayah.confirm),
    _default: req.body.alamatinit.default,
    _no_urut_alamat: req.body.alamatinit.id.split('.')[2],
    kode_laporan: 7001,
    reported_by: decoded._username,
    reported_at: new Date()
  }

  if (alamatCheck.length !== 0) {
    // console.log('alamat')
    db('trx_alamat')
    .insert(alamatUbah)
    .then(() => {

    })
    .catch((err) => { console.log(err)})
  }

  let kontakConfirm = _.map(req.body.kontak, (val) => {
    return [val.confirm]
  })
  let kontakConfirm2 = _.flatten(kontakConfirm)
  let kontakUbah = _.filter(req.body.kontak, (o) => {
    return( o.confirm === true && (o.new_value === null || o.new_value !== ''))
  })
  let kontakUbah2 = _.map(kontakUbah, (o) => {
    return {
      id_kontak: o.id,
      kontak_value: o.new_value,
      ref_tipe_kontak: Number(o.ref_tipe_kontak),
      tbl_alamat: _S.strLeftBack(o.id, '.'),
      no_urut_kontak: Number(_S.strRightBack(o.id, '.')),
      reported_at: new Date(),
      reported_by: decoded._username
    }
  })

  if ( _.contains(kontakConfirm2, true)) {
    eachAsync(kontakUbah2, (item, index, done) => {

      db('trx_kontak')
      .insert(item)
      .then(() => {

      })
      .catch((err) => { console.log(err)})
    })
  }


  //P2TEL
  if (decoded.ref_user_level === 5) {
    if (_.size(req.body.ahliwaris) === 0) {
      db('tbl_transaksi_datul_aw0')
      .insert({
        _wakil_datul: null,
        _keterangan_datul: null,
        _tgl_datul: new Date(),
        _id_Transaksi: null,
        _verifikasi: null,
        tbl_pmp: req.body.id_pmp,
        _petugas_p2tel: req.body.verifikasi.petugas.new_value,
        tbl_user: decoded._username,
        createdAt: new Date(),
        updatedAt: new Date(),
        tgl_dikunjungi: req.body.verifikasi.waktu.new_value,
        cara_verifikasi: req.body.verifikasi.cara.new_value,
        catatan: req.body.catatan,
        petugas_datul: req.body.petugas_datul
      })
      .then(() => {

      })
    } else {
      db('tbl_transaksi_datul')
      .insert({
        _wakil_datul: null,
        _keterangan_datul: null,
        _tgl_datul: new Date(),
        _id_Transaksi: null,
        _verifikasi: null,
        tbl_pmp: req.body.id_pmp,
        _petugas_p2tel: req.body.verifikasi.petugas.new_value,
        tbl_user: decoded._username,
        createdAt: new Date(),
        updatedAt: new Date(),
        tgl_dikunjungi: req.body.verifikasi.waktu.new_value,
        cara_verifikasi: req.body.verifikasi.cara.new_value,
        catatan: req.body.catatan,
        petugas_datul: req.body.petugas_datul
      })
      .then(() => {
        db('trx_ahliwaris')
        .insert({
          val: JSON.stringify(req.body.ahliwaris)
        })
        .then(() => { })
      })
    }
  } else if (decoded.ref_user_level !== 5) {
    if (decoded._petugas_ID === 1 && (req.body.datafr_mandiri >= 1 || (_.size(req.body.ahliwaris) === 0 && req.body.datafr_aw0 >= 1))) {
		db('tbl_transaksi_datul')
          .insert({
            _wakil_datul: null,
            _keterangan_datul: null,
            _tgl_datul: new Date(),
            _id_Transaksi: null,
            _verifikasi: null,
            tbl_pmp: req.body.id_pmp,
            _petugas_p2tel: req.body.verifikasi.petugas.new_value,
            tbl_user: decoded._username,
            createdAt: new Date(),
            updatedAt: new Date(),
            tgl_dikunjungi: req.body.verifikasi.waktu.new_value,
            cara_verifikasi: req.body.verifikasi.cara.new_value,
            catatan: req.body.catatan,
            petugas_datul: req.body.petugas_datul
          })
          .then(() => {
            db('trx_ahliwaris')
            .insert({
              val: JSON.stringify(req.body.ahliwaris)
            })
            .then(() => { })
          })
    }else if (decoded._petugas_ID === 1) {
      db('tbl_transaksi_datul_mandiri')
      .insert({
        _wakil_datul: null,
        _keterangan_datul: null,
        _tgl_datul: new Date(),
        _id_Transaksi: null,
        _verifikasi: null,
        tbl_pmp: req.body.id_pmp,
        _petugas_p2tel: req.body.verifikasi.petugas.new_value,
        tbl_user: decoded._username,
        createdAt: new Date(),
        updatedAt: new Date(),
        tgl_dikunjungi: req.body.verifikasi.waktu.new_value,
        cara_verifikasi: req.body.verifikasi.cara.new_value,
        catatan: req.body.catatan,
        petugas_datul: req.body.petugas_datul
      })
      .then(() => {
        db('trx_ahliwaris')
        .insert({
          val: JSON.stringify(req.body.ahliwaris)
        })
        .then(() => { })
      })
    } else {
      if (req.body.verifikasi.cara.new_value == 4) {
         db('tbl_transaksi_datul_mandiri')
        .insert({
          _wakil_datul: null,
          _keterangan_datul: null,
          _tgl_datul: new Date(),
          _id_Transaksi: null,
          _verifikasi: null,
          tbl_pmp: req.body.id_pmp,
          _petugas_p2tel: decoded._username,
          tbl_user: decoded._username,
          createdAt: new Date(),
          updatedAt: new Date(),
          tgl_dikunjungi: req.body.verifikasi.waktu.new_value,
          cara_verifikasi: req.body.verifikasi.cara.new_value,
          catatan: req.body.catatan,
          petugas_datul: req.body.petugas_datul
        })
        .then(() => {
          db('trx_ahliwaris')
          .insert({
            val: JSON.stringify(req.body.ahliwaris)
          })
          .then(() => { })
        })
      } else if (req.body.verifikasi.cara.new_value == 5) {
        if (req.body.ahliwaris.length === 0) {
          db('tbl_transaksi_datul_aw0')
          .insert({
            _wakil_datul: null,
            _keterangan_datul: null,
            _tgl_datul: new Date(),
            _id_Transaksi: null,
            _verifikasi: null,
            tbl_pmp: req.body.id_pmp,
            _petugas_p2tel: req.body.verifikasi.petugas.new_value,
            tbl_user: decoded._username,
            createdAt: new Date(),
            updatedAt: new Date(),
            tgl_dikunjungi: req.body.verifikasi.waktu.new_value,
            cara_verifikasi: req.body.verifikasi.cara.new_value,
            catatan: req.body.catatan,
            petugas_datul: req.body.petugas_datul
          })
          .then(() => {
            db('trx_ahliwaris')
            .insert({
              val: JSON.stringify(req.body.ahliwaris)
            })
            .then(() => { })
          })
        } else {
          db('tbl_transaksi_datul')
          .insert({
            _wakil_datul: null,
            _keterangan_datul: null,
            _tgl_datul: new Date(),
            _id_Transaksi: null,
            _verifikasi: null,
            tbl_pmp: req.body.id_pmp,
            _petugas_p2tel: req.body.verifikasi.petugas.new_value,
            tbl_user: decoded._username,
            createdAt: new Date(),
            updatedAt: new Date(),
            tgl_dikunjungi: req.body.verifikasi.waktu.new_value,
            cara_verifikasi: req.body.verifikasi.cara.new_value,
            catatan: req.body.catatan,
            petugas_datul: req.body.petugas_datul
          })
          .then(() => {
            db('trx_ahliwaris')
            .insert({
              val: JSON.stringify(req.body.ahliwaris)
            })
            .then(() => { })
          })
        }
      }
    }
  }
  try {
    res.send({
      success: true
      // data: kontakUbah2
    })
  }
  catch (err) {
    console.log(err)
  }

  // if (req.body.ahliwaris.length === 0) {

  // } else {

  // }
  // console.log(req.body)
  // res.send(req.body)
  // var decoded = jwt.decode(req.body.token, 'ilhamgantengpisan')
  // if (req.body.jumlah_ahliwaris == 0) {
  //   db('tbl_transaksi_datul_aw0')
  //   .insert({
  //     _wakil_datul: null,
  //     _keterangan_datul: null,
  //     _tgl_datul: new Date(),
  //     _id_Transaksi: null,
  //     _verifikasi: null,
  //     tbl_pmp: req.body.id_pmp,
  //     _petugas_p2tel: req.body.petugas_p2tel,
  //     tbl_user: decoded._username,
  //     createdAt: new Date(),
  //     updatedAt: new Date(),
  //     tgl_dikunjungi: req.body.tgl_dikunjungi,
  //     cara_verifikasi: req.body.cara_verifikasi,
  //   })
  //   .then(() => {
  //     res.send({
  //       success: true
  //     })
  //   })
  // } else {
  //   db('tbl_transaksi_datul')
  //   .insert({
  //     _wakil_datul: null,
  //     _keterangan_datul: null,
  //     _tgl_datul: new Date(),
  //     _id_Transaksi: null,
  //     _verifikasi: null,
  //     tbl_pmp: req.body.id_pmp,
  //     _petugas_p2tel: req.body.petugas_p2tel,
  //     tbl_user: decoded._username,
  //     createdAt: new Date(),
  //     updatedAt: new Date(),
  //     tgl_dikunjungi: req.body.tgl_dikunjungi,
  //     cara_verifikasi: req.body.cara_verifikasi,
  //   })
  //   .then(() => {
  //     res.send({
  //       success: true
  //     })
  //   })
  // }
})

datul.post('/buktidatul', (req, res) => {
  db.select(
		'pmp._nik as nik',
		'pmp._nama as nama',
		'alamat._jalan as jalan',
		'alamat._komp as komp',
		'alamat._blok as blok',
		'alamat._no as no',
		'alamat._rt as rt',
		'alamat._rw as rw',
		'kelurahan._nama_kelurahan as kelurahan',
		'kelurahan._kode_pos as kodepos',
		'kecamatan._nama_kecamatan as kecamatan',
		'kota._nama_kota as kota',
		'provinsi._nama_provinsi as provinsi',
		'p2tel._nama_cabang as namaCabang',
		'transaksi._tgl_datul as tglDatul',
		'transaksi.tbl_user as username',
		'user._petugas as namaBank',
		'user._cabang_petugas as bankCabang',
		'user.tbl_cabang_p2tel as cabangP2tel'
	)
	.from('tbl_transaksi_datul as transaksi')
	.leftJoin('tbl_pmp as pmp', 'pmp.ID', 'transaksi.tbl_pmp')
	.leftJoin('tbl_alamat AS alamat','alamat.tbl_pmp','pmp.ID')
	.leftJoin('tbl_kelurahan AS kelurahan', 'kelurahan.ID', 'alamat.tbl_kelurahan_ID')
	.leftJoin('tbl_cabang_p2tel AS p2tel', 'p2tel._kode_p2tel', 'kelurahan._tbl_cabang_p2tel')
  .leftJoin('tbl_kecamatan as kecamatan', 'kecamatan.ID', 'kelurahan.tbl_kecamatan_ID')
  .leftJoin('tbl_kota as kota', 'kota.ID', 'kecamatan.tbl_kota_ID')
  .leftJoin('tbl_provinsi as provinsi', 'provinsi.ID', 'kota.tbl_provinsi_ID')
  .leftJoin('tbl_user as user', 'user._username', 'transaksi.tbl_user')
	.where('transaksi.tbl_pmp', req.body.id_pmp)
	.then((data) => {
		console.log(data)

		var template = ejs.render(html, {
			// date : req.body.date,
			data : data,
			datulBerikutnya : datulConfig.nextDatul(data[0].tglDatul),
			// CATATAN
			username: data[0].username,
			namaBank: data[0].namaBank,
			bankCabang: data[0].bankCabang,
			cabangP2tel: data[0].cabangP2tel,
			tglDatul: moment(data[0].tglDatul).locale('id').format('DD MMMM YYYY')

		});
		pdf.create(template, {
			'height' : '297mm',
			'width': '210mm',
			'base' : 'http://localhost:3000/'
		})
		.toBuffer(function(err, buffer){
			res.set({
				'Content-type': 'application/pdf',
				'Content-disposition': 'attachment; filename=' + req.body.id_pmp + '.pdf'
			});
        console.log(err)
  			res.send(buffer);
		});
	})
})

datul.post('/buktidatulkonfirmasidapen', (req, res) => {
  db.select(
		'pmp._nik as nik',
		'pmp._nama as nama',
		'alamat._jalan as jalan',
		'alamat._komp as komp',
		'alamat._blok as blok',
		'alamat._no as no',
		'alamat._rt as rt',
		'alamat._rw as rw',
		'kelurahan._nama_kelurahan as kelurahan',
		'kelurahan._kode_pos as kodepos',
		'kecamatan._nama_kecamatan as kecamatan',
		'kota._nama_kota as kota',
		'provinsi._nama_provinsi as provinsi',
		'p2tel._nama_cabang as namaCabang',
		'transaksi._tgl_datul as tglDatul',
		'transaksi.tbl_user as username',
		'user._petugas as namaBank',
		'user._cabang_petugas as bankCabang',
		'user.tbl_cabang_p2tel as cabangP2tel'
	)
	.from('tbl_transaksi_datul_aw0 as transaksi')
	.leftJoin('tbl_pmp as pmp', 'pmp.ID', 'transaksi.tbl_pmp')
	.leftJoin('tbl_alamat AS alamat','alamat.tbl_pmp','pmp.ID')
	.leftJoin('tbl_kelurahan AS kelurahan', 'kelurahan.ID', 'alamat.tbl_kelurahan_ID')
	.leftJoin('tbl_cabang_p2tel AS p2tel', 'p2tel._kode_p2tel', 'kelurahan._tbl_cabang_p2tel')
  .leftJoin('tbl_kecamatan as kecamatan', 'kecamatan.ID', 'kelurahan.tbl_kecamatan_ID')
  .leftJoin('tbl_kota as kota', 'kota.ID', 'kecamatan.tbl_kota_ID')
  .leftJoin('tbl_provinsi as provinsi', 'provinsi.ID', 'kota.tbl_provinsi_ID')
  .leftJoin('tbl_user as user', 'user._username', 'transaksi.tbl_user')
	.where('transaksi.tbl_pmp', req.body.id_pmp)
	.then((data) => {

		var template = ejs.render(html, {
			// date : req.body.date,
			data : data,
			datulBerikutnya : datulConfig.nextDatul(data[0].tglDatul),
			// CATATAN
			username: data[0].username,
			namaBank: data[0].namaBank,
			bankCabang: data[0].bankCabang,
			cabangP2tel: data[0].cabangP2tel,
			tglDatul: moment(data[0].tglDatul).locale('id').format('DD MMMM YYYY')

		});
		pdf.create(template, {
			'height' : '297mm',
			'width': '210mm',
			'base' : 'http://localhost:3000/'
		})
		.toBuffer(function(err, buffer){
			res.set({
				'Content-type': 'application/pdf',
				'Content-disposition': 'attachment; filename=' + req.body.id_pmp + '.pdf'
			});
        console.log(err)
  			res.send(buffer);
		});
	})
})

datul.post('/buktidatul2', (req,res) => {
	// var token = jwt.decode(req.body.token, 'ilhamgantengpisan')
	var trDatul     	=   db.select(db.raw('ID, max(_tgl_datul) AS _tgl_datul, tbl_pmp, cara_verifikasi, petugas_datul'))
						.from('tbl_transaksi_datul').groupBy('ID','tbl_pmp').as('transaksi');
	var trDatulaw0  	=   db.select(db.raw('ID, max(_tgl_datul) AS _tgl_datul, tbl_pmp, cara_verifikasi, petugas_datul'))
						.from('tbl_transaksi_datul_aw0').groupBy('ID','tbl_pmp').as('transaksiAW0');
	var trDatulM    	=   db.select(db.raw('ID, max(_tgl_datul) AS _tgl_datul, tbl_pmp, cara_verifikasi, petugas_datul'))
						.from('tbl_transaksi_datul_mandiri').groupBy('ID','tbl_pmp').as('transaksiM');
	db.select(
		'pmp.ID AS ID',
		'pmp._nik AS _nik',
		'pmp._naw AS _naw',
		'pmp._nama AS _nama',
		'pmp._kategori AS _kategori',
		'pmp._ktp AS _ktp',
		'pmp._npwp AS _npwp',
		'pmp._tgl_lahir AS _tgl_lahir',
		'pmp._agama AS _agama_ID',
		'ref_agama._agama AS _agama_val',
		'pmp._golongan_darah AS _golongan_darah',
		'pmp._gender AS _gender',
		'pmp._flag_hak AS _flag_hak',
		'pmp._tgl_meninggal as _tgl_meninggal',
		'pmp._tgl_pensiun as _tgl_pensiun',
		'transaksi._tgl_datul as _tgl_datul',
		'transaksiAW0._tgl_datul as _tgl_datul_aw0',
		'transaksiM._tgl_datul as _tgl_datul_m',
		'transaksi.cara_verifikasi as cara_verifikasi_3',
		'transaksiAW0.cara_verifikasi as cara_verifikasi_2',
		'transaksiM.cara_verifikasi as cara_verifikasi_1',
		'transaksi.petugas_datul as petugas_datul_3',
		'transaksiAW0.petugas_datul as petugas_datul_2',
		'transaksiM.petugas_datul as petugas_datul_1',
		'p2tel._nama_cabang AS _nama_cabang'
	).from('tbl_pmp as pmp')
	.leftJoin('ref_agama', 'ref_agama.ID', 'pmp._agama')
	.leftJoin(trDatul, 'transaksi.tbl_pmp', 'pmp.ID')
	.leftJoin(trDatulaw0, 'transaksiAW0.tbl_pmp', 'pmp.ID')
	.leftJoin(trDatulM, 'transaksiM.tbl_pmp', 'pmp.ID')
	.leftJoin('tbl_alamat AS alamat','alamat.tbl_pmp','pmp.ID')
	.leftJoin('tbl_kelurahan AS kelurahan', 'kelurahan.ID', 'alamat.tbl_kelurahan_ID')
	.leftJoin('tbl_cabang_p2tel AS p2tel', 'p2tel._kode_p2tel', 'kelurahan._tbl_cabang_p2tel')
	.where('pmp.ID', req.body.id_pmp)
	.where('alamat._default', 1)
	.map((row) => {
		return {
			id: row.ID,
			nik: row._nik,
			naw: row._naw,
			nik_naw: `${ row._nik }_${ row._naw }`,
			nama: row._nama,
			kategori : row._kategori,
			photo: checkPhoto(row.ID),
			ktp: row._ktp,
			ktp_attch: checkDokumen(row.ID, 'ktp'),
			npwp: row._npwp,
			npwp_attch: checkDokumen(row.ID, 'npwp'),
			tanggal_lahir: checkDate(row._tgl_lahir),
			agama: row._agama_ID,
			agama_val: row._agama_val,
			golongan_darah: row._golongan_darah,
			jenis_kelamin: row._gender,
			status: checkStatusDatul(row._flag_hak, row._tgl_meninggal, row.tgl_pensiun, row._tgl_datul, row._tgl_datul_aw0, row._tgl_datul_m),
			tgl_datul_1:  checkDate(row._tgl_datul_m),
			tgl_datul_2: checkDate(row._tgl_datul_aw0),
			tgl_datul_3: checkDate(row._tgl_datul),
			cara_verifikasi_1: row.cara_verifikasi_1,
			cara_verifikasi_2: row.cara_verifikasi_2,
			cara_verifikasi_3: row.cara_verifikasi_3,
			petugas_datul_1: row.petugas_datul_1,
			petugas_datul_2: row.petugas_datul_2,
			petugas_datul_3: row.petugas_datul_3,
			nama_cabang: row._nama_cabang
		}
	})
	.then((data) => {

		db.select(
			'alamat.ID AS _id',
			'alamat._jalan AS _jalan',
			'alamat._komp AS _komp',
			'alamat._blok AS _blok',
			'alamat._no as _no',
			'alamat._rt as _rt',
			'alamat._rw as _rw',
			'kelurahan._nama_kelurahan as _namakelurahan',
			'kecamatan._nama_kecamatan as _namakecamatan',
			'kota._nama_kota as _namakota',
			'provinsi._nama_provinsi as _namaprovinsi',
			'kelurahan._kode_pos as _kodepos',
			'kelurahan.ID as _kodeKelurahan',
			'kecamatan.ID as _kodeKecamatan',
			'kota.ID as _kodeKota',
			'provinsi.ID as _kodeProvinsi',
			'alamat._default as _default',
			'kelurahan._tbl_cabang_p2tel as _cabangp2tel',
			'kontak.ID as _kontak__id',
			'tipeKontak._kontak_type_name AS _kontak__tipekontak',
			'tipeKontak.ID AS _kontak__refTipeKontak',
			'kontak._kontak_value as _kontak__kontakvalue'
		).from('tbl_alamat as alamat')
		.leftJoin('tbl_kelurahan AS kelurahan', 'kelurahan.ID', 'alamat.tbl_kelurahan_ID')
		.leftJoin('tbl_kecamatan as kecamatan', 'kecamatan.ID', 'kelurahan.tbl_kecamatan_ID')
		.leftJoin('tbl_kota as kota', 'kota.ID', 'kecamatan.tbl_kota_ID')
		.leftJoin('tbl_provinsi as provinsi', 'provinsi.ID', 'kota.tbl_provinsi_ID')
		.leftJoin('tbl_kontak AS kontak','kontak.tbl_alamat','alamat.ID')
		.leftJoin('ref_tipe_kontak AS tipeKontak', 'tipeKontak.ID', 'kontak.ref_tipe_kontak')
		.where('alamat.tbl_pmp', data[0].id)
		.then(NestHydrationJS.nest)
		// .where('alamat._default', 1)
		.then((alamat) => {

			db.select(
				'alamat.ID AS _id',
				'alamat._jalan AS _jalan',
				'alamat._komp AS _komp',
				'alamat._blok AS _blok',
				'alamat._no as _no',
				'alamat._rt as _rt',
				'alamat._rw as _rw',
				'kelurahan._nama_kelurahan as _namakelurahan',
				'kecamatan._nama_kecamatan as _namakecamatan',
				'kota._nama_kota as _namakota',
				'provinsi._nama_provinsi as _namaprovinsi',
				'kelurahan._kode_pos as _kodepos',
				'kelurahan.ID as _kodeKelurahan',
				'kecamatan.ID as _kodeKecamatan',
				'kota.ID as _kodeKota',
				'provinsi.ID as _kodeProvinsi',
				'alamat._default as _default',
				'kelurahan._tbl_cabang_p2tel as _cabangp2tel',
				'kontak.ID as _kontak__id',
				'tipeKontak._kontak_type_name AS _kontak__tipekontak',
				'tipeKontak.ID AS _kontak__refTipeKontak',
				'kontak._kontak_value as _kontak__kontakvalue'
			).from('trx_alamat as alamat')
			.leftJoin('tbl_kelurahan AS kelurahan', 'kelurahan.ID', 'alamat.tbl_kelurahan_ID')
			.leftJoin('tbl_kecamatan as kecamatan', 'kecamatan.ID', 'kelurahan.tbl_kecamatan_ID')
			.leftJoin('tbl_kota as kota', 'kota.ID', 'kecamatan.tbl_kota_ID')
			.leftJoin('tbl_provinsi as provinsi', 'provinsi.ID', 'kota.tbl_provinsi_ID')
			.leftJoin('tbl_kontak AS kontak','kontak.tbl_alamat','alamat.ID')
			.leftJoin('ref_tipe_kontak AS tipeKontak', 'tipeKontak.ID', 'kontak.ref_tipe_kontak')
			.where('alamat.tbl_pmp', data[0].id)
			.then(NestHydrationJS.nest)
			.then((perubahanAlamat) => {

				let template;
				var data_alamat = _.where(alamat, { default: 1});
				var status_perubahan = '';
				console.log()
				if(perubahanAlamat != null){
					data_alamat = perubahanAlamat
					status_perubahan = 'perubahan'
				}
				if (data[0].status === 3) {
					template = ejs.render(html2, {
					  data: data,
					  alamat: data_alamat,
					  pelaksanaDatul: data[0].petugas_datul_1,
					  tglDatul: data[0].tgl_datul_1,
					  judul: 'Konfirmasi',
					  status_perubahan: status_perubahan
					})
				} else if (data[0].status === 2) {
					template = ejs.render(html2, {
					  data: data,
					  alamat: data_alamat,
					  pelaksanaDatul: data[0].petugas_datul_2,
					  tglDatul: data[0].tgl_datul_2,
					  judul: 'Konfirmasi',
					  status_perubahan: status_perubahan
					})
				} else if (data[0].status === 1) {
					template = ejs.render(html2, {
					  data: data,
					  alamat: data_alamat,
					  pelaksanaDatul: data[0].petugas_datul_3,
					  tglDatul: data[0].tgl_datul_3,
					  judul: 'Tanda Terima',
					  status_perubahan: status_perubahan
					})
				}

				pdf.create(template, {
					'height' : '297mm',
					'width': '210mm',
					'base' : 'http://localhost:3000/'
				})
				.toBuffer(function(err, buffer){
					res.set({
					  'Content-type': 'application/pdf',
					  'Content-disposition': 'attachment; filename=' + req.body.id_pmp + '.pdf'
					});
					console.log(err)
					res.send(buffer);
				});
			})
		})
	})

})

datul.post('/buktidatul3', (req,res) => {
	// var token = jwt.decode(req.body.token, 'ilhamgantengpisan')
	var trDatul     	=   db.select(db.raw('ID, max(_tgl_datul) AS _tgl_datul, tbl_pmp, cara_verifikasi, petugas_datul'))
						.from('tbl_transaksi_datul').groupBy('ID','tbl_pmp').as('transaksi');
	var trDatulaw0  	=   db.select(db.raw('ID, max(_tgl_datul) AS _tgl_datul, tbl_pmp, cara_verifikasi, petugas_datul'))
						.from('tbl_transaksi_datul_aw0').groupBy('ID','tbl_pmp').as('transaksiAW0');
	var trDatulM    	=   db.select(db.raw('ID, max(_tgl_datul) AS _tgl_datul, tbl_pmp, cara_verifikasi, petugas_datul'))
						.from('tbl_transaksi_datul_mandiri').groupBy('ID','tbl_pmp').as('transaksiM');
	db.select(
		'pmp.ID AS ID',
		'pmp._nik AS _nik',
		'pmp._naw AS _naw',
		'pmp._nama AS _nama',
		'pmp._kategori AS _kategori',
		'pmp._ktp AS _ktp',
		'pmp._npwp AS _npwp',
		'pmp._tgl_lahir AS _tgl_lahir',
		'pmp._agama AS _agama_ID',
		'ref_agama._agama AS _agama_val',
		'pmp._golongan_darah AS _golongan_darah',
		'pmp._gender AS _gender',
		'pmp._flag_hak AS _flag_hak',
		'pmp._tgl_meninggal as _tgl_meninggal',
		'pmp._tgl_pensiun as _tgl_pensiun',
		'transaksi._tgl_datul as _tgl_datul',
		'transaksiAW0._tgl_datul as _tgl_datul_aw0',
		'transaksiM._tgl_datul as _tgl_datul_m',
		'transaksi.cara_verifikasi as cara_verifikasi_3',
		'transaksiAW0.cara_verifikasi as cara_verifikasi_2',
		'transaksiM.cara_verifikasi as cara_verifikasi_1',
		'transaksi.petugas_datul as petugas_datul_3',
		'transaksiAW0.petugas_datul as petugas_datul_2',
		'transaksiM.petugas_datul as petugas_datul_1',
		'p2tel._nama_cabang AS _nama_cabang'
	).from('tbl_pmp as pmp')
	.leftJoin('ref_agama', 'ref_agama.ID', 'pmp._agama')
	.leftJoin(trDatul, 'transaksi.tbl_pmp', 'pmp.ID')
	.leftJoin(trDatulaw0, 'transaksiAW0.tbl_pmp', 'pmp.ID')
	.leftJoin(trDatulM, 'transaksiM.tbl_pmp', 'pmp.ID')
	.leftJoin('tbl_alamat AS alamat','alamat.tbl_pmp','pmp.ID')
	.leftJoin('tbl_kelurahan AS kelurahan', 'kelurahan.ID', 'alamat.tbl_kelurahan_ID')
	.leftJoin('tbl_cabang_p2tel AS p2tel', 'p2tel._kode_p2tel', 'kelurahan._tbl_cabang_p2tel')
	.where('pmp.ID', req.body.id_pmp)
	.where('alamat._default', 1)
	.map((row) => {
		return {
			id: row.ID,
			nik: row._nik,
			naw: row._naw,
			nik_naw: `${ row._nik }_${ row._naw }`,
			nama: row._nama,
			kategori : row._kategori,
			photo: checkPhoto(row.ID),
			ktp: row._ktp,
			ktp_attch: checkDokumen(row.ID, 'ktp'),
			npwp: row._npwp,
			npwp_attch: checkDokumen(row.ID, 'npwp'),
			tanggal_lahir: checkDate(row._tgl_lahir),
			agama: row._agama_ID,
			agama_val: row._agama_val,
			golongan_darah: row._golongan_darah,
			jenis_kelamin: row._gender,
			status: checkStatusDatul(row._flag_hak, row._tgl_meninggal, row.tgl_pensiun, row._tgl_datul, row._tgl_datul_aw0, row._tgl_datul_m),
			tgl_datul_1:  checkDate(row._tgl_datul_m),
			tgl_datul_2: checkDate(row._tgl_datul_aw0),
			tgl_datul_3: checkDate(row._tgl_datul),
			cara_verifikasi_1: row.cara_verifikasi_1,
			cara_verifikasi_2: row.cara_verifikasi_2,
			cara_verifikasi_3: row.cara_verifikasi_3,
			petugas_datul_1: row.petugas_datul_1,
			petugas_datul_2: row.petugas_datul_2,
			petugas_datul_3: row.petugas_datul_3,
			nama_cabang: row._nama_cabang
		}
	})
	.then((data) => {
		db.select(
			'alamat.ID AS _id',
			'alamat._jalan AS _jalan',
			'alamat._komp AS _komp',
			'alamat._blok AS _blok',
			'alamat._no as _no',
			'alamat._rt as _rt',
			'alamat._rw as _rw',
			'kelurahan._nama_kelurahan as _namakelurahan',
			'kecamatan._nama_kecamatan as _namakecamatan',
			'kota._nama_kota as _namakota',
			'provinsi._nama_provinsi as _namaprovinsi',
			'kelurahan._kode_pos as _kodepos',
			'kelurahan.ID as _kodeKelurahan',
			'kecamatan.ID as _kodeKecamatan',
			'kota.ID as _kodeKota',
			'provinsi.ID as _kodeProvinsi',
			'alamat._default as _default',
			'kelurahan._tbl_cabang_p2tel as _cabangp2tel',
			'kontak.ID as _kontak__id',
			'tipeKontak._kontak_type_name AS _kontak__tipekontak',
			'tipeKontak.ID AS _kontak__refTipeKontak',
			'kontak._kontak_value as _kontak__kontakvalue'
		).from('tbl_alamat as alamat')
		.leftJoin('tbl_kelurahan AS kelurahan', 'kelurahan.ID', 'alamat.tbl_kelurahan_ID')
		.leftJoin('tbl_kecamatan as kecamatan', 'kecamatan.ID', 'kelurahan.tbl_kecamatan_ID')
		.leftJoin('tbl_kota as kota', 'kota.ID', 'kecamatan.tbl_kota_ID')
		.leftJoin('tbl_provinsi as provinsi', 'provinsi.ID', 'kota.tbl_provinsi_ID')
		.leftJoin('tbl_kontak AS kontak','kontak.tbl_alamat','alamat.ID')
		.leftJoin('ref_tipe_kontak AS tipeKontak', 'tipeKontak.ID', 'kontak.ref_tipe_kontak')
		.where('.alamat.tbl_pmp', data[0].id)
		.then(NestHydrationJS.nest)
		// .where('alamat._default', 1)
		.then((alamat) => {
			db.select(
				'tbl_kontak.ID AS id',
				'ref_tipe_kontak._kontak_type_name AS tipe_kontak',
				'tbl_kontak._kontak_value AS kontak_value'
			).from('tbl_kontak')
			.leftJoin('ref_tipe_kontak', 'ref_tipe_kontak.ID', 'tbl_kontak.ref_tipe_kontak')
			.where('tbl_alamat', alamat[0].id)
			.then((kontak) => {
				db.select(
					'ahliwaris.ID AS ID',
					'ahliwaris._nik as _nik',
					'ahliwaris._naw as _naw',
					'ahliwaris._nama AS _nama',
					'ahliwaris.ref_tipe_ahliwaris as ref_tipe_ahliwaris',
					'ahliwaris._tgl_lahir as _tgl_lahir',
					'ahliwaris._tgl_lulus as _tgl_lulus',
					'ahliwaris._tgl_menikah as _tgl_menikah',
					'ahliwaris._tgl_bekerja as _tgl_bekerja',
					'ahliwaris._tgl_menikah_lagi as _tgl_menikah_lagi',
					'ahliwaris._tgl_meninggal as _tgl_meninggal',
					'ahliwaris._tgl_cerai as _tgl_cerai',
					'ahliwaris._gender as _jenis_kelamin',
					'ahliwaris._agama as _agama_ID',
					'ref_agama._agama as _agama_val',
					'ahliwaris._ktp as _ktp',
					'ahliwaris._npwp as _npwp',
					'ahliwaris._golongan_darah as _golongan_darah',
          'ref_tipe_ahliwaris._type_name as _tipe_ahliwaris'

				).from('tbl_calon_pmp as ahliwaris')
				.leftJoin('ref_agama', 'ref_agama.ID', 'ahliwaris._agama')
        .leftJoin('ref_tipe_ahliwaris', 'ref_tipe_ahliwaris.ID', 'ahliwaris.ref_tipe_ahliwaris')
				.where('_nik', data[0].nik)
				.whereNot('ahliwaris.ID', data[0].id)
				.then((ahliwaris) => {

					db.select()
					.from('trx_biodata')
					// .groupBy('kode_laporan')
					.where('tbl_pmp', req.body.id_pmp)
					.whereNull('approve_at')
					.then((perubahan) => {

						db('trx_alamat')
						.where('tbl_pmp', req.body.id_pmp)
						.whereNull('approved_at')
						.then((perubahanAlamat) => {

							var awFilter =  _.map(_.reject(ahliwaris, function(o) {
								return ( o._tgl_meninggal != null || (o.ref_tipe_ahliwaris == 4 && dateFns.differenceInDays(new Date(), o._tgl_lahir) > 9131) || o._tgl_lulus != null || o._tgl_menikah_lagi != null || o._tgl_cerai != null ) })
								, function(o){
								return  {
									id : `${ o._naw }${ o._nik }`,
									nama: o._nama,
									photo : checkPhoto(o.ID),
		              nik: o._nik,
		              naw: o._naw,
									ktp: o._ktp,
									tgl_lahir: checkDate(o._tgl_lahir),
									agama_id: checkZero(o._agama_ID),
									agama_val: o._agama_val,
									golongan_darah: checkZero(o._golongan_darah),
									jenis_kelamin: o._jenis_kelamin,
                  tipe: o._tipe_ahliwaris
								}
							});

				let template;
				let pelaksanaDatul;

              // if (data[0])

				if (data[0].status === 3) {
					template = ejs.render(html2, {
					  data: data,
					  alamat: _.where(alamat, { default: 1}),
					  pelaksanaDatul: data[0].petugas_datul_1,
					  tglDatul: data[0].tgl_datul_1,
					  judul: 'Tanda Terima'
					})
				} else if (data[0].status === 2) {
					template = ejs.render(html2, {
					  data: data,
					  alamat: _.where(alamat, { default: 1}),
					  pelaksanaDatul: data[0].petugas_datul_2,
					  tglDatul: data[0].tgl_datul_2,
					  judul: 'Tanda Terima'
					})
				} else if (data[0].status === 1) {
					template = ejs.render(html2, {
					  data: data,
					  alamat: _.where(alamat, { default: 1}),
					  pelaksanaDatul: data[0].petugas_datul_3,
					  tglDatul: data[0].tgl_datul_3,
					  judul: 'Konfirmasi'
					})
				}

				pdf.create(template, {
					'height' : '297mm',
					'width': '210mm',
					'base' : 'http://localhost:3000/'
				})
				.toBuffer(function(err, buffer){
					res.set({
					  'Content-type': 'application/pdf',
					  'Content-disposition': 'attachment; filename=' + req.body.id_pmp + '.pdf'
					});
					console.log(err)
					res.send(buffer);
				});
						})
					})



				})
			})
		})
	})

})

datul.get('/buktidatul2mobile/:id_pmp', (req,res) => {
	// var token = jwt.decode(req.body.token, 'ilhamgantengpisan')
	var trDatul     	=   db.select(db.raw('ID, max(_tgl_datul) AS _tgl_datul, tbl_pmp, cara_verifikasi, petugas_datul'))
		.from('tbl_transaksi_datul').groupBy('ID','tbl_pmp').as('transaksi');
	var trDatulaw0  	=   db.select(db.raw('ID, max(_tgl_datul) AS _tgl_datul, tbl_pmp, cara_verifikasi, petugas_datul'))
		.from('tbl_transaksi_datul_aw0').groupBy('ID','tbl_pmp').as('transaksiAW0');
	var trDatulM    	=   db.select(db.raw('ID, max(_tgl_datul) AS _tgl_datul, tbl_pmp, cara_verifikasi, petugas_datul'))
		.from('tbl_transaksi_datul_mandiri').groupBy('ID','tbl_pmp').as('transaksiM');
	db.select(
		'pmp.ID AS ID',
		'pmp._nik AS _nik',
		'pmp._naw AS _naw',
		'pmp._nama AS _nama',
		'pmp._kategori AS _kategori',
		'pmp._ktp AS _ktp',
		'pmp._npwp AS _npwp',
		'pmp._tgl_lahir AS _tgl_lahir',
		'pmp._agama AS _agama_ID',
		'ref_agama._agama AS _agama_val',
		'pmp._golongan_darah AS _golongan_darah',
		'pmp._gender AS _gender',
		'pmp._flag_hak AS _flag_hak',
		'pmp._tgl_meninggal as _tgl_meninggal',
		'pmp._tgl_pensiun as _tgl_pensiun',
		'transaksi._tgl_datul as _tgl_datul',
		'transaksiAW0._tgl_datul as _tgl_datul_aw0',
		'transaksiM._tgl_datul as _tgl_datul_m',
		'transaksi.cara_verifikasi as cara_verifikasi_3',
		'transaksiAW0.cara_verifikasi as cara_verifikasi_2',
		'transaksiM.cara_verifikasi as cara_verifikasi_1',
		'transaksi.petugas_datul as petugas_datul_3',
		'transaksiAW0.petugas_datul as petugas_datul_2',
		'transaksiM.petugas_datul as petugas_datul_1',
		'p2tel._nama_cabang AS _nama_cabang'
	).from('tbl_pmp as pmp')
		.leftJoin('ref_agama', 'ref_agama.ID', 'pmp._agama')
		.leftJoin(trDatul, 'transaksi.tbl_pmp', 'pmp.ID')
		.leftJoin(trDatulaw0, 'transaksiAW0.tbl_pmp', 'pmp.ID')
		.leftJoin(trDatulM, 'transaksiM.tbl_pmp', 'pmp.ID')
		.leftJoin('tbl_alamat AS alamat','alamat.tbl_pmp','pmp.ID')
		.leftJoin('tbl_kelurahan AS kelurahan', 'kelurahan.ID', 'alamat.tbl_kelurahan_ID')
		.leftJoin('tbl_cabang_p2tel AS p2tel', 'p2tel._kode_p2tel', 'kelurahan._tbl_cabang_p2tel')
		.where('pmp.ID', req.params.id_pmp)
		.where('alamat._default', 1)
		.map((row) => {
			return {
				id: row.ID,
				nik: row._nik,
				naw: row._naw,
				nik_naw: `${ row._nik }_${ row._naw }`,
				nama: row._nama,
				kategori : row._kategori,
				photo: checkPhoto(row.ID),
				ktp: row._ktp,
				ktp_attch: checkDokumen(row.ID, 'ktp'),
				npwp: row._npwp,
				npwp_attch: checkDokumen(row.ID, 'npwp'),
				tanggal_lahir: checkDate(row._tgl_lahir),
				agama: row._agama_ID,
				agama_val: row._agama_val,
				golongan_darah: row._golongan_darah,
				jenis_kelamin: row._gender,
				status: checkStatusDatul(row._flag_hak, row._tgl_meninggal, row.tgl_pensiun, row._tgl_datul, row._tgl_datul_aw0, row._tgl_datul_m),
				tgl_datul_1:  checkDate(row._tgl_datul_m),
				tgl_datul_2: checkDate(row._tgl_datul_aw0),
				tgl_datul_3: checkDate(row._tgl_datul),
				cara_verifikasi_1: row.cara_verifikasi_1,
				cara_verifikasi_2: row.cara_verifikasi_2,
				cara_verifikasi_3: row.cara_verifikasi_3,
				petugas_datul_1: row.petugas_datul_1,
				petugas_datul_2: row.petugas_datul_2,
				petugas_datul_3: row.petugas_datul_3,
				nama_cabang: row._nama_cabang
			}
		})
		.then((data) => {

			db.select(
				'alamat.ID AS _id',
				'alamat._jalan AS _jalan',
				'alamat._komp AS _komp',
				'alamat._blok AS _blok',
				'alamat._no as _no',
				'alamat._rt as _rt',
				'alamat._rw as _rw',
				'kelurahan._nama_kelurahan as _namakelurahan',
				'kecamatan._nama_kecamatan as _namakecamatan',
				'kota._nama_kota as _namakota',
				'provinsi._nama_provinsi as _namaprovinsi',
				'kelurahan._kode_pos as _kodepos',
				'kelurahan.ID as _kodeKelurahan',
				'kecamatan.ID as _kodeKecamatan',
				'kota.ID as _kodeKota',
				'provinsi.ID as _kodeProvinsi',
				'alamat._default as _default',
				'kelurahan._tbl_cabang_p2tel as _cabangp2tel',
				'kontak.ID as _kontak__id',
				'tipeKontak._kontak_type_name AS _kontak__tipekontak',
				'tipeKontak.ID AS _kontak__refTipeKontak',
				'kontak._kontak_value as _kontak__kontakvalue'
			).from('tbl_alamat as alamat')
				.leftJoin('tbl_kelurahan AS kelurahan', 'kelurahan.ID', 'alamat.tbl_kelurahan_ID')
				.leftJoin('tbl_kecamatan as kecamatan', 'kecamatan.ID', 'kelurahan.tbl_kecamatan_ID')
				.leftJoin('tbl_kota as kota', 'kota.ID', 'kecamatan.tbl_kota_ID')
				.leftJoin('tbl_provinsi as provinsi', 'provinsi.ID', 'kota.tbl_provinsi_ID')
				.leftJoin('tbl_kontak AS kontak','kontak.tbl_alamat','alamat.ID')
				.leftJoin('ref_tipe_kontak AS tipeKontak', 'tipeKontak.ID', 'kontak.ref_tipe_kontak')
				.where('alamat.tbl_pmp', data[0].id)
				.then(NestHydrationJS.nest)
				// .where('alamat._default', 1)
				.then((alamat) => {

					db.select(
						'alamat.ID AS _id',
						'alamat._jalan AS _jalan',
						'alamat._komp AS _komp',
						'alamat._blok AS _blok',
						'alamat._no as _no',
						'alamat._rt as _rt',
						'alamat._rw as _rw',
						'kelurahan._nama_kelurahan as _namakelurahan',
						'kecamatan._nama_kecamatan as _namakecamatan',
						'kota._nama_kota as _namakota',
						'provinsi._nama_provinsi as _namaprovinsi',
						'kelurahan._kode_pos as _kodepos',
						'kelurahan.ID as _kodeKelurahan',
						'kecamatan.ID as _kodeKecamatan',
						'kota.ID as _kodeKota',
						'provinsi.ID as _kodeProvinsi',
						'alamat._default as _default',
						'kelurahan._tbl_cabang_p2tel as _cabangp2tel',
						'kontak.ID as _kontak__id',
						'tipeKontak._kontak_type_name AS _kontak__tipekontak',
						'tipeKontak.ID AS _kontak__refTipeKontak',
						'kontak._kontak_value as _kontak__kontakvalue'
					).from('trx_alamat as alamat')
						.leftJoin('tbl_kelurahan AS kelurahan', 'kelurahan.ID', 'alamat.tbl_kelurahan_ID')
						.leftJoin('tbl_kecamatan as kecamatan', 'kecamatan.ID', 'kelurahan.tbl_kecamatan_ID')
						.leftJoin('tbl_kota as kota', 'kota.ID', 'kecamatan.tbl_kota_ID')
						.leftJoin('tbl_provinsi as provinsi', 'provinsi.ID', 'kota.tbl_provinsi_ID')
						.leftJoin('tbl_kontak AS kontak','kontak.tbl_alamat','alamat.ID')
						.leftJoin('ref_tipe_kontak AS tipeKontak', 'tipeKontak.ID', 'kontak.ref_tipe_kontak')
						.where('alamat.tbl_pmp', data[0].id)
						.then(NestHydrationJS.nest)
						.then((perubahanAlamat) => {

							let template;
							var data_alamat = _.where(alamat, { default: 1});
							var status_perubahan = '';
							console.log()
							if(perubahanAlamat != null){
								data_alamat = perubahanAlamat
								status_perubahan = 'perubahan'
							}
							if (data[0].status === 3) {
								template = ejs.render(html2, {
									data: data,
									alamat: data_alamat,
									pelaksanaDatul: data[0].petugas_datul_1,
									tglDatul: data[0].tgl_datul_1,
									judul: 'Konfirmasi',
									status_perubahan: status_perubahan
								})
							} else if (data[0].status === 2) {
								template = ejs.render(html2, {
									data: data,
									alamat: data_alamat,
									pelaksanaDatul: data[0].petugas_datul_2,
									tglDatul: data[0].tgl_datul_2,
									judul: 'Konfirmasi',
									status_perubahan: status_perubahan
								})
							} else if (data[0].status === 1) {
								template = ejs.render(html2, {
									data: data,
									alamat: data_alamat,
									pelaksanaDatul: data[0].petugas_datul_3,
									tglDatul: data[0].tgl_datul_3,
									judul: 'Tanda Terima',
									status_perubahan: status_perubahan
								})
							}

							pdf.create(template, {
									'height' : '297mm',
									'width': '210mm',
									'base' : 'http://localhost:3000/'
								})
								.toBuffer(function(err, buffer){
									res.set({
										'Content-type': 'application/pdf',
										'Content-disposition': 'attachment; filename=' + req.body.id_pmp + '.pdf'
									});
									console.log(err)
									res.send(buffer);
								});
						})
				})
		})

})

module.exports = datul;
