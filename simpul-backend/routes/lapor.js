var express = require('express')
var lapor = express.Router()
var db = require('knex')(require('../config/db').mysql)
var moment = require('moment')
var bcrypt = require('bcrypt-nodejs')
var _ = require('underscore')
var _S = require('underscore.string')
var jwt = require('jwt-simple')

var checkPhoto = require('../util/check-photo')
var checkDate = require('../util/check-date')


lapor.post('/meninggal', (req, res) => {
  db.select(
    'tbl_pmp.ID AS id',
    'tbl_pmp._nik as nik',
    'tbl_pmp._nama as nama',
    'trx_biodata.tgl_meninggal as tgl_meninggal',
    'tbl_pmp._tgl_meninggal as _tgl_meninggal'
  )
  .from('trx_biodata')
  .leftJoin('tbl_pmp', 'tbl_pmp.ID', 'trx_biodata.tbl_pmp')
  .map((row) => { 
    return {
       photo: checkPhoto(row.ID),
       nik: row.nik,
       nama: row.nama,
       tgl_meninggal: checkDate(row.tgl_meninggal),
       _tgl_meninggal: checkDate(row._tgl_meninggal)
    }
    
  })
  .then((data) => {
    res.send({
      success: true,
      data: data
    })
  })
})

module.exports = lapor