var express = require('express');
var pmp = express.Router();
var db = require('knex')(require('../config/db').mysql);
var moment = require('moment');
var bcrypt = require('bcrypt-nodejs');
var _ = require('underscore');
var _S = require('underscore.string');
var jwt = require('jwt-simple');
var NestHydrationJS = require('nesthydrationjs')();
var dateFns = require('date-fns');

var authUtil = require('../util/auth');
var checkDate = require('../util/check-date');
var fieldMaker = require('../util/field-maker');
var checkGender = require('../util/check-gender');
var checkPhoto = require('../util/check-photo');
var checkDokumen = require('../util/check-document');
var checkStatusDatul = require('../util/check-status-datul');
var checkZero = require('../util/check-zero');
var datulConfig = require('../config/datul-config')

pmp.post('/', (req,res) => {
	const body = req.body

	var token = jwt.decode(req.body.token, 'ilhamgantengpisan')
	var trDatul     	=   db.select(db.raw('ID, max(_tgl_datul) AS _tgl_datul, tbl_pmp, catatan'))
						.from('tbl_transaksi_datul').groupBy('ID','tbl_pmp').as('transaksi');
	var trDatulaw0  	=   db.select(db.raw('ID, max(_tgl_datul) AS _tgl_datul, tbl_pmp, catatan'))
						.from('tbl_transaksi_datul_aw0').groupBy('ID','tbl_pmp').as('transaksiAW0');
	var trDatulM    	=   db.select(db.raw('ID, max(_tgl_datul) AS _tgl_datul, tbl_pmp, catatan'))
						.from('tbl_transaksi_datul_mandiri').groupBy('ID','tbl_pmp').as('transaksiM');

	db.select(
		'pmp.ID AS ID',
		'pmp._nik AS _nik',
		'pmp._naw AS _naw',
		'pmp._nama AS _nama',
		'pmp._kategori AS _kategori',
		'pmp._ktp AS _ktp',
		'pmp._npwp AS _npwp',
		'pmp._tgl_lahir AS _tgl_lahir',
		'pmp._agama AS _agama_ID',
		'ref_agama._agama AS _agama_val',
		'pmp._golongan_darah AS _golongan_darah',
		'pmp._gender AS _gender',
		'pmp._flag_hak AS _flag_hak',
		'pmp._tgl_meninggal as _tgl_meninggal',
		'pmp._tgl_pensiun as _tgl_pensiun',
		'transaksi._tgl_datul as _tgl_datul',
		'transaksiAW0._tgl_datul as _tgl_datul_aw0',
		'transaksiM._tgl_datul as _tgl_datul_m',
		'transaksi.catatan as catatan_3',
		'transaksiAW0.catatan as catatan_2',
		'transaksiM.catatan as catatan_1',
		'p2tel._nama_cabang AS _nama_cabang'
	).from('tbl_pmp as pmp')
	.leftJoin('ref_agama', 'ref_agama.ID', 'pmp._agama')
	.leftJoin(trDatul, 'transaksi.tbl_pmp', 'pmp.ID')
	.leftJoin(trDatulaw0, 'transaksiAW0.tbl_pmp', 'pmp.ID')
	.leftJoin(trDatulM, 'transaksiM.tbl_pmp', 'pmp.ID')
	.leftJoin('tbl_alamat AS alamat','alamat.tbl_pmp','pmp.ID')
	.leftJoin('tbl_kelurahan AS kelurahan', 'kelurahan.ID', 'alamat.tbl_kelurahan_ID')
	.leftJoin('tbl_cabang_p2tel AS p2tel', 'p2tel._kode_p2tel', 'kelurahan._tbl_cabang_p2tel')
	.where('pmp.ID', req.body.id_pmp)
	.where('alamat._default', 1)
	.map((row) => {
		const tgl_datul_3 = checkDate(row._tgl_datul);
		const tgl_datul_2 = checkDate(row._tgl_datul_aw0);
		const tgl_datul_1 = checkDate(row._tgl_datul_m);
		let tgl_datul_terakhir = tgl_datul_3 ? tgl_datul_3 : tgl_datul_2 ? tgl_datul_2 : tgl_datul_1 ? tgl_datul_1 : null;
		return {
			id: row.ID,
			nik: row._nik,
			naw: row._naw,
			nik_naw: `${ row._nik }_${ row._naw }`,
			nama: row._nama,
			kategori : row._kategori,
			photo: checkPhoto(row.ID),
			ktp: row._ktp,
			ktp_attch: checkDokumen(row.ID, 'ktp'),
			npwp: row._npwp,
			npwp_attch: checkDokumen(row.ID, 'npwp'),
			tanggal_lahir: checkDate(row._tgl_lahir),
			agama: row._agama_ID,
			agama_val: row._agama_val,
			golongan_darah: row._golongan_darah,
			jenis_kelamin: row._gender,
			status: checkStatusDatul(row._flag_hak, row._tgl_meninggal, row.tgl_pensiun, row._tgl_datul, row._tgl_datul_aw0, row._tgl_datul_m),
			tgl_datul_1:  checkDate(row._tgl_datul_m),
			tgl_datul_2: checkDate(row._tgl_datul_aw0),
			tgl_datul_3: checkDate(row._tgl_datul),
			tgl_datul_terakhir,
			catatan_1: row.catatan_1,
			catatan_2: row.catatan_2,
			catatan_3: row.catatan_3,
			nama_cabang: row._nama_cabang,
		}
	})
	.then((data) => {
		db.select(
			'alamat.ID AS _id',
			'alamat._jalan AS _jalan',
			'alamat._komp AS _komp',
			'alamat._blok AS _blok',
			'alamat._no as _no',
			'alamat._rt as _rt',
			'alamat._rw as _rw',
			'kelurahan._nama_kelurahan as _namakelurahan',
			'kecamatan._nama_kecamatan as _namakecamatan',
			'kota._nama_kota as _namakota',
			'provinsi._nama_provinsi as _namaprovinsi',
			'kelurahan._kode_pos as _kodepos',
			'kelurahan.ID as _kodeKelurahan',
			'kecamatan.ID as _kodeKecamatan',
			'kota.ID as _kodeKota',
			'provinsi.ID as _kodeProvinsi',
			'alamat._default as _default',
			'kelurahan._tbl_cabang_p2tel as _cabangp2tel',
			'kontak.ID as _kontak__id',
			'tipeKontak._kontak_type_name AS _kontak__tipekontak',
			'tipeKontak.ID AS _kontak__refTipeKontak',
			'kontak._kontak_value as _kontak__kontakvalue',
			'tipeKontak.validation AS _kontak__validation'
		).from('tbl_alamat as alamat')
		.leftJoin('tbl_kelurahan AS kelurahan', 'kelurahan.ID', 'alamat.tbl_kelurahan_ID')
		.leftJoin('tbl_kecamatan as kecamatan', 'kecamatan.ID', 'kelurahan.tbl_kecamatan_ID')
		.leftJoin('tbl_kota as kota', 'kota.ID', 'kecamatan.tbl_kota_ID')
		.leftJoin('tbl_provinsi as provinsi', 'provinsi.ID', 'kota.tbl_provinsi_ID')
		.leftJoin('tbl_kontak AS kontak','kontak.tbl_alamat','alamat.ID')
		.leftJoin('ref_tipe_kontak AS tipeKontak', 'tipeKontak.ID', 'kontak.ref_tipe_kontak')
		.where('.alamat.tbl_pmp', data[0].id)
		.then(NestHydrationJS.nest)
		.then((alamat) => {
			db.select(
				'tbl_kontak.ID AS id',
				'ref_tipe_kontak._kontak_type_name AS tipe_kontak',
				'tbl_kontak._kontak_value AS kontak_value',
				'ref_tipe_kontak.validation AS validation'
			).from('tbl_kontak')
				.leftJoin('ref_tipe_kontak', 'ref_tipe_kontak.ID', 'tbl_kontak.ref_tipe_kontak')
				.where('tbl_alamat', alamat[0].id)
				.then((kontak) => {

				db.select(
					'ahliwaris.ID AS ID',
					'ahliwaris._nik as _nik',
					'ahliwaris._naw as _naw',
					'ahliwaris._nama AS _nama',
					'ahliwaris.ref_tipe_ahliwaris as ref_tipe_ahliwaris',
					'ahliwaris._tgl_lahir as _tgl_lahir',
					'ahliwaris._tgl_lulus as _tgl_lulus',
					'ahliwaris._tgl_menikah as _tgl_menikah',
					'ahliwaris._tgl_bekerja as _tgl_bekerja',
					'ahliwaris._tgl_menikah_lagi as _tgl_menikah_lagi',
					'ahliwaris._tgl_meninggal as _tgl_meninggal',
					'ahliwaris._tgl_cerai as _tgl_cerai',
					'ahliwaris._gender as _jenis_kelamin',
					'ahliwaris._agama as _agama_ID',
					'ref_agama._agama as _agama_val',
					'ahliwaris._ktp as _ktp',
					'ahliwaris._npwp as _npwp',
					'ahliwaris._golongan_darah as _golongan_darah',
					'ref_tipe_ahliwaris._type_name as _tipe_ahliwaris'
				).from('tbl_calon_pmp as ahliwaris')
				.leftJoin('ref_agama', 'ref_agama.ID', 'ahliwaris._agama')
				.leftJoin('ref_tipe_ahliwaris', 'ref_tipe_ahliwaris.ID', 'ahliwaris.ref_tipe_ahliwaris')
				.where('_nik', data[0].nik)
				.whereNot('ahliwaris.ID', data[0].id)
				.then((ahliwaris) => {
					db.select()
					.from('trx_biodata')
					.where('tbl_pmp', req.body.id_pmp)
					.whereNull('approve_at')
					.then((perubahan) => {

						db('trx_alamat as alamat')
						.where('tbl_pmp', req.body.id_pmp)
						.leftJoin('tbl_kelurahan AS kelurahan', 'kelurahan.ID', 'alamat.tbl_kelurahan_ID')
						.leftJoin('tbl_kecamatan as kecamatan', 'kecamatan.ID', 'kelurahan.tbl_kecamatan_ID')
						.leftJoin('tbl_kota as kota', 'kota.ID', 'kecamatan.tbl_kota_ID')
						.leftJoin('tbl_provinsi as provinsi', 'provinsi.ID', 'kota.tbl_provinsi_ID')
						.whereNull('approved_at')
						.then((perubahanAlamat) => {

							var awFilter =  _.map(_.reject(ahliwaris, function(o) {
								return ( o._tgl_meninggal != null || (o.ref_tipe_ahliwaris == 4 && dateFns.differenceInDays(new Date(), o._tgl_lahir) > 9131) || o._tgl_lulus != null || o._tgl_menikah_lagi != null || o._tgl_cerai != null ) })
								, function(o){
								return  {
									id : `${ o._naw }${ o._nik }`,
									nama: o._nama,
									photo : checkPhoto(o.ID),
									nik: o._nik,
									naw: o._naw,
									ktp: o._ktp,
									tgl_lahir: checkDate(o._tgl_lahir),
									agama_id: checkZero(o._agama_ID),
									agama_val: o._agama_val,
									golongan_darah: checkZero(o._golongan_darah),
									jenis_kelamin: o._jenis_kelamin,
									tipe: o._tipe_ahliwaris
								}
							});

							db.select('datul_mandiri.tbl_user')
							.from('tbl_transaksi_datul_mandiri as datul_mandiri')
							.where('tbl_pmp', req.body.id_pmp)
							.groupBy('datul_mandiri.tbl_user')
							.then((datafr_mandiri) => {

								db.select('datul_aw0.tbl_user')
								.from('tbl_transaksi_datul_aw0 as datul_aw0')
								.where('tbl_pmp', req.body.id_pmp)
								.groupBy('datul_aw0.tbl_user')
								.then((datafr_aw0) => {

									datulConfig.openClose(token._petugas_ID)
									.then((openCloseRes) => {
										res.send({
											success: true,
											data: {
												data: data,
												alamat: alamat,
												alamatUtama: _.where(alamat, { default: 1}),
												alamatLain: _.where(alamat, { default: 0}),
												kontak: kontak,
												ahliwaris: awFilter,
												perubahan: perubahan,
												perubahanAlamat: perubahanAlamat,
												datafr_mandiri: datafr_mandiri.length,
												datafr_aw0: datafr_aw0.length,
												periode_datul: openCloseRes
											}
										})

									})



								})
							})
						})
					})
				})
			})
		})
	})
});

module.exports = pmp;
