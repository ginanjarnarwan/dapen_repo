const express = require('express');
const knex = require('knex');
const db = knex(require('../config/db').mysql);
const moment = require('moment');
const jwt = require('jwt-simple');

const misc = express.Router();

const check_token_body = (req, res) => {
	const token = req.body.token;
	if (!token) {
		res.sendStatus(401);
		return
	}
	return jwt.decode(token, 'ilhamgantengpisan', null, null)
};

misc.post('/rating', (req, res) => {
	// const token = check_token_body(req, res);
	const body = req.body;
	db('tbl_feedback')
		.insert({
			rating: body.rating,
			comment: body.comment,
			pmp_id: body.id_pmp,
			created_date: new Date(),
			created_by: body.id_pmp,
		})
		.then(() => {
			res.send({
				success: true
			})
		})
		.catch((e) => {
			res.send({
				success: false,
				msg: e
			})
		})
});

module.exports = misc;
