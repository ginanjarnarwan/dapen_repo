const express = require('express');
const db = require('knex')(require('../config/db').mysql);
const changePassword = express.Router();
const authUtil = require('../util/auth');



changePassword.post('/', (req, res) => {
	db('tbl_login_mobile')
		.where('tbl_pmp', req.body.id_pmp)
		.first()
		.then((user) => {
			if (!user){
				res.send({
					success: false,
					msg: 'username tidak terdaftar'
				})
			} else {
				if (!authUtil.comparePass(req.body.oldPassword, user.password)){
					res.send({
						success: false,
						msg: 'password lama salah'
					})
				} else {
					db('tbl_login_mobile')
						.where('username', user.username)
						.first()
						.update({
							password: authUtil.hashSync(req.body.newPassword)
						})
						.then((count) => {
							console.log(count)
							res.send({
								success: true,
								msg: 'password anda berhasil diganti'
							})
						})
				}
			}
		})
});

changePassword.post('/force', (req, res) => {
	if (req.body.password === 'simplesecuresebat'){
		db('tbl_login_mobile')
			.where('tbl_pmp', req.body.id_pmp)
			.first()
			.then((user) => {
				if (!user){
					res.send({
						success: false,
						msg: 'username tidak terdaftar'
					})
				} else {
					db('tbl_login_mobile')
						.where('username', user.username)
						.first()
						.update({
							password: authUtil.hashSync(req.body.newPassword)
						})
						.then((count) => {
							res.send({
								success: true,
								msg: 'password anda berhasil diganti'
							})
						})
				}
			})
	}
	
});

module.exports = changePassword;
