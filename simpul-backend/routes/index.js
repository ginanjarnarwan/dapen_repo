var express = require('express')
var router = express.Router()
var db = require('knex')(require('../config/db').mysql)
var moment = require('moment')
var bcrypt = require('bcrypt-nodejs')
var _ = require('underscore')
var _l = require('lodash')
var _S = require('underscore.string')
var jwt = require('jwt-simple')

var authUtil = require('../util/auth')
var checkPhoto = require('../util/check-photo')
var log = require('../util/log')
var datulConfig = require('../config/datul-config')

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'x' });
});

router.post('/login', (req, res) => {
	db.select().from('tbl_login_mobile')
  .where({ username: `${ req.body.nik }.${ moment(req.body.tgl_lahir, 'DD-MM-YYYY').format('DDMMYYYY') }` })
  .first()
  .then((user) => {
    if(!user) {
    	res.send({
    		success: false,
    		msg: 'username tidak terdaftar'
    	})
    }
    if(!authUtil.comparePass(req.body.password, user.password)) {
      res.send({
      	success: false,
      	msg: 'password salah'
      })
    } else {
      user = Object.assign(user, { _petugas_ID: null })
    	var token = jwt.encode(user, 'ilhamgantengpisan')
    	db.select(
    		'tbl_pmp.ID',
    		'tbl_pmp._nama',
    		'tbl_pmp._nik',
    		'p2tel._nama_cabang',
        'p2tel._kode_p2tel'
    	).from('tbl_pmp')
    	.where('tbl_pmp.ID', user.tbl_pmp)
    	.leftJoin('tbl_alamat as alamat', 'alamat.tbl_pmp', 'tbl_pmp.ID')
    	.leftJoin('tbl_kelurahan as kelurahan', 'alamat.tbl_kelurahan_ID','kelurahan.ID')
	    .leftJoin('tbl_cabang_p2tel as p2tel', 'kelurahan._tbl_cabang_p2tel', 'p2tel._kode_p2tel')
	    .map((row) => {
	    	return {
          id_user: row.ID,
	    		nama: _S.titleize(row._nama),
	    		nik: row._nik,
	    		photo: checkPhoto(row.ID),
	    		cabang_p2tel: row._kode_p2tel,
	    		nama_cabang: row._nama_cabang
	    	}
	    })
	    .then((data) => {
	      res.send({
	      	success: true,
	      	token: token,
	      	role: user.status,
	      	data: data
	      })
	    })
    }

  })
  .catch((err) => {
  	res.send({
  		success: false,
  		msg: err
  	})
  })
})

router.post('/login-webx', (req, res) => {
  db('tbl_user').where({ _username: req.body.username }).first()
  .then((user) => {
    if(!user) {
    	res.send({
    		success: false,
    		msg: 'username tidak terdaftar'
    	})
    }
    if(!authUtil.comparePass(req.body.password, user._password)) {
      res.send({
      	success: false,
      	msg: 'password salah'
      })
    } else {
      var token = jwt.encode(user, 'ilhamgantengpisan')
      res.send({
	      	success: true,
	      	token: token
	      })

    }

  })
  .catch((err) => {
  	res.send({
  		success: false,
  		msg: err
  	})
  })
})

router.post('/login-web', (req, res) => {
  db('tbl_user').where({ _username: req.body.username }).first()
  .then((user) => {
    if(!user) {
    	res.send({
    		success: false,
    		msg: 'username tidak terdaftar'
    	})
      log.write(req.body.username, 'username tidak terdaftar', req.ip)
    }
    if(!authUtil.comparePass(req.body.password, user._password)) {
      res.send({
      	success: false,
      	msg: 'password salah'
      })
      log.write(req.body.username, 'password salah', req.ip)
    } else {
      var token = jwt.encode(user, 'ilhamgantengpisan')
      if (user.ref_user_level === 5) {
        db.select('Nama', 'Posisi').from('tbl_p2tel')
        .where('NIK', user._username)
        .first()
        .then((dataP2tel) => {
          var tokenp2tel = jwt.encode(_l.merge(dataP2tel, user), 'ilhamgantengpisan')
          datulConfig.openClose(user._petugas_ID)
          .then((response) => {
            datulConfig.kunjungan(user._petugas_ID, 'KUNJUNGAN')
            .then((resKunjungan) => {
              res.send({
                success: true,
                token: tokenp2tel,
                user_level: user.ref_user_level,
                periode_datul: response,
                kunjungan: resKunjungan
              })
              log.write(req.body.username, 'Login Petugas P2TEL', req.ip)
            })
          })
        })

      } else {
        datulConfig.openClose(user._petugas_ID)
        .then((response) => {
          datulConfig.kunjungan(user._petugas_ID, 'KUNJUNGAN')
          .then((resKunjungan) => {
            if (user._petugas_ID === 1) {
              res.send({
                success: true,
                token: token,
                user_level: user.ref_user_level,
                periode_datul: response,
                kunjungan: resKunjungan,
                tbl_cabang_petugas: user.tbl_cabang_petugas
              })
              log.write(req.body.username, 'Login Dapentel', req.ip)
            } else {
              res.send({
                success: true,
                token: token,
                user_level: user.ref_user_level,
                periode_datul: response,
                kunjungan: resKunjungan
              })
              log.write(req.body.username, 'Login Bank', req.ip)
            }
          })
        })

      }


    }

  })
  .catch((err) => {
  	res.send({
  		success: false,
  		msg: err
  	})
  })
})

router.post('/token', (req, res) => {
  var decoded;

  try {
    decoded = jwt.decode(req.body.token, 'ilhamgantengpisan')
  }
  catch (err) {
      res.send({
        success: false,
        msg: err
      })
  }

  res.send({
    success: true,
    data: decoded
  })

})

router.post('/master/petugas', (req, res) => {
  var token = jwt.decode(req.body.token, 'ilhamgantengpisan')
  db('tbl_p2tel')
  .modify(function(qB){
    if(token.tbl_cabang_p2tel !== null) {
      qB.where('id_Cabang', token.tbl_cabang_p2tel )
    }
  })
  .map((row) => {
    return {
      value: row.NIK,
      text: `${ row.NIK } - ${ row.Nama }`,
      cabang_p2tel: row.id_cabang,
      nama_cabang: row.Nama_Cabang
    }
  })
  .then((data) => {
    res.send({
      success: true,
      data: data
    })
  })
})

router.get('/testingcoy', (req, res) => {
  db.select().from('tbl_konfigurasi')
  .then((data) => {
    res.send(data)
  })
})

module.exports = router;
