var express = require('express')
var admin = express.Router()
var db = require('knex')(require('../config/db').mysql)
var moment = require('moment')
var bcrypt = require('bcrypt-nodejs')
var _ = require('underscore')
const _l = require('lodash')
var _S = require('underscore.string')
var jwt = require('jwt-simple')
var NestHydrationJS = require('nesthydrationjs')()
var dateFns = require('date-fns');
var log = require('../util/log')

admin.post('/', (req, res) => {
  res.sendStatus(200)
})

admin.post('/ubah-biodata', (req, res) => {
  // body : id_pmp, value, data_perubahan, token
  const body = req.body
  const token = body.token
  if (!token) {
    res.sendStatus(401)
    return
  }
  const token_data = jwt.decode(token, 'ilhamgantengpisan', null, null)
  const valid_column = ['_gender',	'_ktp',	'_npwp',	'_tgl_meninggal',	'_agama',	'_golongan_darah']

  if (!_.includes(valid_column, body.data_perubahan)) {
    res.send({
      success: false,
      msg: 'data perubahan tidak valid'
    })
  }

  db('tbl_pmp')
  .update(body.data_perubahan, body.value)
  .update('updatedAt', new Date())
  .where('ID', body.id_pmp)
  .then(() => {
    res.send({
      success: true,
      msg: `Perubahan data ${ body.data_perubahan }`
    })
    log.write(token_data.username, `Ubah Data ${ body.data_perubahan } untuk PMP dengan ID ${ body.id_pmp }`, req.ip)
  })
  .catch((err) => {
    res.send({
      success: false,
      msg: err
    })
  })

})

admin.post('/ubah-alamat', (req, res) => {
  const body = req.body
  const token = body.token
  if (!token) {
    res.sendStatus(401)
    return
  }
  const token_data = jwt.decode(token, 'ilhamgantengpisan', null, null)

  db('tbl_alamat')
  .update({
    _jalan: body.jalan,
    _komp: body.komp,
    _blok: body.blok,
    _no: body.no,
    _rt: body.rt,
    _rw: body.rw,
    tbl_kelurahan_ID: body.kelurahan_id,
    updatedAt: new Date()
  })
  .where('id', body.id_alamat)
  .then(() => {
    res.send({
      success: true
    })
    log.write(token_data.username, `Ubah Data alamat untuk PMP dengan ID ${ body.id_alamat }`, req.ip)
  })
  .catch((err) => {
    res.send({
      success: false,
      msg: err
    })
  })
})

admin.post('/trx-ahliwaris', (req, res) => {
  // res.sendStatus(200)
  db('trx_ahliwaris')
  .map((row) => {
    return {
      id: row.id,
      val: row.val
    }
  })
  .then((data) => {
    res.send(data)
  })
  .catch((err) => {
    res.send(err)
  })
})

module.exports = admin
