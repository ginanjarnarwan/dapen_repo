const cron = require('node-cron')
const db = require('knex')(require('./config/db').mysql);
const _ = require('underscore')

cron.schedule('* * * * * *', () => {
  db.select()
  .from('trx_alamat')
  .where('approved_by', null)
  // .first()
  .then((data) => {
    // data = _.chain(data).omit('id').omit('id_alamat').value()
    let dataProses = _.map(data, (o) => {
      return {
        _jalan: o._jalan,
        _komp: o._komp,
        _blok: o._blok,
        _no: o._no,
        _rt: o._rt,
        _rw: o._rw,
        tbl_pmp: o.tbl_pmp,
        tbl_kelurahan_ID: o.tbl_kelurahan_ID,
        _default: o._default,
        _no_urut_alamat: o._no_urut_alamat,
        updatedAt: new Date()
      }
    })[0]
    // console.log(dataProses)
    db('tbl_alamat')
    .update(dataProses)
    .where('id', data[0].id_alamat)
    .then(() => {
      db('trx_alamat')
      .update({
        approved_by: 'SYSTEM',
        approved_at: new Date()
      })
      .where('id', data[0].id)
      .then(() => {
        console.log('done')
      })
    })
  })
})
