var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var passport = require('passport');
var session = require('express-session');
var MemoryStore = require('session-memory-store')(session);
var flash = require('connect-flash');
// Initialize passport variables
var jwt = require('jwt-simple');
var passportConfig = require('./config/passport');

var app = express();

app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');


// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ extended: false, parameterLimit: 50000, limit: '50mb' }));
app.use(cookieParser('EU+m$4%@6bVzCRzwY9b_6&9QD69FyNVPZxC^WJ$*X&8bNkU4PPTRJWm5RX!79!!Hh#sd3d@$qqFtbkPJ*AD7_c#XZnfkd@Xz^RZ4cWQR7h=7SaynMujLGubJu=R!cwku'));
app.use(express.static(path.join(__dirname, 'public')));

// setup the flash message middleware
app.use(flash());

// setup the session middleware
app.use(session({
  secret : 'R4L%wdMT_&Ke*DP+*dZGE+dvd+TfA2Srgws_Ew7AN6Ph9qupkt*V6AAbsC%XG=c9dbD6%FMG4h@_5jar56htkNu?nYeJy!x%F+ruEwXzqH65fu93KEnWmx-VNL$8RYe4',
  resave : false,
  saveUninitialized : true,
  store: new MemoryStore()
}));

app.use(passport.initialize());
app.use(passport.session());

app.use('/', require('./routes/index'));
app.use('/welcome', require('./routes/welcome'));

app.use('/register', require('./routes/register'));
app.use('/datulmandiri', require('./routes/datulmandiri'));
app.use('/masterlokasi', require('./routes/masterlokasi'));
app.use('/dashboarddatul', require('./routes/dashboarddatul'));
app.use('/listanggota', require('./routes/listanggota'));
app.use('/pmp', require('./routes/pmp'));
app.use('/ahliwaris', require('./routes/ahliwaris'));
app.use('/datul', require('./routes/datul'));
app.use('/perubahan', require('./routes/perubahan'));
app.use('/upload', require('./routes/upload'));
app.use('/berita', require('./routes/berita'));
app.use('/testing', require('./routes/testing'));
app.use('/carirekan', require('./routes/carirekan'));
app.use('/laporan', require('./routes/lapor'));
app.use('/report', require('./routes/report'));
app.use('/catatan', require('./routes/catatan'));
app.use('/changePassword', require('./routes/changePassword'));
app.use('/cabang', require('./routes/cabang'));

app.use('/absen/dashboard', require('./routes/absen/dashboard'));
app.use('/absen/upload', require('./routes/absen/upload'));
app.use('/absen', require('./routes/absen/absen'));

app.use('/datul-bank', require('./routes/datul-bank'))
app.use('/konfigurasi', require('./routes/konfigurasi'));
app.use('/misc', require('./routes/misc'));
app.use('/admin', require('./routes/admin'))

// catch 404 and forward to error handler
app.use(function(req, res, next) {
	console.log(req);
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

// console.log(app._router.stack);

module.exports = app;
