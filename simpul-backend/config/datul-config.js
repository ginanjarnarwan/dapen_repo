var moment = require('moment');
var thisYear = (new Date()).getFullYear();
var thisMonth = (new Date()).getMonth();
var thisDate = (new Date()).getDate();
var dateFns = require('date-fns');
const _ = require('underscore')
const db = require('knex')(require('../config/db').mysql)

let startDatul
let finishDatul
let semester

if ( dateFns.isBefore(new Date(thisYear, thisMonth, thisDate), new Date(thisYear, 5 , 1)) ) {
	startDatul = new Date(thisYear, 0, 1)
	// finishDatul = new Date(thisYear, 5, 30)
	finishDatul = new Date(thisYear, 4, 30)
	semester = 1
} else {
	// startDatul = new Date(thisYear, 6, 1)
	startDatul = new Date(thisYear, 5, 1)
	finishDatul = new Date(thisYear, 11, 31)
	semester = 2
}

exports.start = function() {
	return startDatul

}

exports.finish = function() {
	return finishDatul
}

exports.nextDatul = function(tgl_datul) {
	if( dateFns.isBefore( new Date(thisYear, thisMonth, thisDate), new Date(thisYear, 7 , 1)) ){
		return '1 Juli ' + thisYear + ' s/d ' + '15 Desember ' + thisYear
	} else {
		return '1 Januari ' + (Number(thisYear) + 1) + ' s/d ' + '15 Juni ' + (Number(thisYear) + 1)
	}
}

exports.openClose = function (petugas_id) {
	// return this.nextDatul()
	return db('tbl_konfigurasi')
	.where('category', 'DATUL')
	.then((data) => {
		let startDatulConfig = Number(_.where(data, {key: 'OPEN'})[0].value)
		let finishDatulConfig = Number(_.where(data, {key: 'CLOSE'})[0].value)
		startDatulConfig = moment(startDatul).add(startDatulConfig, 'days')
		finsihDatulConfig = moment(finishDatul).subtract(finishDatulConfig, 'days')
		if (petugas_id === 1) {
			return {
				status: true,
				semester: semester,
				close: finsihDatulConfig,
				open: startDatulConfig,
				tahun: thisYear
			}
		} else {
			if (  dateFns.isWithinRange(new Date(), startDatulConfig, finsihDatulConfig)  ) {
				return {
					status: true,
					semester: semester,
					close: finsihDatulConfig,
					open: startDatulConfig,
					tahun: thisYear
				}
			} else {
				return {
					status: false,
					semester: semester,
					close: finsihDatulConfig,
					open: startDatulConfig,
					tahun: thisYear
				}
			}

		}
	})
}

exports.kunjungan = function(petugas_id, tipe_kunjungan) {
	return db('tbl_konfigurasi')
	.where('category', tipe_kunjungan)
	.then((data) => {
		// return data
		let openKunjungan = moment(_.where(data, {key: 'OPEN'})[0].value).format('YYYY-MM-DD')
		let closeKunjungan = moment(_.where(data, {key: 'CLOSE'})[0].value).format('YYYY-MM-DD')
		// return openKunjungan
		if (petugas_id === 1) {
			return {
				status: true,
				close: closeKunjungan,
				open: openKunjungan
			}
		} else {
			if (  dateFns.isWithinRange(new Date(), openKunjungan, closeKunjungan)  ) {
				return {
					status: true,
					close: closeKunjungan,
					open: openKunjungan
				}
			} else {
				return {
					status: false,
					close: closeKunjungan,
					open: openKunjungan
				}
			}
		}

	})


}
