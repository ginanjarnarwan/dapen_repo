var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var knex = require('knex')(require('./db').mysql);
var authUtil = require('../util/auth');
var moment = require('moment')
// var log = require('../util/log');

passport.use('web', new LocalStrategy(function(username, password, done) {
  knex('tbl_user').where({ _username: username }).first()
  .then(function(user) {
    if(!user) { return done(null, false) };
    if(!authUtil.comparePass(password, user._password)) {

      return done(null, false)
    } else {
      return done(null, user)
    }
  })
  .catch(function(err){
    return done(err)
  })
}));

var JwtStrategy = require('passport-jwt').Strategy;
ExtractJwt = require('passport-jwt').ExtractJwt;
var opts = {};
opts.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken()
opts.secretOrKey = 'ilhamgantengpisan';

passport.use('mobile', new JwtStrategy(opts, function(jwt_payload, done){
  knex('tbl_login_mobile').where({ username: username }).first()
  .then((user) => {
    if(!user) { return done(null, false) }
    if(!authUtil.comparePass(password, user.password)) {
      return done(null, false)
    } else {
      return done(null, user)
    }
  })
  .catch((err) => {
    return done(err)
  })
}));

// serialize session, only store user id in the session information
passport.serializeUser(function(user, done) {
  done(null, user.ID);
});

// from the user id, figure out who the user is...
passport.deserializeUser(function(userId, done){
  knex('tbl_login_mobile')
  .where('username', userId)
  .first()
  .then((user) => {
    done(null, user)
  })
  .catch((err) => {
    done(err, null)
  })
});
