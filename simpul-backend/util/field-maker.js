module.exports = function(key, value, status, editable) {
	return {
		name: key,
		value: value,
		status: status,
		editable: editable
	}
}