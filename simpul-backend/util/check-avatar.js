const checkPhoto = require('./check-photo');
const fs = require('fs');
const server = require('../config/server');
const find = require('lodash/find');

module.exports = function(id) {
	const avatar_path = './public/avatar';
	const files = fs.readdirSync(avatar_path);
	const dicari = id.toString();
	const found = find(files, (file) => {
		return dicari === file.split(".")[0].split("_")[0]
	});
	if (found){
		return `http://${server.production.ip}:${server.production.port}/avatar/${found}`
	} else {
		return checkPhoto(id)
	}
};
