var moment = require('moment')

module.exports = function(input) {
  if (input == null) {
    return null
  } else {
    return moment(input).locale('id').format('DD MMMM YYYY')
  }
}