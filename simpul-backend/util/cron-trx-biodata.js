const cron = require('node-cron')
const db = require('knex')(require('./config/db').mysql);
const _ = require('underscore')

cron.schedule('* * * * * *', () => {
  db.select()
  .from('trx_biodata')
  .where('approved_by', null)
  .first()
  .then((data) => {
    console.log(data)
  })
})
