var fileExists = require('file-exists')
const server = require('../config/server');

module.exports.umum = function(id) {
  if (fileExists.sync('./public/berita/' + id + '.jpg')) {

    return `${ server.domain }/berita/${ id }.jpg`

  } else {

    return `${ server.domain }/berita/default.jpg`

  }
}
