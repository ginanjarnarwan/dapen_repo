var db = require('knex')(require('../config/db').mysql);

function write(username, description, ip) {
	db('log')
	.insert({
		username: username,
		description: description,
		IP: ip,
		log_time: new Date()
	}).then(() => {})
}

module.exports = {
	write
}