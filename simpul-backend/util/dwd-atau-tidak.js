var dateFns = require('date-fns')
var moment = require('moment')

var datulConfig = require('../config/datul-config')

function dateformat (d) {
  if ( d == null) {
    return null
  } else {
    return new Date(d.getFullYear(), d.getMonth(), d.getDate())
  }
}


module.exports =  function(flag_hak, tgl_meninggal, tgl_pensiun) {
  if ( flag_hak === 0 ) {
    // Tangguhan
    return 11
	} else if (tgl_meninggal != null ) {
    //	Sudah Meninggal
    return 12
  } else if ( dateFns.isWithinRange( dateformat(tgl_pensiun), datulConfig.start(), datulConfig.finish() )   ) {
		// pikiran olangan
    return 13
	} else {
		return 10
	}


}
