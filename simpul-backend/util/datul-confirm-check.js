var dateFns = require('date-fns')
var moment = require('moment')

var datulConfig = require('../config/datul-config')

function momentDate (d) {
  if ( d == null) {
    return null
  } else {
    return new Date(d.getFullYear(), d.getMonth(), d.getDate())
  }
}

module.exports = function(d, aw, m, p) {
  if (dateFns.isWithinRange( momentDate(d) , datulConfig.start(), datulConfig.finish() )) {
  // if ( Mom(d).within(Mom.range( datulConfig.start(), datulConfig.finish() )) ) {
    return 1
  } else if ( (dateFns.isAfter(momentDate(aw),momentDate(d)) && (p == 1) ) || dateFns.isWithinRange( momentDate(d), datulConfig.start(), datulConfig.finish() ) ) {
    return 2
  } else if ( (dateFns.isAfter(momentDate(aw), momentDate(d)) && (p != 1)) || dateFns.isWithinRange( momentDate(d), datulConfig.start(), datulConfig.finish() ) ){
    return 3
  } else if ( dateFns.isAfter(momentDate(m), momentDate(d)) || dateFns.isWithinRange( momentDate(d), datulConfig.start(), datulConfig.finish() )) {
    return 4
  } else {
    return 5
  }
}