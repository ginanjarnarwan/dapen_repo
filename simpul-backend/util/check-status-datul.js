var dateFns = require('date-fns')
var moment = require('moment')

var datulConfig = require('../config/datul-config')

function dateformat (d) {
  if ( d == null) {
    return null
  } else {
    return new Date(d.getFullYear(), d.getMonth(), d.getDate())
  }
}


module.exports =  function(flag_hak, tgl_meninggal, tgl_pensiun, tgl_datul, tgl_datul_aw, tgl_datul_m) {
  if ( flag_hak == 0 ) {
    // tangguhan
    return 6
  } else if (tgl_meninggal != null) {
    // die
    return 5
	} else if (dateFns.isWithinRange( dateformat(tgl_pensiun), datulConfig.start(), datulConfig.finish() )   ) {
    //	tidak wajib datul
    return 5
  } else if ( dateFns.isWithinRange(dateformat(tgl_datul), datulConfig.start(), datulConfig.finish()) ) {
		// selesai datul
    return 1
  } else if ( dateFns.isAfter(dateformat(tgl_datul_aw),dateformat(tgl_datul)) && dateFns.isWithinRange(dateformat(tgl_datul_aw), datulConfig.start(), datulConfig.finish()) ) {
    // konfirmasi dapen
    return 2
  } else if (dateFns.isAfter(dateformat(tgl_datul_m),dateformat(tgl_datul)) && dateFns.isWithinRange(dateformat(tgl_datul_m), datulConfig.start(), datulConfig.finish()) ) {
    // konfirmasi p2tel
    return 3
	} else {
		return 4
	}


}
