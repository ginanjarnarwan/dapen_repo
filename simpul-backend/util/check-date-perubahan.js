var moment = require('moment')

module.exports = function(input) {
  if (input == null) {
    return null
  } else {
    return moment(input, 'DD-MM-YYYY').locale('id').format('YYYY-MM-DD')
  }
}