var fileExists = require('file-exists')
const server = require('../config/server');

module.exports = function(id, type) {
  if (fileExists.sync(`./public/dokumen/${ type }/${ id }.jpg`)) {

    return `${ server.domain }/dokumen/${ type }/${ id }.jpg`

  } else {

    return null

  }
}
