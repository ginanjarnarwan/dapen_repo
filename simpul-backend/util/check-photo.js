var fileExists = require('file-exists')
const server = require('../config/server');

module.exports = function(id) {
  if (fileExists.sync('./public/photo_profile/' + id + '.jpg')) {

    //return `http://139.59.109.145:3000/photo_profile/${ id }.jpg`
    return `${ server.production.domain }/photo_profile/${ id }.jpg`

  } else {

    //return `http://139.59.109.145:3000/photo_profile/default.jpg`
    return `${ server.production.domain }/photo_profile/default.jpg`

  }
}
