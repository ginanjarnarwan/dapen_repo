const fs = require('fs');
const server = require('../../config/server');

module.exports = function(id) {
	try {
		const dicari = id.toString();
		const dokumen_path = `./public/absensi/dokumen/${dicari}`;
		const dokumen_types = ['foto', 'form'];
		if (!fs.existsSync(dokumen_path)) {
			return dokumen_types.map((dok_type) => {return {type: dok_type, uri: null}});
		} else {
			const files = fs.readdirSync(dokumen_path);
			return dokumen_types.map((dok_type) => {
				let retval = {type: dok_type, uri: null};
				for (const file of files) {
					const file_type = file.split(".")[0].split("_")[1];
					if (file_type === dok_type) {
						retval.uri = `${server.production.domain}/absensi/dokumen/${dicari}/${file}`;
						break
					}
				}
				return retval;
			});
		}
	} catch (e) {
		return []
	}
	
	
	// const founds = files.map((file) => {
	// 	let found_type;
	// 	try {
	// 		found_type = file.split(".")[0].split("_")[1];
	// 	} catch (e) {
	// 		// do nothing
	// 	}
	//
	// 	let retval = {type: null, uri: null};
	// 	if (['foto', 'form'].indexOf(found_type) > -1) {
	// 		retval.type = found_type;
	// 		retval.uri = `${server.production.domain}/absensi/dokumen/${dicari}/${file}`;
	// 		return retval;
	// 	}
	// });
	// if (founds.length !== 0){
	// 	return founds;
	// } else {
	// 	return null;
	// }
};