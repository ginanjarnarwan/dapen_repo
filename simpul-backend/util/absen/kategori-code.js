const toString = (kategori_code) => {
	switch (kategori_code) {
		case 1:
			return "Kunjungan";
		case 2:
			return "Non Kunjungan";
		default:
			return "-"
	}
};

module.exports = toString;
