const status_code_to_string = (code) => {
	switch (code) {
		case 0: return "Belum Absen";
		case 1: return "Selesai Absen";
		case 2: return "Menunggu Kunjungan";
		case 3: return "Selesai Kunjungan";
		default: return "-";
	}
};

const kategori_id_to_string = (id) => {
	switch (id) {
		case 1: return "Kunjungan";
		case 2: return "Non Kunjungan";
		default: return "-"
	}
};

const get_status_info = (row) => {
	let status_code = 0;
	if (row.tgl_absen) {
		if (row.kategori_kunjungan === 2){
			status_code = 1;
		}
		if (row.kategori_kunjungan === 1){
			status_code = 2;
		}
		if (row.kategori_kunjungan === 1 && row.tgl_kunjungan) {
			status_code = 3;
		}
	}
	return {
		status_code,
		status_title: status_code_to_string(status_code),
		kategori_title: kategori_id_to_string(row.kategori_kunjungan)
	}
};

module.exports = {
	status_code_to_string,
	kategori_id_to_string,
	get_status_info
};
