const MAX_ROWS = 50;
const DEFAULT_PAGE = 1;

module.exports = function(dbConfig) {
	const knex = require('knex')(dbConfig);
	
	const KnexQueryBuilder = require('knex/lib/query/builder');
	
	// noinspection JSUnresolvedVariable
	KnexQueryBuilder.prototype.paginate = function (per_page, current_page) {
		const pagination = {};
		let per_pages = per_page || MAX_ROWS;
		let page = current_page || DEFAULT_PAGE;
		if (page < 1) page = DEFAULT_PAGE;
		const offset = (page - 1) * per_pages;
		return Promise.all([
				this.clone().count('* as count').first(),
				this.offset(offset).limit(per_pages)
			])
			.then(([total, rows]) => {
				pagination.total = total.count;
				pagination.per_page = per_pages;
				pagination.offset = offset;
				pagination.to = offset + rows.length;
				pagination.last_page = Math.ceil(count / per_pages);
				pagination.current_page = page;
				pagination.from = offset;
				pagination.data = rows;
				return pagination;
			});
	};
	
	knex.queryBuilder = function () {
		return new KnexQueryBuilder(knex.client);
	};
	
	return knex;
};
