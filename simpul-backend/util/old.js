var lazy = require('lazy.js');
var moment = require('moment');
var fileExists = require('file-exists');
var path = require('path');
var datulConfig = require('../config/datul-config');
var dateFns = require('date-fns');
var MomentRange = require('moment-range');
var Mom = MomentRange.extendMoment(moment);

// middleware to see if we're authenticated or not
exports.ensureAuthenticated = function (req, res, next) {
  if (req.isAuthenticated()) {
    return next();
  }
  res.redirect('/');
}

exports.genderfunction = function (gender){

  if (gender === 0) {
    return 'Pria'
  } else if(gender === 1) {
    return 'wanita'
  } else {
    return '-'
  }

}

exports.ifnulltext = function (data){
  if(data == "NULL") {
    return ''
  } else {
    return data
  }
}

exports.concatenull = function(text1, text2) {
  if(text1 == null && text2 == null) {
    return ''
  } else if(text1 == null) {
    return text2
  } else if(text2 == null) {
    return text1
  } else {
    return text1 + " - " + text2
  }

}

exports.postchecknull = function(input) {
  if(input == '') {
    return null
  } else {
    return input
  }
}

exports.compareValue = function(before, after) {
  if ( before == after ){
    return before
  } else {
    return after
  }
}

exports.datechecker = function(input) {
  if (input == null) {
    return null
  } else {
    return moment(input).locale('id').format('D MMMM YYYY')
  }
}

exports.datulCheck = function(input) {
  if (input == null || !(moment(input).isBetween(datulConfig.start(), datulConfig.finish()))) {
    return 'Belum Datul'
  } else {
    return moment(input).locale('id').format('D MMMM YYYY')
  }
}

exports.statusTidakDatul = function(dod, dor) {
  if( dod == null) {
    return 'Pensiun Periode Ini'
  } else {
    return 'Sudah Meninggal'
  }
}

function momentDate (d) {
  if ( d == null) {
    return null
  } else {
    return new Date(d.getFullYear(), d.getMonth(), d.getDate())
  }
}

exports.datulConfirmCheck = function(d, aw, m, p) {
  if (dateFns.isWithinRange( momentDate(d) , datulConfig.start(), datulConfig.finish() )) {
  // if ( Mom(d).within(Mom.range( datulConfig.start(), datulConfig.finish() )) ) {
    return 1
  } else if ( (dateFns.isAfter(momentDate(aw),momentDate(d)) && (p == 1) ) || dateFns.isWithinRange( momentDate(d), datulConfig.start(), datulConfig.finish() ) ) {
    return 2
  } else if ( (dateFns.isAfter(momentDate(aw), momentDate(d)) && (p != 1)) || dateFns.isWithinRange( momentDate(d), datulConfig.start(), datulConfig.finish() ) ){
    return 3
  } else if ( dateFns.isAfter(momentDate(m), momentDate(d)) || dateFns.isWithinRange( momentDate(d), datulConfig.start(), datulConfig.finish() )) {
    return 4
  } else {
    return 5
  }
}

exports.datulpencarian = function(fh, dod, dor, tgl) {
  if ( dod != null ) {
    return 'Sudah Meninggal'
  } else if (moment(dor).isBetween(datulConfig.start(), datulConfig.finish())) {
    return 'Pensiun Periode Ini'
  } else if(fh == 0) {
    return 'Bukan Datul Simpul'
  } else if (fh == 1 && tgl == null) {
    return 'Belum Datul'
  } else {
    return moment(tgl).locale('id').format('D MMMM YYYY')
  }
}

exports.checkPhoto = function(id) {
  if (fileExists.sync('./client/public/assets/photo_profile/' + id + '.jpg')) {
    return id
  } else {
    return "default"
  }
}

// exports.checkPhoto = function (id) {
//   fileExists('./client/public/assets/photo_profile/' + id + '.jpg', function (err, exists) {
//     if (exists) {
//       return id
//     } else {
//       return 'default'
//     }
//   })
// }

exports.checkPhotoUser = function(id) {
  if (fileExists.sync('./client/public/assets/user_profile/' + id + '.jpg')) {
    return id
  } else {
    return "default"
  }
}

exports.gabungNama = function(pmp, tipe, peserta) {
  if(tipe == null) {
    return pmp
  } else {
    return pmp + ' ' + tipe + ' ' + peserta
  }
}

exports.checkStatus = function(pmp, peserta, calon) {
  if (pmp == 1) {
    return 'pmp'
  } else if (peserta == 1) {
    return 'peserta'
  } else if (calon == 1) {
    return 'calon'
  }
}

exports.checkFinger = function(data) {
  if (data === '') {
    return 'false'
  } else {
    return 'true'
  }
}

exports.getNIK = function(x) {
  var i = x.toString()
  return (i).substring(i.length - 6, i.length)
}

exports.getNAW = function(x) {
  var i = x.toString()
  return (i).substring(0, i.length - 6)
}

exports.p2telBank = function(level,p2tel,bank,cabangBank) {
  if ( level === 5) {
    return 'P2TEL ' + p2tel
  } else {
    return bank + ' ' + cabangBank
  }
}