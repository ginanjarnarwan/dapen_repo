module.exports = function (oldval, newval, confirm) {
	if (confirm) {
		return newval
	} else {
		return oldval
	}
}