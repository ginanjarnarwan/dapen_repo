var bcrypt = require('bcrypt-nodejs');
var knex = require('knex')(require('../config/db').mysql);

function comparePass(userPassword, databasePassword) {
  return bcrypt.compareSync(userPassword, databasePassword);
}

function ensureAuth(req, res, next) {
	if (req.isAuthenticated()) {
    	return next();
  	}
  		res.redirect('/');
}

function hashSync(userPassword) {
	return bcrypt.hashSync(userPassword)
}

module.exports = {
	comparePass,
	ensureAuth,
	hashSync
};