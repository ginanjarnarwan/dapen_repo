const cron = require('node-cron')
const db = require('knex')(require('./config/db').mysql);
const _ = require('underscore')

cron.schedule('* * * * * *', () => {
  db.select()
  .from('trx_biodata')
  .where('approve_by', null)
  .first()
  .then((data) => {

    db.select()
    .from('ref_golongan_darah')
    .then((dataGD) => {

      let kode = data.kode_laporan
      let id_pmp = data.tbl_pmp
      let dataPerubahan = data.data_perubahan
      let kolomPerubahan
      if (kode === 6001) {
        kolomPerubahan = '_tgl_lahir'
      } else if (kode === 6002) {
        kolomPerubahan = '_ktp'
      } else if (kode === 6003 ) {
        kolomPerubahan = '_npwp'
      } else if (kode === 6004) {
        kolomPerubahan = '_golongan_darah'
        dataGD = _.where(dataGD, { _golongan_darah: 'O' })
        dataPerubahan = dataGD[0].ID
      } else if (kode === 6005) {
        kolomPerubahan = '_agama'
        dataPerubahan = Number(dataPerubahan)
      } else if (kode === 6006) {
        kolomPerubahan = '_gender'
      } else if (kode === 6007) {
        kolomPerubahan = '_tgl_meninggal'
      } else if (kode === 6008) {
        kolomPerubahan = '_rekening'
      }

      db('tbl_pmp')
      .update(kolomPerubahan, dataPerubahan)
      .update('updatedAt', new Date())
      .where('ID', id_pmp)
      .then(() => {
        db('trx_biodata')
        .update({
          approve_by: 'SYSTEM',
          approve_at: new Date
        })
        .where('id', data.id)
        .then(() => {
          console.log(`done trx biodata no ${ data.id } - id pmp: ${ id_pmp }`)
        })
      })

    })



  })
})
